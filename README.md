===============
c2mpfr tool
===============
Convert programs from c language to the version uses mpfr library. This enable future analysis on floating point precision of each variable in the program.

This tool is built from pycparser v2.14 . Please find the original readme file for information regarding Pycparser project.
