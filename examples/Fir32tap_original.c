#include <stdio.h>
#include <stdlib.h>
double h[32]= {0.2234, -0.844, 0.5823, -0.3342, -0.3418, 0.6263, 0.9566, -0.742, 0.958, 0.184, 0.362, 0.098, -0.892, -0.79, -0.964, 0.69, 0.592, -0.968, -0.757, -0.7628, 0.1752, 0.7516, -0.6757, -0.1762, 0.755, -0.06572, 0.7573, 0.6502, 0.6736, -0.6752, -0.2756, -0.97656};
float x[32];
float y, acc;

float fir(float input)
{
	int i;
	x[0] = input;

	acc = x[0] * h[0];

	for(i = 31; i>0; i--)
	{
		acc = acc + x[i]*h[i];
		x[i] = x[i-1];
		//printf("%.5f \n", x[i]);
	}
	y = acc;
	return y;
}

int main()
{
	int i;
    srand(1);
	for (i=0;i<INPUT_LEN;i++)
	{
		signal_in[i] = (float)(rand() - RAND_MAX/2)/(RAND_MAX/2);
	}

	for (i=0;i<INPUT_LEN;i++)
	{
		signal_out[i] = fir(signal_in[i]);
		printf("%f,",signal_out[i]);
	}
	return 0;
}
