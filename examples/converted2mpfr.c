//#include <stdio.h>
//~ #include <time.h>
//#include <stdlib.h>
//#include <math.h>
#include <mpfr.h>
#define LEN 13 
 int config_vals[LEN];
mpfr_t h_mpfr;
mpfr_t x_mpfr;
mpfr_t y;
mpfr_t acc;
mpfr_t z_mpfr;
mpfr_t z1_mpfr;
mpfr_t signal_in_mpfr;
mpfr_t t;
mpfr_t signal_out_mpfr;
  mpfr_t input_fir;
        mpfr_t temp_var_1;
  mpfr_t f_main;
  mpfr_t l_main;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
int init_mpfr() { 
mpfr_init2(h_mpfr, config_vals[1]);
mpfr_init2(x_mpfr, config_vals[2]);
mpfr_init2(y, config_vals[3]);
mpfr_init2(acc, config_vals[4]);
mpfr_init2(z_mpfr, config_vals[5]);
mpfr_init2(z1_mpfr, config_vals[6]);
mpfr_init2(signal_in_mpfr, config_vals[7]);
mpfr_init2(t, config_vals[8]);
mpfr_init2(signal_out_mpfr, config_vals[9]);
mpfr_init2(input_fir, config_vals[10]);
        mpfr_init2 (temp_var_1, config_vals[0]);
  mpfr_init2(f_main, config_vals[11]);
  mpfr_init2(l_main, config_vals[12]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

double h[32] = {0.2234, -0.844, 0.5823, -0.3342, -0.3418, 0.6263, 0.9566, -0.742, 0.958, 0.184, 0.362, 0.098, -0.892, -0.79, -0.964, 0.69, 0.592, -0.968, -0.757, -0.7628, 0.1752, 0.7516, -0.6757, -0.1762, 0.755, -0.06572, 0.7573, 0.6502, 0.6736, -0.6752, -0.2756, -0.97656};
float *x;
float z[3][4];
float **z1;
float signal_in[96];
float signal_out[96];
float fir(float input)
{
  int i;
  mpfr_set(x_mpfr, input_fir, MPFR_RNDZ);
  x[0] = mpfr_get_d(x_mpfr, MPFR_RNDZ);
 mpfr_set_d(input_fir,input,MPFR_RNDZ);
 mpfr_set_d(h_mpfr,h[0],MPFR_RNDZ);
 mpfr_set_d(x_mpfr,x[0],MPFR_RNDZ);
    mpfr_mul(acc, x_mpfr, h_mpfr, MPFR_RNDZ);
  for (i = 31; i > 0; i--){
  {
    ;
 mpfr_set_d(h_mpfr,h[i],MPFR_RNDZ);
 mpfr_set_d(x_mpfr,x[i],MPFR_RNDZ);

        mpfr_mul(temp_var_1, x_mpfr, h_mpfr, MPFR_RNDZ);
        mpfr_add(acc, acc, (temp_var_1), MPFR_RNDZ);
    //array_ref_output_stack != null from parsing right hand side, dump all itermediate operations here
 mpfr_set_d(x_mpfr,x[i - 1],MPFR_RNDZ);
mpfr_set(x_mpfr, x_mpfr, MPFR_RNDZ);
    x[i] = mpfr_get_d(x_mpfr, MPFR_RNDZ);
  }

    }

  mpfr_set(y, acc, MPFR_RNDZ);
  return mpfr_get_d(y, MPFR_RNDZ);
}

int main()
{
 init_readconfig();
  int i;
  if (i == 1)
  {
    printf("test\n");
  }

  ;
  mpfr_set_d(f_main, 0.0, MPFR_RNDZ);
  if ((mpfr_cmp_d(f_main,0) == 0)
//original  f_main == 0
)
  {
    printf("test\n");
  }

  mpfr_set_d(l_main, 1.0, MPFR_RNDZ);
  if ((mpfr_cmp(f_main,l_main) <= 0)
//original  f_main <= l_main
)
  {
    printf("test");
  }

  ;
 mpfr_set_d(z1_mpfr,z1[2][2],MPFR_RNDZ);

    mpfr_mul(temp_var_2, f_main, z1_mpfr, MPFR_RNDZ);

    mpfr_mul_d(temp_var_3, l_main, 3.0, MPFR_RNDZ);
if ((mpfr_cmp((temp_var_2),(temp_var_3)) <= 0)
//original  (temp_var_2) <= (temp_var_3)
)
  {
    print("test");
  }

  return 0;
}

//end of conversion, hopefully it will work :)