#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "randdp.h"
#include <mpfr.h>
typedef enum { false, true } logical;
#define LEN 32 
 int config_vals[LEN];
  mpfr_t zeta_main;
  mpfr_t rnorm_main;
  mpfr_t norm_temp1_main;
  mpfr_t norm_temp2_main;
    mpfr_t temp_var_1;
            mpfr_t temp_var_2;
            mpfr_t temp_var_3;
            mpfr_t temp_var_4;
            mpfr_t temp_var_5;
        mpfr_t temp_var_6;
            mpfr_t temp_var_7;
            mpfr_t temp_var_8;
            mpfr_t temp_var_9;
            mpfr_t temp_var_10;
        mpfr_t temp_var_11;
        mpfr_t temp_var_12;
            mpfr_t temp_var_13;
        mpfr_t temp_var_14;
        mpfr_t temp_var_15;
  mpfr_t x_temp_conj_grad;
  mpfr_t z_temp_conj_grad;
  mpfr_t a_temp_conj_grad;
  mpfr_t p_temp_conj_grad;
  mpfr_t q_temp_conj_grad;
  mpfr_t r_temp_conj_grad;
  mpfr_t rnorm_temp_conj_grad;
  mpfr_t d_conj_grad;
  mpfr_t sum_conj_grad;
  mpfr_t rho_conj_grad;
  mpfr_t rho0_conj_grad;
  mpfr_t alpha_conj_grad;
  mpfr_t beta_conj_grad;
        mpfr_t temp_var_16;
                mpfr_t temp_var_17;
            mpfr_t temp_var_18;
            mpfr_t temp_var_19;
            mpfr_t temp_var_20;
            mpfr_t temp_var_21;
            mpfr_t temp_var_22;
            mpfr_t temp_var_23;
        mpfr_t temp_var_24;
  mpfr_t vc_temp_makea;
        mpfr_t temp_var_25;
        mpfr_t temp_var_26;
            mpfr_t temp_var_27;
  mpfr_t a_temp_sparse;
  mpfr_t rcond_temp_sparse;
  mpfr_t shift_temp_sparse;
  mpfr_t size_sparse;
  mpfr_t scale_sparse;
  mpfr_t ratio_sparse;
  mpfr_t va_sparse;
    mpfr_t temp_var_28;
            mpfr_t temp_var_29;
            mpfr_t temp_var_30;
        mpfr_t temp_var_31;
    mpfr_t temp_var_32;
                    mpfr_t temp_var_33;
                        mpfr_t temp_var_34;
        mpfr_t temp_var_35;
            mpfr_t temp_var_36;
        mpfr_t temp_var_37;
  mpfr_t v_temp_sprnvc;
  mpfr_t vecelt_sprnvc;
  mpfr_t vecloc_sprnvc;
            mpfr_t temp_var_38;
        mpfr_t temp_var_39;
    mpfr_t temp_var_40;
        mpfr_t temp_var_41;
  mpfr_t x_temp_icnvrt;
  mpfr_t val_temp_vecset;
  mpfr_t v_temp_vecset;
        mpfr_t temp_var_42;
    mpfr_t temp_var_43;
int init_mpfr() { 
  mpfr_init2(zeta_main, config_vals[1]);
  mpfr_init2(rnorm_main, config_vals[2]);
  mpfr_init2(norm_temp1_main, config_vals[3]);
  mpfr_init2(norm_temp2_main, config_vals[4]);
    mpfr_init2 (temp_var_1, config_vals[0]);
            mpfr_init2 (temp_var_2, config_vals[0]);
            mpfr_init2 (temp_var_3, config_vals[0]);
            mpfr_init2 (temp_var_4, config_vals[0]);
            mpfr_init2 (temp_var_5, config_vals[0]);
        mpfr_init2 (temp_var_6, config_vals[0]);
            mpfr_init2 (temp_var_7, config_vals[0]);
            mpfr_init2 (temp_var_8, config_vals[0]);
            mpfr_init2 (temp_var_9, config_vals[0]);
            mpfr_init2 (temp_var_10, config_vals[0]);
        mpfr_init2 (temp_var_11, config_vals[0]);
        mpfr_init2 (temp_var_12, config_vals[0]);
            mpfr_init2 (temp_var_13, config_vals[0]);
        mpfr_init2 (temp_var_14, config_vals[0]);
        mpfr_init2 (temp_var_15, config_vals[0]);
  mpfr_init2(x_temp_conj_grad, config_vals[5]);
  mpfr_init2(z_temp_conj_grad, config_vals[6]);
  mpfr_init2(a_temp_conj_grad, config_vals[7]);
  mpfr_init2(p_temp_conj_grad, config_vals[8]);
  mpfr_init2(q_temp_conj_grad, config_vals[9]);
  mpfr_init2(r_temp_conj_grad, config_vals[10]);
  mpfr_init2(rnorm_temp_conj_grad, config_vals[11]);
  mpfr_init2(d_conj_grad, config_vals[12]);
  mpfr_init2(sum_conj_grad, config_vals[13]);
  mpfr_init2(rho_conj_grad, config_vals[14]);
  mpfr_init2(rho0_conj_grad, config_vals[15]);
  mpfr_init2(alpha_conj_grad, config_vals[16]);
  mpfr_init2(beta_conj_grad, config_vals[17]);
        mpfr_init2 (temp_var_16, config_vals[0]);
                mpfr_init2 (temp_var_17, config_vals[0]);
            mpfr_init2 (temp_var_18, config_vals[0]);
            mpfr_init2 (temp_var_19, config_vals[0]);
            mpfr_init2 (temp_var_20, config_vals[0]);
            mpfr_init2 (temp_var_21, config_vals[0]);
            mpfr_init2 (temp_var_22, config_vals[0]);
            mpfr_init2 (temp_var_23, config_vals[0]);
        mpfr_init2 (temp_var_24, config_vals[0]);
  mpfr_init2(vc_temp_makea, config_vals[18]);
        mpfr_init2 (temp_var_25, config_vals[0]);
        mpfr_init2 (temp_var_26, config_vals[0]);
            mpfr_init2 (temp_var_27, config_vals[0]);
  mpfr_init2(a_temp_sparse, config_vals[19]);
  mpfr_init2(rcond_temp_sparse, config_vals[20]);
  mpfr_init2(shift_temp_sparse, config_vals[21]);
  mpfr_init2(size_sparse, config_vals[22]);
  mpfr_init2(scale_sparse, config_vals[23]);
  mpfr_init2(ratio_sparse, config_vals[24]);
  mpfr_init2(va_sparse, config_vals[25]);
    mpfr_init2 (temp_var_28, config_vals[0]);
            mpfr_init2 (temp_var_29, config_vals[0]);
            mpfr_init2 (temp_var_30, config_vals[0]);
        mpfr_init2 (temp_var_31, config_vals[0]);
    mpfr_init2 (temp_var_32, config_vals[0]);
                    mpfr_init2 (temp_var_33, config_vals[0]);
                        mpfr_init2 (temp_var_34, config_vals[0]);
        mpfr_init2 (temp_var_35, config_vals[0]);
            mpfr_init2 (temp_var_36, config_vals[0]);
        mpfr_init2 (temp_var_37, config_vals[0]);
  mpfr_init2(v_temp_sprnvc, config_vals[26]);
  mpfr_init2(vecelt_sprnvc, config_vals[27]);
  mpfr_init2(vecloc_sprnvc, config_vals[28]);
            mpfr_init2 (temp_var_38, config_vals[0]);
        mpfr_init2 (temp_var_39, config_vals[0]);
    mpfr_init2 (temp_var_40, config_vals[0]);
        mpfr_init2 (temp_var_41, config_vals[0]);
  mpfr_init2(x_temp_icnvrt, config_vals[29]);
  mpfr_init2(val_temp_vecset, config_vals[30]);
  mpfr_init2(v_temp_vecset, config_vals[31]);
        mpfr_init2 (temp_var_42, config_vals[0]);
    mpfr_init2 (temp_var_43, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

static int colidx[(14000 * (11 + 1)) * (11 + 1)];
static int rowstr[14000 + 1];
static int iv[14000];
static int arow[14000];
static int acol[14000 * (11 + 1)];
static double aelt[14000 * (11 + 1)];
static double a[(14000 * (11 + 1)) * (11 + 1)];
static double x[14000 + 2];
static double z[14000 + 2];
static double p[14000 + 2];
static double q[14000 + 2];
static double r[14000 + 2];
static int naa;
static int nzz;
static int firstrow;
static int lastrow;
static int firstcol;
static int lastcol;
static double amult;
static double tran;
static logical timeron;
static void conj_grad(int colidx[], int rowstr[], double x[], double z[], double a[], double p[], double q[], double r[], double *rnorm);
static void makea(int n, int nz, double a[], int colidx[], int rowstr[], int firstrow, int lastrow, int firstcol, int lastcol, int arow[], int acol[][11 + 1], double aelt[][11 + 1], int iv[]);
static void sparse(double a[], int colidx[], int rowstr[], int n, int nz, int nozer, int arow[], int acol[][11 + 1], double aelt[][11 + 1], int firstrow, int lastrow, int nzloc[], double rcond, double shift);
static void sprnvc(int n, int nz, int nn1, double v[], int iv[]);
static int icnvrt(double x, int ipwr2);
static void vecset(int n, double v[], int iv[], int *nzv, int i, double val);
int main(int argc, char *argv[])
{
 init_readconfig();
  int i;
  int j;
  int k;
  int it;
double t, mflops, tmax;
  char Class;
  logical verified;
  
 double zeta_verify_value, epsilon, err;
  
  double rnorm_main_temp;
  
  
  char *t_names[3];


  firstrow = 0;
  lastrow = 14000 - 1;
  firstcol = 0;
  lastcol = 14000 - 1;
  if ((((14000 == 1400) && (11 == 7)) && (15 == 15)) && (20.0 == 10))
  {
    Class = 'S';
    zeta_verify_value = 8.5971775078648;
  }
  else
    if ((((14000 == 7000) && (11 == 8)) && (15 == 15)) && (20.0 == 12))
  {
    Class = 'W';
    zeta_verify_value = 10.362595087124;
  }
  else
    if ((((14000 == 14000) && (11 == 11)) && (15 == 15)) && (20.0 == 20))
  {
    Class = 'A';
    zeta_verify_value = 17.130235054029;
  }
  else
    if ((((14000 == 75000) && (11 == 13)) && (15 == 75)) && (20.0 == 60))
  {
    Class = 'B';
    zeta_verify_value = 22.712745482631;
  }
  else
    if ((((14000 == 150000) && (11 == 15)) && (15 == 75)) && (20.0 == 110))
  {
    Class = 'C';
    zeta_verify_value = 28.973605592845;
  }
  else
    if ((((14000 == 1500000) && (11 == 21)) && (15 == 100)) && (20.0 == 500))
  {
    Class = 'D';
    zeta_verify_value = 52.514532105794;
  }
  else
    if ((((14000 == 9000000) && (11 == 26)) && (15 == 100)) && (20.0 == 1500))
  {
    Class = 'E';
    zeta_verify_value = 77.522164599383;
  }
  else
  {
    Class = 'U';
  }







  //~ printf("\n\n NAS Parallel Benchmarks (NPB3.3-SER-C) - CG Benchmark\n\n");
  //~ printf(" Size: %11d\n", 14000);
  //~ printf(" Iterations: %5d\n", 15);
  //~ printf("\n");
  naa = 14000;
  nzz = (14000 * (11 + 1)) * (11 + 1);
  tran = 314159265.0;
  amult = 1220703125.0;
  mpfr_set_d(zeta_main, randlc(&tran, amult), MPFR_RNDN);
  makea(naa, nzz, a, colidx, rowstr, firstrow, lastrow, firstcol, lastcol, arow, (int (*)[11 + 1]) ((void *) acol), (double (*)[11 + 1]) ((void *) aelt), iv);
  for (j = 0; j < ((lastrow - firstrow) + 1); j++){
  {
    for (k = rowstr[j]; k < rowstr[j + 1]; k++){
    {
      colidx[k] = colidx[k] - firstcol;
    }

        }

  }

    }

  for (i = 0; i < (14000 + 1); i++){
  {
    x[i] = 1.0;
  }

    }

  for (j = 0; j < ((lastcol - firstcol) + 1); j++){
  {
    q[j] = 0.0;
    z[j] = 0.0;
    r[j] = 0.0;
    p[j] = 0.0;
  }

    }

  mpfr_set_d(zeta_main, 0.0, MPFR_RNDN);
  for (it = 1; it <= 1; it++){
  {
	rnorm_main_temp = mpfr_get_d(rnorm_main, MPFR_RNDN);
    conj_grad(colidx, rowstr, x, z, a, p, q, r, &rnorm_main_temp);
    mpfr_set_d(rnorm_main, rnorm_main_temp, MPFR_RNDN);
    
    mpfr_set_d(norm_temp1_main, 0.0, MPFR_RNDN);
    mpfr_set_d(norm_temp2_main, 0.0, MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      

            mpfr_set_d(temp_var_3, x[j], MPFR_RNDN);

            mpfr_mul_d(temp_var_4, temp_var_3, z[j], MPFR_RNDN);
            mpfr_add(norm_temp1_main, norm_temp1_main, (temp_var_4), MPFR_RNDN);
      
            mpfr_add_d(norm_temp2_main, norm_temp2_main, (z[j] * z[j]), MPFR_RNDN);
    }

        }

    mpfr_set_d(norm_temp2_main, 1.0 / sqrt(mpfr_get_d(norm_temp2_main, MPFR_RNDN)), MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      

            mpfr_mul_d(temp_var_7, norm_temp2_main, z[j], MPFR_RNDN);

x[j] = mpfr_get_d(temp_var_7, MPFR_RNDN);
    }

        }

  }

    }

  for (i = 0; i < (14000 + 1); i++){
  {
    x[i] = 1.0;
  }

    }

  mpfr_set_d(zeta_main, 0.0, MPFR_RNDN);
  for (it = 1; it <= 15; it++){
  {
	 
	rnorm_main_temp = mpfr_get_d(rnorm_main, MPFR_RNDN);  
    conj_grad(colidx, rowstr, x, z, a, p, q, r, &rnorm_main_temp);
    mpfr_set_d(rnorm_main, rnorm_main_temp, MPFR_RNDN);
    
    mpfr_set_d(norm_temp1_main, 0.0, MPFR_RNDN);
    mpfr_set_d(norm_temp2_main, 0.0, MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      
            mpfr_set_d(temp_var_8, x[j], MPFR_RNDN);

            mpfr_mul_d(temp_var_9, temp_var_8, z[j], MPFR_RNDN);
            mpfr_add(norm_temp1_main, norm_temp1_main, (temp_var_9), MPFR_RNDN);
      
            mpfr_add_d(norm_temp2_main, norm_temp2_main, (z[j] * z[j]), MPFR_RNDN);
    }

        }

    mpfr_set_d(norm_temp2_main, 1.0 / sqrt(mpfr_get_d(norm_temp2_main, MPFR_RNDN)), MPFR_RNDN);
    

        mpfr_d_div(temp_var_12, 1.0, norm_temp1_main, MPFR_RNDN);
        mpfr_add_d(zeta_main, (temp_var_12), 20.0, MPFR_RNDN);
 //~ 
    //~ if (it == 1)
      //~ printf("\n   iteration           ||r||                 zeta\n");
//~ 
    //~ printf("    %5d       %20.14E%20.13f\n", it, mpfr_get_d(rnorm_main, MPFR_RNDN), mpfr_get_d(zeta_main, MPFR_RNDN));
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      
            mpfr_mul_d(temp_var_13, norm_temp2_main, z[j], MPFR_RNDN);

x[j] = mpfr_get_d(temp_var_13, MPFR_RNDN);
    }

        }

  }

    }

  //~ printf(" Benchmark completed\n");
  epsilon = 1.0e-10;
  //~ if (Class != 'U')
  //~ {
    err = fabs(mpfr_get_d(zeta_main, MPFR_RNDN) - zeta_verify_value) / zeta_verify_value;
    printf("%.12lf,", err);
    //~ if (err <= epsilon)
    //~ {
      //~ verified = true;
      //~ printf(" VERIFICATION SUCCESSFUL\n");
      //~ printf(" Zeta is    %20.13E\n", mpfr_get_d(zeta_main, MPFR_RNDN));
      //~ printf(" Error is   %20.13E\n", err);
    //~ }
    //~ else
    //~ {
      //~ verified = false;
      //~ printf(" VERIFICATION FAILED\n");
      //~ printf(" Zeta                %20.13E\n", mpfr_get_d(zeta_main, MPFR_RNDN));
      //~ printf(" The correct zeta is %20.13E\n", zeta_verify_value);
    //~ }
//~ 
  //~ }
  //~ else
  //~ {
    //~ verified = false;
    //~ printf(" Problem size unknown\n");
    //~ printf(" NO VERIFICATION PERFORMED\n");
  //~ }

  if (t != 0.0)
  {
    mflops = ((((double) ((2 * 15) * 14000)) * (((3.0 + ((double) (11 * (11 + 1)))) + (25.0 * (5.0 + ((double) (11 * (11 + 1)))))) + 3.0)) / t) / 1000000.0;
  }
  else
  {
    mflops = 0.0;
  }

  return 0;
}

static void conj_grad(int colidx[], int rowstr[], double x[], double z[], double a[], double p[], double q[], double r[], double *rnorm)
{
  int j;
  int k;
  int cgit;
  int cgitmax = 25;
  mpfr_set_d(rho_conj_grad, 0.0, MPFR_RNDN);
  for (j = 0; j < (naa + 1); j++){
  {
    mpfr_set_d(q_temp_conj_grad, 0.0, MPFR_RNDN);
    
q[j] = mpfr_get_d(q_temp_conj_grad, MPFR_RNDN);
    mpfr_set_d(z_temp_conj_grad, 0.0, MPFR_RNDN);
    
z[j] = mpfr_get_d(z_temp_conj_grad, MPFR_RNDN);
    mpfr_set_d(x_temp_conj_grad, x[j], MPFR_RNDN);
    
r[j] = mpfr_get_d(x_temp_conj_grad, MPFR_RNDN);
    mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
    
p[j] = mpfr_get_d(r_temp_conj_grad, MPFR_RNDN);
  }

    }

  for (j = 0; j < ((lastcol - firstcol) + 1); j++){
  {
    mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
    
        mpfr_mul(temp_var_16, r_temp_conj_grad, r_temp_conj_grad, MPFR_RNDN);
        mpfr_add(rho_conj_grad, rho_conj_grad, (temp_var_16), MPFR_RNDN);
  }

    }

  for (cgit = 1; cgit <= cgitmax; cgit++){
  {
    for (j = 0; j < ((lastrow - firstrow) + 1); j++){
    {
      mpfr_set_d(sum_conj_grad, 0.0, MPFR_RNDN);
      for (k = rowstr[j]; k < rowstr[j + 1]; k++){
      {
        mpfr_set_d(a_temp_conj_grad, a[k], MPFR_RNDN);
        mpfr_set_d(p_temp_conj_grad, p[colidx[k]], MPFR_RNDN);
        
                mpfr_mul(temp_var_17, a_temp_conj_grad, p_temp_conj_grad, MPFR_RNDN);
                mpfr_add(sum_conj_grad, sum_conj_grad, (temp_var_17), MPFR_RNDN);
      }

            }

      mpfr_set(q_temp_conj_grad, sum_conj_grad, MPFR_RNDN);
      
q[j] = mpfr_get_d(q_temp_conj_grad, MPFR_RNDN);
    }

        }

    mpfr_set_d(d_conj_grad, 0.0, MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      mpfr_set_d(p_temp_conj_grad, p[j], MPFR_RNDN);
      mpfr_set_d(q_temp_conj_grad, q[j], MPFR_RNDN);
      
            mpfr_mul(temp_var_18, p_temp_conj_grad, q_temp_conj_grad, MPFR_RNDN);
            mpfr_add(d_conj_grad, d_conj_grad, (temp_var_18), MPFR_RNDN);
    }

        }

            mpfr_div(alpha_conj_grad, rho_conj_grad, d_conj_grad, MPFR_RNDN);
    mpfr_set(rho0_conj_grad, rho_conj_grad, MPFR_RNDN);
    mpfr_set_d(rho_conj_grad, 0.0, MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      mpfr_set_d(p_temp_conj_grad, p[j], MPFR_RNDN);
      mpfr_set_d(z_temp_conj_grad, z[j], MPFR_RNDN);
      
            mpfr_mul(temp_var_19, alpha_conj_grad, p_temp_conj_grad, MPFR_RNDN);
            mpfr_add(z_temp_conj_grad, z_temp_conj_grad, (temp_var_19), MPFR_RNDN);
      
z[j] = mpfr_get_d(z_temp_conj_grad, MPFR_RNDN);
      mpfr_set_d(q_temp_conj_grad, q[j], MPFR_RNDN);
      mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
      
            mpfr_mul(temp_var_20, alpha_conj_grad, q_temp_conj_grad, MPFR_RNDN);
            mpfr_sub(r_temp_conj_grad, r_temp_conj_grad, (temp_var_20), MPFR_RNDN);
      
r[j] = mpfr_get_d(r_temp_conj_grad, MPFR_RNDN);
    }

        }

    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
      
            mpfr_mul(temp_var_21, r_temp_conj_grad, r_temp_conj_grad, MPFR_RNDN);
            mpfr_add(rho_conj_grad, rho_conj_grad, (temp_var_21), MPFR_RNDN);
    }

        }

            mpfr_div(beta_conj_grad, rho_conj_grad, rho0_conj_grad, MPFR_RNDN);
    for (j = 0; j < ((lastcol - firstcol) + 1); j++){
    {
      mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
      mpfr_set_d(p_temp_conj_grad, p[j], MPFR_RNDN);
      
            mpfr_mul(temp_var_22, beta_conj_grad, p_temp_conj_grad, MPFR_RNDN);
            mpfr_add(p_temp_conj_grad, r_temp_conj_grad, (temp_var_22), MPFR_RNDN);
      
p[j] = mpfr_get_d(p_temp_conj_grad, MPFR_RNDN);
    }

        }

  }

    }

  mpfr_set_d(sum_conj_grad, 0.0, MPFR_RNDN);
  for (j = 0; j < ((lastrow - firstrow) + 1); j++){
  {
    mpfr_set_d(d_conj_grad, 0.0, MPFR_RNDN);
    for (k = rowstr[j]; k < rowstr[j + 1]; k++){
    {
      mpfr_set_d(a_temp_conj_grad, a[k], MPFR_RNDN);
      mpfr_set_d(z_temp_conj_grad, z[colidx[k]], MPFR_RNDN);
      
            mpfr_mul(temp_var_23, a_temp_conj_grad, z_temp_conj_grad, MPFR_RNDN);
            mpfr_add(d_conj_grad, d_conj_grad, (temp_var_23), MPFR_RNDN);
    }

        }

    mpfr_set(r_temp_conj_grad, d_conj_grad, MPFR_RNDN);
    
r[j] = mpfr_get_d(r_temp_conj_grad, MPFR_RNDN);
  }

    }

  for (j = 0; j < ((lastcol - firstcol) + 1); j++){
  {
    mpfr_set_d(x_temp_conj_grad, x[j], MPFR_RNDN);
    mpfr_set_d(r_temp_conj_grad, r[j], MPFR_RNDN);
            mpfr_sub(d_conj_grad, x_temp_conj_grad, r_temp_conj_grad, MPFR_RNDN);
    
        mpfr_mul(temp_var_24, d_conj_grad, d_conj_grad, MPFR_RNDN);
        mpfr_add(sum_conj_grad, sum_conj_grad, (temp_var_24), MPFR_RNDN);
  }

    }

  mpfr_set_d(rnorm_temp_conj_grad, sqrt(mpfr_get_d(sum_conj_grad, MPFR_RNDN)), MPFR_RNDN);
  
*rnorm = mpfr_get_d(rnorm_temp_conj_grad, MPFR_RNDN);
}

static void makea(int n, int nz, double a[], int colidx[], int rowstr[], int firstrow, int lastrow, int firstcol, int lastcol, int arow[], int acol[][11 + 1], double aelt[][11 + 1], int iv[])
{
  int iouter;
  int ivelt;
  int nzv;
  int nn1;
  int ivc[11 + 1];
  double vc[11 + 1];
  nn1 = 1;
  do
  {
    nn1 = 2 * nn1;
  }
  while (nn1 < n);
  for (iouter = 0; iouter < n; iouter++){
  {
    nzv = 11;
    sprnvc(n, nzv, nn1, vc, ivc);
    vecset(n, vc, ivc, &nzv, iouter + 1, 0.5);
    arow[iouter] = nzv;
    for (ivelt = 0; ivelt < nzv; ivelt++){
    {
      acol[iouter][ivelt] = ivc[ivelt] - 1;
      mpfr_set_d(vc_temp_makea, vc[ivelt], MPFR_RNDN);
      


aelt[iouter][ivelt] = mpfr_get_d(vc_temp_makea, MPFR_RNDN);
    }

        }

  }

    }

  sparse(a, colidx, rowstr, n, nz, 11, arow, acol, aelt, firstrow, lastrow, iv, 1.0e-1, 20.0);
}

static void sparse(double a[], int colidx[], int rowstr[], int n, int nz, int nozer, int arow[], int acol[][11 + 1], double aelt[][11 + 1], int firstrow, int lastrow, int nzloc[], double rcond, double shift)
{
  int nrows;
  int i;
  int j;
  int j1;
  int j2;
  int nza;
  int k;
  int kk;
  int nzrow;
  int jcol;
  logical cont40;
  mpfr_set_d(rcond_temp_sparse, rcond, MPFR_RNDN);
  mpfr_set_d(shift_temp_sparse, shift, MPFR_RNDN);
  nrows = (lastrow - firstrow) + 1;
  for (j = 0; j < (nrows + 1); j++){
  {
    rowstr[j] = 0;
  }

    }

  for (i = 0; i < n; i++){
  {
    for (nza = 0; nza < arow[i]; nza++){
    {
      j = acol[i][nza] + 1;
      rowstr[j] = rowstr[j] + arow[i];
    }

        }

  }

    }

  rowstr[0] = 0;
  for (j = 1; j < (nrows + 1); j++){
  {
    rowstr[j] = rowstr[j] + rowstr[j - 1];
  }

    }

  nza = rowstr[nrows] - 1;
  if (nza > nz)
  {
    printf("Space for matrix elements exceeded in sparse\n");
    printf("nza, nzmax = %d, %d\n", nza, nz);
    exit(EXIT_FAILURE);
  }

  for (j = 0; j < nrows; j++){
  {
    for (k = rowstr[j]; k < rowstr[j + 1]; k++){
    {
      mpfr_set_d(a_temp_sparse, 0.0, MPFR_RNDN);
      
a[k] = mpfr_get_d(a_temp_sparse, MPFR_RNDN);
      colidx[k] = -1;
    }

        }

    nzloc[j] = 0;
  }

    }

  mpfr_set_d(size_sparse, 1.0, MPFR_RNDN);
  mpfr_set_d(ratio_sparse, pow(mpfr_get_d(rcond_temp_sparse, MPFR_RNDN), 1.0 / ((double) n)), MPFR_RNDN);
  for (i = 0; i < n; i++){
  {
    for (nza = 0; nza < arow[i]; nza++){
    {
      j = acol[i][nza];
                  mpfr_mul_d(scale_sparse, size_sparse, aelt[i][nza], MPFR_RNDN);
      for (nzrow = 0; nzrow < arow[i]; nzrow++){
      {
        jcol = acol[i][nzrow];
                        mpfr_mul_d(va_sparse, scale_sparse, aelt[i][nzrow], MPFR_RNDN);
        if ((jcol == j) && (j == i))
        {
          
                    mpfr_add(temp_var_33, va_sparse, rcond_temp_sparse, MPFR_RNDN);
                    mpfr_sub(va_sparse, (temp_var_33), shift_temp_sparse, MPFR_RNDN);
        }

        cont40 = false;
        for (k = rowstr[j]; k < rowstr[j + 1]; k++){
        {
          if (colidx[k] > jcol)
          {
            for (kk = rowstr[j + 1] - 2; kk >= k; kk--){
            {
              if (colidx[kk] > (-1))
              {
                mpfr_set_d(a_temp_sparse, a[kk], MPFR_RNDN);
                
a[kk + 1] = mpfr_get_d(a_temp_sparse, MPFR_RNDN);
                colidx[kk + 1] = colidx[kk];
              }

            }

                        }

            colidx[k] = jcol;
            mpfr_set_d(a_temp_sparse, 0.0, MPFR_RNDN);
            
a[k] = mpfr_get_d(a_temp_sparse, MPFR_RNDN);
            cont40 = true;
            break;
          }
          else
            if (colidx[k] == (-1))
          {
            colidx[k] = jcol;
            cont40 = true;
            break;
          }
          else
            if (colidx[k] == jcol)
          {
            nzloc[j] = nzloc[j] + 1;
            cont40 = true;
            break;
          }



        }

                }

        if (cont40 == false)
        {
          printf("internal error in sparse: i=%d\n", i);
          exit(EXIT_FAILURE);
        }

        mpfr_set_d(a_temp_sparse, a[k], MPFR_RNDN);
                        mpfr_add(a_temp_sparse, a_temp_sparse, va_sparse, MPFR_RNDN);
        
a[k] = mpfr_get_d(a_temp_sparse, MPFR_RNDN);
      }

            }

    }

        }

            mpfr_mul(size_sparse, size_sparse, ratio_sparse, MPFR_RNDN);
  }

    }

  for (j = 1; j < nrows; j++){
  {
    nzloc[j] = nzloc[j] + nzloc[j - 1];
  }

    }

  for (j = 0; j < nrows; j++){
  {
    if (j > 0)
    {
      j1 = rowstr[j] - nzloc[j - 1];
    }
    else
    {
      j1 = 0;
    }

    j2 = rowstr[j + 1] - nzloc[j];
    nza = rowstr[j];
    for (k = j1; k < j2; k++){
    {
      mpfr_set_d(a_temp_sparse, a[nza], MPFR_RNDN);
      


a[k] = mpfr_get_d(a_temp_sparse, MPFR_RNDN);
      colidx[k] = colidx[nza];
      nza = nza + 1;
    }

        }

  }

    }

  for (j = 1; j < (nrows + 1); j++){
  {
    rowstr[j] = rowstr[j] - nzloc[j - 1];
  }

    }

  nza = rowstr[nrows] - 1;
}

static void sprnvc(int n, int nz, int nn1, double v[], int iv[])
{
  int nzv;
  int ii;
  int i;
  nzv = 0;
  while (nzv < nz)
  {
    mpfr_set_d(vecelt_sprnvc, randlc(&tran, amult), MPFR_RNDN);
    mpfr_set_d(vecloc_sprnvc, randlc(&tran, amult), MPFR_RNDN);
    i = icnvrt(mpfr_get_d(vecloc_sprnvc, MPFR_RNDN), nn1) + 1;
    if (i > n)
      continue;

    logical was_gen = false;
    for (ii = 0; ii < nzv; ii++){
    {
      if (iv[ii] == i)
      {
        was_gen = true;
        break;
      }

    }

        }

    if (was_gen)
      continue;

    mpfr_set(v_temp_sprnvc, vecelt_sprnvc, MPFR_RNDN);
    
v[nzv] = mpfr_get_d(v_temp_sprnvc, MPFR_RNDN);
    iv[nzv] = i;
    nzv = nzv + 1;
  }

}

static int icnvrt(double x, int ipwr2)
{
  mpfr_set_d(x_temp_icnvrt, x, MPFR_RNDN);
      mpfr_mul_si(temp_var_43, x_temp_icnvrt, ipwr2, MPFR_RNDN);
  int result = (int) (mpfr_get_d(temp_var_43, MPFR_RNDN));
  return result;
}

static void vecset(int n, double v[], int iv[], int *nzv, int i, double val)
{
  int k;
  logical set;
  mpfr_set_d(val_temp_vecset, val, MPFR_RNDN);
  set = false;
  for (k = 0; k < (*nzv); k++){
  {
    if (iv[k] == i)


    {
      mpfr_set(v_temp_vecset, val_temp_vecset, MPFR_RNDN);
      
v[k] = mpfr_get_d(v_temp_vecset, MPFR_RNDN);
      set = true;
    }

  }

    }

  if (set == false)
  {
    mpfr_set(v_temp_vecset, val_temp_vecset, MPFR_RNDN);
    
v[*nzv] = mpfr_get_d(v_temp_vecset, MPFR_RNDN);
    iv[*nzv] = i;
    *nzv = (*nzv) + 1;
  }

}

//end of conversion, hopefully it will work :)
