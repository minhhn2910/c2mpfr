#include <stdio.h>
int main()
{
double t1,t2;
float x1,x2,a1,a2;

//~ 4194306.0 1819863.0 1.0 909932.0
a1 = 4194306.0;
x1 = 1819863.0;
a2 = 1.0;
x2 = 909932.0;
t1 = a1*x1 + a2*x2;
printf ("float no cast %.12f\n",t1);


t1 = a1*(double)x1 + a2*(double)x2;
printf ("cast mul double %.12f\n",t1);

t1 = (double)a1*x1 + (double)a2*x2;
printf ("cast mul double %.12f\n",t1);

t1 = a1*x1 + (double)a2*x2;
printf ("cast 1/2mul double %.12f\n",t1);

t1 = (double)(a1*x1 + a2*x2);
printf ("cast all %.12f\n",t1);

t1 = (double)(a1*x1) + (double)(a2*x2);
printf ("cast mul result %.12f\n",t1);

long x1_l,x2_l,a1_l,a2_l;
a1_l = 4194306;
x1_l = 1819863;
a2_l = 1;
x2_l = 909932;
t1 = a1_l*x1_l + a2_l*x2_l;
printf ("long no cast %.12f\n",t1);

return 0;
}
