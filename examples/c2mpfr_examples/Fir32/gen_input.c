#include <stdio.h>
#include <stdlib.h>
#define MAX_value 10e12  //generate random number
#define INPUT_LEN 32*3
float signal_in_track;
float signal_in[INPUT_LEN];

int main()
{
	int i;
	
	srand48(1);
	
	for (i=0;i<INPUT_LEN;i++)
	{
		signal_in_track = drand48() * MAX_value;
		signal_in[i] = signal_in_track;
		printf("%.5f,",signal_in[i]);
	}
	printf("\n");
	
	return 0;
}
