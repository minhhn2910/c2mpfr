#!/usr/bin/env python
import os
import sys
import time
import subprocess

def parse_output(line):
        list_target = []
        line.replace(" ", "")
        line.replace('\n','')
        #remove unexpected space
        array = line.split(',')
#       print array
        for target in array:
                try:
                        if(len(target)>0 and target!='\n'):
                                list_target.append(float(target))
                except:
                        #print "Failed to parse output string"
                        continue
        return  list_target
        
def run_program(double_program,float_program,seed):
	#get approximate ouput by float type
	output = subprocess.Popen([float_program, '%s'%(seed)], stdout=subprocess.PIPE).communicate()[0]
	floating_result = parse_output(output)
	#get target output
	output = subprocess.Popen([double_program, '%s'%(seed)], stdout=subprocess.PIPE).communicate()[0]
	target_result = parse_output(output)
	return check_output(floating_result,target_result)

def check_output(floating_result,target_result):
#TODO: modify this func to return checksum error. instead of true and false. feed the checsum error to greedy decision func
	if len(floating_result)== 0:
			print 'error: len(result) = 0'
	if len(floating_result) != len(target_result):
		print 'Error : float result has length: %s while double_result has length: %s' %(len(floating_result),len(target_result))
		print floating_result
		return 0.0
	signal_sqr = 0.0
	error_sqr = 0.0	
	for i in range(len(floating_result)):
		signal_sqr += target_result[i]**2
		error_sqr  += (floating_result[i]-target_result[i])**2
		#~ print error_sqr
	sqnr = 0.0
	if error_sqr !=0.0:
		sqnr = signal_sqr/error_sqr
	if sqnr != 0:
		return 1.0/sqnr
	else:
		return 0.0
def main (double_ver, float_ver):
	max_error = 1.55975302683e-12
	max_seed = 358557
	for seed in range(200000,400000):
		current_err = run_program(double_ver, float_ver, seed)
		if current_err > max_error:
			max_error = current_err
			max_seed = seed
			print "update max, current err " + str(current_err) + " current_seed " + str(seed)
	print "max_error " + str(max_error)
	print "worst seed " + str(max_seed)
if __name__ == '__main__':
	arguments = sys.argv[1:]
	if len(arguments)!=2:
		print "usage ./find_worst_conf.py double_ver float_ver"
		exit(0)
	if not ('/' in arguments[0]):
		arguments[0] = './' + arguments[0]
	if not ('/' in arguments[1]):
		arguments[1] = './' + arguments[1]
	main(arguments[0],arguments[1])

