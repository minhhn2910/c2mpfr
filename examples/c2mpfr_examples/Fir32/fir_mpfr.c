#include <stdio.h>
#include <time.h>
//~ #include <math.h>
#include <stdlib.h>
#include <mpfr.h>
#define LEN 8 
#define MAX_value 10e16
#define INPUT_LEN 96
 int config_vals[LEN];
mpfr_t y;
mpfr_t acc;
mpfr_t x_track;
mpfr_t h_track;
mpfr_t signal_in_track;
mpfr_t signal_out_track;

mpfr_t acc_mul;

    mpfr_t temp_var_1;
        mpfr_t temp_var_2;
int init_mpfr() { 
mpfr_init2(y, config_vals[1]);
mpfr_init2(acc, config_vals[2]);
mpfr_init2(x_track, config_vals[3]);
mpfr_init2(h_track, config_vals[4]);
mpfr_init2(signal_in_track, config_vals[5]);
mpfr_init2(signal_out_track, config_vals[6]);
mpfr_init2(acc_mul, config_vals[7]);
    mpfr_init2 (temp_var_1, config_vals[0]);
        mpfr_init2 (temp_var_2, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

float h[32] = {0.2234, -0.844, 0.5823, -0.3342, -0.3418, 0.6263, 0.9566, -0.742, 0.958, 0.184, 0.362, 0.098, -0.892, -0.79, -0.964, 0.69, 0.592, -0.968, -0.757, -0.7628, 0.1752, 0.7516, -0.6757, -0.1762, 0.755, -0.06572, 0.7573, 0.6502, 0.6736, -0.6752, -0.2756, -0.97656};
float x[32];
float signal_in[INPUT_LEN];
float signal_out[INPUT_LEN];
float fir(float input)
{
  int i;
  mpfr_set_d(x_track, input, MPFR_RNDZ);
  
  x[0] = mpfr_get_d(x_track, MPFR_RNDZ);

  //~ mpfr_set_d(acc_mul, x[0] * h[0], MPFR_RNDZ);
  mpfr_set_d(acc, x[0] * h[0], MPFR_RNDZ);
 //~ printf("acc %d = %lf \n", 0, mpfr_get_d(acc,MPFR_RNDZ) );
  for (i = 31; i > 0; i--){
  { 
	//mpfr_set_d(acc_mul, x[i] * h[i], MPFR_RNDZ);
        mpfr_add_d(acc, acc, x[i] * h[i], MPFR_RNDZ); 
       
       		//~ printf("acc %d = %lf \n", i, mpfr_get_d(acc,MPFR_RNDZ) );
        
    mpfr_set_d(x_track, x[i - 1], MPFR_RNDZ);
    
x[i] = mpfr_get_d(x_track, MPFR_RNDZ);
  }

    }


  mpfr_set(y, acc, MPFR_RNDZ);
  return mpfr_get_d(y, MPFR_RNDZ);
}

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
			srand(111);
	}
	else 
	{
		srand(atoi(argv[1]));
	}
	
 init_readconfig();
  int i;
  for (i = 0; i < 32; i++){
    mpfr_set_d(h_track, h[i], MPFR_RNDZ);
	h[i] = mpfr_get_d(h_track,MPFR_RNDZ);
    }

//~ mpfr_set_d(signal_in_track, -0.997497, MPFR_RNDZ);
//~ signal_in[0] = mpfr_get_d(signal_in_track, MPFR_RNDZ);
//~ mpfr_set_d(signal_in_track, 0.127205, MPFR_RNDZ);
//~ signal_in[1] = mpfr_get_d(signal_in_track, MPFR_RNDZ);

	//~ int max_exp = 12; 
//~ double max_rand_value = (double) pow((double)10, (double)max_exp);
//~ 
  //~ srand(1);
  
  for (i = 0; i < INPUT_LEN; i++){
  {
	  double temp = (float)(rand() - RAND_MAX/2)/(RAND_MAX/2), MPFR_RNDZ;
		mpfr_set_d(signal_in_track, temp, MPFR_RNDZ);
		signal_in[i] = mpfr_get_d(signal_in_track, MPFR_RNDZ);
	}
	}
		//~ 
//~ //printf("%.5f,", mpfr_get_d(signal_in_track,));
//~ mpfr_set_d(signal_in_track, (double)rand()/(double)(RAND_MAX) * max_rand_value, MPFR_RNDZ);
//~ signal_in[i] = mpfr_get_d(signal_in_track, MPFR_RNDZ);
    //~ printf("%f,", temp);
  //~ }

    //~ }
 
  //~ printf("\n");    

  for (i = 0; i < INPUT_LEN; i++){
  {
    mpfr_set_d(signal_out_track, fir(signal_in[i]), MPFR_RNDZ);
    
signal_out[i] = mpfr_get_d(signal_out_track, MPFR_RNDZ);
    printf("%f,", signal_out[i]);
  }

    }

  //~ printf("\n");
  return 0;
}

//end of conversion, hopefully it will work :)
