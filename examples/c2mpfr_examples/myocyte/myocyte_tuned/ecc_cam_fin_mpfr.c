#include <mpfr.h>
#define LEN 418 
 int config_vals[LEN];
  mpfr_t initvalu_1_ecc;
  mpfr_t initvalu_2_ecc;
  mpfr_t initvalu_3_ecc;
  mpfr_t initvalu_4_ecc;
  mpfr_t initvalu_5_ecc;
  mpfr_t initvalu_6_ecc;
  mpfr_t initvalu_7_ecc;
  mpfr_t initvalu_8_ecc;
  mpfr_t initvalu_9_ecc;
  mpfr_t initvalu_10_ecc;
  mpfr_t initvalu_11_ecc;
  mpfr_t initvalu_12_ecc;
  mpfr_t initvalu_13_ecc;
  mpfr_t initvalu_14_ecc;
  mpfr_t initvalu_15_ecc;
  mpfr_t initvalu_16_ecc;
  mpfr_t initvalu_17_ecc;
  mpfr_t initvalu_18_ecc;
  mpfr_t initvalu_19_ecc;
  mpfr_t initvalu_20_ecc;
  mpfr_t initvalu_21_ecc;
  mpfr_t initvalu_22_ecc;
  mpfr_t initvalu_23_ecc;
  mpfr_t initvalu_24_ecc;
  mpfr_t initvalu_25_ecc;
  mpfr_t initvalu_26_ecc;
  mpfr_t initvalu_27_ecc;
  mpfr_t initvalu_28_ecc;
  mpfr_t initvalu_29_ecc;
  mpfr_t initvalu_30_ecc;
  mpfr_t initvalu_31_ecc;
  mpfr_t initvalu_32_ecc;
  mpfr_t initvalu_33_ecc;
  mpfr_t initvalu_34_ecc;
  mpfr_t initvalu_35_ecc;
  mpfr_t initvalu_36_ecc;
  mpfr_t initvalu_37_ecc;
  mpfr_t initvalu_38_ecc;
  mpfr_t initvalu_39_ecc;
  mpfr_t initvalu_40_ecc;
  mpfr_t initvalu_41_ecc;
  mpfr_t initvalu_42_ecc;
  mpfr_t initvalu_43_ecc;
  mpfr_t initvalu_44_ecc;
  mpfr_t initvalu_45_ecc;
  mpfr_t initvalu_46_ecc;
  mpfr_t parameter_1_ecc;
  mpfr_t pi_ecc;
  mpfr_t R_ecc;
  mpfr_t Frdy_ecc;
  mpfr_t Temp_ecc;
  mpfr_t FoRT_ecc;
  mpfr_t Cmem_ecc;
  mpfr_t Qpow_ecc;
  mpfr_t cellLength_ecc;
  mpfr_t cellRadius_ecc;
  mpfr_t junctionLength_ecc;
  mpfr_t junctionRadius_ecc;
  mpfr_t distSLcyto_ecc;
  mpfr_t distJuncSL_ecc;
  mpfr_t DcaJuncSL_ecc;
  mpfr_t DcaSLcyto_ecc;
  mpfr_t DnaJuncSL_ecc;
  mpfr_t DnaSLcyto_ecc;
  mpfr_t Vcell_ecc;
  mpfr_t Vmyo_ecc;
  mpfr_t Vsr_ecc;
  mpfr_t Vsl_ecc;
  mpfr_t Vjunc_ecc;
  mpfr_t SAjunc_ecc;
  mpfr_t SAsl_ecc;
  mpfr_t J_ca_juncsl_ecc;
  mpfr_t J_ca_slmyo_ecc;
  mpfr_t J_na_juncsl_ecc;
  mpfr_t J_na_slmyo_ecc;
  mpfr_t Fjunc_ecc;
  mpfr_t Fsl_ecc;
  mpfr_t Fjunc_CaL_ecc;
  mpfr_t Fsl_CaL_ecc;
  mpfr_t Cli_ecc;
  mpfr_t Clo_ecc;
  mpfr_t Ko_ecc;
  mpfr_t Nao_ecc;
  mpfr_t Cao_ecc;
  mpfr_t Mgi_ecc;
  mpfr_t ena_junc_ecc;
  mpfr_t ena_sl_ecc;
  mpfr_t ek_ecc;
  mpfr_t eca_junc_ecc;
  mpfr_t eca_sl_ecc;
  mpfr_t ecl_ecc;
  mpfr_t GNa_ecc;
  mpfr_t GNaB_ecc;
  mpfr_t IbarNaK_ecc;
  mpfr_t KmNaip_ecc;
  mpfr_t KmKo_ecc;
  mpfr_t Q10NaK_ecc;
  mpfr_t Q10KmNai_ecc;
  mpfr_t pNaK_ecc;
  mpfr_t GtoSlow_ecc;
  mpfr_t GtoFast_ecc;
  mpfr_t gkp_ecc;
  mpfr_t GClCa_ecc;
  mpfr_t GClB_ecc;
  mpfr_t KdClCa_ecc;
  mpfr_t pNa_ecc;
  mpfr_t pCa_ecc;
  mpfr_t pK_ecc;
  mpfr_t KmCa_ecc;
  mpfr_t Q10CaL_ecc;
  mpfr_t IbarNCX_ecc;
  mpfr_t KmCai_ecc;
  mpfr_t KmCao_ecc;
  mpfr_t KmNai_ecc;
  mpfr_t KmNao_ecc;
  mpfr_t ksat_ecc;
  mpfr_t nu_ecc;
  mpfr_t Kdact_ecc;
  mpfr_t Q10NCX_ecc;
  mpfr_t IbarSLCaP_ecc;
  mpfr_t KmPCa_ecc;
  mpfr_t GCaB_ecc;
  mpfr_t Q10SLCaP_ecc;
  mpfr_t Q10SRCaP_ecc;
  mpfr_t Vmax_SRCaP_ecc;
  mpfr_t Kmf_ecc;
  mpfr_t Kmr_ecc;
  mpfr_t hillSRCaP_ecc;
  mpfr_t ks_ecc;
  mpfr_t koCa_ecc;
  mpfr_t kom_ecc;
  mpfr_t kiCa_ecc;
  mpfr_t kim_ecc;
  mpfr_t ec50SR_ecc;
  mpfr_t Bmax_Naj_ecc;
  mpfr_t Bmax_Nasl_ecc;
  mpfr_t koff_na_ecc;
  mpfr_t kon_na_ecc;
  mpfr_t Bmax_TnClow_ecc;
  mpfr_t koff_tncl_ecc;
  mpfr_t kon_tncl_ecc;
  mpfr_t Bmax_TnChigh_ecc;
  mpfr_t koff_tnchca_ecc;
  mpfr_t kon_tnchca_ecc;
  mpfr_t koff_tnchmg_ecc;
  mpfr_t kon_tnchmg_ecc;
  mpfr_t Bmax_CaM_ecc;
  mpfr_t koff_cam_ecc;
  mpfr_t kon_cam_ecc;
  mpfr_t Bmax_myosin_ecc;
  mpfr_t koff_myoca_ecc;
  mpfr_t kon_myoca_ecc;
  mpfr_t koff_myomg_ecc;
  mpfr_t kon_myomg_ecc;
  mpfr_t Bmax_SR_ecc;
  mpfr_t koff_sr_ecc;
  mpfr_t kon_sr_ecc;
  mpfr_t Bmax_SLlowsl_ecc;
  mpfr_t Bmax_SLlowj_ecc;
  mpfr_t koff_sll_ecc;
  mpfr_t kon_sll_ecc;
  mpfr_t Bmax_SLhighsl_ecc;
  mpfr_t Bmax_SLhighj_ecc;
  mpfr_t koff_slh_ecc;
  mpfr_t kon_slh_ecc;
  mpfr_t Bmax_Csqn_ecc;
  mpfr_t koff_csqn_ecc;
  mpfr_t kon_csqn_ecc;
  mpfr_t am_ecc;
  mpfr_t bm_ecc;
  mpfr_t ah_ecc;
  mpfr_t bh_ecc;
  mpfr_t aj_ecc;
  mpfr_t bj_ecc;
  mpfr_t I_Na_junc_ecc;
  mpfr_t I_Na_sl_ecc;
  mpfr_t I_Na_ecc;
  mpfr_t I_nabk_junc_ecc;
  mpfr_t I_nabk_sl_ecc;
  mpfr_t I_nabk_ecc;
  mpfr_t sigma_ecc;
  mpfr_t fnak_ecc;
  mpfr_t I_nak_junc_ecc;
  mpfr_t I_nak_sl_ecc;
  mpfr_t I_nak_ecc;
  mpfr_t gkr_ecc;
  mpfr_t xrss_ecc;
  mpfr_t tauxr_ecc;
  mpfr_t rkr_ecc;
  mpfr_t I_kr_ecc;
  mpfr_t pcaks_junc_ecc;
  mpfr_t pcaks_sl_ecc;
  mpfr_t gks_junc_ecc;
  mpfr_t gks_sl_ecc;
  mpfr_t eks_ecc;
  mpfr_t xsss_ecc;
  mpfr_t tauxs_ecc;
  mpfr_t I_ks_junc_ecc;
  mpfr_t I_ks_sl_ecc;
  mpfr_t I_ks_ecc;
  mpfr_t kp_kp_ecc;
  mpfr_t I_kp_junc_ecc;
  mpfr_t I_kp_sl_ecc;
  mpfr_t I_kp_ecc;
  mpfr_t xtoss_ecc;
  mpfr_t ytoss_ecc;
  mpfr_t rtoss_ecc;
  mpfr_t tauxtos_ecc;
  mpfr_t tauytos_ecc;
  mpfr_t taurtos_ecc;
  mpfr_t I_tos_ecc;
  mpfr_t tauxtof_ecc;
  mpfr_t tauytof_ecc;
  mpfr_t I_tof_ecc;
  mpfr_t I_to_ecc;
  mpfr_t aki_ecc;
  mpfr_t bki_ecc;
  mpfr_t kiss_ecc;
  mpfr_t I_ki_ecc;
  mpfr_t I_ClCa_junc_ecc;
  mpfr_t I_ClCa_sl_ecc;
  mpfr_t I_ClCa_ecc;
  mpfr_t I_Clbk_ecc;
  mpfr_t dss_ecc;
  mpfr_t taud_ecc;
  mpfr_t fss_ecc;
  mpfr_t tauf_ecc;
  mpfr_t ibarca_j_ecc;
  mpfr_t ibarca_sl_ecc;
  mpfr_t ibark_ecc;
  mpfr_t ibarna_j_ecc;
  mpfr_t ibarna_sl_ecc;
  mpfr_t I_Ca_junc_ecc;
  mpfr_t I_Ca_sl_ecc;
  mpfr_t I_Ca_ecc;
  mpfr_t I_CaK_ecc;
  mpfr_t I_CaNa_junc_ecc;
  mpfr_t I_CaNa_sl_ecc;
  mpfr_t I_CaNa_ecc;
  mpfr_t I_Catot_ecc;
  mpfr_t Ka_junc_ecc;
  mpfr_t Ka_sl_ecc;
  mpfr_t s1_junc_ecc;
  mpfr_t s1_sl_ecc;
  mpfr_t s2_junc_ecc;
  mpfr_t s3_junc_ecc;
  mpfr_t s2_sl_ecc;
  mpfr_t s3_sl_ecc;
  mpfr_t I_ncx_junc_ecc;
  mpfr_t I_ncx_sl_ecc;
  mpfr_t I_ncx_ecc;
  mpfr_t I_pca_junc_ecc;
  mpfr_t I_pca_sl_ecc;
  mpfr_t I_pca_ecc;
  mpfr_t I_cabk_junc_ecc;
  mpfr_t I_cabk_sl_ecc;
  mpfr_t I_cabk_ecc;
  mpfr_t MaxSR_ecc;
  mpfr_t MinSR_ecc;
  mpfr_t kCaSR_ecc;
  mpfr_t koSRCa_ecc;
  mpfr_t kiSRCa_ecc;
  mpfr_t RI_ecc;
  mpfr_t J_SRCarel_ecc;
  mpfr_t J_serca_ecc;
  mpfr_t J_SRleak_ecc;
  mpfr_t J_CaB_cytosol_ecc;
  mpfr_t J_CaB_junction_ecc;
  mpfr_t J_CaB_sl_ecc;
  mpfr_t oneovervsr_ecc;
  mpfr_t I_Na_tot_junc_ecc;
  mpfr_t I_Na_tot_sl_ecc;
  mpfr_t oneovervsl_ecc;
  mpfr_t I_K_tot_ecc;
  mpfr_t I_Ca_tot_junc_ecc;
  mpfr_t I_Ca_tot_sl_ecc;
  mpfr_t junc_sl_ecc;
  mpfr_t sl_junc_ecc;
  mpfr_t sl_myo_ecc;
  mpfr_t myo_sl_ecc;
  mpfr_t I_app_ecc;
  mpfr_t V_hold_ecc;
  mpfr_t V_test_ecc;
  mpfr_t V_clamp_ecc;
  mpfr_t R_clamp_ecc;
  mpfr_t I_Na_tot_ecc;
  mpfr_t I_Cl_tot_ecc;
  mpfr_t I_Ca_tot_ecc;
  mpfr_t I_tot_ecc;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
    mpfr_t temp_var_11;
    mpfr_t temp_var_12;
    mpfr_t temp_var_13;
    mpfr_t temp_var_14;
    mpfr_t temp_var_15;
    mpfr_t temp_var_16;
    mpfr_t temp_var_17;
    mpfr_t temp_var_18;
    mpfr_t temp_var_19;
    mpfr_t temp_var_20;
    mpfr_t temp_var_21;
    mpfr_t temp_var_22;
    mpfr_t temp_var_23;
    mpfr_t temp_var_24;
    mpfr_t temp_var_25;
    mpfr_t temp_var_26;
    mpfr_t temp_var_27;
    mpfr_t temp_var_28;
    mpfr_t temp_var_29;
    mpfr_t temp_var_30;
    mpfr_t temp_var_31;
    mpfr_t temp_var_32;
    mpfr_t temp_var_33;
    mpfr_t temp_var_34;
    mpfr_t temp_var_35;
    mpfr_t temp_var_36;
    mpfr_t temp_var_37;
    mpfr_t temp_var_38;
    mpfr_t temp_var_39;
    mpfr_t temp_var_40;
    mpfr_t temp_var_41;
    mpfr_t temp_var_42;
    mpfr_t temp_var_43;
    mpfr_t temp_var_44;
    mpfr_t temp_var_45;
    mpfr_t temp_var_46;
    mpfr_t temp_var_47;
    mpfr_t temp_var_48;
    mpfr_t temp_var_49;
    mpfr_t temp_var_50;
    mpfr_t temp_var_51;
    mpfr_t temp_var_52;
    mpfr_t temp_var_53;
    mpfr_t temp_var_54;
    mpfr_t temp_var_55;
    mpfr_t temp_var_56;
    mpfr_t temp_var_57;
    mpfr_t temp_var_58;
    mpfr_t temp_var_59;
    mpfr_t temp_var_60;
    mpfr_t temp_var_61;
    mpfr_t temp_var_62;
    mpfr_t temp_var_63;
    mpfr_t temp_var_64;
    mpfr_t temp_var_65;
    mpfr_t temp_var_66;
    mpfr_t temp_var_67;
    mpfr_t temp_var_68;
    mpfr_t temp_var_69;
    mpfr_t temp_var_70;
    mpfr_t temp_var_71;
    mpfr_t temp_var_72;
    mpfr_t temp_var_73;
    mpfr_t temp_var_74;
    mpfr_t temp_var_75;
    mpfr_t temp_var_76;
    mpfr_t temp_var_77;
    mpfr_t temp_var_78;
    mpfr_t temp_var_79;
    mpfr_t temp_var_80;
        mpfr_t temp_var_81;
        mpfr_t temp_var_82;
        mpfr_t temp_var_83;
        mpfr_t temp_var_84;
        mpfr_t temp_var_85;
        mpfr_t temp_var_86;
        mpfr_t temp_var_87;
        mpfr_t temp_var_88;
        mpfr_t temp_var_89;
        mpfr_t temp_var_90;
        mpfr_t temp_var_91;
        mpfr_t temp_var_92;
        mpfr_t temp_var_93;
        mpfr_t temp_var_94;
        mpfr_t temp_var_95;
        mpfr_t temp_var_96;
        mpfr_t temp_var_97;
        mpfr_t temp_var_98;
        mpfr_t temp_var_99;
        mpfr_t temp_var_100;
        mpfr_t temp_var_101;
        mpfr_t temp_var_102;
        mpfr_t temp_var_103;
        mpfr_t temp_var_104;
        mpfr_t temp_var_105;
    mpfr_t temp_var_106;
    mpfr_t temp_var_107;
    mpfr_t temp_var_108;
    mpfr_t temp_var_109;
    mpfr_t temp_var_110;
    mpfr_t temp_var_111;
    mpfr_t temp_var_112;
    mpfr_t temp_var_113;
    mpfr_t temp_var_114;
    mpfr_t temp_var_115;
    mpfr_t temp_var_116;
    mpfr_t temp_var_117;
    mpfr_t temp_var_118;
    mpfr_t temp_var_119;
    mpfr_t temp_var_120;
    mpfr_t temp_var_121;
    mpfr_t temp_var_122;
    mpfr_t temp_var_123;
    mpfr_t temp_var_124;
    mpfr_t temp_var_125;
    mpfr_t temp_var_126;
    mpfr_t temp_var_127;
    mpfr_t temp_var_128;
    mpfr_t temp_var_129;
    mpfr_t temp_var_130;
    mpfr_t temp_var_131;
    mpfr_t temp_var_132;
    mpfr_t temp_var_133;
    mpfr_t temp_var_134;
    mpfr_t temp_var_135;
    mpfr_t temp_var_136;
    mpfr_t temp_var_137;
    mpfr_t temp_var_138;
    mpfr_t temp_var_139;
    mpfr_t temp_var_140;
    mpfr_t temp_var_141;
    mpfr_t temp_var_142;
    mpfr_t temp_var_143;
    mpfr_t temp_var_144;
    mpfr_t temp_var_145;
    mpfr_t temp_var_146;
    mpfr_t temp_var_147;
    mpfr_t temp_var_148;
    mpfr_t temp_var_149;
    mpfr_t temp_var_150;
    mpfr_t temp_var_151;
    mpfr_t temp_var_152;
    mpfr_t temp_var_153;
    mpfr_t temp_var_154;
    mpfr_t temp_var_155;
    mpfr_t temp_var_156;
    mpfr_t temp_var_157;
    mpfr_t temp_var_158;
    mpfr_t temp_var_159;
    mpfr_t temp_var_160;
    mpfr_t temp_var_161;
    mpfr_t temp_var_162;
    mpfr_t temp_var_163;
    mpfr_t temp_var_164;
    mpfr_t temp_var_165;
    mpfr_t temp_var_166;
    mpfr_t temp_var_167;
    mpfr_t temp_var_168;
    mpfr_t temp_var_169;
    mpfr_t temp_var_170;
    mpfr_t temp_var_171;
    mpfr_t temp_var_172;
    mpfr_t temp_var_173;
    mpfr_t temp_var_174;
    mpfr_t temp_var_175;
    mpfr_t temp_var_176;
    mpfr_t temp_var_177;
    mpfr_t temp_var_178;
    mpfr_t temp_var_179;
    mpfr_t temp_var_180;
    mpfr_t temp_var_181;
    mpfr_t temp_var_182;
    mpfr_t temp_var_183;
    mpfr_t temp_var_184;
    mpfr_t temp_var_185;
    mpfr_t temp_var_186;
    mpfr_t temp_var_187;
    mpfr_t temp_var_188;
    mpfr_t temp_var_189;
    mpfr_t temp_var_190;
    mpfr_t temp_var_191;
    mpfr_t temp_var_192;
    mpfr_t temp_var_193;
    mpfr_t temp_var_194;
    mpfr_t temp_var_195;
    mpfr_t temp_var_196;
    mpfr_t temp_var_197;
    mpfr_t temp_var_198;
    mpfr_t temp_var_199;
    mpfr_t temp_var_200;
    mpfr_t temp_var_201;
    mpfr_t temp_var_202;
    mpfr_t temp_var_203;
    mpfr_t temp_var_204;
    mpfr_t temp_var_205;
    mpfr_t temp_var_206;
    mpfr_t temp_var_207;
    mpfr_t temp_var_208;
    mpfr_t temp_var_209;
    mpfr_t temp_var_210;
    mpfr_t temp_var_211;
    mpfr_t temp_var_212;
    mpfr_t temp_var_213;
    mpfr_t temp_var_214;
    mpfr_t temp_var_215;
    mpfr_t temp_var_216;
    mpfr_t temp_var_217;
    mpfr_t temp_var_218;
    mpfr_t temp_var_219;
    mpfr_t temp_var_220;
    mpfr_t temp_var_221;
    mpfr_t temp_var_222;
    mpfr_t temp_var_223;
    mpfr_t temp_var_224;
    mpfr_t temp_var_225;
    mpfr_t temp_var_226;
    mpfr_t temp_var_227;
    mpfr_t temp_var_228;
    mpfr_t temp_var_229;
    mpfr_t temp_var_230;
    mpfr_t temp_var_231;
    mpfr_t temp_var_232;
    mpfr_t temp_var_233;
    mpfr_t temp_var_234;
    mpfr_t temp_var_235;
    mpfr_t temp_var_236;
    mpfr_t temp_var_237;
    mpfr_t temp_var_238;
    mpfr_t temp_var_239;
    mpfr_t temp_var_240;
    mpfr_t temp_var_241;
    mpfr_t temp_var_242;
    mpfr_t temp_var_243;
    mpfr_t temp_var_244;
    mpfr_t temp_var_245;
    mpfr_t temp_var_246;
    mpfr_t temp_var_247;
    mpfr_t temp_var_248;
    mpfr_t temp_var_249;
    mpfr_t temp_var_250;
    mpfr_t temp_var_251;
    mpfr_t temp_var_252;
    mpfr_t temp_var_253;
    mpfr_t temp_var_254;
    mpfr_t temp_var_255;
    mpfr_t temp_var_256;
    mpfr_t temp_var_257;
    mpfr_t temp_var_258;
    mpfr_t temp_var_259;
    mpfr_t temp_var_260;
    mpfr_t temp_var_261;
    mpfr_t temp_var_262;
    mpfr_t temp_var_263;
    mpfr_t temp_var_264;
    mpfr_t temp_var_265;
    mpfr_t temp_var_266;
    mpfr_t temp_var_267;
    mpfr_t temp_var_268;
    mpfr_t temp_var_269;
    mpfr_t temp_var_270;
    mpfr_t temp_var_271;
    mpfr_t temp_var_272;
    mpfr_t temp_var_273;
    mpfr_t temp_var_274;
    mpfr_t temp_var_275;
    mpfr_t temp_var_276;
    mpfr_t temp_var_277;
    mpfr_t temp_var_278;
    mpfr_t temp_var_279;
    mpfr_t temp_var_280;
    mpfr_t temp_var_281;
    mpfr_t temp_var_282;
    mpfr_t temp_var_283;
    mpfr_t temp_var_284;
    mpfr_t temp_var_285;
    mpfr_t temp_var_286;
    mpfr_t temp_var_287;
    mpfr_t temp_var_288;
    mpfr_t temp_var_289;
    mpfr_t temp_var_290;
    mpfr_t temp_var_291;
    mpfr_t temp_var_292;
    mpfr_t temp_var_293;
    mpfr_t temp_var_294;
    mpfr_t temp_var_295;
    mpfr_t temp_var_296;
    mpfr_t temp_var_297;
    mpfr_t temp_var_298;
    mpfr_t temp_var_299;
    mpfr_t temp_var_300;
    mpfr_t temp_var_301;
    mpfr_t temp_var_302;
    mpfr_t temp_var_303;
    mpfr_t temp_var_304;
    mpfr_t temp_var_305;
    mpfr_t temp_var_306;
    mpfr_t temp_var_307;
    mpfr_t temp_var_308;
    mpfr_t temp_var_309;
    mpfr_t temp_var_310;
    mpfr_t temp_var_311;
    mpfr_t temp_var_312;
    mpfr_t temp_var_313;
    mpfr_t temp_var_314;
    mpfr_t temp_var_315;
    mpfr_t temp_var_316;
    mpfr_t temp_var_317;
    mpfr_t temp_var_318;
    mpfr_t temp_var_319;
    mpfr_t temp_var_320;
    mpfr_t temp_var_321;
    mpfr_t temp_var_322;
    mpfr_t temp_var_323;
    mpfr_t temp_var_324;
    mpfr_t temp_var_325;
    mpfr_t temp_var_326;
    mpfr_t temp_var_327;
    mpfr_t temp_var_328;
    mpfr_t temp_var_329;
    mpfr_t temp_var_330;
    mpfr_t temp_var_331;
    mpfr_t temp_var_332;
    mpfr_t temp_var_333;
    mpfr_t temp_var_334;
    mpfr_t temp_var_335;
    mpfr_t temp_var_336;
    mpfr_t temp_var_337;
    mpfr_t temp_var_338;
    mpfr_t temp_var_339;
    mpfr_t temp_var_340;
    mpfr_t temp_var_341;
    mpfr_t temp_var_342;
    mpfr_t temp_var_343;
    mpfr_t temp_var_344;
    mpfr_t temp_var_345;
    mpfr_t temp_var_346;
    mpfr_t temp_var_347;
    mpfr_t temp_var_348;
    mpfr_t temp_var_349;
    mpfr_t temp_var_350;
    mpfr_t temp_var_351;
    mpfr_t temp_var_352;
    mpfr_t temp_var_353;
    mpfr_t temp_var_354;
    mpfr_t temp_var_355;
    mpfr_t temp_var_356;
    mpfr_t temp_var_357;
    mpfr_t temp_var_358;
    mpfr_t temp_var_359;
    mpfr_t temp_var_360;
    mpfr_t temp_var_361;
    mpfr_t temp_var_362;
    mpfr_t temp_var_363;
    mpfr_t temp_var_364;
    mpfr_t temp_var_365;
    mpfr_t temp_var_366;
    mpfr_t temp_var_367;
    mpfr_t temp_var_368;
    mpfr_t temp_var_369;
    mpfr_t temp_var_370;
    mpfr_t temp_var_371;
    mpfr_t temp_var_372;
    mpfr_t temp_var_373;
    mpfr_t temp_var_374;
    mpfr_t temp_var_375;
    mpfr_t temp_var_376;
    mpfr_t temp_var_377;
    mpfr_t temp_var_378;
    mpfr_t temp_var_379;
    mpfr_t temp_var_380;
    mpfr_t temp_var_381;
    mpfr_t temp_var_382;
    mpfr_t temp_var_383;
    mpfr_t temp_var_384;
    mpfr_t temp_var_385;
    mpfr_t temp_var_386;
    mpfr_t temp_var_387;
    mpfr_t temp_var_388;
    mpfr_t temp_var_389;
    mpfr_t temp_var_390;
    mpfr_t temp_var_391;
    mpfr_t temp_var_392;
    mpfr_t temp_var_393;
    mpfr_t temp_var_394;
    mpfr_t temp_var_395;
    mpfr_t temp_var_396;
    mpfr_t temp_var_397;
    mpfr_t temp_var_398;
    mpfr_t temp_var_399;
    mpfr_t temp_var_400;
    mpfr_t temp_var_401;
    mpfr_t temp_var_402;
    mpfr_t temp_var_403;
    mpfr_t temp_var_404;
    mpfr_t temp_var_405;
    mpfr_t temp_var_406;
    mpfr_t temp_var_407;
    mpfr_t temp_var_408;
    mpfr_t temp_var_409;
    mpfr_t temp_var_410;
    mpfr_t temp_var_411;
    mpfr_t temp_var_412;
    mpfr_t temp_var_413;
    mpfr_t temp_var_414;
    mpfr_t temp_var_415;
    mpfr_t temp_var_416;
    mpfr_t temp_var_417;
    mpfr_t temp_var_418;
    mpfr_t temp_var_419;
    mpfr_t temp_var_420;
    mpfr_t temp_var_421;
    mpfr_t temp_var_422;
    mpfr_t temp_var_423;
    mpfr_t temp_var_424;
    mpfr_t temp_var_425;
    mpfr_t temp_var_426;
    mpfr_t temp_var_427;
    mpfr_t temp_var_428;
    mpfr_t temp_var_429;
    mpfr_t temp_var_430;
    mpfr_t temp_var_431;
    mpfr_t temp_var_432;
    mpfr_t temp_var_433;
    mpfr_t temp_var_434;
    mpfr_t temp_var_435;
    mpfr_t temp_var_436;
    mpfr_t temp_var_437;
    mpfr_t temp_var_438;
    mpfr_t temp_var_439;
    mpfr_t temp_var_440;
    mpfr_t temp_var_441;
    mpfr_t temp_var_442;
    mpfr_t temp_var_443;
    mpfr_t temp_var_444;
    mpfr_t temp_var_445;
    mpfr_t temp_var_446;
    mpfr_t temp_var_447;
    mpfr_t temp_var_448;
    mpfr_t temp_var_449;
    mpfr_t temp_var_450;
    mpfr_t temp_var_451;
    mpfr_t temp_var_452;
    mpfr_t temp_var_453;
    mpfr_t temp_var_454;
    mpfr_t temp_var_455;
    mpfr_t temp_var_456;
    mpfr_t temp_var_457;
    mpfr_t temp_var_458;
    mpfr_t temp_var_459;
    mpfr_t temp_var_460;
    mpfr_t temp_var_461;
    mpfr_t temp_var_462;
    mpfr_t temp_var_463;
    mpfr_t temp_var_464;
    mpfr_t temp_var_465;
    mpfr_t temp_var_466;
    mpfr_t temp_var_467;
    mpfr_t temp_var_468;
    mpfr_t temp_var_469;
    mpfr_t temp_var_470;
    mpfr_t temp_var_471;
    mpfr_t temp_var_472;
    mpfr_t temp_var_473;
    mpfr_t temp_var_474;
    mpfr_t temp_var_475;
    mpfr_t temp_var_476;
    mpfr_t temp_var_477;
    mpfr_t temp_var_478;
    mpfr_t temp_var_479;
    mpfr_t temp_var_480;
    mpfr_t temp_var_481;
    mpfr_t temp_var_482;
    mpfr_t temp_var_483;
    mpfr_t temp_var_484;
    mpfr_t temp_var_485;
    mpfr_t temp_var_486;
    mpfr_t temp_var_487;
    mpfr_t temp_var_488;
    mpfr_t temp_var_489;
    mpfr_t temp_var_490;
    mpfr_t temp_var_491;
    mpfr_t temp_var_492;
    mpfr_t temp_var_493;
    mpfr_t temp_var_494;
    mpfr_t temp_var_495;
    mpfr_t temp_var_496;
    mpfr_t temp_var_497;
    mpfr_t temp_var_498;
    mpfr_t temp_var_499;
    mpfr_t temp_var_500;
    mpfr_t temp_var_501;
    mpfr_t temp_var_502;
    mpfr_t temp_var_503;
    mpfr_t temp_var_504;
    mpfr_t temp_var_505;
    mpfr_t temp_var_506;
    mpfr_t temp_var_507;
    mpfr_t temp_var_508;
    mpfr_t temp_var_509;
    mpfr_t temp_var_510;
    mpfr_t temp_var_511;
    mpfr_t temp_var_512;
    mpfr_t temp_var_513;
    mpfr_t temp_var_514;
    mpfr_t temp_var_515;
    mpfr_t temp_var_516;
    mpfr_t temp_var_517;
    mpfr_t temp_var_518;
    mpfr_t temp_var_519;
    mpfr_t temp_var_520;
    mpfr_t temp_var_521;
    mpfr_t temp_var_522;
    mpfr_t temp_var_523;
    mpfr_t temp_var_524;
    mpfr_t temp_var_525;
    mpfr_t temp_var_526;
    mpfr_t temp_var_527;
    mpfr_t temp_var_528;
    mpfr_t temp_var_529;
    mpfr_t temp_var_530;
    mpfr_t temp_var_531;
    mpfr_t temp_var_532;
    mpfr_t temp_var_533;
    mpfr_t temp_var_534;
    mpfr_t temp_var_535;
    mpfr_t temp_var_536;
    mpfr_t temp_var_537;
    mpfr_t temp_var_538;
    mpfr_t temp_var_539;
    mpfr_t temp_var_540;
    mpfr_t temp_var_541;
    mpfr_t temp_var_542;
    mpfr_t temp_var_543;
    mpfr_t temp_var_544;
    mpfr_t temp_var_545;
    mpfr_t temp_var_546;
    mpfr_t temp_var_547;
    mpfr_t temp_var_548;
    mpfr_t temp_var_549;
    mpfr_t temp_var_550;
    mpfr_t temp_var_551;
    mpfr_t temp_var_552;
    mpfr_t temp_var_553;
    mpfr_t temp_var_554;
    mpfr_t temp_var_555;
    mpfr_t temp_var_556;
    mpfr_t temp_var_557;
    mpfr_t temp_var_558;
    mpfr_t temp_var_559;
    mpfr_t temp_var_560;
    mpfr_t temp_var_561;
    mpfr_t temp_var_562;
    mpfr_t temp_var_563;
    mpfr_t temp_var_564;
    mpfr_t temp_var_565;
    mpfr_t temp_var_566;
    mpfr_t temp_var_567;
    mpfr_t temp_var_568;
    mpfr_t temp_var_569;
    mpfr_t temp_var_570;
    mpfr_t temp_var_571;
    mpfr_t temp_var_572;
    mpfr_t temp_var_573;
    mpfr_t temp_var_574;
    mpfr_t temp_var_575;
    mpfr_t temp_var_576;
    mpfr_t temp_var_577;
    mpfr_t temp_var_578;
    mpfr_t temp_var_579;
    mpfr_t temp_var_580;
    mpfr_t temp_var_581;
    mpfr_t temp_var_582;
    mpfr_t temp_var_583;
    mpfr_t temp_var_584;
    mpfr_t temp_var_585;
    mpfr_t temp_var_586;
    mpfr_t temp_var_587;
    mpfr_t temp_var_588;
    mpfr_t temp_var_589;
    mpfr_t temp_var_590;
    mpfr_t temp_var_591;
    mpfr_t temp_var_592;
    mpfr_t temp_var_593;
    mpfr_t temp_var_594;
    mpfr_t temp_var_595;
    mpfr_t temp_var_596;
    mpfr_t temp_var_597;
    mpfr_t temp_var_598;
    mpfr_t temp_var_599;
    mpfr_t temp_var_600;
    mpfr_t temp_var_601;
    mpfr_t temp_var_602;
    mpfr_t temp_var_603;
    mpfr_t temp_var_604;
    mpfr_t temp_var_605;
    mpfr_t temp_var_606;
    mpfr_t temp_var_607;
    mpfr_t temp_var_608;
    mpfr_t temp_var_609;
    mpfr_t temp_var_610;
    mpfr_t temp_var_611;
    mpfr_t temp_var_612;
    mpfr_t temp_var_613;
    mpfr_t temp_var_614;
    mpfr_t temp_var_615;
    mpfr_t temp_var_616;
    mpfr_t temp_var_617;
    mpfr_t temp_var_618;
    mpfr_t temp_var_619;
    mpfr_t temp_var_620;
    mpfr_t temp_var_621;
    mpfr_t temp_var_622;
    mpfr_t temp_var_623;
    mpfr_t temp_var_624;
    mpfr_t temp_var_625;
    mpfr_t temp_var_626;
    mpfr_t temp_var_627;
    mpfr_t temp_var_628;
    mpfr_t temp_var_629;
    mpfr_t temp_var_630;
    mpfr_t temp_var_631;
    mpfr_t temp_var_632;
    mpfr_t temp_var_633;
    mpfr_t temp_var_634;
    mpfr_t temp_var_635;
    mpfr_t temp_var_636;
    mpfr_t temp_var_637;
    mpfr_t temp_var_638;
    mpfr_t temp_var_639;
    mpfr_t temp_var_640;
    mpfr_t temp_var_641;
    mpfr_t temp_var_642;
    mpfr_t temp_var_643;
    mpfr_t temp_var_644;
    mpfr_t temp_var_645;
    mpfr_t temp_var_646;
    mpfr_t temp_var_647;
    mpfr_t temp_var_648;
    mpfr_t temp_var_649;
    mpfr_t temp_var_650;
    mpfr_t temp_var_651;
    mpfr_t temp_var_652;
    mpfr_t temp_var_653;
    mpfr_t temp_var_654;
    mpfr_t temp_var_655;
    mpfr_t temp_var_656;
    mpfr_t temp_var_657;
    mpfr_t temp_var_658;
    mpfr_t temp_var_659;
    mpfr_t temp_var_660;
    mpfr_t temp_var_661;
    mpfr_t temp_var_662;
    mpfr_t temp_var_663;
    mpfr_t temp_var_664;
    mpfr_t temp_var_665;
    mpfr_t temp_var_666;
    mpfr_t temp_var_667;
    mpfr_t temp_var_668;
    mpfr_t temp_var_669;
    mpfr_t temp_var_670;
    mpfr_t temp_var_671;
    mpfr_t temp_var_672;
    mpfr_t temp_var_673;
    mpfr_t temp_var_674;
    mpfr_t temp_var_675;
    mpfr_t temp_var_676;
    mpfr_t temp_var_677;
    mpfr_t temp_var_678;
    mpfr_t temp_var_679;
    mpfr_t temp_var_680;
    mpfr_t temp_var_681;
    mpfr_t temp_var_682;
    mpfr_t temp_var_683;
    mpfr_t temp_var_684;
    mpfr_t temp_var_685;
    mpfr_t temp_var_686;
    mpfr_t temp_var_687;
    mpfr_t temp_var_688;
    mpfr_t temp_var_689;
    mpfr_t temp_var_690;
    mpfr_t temp_var_691;
    mpfr_t temp_var_692;
    mpfr_t temp_var_693;
    mpfr_t temp_var_694;
    mpfr_t temp_var_695;
    mpfr_t temp_var_696;
    mpfr_t temp_var_697;
    mpfr_t temp_var_698;
    mpfr_t temp_var_699;
    mpfr_t temp_var_700;
    mpfr_t temp_var_701;
    mpfr_t temp_var_702;
    mpfr_t temp_var_703;
    mpfr_t temp_var_704;
    mpfr_t temp_var_705;
    mpfr_t temp_var_706;
    mpfr_t temp_var_707;
    mpfr_t temp_var_708;
    mpfr_t temp_var_709;
    mpfr_t temp_var_710;
    mpfr_t temp_var_711;
    mpfr_t temp_var_712;
    mpfr_t temp_var_713;
    mpfr_t temp_var_714;
    mpfr_t temp_var_715;
    mpfr_t temp_var_716;
    mpfr_t temp_var_717;
    mpfr_t temp_var_718;
    mpfr_t temp_var_719;
    mpfr_t temp_var_720;
    mpfr_t temp_var_721;
        mpfr_t temp_var_722;
    mpfr_t temp_var_723;
    mpfr_t temp_var_724;
    
  mpfr_t JCa_cam;
  mpfr_t CaM_cam;
  mpfr_t Ca2CaM_cam;
  mpfr_t Ca4CaM_cam;
  mpfr_t CaMB_cam;
  mpfr_t Ca2CaMB_cam;
  mpfr_t Ca4CaMB_cam;
  mpfr_t Pb2_cam;
  mpfr_t Pb_cam;
  mpfr_t Pt_cam;
  mpfr_t Pt2_cam;
  mpfr_t Pa_cam;
  mpfr_t Ca4CaN_cam;
  mpfr_t CaMCa4CaN_cam;
  mpfr_t Ca2CaMCa4CaN_cam;
  mpfr_t Ca4CaMCa4CaN_cam;
  mpfr_t CaMtot_cam;
  mpfr_t Btot_cam;
  mpfr_t CaMKIItot_cam;
  mpfr_t CaNtot_cam;
  mpfr_t PP1tot_cam;
  mpfr_t K_cam;
  mpfr_t Mg_cam;
  mpfr_t Kd02_cam;
  mpfr_t Kd24_cam;
  mpfr_t k20_cam;
  mpfr_t k02_cam;
  mpfr_t k42_cam;
  mpfr_t k24_cam;
  mpfr_t k0Boff_cam;
  mpfr_t k0Bon_cam;
  mpfr_t k2Boff_cam;
  mpfr_t k2Bon_cam;
  mpfr_t k4Boff_cam;
  mpfr_t k4Bon_cam;
  mpfr_t k20B_cam;
  mpfr_t k02B_cam;
  mpfr_t k42B_cam;
  mpfr_t k24B_cam;
  mpfr_t kbi_cam;
  mpfr_t kib_cam;
  mpfr_t kpp1_cam;
  mpfr_t Kmpp1_cam;
  mpfr_t kib2_cam;
  mpfr_t kb2i_cam;
  mpfr_t kb24_cam;
  mpfr_t kb42_cam;
  mpfr_t kta_cam;
  mpfr_t kat_cam;
  mpfr_t kt42_cam;
  mpfr_t kt24_cam;
  mpfr_t kat2_cam;
  mpfr_t kt2a_cam;
  mpfr_t kcanCaoff_cam;
  mpfr_t kcanCaon_cam;
  mpfr_t kcanCaM4on_cam;
  mpfr_t kcanCaM4off_cam;
  mpfr_t kcanCaM2on_cam;
  mpfr_t kcanCaM2off_cam;
  mpfr_t kcanCaM0on_cam;
  mpfr_t kcanCaM0off_cam;
  mpfr_t k02can_cam;
  mpfr_t k20can_cam;
  mpfr_t k24can_cam;
  mpfr_t k42can_cam;
  mpfr_t rcn02_cam;
  mpfr_t rcn24_cam;
  mpfr_t B_cam;
  mpfr_t rcn02B_cam;
  mpfr_t rcn24B_cam;
  mpfr_t rcn0B_cam;
  mpfr_t rcn2B_cam;
  mpfr_t rcn4B_cam;
  mpfr_t Ca2CaN_cam;
  mpfr_t rcnCa4CaN_cam;
  mpfr_t rcn02CaN_cam;
  mpfr_t rcn24CaN_cam;
  mpfr_t rcn0CaN_cam;
  mpfr_t rcn2CaN_cam;
  mpfr_t rcn4CaN_cam;
  mpfr_t Pix_cam;
  mpfr_t rcnCKib2_cam;
  mpfr_t rcnCKb2b_cam;
  mpfr_t rcnCKib_cam;
  mpfr_t T_cam;
  mpfr_t kbt_cam;
  mpfr_t rcnCKbt_cam;
  mpfr_t rcnCKtt2_cam;
  mpfr_t rcnCKta_cam;
  mpfr_t rcnCKt2a_cam;
  mpfr_t rcnCKt2b2_cam;
  mpfr_t rcnCKai_cam;
  mpfr_t dCaM_cam;
  mpfr_t dCa2CaM_cam;
  mpfr_t dCa4CaM_cam;
  mpfr_t dCaMB_cam;
  mpfr_t dCa2CaMB_cam;
  mpfr_t dCa4CaMB_cam;
  mpfr_t dPb2_cam;
  mpfr_t dPb_cam;
  mpfr_t dPt_cam;
  mpfr_t dPt2_cam;
  mpfr_t dPa_cam;
  mpfr_t dCa4CaN_cam;
  mpfr_t dCaMCa4CaN_cam;
  mpfr_t dCa2CaMCa4CaN_cam;
  mpfr_t dCa4CaMCa4CaN_cam;
    mpfr_t temp_var_725;
    mpfr_t temp_var_726;
    mpfr_t temp_var_727;
    mpfr_t temp_var_728;
    mpfr_t temp_var_729;
    mpfr_t temp_var_730;
    mpfr_t temp_var_731;
    mpfr_t temp_var_732;
    mpfr_t temp_var_733;
    mpfr_t temp_var_734;
    mpfr_t temp_var_735;
    mpfr_t temp_var_736;
    mpfr_t temp_var_737;
    mpfr_t temp_var_738;
    mpfr_t temp_var_739;
    mpfr_t temp_var_740;
    mpfr_t temp_var_741;
    mpfr_t temp_var_742;
    mpfr_t temp_var_743;
        mpfr_t temp_var_744;
        mpfr_t temp_var_745;
        mpfr_t temp_var_746;
        mpfr_t temp_var_747;
        mpfr_t temp_var_748;
        mpfr_t temp_var_749;
        mpfr_t temp_var_750;
        mpfr_t temp_var_751;
        mpfr_t temp_var_752;
        mpfr_t temp_var_753;
        mpfr_t temp_var_754;
        mpfr_t temp_var_755;
        mpfr_t temp_var_756;
        mpfr_t temp_var_757;
        mpfr_t temp_var_758;
        mpfr_t temp_var_759;
        mpfr_t temp_var_760;
        mpfr_t temp_var_761;
        mpfr_t temp_var_762;
        mpfr_t temp_var_763;
        mpfr_t temp_var_764;
        mpfr_t temp_var_765;
        mpfr_t temp_var_766;
        mpfr_t temp_var_767;
        mpfr_t temp_var_768;
        mpfr_t temp_var_769;
        mpfr_t temp_var_770;
        mpfr_t temp_var_771;
        mpfr_t temp_var_772;
        mpfr_t temp_var_773;
        mpfr_t temp_var_774;
        mpfr_t temp_var_775;
        mpfr_t temp_var_776;
        mpfr_t temp_var_777;
        mpfr_t temp_var_778;
        mpfr_t temp_var_779;
        mpfr_t temp_var_780;
        mpfr_t temp_var_781;
        mpfr_t temp_var_782;
        mpfr_t temp_var_783;
        mpfr_t temp_var_784;
        mpfr_t temp_var_785;
        mpfr_t temp_var_786;
        mpfr_t temp_var_787;
    mpfr_t temp_var_788;
    mpfr_t temp_var_789;
    mpfr_t temp_var_790;
    mpfr_t temp_var_791;
    mpfr_t temp_var_792;
    mpfr_t temp_var_793;
    mpfr_t temp_var_794;
    mpfr_t temp_var_795;
    mpfr_t temp_var_796;
    mpfr_t temp_var_797;
    mpfr_t temp_var_798;
    mpfr_t temp_var_799;
    mpfr_t temp_var_800;
    mpfr_t temp_var_801;
    mpfr_t temp_var_802;
    mpfr_t temp_var_803;
    mpfr_t temp_var_804;
    mpfr_t temp_var_805;
    mpfr_t temp_var_806;
    mpfr_t temp_var_807;
    mpfr_t temp_var_808;
    mpfr_t temp_var_809;
    mpfr_t temp_var_810;
    mpfr_t temp_var_811;
    mpfr_t temp_var_812;
    mpfr_t temp_var_813;
    mpfr_t temp_var_814;
    mpfr_t temp_var_815;
    mpfr_t temp_var_816;
    mpfr_t temp_var_817;
    mpfr_t temp_var_818;
    mpfr_t temp_var_819;
    mpfr_t temp_var_820;
    mpfr_t temp_var_821;
    mpfr_t temp_var_822;
    mpfr_t temp_var_823;
    mpfr_t temp_var_824;
    mpfr_t temp_var_825;
    mpfr_t temp_var_826;
    mpfr_t temp_var_827;
    mpfr_t temp_var_828;
    mpfr_t temp_var_829;
    mpfr_t temp_var_830;
    mpfr_t temp_var_831;
    mpfr_t temp_var_832;
    mpfr_t temp_var_833;
    mpfr_t temp_var_834;
    mpfr_t temp_var_835;
    mpfr_t temp_var_836;
    mpfr_t temp_var_837;
    mpfr_t temp_var_838;
    mpfr_t temp_var_839;
    mpfr_t temp_var_840;
    mpfr_t temp_var_841;
    mpfr_t temp_var_842;
    mpfr_t temp_var_843;
    mpfr_t temp_var_844;
    mpfr_t temp_var_845;
    mpfr_t temp_var_846;
    mpfr_t temp_var_847;
    mpfr_t temp_var_848;
    mpfr_t temp_var_849;
    mpfr_t temp_var_850;
    mpfr_t temp_var_851;
    mpfr_t temp_var_852;
    mpfr_t temp_var_853;
    mpfr_t temp_var_854;
    mpfr_t temp_var_855;
    mpfr_t temp_var_856;
    mpfr_t temp_var_857;
    mpfr_t temp_var_858;
    mpfr_t temp_var_859;
    mpfr_t temp_var_860;
    mpfr_t temp_var_861;
    mpfr_t temp_var_862;
    mpfr_t temp_var_863;
    mpfr_t temp_var_864;
    mpfr_t temp_var_865;
    mpfr_t temp_var_866;
    mpfr_t temp_var_867;
    mpfr_t temp_var_868;
    mpfr_t temp_var_869;
    mpfr_t temp_var_870;
    mpfr_t temp_var_871;
    mpfr_t temp_var_872;
    mpfr_t temp_var_873;
    mpfr_t temp_var_874;
    mpfr_t temp_var_875;
    mpfr_t temp_var_876;
    mpfr_t temp_var_877;
    mpfr_t temp_var_878;
    mpfr_t temp_var_879;
    mpfr_t temp_var_880;
    mpfr_t temp_var_881;
    mpfr_t temp_var_882;
    mpfr_t temp_var_883;
    mpfr_t temp_var_884;
    mpfr_t temp_var_885;
    mpfr_t temp_var_886;
    mpfr_t temp_var_887;
    mpfr_t temp_var_888;
    mpfr_t temp_var_889;
    mpfr_t temp_var_890;
    mpfr_t temp_var_891;
    mpfr_t temp_var_892;
    mpfr_t temp_var_893;
    mpfr_t temp_var_894;
    mpfr_t temp_var_895;
    mpfr_t temp_var_896;
    mpfr_t temp_var_897;
    mpfr_t temp_var_898;
    mpfr_t temp_var_899;
    mpfr_t temp_var_900;
    mpfr_t temp_var_901;
    mpfr_t temp_var_902;
    mpfr_t temp_var_903;
    mpfr_t temp_var_904;
    mpfr_t temp_var_905;
    mpfr_t temp_var_906;
    mpfr_t temp_var_907;
    mpfr_t temp_var_908;
    mpfr_t temp_var_909;
    mpfr_t temp_var_910;
    mpfr_t temp_var_911;
    mpfr_t temp_var_912;
    mpfr_t temp_var_913;
    mpfr_t temp_var_914;
    mpfr_t temp_var_915;
    mpfr_t temp_var_916;
    mpfr_t temp_var_917;
    mpfr_t temp_var_918;
    mpfr_t temp_var_919;
    mpfr_t temp_var_920;
    mpfr_t temp_var_921;
    mpfr_t temp_var_922;
    mpfr_t temp_var_923;
  mpfr_t BtotDyad_fin;
  mpfr_t CaMKIItotDyad_fin;
  mpfr_t Vmyo_fin;
  mpfr_t Vdyad_fin;
  mpfr_t VSL_fin;
  mpfr_t kDyadSL_fin;
  mpfr_t kSLmyo_fin;
  mpfr_t k0Boff_fin;
  mpfr_t k0Bon_fin;
  mpfr_t k2Boff_fin;
  mpfr_t k2Bon_fin;
  mpfr_t k4Boff_fin;
  mpfr_t k4Bon_fin;
  mpfr_t CaMtotDyad_fin;
  mpfr_t Bdyad_fin;
  mpfr_t J_cam_dyadSL_fin;
  mpfr_t J_ca2cam_dyadSL_fin;
  mpfr_t J_ca4cam_dyadSL_fin;
  mpfr_t J_cam_SLmyo_fin;
  mpfr_t J_ca2cam_SLmyo_fin;
  mpfr_t J_ca4cam_SLmyo_fin;
    mpfr_t temp_var_924;
    mpfr_t temp_var_925;
    mpfr_t temp_var_926;
    mpfr_t temp_var_927;
    mpfr_t temp_var_928;
    mpfr_t temp_var_929;
    mpfr_t temp_var_930;
    mpfr_t temp_var_931;
    mpfr_t temp_var_932;
    mpfr_t temp_var_933;
    mpfr_t temp_var_934;
    mpfr_t temp_var_935;
    mpfr_t temp_var_936;
    mpfr_t temp_var_937;
    mpfr_t temp_var_938;
    mpfr_t temp_var_939;
    mpfr_t temp_var_940;
    mpfr_t temp_var_941;
    mpfr_t temp_var_942;
    mpfr_t temp_var_943;
    mpfr_t temp_var_944;
    mpfr_t temp_var_945;
    mpfr_t temp_var_946;
    mpfr_t temp_var_947;
    mpfr_t temp_var_948;
    mpfr_t temp_var_949;
    mpfr_t temp_var_950;
    mpfr_t temp_var_951;
    mpfr_t temp_var_952;
    mpfr_t temp_var_953;
    mpfr_t temp_var_954;
    mpfr_t temp_var_955;
    mpfr_t temp_var_956;
    mpfr_t temp_var_957;
    mpfr_t temp_var_958;
    mpfr_t temp_var_959;
    mpfr_t temp_var_960;
    mpfr_t temp_var_961;
    mpfr_t temp_var_962;
    mpfr_t temp_var_963;
    mpfr_t temp_var_964;
    mpfr_t temp_var_965;
    mpfr_t temp_var_966;
    mpfr_t temp_var_967;
    mpfr_t temp_var_968;
    mpfr_t temp_var_969;
    mpfr_t temp_var_970;
    mpfr_t temp_var_971;
    mpfr_t temp_var_972;
    mpfr_t temp_var_973;
    mpfr_t temp_var_974;
    mpfr_t temp_var_975;
    mpfr_t temp_var_976;
    mpfr_t temp_var_977;
    mpfr_t temp_var_978;
    mpfr_t temp_var_979;
    mpfr_t temp_var_980;
    mpfr_t temp_var_981;
    mpfr_t temp_var_982;
    mpfr_t temp_var_983;
    mpfr_t temp_var_984;
    mpfr_t temp_var_985;
    mpfr_t temp_var_986;
    mpfr_t temp_var_987;
    mpfr_t temp_var_988;
    mpfr_t temp_var_989;
    mpfr_t temp_var_990;
    mpfr_t temp_var_991;
    mpfr_t temp_var_992;
    mpfr_t temp_var_993;
    mpfr_t temp_var_994;
    mpfr_t temp_var_995;
    mpfr_t temp_var_996;
    mpfr_t temp_var_997;
    mpfr_t temp_var_998;
    mpfr_t temp_var_999;
    mpfr_t temp_var_1000;
    mpfr_t temp_var_1001;
    mpfr_t temp_var_1002;
    mpfr_t temp_var_1003;
    mpfr_t temp_var_1004;
    mpfr_t temp_var_1005;
    mpfr_t temp_var_1006;
    mpfr_t temp_var_1007;
    mpfr_t temp_var_1008;
    mpfr_t temp_var_1009;
    mpfr_t temp_var_1010;
    mpfr_t temp_var_1011;
    mpfr_t temp_var_1012;
    mpfr_t temp_var_1013;
    mpfr_t temp_var_1014;
    mpfr_t temp_var_1015;
    mpfr_t temp_var_1016;
    mpfr_t temp_var_1017;
    mpfr_t temp_var_1018;
    mpfr_t temp_var_1019;
    mpfr_t temp_var_1020;
int init_mpfr() { 
  mpfr_init2(initvalu_1_ecc, config_vals[1]);
  mpfr_init2(initvalu_2_ecc, config_vals[2]);
  mpfr_init2(initvalu_3_ecc, config_vals[3]);
  mpfr_init2(initvalu_4_ecc, config_vals[4]);
  mpfr_init2(initvalu_5_ecc, config_vals[5]);
  mpfr_init2(initvalu_6_ecc, config_vals[6]);
  mpfr_init2(initvalu_7_ecc, config_vals[7]);
  mpfr_init2(initvalu_8_ecc, config_vals[8]);
  mpfr_init2(initvalu_9_ecc, config_vals[9]);
  mpfr_init2(initvalu_10_ecc, config_vals[10]);
  mpfr_init2(initvalu_11_ecc, config_vals[11]);
  mpfr_init2(initvalu_12_ecc, config_vals[12]);
  mpfr_init2(initvalu_13_ecc, config_vals[13]);
  mpfr_init2(initvalu_14_ecc, config_vals[14]);
  mpfr_init2(initvalu_15_ecc, config_vals[15]);
  mpfr_init2(initvalu_16_ecc, config_vals[16]);
  mpfr_init2(initvalu_17_ecc, config_vals[17]);
  mpfr_init2(initvalu_18_ecc, config_vals[18]);
  mpfr_init2(initvalu_19_ecc, config_vals[19]);
  mpfr_init2(initvalu_20_ecc, config_vals[20]);
  mpfr_init2(initvalu_21_ecc, config_vals[21]);
  mpfr_init2(initvalu_22_ecc, config_vals[22]);
  mpfr_init2(initvalu_23_ecc, config_vals[23]);
  mpfr_init2(initvalu_24_ecc, config_vals[24]);
  mpfr_init2(initvalu_25_ecc, config_vals[25]);
  mpfr_init2(initvalu_26_ecc, config_vals[26]);
  mpfr_init2(initvalu_27_ecc, config_vals[27]);
  mpfr_init2(initvalu_28_ecc, config_vals[28]);
  mpfr_init2(initvalu_29_ecc, config_vals[29]);
  mpfr_init2(initvalu_30_ecc, config_vals[30]);
  mpfr_init2(initvalu_31_ecc, config_vals[31]);
  mpfr_init2(initvalu_32_ecc, config_vals[32]);
  mpfr_init2(initvalu_33_ecc, config_vals[33]);
  mpfr_init2(initvalu_34_ecc, config_vals[34]);
  mpfr_init2(initvalu_35_ecc, config_vals[35]);
  mpfr_init2(initvalu_36_ecc, config_vals[36]);
  mpfr_init2(initvalu_37_ecc, config_vals[37]);
  mpfr_init2(initvalu_38_ecc, config_vals[38]);
  mpfr_init2(initvalu_39_ecc, config_vals[39]);
  mpfr_init2(initvalu_40_ecc, config_vals[40]);
  mpfr_init2(initvalu_41_ecc, config_vals[41]);
  mpfr_init2(initvalu_42_ecc, config_vals[42]);
  mpfr_init2(initvalu_43_ecc, config_vals[43]);
  mpfr_init2(initvalu_44_ecc, config_vals[44]);
  mpfr_init2(initvalu_45_ecc, config_vals[45]);
  mpfr_init2(initvalu_46_ecc, config_vals[46]);
  mpfr_init2(parameter_1_ecc, config_vals[47]);
  mpfr_init2(pi_ecc, config_vals[48]);
  mpfr_init2(R_ecc, config_vals[49]);
  mpfr_init2(Frdy_ecc, config_vals[50]);
  mpfr_init2(Temp_ecc, config_vals[51]);
  mpfr_init2(FoRT_ecc, config_vals[52]);
  mpfr_init2(Cmem_ecc, config_vals[53]);
  mpfr_init2(Qpow_ecc, config_vals[54]);
  mpfr_init2(cellLength_ecc, config_vals[55]);
  mpfr_init2(cellRadius_ecc, config_vals[56]);
  mpfr_init2(junctionLength_ecc, config_vals[57]);
  mpfr_init2(junctionRadius_ecc, config_vals[58]);
  mpfr_init2(distSLcyto_ecc, config_vals[59]);
  mpfr_init2(distJuncSL_ecc, config_vals[60]);
  mpfr_init2(DcaJuncSL_ecc, config_vals[61]);
  mpfr_init2(DcaSLcyto_ecc, config_vals[62]);
  mpfr_init2(DnaJuncSL_ecc, config_vals[63]);
  mpfr_init2(DnaSLcyto_ecc, config_vals[64]);
  mpfr_init2(Vcell_ecc, config_vals[65]);
  mpfr_init2(Vmyo_ecc, config_vals[66]);
  mpfr_init2(Vsr_ecc, config_vals[67]);
  mpfr_init2(Vsl_ecc, config_vals[68]);
  mpfr_init2(Vjunc_ecc, config_vals[69]);
  mpfr_init2(SAjunc_ecc, config_vals[70]);
  mpfr_init2(SAsl_ecc, config_vals[71]);
  mpfr_init2(J_ca_juncsl_ecc, config_vals[72]);
  mpfr_init2(J_ca_slmyo_ecc, config_vals[73]);
  mpfr_init2(J_na_juncsl_ecc, config_vals[74]);
  mpfr_init2(J_na_slmyo_ecc, config_vals[75]);
  mpfr_init2(Fjunc_ecc, config_vals[76]);
  mpfr_init2(Fsl_ecc, config_vals[77]);
  mpfr_init2(Fjunc_CaL_ecc, config_vals[78]);
  mpfr_init2(Fsl_CaL_ecc, config_vals[79]);
  mpfr_init2(Cli_ecc, config_vals[80]);
  mpfr_init2(Clo_ecc, config_vals[81]);
  mpfr_init2(Ko_ecc, config_vals[82]);
  mpfr_init2(Nao_ecc, config_vals[83]);
  mpfr_init2(Cao_ecc, config_vals[84]);
  mpfr_init2(Mgi_ecc, config_vals[85]);
  mpfr_init2(ena_junc_ecc, config_vals[86]);
  mpfr_init2(ena_sl_ecc, config_vals[87]);
  mpfr_init2(ek_ecc, config_vals[88]);
  mpfr_init2(eca_junc_ecc, config_vals[89]);
  mpfr_init2(eca_sl_ecc, config_vals[90]);
  mpfr_init2(ecl_ecc, config_vals[91]);
  mpfr_init2(GNa_ecc, config_vals[92]);
  mpfr_init2(GNaB_ecc, config_vals[93]);
  mpfr_init2(IbarNaK_ecc, config_vals[94]);
  mpfr_init2(KmNaip_ecc, config_vals[95]);
  mpfr_init2(KmKo_ecc, config_vals[96]);
  mpfr_init2(Q10NaK_ecc, config_vals[97]);
  mpfr_init2(Q10KmNai_ecc, config_vals[98]);
  mpfr_init2(pNaK_ecc, config_vals[99]);
  mpfr_init2(GtoSlow_ecc, config_vals[100]);
  mpfr_init2(GtoFast_ecc, config_vals[101]);
  mpfr_init2(gkp_ecc, config_vals[102]);
  mpfr_init2(GClCa_ecc, config_vals[103]);
  mpfr_init2(GClB_ecc, config_vals[104]);
  mpfr_init2(KdClCa_ecc, config_vals[105]);
  mpfr_init2(pNa_ecc, config_vals[106]);
  mpfr_init2(pCa_ecc, config_vals[107]);
  mpfr_init2(pK_ecc, config_vals[108]);
  mpfr_init2(KmCa_ecc, config_vals[109]);
  mpfr_init2(Q10CaL_ecc, config_vals[110]);
  mpfr_init2(IbarNCX_ecc, config_vals[111]);
  mpfr_init2(KmCai_ecc, config_vals[112]);
  mpfr_init2(KmCao_ecc, config_vals[113]);
  mpfr_init2(KmNai_ecc, config_vals[114]);
  mpfr_init2(KmNao_ecc, config_vals[115]);
  mpfr_init2(ksat_ecc, config_vals[116]);
  mpfr_init2(nu_ecc, config_vals[117]);
  mpfr_init2(Kdact_ecc, config_vals[118]);
  mpfr_init2(Q10NCX_ecc, config_vals[119]);
  mpfr_init2(IbarSLCaP_ecc, config_vals[120]);
  mpfr_init2(KmPCa_ecc, config_vals[121]);
  mpfr_init2(GCaB_ecc, config_vals[122]);
  mpfr_init2(Q10SLCaP_ecc, config_vals[123]);
  mpfr_init2(Q10SRCaP_ecc, config_vals[124]);
  mpfr_init2(Vmax_SRCaP_ecc, config_vals[125]);
  mpfr_init2(Kmf_ecc, config_vals[126]);
  mpfr_init2(Kmr_ecc, config_vals[127]);
  mpfr_init2(hillSRCaP_ecc, config_vals[128]);
  mpfr_init2(ks_ecc, config_vals[129]);
  mpfr_init2(koCa_ecc, config_vals[130]);
  mpfr_init2(kom_ecc, config_vals[131]);
  mpfr_init2(kiCa_ecc, config_vals[132]);
  mpfr_init2(kim_ecc, config_vals[133]);
  mpfr_init2(ec50SR_ecc, config_vals[134]);
  mpfr_init2(Bmax_Naj_ecc, config_vals[135]);
  mpfr_init2(Bmax_Nasl_ecc, config_vals[136]);
  mpfr_init2(koff_na_ecc, config_vals[137]);
  mpfr_init2(kon_na_ecc, config_vals[138]);
  mpfr_init2(Bmax_TnClow_ecc, config_vals[139]);
  mpfr_init2(koff_tncl_ecc, config_vals[140]);
  mpfr_init2(kon_tncl_ecc, config_vals[141]);
  mpfr_init2(Bmax_TnChigh_ecc, config_vals[142]);
  mpfr_init2(koff_tnchca_ecc, config_vals[143]);
  mpfr_init2(kon_tnchca_ecc, config_vals[144]);
  mpfr_init2(koff_tnchmg_ecc, config_vals[145]);
  mpfr_init2(kon_tnchmg_ecc, config_vals[146]);
  mpfr_init2(Bmax_CaM_ecc, config_vals[147]);
  mpfr_init2(koff_cam_ecc, config_vals[148]);
  mpfr_init2(kon_cam_ecc, config_vals[149]);
  mpfr_init2(Bmax_myosin_ecc, config_vals[150]);
  mpfr_init2(koff_myoca_ecc, config_vals[151]);
  mpfr_init2(kon_myoca_ecc, config_vals[152]);
  mpfr_init2(koff_myomg_ecc, config_vals[153]);
  mpfr_init2(kon_myomg_ecc, config_vals[154]);
  mpfr_init2(Bmax_SR_ecc, config_vals[155]);
  mpfr_init2(koff_sr_ecc, config_vals[156]);
  mpfr_init2(kon_sr_ecc, config_vals[157]);
  mpfr_init2(Bmax_SLlowsl_ecc, config_vals[158]);
  mpfr_init2(Bmax_SLlowj_ecc, config_vals[159]);
  mpfr_init2(koff_sll_ecc, config_vals[160]);
  mpfr_init2(kon_sll_ecc, config_vals[161]);
  mpfr_init2(Bmax_SLhighsl_ecc, config_vals[162]);
  mpfr_init2(Bmax_SLhighj_ecc, config_vals[163]);
  mpfr_init2(koff_slh_ecc, config_vals[164]);
  mpfr_init2(kon_slh_ecc, config_vals[165]);
  mpfr_init2(Bmax_Csqn_ecc, config_vals[166]);
  mpfr_init2(koff_csqn_ecc, config_vals[167]);
  mpfr_init2(kon_csqn_ecc, config_vals[168]);
  mpfr_init2(am_ecc, config_vals[169]);
  mpfr_init2(bm_ecc, config_vals[170]);
  mpfr_init2(ah_ecc, config_vals[171]);
  mpfr_init2(bh_ecc, config_vals[172]);
  mpfr_init2(aj_ecc, config_vals[173]);
  mpfr_init2(bj_ecc, config_vals[174]);
  mpfr_init2(I_Na_junc_ecc, config_vals[175]);
  mpfr_init2(I_Na_sl_ecc, config_vals[176]);
  mpfr_init2(I_Na_ecc, config_vals[177]);
  mpfr_init2(I_nabk_junc_ecc, config_vals[178]);
  mpfr_init2(I_nabk_sl_ecc, config_vals[179]);
  mpfr_init2(I_nabk_ecc, config_vals[180]);
  mpfr_init2(sigma_ecc, config_vals[181]);
  mpfr_init2(fnak_ecc, config_vals[182]);
  mpfr_init2(I_nak_junc_ecc, config_vals[183]);
  mpfr_init2(I_nak_sl_ecc, config_vals[184]);
  mpfr_init2(I_nak_ecc, config_vals[185]);
  mpfr_init2(gkr_ecc, config_vals[186]);
  mpfr_init2(xrss_ecc, config_vals[187]);
  mpfr_init2(tauxr_ecc, config_vals[188]);
  mpfr_init2(rkr_ecc, config_vals[189]);
  mpfr_init2(I_kr_ecc, config_vals[190]);
  mpfr_init2(pcaks_junc_ecc, config_vals[191]);
  mpfr_init2(pcaks_sl_ecc, config_vals[192]);
  mpfr_init2(gks_junc_ecc, config_vals[193]);
  mpfr_init2(gks_sl_ecc, config_vals[194]);
  mpfr_init2(eks_ecc, config_vals[195]);
  mpfr_init2(xsss_ecc, config_vals[196]);
  mpfr_init2(tauxs_ecc, config_vals[197]);
  mpfr_init2(I_ks_junc_ecc, config_vals[198]);
  mpfr_init2(I_ks_sl_ecc, config_vals[199]);
  mpfr_init2(I_ks_ecc, config_vals[200]);
  mpfr_init2(kp_kp_ecc, config_vals[201]);
  mpfr_init2(I_kp_junc_ecc, config_vals[202]);
  mpfr_init2(I_kp_sl_ecc, config_vals[203]);
  mpfr_init2(I_kp_ecc, config_vals[204]);
  mpfr_init2(xtoss_ecc, config_vals[205]);
  mpfr_init2(ytoss_ecc, config_vals[206]);
  mpfr_init2(rtoss_ecc, config_vals[207]);
  mpfr_init2(tauxtos_ecc, config_vals[208]);
  mpfr_init2(tauytos_ecc, config_vals[209]);
  mpfr_init2(taurtos_ecc, config_vals[210]);
  mpfr_init2(I_tos_ecc, config_vals[211]);
  mpfr_init2(tauxtof_ecc, config_vals[212]);
  mpfr_init2(tauytof_ecc, config_vals[213]);
  mpfr_init2(I_tof_ecc, config_vals[214]);
  mpfr_init2(I_to_ecc, config_vals[215]);
  mpfr_init2(aki_ecc, config_vals[216]);
  mpfr_init2(bki_ecc, config_vals[217]);
  mpfr_init2(kiss_ecc, config_vals[218]);
  mpfr_init2(I_ki_ecc, config_vals[219]);
  mpfr_init2(I_ClCa_junc_ecc, config_vals[220]);
  mpfr_init2(I_ClCa_sl_ecc, config_vals[221]);
  mpfr_init2(I_ClCa_ecc, config_vals[222]);
  mpfr_init2(I_Clbk_ecc, config_vals[223]);
  mpfr_init2(dss_ecc, config_vals[224]);
  mpfr_init2(taud_ecc, config_vals[225]);
  mpfr_init2(fss_ecc, config_vals[226]);
  mpfr_init2(tauf_ecc, config_vals[227]);
  mpfr_init2(ibarca_j_ecc, config_vals[228]);
  mpfr_init2(ibarca_sl_ecc, config_vals[229]);
  mpfr_init2(ibark_ecc, config_vals[230]);
  mpfr_init2(ibarna_j_ecc, config_vals[231]);
  mpfr_init2(ibarna_sl_ecc, config_vals[232]);
  mpfr_init2(I_Ca_junc_ecc, config_vals[233]);
  mpfr_init2(I_Ca_sl_ecc, config_vals[234]);
  mpfr_init2(I_Ca_ecc, config_vals[235]);
  mpfr_init2(I_CaK_ecc, config_vals[236]);
  mpfr_init2(I_CaNa_junc_ecc, config_vals[237]);
  mpfr_init2(I_CaNa_sl_ecc, config_vals[238]);
  mpfr_init2(I_CaNa_ecc, config_vals[239]);
  mpfr_init2(I_Catot_ecc, config_vals[240]);
  mpfr_init2(Ka_junc_ecc, config_vals[241]);
  mpfr_init2(Ka_sl_ecc, config_vals[242]);
  mpfr_init2(s1_junc_ecc, config_vals[243]);
  mpfr_init2(s1_sl_ecc, config_vals[244]);
  mpfr_init2(s2_junc_ecc, config_vals[245]);
  mpfr_init2(s3_junc_ecc, config_vals[246]);
  mpfr_init2(s2_sl_ecc, config_vals[247]);
  mpfr_init2(s3_sl_ecc, config_vals[248]);
  mpfr_init2(I_ncx_junc_ecc, config_vals[249]);
  mpfr_init2(I_ncx_sl_ecc, config_vals[250]);
  mpfr_init2(I_ncx_ecc, config_vals[251]);
  mpfr_init2(I_pca_junc_ecc, config_vals[252]);
  mpfr_init2(I_pca_sl_ecc, config_vals[253]);
  mpfr_init2(I_pca_ecc, config_vals[254]);
  mpfr_init2(I_cabk_junc_ecc, config_vals[255]);
  mpfr_init2(I_cabk_sl_ecc, config_vals[256]);
  mpfr_init2(I_cabk_ecc, config_vals[257]);
  mpfr_init2(MaxSR_ecc, config_vals[258]);
  mpfr_init2(MinSR_ecc, config_vals[259]);
  mpfr_init2(kCaSR_ecc, config_vals[260]);
  mpfr_init2(koSRCa_ecc, config_vals[261]);
  mpfr_init2(kiSRCa_ecc, config_vals[262]);
  mpfr_init2(RI_ecc, config_vals[263]);
  mpfr_init2(J_SRCarel_ecc, config_vals[264]);
  mpfr_init2(J_serca_ecc, config_vals[265]);
  mpfr_init2(J_SRleak_ecc, config_vals[266]);
  mpfr_init2(J_CaB_cytosol_ecc, config_vals[267]);
  mpfr_init2(J_CaB_junction_ecc, config_vals[268]);
  mpfr_init2(J_CaB_sl_ecc, config_vals[269]);
  mpfr_init2(oneovervsr_ecc, config_vals[270]);
  mpfr_init2(I_Na_tot_junc_ecc, config_vals[271]);
  mpfr_init2(I_Na_tot_sl_ecc, config_vals[272]);
  mpfr_init2(oneovervsl_ecc, config_vals[273]);
  mpfr_init2(I_K_tot_ecc, config_vals[274]);
  mpfr_init2(I_Ca_tot_junc_ecc, config_vals[275]);
  mpfr_init2(I_Ca_tot_sl_ecc, config_vals[276]);
  mpfr_init2(junc_sl_ecc, config_vals[277]);
  mpfr_init2(sl_junc_ecc, config_vals[278]);
  mpfr_init2(sl_myo_ecc, config_vals[279]);
  mpfr_init2(myo_sl_ecc, config_vals[280]);
  mpfr_init2(I_app_ecc, config_vals[281]);
  mpfr_init2(V_hold_ecc, config_vals[282]);
  mpfr_init2(V_test_ecc, config_vals[283]);
  mpfr_init2(V_clamp_ecc, config_vals[284]);
  mpfr_init2(R_clamp_ecc, config_vals[285]);
  mpfr_init2(I_Na_tot_ecc, config_vals[286]);
  mpfr_init2(I_Cl_tot_ecc, config_vals[287]);
  mpfr_init2(I_Ca_tot_ecc, config_vals[288]);
  mpfr_init2(I_tot_ecc, config_vals[289]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
    mpfr_init2 (temp_var_4, config_vals[0]);
    mpfr_init2 (temp_var_5, config_vals[0]);
    mpfr_init2 (temp_var_6, config_vals[0]);
    mpfr_init2 (temp_var_7, config_vals[0]);
    mpfr_init2 (temp_var_8, config_vals[0]);
    mpfr_init2 (temp_var_9, config_vals[0]);
    mpfr_init2 (temp_var_10, config_vals[0]);
    mpfr_init2 (temp_var_11, config_vals[0]);
    mpfr_init2 (temp_var_12, config_vals[0]);
    mpfr_init2 (temp_var_13, config_vals[0]);
    mpfr_init2 (temp_var_14, config_vals[0]);
    mpfr_init2 (temp_var_15, config_vals[0]);
    mpfr_init2 (temp_var_16, config_vals[0]);
    mpfr_init2 (temp_var_17, config_vals[0]);
    mpfr_init2 (temp_var_18, config_vals[0]);
    mpfr_init2 (temp_var_19, config_vals[0]);
    mpfr_init2 (temp_var_20, config_vals[0]);
    mpfr_init2 (temp_var_21, config_vals[0]);
    mpfr_init2 (temp_var_22, config_vals[0]);
    mpfr_init2 (temp_var_23, config_vals[0]);
    mpfr_init2 (temp_var_24, config_vals[0]);
    mpfr_init2 (temp_var_25, config_vals[0]);
    mpfr_init2 (temp_var_26, config_vals[0]);
    mpfr_init2 (temp_var_27, config_vals[0]);
    mpfr_init2 (temp_var_28, config_vals[0]);
    mpfr_init2 (temp_var_29, config_vals[0]);
    mpfr_init2 (temp_var_30, config_vals[0]);
    mpfr_init2 (temp_var_31, config_vals[0]);
    mpfr_init2 (temp_var_32, config_vals[0]);
    mpfr_init2 (temp_var_33, config_vals[0]);
    mpfr_init2 (temp_var_34, config_vals[0]);
    mpfr_init2 (temp_var_35, config_vals[0]);
    mpfr_init2 (temp_var_36, config_vals[0]);
    mpfr_init2 (temp_var_37, config_vals[0]);
    mpfr_init2 (temp_var_38, config_vals[0]);
    mpfr_init2 (temp_var_39, config_vals[0]);
    mpfr_init2 (temp_var_40, config_vals[0]);
    mpfr_init2 (temp_var_41, config_vals[0]);
    mpfr_init2 (temp_var_42, config_vals[0]);
    mpfr_init2 (temp_var_43, config_vals[0]);
    mpfr_init2 (temp_var_44, config_vals[0]);
    mpfr_init2 (temp_var_45, config_vals[0]);
    mpfr_init2 (temp_var_46, config_vals[0]);
    mpfr_init2 (temp_var_47, config_vals[0]);
    mpfr_init2 (temp_var_48, config_vals[0]);
    mpfr_init2 (temp_var_49, config_vals[0]);
    mpfr_init2 (temp_var_50, config_vals[0]);
    mpfr_init2 (temp_var_51, config_vals[0]);
    mpfr_init2 (temp_var_52, config_vals[0]);
    mpfr_init2 (temp_var_53, config_vals[0]);
    mpfr_init2 (temp_var_54, config_vals[0]);
    mpfr_init2 (temp_var_55, config_vals[0]);
    mpfr_init2 (temp_var_56, config_vals[0]);
    mpfr_init2 (temp_var_57, config_vals[0]);
    mpfr_init2 (temp_var_58, config_vals[0]);
    mpfr_init2 (temp_var_59, config_vals[0]);
    mpfr_init2 (temp_var_60, config_vals[0]);
    mpfr_init2 (temp_var_61, config_vals[0]);
    mpfr_init2 (temp_var_62, config_vals[0]);
    mpfr_init2 (temp_var_63, config_vals[0]);
    mpfr_init2 (temp_var_64, config_vals[0]);
    mpfr_init2 (temp_var_65, config_vals[0]);
    mpfr_init2 (temp_var_66, config_vals[0]);
    mpfr_init2 (temp_var_67, config_vals[0]);
    mpfr_init2 (temp_var_68, config_vals[0]);
    mpfr_init2 (temp_var_69, config_vals[0]);
    mpfr_init2 (temp_var_70, config_vals[0]);
    mpfr_init2 (temp_var_71, config_vals[0]);
    mpfr_init2 (temp_var_72, config_vals[0]);
    mpfr_init2 (temp_var_73, config_vals[0]);
    mpfr_init2 (temp_var_74, config_vals[0]);
    mpfr_init2 (temp_var_75, config_vals[0]);
    mpfr_init2 (temp_var_76, config_vals[0]);
    mpfr_init2 (temp_var_77, config_vals[0]);
    mpfr_init2 (temp_var_78, config_vals[0]);
    mpfr_init2 (temp_var_79, config_vals[0]);
    mpfr_init2 (temp_var_80, config_vals[0]);
        mpfr_init2 (temp_var_81, config_vals[0]);
        mpfr_init2 (temp_var_82, config_vals[0]);
        mpfr_init2 (temp_var_83, config_vals[0]);
        mpfr_init2 (temp_var_84, config_vals[0]);
        mpfr_init2 (temp_var_85, config_vals[0]);
        mpfr_init2 (temp_var_86, config_vals[0]);
        mpfr_init2 (temp_var_87, config_vals[0]);
        mpfr_init2 (temp_var_88, config_vals[0]);
        mpfr_init2 (temp_var_89, config_vals[0]);
        mpfr_init2 (temp_var_90, config_vals[0]);
        mpfr_init2 (temp_var_91, config_vals[0]);
        mpfr_init2 (temp_var_92, config_vals[0]);
        mpfr_init2 (temp_var_93, config_vals[0]);
        mpfr_init2 (temp_var_94, config_vals[0]);
        mpfr_init2 (temp_var_95, config_vals[0]);
        mpfr_init2 (temp_var_96, config_vals[0]);
        mpfr_init2 (temp_var_97, config_vals[0]);
        mpfr_init2 (temp_var_98, config_vals[0]);
        mpfr_init2 (temp_var_99, config_vals[0]);
        mpfr_init2 (temp_var_100, config_vals[0]);
        mpfr_init2 (temp_var_101, config_vals[0]);
        mpfr_init2 (temp_var_102, config_vals[0]);
        mpfr_init2 (temp_var_103, config_vals[0]);
        mpfr_init2 (temp_var_104, config_vals[0]);
        mpfr_init2 (temp_var_105, config_vals[0]);
    mpfr_init2 (temp_var_106, config_vals[0]);
    mpfr_init2 (temp_var_107, config_vals[0]);
    mpfr_init2 (temp_var_108, config_vals[0]);
    mpfr_init2 (temp_var_109, config_vals[0]);
    mpfr_init2 (temp_var_110, config_vals[0]);
    mpfr_init2 (temp_var_111, config_vals[0]);
    mpfr_init2 (temp_var_112, config_vals[0]);
    mpfr_init2 (temp_var_113, config_vals[0]);
    mpfr_init2 (temp_var_114, config_vals[0]);
    mpfr_init2 (temp_var_115, config_vals[0]);
    mpfr_init2 (temp_var_116, config_vals[0]);
    mpfr_init2 (temp_var_117, config_vals[0]);
    mpfr_init2 (temp_var_118, config_vals[0]);
    mpfr_init2 (temp_var_119, config_vals[0]);
    mpfr_init2 (temp_var_120, config_vals[0]);
    mpfr_init2 (temp_var_121, config_vals[0]);
    mpfr_init2 (temp_var_122, config_vals[0]);
    mpfr_init2 (temp_var_123, config_vals[0]);
    mpfr_init2 (temp_var_124, config_vals[0]);
    mpfr_init2 (temp_var_125, config_vals[0]);
    mpfr_init2 (temp_var_126, config_vals[0]);
    mpfr_init2 (temp_var_127, config_vals[0]);
    mpfr_init2 (temp_var_128, config_vals[0]);
    mpfr_init2 (temp_var_129, config_vals[0]);
    mpfr_init2 (temp_var_130, config_vals[0]);
    mpfr_init2 (temp_var_131, config_vals[0]);
    mpfr_init2 (temp_var_132, config_vals[0]);
    mpfr_init2 (temp_var_133, config_vals[0]);
    mpfr_init2 (temp_var_134, config_vals[0]);
    mpfr_init2 (temp_var_135, config_vals[0]);
    mpfr_init2 (temp_var_136, config_vals[0]);
    mpfr_init2 (temp_var_137, config_vals[0]);
    mpfr_init2 (temp_var_138, config_vals[0]);
    mpfr_init2 (temp_var_139, config_vals[0]);
    mpfr_init2 (temp_var_140, config_vals[0]);
    mpfr_init2 (temp_var_141, config_vals[0]);
    mpfr_init2 (temp_var_142, config_vals[0]);
    mpfr_init2 (temp_var_143, config_vals[0]);
    mpfr_init2 (temp_var_144, config_vals[0]);
    mpfr_init2 (temp_var_145, config_vals[0]);
    mpfr_init2 (temp_var_146, config_vals[0]);
    mpfr_init2 (temp_var_147, config_vals[0]);
    mpfr_init2 (temp_var_148, config_vals[0]);
    mpfr_init2 (temp_var_149, config_vals[0]);
    mpfr_init2 (temp_var_150, config_vals[0]);
    mpfr_init2 (temp_var_151, config_vals[0]);
    mpfr_init2 (temp_var_152, config_vals[0]);
    mpfr_init2 (temp_var_153, config_vals[0]);
    mpfr_init2 (temp_var_154, config_vals[0]);
    mpfr_init2 (temp_var_155, config_vals[0]);
    mpfr_init2 (temp_var_156, config_vals[0]);
    mpfr_init2 (temp_var_157, config_vals[0]);
    mpfr_init2 (temp_var_158, config_vals[0]);
    mpfr_init2 (temp_var_159, config_vals[0]);
    mpfr_init2 (temp_var_160, config_vals[0]);
    mpfr_init2 (temp_var_161, config_vals[0]);
    mpfr_init2 (temp_var_162, config_vals[0]);
    mpfr_init2 (temp_var_163, config_vals[0]);
    mpfr_init2 (temp_var_164, config_vals[0]);
    mpfr_init2 (temp_var_165, config_vals[0]);
    mpfr_init2 (temp_var_166, config_vals[0]);
    mpfr_init2 (temp_var_167, config_vals[0]);
    mpfr_init2 (temp_var_168, config_vals[0]);
    mpfr_init2 (temp_var_169, config_vals[0]);
    mpfr_init2 (temp_var_170, config_vals[0]);
    mpfr_init2 (temp_var_171, config_vals[0]);
    mpfr_init2 (temp_var_172, config_vals[0]);
    mpfr_init2 (temp_var_173, config_vals[0]);
    mpfr_init2 (temp_var_174, config_vals[0]);
    mpfr_init2 (temp_var_175, config_vals[0]);
    mpfr_init2 (temp_var_176, config_vals[0]);
    mpfr_init2 (temp_var_177, config_vals[0]);
    mpfr_init2 (temp_var_178, config_vals[0]);
    mpfr_init2 (temp_var_179, config_vals[0]);
    mpfr_init2 (temp_var_180, config_vals[0]);
    mpfr_init2 (temp_var_181, config_vals[0]);
    mpfr_init2 (temp_var_182, config_vals[0]);
    mpfr_init2 (temp_var_183, config_vals[0]);
    mpfr_init2 (temp_var_184, config_vals[0]);
    mpfr_init2 (temp_var_185, config_vals[0]);
    mpfr_init2 (temp_var_186, config_vals[0]);
    mpfr_init2 (temp_var_187, config_vals[0]);
    mpfr_init2 (temp_var_188, config_vals[0]);
    mpfr_init2 (temp_var_189, config_vals[0]);
    mpfr_init2 (temp_var_190, config_vals[0]);
    mpfr_init2 (temp_var_191, config_vals[0]);
    mpfr_init2 (temp_var_192, config_vals[0]);
    mpfr_init2 (temp_var_193, config_vals[0]);
    mpfr_init2 (temp_var_194, config_vals[0]);
    mpfr_init2 (temp_var_195, config_vals[0]);
    mpfr_init2 (temp_var_196, config_vals[0]);
    mpfr_init2 (temp_var_197, config_vals[0]);
    mpfr_init2 (temp_var_198, config_vals[0]);
    mpfr_init2 (temp_var_199, config_vals[0]);
    mpfr_init2 (temp_var_200, config_vals[0]);
    mpfr_init2 (temp_var_201, config_vals[0]);
    mpfr_init2 (temp_var_202, config_vals[0]);
    mpfr_init2 (temp_var_203, config_vals[0]);
    mpfr_init2 (temp_var_204, config_vals[0]);
    mpfr_init2 (temp_var_205, config_vals[0]);
    mpfr_init2 (temp_var_206, config_vals[0]);
    mpfr_init2 (temp_var_207, config_vals[0]);
    mpfr_init2 (temp_var_208, config_vals[0]);
    mpfr_init2 (temp_var_209, config_vals[0]);
    mpfr_init2 (temp_var_210, config_vals[0]);
    mpfr_init2 (temp_var_211, config_vals[0]);
    mpfr_init2 (temp_var_212, config_vals[0]);
    mpfr_init2 (temp_var_213, config_vals[0]);
    mpfr_init2 (temp_var_214, config_vals[0]);
    mpfr_init2 (temp_var_215, config_vals[0]);
    mpfr_init2 (temp_var_216, config_vals[0]);
    mpfr_init2 (temp_var_217, config_vals[0]);
    mpfr_init2 (temp_var_218, config_vals[0]);
    mpfr_init2 (temp_var_219, config_vals[0]);
    mpfr_init2 (temp_var_220, config_vals[0]);
    mpfr_init2 (temp_var_221, config_vals[0]);
    mpfr_init2 (temp_var_222, config_vals[0]);
    mpfr_init2 (temp_var_223, config_vals[0]);
    mpfr_init2 (temp_var_224, config_vals[0]);
    mpfr_init2 (temp_var_225, config_vals[0]);
    mpfr_init2 (temp_var_226, config_vals[0]);
    mpfr_init2 (temp_var_227, config_vals[0]);
    mpfr_init2 (temp_var_228, config_vals[0]);
    mpfr_init2 (temp_var_229, config_vals[0]);
    mpfr_init2 (temp_var_230, config_vals[0]);
    mpfr_init2 (temp_var_231, config_vals[0]);
    mpfr_init2 (temp_var_232, config_vals[0]);
    mpfr_init2 (temp_var_233, config_vals[0]);
    mpfr_init2 (temp_var_234, config_vals[0]);
    mpfr_init2 (temp_var_235, config_vals[0]);
    mpfr_init2 (temp_var_236, config_vals[0]);
    mpfr_init2 (temp_var_237, config_vals[0]);
    mpfr_init2 (temp_var_238, config_vals[0]);
    mpfr_init2 (temp_var_239, config_vals[0]);
    mpfr_init2 (temp_var_240, config_vals[0]);
    mpfr_init2 (temp_var_241, config_vals[0]);
    mpfr_init2 (temp_var_242, config_vals[0]);
    mpfr_init2 (temp_var_243, config_vals[0]);
    mpfr_init2 (temp_var_244, config_vals[0]);
    mpfr_init2 (temp_var_245, config_vals[0]);
    mpfr_init2 (temp_var_246, config_vals[0]);
    mpfr_init2 (temp_var_247, config_vals[0]);
    mpfr_init2 (temp_var_248, config_vals[0]);
    mpfr_init2 (temp_var_249, config_vals[0]);
    mpfr_init2 (temp_var_250, config_vals[0]);
    mpfr_init2 (temp_var_251, config_vals[0]);
    mpfr_init2 (temp_var_252, config_vals[0]);
    mpfr_init2 (temp_var_253, config_vals[0]);
    mpfr_init2 (temp_var_254, config_vals[0]);
    mpfr_init2 (temp_var_255, config_vals[0]);
    mpfr_init2 (temp_var_256, config_vals[0]);
    mpfr_init2 (temp_var_257, config_vals[0]);
    mpfr_init2 (temp_var_258, config_vals[0]);
    mpfr_init2 (temp_var_259, config_vals[0]);
    mpfr_init2 (temp_var_260, config_vals[0]);
    mpfr_init2 (temp_var_261, config_vals[0]);
    mpfr_init2 (temp_var_262, config_vals[0]);
    mpfr_init2 (temp_var_263, config_vals[0]);
    mpfr_init2 (temp_var_264, config_vals[0]);
    mpfr_init2 (temp_var_265, config_vals[0]);
    mpfr_init2 (temp_var_266, config_vals[0]);
    mpfr_init2 (temp_var_267, config_vals[0]);
    mpfr_init2 (temp_var_268, config_vals[0]);
    mpfr_init2 (temp_var_269, config_vals[0]);
    mpfr_init2 (temp_var_270, config_vals[0]);
    mpfr_init2 (temp_var_271, config_vals[0]);
    mpfr_init2 (temp_var_272, config_vals[0]);
    mpfr_init2 (temp_var_273, config_vals[0]);
    mpfr_init2 (temp_var_274, config_vals[0]);
    mpfr_init2 (temp_var_275, config_vals[0]);
    mpfr_init2 (temp_var_276, config_vals[0]);
    mpfr_init2 (temp_var_277, config_vals[0]);
    mpfr_init2 (temp_var_278, config_vals[0]);
    mpfr_init2 (temp_var_279, config_vals[0]);
    mpfr_init2 (temp_var_280, config_vals[0]);
    mpfr_init2 (temp_var_281, config_vals[0]);
    mpfr_init2 (temp_var_282, config_vals[0]);
    mpfr_init2 (temp_var_283, config_vals[0]);
    mpfr_init2 (temp_var_284, config_vals[0]);
    mpfr_init2 (temp_var_285, config_vals[0]);
    mpfr_init2 (temp_var_286, config_vals[0]);
    mpfr_init2 (temp_var_287, config_vals[0]);
    mpfr_init2 (temp_var_288, config_vals[0]);
    mpfr_init2 (temp_var_289, config_vals[0]);
    mpfr_init2 (temp_var_290, config_vals[0]);
    mpfr_init2 (temp_var_291, config_vals[0]);
    mpfr_init2 (temp_var_292, config_vals[0]);
    mpfr_init2 (temp_var_293, config_vals[0]);
    mpfr_init2 (temp_var_294, config_vals[0]);
    mpfr_init2 (temp_var_295, config_vals[0]);
    mpfr_init2 (temp_var_296, config_vals[0]);
    mpfr_init2 (temp_var_297, config_vals[0]);
    mpfr_init2 (temp_var_298, config_vals[0]);
    mpfr_init2 (temp_var_299, config_vals[0]);
    mpfr_init2 (temp_var_300, config_vals[0]);
    mpfr_init2 (temp_var_301, config_vals[0]);
    mpfr_init2 (temp_var_302, config_vals[0]);
    mpfr_init2 (temp_var_303, config_vals[0]);
    mpfr_init2 (temp_var_304, config_vals[0]);
    mpfr_init2 (temp_var_305, config_vals[0]);
    mpfr_init2 (temp_var_306, config_vals[0]);
    mpfr_init2 (temp_var_307, config_vals[0]);
    mpfr_init2 (temp_var_308, config_vals[0]);
    mpfr_init2 (temp_var_309, config_vals[0]);
    mpfr_init2 (temp_var_310, config_vals[0]);
    mpfr_init2 (temp_var_311, config_vals[0]);
    mpfr_init2 (temp_var_312, config_vals[0]);
    mpfr_init2 (temp_var_313, config_vals[0]);
    mpfr_init2 (temp_var_314, config_vals[0]);
    mpfr_init2 (temp_var_315, config_vals[0]);
    mpfr_init2 (temp_var_316, config_vals[0]);
    mpfr_init2 (temp_var_317, config_vals[0]);
    mpfr_init2 (temp_var_318, config_vals[0]);
    mpfr_init2 (temp_var_319, config_vals[0]);
    mpfr_init2 (temp_var_320, config_vals[0]);
    mpfr_init2 (temp_var_321, config_vals[0]);
    mpfr_init2 (temp_var_322, config_vals[0]);
    mpfr_init2 (temp_var_323, config_vals[0]);
    mpfr_init2 (temp_var_324, config_vals[0]);
    mpfr_init2 (temp_var_325, config_vals[0]);
    mpfr_init2 (temp_var_326, config_vals[0]);
    mpfr_init2 (temp_var_327, config_vals[0]);
    mpfr_init2 (temp_var_328, config_vals[0]);
    mpfr_init2 (temp_var_329, config_vals[0]);
    mpfr_init2 (temp_var_330, config_vals[0]);
    mpfr_init2 (temp_var_331, config_vals[0]);
    mpfr_init2 (temp_var_332, config_vals[0]);
    mpfr_init2 (temp_var_333, config_vals[0]);
    mpfr_init2 (temp_var_334, config_vals[0]);
    mpfr_init2 (temp_var_335, config_vals[0]);
    mpfr_init2 (temp_var_336, config_vals[0]);
    mpfr_init2 (temp_var_337, config_vals[0]);
    mpfr_init2 (temp_var_338, config_vals[0]);
    mpfr_init2 (temp_var_339, config_vals[0]);
    mpfr_init2 (temp_var_340, config_vals[0]);
    mpfr_init2 (temp_var_341, config_vals[0]);
    mpfr_init2 (temp_var_342, config_vals[0]);
    mpfr_init2 (temp_var_343, config_vals[0]);
    mpfr_init2 (temp_var_344, config_vals[0]);
    mpfr_init2 (temp_var_345, config_vals[0]);
    mpfr_init2 (temp_var_346, config_vals[0]);
    mpfr_init2 (temp_var_347, config_vals[0]);
    mpfr_init2 (temp_var_348, config_vals[0]);
    mpfr_init2 (temp_var_349, config_vals[0]);
    mpfr_init2 (temp_var_350, config_vals[0]);
    mpfr_init2 (temp_var_351, config_vals[0]);
    mpfr_init2 (temp_var_352, config_vals[0]);
    mpfr_init2 (temp_var_353, config_vals[0]);
    mpfr_init2 (temp_var_354, config_vals[0]);
    mpfr_init2 (temp_var_355, config_vals[0]);
    mpfr_init2 (temp_var_356, config_vals[0]);
    mpfr_init2 (temp_var_357, config_vals[0]);
    mpfr_init2 (temp_var_358, config_vals[0]);
    mpfr_init2 (temp_var_359, config_vals[0]);
    mpfr_init2 (temp_var_360, config_vals[0]);
    mpfr_init2 (temp_var_361, config_vals[0]);
    mpfr_init2 (temp_var_362, config_vals[0]);
    mpfr_init2 (temp_var_363, config_vals[0]);
    mpfr_init2 (temp_var_364, config_vals[0]);
    mpfr_init2 (temp_var_365, config_vals[0]);
    mpfr_init2 (temp_var_366, config_vals[0]);
    mpfr_init2 (temp_var_367, config_vals[0]);
    mpfr_init2 (temp_var_368, config_vals[0]);
    mpfr_init2 (temp_var_369, config_vals[0]);
    mpfr_init2 (temp_var_370, config_vals[0]);
    mpfr_init2 (temp_var_371, config_vals[0]);
    mpfr_init2 (temp_var_372, config_vals[0]);
    mpfr_init2 (temp_var_373, config_vals[0]);
    mpfr_init2 (temp_var_374, config_vals[0]);
    mpfr_init2 (temp_var_375, config_vals[0]);
    mpfr_init2 (temp_var_376, config_vals[0]);
    mpfr_init2 (temp_var_377, config_vals[0]);
    mpfr_init2 (temp_var_378, config_vals[0]);
    mpfr_init2 (temp_var_379, config_vals[0]);
    mpfr_init2 (temp_var_380, config_vals[0]);
    mpfr_init2 (temp_var_381, config_vals[0]);
    mpfr_init2 (temp_var_382, config_vals[0]);
    mpfr_init2 (temp_var_383, config_vals[0]);
    mpfr_init2 (temp_var_384, config_vals[0]);
    mpfr_init2 (temp_var_385, config_vals[0]);
    mpfr_init2 (temp_var_386, config_vals[0]);
    mpfr_init2 (temp_var_387, config_vals[0]);
    mpfr_init2 (temp_var_388, config_vals[0]);
    mpfr_init2 (temp_var_389, config_vals[0]);
    mpfr_init2 (temp_var_390, config_vals[0]);
    mpfr_init2 (temp_var_391, config_vals[0]);
    mpfr_init2 (temp_var_392, config_vals[0]);
    mpfr_init2 (temp_var_393, config_vals[0]);
    mpfr_init2 (temp_var_394, config_vals[0]);
    mpfr_init2 (temp_var_395, config_vals[0]);
    mpfr_init2 (temp_var_396, config_vals[0]);
    mpfr_init2 (temp_var_397, config_vals[0]);
    mpfr_init2 (temp_var_398, config_vals[0]);
    mpfr_init2 (temp_var_399, config_vals[0]);
    mpfr_init2 (temp_var_400, config_vals[0]);
    mpfr_init2 (temp_var_401, config_vals[0]);
    mpfr_init2 (temp_var_402, config_vals[0]);
    mpfr_init2 (temp_var_403, config_vals[0]);
    mpfr_init2 (temp_var_404, config_vals[0]);
    mpfr_init2 (temp_var_405, config_vals[0]);
    mpfr_init2 (temp_var_406, config_vals[0]);
    mpfr_init2 (temp_var_407, config_vals[0]);
    mpfr_init2 (temp_var_408, config_vals[0]);
    mpfr_init2 (temp_var_409, config_vals[0]);
    mpfr_init2 (temp_var_410, config_vals[0]);
    mpfr_init2 (temp_var_411, config_vals[0]);
    mpfr_init2 (temp_var_412, config_vals[0]);
    mpfr_init2 (temp_var_413, config_vals[0]);
    mpfr_init2 (temp_var_414, config_vals[0]);
    mpfr_init2 (temp_var_415, config_vals[0]);
    mpfr_init2 (temp_var_416, config_vals[0]);
    mpfr_init2 (temp_var_417, config_vals[0]);
    mpfr_init2 (temp_var_418, config_vals[0]);
    mpfr_init2 (temp_var_419, config_vals[0]);
    mpfr_init2 (temp_var_420, config_vals[0]);
    mpfr_init2 (temp_var_421, config_vals[0]);
    mpfr_init2 (temp_var_422, config_vals[0]);
    mpfr_init2 (temp_var_423, config_vals[0]);
    mpfr_init2 (temp_var_424, config_vals[0]);
    mpfr_init2 (temp_var_425, config_vals[0]);
    mpfr_init2 (temp_var_426, config_vals[0]);
    mpfr_init2 (temp_var_427, config_vals[0]);
    mpfr_init2 (temp_var_428, config_vals[0]);
    mpfr_init2 (temp_var_429, config_vals[0]);
    mpfr_init2 (temp_var_430, config_vals[0]);
    mpfr_init2 (temp_var_431, config_vals[0]);
    mpfr_init2 (temp_var_432, config_vals[0]);
    mpfr_init2 (temp_var_433, config_vals[0]);
    mpfr_init2 (temp_var_434, config_vals[0]);
    mpfr_init2 (temp_var_435, config_vals[0]);
    mpfr_init2 (temp_var_436, config_vals[0]);
    mpfr_init2 (temp_var_437, config_vals[0]);
    mpfr_init2 (temp_var_438, config_vals[0]);
    mpfr_init2 (temp_var_439, config_vals[0]);
    mpfr_init2 (temp_var_440, config_vals[0]);
    mpfr_init2 (temp_var_441, config_vals[0]);
    mpfr_init2 (temp_var_442, config_vals[0]);
    mpfr_init2 (temp_var_443, config_vals[0]);
    mpfr_init2 (temp_var_444, config_vals[0]);
    mpfr_init2 (temp_var_445, config_vals[0]);
    mpfr_init2 (temp_var_446, config_vals[0]);
    mpfr_init2 (temp_var_447, config_vals[0]);
    mpfr_init2 (temp_var_448, config_vals[0]);
    mpfr_init2 (temp_var_449, config_vals[0]);
    mpfr_init2 (temp_var_450, config_vals[0]);
    mpfr_init2 (temp_var_451, config_vals[0]);
    mpfr_init2 (temp_var_452, config_vals[0]);
    mpfr_init2 (temp_var_453, config_vals[0]);
    mpfr_init2 (temp_var_454, config_vals[0]);
    mpfr_init2 (temp_var_455, config_vals[0]);
    mpfr_init2 (temp_var_456, config_vals[0]);
    mpfr_init2 (temp_var_457, config_vals[0]);
    mpfr_init2 (temp_var_458, config_vals[0]);
    mpfr_init2 (temp_var_459, config_vals[0]);
    mpfr_init2 (temp_var_460, config_vals[0]);
    mpfr_init2 (temp_var_461, config_vals[0]);
    mpfr_init2 (temp_var_462, config_vals[0]);
    mpfr_init2 (temp_var_463, config_vals[0]);
    mpfr_init2 (temp_var_464, config_vals[0]);
    mpfr_init2 (temp_var_465, config_vals[0]);
    mpfr_init2 (temp_var_466, config_vals[0]);
    mpfr_init2 (temp_var_467, config_vals[0]);
    mpfr_init2 (temp_var_468, config_vals[0]);
    mpfr_init2 (temp_var_469, config_vals[0]);
    mpfr_init2 (temp_var_470, config_vals[0]);
    mpfr_init2 (temp_var_471, config_vals[0]);
    mpfr_init2 (temp_var_472, config_vals[0]);
    mpfr_init2 (temp_var_473, config_vals[0]);
    mpfr_init2 (temp_var_474, config_vals[0]);
    mpfr_init2 (temp_var_475, config_vals[0]);
    mpfr_init2 (temp_var_476, config_vals[0]);
    mpfr_init2 (temp_var_477, config_vals[0]);
    mpfr_init2 (temp_var_478, config_vals[0]);
    mpfr_init2 (temp_var_479, config_vals[0]);
    mpfr_init2 (temp_var_480, config_vals[0]);
    mpfr_init2 (temp_var_481, config_vals[0]);
    mpfr_init2 (temp_var_482, config_vals[0]);
    mpfr_init2 (temp_var_483, config_vals[0]);
    mpfr_init2 (temp_var_484, config_vals[0]);
    mpfr_init2 (temp_var_485, config_vals[0]);
    mpfr_init2 (temp_var_486, config_vals[0]);
    mpfr_init2 (temp_var_487, config_vals[0]);
    mpfr_init2 (temp_var_488, config_vals[0]);
    mpfr_init2 (temp_var_489, config_vals[0]);
    mpfr_init2 (temp_var_490, config_vals[0]);
    mpfr_init2 (temp_var_491, config_vals[0]);
    mpfr_init2 (temp_var_492, config_vals[0]);
    mpfr_init2 (temp_var_493, config_vals[0]);
    mpfr_init2 (temp_var_494, config_vals[0]);
    mpfr_init2 (temp_var_495, config_vals[0]);
    mpfr_init2 (temp_var_496, config_vals[0]);
    mpfr_init2 (temp_var_497, config_vals[0]);
    mpfr_init2 (temp_var_498, config_vals[0]);
    mpfr_init2 (temp_var_499, config_vals[0]);
    mpfr_init2 (temp_var_500, config_vals[0]);
    mpfr_init2 (temp_var_501, config_vals[0]);
    mpfr_init2 (temp_var_502, config_vals[0]);
    mpfr_init2 (temp_var_503, config_vals[0]);
    mpfr_init2 (temp_var_504, config_vals[0]);
    mpfr_init2 (temp_var_505, config_vals[0]);
    mpfr_init2 (temp_var_506, config_vals[0]);
    mpfr_init2 (temp_var_507, config_vals[0]);
    mpfr_init2 (temp_var_508, config_vals[0]);
    mpfr_init2 (temp_var_509, config_vals[0]);
    mpfr_init2 (temp_var_510, config_vals[0]);
    mpfr_init2 (temp_var_511, config_vals[0]);
    mpfr_init2 (temp_var_512, config_vals[0]);
    mpfr_init2 (temp_var_513, config_vals[0]);
    mpfr_init2 (temp_var_514, config_vals[0]);
    mpfr_init2 (temp_var_515, config_vals[0]);
    mpfr_init2 (temp_var_516, config_vals[0]);
    mpfr_init2 (temp_var_517, config_vals[0]);
    mpfr_init2 (temp_var_518, config_vals[0]);
    mpfr_init2 (temp_var_519, config_vals[0]);
    mpfr_init2 (temp_var_520, config_vals[0]);
    mpfr_init2 (temp_var_521, config_vals[0]);
    mpfr_init2 (temp_var_522, config_vals[0]);
    mpfr_init2 (temp_var_523, config_vals[0]);
    mpfr_init2 (temp_var_524, config_vals[0]);
    mpfr_init2 (temp_var_525, config_vals[0]);
    mpfr_init2 (temp_var_526, config_vals[0]);
    mpfr_init2 (temp_var_527, config_vals[0]);
    mpfr_init2 (temp_var_528, config_vals[0]);
    mpfr_init2 (temp_var_529, config_vals[0]);
    mpfr_init2 (temp_var_530, config_vals[0]);
    mpfr_init2 (temp_var_531, config_vals[0]);
    mpfr_init2 (temp_var_532, config_vals[0]);
    mpfr_init2 (temp_var_533, config_vals[0]);
    mpfr_init2 (temp_var_534, config_vals[0]);
    mpfr_init2 (temp_var_535, config_vals[0]);
    mpfr_init2 (temp_var_536, config_vals[0]);
    mpfr_init2 (temp_var_537, config_vals[0]);
    mpfr_init2 (temp_var_538, config_vals[0]);
    mpfr_init2 (temp_var_539, config_vals[0]);
    mpfr_init2 (temp_var_540, config_vals[0]);
    mpfr_init2 (temp_var_541, config_vals[0]);
    mpfr_init2 (temp_var_542, config_vals[0]);
    mpfr_init2 (temp_var_543, config_vals[0]);
    mpfr_init2 (temp_var_544, config_vals[0]);
    mpfr_init2 (temp_var_545, config_vals[0]);
    mpfr_init2 (temp_var_546, config_vals[0]);
    mpfr_init2 (temp_var_547, config_vals[0]);
    mpfr_init2 (temp_var_548, config_vals[0]);
    mpfr_init2 (temp_var_549, config_vals[0]);
    mpfr_init2 (temp_var_550, config_vals[0]);
    mpfr_init2 (temp_var_551, config_vals[0]);
    mpfr_init2 (temp_var_552, config_vals[0]);
    mpfr_init2 (temp_var_553, config_vals[0]);
    mpfr_init2 (temp_var_554, config_vals[0]);
    mpfr_init2 (temp_var_555, config_vals[0]);
    mpfr_init2 (temp_var_556, config_vals[0]);
    mpfr_init2 (temp_var_557, config_vals[0]);
    mpfr_init2 (temp_var_558, config_vals[0]);
    mpfr_init2 (temp_var_559, config_vals[0]);
    mpfr_init2 (temp_var_560, config_vals[0]);
    mpfr_init2 (temp_var_561, config_vals[0]);
    mpfr_init2 (temp_var_562, config_vals[0]);
    mpfr_init2 (temp_var_563, config_vals[0]);
    mpfr_init2 (temp_var_564, config_vals[0]);
    mpfr_init2 (temp_var_565, config_vals[0]);
    mpfr_init2 (temp_var_566, config_vals[0]);
    mpfr_init2 (temp_var_567, config_vals[0]);
    mpfr_init2 (temp_var_568, config_vals[0]);
    mpfr_init2 (temp_var_569, config_vals[0]);
    mpfr_init2 (temp_var_570, config_vals[0]);
    mpfr_init2 (temp_var_571, config_vals[0]);
    mpfr_init2 (temp_var_572, config_vals[0]);
    mpfr_init2 (temp_var_573, config_vals[0]);
    mpfr_init2 (temp_var_574, config_vals[0]);
    mpfr_init2 (temp_var_575, config_vals[0]);
    mpfr_init2 (temp_var_576, config_vals[0]);
    mpfr_init2 (temp_var_577, config_vals[0]);
    mpfr_init2 (temp_var_578, config_vals[0]);
    mpfr_init2 (temp_var_579, config_vals[0]);
    mpfr_init2 (temp_var_580, config_vals[0]);
    mpfr_init2 (temp_var_581, config_vals[0]);
    mpfr_init2 (temp_var_582, config_vals[0]);
    mpfr_init2 (temp_var_583, config_vals[0]);
    mpfr_init2 (temp_var_584, config_vals[0]);
    mpfr_init2 (temp_var_585, config_vals[0]);
    mpfr_init2 (temp_var_586, config_vals[0]);
    mpfr_init2 (temp_var_587, config_vals[0]);
    mpfr_init2 (temp_var_588, config_vals[0]);
    mpfr_init2 (temp_var_589, config_vals[0]);
    mpfr_init2 (temp_var_590, config_vals[0]);
    mpfr_init2 (temp_var_591, config_vals[0]);
    mpfr_init2 (temp_var_592, config_vals[0]);
    mpfr_init2 (temp_var_593, config_vals[0]);
    mpfr_init2 (temp_var_594, config_vals[0]);
    mpfr_init2 (temp_var_595, config_vals[0]);
    mpfr_init2 (temp_var_596, config_vals[0]);
    mpfr_init2 (temp_var_597, config_vals[0]);
    mpfr_init2 (temp_var_598, config_vals[0]);
    mpfr_init2 (temp_var_599, config_vals[0]);
    mpfr_init2 (temp_var_600, config_vals[0]);
    mpfr_init2 (temp_var_601, config_vals[0]);
    mpfr_init2 (temp_var_602, config_vals[0]);
    mpfr_init2 (temp_var_603, config_vals[0]);
    mpfr_init2 (temp_var_604, config_vals[0]);
    mpfr_init2 (temp_var_605, config_vals[0]);
    mpfr_init2 (temp_var_606, config_vals[0]);
    mpfr_init2 (temp_var_607, config_vals[0]);
    mpfr_init2 (temp_var_608, config_vals[0]);
    mpfr_init2 (temp_var_609, config_vals[0]);
    mpfr_init2 (temp_var_610, config_vals[0]);
    mpfr_init2 (temp_var_611, config_vals[0]);
    mpfr_init2 (temp_var_612, config_vals[0]);
    mpfr_init2 (temp_var_613, config_vals[0]);
    mpfr_init2 (temp_var_614, config_vals[0]);
    mpfr_init2 (temp_var_615, config_vals[0]);
    mpfr_init2 (temp_var_616, config_vals[0]);
    mpfr_init2 (temp_var_617, config_vals[0]);
    mpfr_init2 (temp_var_618, config_vals[0]);
    mpfr_init2 (temp_var_619, config_vals[0]);
    mpfr_init2 (temp_var_620, config_vals[0]);
    mpfr_init2 (temp_var_621, config_vals[0]);
    mpfr_init2 (temp_var_622, config_vals[0]);
    mpfr_init2 (temp_var_623, config_vals[0]);
    mpfr_init2 (temp_var_624, config_vals[0]);
    mpfr_init2 (temp_var_625, config_vals[0]);
    mpfr_init2 (temp_var_626, config_vals[0]);
    mpfr_init2 (temp_var_627, config_vals[0]);
    mpfr_init2 (temp_var_628, config_vals[0]);
    mpfr_init2 (temp_var_629, config_vals[0]);
    mpfr_init2 (temp_var_630, config_vals[0]);
    mpfr_init2 (temp_var_631, config_vals[0]);
    mpfr_init2 (temp_var_632, config_vals[0]);
    mpfr_init2 (temp_var_633, config_vals[0]);
    mpfr_init2 (temp_var_634, config_vals[0]);
    mpfr_init2 (temp_var_635, config_vals[0]);
    mpfr_init2 (temp_var_636, config_vals[0]);
    mpfr_init2 (temp_var_637, config_vals[0]);
    mpfr_init2 (temp_var_638, config_vals[0]);
    mpfr_init2 (temp_var_639, config_vals[0]);
    mpfr_init2 (temp_var_640, config_vals[0]);
    mpfr_init2 (temp_var_641, config_vals[0]);
    mpfr_init2 (temp_var_642, config_vals[0]);
    mpfr_init2 (temp_var_643, config_vals[0]);
    mpfr_init2 (temp_var_644, config_vals[0]);
    mpfr_init2 (temp_var_645, config_vals[0]);
    mpfr_init2 (temp_var_646, config_vals[0]);
    mpfr_init2 (temp_var_647, config_vals[0]);
    mpfr_init2 (temp_var_648, config_vals[0]);
    mpfr_init2 (temp_var_649, config_vals[0]);
    mpfr_init2 (temp_var_650, config_vals[0]);
    mpfr_init2 (temp_var_651, config_vals[0]);
    mpfr_init2 (temp_var_652, config_vals[0]);
    mpfr_init2 (temp_var_653, config_vals[0]);
    mpfr_init2 (temp_var_654, config_vals[0]);
    mpfr_init2 (temp_var_655, config_vals[0]);
    mpfr_init2 (temp_var_656, config_vals[0]);
    mpfr_init2 (temp_var_657, config_vals[0]);
    mpfr_init2 (temp_var_658, config_vals[0]);
    mpfr_init2 (temp_var_659, config_vals[0]);
    mpfr_init2 (temp_var_660, config_vals[0]);
    mpfr_init2 (temp_var_661, config_vals[0]);
    mpfr_init2 (temp_var_662, config_vals[0]);
    mpfr_init2 (temp_var_663, config_vals[0]);
    mpfr_init2 (temp_var_664, config_vals[0]);
    mpfr_init2 (temp_var_665, config_vals[0]);
    mpfr_init2 (temp_var_666, config_vals[0]);
    mpfr_init2 (temp_var_667, config_vals[0]);
    mpfr_init2 (temp_var_668, config_vals[0]);
    mpfr_init2 (temp_var_669, config_vals[0]);
    mpfr_init2 (temp_var_670, config_vals[0]);
    mpfr_init2 (temp_var_671, config_vals[0]);
    mpfr_init2 (temp_var_672, config_vals[0]);
    mpfr_init2 (temp_var_673, config_vals[0]);
    mpfr_init2 (temp_var_674, config_vals[0]);
    mpfr_init2 (temp_var_675, config_vals[0]);
    mpfr_init2 (temp_var_676, config_vals[0]);
    mpfr_init2 (temp_var_677, config_vals[0]);
    mpfr_init2 (temp_var_678, config_vals[0]);
    mpfr_init2 (temp_var_679, config_vals[0]);
    mpfr_init2 (temp_var_680, config_vals[0]);
    mpfr_init2 (temp_var_681, config_vals[0]);
    mpfr_init2 (temp_var_682, config_vals[0]);
    mpfr_init2 (temp_var_683, config_vals[0]);
    mpfr_init2 (temp_var_684, config_vals[0]);
    mpfr_init2 (temp_var_685, config_vals[0]);
    mpfr_init2 (temp_var_686, config_vals[0]);
    mpfr_init2 (temp_var_687, config_vals[0]);
    mpfr_init2 (temp_var_688, config_vals[0]);
    mpfr_init2 (temp_var_689, config_vals[0]);
    mpfr_init2 (temp_var_690, config_vals[0]);
    mpfr_init2 (temp_var_691, config_vals[0]);
    mpfr_init2 (temp_var_692, config_vals[0]);
    mpfr_init2 (temp_var_693, config_vals[0]);
    mpfr_init2 (temp_var_694, config_vals[0]);
    mpfr_init2 (temp_var_695, config_vals[0]);
    mpfr_init2 (temp_var_696, config_vals[0]);
    mpfr_init2 (temp_var_697, config_vals[0]);
    mpfr_init2 (temp_var_698, config_vals[0]);
    mpfr_init2 (temp_var_699, config_vals[0]);
    mpfr_init2 (temp_var_700, config_vals[0]);
    mpfr_init2 (temp_var_701, config_vals[0]);
    mpfr_init2 (temp_var_702, config_vals[0]);
    mpfr_init2 (temp_var_703, config_vals[0]);
    mpfr_init2 (temp_var_704, config_vals[0]);
    mpfr_init2 (temp_var_705, config_vals[0]);
    mpfr_init2 (temp_var_706, config_vals[0]);
    mpfr_init2 (temp_var_707, config_vals[0]);
    mpfr_init2 (temp_var_708, config_vals[0]);
    mpfr_init2 (temp_var_709, config_vals[0]);
    mpfr_init2 (temp_var_710, config_vals[0]);
    mpfr_init2 (temp_var_711, config_vals[0]);
    mpfr_init2 (temp_var_712, config_vals[0]);
    mpfr_init2 (temp_var_713, config_vals[0]);
    mpfr_init2 (temp_var_714, config_vals[0]);
    mpfr_init2 (temp_var_715, config_vals[0]);
    mpfr_init2 (temp_var_716, config_vals[0]);
    mpfr_init2 (temp_var_717, config_vals[0]);
    mpfr_init2 (temp_var_718, config_vals[0]);
    mpfr_init2 (temp_var_719, config_vals[0]);
    mpfr_init2 (temp_var_720, config_vals[0]);
    mpfr_init2 (temp_var_721, config_vals[0]);
        mpfr_init2 (temp_var_722, config_vals[0]);
    mpfr_init2 (temp_var_723, config_vals[0]);
    mpfr_init2 (temp_var_724, config_vals[0]);
  mpfr_init2(JCa_cam, config_vals[290]);
  mpfr_init2(CaM_cam, config_vals[291]);
  mpfr_init2(Ca2CaM_cam, config_vals[292]);
  mpfr_init2(Ca4CaM_cam, config_vals[293]);
  mpfr_init2(CaMB_cam, config_vals[294]);
  mpfr_init2(Ca2CaMB_cam, config_vals[295]);
  mpfr_init2(Ca4CaMB_cam, config_vals[296]);
  mpfr_init2(Pb2_cam, config_vals[297]);
  mpfr_init2(Pb_cam, config_vals[298]);
  mpfr_init2(Pt_cam, config_vals[299]);
  mpfr_init2(Pt2_cam, config_vals[300]);
  mpfr_init2(Pa_cam, config_vals[301]);
  mpfr_init2(Ca4CaN_cam, config_vals[302]);
  mpfr_init2(CaMCa4CaN_cam, config_vals[303]);
  mpfr_init2(Ca2CaMCa4CaN_cam, config_vals[304]);
  mpfr_init2(Ca4CaMCa4CaN_cam, config_vals[305]);
  mpfr_init2(CaMtot_cam, config_vals[306]);
  mpfr_init2(Btot_cam, config_vals[307]);
  mpfr_init2(CaMKIItot_cam, config_vals[308]);
  mpfr_init2(CaNtot_cam, config_vals[309]);
  mpfr_init2(PP1tot_cam, config_vals[310]);
  mpfr_init2(K_cam, config_vals[311]);
  mpfr_init2(Mg_cam, config_vals[312]);
  mpfr_init2(Kd02_cam, config_vals[313]);
  mpfr_init2(Kd24_cam, config_vals[314]);
  mpfr_init2(k20_cam, config_vals[315]);
  mpfr_init2(k02_cam, config_vals[316]);
  mpfr_init2(k42_cam, config_vals[317]);
  mpfr_init2(k24_cam, config_vals[318]);
  mpfr_init2(k0Boff_cam, config_vals[319]);
  mpfr_init2(k0Bon_cam, config_vals[320]);
  mpfr_init2(k2Boff_cam, config_vals[321]);
  mpfr_init2(k2Bon_cam, config_vals[322]);
  mpfr_init2(k4Boff_cam, config_vals[323]);
  mpfr_init2(k4Bon_cam, config_vals[324]);
  mpfr_init2(k20B_cam, config_vals[325]);
  mpfr_init2(k02B_cam, config_vals[326]);
  mpfr_init2(k42B_cam, config_vals[327]);
  mpfr_init2(k24B_cam, config_vals[328]);
  mpfr_init2(kbi_cam, config_vals[329]);
  mpfr_init2(kib_cam, config_vals[330]);
  mpfr_init2(kpp1_cam, config_vals[331]);
  mpfr_init2(Kmpp1_cam, config_vals[332]);
  mpfr_init2(kib2_cam, config_vals[333]);
  mpfr_init2(kb2i_cam, config_vals[334]);
  mpfr_init2(kb24_cam, config_vals[335]);
  mpfr_init2(kb42_cam, config_vals[336]);
  mpfr_init2(kta_cam, config_vals[337]);
  mpfr_init2(kat_cam, config_vals[338]);
  mpfr_init2(kt42_cam, config_vals[339]);
  mpfr_init2(kt24_cam, config_vals[340]);
  mpfr_init2(kat2_cam, config_vals[341]);
  mpfr_init2(kt2a_cam, config_vals[342]);
  mpfr_init2(kcanCaoff_cam, config_vals[343]);
  mpfr_init2(kcanCaon_cam, config_vals[344]);
  mpfr_init2(kcanCaM4on_cam, config_vals[345]);
  mpfr_init2(kcanCaM4off_cam, config_vals[346]);
  mpfr_init2(kcanCaM2on_cam, config_vals[347]);
  mpfr_init2(kcanCaM2off_cam, config_vals[348]);
  mpfr_init2(kcanCaM0on_cam, config_vals[349]);
  mpfr_init2(kcanCaM0off_cam, config_vals[350]);
  mpfr_init2(k02can_cam, config_vals[351]);
  mpfr_init2(k20can_cam, config_vals[352]);
  mpfr_init2(k24can_cam, config_vals[353]);
  mpfr_init2(k42can_cam, config_vals[354]);
  mpfr_init2(rcn02_cam, config_vals[355]);
  mpfr_init2(rcn24_cam, config_vals[356]);
  mpfr_init2(B_cam, config_vals[357]);
  mpfr_init2(rcn02B_cam, config_vals[358]);
  mpfr_init2(rcn24B_cam, config_vals[359]);
  mpfr_init2(rcn0B_cam, config_vals[360]);
  mpfr_init2(rcn2B_cam, config_vals[361]);
  mpfr_init2(rcn4B_cam, config_vals[362]);
  mpfr_init2(Ca2CaN_cam, config_vals[363]);
  mpfr_init2(rcnCa4CaN_cam, config_vals[364]);
  mpfr_init2(rcn02CaN_cam, config_vals[365]);
  mpfr_init2(rcn24CaN_cam, config_vals[366]);
  mpfr_init2(rcn0CaN_cam, config_vals[367]);
  mpfr_init2(rcn2CaN_cam, config_vals[368]);
  mpfr_init2(rcn4CaN_cam, config_vals[369]);
  mpfr_init2(Pix_cam, config_vals[370]);
  mpfr_init2(rcnCKib2_cam, config_vals[371]);
  mpfr_init2(rcnCKb2b_cam, config_vals[372]);
  mpfr_init2(rcnCKib_cam, config_vals[373]);
  mpfr_init2(T_cam, config_vals[374]);
  mpfr_init2(kbt_cam, config_vals[375]);
  mpfr_init2(rcnCKbt_cam, config_vals[376]);
  mpfr_init2(rcnCKtt2_cam, config_vals[377]);
  mpfr_init2(rcnCKta_cam, config_vals[378]);
  mpfr_init2(rcnCKt2a_cam, config_vals[379]);
  mpfr_init2(rcnCKt2b2_cam, config_vals[380]);
  mpfr_init2(rcnCKai_cam, config_vals[381]);
  mpfr_init2(dCaM_cam, config_vals[382]);
  mpfr_init2(dCa2CaM_cam, config_vals[383]);
  mpfr_init2(dCa4CaM_cam, config_vals[384]);
  mpfr_init2(dCaMB_cam, config_vals[385]);
  mpfr_init2(dCa2CaMB_cam, config_vals[386]);
  mpfr_init2(dCa4CaMB_cam, config_vals[387]);
  mpfr_init2(dPb2_cam, config_vals[388]);
  mpfr_init2(dPb_cam, config_vals[389]);
  mpfr_init2(dPt_cam, config_vals[390]);
  mpfr_init2(dPt2_cam, config_vals[391]);
  mpfr_init2(dPa_cam, config_vals[392]);
  mpfr_init2(dCa4CaN_cam, config_vals[393]);
  mpfr_init2(dCaMCa4CaN_cam, config_vals[394]);
  mpfr_init2(dCa2CaMCa4CaN_cam, config_vals[395]);
  mpfr_init2(dCa4CaMCa4CaN_cam, config_vals[396]);
    mpfr_init2 (temp_var_725, config_vals[0]);
    mpfr_init2 (temp_var_726, config_vals[0]);
    mpfr_init2 (temp_var_727, config_vals[0]);
    mpfr_init2 (temp_var_728, config_vals[0]);
    mpfr_init2 (temp_var_729, config_vals[0]);
    mpfr_init2 (temp_var_730, config_vals[0]);
    mpfr_init2 (temp_var_731, config_vals[0]);
    mpfr_init2 (temp_var_732, config_vals[0]);
    mpfr_init2 (temp_var_733, config_vals[0]);
    mpfr_init2 (temp_var_734, config_vals[0]);
    mpfr_init2 (temp_var_735, config_vals[0]);
    mpfr_init2 (temp_var_736, config_vals[0]);
    mpfr_init2 (temp_var_737, config_vals[0]);
    mpfr_init2 (temp_var_738, config_vals[0]);
    mpfr_init2 (temp_var_739, config_vals[0]);
    mpfr_init2 (temp_var_740, config_vals[0]);
    mpfr_init2 (temp_var_741, config_vals[0]);
    mpfr_init2 (temp_var_742, config_vals[0]);
    mpfr_init2 (temp_var_743, config_vals[0]);
        mpfr_init2 (temp_var_744, config_vals[0]);
        mpfr_init2 (temp_var_745, config_vals[0]);
        mpfr_init2 (temp_var_746, config_vals[0]);
        mpfr_init2 (temp_var_747, config_vals[0]);
        mpfr_init2 (temp_var_748, config_vals[0]);
        mpfr_init2 (temp_var_749, config_vals[0]);
        mpfr_init2 (temp_var_750, config_vals[0]);
        mpfr_init2 (temp_var_751, config_vals[0]);
        mpfr_init2 (temp_var_752, config_vals[0]);
        mpfr_init2 (temp_var_753, config_vals[0]);
        mpfr_init2 (temp_var_754, config_vals[0]);
        mpfr_init2 (temp_var_755, config_vals[0]);
        mpfr_init2 (temp_var_756, config_vals[0]);
        mpfr_init2 (temp_var_757, config_vals[0]);
        mpfr_init2 (temp_var_758, config_vals[0]);
        mpfr_init2 (temp_var_759, config_vals[0]);
        mpfr_init2 (temp_var_760, config_vals[0]);
        mpfr_init2 (temp_var_761, config_vals[0]);
        mpfr_init2 (temp_var_762, config_vals[0]);
        mpfr_init2 (temp_var_763, config_vals[0]);
        mpfr_init2 (temp_var_764, config_vals[0]);
        mpfr_init2 (temp_var_765, config_vals[0]);
        mpfr_init2 (temp_var_766, config_vals[0]);
        mpfr_init2 (temp_var_767, config_vals[0]);
        mpfr_init2 (temp_var_768, config_vals[0]);
        mpfr_init2 (temp_var_769, config_vals[0]);
        mpfr_init2 (temp_var_770, config_vals[0]);
        mpfr_init2 (temp_var_771, config_vals[0]);
        mpfr_init2 (temp_var_772, config_vals[0]);
        mpfr_init2 (temp_var_773, config_vals[0]);
        mpfr_init2 (temp_var_774, config_vals[0]);
        mpfr_init2 (temp_var_775, config_vals[0]);
        mpfr_init2 (temp_var_776, config_vals[0]);
        mpfr_init2 (temp_var_777, config_vals[0]);
        mpfr_init2 (temp_var_778, config_vals[0]);
        mpfr_init2 (temp_var_779, config_vals[0]);
        mpfr_init2 (temp_var_780, config_vals[0]);
        mpfr_init2 (temp_var_781, config_vals[0]);
        mpfr_init2 (temp_var_782, config_vals[0]);
        mpfr_init2 (temp_var_783, config_vals[0]);
        mpfr_init2 (temp_var_784, config_vals[0]);
        mpfr_init2 (temp_var_785, config_vals[0]);
        mpfr_init2 (temp_var_786, config_vals[0]);
        mpfr_init2 (temp_var_787, config_vals[0]);
    mpfr_init2 (temp_var_788, config_vals[0]);
    mpfr_init2 (temp_var_789, config_vals[0]);
    mpfr_init2 (temp_var_790, config_vals[0]);
    mpfr_init2 (temp_var_791, config_vals[0]);
    mpfr_init2 (temp_var_792, config_vals[0]);
    mpfr_init2 (temp_var_793, config_vals[0]);
    mpfr_init2 (temp_var_794, config_vals[0]);
    mpfr_init2 (temp_var_795, config_vals[0]);
    mpfr_init2 (temp_var_796, config_vals[0]);
    mpfr_init2 (temp_var_797, config_vals[0]);
    mpfr_init2 (temp_var_798, config_vals[0]);
    mpfr_init2 (temp_var_799, config_vals[0]);
    mpfr_init2 (temp_var_800, config_vals[0]);
    mpfr_init2 (temp_var_801, config_vals[0]);
    mpfr_init2 (temp_var_802, config_vals[0]);
    mpfr_init2 (temp_var_803, config_vals[0]);
    mpfr_init2 (temp_var_804, config_vals[0]);
    mpfr_init2 (temp_var_805, config_vals[0]);
    mpfr_init2 (temp_var_806, config_vals[0]);
    mpfr_init2 (temp_var_807, config_vals[0]);
    mpfr_init2 (temp_var_808, config_vals[0]);
    mpfr_init2 (temp_var_809, config_vals[0]);
    mpfr_init2 (temp_var_810, config_vals[0]);
    mpfr_init2 (temp_var_811, config_vals[0]);
    mpfr_init2 (temp_var_812, config_vals[0]);
    mpfr_init2 (temp_var_813, config_vals[0]);
    mpfr_init2 (temp_var_814, config_vals[0]);
    mpfr_init2 (temp_var_815, config_vals[0]);
    mpfr_init2 (temp_var_816, config_vals[0]);
    mpfr_init2 (temp_var_817, config_vals[0]);
    mpfr_init2 (temp_var_818, config_vals[0]);
    mpfr_init2 (temp_var_819, config_vals[0]);
    mpfr_init2 (temp_var_820, config_vals[0]);
    mpfr_init2 (temp_var_821, config_vals[0]);
    mpfr_init2 (temp_var_822, config_vals[0]);
    mpfr_init2 (temp_var_823, config_vals[0]);
    mpfr_init2 (temp_var_824, config_vals[0]);
    mpfr_init2 (temp_var_825, config_vals[0]);
    mpfr_init2 (temp_var_826, config_vals[0]);
    mpfr_init2 (temp_var_827, config_vals[0]);
    mpfr_init2 (temp_var_828, config_vals[0]);
    mpfr_init2 (temp_var_829, config_vals[0]);
    mpfr_init2 (temp_var_830, config_vals[0]);
    mpfr_init2 (temp_var_831, config_vals[0]);
    mpfr_init2 (temp_var_832, config_vals[0]);
    mpfr_init2 (temp_var_833, config_vals[0]);
    mpfr_init2 (temp_var_834, config_vals[0]);
    mpfr_init2 (temp_var_835, config_vals[0]);
    mpfr_init2 (temp_var_836, config_vals[0]);
    mpfr_init2 (temp_var_837, config_vals[0]);
    mpfr_init2 (temp_var_838, config_vals[0]);
    mpfr_init2 (temp_var_839, config_vals[0]);
    mpfr_init2 (temp_var_840, config_vals[0]);
    mpfr_init2 (temp_var_841, config_vals[0]);
    mpfr_init2 (temp_var_842, config_vals[0]);
    mpfr_init2 (temp_var_843, config_vals[0]);
    mpfr_init2 (temp_var_844, config_vals[0]);
    mpfr_init2 (temp_var_845, config_vals[0]);
    mpfr_init2 (temp_var_846, config_vals[0]);
    mpfr_init2 (temp_var_847, config_vals[0]);
    mpfr_init2 (temp_var_848, config_vals[0]);
    mpfr_init2 (temp_var_849, config_vals[0]);
    mpfr_init2 (temp_var_850, config_vals[0]);
    mpfr_init2 (temp_var_851, config_vals[0]);
    mpfr_init2 (temp_var_852, config_vals[0]);
    mpfr_init2 (temp_var_853, config_vals[0]);
    mpfr_init2 (temp_var_854, config_vals[0]);
    mpfr_init2 (temp_var_855, config_vals[0]);
    mpfr_init2 (temp_var_856, config_vals[0]);
    mpfr_init2 (temp_var_857, config_vals[0]);
    mpfr_init2 (temp_var_858, config_vals[0]);
    mpfr_init2 (temp_var_859, config_vals[0]);
    mpfr_init2 (temp_var_860, config_vals[0]);
    mpfr_init2 (temp_var_861, config_vals[0]);
    mpfr_init2 (temp_var_862, config_vals[0]);
    mpfr_init2 (temp_var_863, config_vals[0]);
    mpfr_init2 (temp_var_864, config_vals[0]);
    mpfr_init2 (temp_var_865, config_vals[0]);
    mpfr_init2 (temp_var_866, config_vals[0]);
    mpfr_init2 (temp_var_867, config_vals[0]);
    mpfr_init2 (temp_var_868, config_vals[0]);
    mpfr_init2 (temp_var_869, config_vals[0]);
    mpfr_init2 (temp_var_870, config_vals[0]);
    mpfr_init2 (temp_var_871, config_vals[0]);
    mpfr_init2 (temp_var_872, config_vals[0]);
    mpfr_init2 (temp_var_873, config_vals[0]);
    mpfr_init2 (temp_var_874, config_vals[0]);
    mpfr_init2 (temp_var_875, config_vals[0]);
    mpfr_init2 (temp_var_876, config_vals[0]);
    mpfr_init2 (temp_var_877, config_vals[0]);
    mpfr_init2 (temp_var_878, config_vals[0]);
    mpfr_init2 (temp_var_879, config_vals[0]);
    mpfr_init2 (temp_var_880, config_vals[0]);
    mpfr_init2 (temp_var_881, config_vals[0]);
    mpfr_init2 (temp_var_882, config_vals[0]);
    mpfr_init2 (temp_var_883, config_vals[0]);
    mpfr_init2 (temp_var_884, config_vals[0]);
    mpfr_init2 (temp_var_885, config_vals[0]);
    mpfr_init2 (temp_var_886, config_vals[0]);
    mpfr_init2 (temp_var_887, config_vals[0]);
    mpfr_init2 (temp_var_888, config_vals[0]);
    mpfr_init2 (temp_var_889, config_vals[0]);
    mpfr_init2 (temp_var_890, config_vals[0]);
    mpfr_init2 (temp_var_891, config_vals[0]);
    mpfr_init2 (temp_var_892, config_vals[0]);
    mpfr_init2 (temp_var_893, config_vals[0]);
    mpfr_init2 (temp_var_894, config_vals[0]);
    mpfr_init2 (temp_var_895, config_vals[0]);
    mpfr_init2 (temp_var_896, config_vals[0]);
    mpfr_init2 (temp_var_897, config_vals[0]);
    mpfr_init2 (temp_var_898, config_vals[0]);
    mpfr_init2 (temp_var_899, config_vals[0]);
    mpfr_init2 (temp_var_900, config_vals[0]);
    mpfr_init2 (temp_var_901, config_vals[0]);
    mpfr_init2 (temp_var_902, config_vals[0]);
    mpfr_init2 (temp_var_903, config_vals[0]);
    mpfr_init2 (temp_var_904, config_vals[0]);
    mpfr_init2 (temp_var_905, config_vals[0]);
    mpfr_init2 (temp_var_906, config_vals[0]);
    mpfr_init2 (temp_var_907, config_vals[0]);
    mpfr_init2 (temp_var_908, config_vals[0]);
    mpfr_init2 (temp_var_909, config_vals[0]);
    mpfr_init2 (temp_var_910, config_vals[0]);
    mpfr_init2 (temp_var_911, config_vals[0]);
    mpfr_init2 (temp_var_912, config_vals[0]);
    mpfr_init2 (temp_var_913, config_vals[0]);
    mpfr_init2 (temp_var_914, config_vals[0]);
    mpfr_init2 (temp_var_915, config_vals[0]);
    mpfr_init2 (temp_var_916, config_vals[0]);
    mpfr_init2 (temp_var_917, config_vals[0]);
    mpfr_init2 (temp_var_918, config_vals[0]);
    mpfr_init2 (temp_var_919, config_vals[0]);
    mpfr_init2 (temp_var_920, config_vals[0]);
    mpfr_init2 (temp_var_921, config_vals[0]);
    mpfr_init2 (temp_var_922, config_vals[0]);
    mpfr_init2 (temp_var_923, config_vals[0]);
  mpfr_init2(BtotDyad_fin, config_vals[397]);
  mpfr_init2(CaMKIItotDyad_fin, config_vals[398]);
  mpfr_init2(Vmyo_fin, config_vals[399]);
  mpfr_init2(Vdyad_fin, config_vals[400]);
  mpfr_init2(VSL_fin, config_vals[401]);
  mpfr_init2(kDyadSL_fin, config_vals[402]);
  mpfr_init2(kSLmyo_fin, config_vals[403]);
  mpfr_init2(k0Boff_fin, config_vals[404]);
  mpfr_init2(k0Bon_fin, config_vals[405]);
  mpfr_init2(k2Boff_fin, config_vals[406]);
  mpfr_init2(k2Bon_fin, config_vals[407]);
  mpfr_init2(k4Boff_fin, config_vals[408]);
  mpfr_init2(k4Bon_fin, config_vals[409]);
  mpfr_init2(CaMtotDyad_fin, config_vals[410]);
  mpfr_init2(Bdyad_fin, config_vals[411]);
  mpfr_init2(J_cam_dyadSL_fin, config_vals[412]);
  mpfr_init2(J_ca2cam_dyadSL_fin, config_vals[413]);
  mpfr_init2(J_ca4cam_dyadSL_fin, config_vals[414]);
  mpfr_init2(J_cam_SLmyo_fin, config_vals[415]);
  mpfr_init2(J_ca2cam_SLmyo_fin, config_vals[416]);
  mpfr_init2(J_ca4cam_SLmyo_fin, config_vals[417]);
    mpfr_init2 (temp_var_924, config_vals[0]);
    mpfr_init2 (temp_var_925, config_vals[0]);
    mpfr_init2 (temp_var_926, config_vals[0]);
    mpfr_init2 (temp_var_927, config_vals[0]);
    mpfr_init2 (temp_var_928, config_vals[0]);
    mpfr_init2 (temp_var_929, config_vals[0]);
    mpfr_init2 (temp_var_930, config_vals[0]);
    mpfr_init2 (temp_var_931, config_vals[0]);
    mpfr_init2 (temp_var_932, config_vals[0]);
    mpfr_init2 (temp_var_933, config_vals[0]);
    mpfr_init2 (temp_var_934, config_vals[0]);
    mpfr_init2 (temp_var_935, config_vals[0]);
    mpfr_init2 (temp_var_936, config_vals[0]);
    mpfr_init2 (temp_var_937, config_vals[0]);
    mpfr_init2 (temp_var_938, config_vals[0]);
    mpfr_init2 (temp_var_939, config_vals[0]);
    mpfr_init2 (temp_var_940, config_vals[0]);
    mpfr_init2 (temp_var_941, config_vals[0]);
    mpfr_init2 (temp_var_942, config_vals[0]);
    mpfr_init2 (temp_var_943, config_vals[0]);
    mpfr_init2 (temp_var_944, config_vals[0]);
    mpfr_init2 (temp_var_945, config_vals[0]);
    mpfr_init2 (temp_var_946, config_vals[0]);
    mpfr_init2 (temp_var_947, config_vals[0]);
    mpfr_init2 (temp_var_948, config_vals[0]);
    mpfr_init2 (temp_var_949, config_vals[0]);
    mpfr_init2 (temp_var_950, config_vals[0]);
    mpfr_init2 (temp_var_951, config_vals[0]);
    mpfr_init2 (temp_var_952, config_vals[0]);
    mpfr_init2 (temp_var_953, config_vals[0]);
    mpfr_init2 (temp_var_954, config_vals[0]);
    mpfr_init2 (temp_var_955, config_vals[0]);
    mpfr_init2 (temp_var_956, config_vals[0]);
    mpfr_init2 (temp_var_957, config_vals[0]);
    mpfr_init2 (temp_var_958, config_vals[0]);
    mpfr_init2 (temp_var_959, config_vals[0]);
    mpfr_init2 (temp_var_960, config_vals[0]);
    mpfr_init2 (temp_var_961, config_vals[0]);
    mpfr_init2 (temp_var_962, config_vals[0]);
    mpfr_init2 (temp_var_963, config_vals[0]);
    mpfr_init2 (temp_var_964, config_vals[0]);
    mpfr_init2 (temp_var_965, config_vals[0]);
    mpfr_init2 (temp_var_966, config_vals[0]);
    mpfr_init2 (temp_var_967, config_vals[0]);
    mpfr_init2 (temp_var_968, config_vals[0]);
    mpfr_init2 (temp_var_969, config_vals[0]);
    mpfr_init2 (temp_var_970, config_vals[0]);
    mpfr_init2 (temp_var_971, config_vals[0]);
    mpfr_init2 (temp_var_972, config_vals[0]);
    mpfr_init2 (temp_var_973, config_vals[0]);
    mpfr_init2 (temp_var_974, config_vals[0]);
    mpfr_init2 (temp_var_975, config_vals[0]);
    mpfr_init2 (temp_var_976, config_vals[0]);
    mpfr_init2 (temp_var_977, config_vals[0]);
    mpfr_init2 (temp_var_978, config_vals[0]);
    mpfr_init2 (temp_var_979, config_vals[0]);
    mpfr_init2 (temp_var_980, config_vals[0]);
    mpfr_init2 (temp_var_981, config_vals[0]);
    mpfr_init2 (temp_var_982, config_vals[0]);
    mpfr_init2 (temp_var_983, config_vals[0]);
    mpfr_init2 (temp_var_984, config_vals[0]);
    mpfr_init2 (temp_var_985, config_vals[0]);
    mpfr_init2 (temp_var_986, config_vals[0]);
    mpfr_init2 (temp_var_987, config_vals[0]);
    mpfr_init2 (temp_var_988, config_vals[0]);
    mpfr_init2 (temp_var_989, config_vals[0]);
    mpfr_init2 (temp_var_990, config_vals[0]);
    mpfr_init2 (temp_var_991, config_vals[0]);
    mpfr_init2 (temp_var_992, config_vals[0]);
    mpfr_init2 (temp_var_993, config_vals[0]);
    mpfr_init2 (temp_var_994, config_vals[0]);
    mpfr_init2 (temp_var_995, config_vals[0]);
    mpfr_init2 (temp_var_996, config_vals[0]);
    mpfr_init2 (temp_var_997, config_vals[0]);
    mpfr_init2 (temp_var_998, config_vals[0]);
    mpfr_init2 (temp_var_999, config_vals[0]);
    mpfr_init2 (temp_var_1000, config_vals[0]);
    mpfr_init2 (temp_var_1001, config_vals[0]);
    mpfr_init2 (temp_var_1002, config_vals[0]);
    mpfr_init2 (temp_var_1003, config_vals[0]);
    mpfr_init2 (temp_var_1004, config_vals[0]);
    mpfr_init2 (temp_var_1005, config_vals[0]);
    mpfr_init2 (temp_var_1006, config_vals[0]);
    mpfr_init2 (temp_var_1007, config_vals[0]);
    mpfr_init2 (temp_var_1008, config_vals[0]);
    mpfr_init2 (temp_var_1009, config_vals[0]);
    mpfr_init2 (temp_var_1010, config_vals[0]);
    mpfr_init2 (temp_var_1011, config_vals[0]);
    mpfr_init2 (temp_var_1012, config_vals[0]);
    mpfr_init2 (temp_var_1013, config_vals[0]);
    mpfr_init2 (temp_var_1014, config_vals[0]);
    mpfr_init2 (temp_var_1015, config_vals[0]);
    mpfr_init2 (temp_var_1016, config_vals[0]);
    mpfr_init2 (temp_var_1017, config_vals[0]);
    mpfr_init2 (temp_var_1018, config_vals[0]);
    mpfr_init2 (temp_var_1019, config_vals[0]);
    mpfr_init2 (temp_var_1020, config_vals[0]);
}

//~ int init_readconfig() {
//~ 
  //~ // For reading precision contents of config_file into array
   //~ FILE *myFile;
     //~ myFile = fopen("config_file.txt", "r");
 //~ 
        //~ if (myFile == NULL) {
					//~ printf("Error Reading File\n");
                //~ exit (0);
                //~ }
 //~ 
        //~ int s;
        //~ for (s = 0; s < LEN; s++) {
            //~ fscanf(myFile, "%d,", &config_vals[s+1]);
                          //~ }
//~ 
        //~ fclose(myFile);
        //~ init_mpfr();
        //~ return 0;             
//~ }

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

void ecc(double timeinst, double *initvalu, int initvalu_offset, double *parameter, int parameter_offset, double *finavalu)
{
  int offset_1;
  int offset_2;
  int offset_3;
  int offset_4;
  int offset_5;
  int offset_6;
  int offset_7;
  int offset_8;
  int offset_9;
  int offset_10;
  int offset_11;
  int offset_12;
  int offset_13;
  int offset_14;
  int offset_15;
  int offset_16;
  int offset_17;
  int offset_18;
  int offset_19;
  int offset_20;
  int offset_21;
  int offset_22;
  int offset_23;
  int offset_24;
  int offset_25;
  int offset_26;
  int offset_27;
  int offset_28;
  int offset_29;
  int offset_30;
  int offset_31;
  int offset_32;
  int offset_33;
  int offset_34;
  int offset_35;
  int offset_36;
  int offset_37;
  int offset_38;
  int offset_39;
  int offset_40;
  int offset_41;
  int offset_42;
  int offset_43;
  int offset_44;
  int offset_45;
  int offset_46;
  int parameter_offset_1;
  int state;
  offset_1 = initvalu_offset;
  offset_2 = initvalu_offset + 1;
  offset_3 = initvalu_offset + 2;
  offset_4 = initvalu_offset + 3;
  offset_5 = initvalu_offset + 4;
  offset_6 = initvalu_offset + 5;
  offset_7 = initvalu_offset + 6;
  offset_8 = initvalu_offset + 7;
  offset_9 = initvalu_offset + 8;
  offset_10 = initvalu_offset + 9;
  offset_11 = initvalu_offset + 10;
  offset_12 = initvalu_offset + 11;
  offset_13 = initvalu_offset + 12;
  offset_14 = initvalu_offset + 13;
  offset_15 = initvalu_offset + 14;
  offset_16 = initvalu_offset + 15;
  offset_17 = initvalu_offset + 16;
  offset_18 = initvalu_offset + 17;
  offset_19 = initvalu_offset + 18;
  offset_20 = initvalu_offset + 19;
  offset_21 = initvalu_offset + 20;
  offset_22 = initvalu_offset + 21;
  offset_23 = initvalu_offset + 22;
  offset_24 = initvalu_offset + 23;
  offset_25 = initvalu_offset + 24;
  offset_26 = initvalu_offset + 25;
  offset_27 = initvalu_offset + 26;
  offset_28 = initvalu_offset + 27;
  offset_29 = initvalu_offset + 28;
  offset_30 = initvalu_offset + 29;
  offset_31 = initvalu_offset + 30;
  offset_32 = initvalu_offset + 31;
  offset_33 = initvalu_offset + 32;
  offset_34 = initvalu_offset + 33;
  offset_35 = initvalu_offset + 34;
  offset_36 = initvalu_offset + 35;
  offset_37 = initvalu_offset + 36;
  offset_38 = initvalu_offset + 37;
  offset_39 = initvalu_offset + 38;
  offset_40 = initvalu_offset + 39;
  offset_41 = initvalu_offset + 40;
  offset_42 = initvalu_offset + 41;
  offset_43 = initvalu_offset + 42;
  offset_44 = initvalu_offset + 43;
  offset_45 = initvalu_offset + 44;
  offset_46 = initvalu_offset + 45;
  parameter_offset_1 = parameter_offset;
  mpfr_set_d(initvalu_1_ecc, initvalu[offset_1], MPFR_RNDN);
  mpfr_set_d(initvalu_2_ecc, initvalu[offset_2], MPFR_RNDN);
  mpfr_set_d(initvalu_3_ecc, initvalu[offset_3], MPFR_RNDN);
  mpfr_set_d(initvalu_4_ecc, initvalu[offset_4], MPFR_RNDN);
  mpfr_set_d(initvalu_5_ecc, initvalu[offset_5], MPFR_RNDN);
  mpfr_set_d(initvalu_6_ecc, initvalu[offset_6], MPFR_RNDN);
  mpfr_set_d(initvalu_7_ecc, initvalu[offset_7], MPFR_RNDN);
  mpfr_set_d(initvalu_8_ecc, initvalu[offset_8], MPFR_RNDN);
  mpfr_set_d(initvalu_9_ecc, initvalu[offset_9], MPFR_RNDN);
  mpfr_set_d(initvalu_10_ecc, initvalu[offset_10], MPFR_RNDN);
  mpfr_set_d(initvalu_11_ecc, initvalu[offset_11], MPFR_RNDN);
  mpfr_set_d(initvalu_12_ecc, initvalu[offset_12], MPFR_RNDN);
  mpfr_set_d(initvalu_13_ecc, initvalu[offset_13], MPFR_RNDN);
  mpfr_set_d(initvalu_14_ecc, initvalu[offset_14], MPFR_RNDN);
  mpfr_set_d(initvalu_15_ecc, initvalu[offset_15], MPFR_RNDN);
  mpfr_set_d(initvalu_16_ecc, initvalu[offset_16], MPFR_RNDN);
  mpfr_set_d(initvalu_17_ecc, initvalu[offset_17], MPFR_RNDN);
  mpfr_set_d(initvalu_18_ecc, initvalu[offset_18], MPFR_RNDN);
  mpfr_set_d(initvalu_19_ecc, initvalu[offset_19], MPFR_RNDN);
  mpfr_set_d(initvalu_20_ecc, initvalu[offset_20], MPFR_RNDN);
  mpfr_set_d(initvalu_21_ecc, initvalu[offset_21], MPFR_RNDN);
  mpfr_set_d(initvalu_22_ecc, initvalu[offset_22], MPFR_RNDN);
  mpfr_set_d(initvalu_23_ecc, initvalu[offset_23], MPFR_RNDN);
  mpfr_set_d(initvalu_24_ecc, initvalu[offset_24], MPFR_RNDN);
  mpfr_set_d(initvalu_25_ecc, initvalu[offset_25], MPFR_RNDN);
  mpfr_set_d(initvalu_26_ecc, initvalu[offset_26], MPFR_RNDN);
  mpfr_set_d(initvalu_27_ecc, initvalu[offset_27], MPFR_RNDN);
  mpfr_set_d(initvalu_28_ecc, initvalu[offset_28], MPFR_RNDN);
  mpfr_set_d(initvalu_29_ecc, initvalu[offset_29], MPFR_RNDN);
  mpfr_set_d(initvalu_30_ecc, initvalu[offset_30], MPFR_RNDN);
  mpfr_set_d(initvalu_31_ecc, initvalu[offset_31], MPFR_RNDN);
  mpfr_set_d(initvalu_32_ecc, initvalu[offset_32], MPFR_RNDN);
  mpfr_set_d(initvalu_33_ecc, initvalu[offset_33], MPFR_RNDN);
  mpfr_set_d(initvalu_34_ecc, initvalu[offset_34], MPFR_RNDN);
  mpfr_set_d(initvalu_35_ecc, initvalu[offset_35], MPFR_RNDN);
  mpfr_set_d(initvalu_36_ecc, initvalu[offset_36], MPFR_RNDN);
  mpfr_set_d(initvalu_37_ecc, initvalu[offset_37], MPFR_RNDN);
  mpfr_set_d(initvalu_38_ecc, initvalu[offset_38], MPFR_RNDN);
  mpfr_set_d(initvalu_39_ecc, initvalu[offset_39], MPFR_RNDN);
  mpfr_set_d(initvalu_40_ecc, initvalu[offset_40], MPFR_RNDN);
  mpfr_set_d(initvalu_41_ecc, initvalu[offset_41], MPFR_RNDN);
  mpfr_set_d(initvalu_42_ecc, initvalu[offset_42], MPFR_RNDN);
  mpfr_set_d(initvalu_43_ecc, initvalu[offset_43], MPFR_RNDN);
  mpfr_set_d(initvalu_44_ecc, initvalu[offset_44], MPFR_RNDN);
  mpfr_set_d(initvalu_45_ecc, initvalu[offset_45], MPFR_RNDN);
  mpfr_set_d(initvalu_46_ecc, initvalu[offset_46], MPFR_RNDN);
  mpfr_set_d(parameter_1_ecc, parameter[parameter_offset_1], MPFR_RNDN);
  mpfr_set_d(pi_ecc, 3.1416, MPFR_RNDN);
  mpfr_set_d(R_ecc, 8314, MPFR_RNDN);
  mpfr_set_d(Frdy_ecc, 96485, MPFR_RNDN);
  mpfr_set_d(Temp_ecc, 310, MPFR_RNDN);
  













































    mpfr_div(temp_var_46, Frdy_ecc, R_ecc, MPFR_RNDN);
    mpfr_div(FoRT_ecc, (temp_var_46), Temp_ecc, MPFR_RNDN);
;
  mpfr_set_d(Cmem_ecc, 1.3810e-10, MPFR_RNDN);
  
    mpfr_sub_si(temp_var_47, Temp_ecc, 310, MPFR_RNDN);
    mpfr_div_si(Qpow_ecc, (temp_var_47), 10, MPFR_RNDN);
;
  mpfr_set_d(cellLength_ecc, 100, MPFR_RNDN);
  mpfr_set_d(cellRadius_ecc, 10.25, MPFR_RNDN);
  mpfr_set_d(junctionLength_ecc, 160e-3, MPFR_RNDN);
  mpfr_set_d(junctionRadius_ecc, 15e-3, MPFR_RNDN);
  mpfr_set_d(distSLcyto_ecc, 0.45, MPFR_RNDN);
  mpfr_set_d(distJuncSL_ecc, 0.5, MPFR_RNDN);
  mpfr_set_d(DcaJuncSL_ecc, 1.64e-6, MPFR_RNDN);
  mpfr_set_d(DcaSLcyto_ecc, 1.22e-6, MPFR_RNDN);
  mpfr_set_d(DnaJuncSL_ecc, 1.09e-5, MPFR_RNDN);
  mpfr_set_d(DnaSLcyto_ecc, 1.79e-5, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_48, pi_ecc, pow(mpfr_get_d(cellRadius_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_mul(temp_var_49, (temp_var_48), cellLength_ecc, MPFR_RNDN);
    mpfr_mul_d(Vcell_ecc, (temp_var_49), 1e-15, MPFR_RNDN);
;
      mpfr_mul_d(Vmyo_ecc, Vcell_ecc, 0.65, MPFR_RNDN);
;
      mpfr_mul_d(Vsr_ecc, Vcell_ecc, 0.035, MPFR_RNDN);
;
      mpfr_mul_d(Vsl_ecc, Vcell_ecc, 0.02, MPFR_RNDN);
;
      mpfr_mul_d(Vjunc_ecc, Vcell_ecc, (0.0539 * 0.01), MPFR_RNDN);
;
  
    mpfr_mul_si(temp_var_50, pi_ecc, 20150, MPFR_RNDN);

    mpfr_mul_si(temp_var_51, (temp_var_50), 2, MPFR_RNDN);

    mpfr_mul(temp_var_52, (temp_var_51), junctionLength_ecc, MPFR_RNDN);
    mpfr_mul(SAjunc_ecc, (temp_var_52), junctionRadius_ecc, MPFR_RNDN);
;
  
    mpfr_mul_si(temp_var_53, pi_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_54, (temp_var_53), cellRadius_ecc, MPFR_RNDN);
    mpfr_mul(SAsl_ecc, (temp_var_54), cellLength_ecc, MPFR_RNDN);
;
  mpfr_set_d(J_ca_juncsl_ecc, 1 / 1.2134e12, MPFR_RNDN);
  mpfr_set_d(J_ca_slmyo_ecc, 1 / 2.68510e11, MPFR_RNDN);
  mpfr_set_d(J_na_juncsl_ecc, 1 / ((1.6382e12 / 3) * 100), MPFR_RNDN);
  mpfr_set_d(J_na_slmyo_ecc, 1 / ((1.8308e10 / 3) * 100), MPFR_RNDN);
  mpfr_set_d(Fjunc_ecc, 0.11, MPFR_RNDN);
      mpfr_si_sub(Fsl_ecc, 1, Fjunc_ecc, MPFR_RNDN);
;
  mpfr_set_d(Fjunc_CaL_ecc, 0.9, MPFR_RNDN);
      mpfr_si_sub(Fsl_CaL_ecc, 1, Fjunc_CaL_ecc, MPFR_RNDN);
;
  mpfr_set_d(Cli_ecc, 15, MPFR_RNDN);
  mpfr_set_d(Clo_ecc, 150, MPFR_RNDN);
  mpfr_set_d(Ko_ecc, 5.4, MPFR_RNDN);
  mpfr_set_d(Nao_ecc, 140, MPFR_RNDN);
  mpfr_set_d(Cao_ecc, 1.8, MPFR_RNDN);
  mpfr_set_d(Mgi_ecc, 1, MPFR_RNDN);
  
    mpfr_si_div(temp_var_55, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_mul_d(ena_junc_ecc, (temp_var_55), log(mpfr_get_d(Nao_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_32_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  
    mpfr_si_div(temp_var_57, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_mul_d(ena_sl_ecc, (temp_var_57), log(mpfr_get_d(Nao_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_33_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  
    mpfr_si_div(temp_var_59, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_mul_d(ek_ecc, (temp_var_59), log(mpfr_get_d(Ko_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_35_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  
    mpfr_si_div(temp_var_61, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_div_si(temp_var_62, (temp_var_61), 2, MPFR_RNDN);

    mpfr_mul_d(eca_junc_ecc, (temp_var_62), log(mpfr_get_d(Cao_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_36_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  
    mpfr_si_div(temp_var_64, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_div_si(temp_var_65, (temp_var_64), 2, MPFR_RNDN);

    mpfr_mul_d(eca_sl_ecc, (temp_var_65), log(mpfr_get_d(Cao_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_37_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  
    mpfr_si_div(temp_var_67, 1, FoRT_ecc, MPFR_RNDN);

    mpfr_mul_d(ecl_ecc, (temp_var_67), log(mpfr_get_d(Cli_ecc, MPFR_RNDN) / mpfr_get_d(Clo_ecc, MPFR_RNDN)), MPFR_RNDN);
;
  mpfr_set_d(GNa_ecc, 16.0, MPFR_RNDN);
  mpfr_set_d(GNaB_ecc, 0.297e-3, MPFR_RNDN);
  mpfr_set_d(IbarNaK_ecc, 1.90719, MPFR_RNDN);
  mpfr_set_d(KmNaip_ecc, 11, MPFR_RNDN);
  mpfr_set_d(KmKo_ecc, 1.5, MPFR_RNDN);
  mpfr_set_d(Q10NaK_ecc, 1.63, MPFR_RNDN);
  mpfr_set_d(Q10KmNai_ecc, 1.39, MPFR_RNDN);
  mpfr_set_d(pNaK_ecc, 0.01833, MPFR_RNDN);
  mpfr_set_d(GtoSlow_ecc, 0.06, MPFR_RNDN);
  mpfr_set_d(GtoFast_ecc, 0.02, MPFR_RNDN);
  mpfr_set_d(gkp_ecc, 0.001, MPFR_RNDN);
  mpfr_set_d(GClCa_ecc, 0.109625, MPFR_RNDN);
  mpfr_set_d(GClB_ecc, 9e-3, MPFR_RNDN);
  mpfr_set_d(KdClCa_ecc, 100e-3, MPFR_RNDN);
  mpfr_set_d(pNa_ecc, 1.5e-8, MPFR_RNDN);
  mpfr_set_d(pCa_ecc, 5.4e-4, MPFR_RNDN);
  mpfr_set_d(pK_ecc, 2.7e-7, MPFR_RNDN);
  mpfr_set_d(KmCa_ecc, 0.6e-3, MPFR_RNDN);
  mpfr_set_d(Q10CaL_ecc, 1.8, MPFR_RNDN);
  mpfr_set_d(IbarNCX_ecc, 9.0, MPFR_RNDN);
  mpfr_set_d(KmCai_ecc, 3.59e-3, MPFR_RNDN);
  mpfr_set_d(KmCao_ecc, 1.3, MPFR_RNDN);
  mpfr_set_d(KmNai_ecc, 12.29, MPFR_RNDN);
  mpfr_set_d(KmNao_ecc, 87.5, MPFR_RNDN);
  mpfr_set_d(ksat_ecc, 0.27, MPFR_RNDN);
  mpfr_set_d(nu_ecc, 0.35, MPFR_RNDN);
  mpfr_set_d(Kdact_ecc, 0.256e-3, MPFR_RNDN);
  mpfr_set_d(Q10NCX_ecc, 1.57, MPFR_RNDN);
  mpfr_set_d(IbarSLCaP_ecc, 0.0673, MPFR_RNDN);
  mpfr_set_d(KmPCa_ecc, 0.5e-3, MPFR_RNDN);
  mpfr_set_d(GCaB_ecc, 2.513e-4, MPFR_RNDN);
  mpfr_set_d(Q10SLCaP_ecc, 2.35, MPFR_RNDN);
  mpfr_set_d(Q10SRCaP_ecc, 2.6, MPFR_RNDN);
  mpfr_set_d(Vmax_SRCaP_ecc, 2.86e-4, MPFR_RNDN);
  mpfr_set_d(Kmf_ecc, 0.246e-3, MPFR_RNDN);
  mpfr_set_d(Kmr_ecc, 1.7, MPFR_RNDN);
  mpfr_set_d(hillSRCaP_ecc, 1.787, MPFR_RNDN);
  mpfr_set_d(ks_ecc, 25, MPFR_RNDN);
  mpfr_set_d(koCa_ecc, 10, MPFR_RNDN);
  mpfr_set_d(kom_ecc, 0.06, MPFR_RNDN);
  mpfr_set_d(kiCa_ecc, 0.5, MPFR_RNDN);
  mpfr_set_d(kim_ecc, 0.005, MPFR_RNDN);
  mpfr_set_d(ec50SR_ecc, 0.45, MPFR_RNDN);
  mpfr_set_d(Bmax_Naj_ecc, 7.561, MPFR_RNDN);
  mpfr_set_d(Bmax_Nasl_ecc, 1.65, MPFR_RNDN);
  mpfr_set_d(koff_na_ecc, 1e-3, MPFR_RNDN);
  mpfr_set_d(kon_na_ecc, 0.1e-3, MPFR_RNDN);
  mpfr_set_d(Bmax_TnClow_ecc, 70e-3, MPFR_RNDN);
  mpfr_set_d(koff_tncl_ecc, 19.6e-3, MPFR_RNDN);
  mpfr_set_d(kon_tncl_ecc, 32.7, MPFR_RNDN);
  mpfr_set_d(Bmax_TnChigh_ecc, 140e-3, MPFR_RNDN);
  mpfr_set_d(koff_tnchca_ecc, 0.032e-3, MPFR_RNDN);
  mpfr_set_d(kon_tnchca_ecc, 2.37, MPFR_RNDN);
  mpfr_set_d(koff_tnchmg_ecc, 3.33e-3, MPFR_RNDN);
  mpfr_set_d(kon_tnchmg_ecc, 3e-3, MPFR_RNDN);
  mpfr_set_d(Bmax_CaM_ecc, 24e-3, MPFR_RNDN);
  mpfr_set_d(koff_cam_ecc, 238e-3, MPFR_RNDN);
  mpfr_set_d(kon_cam_ecc, 34, MPFR_RNDN);
  mpfr_set_d(Bmax_myosin_ecc, 140e-3, MPFR_RNDN);
  mpfr_set_d(koff_myoca_ecc, 0.46e-3, MPFR_RNDN);
  mpfr_set_d(kon_myoca_ecc, 13.8, MPFR_RNDN);
  mpfr_set_d(koff_myomg_ecc, 0.057e-3, MPFR_RNDN);
  mpfr_set_d(kon_myomg_ecc, 0.0157, MPFR_RNDN);
  mpfr_set_d(Bmax_SR_ecc, 19 * 0.9e-3, MPFR_RNDN);
  mpfr_set_d(koff_sr_ecc, 60e-3, MPFR_RNDN);
  mpfr_set_d(kon_sr_ecc, 100, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_69, Vmyo_ecc, 37.38e-3, MPFR_RNDN);
    mpfr_div(Bmax_SLlowsl_ecc, (temp_var_69), Vsl_ecc, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_70, Vmyo_ecc, 4.62e-3, MPFR_RNDN);

    mpfr_div(temp_var_71, (temp_var_70), Vjunc_ecc, MPFR_RNDN);
    mpfr_mul_d(Bmax_SLlowj_ecc, (temp_var_71), 0.1, MPFR_RNDN);
;
  mpfr_set_d(koff_sll_ecc, 1300e-3, MPFR_RNDN);
  mpfr_set_d(kon_sll_ecc, 100, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_72, Vmyo_ecc, 13.35e-3, MPFR_RNDN);
    mpfr_div(Bmax_SLhighsl_ecc, (temp_var_72), Vsl_ecc, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_73, Vmyo_ecc, 1.65e-3, MPFR_RNDN);

    mpfr_div(temp_var_74, (temp_var_73), Vjunc_ecc, MPFR_RNDN);
    mpfr_mul_d(Bmax_SLhighj_ecc, (temp_var_74), 0.1, MPFR_RNDN);
;
  mpfr_set_d(koff_slh_ecc, 30e-3, MPFR_RNDN);
  mpfr_set_d(kon_slh_ecc, 100, MPFR_RNDN);
  mpfr_set_d(Bmax_Csqn_ecc, 2.7, MPFR_RNDN);
  mpfr_set_d(koff_csqn_ecc, 65, MPFR_RNDN);
  mpfr_set_d(kon_csqn_ecc, 100, MPFR_RNDN);
  
    mpfr_add_d(temp_var_75, initvalu_39_ecc, 47.13, MPFR_RNDN);

    mpfr_mul_d(temp_var_76, (temp_var_75), 0.32, MPFR_RNDN);


    mpfr_div_d(am_ecc, (temp_var_76), (1 - exp((-0.1) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 47.13))), MPFR_RNDN);
;
  mpfr_set_d(bm_ecc, 0.08 * exp((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) / 11), MPFR_RNDN);
  if (mpfr_cmp_d (initvalu_39_ecc, (double)(-40)) >=0)
  {
    mpfr_set_d(ah_ecc, 0, MPFR_RNDN);
    mpfr_set_d(aj_ecc, 0, MPFR_RNDN);
    mpfr_set_d(bh_ecc, 1 / (0.13 * (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 10.66)) / 11.1))), MPFR_RNDN);
    mpfr_set_d(bj_ecc, (0.3 * exp((-2.535e-7) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN))) / (1 + exp((-0.1) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 32))), MPFR_RNDN);
  }
  else
  {
    mpfr_set_d(ah_ecc, 0.135 * exp((80 + mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) / (-6.8)), MPFR_RNDN);
    mpfr_set_d(bh_ecc, (3.56 * exp(0.079 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN))) + (3.1e5 * exp(0.35 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN))), MPFR_RNDN);
    

















        mpfr_add_d(temp_var_98, initvalu_39_ecc, 37.78, MPFR_RNDN);

        mpfr_mul_d(temp_var_99, (temp_var_98), (((-127140) * exp(0.2444 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN))) - (3.474e-5 * exp((-0.04391) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)))), MPFR_RNDN);


        mpfr_div_d(aj_ecc, (temp_var_99), (1 + exp(0.311 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 79.23))), MPFR_RNDN);
;
    mpfr_set_d(bj_ecc, (0.1212 * exp((-0.01052) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN))) / (1 + exp((-0.1378) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 40.14))), MPFR_RNDN);
  }

  




    mpfr_si_sub(temp_var_106, 1, initvalu_1_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_107, am_ecc, (temp_var_106), MPFR_RNDN);

    mpfr_mul(temp_var_108, bm_ecc, initvalu_1_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_109, (temp_var_107), (temp_var_108), MPFR_RNDN);

finavalu[offset_1] = mpfr_get_d(temp_var_109, MPFR_RNDN);
  
    mpfr_si_sub(temp_var_110, 1, initvalu_2_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_111, ah_ecc, (temp_var_110), MPFR_RNDN);

    mpfr_mul(temp_var_112, bh_ecc, initvalu_2_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_113, (temp_var_111), (temp_var_112), MPFR_RNDN);

finavalu[offset_2] = mpfr_get_d(temp_var_113, MPFR_RNDN);
  
    mpfr_si_sub(temp_var_114, 1, initvalu_3_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_115, aj_ecc, (temp_var_114), MPFR_RNDN);

    mpfr_mul(temp_var_116, bj_ecc, initvalu_3_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_117, (temp_var_115), (temp_var_116), MPFR_RNDN);

finavalu[offset_3] = mpfr_get_d(temp_var_117, MPFR_RNDN);
  
    mpfr_mul(temp_var_118, Fjunc_ecc, GNa_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_119, (temp_var_118), pow(mpfr_get_d(initvalu_1_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_mul(temp_var_120, (temp_var_119), initvalu_2_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_121, (temp_var_120), initvalu_3_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_122, initvalu_39_ecc, ena_junc_ecc, MPFR_RNDN);
    mpfr_mul(I_Na_junc_ecc, (temp_var_121), (temp_var_122), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_123, Fsl_ecc, GNa_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_124, (temp_var_123), pow(mpfr_get_d(initvalu_1_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_mul(temp_var_125, (temp_var_124), initvalu_2_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_126, (temp_var_125), initvalu_3_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_127, initvalu_39_ecc, ena_sl_ecc, MPFR_RNDN);
    mpfr_mul(I_Na_sl_ecc, (temp_var_126), (temp_var_127), MPFR_RNDN);
;
      mpfr_add(I_Na_ecc, I_Na_junc_ecc, I_Na_sl_ecc, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_128, Fjunc_ecc, GNaB_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_129, initvalu_39_ecc, ena_junc_ecc, MPFR_RNDN);
    mpfr_mul(I_nabk_junc_ecc, (temp_var_128), (temp_var_129), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_130, Fsl_ecc, GNaB_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_131, initvalu_39_ecc, ena_sl_ecc, MPFR_RNDN);
    mpfr_mul(I_nabk_sl_ecc, (temp_var_130), (temp_var_131), MPFR_RNDN);
;
      mpfr_add(I_nabk_ecc, I_nabk_junc_ecc, I_nabk_sl_ecc, MPFR_RNDN);
;
  mpfr_set_d(sigma_ecc, (exp(mpfr_get_d(Nao_ecc, MPFR_RNDN) / 67.3) - 1) / 7, MPFR_RNDN);
  




    mpfr_mul_d(temp_var_136, sigma_ecc, 0.0365, MPFR_RNDN);


    mpfr_mul_d(temp_var_138, (temp_var_136), exp((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_add_d(temp_var_139, (temp_var_138), (1 + (0.1245 * exp(((-0.1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)))), MPFR_RNDN);
    mpfr_si_div(fnak_ecc, 1, (temp_var_139), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_140, Fjunc_ecc, IbarNaK_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_141, (temp_var_140), fnak_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_142, (temp_var_141), Ko_ecc, MPFR_RNDN);



    mpfr_div_d(temp_var_145, (temp_var_142), (1 + pow(mpfr_get_d(KmNaip_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_32_ecc, MPFR_RNDN), 4)), MPFR_RNDN);

    mpfr_add(temp_var_146, Ko_ecc, KmKo_ecc, MPFR_RNDN);
    mpfr_div(I_nak_junc_ecc, (temp_var_145), (temp_var_146), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_147, Fsl_ecc, IbarNaK_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_148, (temp_var_147), fnak_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_149, (temp_var_148), Ko_ecc, MPFR_RNDN);



    mpfr_div_d(temp_var_152, (temp_var_149), (1 + pow(mpfr_get_d(KmNaip_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_33_ecc, MPFR_RNDN), 4)), MPFR_RNDN);

    mpfr_add(temp_var_153, Ko_ecc, KmKo_ecc, MPFR_RNDN);
    mpfr_div(I_nak_sl_ecc, (temp_var_152), (temp_var_153), MPFR_RNDN);
;
      mpfr_add(I_nak_ecc, I_nak_junc_ecc, I_nak_sl_ecc, MPFR_RNDN);
;
  mpfr_set_d(gkr_ecc, 0.03 * sqrt(mpfr_get_d(Ko_ecc, MPFR_RNDN) / 5.4), MPFR_RNDN);
  mpfr_set_d(xrss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 50)) / 7.5)), MPFR_RNDN);
  





    mpfr_add_si(temp_var_159, initvalu_39_ecc, 7, MPFR_RNDN);

    mpfr_mul_d(temp_var_160, (temp_var_159), 0.00138, MPFR_RNDN);



    mpfr_div_d(temp_var_163, (temp_var_160), (1 - exp((-0.123) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 7))), MPFR_RNDN);

    mpfr_add_si(temp_var_164, initvalu_39_ecc, 10, MPFR_RNDN);

    mpfr_mul_d(temp_var_165, (temp_var_164), 6.1e-4, MPFR_RNDN);



    mpfr_div_d(temp_var_168, (temp_var_165), (exp(0.145 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 10)) - 1), MPFR_RNDN);

    mpfr_add(temp_var_169, (temp_var_163), (temp_var_168), MPFR_RNDN);
    mpfr_si_div(tauxr_ecc, 1, (temp_var_169), MPFR_RNDN);
;
  
    mpfr_sub(temp_var_170, xrss_ecc, initvalu_12_ecc, MPFR_RNDN);

    mpfr_div(temp_var_171, (temp_var_170), tauxr_ecc, MPFR_RNDN);

finavalu[offset_12] = mpfr_get_d(temp_var_171, MPFR_RNDN);
  mpfr_set_d(rkr_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 33) / 22.4)), MPFR_RNDN);
  


    mpfr_mul(temp_var_174, gkr_ecc, initvalu_12_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_175, (temp_var_174), rkr_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_176, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_kr_ecc, (temp_var_175), (temp_var_176), MPFR_RNDN);
;
  mpfr_set_d(pcaks_junc_ecc, (-log10(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN))) + 3.0, MPFR_RNDN);
  mpfr_set_d(pcaks_sl_ecc, (-log10(mpfr_get_d(initvalu_37_ecc, MPFR_RNDN))) + 3.0, MPFR_RNDN);
  mpfr_set_d(gks_junc_ecc, 0.07 * (0.057 + (0.19 / (1 + exp(((-7.2) + mpfr_get_d(pcaks_junc_ecc, MPFR_RNDN)) / 0.6)))), MPFR_RNDN);
  mpfr_set_d(gks_sl_ecc, 0.07 * (0.057 + (0.19 / (1 + exp(((-7.2) + mpfr_get_d(pcaks_sl_ecc, MPFR_RNDN)) / 0.6)))), MPFR_RNDN);
  






    mpfr_si_div(temp_var_183, 1, FoRT_ecc, MPFR_RNDN);


    mpfr_mul_d(eks_ecc, (temp_var_183), log((mpfr_get_d(Ko_ecc, MPFR_RNDN) + (mpfr_get_d(pNaK_ecc, MPFR_RNDN) * mpfr_get_d(Nao_ecc, MPFR_RNDN))) / (mpfr_get_d(initvalu_35_ecc, MPFR_RNDN) + (mpfr_get_d(pNaK_ecc, MPFR_RNDN) * mpfr_get_d(initvalu_34_ecc, MPFR_RNDN)))), MPFR_RNDN);
;
  mpfr_set_d(xsss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) - 1.5)) / 16.7)), MPFR_RNDN);
  



    mpfr_add_si(temp_var_189, initvalu_39_ecc, 30, MPFR_RNDN);

    mpfr_mul_d(temp_var_190, (temp_var_189), 7.19e-5, MPFR_RNDN);



    mpfr_div_d(temp_var_193, (temp_var_190), (1 - exp((-0.148) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 30))), MPFR_RNDN);

    mpfr_add_si(temp_var_194, initvalu_39_ecc, 30, MPFR_RNDN);

    mpfr_mul_d(temp_var_195, (temp_var_194), 1.31e-4, MPFR_RNDN);



    mpfr_div_d(temp_var_198, (temp_var_195), (exp(0.0687 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 30)) - 1), MPFR_RNDN);

    mpfr_add(temp_var_199, (temp_var_193), (temp_var_198), MPFR_RNDN);
    mpfr_si_div(tauxs_ecc, 1, (temp_var_199), MPFR_RNDN);
;
  
    mpfr_sub(temp_var_200, xsss_ecc, initvalu_13_ecc, MPFR_RNDN);

    mpfr_div(temp_var_201, (temp_var_200), tauxs_ecc, MPFR_RNDN);

finavalu[offset_13] = mpfr_get_d(temp_var_201, MPFR_RNDN);
  
    mpfr_mul(temp_var_202, Fjunc_ecc, gks_junc_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_203, (temp_var_202), pow(mpfr_get_d(initvalu_12_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_sub(temp_var_204, initvalu_39_ecc, eks_ecc, MPFR_RNDN);
    mpfr_mul(I_ks_junc_ecc, (temp_var_203), (temp_var_204), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_205, Fsl_ecc, gks_sl_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_206, (temp_var_205), pow(mpfr_get_d(initvalu_13_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_sub(temp_var_207, initvalu_39_ecc, eks_ecc, MPFR_RNDN);
    mpfr_mul(I_ks_sl_ecc, (temp_var_206), (temp_var_207), MPFR_RNDN);
;
      mpfr_add(I_ks_ecc, I_ks_junc_ecc, I_ks_sl_ecc, MPFR_RNDN);
;
  mpfr_set_d(kp_kp_ecc, 1 / (1 + exp(7.488 - (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) / 5.98))), MPFR_RNDN);
  


    mpfr_mul(temp_var_210, Fjunc_ecc, gkp_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_211, (temp_var_210), kp_kp_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_212, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_kp_junc_ecc, (temp_var_211), (temp_var_212), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_213, Fsl_ecc, gkp_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_214, (temp_var_213), kp_kp_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_215, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_kp_sl_ecc, (temp_var_214), (temp_var_215), MPFR_RNDN);
;
      mpfr_add(I_kp_ecc, I_kp_junc_ecc, I_kp_sl_ecc, MPFR_RNDN);
;
  mpfr_set_d(xtoss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 3.0)) / 15)), MPFR_RNDN);
  mpfr_set_d(ytoss_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 33.5) / 10)), MPFR_RNDN);
  mpfr_set_d(rtoss_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 33.5) / 10)), MPFR_RNDN);
  mpfr_set_d(tauxtos_ecc, (9 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 3.0) / 15))) + 0.5, MPFR_RNDN);
  mpfr_set_d(tauytos_ecc, (3e3 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 60.0) / 10))) + 30, MPFR_RNDN);
  mpfr_set_d(taurtos_ecc, (2800 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 60.0) / 10))) + 220, MPFR_RNDN);
  













    mpfr_sub(temp_var_229, xtoss_ecc, initvalu_8_ecc, MPFR_RNDN);

    mpfr_div(temp_var_230, (temp_var_229), tauxtos_ecc, MPFR_RNDN);

finavalu[offset_8] = mpfr_get_d(temp_var_230, MPFR_RNDN);
  
    mpfr_sub(temp_var_231, ytoss_ecc, initvalu_9_ecc, MPFR_RNDN);

    mpfr_div(temp_var_232, (temp_var_231), tauytos_ecc, MPFR_RNDN);

finavalu[offset_9] = mpfr_get_d(temp_var_232, MPFR_RNDN);
  
    mpfr_sub(temp_var_233, rtoss_ecc, initvalu_40_ecc, MPFR_RNDN);

    mpfr_div(temp_var_234, (temp_var_233), taurtos_ecc, MPFR_RNDN);

finavalu[offset_40] = mpfr_get_d(temp_var_234, MPFR_RNDN);
  
    mpfr_mul(temp_var_235, GtoSlow_ecc, initvalu_8_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_236, initvalu_40_ecc, 0.5, MPFR_RNDN);

    mpfr_add(temp_var_237, initvalu_9_ecc, (temp_var_236), MPFR_RNDN);

    mpfr_mul(temp_var_238, (temp_var_235), (temp_var_237), MPFR_RNDN);

    mpfr_sub(temp_var_239, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_tos_ecc, (temp_var_238), (temp_var_239), MPFR_RNDN);
;
  mpfr_set_d(tauxtof_ecc, (3.5 * exp((((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) / 30) / 30)) + 1.5, MPFR_RNDN);
  mpfr_set_d(tauytof_ecc, (20.0 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 33.5) / 10))) + 20.0, MPFR_RNDN);
  




    mpfr_sub(temp_var_244, xtoss_ecc, initvalu_10_ecc, MPFR_RNDN);

    mpfr_div(temp_var_245, (temp_var_244), tauxtof_ecc, MPFR_RNDN);

finavalu[offset_10] = mpfr_get_d(temp_var_245, MPFR_RNDN);
  
    mpfr_sub(temp_var_246, ytoss_ecc, initvalu_11_ecc, MPFR_RNDN);

    mpfr_div(temp_var_247, (temp_var_246), tauytof_ecc, MPFR_RNDN);

finavalu[offset_11] = mpfr_get_d(temp_var_247, MPFR_RNDN);
  
    mpfr_mul(temp_var_248, GtoFast_ecc, initvalu_10_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_249, (temp_var_248), initvalu_11_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_250, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_tof_ecc, (temp_var_249), (temp_var_250), MPFR_RNDN);
;
      mpfr_add(I_to_ecc, I_tos_ecc, I_tof_ecc, MPFR_RNDN);
;
  mpfr_set_d(aki_ecc, 1.02 / (1 + exp(0.2385 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) - mpfr_get_d(ek_ecc, MPFR_RNDN)) - 59.215))), MPFR_RNDN);
  mpfr_set_d(bki_ecc, ((0.49124 * exp(0.08032 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 5.476) - mpfr_get_d(ek_ecc, MPFR_RNDN)))) + exp(0.06175 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) - mpfr_get_d(ek_ecc, MPFR_RNDN)) - 594.31))) / (1 + exp((-0.5143) * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) - mpfr_get_d(ek_ecc, MPFR_RNDN)) + 4.753))), MPFR_RNDN);
  







    mpfr_add(temp_var_258, aki_ecc, bki_ecc, MPFR_RNDN);
    mpfr_div(kiss_ecc, aki_ecc, (temp_var_258), MPFR_RNDN);
;
  


    mpfr_mul_d(temp_var_261, kiss_ecc, (0.9 * sqrt(mpfr_get_d(Ko_ecc, MPFR_RNDN) / 5.4)), MPFR_RNDN);

    mpfr_sub(temp_var_262, initvalu_39_ecc, ek_ecc, MPFR_RNDN);
    mpfr_mul(I_ki_ecc, (temp_var_261), (temp_var_262), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_263, Fjunc_ecc, GClCa_ecc, MPFR_RNDN);

    mpfr_div(temp_var_264, KdClCa_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_add_si(temp_var_265, (temp_var_264), 1, MPFR_RNDN);

    mpfr_div(temp_var_266, (temp_var_263), (temp_var_265), MPFR_RNDN);

    mpfr_sub(temp_var_267, initvalu_39_ecc, ecl_ecc, MPFR_RNDN);
    mpfr_mul(I_ClCa_junc_ecc, (temp_var_266), (temp_var_267), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_268, Fsl_ecc, GClCa_ecc, MPFR_RNDN);

    mpfr_div(temp_var_269, KdClCa_ecc, initvalu_37_ecc, MPFR_RNDN);

    mpfr_add_si(temp_var_270, (temp_var_269), 1, MPFR_RNDN);

    mpfr_div(temp_var_271, (temp_var_268), (temp_var_270), MPFR_RNDN);

    mpfr_sub(temp_var_272, initvalu_39_ecc, ecl_ecc, MPFR_RNDN);
    mpfr_mul(I_ClCa_sl_ecc, (temp_var_271), (temp_var_272), MPFR_RNDN);
;
      mpfr_add(I_ClCa_ecc, I_ClCa_junc_ecc, I_ClCa_sl_ecc, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_273, initvalu_39_ecc, ecl_ecc, MPFR_RNDN);
    mpfr_mul(I_Clbk_ecc, GClB_ecc, (temp_var_273), MPFR_RNDN);
;
  mpfr_set_d(dss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 14.5)) / 6.0)), MPFR_RNDN);
  






    mpfr_mul_d(temp_var_280, dss_ecc, (1 - exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 14.5)) / 6.0)), MPFR_RNDN);

    mpfr_add_d(temp_var_281, initvalu_39_ecc, 14.5, MPFR_RNDN);

    mpfr_mul_d(temp_var_282, (temp_var_281), 0.035, MPFR_RNDN);
    mpfr_div(taud_ecc, (temp_var_280), (temp_var_282), MPFR_RNDN);
;
  mpfr_set_d(fss_ecc, (1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 35.06) / 3.6))) + (0.6 / (1 + exp((50 - mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) / 20))), MPFR_RNDN);
  mpfr_set_d(tauf_ecc, 1 / ((0.0197 * exp(-pow(0.0337 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) + 14.5), 2))) + 0.02), MPFR_RNDN);
  






    mpfr_sub(temp_var_289, dss_ecc, initvalu_4_ecc, MPFR_RNDN);

    mpfr_div(temp_var_290, (temp_var_289), taud_ecc, MPFR_RNDN);

finavalu[offset_4] = mpfr_get_d(temp_var_290, MPFR_RNDN);
  
    mpfr_sub(temp_var_291, fss_ecc, initvalu_5_ecc, MPFR_RNDN);

    mpfr_div(temp_var_292, (temp_var_291), tauf_ecc, MPFR_RNDN);

finavalu[offset_5] = mpfr_get_d(temp_var_292, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_293, initvalu_36_ecc, 1.7, MPFR_RNDN);

    mpfr_si_sub(temp_var_294, 1, initvalu_6_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_295, (temp_var_293), (temp_var_294), MPFR_RNDN);

    mpfr_mul_d(temp_var_296, initvalu_6_ecc, 11.9e-3, MPFR_RNDN);

    mpfr_sub(temp_var_297, (temp_var_295), (temp_var_296), MPFR_RNDN);

finavalu[offset_6] = mpfr_get_d(temp_var_297, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_298, initvalu_37_ecc, 1.7, MPFR_RNDN);

    mpfr_si_sub(temp_var_299, 1, initvalu_7_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_300, (temp_var_298), (temp_var_299), MPFR_RNDN);

    mpfr_mul_d(temp_var_301, initvalu_7_ecc, 11.9e-3, MPFR_RNDN);

    mpfr_sub(temp_var_302, (temp_var_300), (temp_var_301), MPFR_RNDN);

finavalu[offset_7] = mpfr_get_d(temp_var_302, MPFR_RNDN);
  
    mpfr_mul_si(temp_var_303, pCa_ecc, 4, MPFR_RNDN);

    mpfr_mul(temp_var_304, initvalu_39_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_305, (temp_var_304), FoRT_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_306, (temp_var_303), (temp_var_305), MPFR_RNDN);

    mpfr_mul_d(temp_var_307, initvalu_36_ecc, 0.341, MPFR_RNDN);


    mpfr_mul_d(temp_var_309, (temp_var_307), exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul_d(temp_var_310, Cao_ecc, 0.341, MPFR_RNDN);

    mpfr_sub(temp_var_311, (temp_var_309), (temp_var_310), MPFR_RNDN);

    mpfr_mul(temp_var_312, (temp_var_306), (temp_var_311), MPFR_RNDN);


    mpfr_div_d(ibarca_j_ecc, (temp_var_312), (exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) - 1), MPFR_RNDN);
;
  
    mpfr_mul_si(temp_var_315, pCa_ecc, 4, MPFR_RNDN);

    mpfr_mul(temp_var_316, initvalu_39_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_317, (temp_var_316), FoRT_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_318, (temp_var_315), (temp_var_317), MPFR_RNDN);

    mpfr_mul_d(temp_var_319, initvalu_37_ecc, 0.341, MPFR_RNDN);


    mpfr_mul_d(temp_var_321, (temp_var_319), exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul_d(temp_var_322, Cao_ecc, 0.341, MPFR_RNDN);

    mpfr_sub(temp_var_323, (temp_var_321), (temp_var_322), MPFR_RNDN);

    mpfr_mul(temp_var_324, (temp_var_318), (temp_var_323), MPFR_RNDN);


    mpfr_div_d(ibarca_sl_ecc, (temp_var_324), (exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) - 1), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_327, initvalu_39_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_328, (temp_var_327), FoRT_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_329, pK_ecc, (temp_var_328), MPFR_RNDN);

    mpfr_mul_d(temp_var_330, initvalu_35_ecc, 0.75, MPFR_RNDN);


    mpfr_mul_d(temp_var_332, (temp_var_330), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul_d(temp_var_333, Ko_ecc, 0.75, MPFR_RNDN);

    mpfr_sub(temp_var_334, (temp_var_332), (temp_var_333), MPFR_RNDN);

    mpfr_mul(temp_var_335, (temp_var_329), (temp_var_334), MPFR_RNDN);


    mpfr_div_d(ibark_ecc, (temp_var_335), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) - 1), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_338, initvalu_39_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_339, (temp_var_338), FoRT_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_340, pNa_ecc, (temp_var_339), MPFR_RNDN);

    mpfr_mul_d(temp_var_341, initvalu_32_ecc, 0.75, MPFR_RNDN);


    mpfr_mul_d(temp_var_343, (temp_var_341), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul_d(temp_var_344, Nao_ecc, 0.75, MPFR_RNDN);

    mpfr_sub(temp_var_345, (temp_var_343), (temp_var_344), MPFR_RNDN);

    mpfr_mul(temp_var_346, (temp_var_340), (temp_var_345), MPFR_RNDN);


    mpfr_div_d(ibarna_j_ecc, (temp_var_346), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) - 1), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_349, initvalu_39_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_350, (temp_var_349), FoRT_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_351, pNa_ecc, (temp_var_350), MPFR_RNDN);

    mpfr_mul_d(temp_var_352, initvalu_33_ecc, 0.75, MPFR_RNDN);


    mpfr_mul_d(temp_var_354, (temp_var_352), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul_d(temp_var_355, Nao_ecc, 0.75, MPFR_RNDN);

    mpfr_sub(temp_var_356, (temp_var_354), (temp_var_355), MPFR_RNDN);

    mpfr_mul(temp_var_357, (temp_var_351), (temp_var_356), MPFR_RNDN);


    mpfr_div_d(ibarna_sl_ecc, (temp_var_357), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDN) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) - 1), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_360, Fjunc_CaL_ecc, ibarca_j_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_361, (temp_var_360), initvalu_4_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_362, (temp_var_361), initvalu_5_ecc, MPFR_RNDN);

    mpfr_si_sub(temp_var_363, 1, initvalu_6_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_364, (temp_var_362), (temp_var_363), MPFR_RNDN);

    mpfr_mul_d(temp_var_365, (temp_var_364), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);
    mpfr_mul_d(I_Ca_junc_ecc, (temp_var_365), 0.45, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_366, Fsl_CaL_ecc, ibarca_sl_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_367, (temp_var_366), initvalu_4_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_368, (temp_var_367), initvalu_5_ecc, MPFR_RNDN);

    mpfr_si_sub(temp_var_369, 1, initvalu_7_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_370, (temp_var_368), (temp_var_369), MPFR_RNDN);

    mpfr_mul_d(temp_var_371, (temp_var_370), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);
    mpfr_mul_d(I_Ca_sl_ecc, (temp_var_371), 0.45, MPFR_RNDN);
;
      mpfr_add(I_Ca_ecc, I_Ca_junc_ecc, I_Ca_sl_ecc, MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_373, (I_Ca_ecc), MPFR_RNDN);
mpfr_mul(temp_var_372, temp_var_373, Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_374, Vmyo_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_375, (temp_var_374), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_376, (temp_var_372), (temp_var_375), MPFR_RNDN);

    mpfr_mul_d(temp_var_377, (temp_var_376), 1e3, MPFR_RNDN);

finavalu[offset_43] = mpfr_get_d(temp_var_377, MPFR_RNDN);
  
    mpfr_mul(temp_var_378, ibark_ecc, initvalu_4_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_379, (temp_var_378), initvalu_5_ecc, MPFR_RNDN);

    mpfr_si_sub(temp_var_380, 1, initvalu_6_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_381, Fjunc_CaL_ecc, (temp_var_380), MPFR_RNDN);

    mpfr_si_sub(temp_var_382, 1, initvalu_7_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_383, Fsl_CaL_ecc, (temp_var_382), MPFR_RNDN);

    mpfr_add(temp_var_384, (temp_var_381), (temp_var_383), MPFR_RNDN);

    mpfr_mul(temp_var_385, (temp_var_379), (temp_var_384), MPFR_RNDN);

    mpfr_mul_d(temp_var_386, (temp_var_385), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);
    mpfr_mul_d(I_CaK_ecc, (temp_var_386), 0.45, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_387, Fjunc_CaL_ecc, ibarna_j_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_388, (temp_var_387), initvalu_4_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_389, (temp_var_388), initvalu_5_ecc, MPFR_RNDN);

    mpfr_si_sub(temp_var_390, 1, initvalu_6_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_391, (temp_var_389), (temp_var_390), MPFR_RNDN);

    mpfr_mul_d(temp_var_392, (temp_var_391), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);
    mpfr_mul_d(I_CaNa_junc_ecc, (temp_var_392), 0.45, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_393, Fsl_CaL_ecc, ibarna_sl_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_394, (temp_var_393), initvalu_4_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_395, (temp_var_394), initvalu_5_ecc, MPFR_RNDN);

    mpfr_si_sub(temp_var_396, 1, initvalu_7_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_397, (temp_var_395), (temp_var_396), MPFR_RNDN);

    mpfr_mul_d(temp_var_398, (temp_var_397), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);
    mpfr_mul_d(I_CaNa_sl_ecc, (temp_var_398), 0.45, MPFR_RNDN);
;
      mpfr_add(I_CaNa_ecc, I_CaNa_junc_ecc, I_CaNa_sl_ecc, MPFR_RNDN);
;
  
    mpfr_add(temp_var_399, I_Ca_ecc, I_CaK_ecc, MPFR_RNDN);
    mpfr_add(I_Catot_ecc, (temp_var_399), I_CaNa_ecc, MPFR_RNDN);
;
  mpfr_set_d(Ka_junc_ecc, 1 / (1 + pow(mpfr_get_d(Kdact_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
  mpfr_set_d(Ka_sl_ecc, 1 / (1 + pow(mpfr_get_d(Kdact_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_37_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
  





    mpfr_mul_d(s1_junc_ecc, Cao_ecc, (exp((mpfr_get_d(nu_ecc, MPFR_RNDN) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) * pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
;
  

    mpfr_mul_d(s1_sl_ecc, Cao_ecc, (exp((mpfr_get_d(nu_ecc, MPFR_RNDN) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) * pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
;
  

    mpfr_mul_d(s2_junc_ecc, initvalu_36_ecc, (exp(((mpfr_get_d(nu_ecc, MPFR_RNDN) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) * pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_410, KmCai_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3), MPFR_RNDN);



    mpfr_mul_d(temp_var_413, (temp_var_410), (1 + pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDN) / mpfr_get_d(KmNai_ecc, MPFR_RNDN), 3)), MPFR_RNDN);

    mpfr_mul_d(temp_var_414, initvalu_36_ecc, pow(mpfr_get_d(KmNao_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_415, (temp_var_413), (temp_var_414), MPFR_RNDN);

    mpfr_mul_d(temp_var_416, Cao_ecc, pow(mpfr_get_d(KmNai_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_div(temp_var_417, initvalu_36_ecc, KmCai_ecc, MPFR_RNDN);

    mpfr_add_si(temp_var_418, (temp_var_417), 1, MPFR_RNDN);

    mpfr_mul(temp_var_419, (temp_var_416), (temp_var_418), MPFR_RNDN);

    mpfr_add(temp_var_420, (temp_var_415), (temp_var_419), MPFR_RNDN);

    mpfr_mul_d(temp_var_421, KmCao_ecc, pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_422, (temp_var_420), (temp_var_421), MPFR_RNDN);

    mpfr_mul_d(temp_var_423, Cao_ecc, pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_424, (temp_var_422), (temp_var_423), MPFR_RNDN);

    mpfr_mul_d(temp_var_425, initvalu_36_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_426, (temp_var_424), (temp_var_425), MPFR_RNDN);


    mpfr_mul_d(temp_var_428, ksat_ecc, exp(((mpfr_get_d(nu_ecc, MPFR_RNDN) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_add_si(temp_var_429, (temp_var_428), 1, MPFR_RNDN);
    mpfr_mul(s3_junc_ecc, (temp_var_426), (temp_var_429), MPFR_RNDN);
;
  

    mpfr_mul_d(s2_sl_ecc, initvalu_37_ecc, (exp(((mpfr_get_d(nu_ecc, MPFR_RNDN) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)) * pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3)), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_432, KmCai_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3), MPFR_RNDN);



    mpfr_mul_d(temp_var_435, (temp_var_432), (1 + pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDN) / mpfr_get_d(KmNai_ecc, MPFR_RNDN), 3)), MPFR_RNDN);

    mpfr_mul_d(temp_var_436, initvalu_37_ecc, pow(mpfr_get_d(KmNao_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_437, (temp_var_435), (temp_var_436), MPFR_RNDN);

    mpfr_mul_d(temp_var_438, Cao_ecc, pow(mpfr_get_d(KmNai_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_div(temp_var_439, initvalu_37_ecc, KmCai_ecc, MPFR_RNDN);

    mpfr_add_si(temp_var_440, (temp_var_439), 1, MPFR_RNDN);

    mpfr_mul(temp_var_441, (temp_var_438), (temp_var_440), MPFR_RNDN);

    mpfr_add(temp_var_442, (temp_var_437), (temp_var_441), MPFR_RNDN);

    mpfr_mul_d(temp_var_443, KmCao_ecc, pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_444, (temp_var_442), (temp_var_443), MPFR_RNDN);

    mpfr_mul_d(temp_var_445, Cao_ecc, pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_446, (temp_var_444), (temp_var_445), MPFR_RNDN);

    mpfr_mul_d(temp_var_447, initvalu_37_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDN), 3), MPFR_RNDN);

    mpfr_add(temp_var_448, (temp_var_446), (temp_var_447), MPFR_RNDN);


    mpfr_mul_d(temp_var_450, ksat_ecc, exp(((mpfr_get_d(nu_ecc, MPFR_RNDN) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDN)) * mpfr_get_d(FoRT_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_add_si(temp_var_451, (temp_var_450), 1, MPFR_RNDN);
    mpfr_mul(s3_sl_ecc, (temp_var_448), (temp_var_451), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_452, Fjunc_ecc, IbarNCX_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_453, (temp_var_452), pow(mpfr_get_d(Q10NCX_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul(temp_var_454, (temp_var_453), Ka_junc_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_455, s1_junc_ecc, s2_junc_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_456, (temp_var_454), (temp_var_455), MPFR_RNDN);
    mpfr_div(I_ncx_junc_ecc, (temp_var_456), s3_junc_ecc, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_457, Fsl_ecc, IbarNCX_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_458, (temp_var_457), pow(mpfr_get_d(Q10NCX_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul(temp_var_459, (temp_var_458), Ka_sl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_460, s1_sl_ecc, s2_sl_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_461, (temp_var_459), (temp_var_460), MPFR_RNDN);
    mpfr_div(I_ncx_sl_ecc, (temp_var_461), s3_sl_ecc, MPFR_RNDN);
;
      mpfr_add(I_ncx_ecc, I_ncx_junc_ecc, I_ncx_sl_ecc, MPFR_RNDN);
;
  
    mpfr_mul_si(temp_var_462, I_ncx_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_463, (temp_var_462), Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_464, Vmyo_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_465, (temp_var_464), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_466, (temp_var_463), (temp_var_465), MPFR_RNDN);

    mpfr_mul_d(temp_var_467, (temp_var_466), 1e3, MPFR_RNDN);

finavalu[offset_45] = mpfr_get_d(temp_var_467, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_468, Fjunc_ecc, pow(mpfr_get_d(Q10SLCaP_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul(temp_var_469, (temp_var_468), IbarSLCaP_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_470, (temp_var_469), pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 1.6), MPFR_RNDN);

    mpfr_div_d(I_pca_junc_ecc, (temp_var_470), (pow(mpfr_get_d(KmPCa_ecc, MPFR_RNDN), 1.6) + pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 1.6)), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_472, Fsl_ecc, pow(mpfr_get_d(Q10SLCaP_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);

    mpfr_mul(temp_var_473, (temp_var_472), IbarSLCaP_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_474, (temp_var_473), pow(mpfr_get_d(initvalu_37_ecc, MPFR_RNDN), 1.6), MPFR_RNDN);

    mpfr_div_d(I_pca_sl_ecc, (temp_var_474), (pow(mpfr_get_d(KmPCa_ecc, MPFR_RNDN), 1.6) + pow(mpfr_get_d(initvalu_37_ecc, MPFR_RNDN), 1.6)), MPFR_RNDN);
;
      mpfr_add(I_pca_ecc, I_pca_junc_ecc, I_pca_sl_ecc, MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_477, (I_pca_ecc), MPFR_RNDN);
mpfr_mul(temp_var_476, temp_var_477, Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_478, Vmyo_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_479, (temp_var_478), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_480, (temp_var_476), (temp_var_479), MPFR_RNDN);

    mpfr_mul_d(temp_var_481, (temp_var_480), 1e3, MPFR_RNDN);

finavalu[offset_44] = mpfr_get_d(temp_var_481, MPFR_RNDN);
  
    mpfr_mul(temp_var_482, Fjunc_ecc, GCaB_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_483, initvalu_39_ecc, eca_junc_ecc, MPFR_RNDN);
    mpfr_mul(I_cabk_junc_ecc, (temp_var_482), (temp_var_483), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_484, Fsl_ecc, GCaB_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_485, initvalu_39_ecc, eca_sl_ecc, MPFR_RNDN);
    mpfr_mul(I_cabk_sl_ecc, (temp_var_484), (temp_var_485), MPFR_RNDN);
;
      mpfr_add(I_cabk_ecc, I_cabk_junc_ecc, I_cabk_sl_ecc, MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_487, (I_cabk_ecc), MPFR_RNDN);
mpfr_mul(temp_var_486, temp_var_487, Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_488, Vmyo_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_489, (temp_var_488), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_490, (temp_var_486), (temp_var_489), MPFR_RNDN);

    mpfr_mul_d(temp_var_491, (temp_var_490), 1e3, MPFR_RNDN);

finavalu[offset_46] = mpfr_get_d(temp_var_491, MPFR_RNDN);
  mpfr_set_d(MaxSR_ecc, 15, MPFR_RNDN);
  mpfr_set_d(MinSR_ecc, 1, MPFR_RNDN);
  
    mpfr_sub(temp_var_492, MaxSR_ecc, MinSR_ecc, MPFR_RNDN);



    mpfr_div_d(temp_var_495, (temp_var_492), (1 + pow(mpfr_get_d(ec50SR_ecc, MPFR_RNDN) / mpfr_get_d(initvalu_31_ecc, MPFR_RNDN), 2.5)), MPFR_RNDN);
    mpfr_sub(kCaSR_ecc, MaxSR_ecc, (temp_var_495), MPFR_RNDN);
;
      mpfr_div(koSRCa_ecc, koCa_ecc, kCaSR_ecc, MPFR_RNDN);
;
      mpfr_mul(kiSRCa_ecc, kiCa_ecc, kCaSR_ecc, MPFR_RNDN);
;
  
    mpfr_si_sub(temp_var_496, 1, initvalu_14_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_497, (temp_var_496), initvalu_15_ecc, MPFR_RNDN);
    mpfr_sub(RI_ecc, (temp_var_497), initvalu_16_ecc, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_498, kim_ecc, RI_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_499, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_500, (temp_var_499), initvalu_14_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_501, (temp_var_498), (temp_var_500), MPFR_RNDN);

    mpfr_mul_d(temp_var_502, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_mul(temp_var_503, (temp_var_502), initvalu_14_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_504, kom_ecc, initvalu_15_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_505, (temp_var_503), (temp_var_504), MPFR_RNDN);

    mpfr_sub(temp_var_506, (temp_var_501), (temp_var_505), MPFR_RNDN);

finavalu[offset_14] = mpfr_get_d(temp_var_506, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_507, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_mul(temp_var_508, (temp_var_507), initvalu_14_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_509, kom_ecc, initvalu_15_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_510, (temp_var_508), (temp_var_509), MPFR_RNDN);

    mpfr_mul(temp_var_511, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_512, (temp_var_511), initvalu_15_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_513, kim_ecc, initvalu_16_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_514, (temp_var_512), (temp_var_513), MPFR_RNDN);

    mpfr_sub(temp_var_515, (temp_var_510), (temp_var_514), MPFR_RNDN);

finavalu[offset_15] = mpfr_get_d(temp_var_515, MPFR_RNDN);
  
    mpfr_mul(temp_var_516, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_517, (temp_var_516), initvalu_15_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_518, kim_ecc, initvalu_16_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_519, (temp_var_517), (temp_var_518), MPFR_RNDN);

    mpfr_mul(temp_var_520, kom_ecc, initvalu_16_ecc, MPFR_RNDN);

    mpfr_mul_d(temp_var_521, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDN), 2), MPFR_RNDN);

    mpfr_mul(temp_var_522, (temp_var_521), RI_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_523, (temp_var_520), (temp_var_522), MPFR_RNDN);

    mpfr_sub(temp_var_524, (temp_var_519), (temp_var_523), MPFR_RNDN);

finavalu[offset_16] = mpfr_get_d(temp_var_524, MPFR_RNDN);
  
    mpfr_mul(temp_var_525, ks_ecc, initvalu_15_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_526, initvalu_31_ecc, initvalu_36_ecc, MPFR_RNDN);
    mpfr_mul(J_SRCarel_ecc, (temp_var_525), (temp_var_526), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_527, Vmax_SRCaP_ecc, pow(mpfr_get_d(Q10SRCaP_ecc, MPFR_RNDN), mpfr_get_d(Qpow_ecc, MPFR_RNDN)), MPFR_RNDN);




    mpfr_mul_d(temp_var_531, (temp_var_527), (pow(mpfr_get_d(initvalu_38_ecc, MPFR_RNDN) / mpfr_get_d(Kmf_ecc, MPFR_RNDN), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDN)) - pow(mpfr_get_d(initvalu_31_ecc, MPFR_RNDN) / mpfr_get_d(Kmr_ecc, MPFR_RNDN), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDN))), MPFR_RNDN);



    mpfr_div_d(J_serca_ecc, (temp_var_531), ((1 + pow(mpfr_get_d(initvalu_38_ecc, MPFR_RNDN) / mpfr_get_d(Kmf_ecc, MPFR_RNDN), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDN))) + pow(mpfr_get_d(initvalu_31_ecc, MPFR_RNDN) / mpfr_get_d(Kmr_ecc, MPFR_RNDN), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDN))), MPFR_RNDN);
;
  
    mpfr_sub(temp_var_535, initvalu_31_ecc, initvalu_36_ecc, MPFR_RNDN);
    mpfr_mul_d(J_SRleak_ecc, (temp_var_535), 5.348e-6, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_536, kon_na_ecc, initvalu_32_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_537, Bmax_Naj_ecc, initvalu_17_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_538, (temp_var_536), (temp_var_537), MPFR_RNDN);

    mpfr_mul(temp_var_539, koff_na_ecc, initvalu_17_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_540, (temp_var_538), (temp_var_539), MPFR_RNDN);

finavalu[offset_17] = mpfr_get_d(temp_var_540, MPFR_RNDN);
  
    mpfr_mul(temp_var_541, kon_na_ecc, initvalu_33_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_542, Bmax_Nasl_ecc, initvalu_18_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_543, (temp_var_541), (temp_var_542), MPFR_RNDN);

    mpfr_mul(temp_var_544, koff_na_ecc, initvalu_18_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_545, (temp_var_543), (temp_var_544), MPFR_RNDN);

finavalu[offset_18] = mpfr_get_d(temp_var_545, MPFR_RNDN);
  
    mpfr_mul(temp_var_546, kon_tncl_ecc, initvalu_38_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_547, Bmax_TnClow_ecc, initvalu_19_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_548, (temp_var_546), (temp_var_547), MPFR_RNDN);

    mpfr_mul(temp_var_549, koff_tncl_ecc, initvalu_19_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_550, (temp_var_548), (temp_var_549), MPFR_RNDN);

finavalu[offset_19] = mpfr_get_d(temp_var_550, MPFR_RNDN);
  
    mpfr_mul(temp_var_551, kon_tnchca_ecc, initvalu_38_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_552, Bmax_TnChigh_ecc, initvalu_20_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_553, (temp_var_552), initvalu_21_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_554, (temp_var_551), (temp_var_553), MPFR_RNDN);

    mpfr_mul(temp_var_555, koff_tnchca_ecc, initvalu_20_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_556, (temp_var_554), (temp_var_555), MPFR_RNDN);

finavalu[offset_20] = mpfr_get_d(temp_var_556, MPFR_RNDN);
  
    mpfr_mul(temp_var_557, kon_tnchmg_ecc, Mgi_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_558, Bmax_TnChigh_ecc, initvalu_20_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_559, (temp_var_558), initvalu_21_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_560, (temp_var_557), (temp_var_559), MPFR_RNDN);

    mpfr_mul(temp_var_561, koff_tnchmg_ecc, initvalu_21_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_562, (temp_var_560), (temp_var_561), MPFR_RNDN);

finavalu[offset_21] = mpfr_get_d(temp_var_562, MPFR_RNDN);
  finavalu[offset_22] = 0;
  
    mpfr_mul(temp_var_563, kon_myoca_ecc, initvalu_38_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_564, Bmax_myosin_ecc, initvalu_23_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_565, (temp_var_564), initvalu_24_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_566, (temp_var_563), (temp_var_565), MPFR_RNDN);

    mpfr_mul(temp_var_567, koff_myoca_ecc, initvalu_23_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_568, (temp_var_566), (temp_var_567), MPFR_RNDN);

finavalu[offset_23] = mpfr_get_d(temp_var_568, MPFR_RNDN);
  
    mpfr_mul(temp_var_569, kon_myomg_ecc, Mgi_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_570, Bmax_myosin_ecc, initvalu_23_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_571, (temp_var_570), initvalu_24_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_572, (temp_var_569), (temp_var_571), MPFR_RNDN);

    mpfr_mul(temp_var_573, koff_myomg_ecc, initvalu_24_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_574, (temp_var_572), (temp_var_573), MPFR_RNDN);

finavalu[offset_24] = mpfr_get_d(temp_var_574, MPFR_RNDN);
  
    mpfr_mul(temp_var_575, kon_sr_ecc, initvalu_38_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_576, Bmax_SR_ecc, initvalu_25_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_577, (temp_var_575), (temp_var_576), MPFR_RNDN);

    mpfr_mul(temp_var_578, koff_sr_ecc, initvalu_25_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_579, (temp_var_577), (temp_var_578), MPFR_RNDN);

finavalu[offset_25] = mpfr_get_d(temp_var_579, MPFR_RNDN);
  
    mpfr_set_d(temp_var_580, finavalu[offset_19], MPFR_RNDN);

    mpfr_set_d(temp_var_581, finavalu[offset_20], MPFR_RNDN);

    mpfr_add(temp_var_582, temp_var_580, temp_var_581, MPFR_RNDN);

    mpfr_set_d(temp_var_583, finavalu[offset_21], MPFR_RNDN);

    mpfr_add(temp_var_584, (temp_var_582), temp_var_583, MPFR_RNDN);

    mpfr_set_d(temp_var_585, finavalu[offset_22], MPFR_RNDN);

    mpfr_add(temp_var_586, (temp_var_584), temp_var_585, MPFR_RNDN);

    mpfr_set_d(temp_var_587, finavalu[offset_23], MPFR_RNDN);

    mpfr_add(temp_var_588, (temp_var_586), temp_var_587, MPFR_RNDN);

    mpfr_set_d(temp_var_589, finavalu[offset_24], MPFR_RNDN);

    mpfr_add(temp_var_590, (temp_var_588), temp_var_589, MPFR_RNDN);

    mpfr_set_d(temp_var_591, finavalu[offset_25], MPFR_RNDN);
    mpfr_add(J_CaB_cytosol_ecc, (temp_var_590), temp_var_591, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_592, kon_sll_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_593, Bmax_SLlowj_ecc, initvalu_26_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_594, (temp_var_592), (temp_var_593), MPFR_RNDN);

    mpfr_mul(temp_var_595, koff_sll_ecc, initvalu_26_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_596, (temp_var_594), (temp_var_595), MPFR_RNDN);

finavalu[offset_26] = mpfr_get_d(temp_var_596, MPFR_RNDN);
  
    mpfr_mul(temp_var_597, kon_sll_ecc, initvalu_37_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_598, Bmax_SLlowsl_ecc, initvalu_27_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_599, (temp_var_597), (temp_var_598), MPFR_RNDN);

    mpfr_mul(temp_var_600, koff_sll_ecc, initvalu_27_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_601, (temp_var_599), (temp_var_600), MPFR_RNDN);

finavalu[offset_27] = mpfr_get_d(temp_var_601, MPFR_RNDN);
  
    mpfr_mul(temp_var_602, kon_slh_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_603, Bmax_SLhighj_ecc, initvalu_28_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_604, (temp_var_602), (temp_var_603), MPFR_RNDN);

    mpfr_mul(temp_var_605, koff_slh_ecc, initvalu_28_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_606, (temp_var_604), (temp_var_605), MPFR_RNDN);

finavalu[offset_28] = mpfr_get_d(temp_var_606, MPFR_RNDN);
  
    mpfr_mul(temp_var_607, kon_slh_ecc, initvalu_37_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_608, Bmax_SLhighsl_ecc, initvalu_29_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_609, (temp_var_607), (temp_var_608), MPFR_RNDN);

    mpfr_mul(temp_var_610, koff_slh_ecc, initvalu_29_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_611, (temp_var_609), (temp_var_610), MPFR_RNDN);

finavalu[offset_29] = mpfr_get_d(temp_var_611, MPFR_RNDN);
  
    mpfr_set_d(temp_var_612, finavalu[offset_26], MPFR_RNDN);

    mpfr_set_d(temp_var_613, finavalu[offset_28], MPFR_RNDN);
    mpfr_add(J_CaB_junction_ecc, temp_var_612, temp_var_613, MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_614, finavalu[offset_27], MPFR_RNDN);

    mpfr_set_d(temp_var_615, finavalu[offset_29], MPFR_RNDN);
    mpfr_add(J_CaB_sl_ecc, temp_var_614, temp_var_615, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_616, kon_csqn_ecc, initvalu_31_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_617, Bmax_Csqn_ecc, initvalu_30_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_618, (temp_var_616), (temp_var_617), MPFR_RNDN);

    mpfr_mul(temp_var_619, koff_csqn_ecc, initvalu_30_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_620, (temp_var_618), (temp_var_619), MPFR_RNDN);

finavalu[offset_30] = mpfr_get_d(temp_var_620, MPFR_RNDN);
      mpfr_si_div(oneovervsr_ecc, 1, Vsr_ecc, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_621, J_serca_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_622, (temp_var_621), oneovervsr_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_623, J_SRleak_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_624, (temp_var_623), oneovervsr_ecc, MPFR_RNDN);

    mpfr_add(temp_var_625, (temp_var_624), J_SRCarel_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_626, (temp_var_622), (temp_var_625), MPFR_RNDN);

    mpfr_set_d(temp_var_627, finavalu[offset_30], MPFR_RNDN);

    mpfr_sub(temp_var_628, (temp_var_626), temp_var_627, MPFR_RNDN);

finavalu[offset_31] = mpfr_get_d(temp_var_628, MPFR_RNDN);
  
    mpfr_add(temp_var_629, I_Na_junc_ecc, I_nabk_junc_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_630, I_ncx_junc_ecc, 3, MPFR_RNDN);

    mpfr_add(temp_var_631, (temp_var_629), (temp_var_630), MPFR_RNDN);

    mpfr_mul_si(temp_var_632, I_nak_junc_ecc, 3, MPFR_RNDN);

    mpfr_add(temp_var_633, (temp_var_631), (temp_var_632), MPFR_RNDN);
    mpfr_add(I_Na_tot_junc_ecc, (temp_var_633), I_CaNa_junc_ecc, MPFR_RNDN);
;
  
    mpfr_add(temp_var_634, I_Na_sl_ecc, I_nabk_sl_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_635, I_ncx_sl_ecc, 3, MPFR_RNDN);

    mpfr_add(temp_var_636, (temp_var_634), (temp_var_635), MPFR_RNDN);

    mpfr_mul_si(temp_var_637, I_nak_sl_ecc, 3, MPFR_RNDN);

    mpfr_add(temp_var_638, (temp_var_636), (temp_var_637), MPFR_RNDN);
    mpfr_add(I_Na_tot_sl_ecc, (temp_var_638), I_CaNa_sl_ecc, MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_640, (I_Na_tot_junc_ecc), MPFR_RNDN);
mpfr_mul(temp_var_639, temp_var_640, Cmem_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_641, Vjunc_ecc, Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_642, (temp_var_639), (temp_var_641), MPFR_RNDN);

    mpfr_div(temp_var_643, J_na_juncsl_ecc, Vjunc_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_644, initvalu_33_ecc, initvalu_32_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_645, (temp_var_643), (temp_var_644), MPFR_RNDN);

    mpfr_add(temp_var_646, (temp_var_642), (temp_var_645), MPFR_RNDN);

    mpfr_set_d(temp_var_647, finavalu[offset_17], MPFR_RNDN);

    mpfr_sub(temp_var_648, (temp_var_646), temp_var_647, MPFR_RNDN);

finavalu[offset_32] = mpfr_get_d(temp_var_648, MPFR_RNDN);
      mpfr_si_div(oneovervsl_ecc, 1, Vsl_ecc, MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_650, (I_Na_tot_sl_ecc), MPFR_RNDN);
mpfr_mul(temp_var_649, temp_var_650, Cmem_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_651, (temp_var_649), oneovervsl_ecc, MPFR_RNDN);

    mpfr_div(temp_var_652, (temp_var_651), Frdy_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_653, J_na_juncsl_ecc, oneovervsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_654, initvalu_32_ecc, initvalu_33_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_655, (temp_var_653), (temp_var_654), MPFR_RNDN);

    mpfr_add(temp_var_656, (temp_var_652), (temp_var_655), MPFR_RNDN);

    mpfr_mul(temp_var_657, J_na_slmyo_ecc, oneovervsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_658, initvalu_34_ecc, initvalu_33_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_659, (temp_var_657), (temp_var_658), MPFR_RNDN);

    mpfr_add(temp_var_660, (temp_var_656), (temp_var_659), MPFR_RNDN);

    mpfr_set_d(temp_var_661, finavalu[offset_18], MPFR_RNDN);

    mpfr_sub(temp_var_662, (temp_var_660), temp_var_661, MPFR_RNDN);

finavalu[offset_33] = mpfr_get_d(temp_var_662, MPFR_RNDN);
  
    mpfr_div(temp_var_663, J_na_slmyo_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_664, initvalu_33_ecc, initvalu_34_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_665, (temp_var_663), (temp_var_664), MPFR_RNDN);

finavalu[offset_34] = mpfr_get_d(temp_var_665, MPFR_RNDN);
  
    mpfr_add(temp_var_666, I_to_ecc, I_kr_ecc, MPFR_RNDN);

    mpfr_add(temp_var_667, (temp_var_666), I_ks_ecc, MPFR_RNDN);

    mpfr_add(temp_var_668, (temp_var_667), I_ki_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_669, I_nak_ecc, 2, MPFR_RNDN);

    mpfr_sub(temp_var_670, (temp_var_668), (temp_var_669), MPFR_RNDN);

    mpfr_add(temp_var_671, (temp_var_670), I_CaK_ecc, MPFR_RNDN);
    mpfr_add(I_K_tot_ecc, (temp_var_671), I_kp_ecc, MPFR_RNDN);
;
  finavalu[offset_35] = 0;
  
    mpfr_add(temp_var_672, I_Ca_junc_ecc, I_cabk_junc_ecc, MPFR_RNDN);

    mpfr_add(temp_var_673, (temp_var_672), I_pca_junc_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_674, I_ncx_junc_ecc, 2, MPFR_RNDN);
    mpfr_sub(I_Ca_tot_junc_ecc, (temp_var_673), (temp_var_674), MPFR_RNDN);
;
  
    mpfr_add(temp_var_675, I_Ca_sl_ecc, I_cabk_sl_ecc, MPFR_RNDN);

    mpfr_add(temp_var_676, (temp_var_675), I_pca_sl_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_677, I_ncx_sl_ecc, 2, MPFR_RNDN);
    mpfr_sub(I_Ca_tot_sl_ecc, (temp_var_676), (temp_var_677), MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_679, (I_Ca_tot_junc_ecc), MPFR_RNDN);
mpfr_mul(temp_var_678, temp_var_679, Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_680, Vjunc_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_681, (temp_var_680), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_682, (temp_var_678), (temp_var_681), MPFR_RNDN);

    mpfr_div(temp_var_683, J_ca_juncsl_ecc, Vjunc_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_684, initvalu_37_ecc, initvalu_36_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_685, (temp_var_683), (temp_var_684), MPFR_RNDN);

    mpfr_add(temp_var_686, (temp_var_682), (temp_var_685), MPFR_RNDN);

    mpfr_sub(temp_var_687, (temp_var_686), J_CaB_junction_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_688, J_SRCarel_ecc, Vsr_ecc, MPFR_RNDN);

    mpfr_div(temp_var_689, (temp_var_688), Vjunc_ecc, MPFR_RNDN);

    mpfr_add(temp_var_690, (temp_var_687), (temp_var_689), MPFR_RNDN);

    mpfr_mul(temp_var_691, J_SRleak_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_div(temp_var_692, (temp_var_691), Vjunc_ecc, MPFR_RNDN);

    mpfr_add(temp_var_693, (temp_var_690), (temp_var_692), MPFR_RNDN);

finavalu[offset_36] = mpfr_get_d(temp_var_693, MPFR_RNDN);
  
    
mpfr_neg (temp_var_695, (I_Ca_tot_sl_ecc), MPFR_RNDN);
mpfr_mul(temp_var_694, temp_var_695, Cmem_ecc, MPFR_RNDN);

    mpfr_mul_si(temp_var_696, Vsl_ecc, 2, MPFR_RNDN);

    mpfr_mul(temp_var_697, (temp_var_696), Frdy_ecc, MPFR_RNDN);

    mpfr_div(temp_var_698, (temp_var_694), (temp_var_697), MPFR_RNDN);

    mpfr_div(temp_var_699, J_ca_juncsl_ecc, Vsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_700, initvalu_36_ecc, initvalu_37_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_701, (temp_var_699), (temp_var_700), MPFR_RNDN);

    mpfr_add(temp_var_702, (temp_var_698), (temp_var_701), MPFR_RNDN);

    mpfr_div(temp_var_703, J_ca_slmyo_ecc, Vsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_704, initvalu_38_ecc, initvalu_37_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_705, (temp_var_703), (temp_var_704), MPFR_RNDN);

    mpfr_add(temp_var_706, (temp_var_702), (temp_var_705), MPFR_RNDN);

    mpfr_sub(temp_var_707, (temp_var_706), J_CaB_sl_ecc, MPFR_RNDN);

finavalu[offset_37] = mpfr_get_d(temp_var_707, MPFR_RNDN);
  
    
mpfr_neg (temp_var_709, (J_serca_ecc), MPFR_RNDN);
mpfr_sub(temp_var_708, temp_var_709, J_CaB_cytosol_ecc, MPFR_RNDN);

    mpfr_div(temp_var_710, J_ca_slmyo_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_711, initvalu_37_ecc, initvalu_38_ecc, MPFR_RNDN);

    mpfr_mul(temp_var_712, (temp_var_710), (temp_var_711), MPFR_RNDN);

    mpfr_add(temp_var_713, (temp_var_708), (temp_var_712), MPFR_RNDN);

finavalu[offset_38] = mpfr_get_d(temp_var_713, MPFR_RNDN);
  
    mpfr_div(temp_var_714, J_ca_juncsl_ecc, Vsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_715, initvalu_36_ecc, initvalu_37_ecc, MPFR_RNDN);
    mpfr_mul(junc_sl_ecc, (temp_var_714), (temp_var_715), MPFR_RNDN);
;
  
    mpfr_div(temp_var_716, J_ca_juncsl_ecc, Vjunc_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_717, initvalu_37_ecc, initvalu_36_ecc, MPFR_RNDN);
    mpfr_mul(sl_junc_ecc, (temp_var_716), (temp_var_717), MPFR_RNDN);
;
  
    mpfr_div(temp_var_718, J_ca_slmyo_ecc, Vsl_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_719, initvalu_38_ecc, initvalu_37_ecc, MPFR_RNDN);
    mpfr_mul(sl_myo_ecc, (temp_var_718), (temp_var_719), MPFR_RNDN);
;
  
    mpfr_div(temp_var_720, J_ca_slmyo_ecc, Vmyo_ecc, MPFR_RNDN);

    mpfr_sub(temp_var_721, initvalu_37_ecc, initvalu_38_ecc, MPFR_RNDN);
    mpfr_mul(myo_sl_ecc, (temp_var_720), (temp_var_721), MPFR_RNDN);
;
  state = 1;
  switch (state)
  {
    case 0:
      mpfr_set_d(I_app_ecc, 0, MPFR_RNDN);
      break;

    case 1:
      if (fmod(timeinst, mpfr_get_d(parameter_1_ecc, MPFR_RNDN)) <= 5)
    {
      mpfr_set_d(I_app_ecc, 9.5, MPFR_RNDN);
    }
    else
    {
      mpfr_set_d(I_app_ecc, 0.0, MPFR_RNDN);
    }

      break;

    case 2:
      mpfr_set_d(V_hold_ecc, -55, MPFR_RNDN);
      mpfr_set_d(V_test_ecc, 0, MPFR_RNDN);
      if ((timeinst > 0.5) & (timeinst < 200.5))
    {
      mpfr_set(V_clamp_ecc, V_test_ecc, MPFR_RNDN);
    }
    else
    {
      mpfr_set(V_clamp_ecc, V_hold_ecc, MPFR_RNDN);
    }

      mpfr_set_d(R_clamp_ecc, 0.04, MPFR_RNDN);
      
        mpfr_sub(temp_var_722, V_clamp_ecc, initvalu_39_ecc, MPFR_RNDN);
        mpfr_div(I_app_ecc, (temp_var_722), R_clamp_ecc, MPFR_RNDN);
;
      break;

  }

      mpfr_add(I_Na_tot_ecc, I_Na_tot_junc_ecc, I_Na_tot_sl_ecc, MPFR_RNDN);
;
      mpfr_add(I_Cl_tot_ecc, I_ClCa_ecc, I_Clbk_ecc, MPFR_RNDN);
;
      mpfr_add(I_Ca_tot_ecc, I_Ca_tot_junc_ecc, I_Ca_tot_sl_ecc, MPFR_RNDN);
;
  
    mpfr_add(temp_var_723, I_Na_tot_ecc, I_Cl_tot_ecc, MPFR_RNDN);

    mpfr_add(temp_var_724, (temp_var_723), I_Ca_tot_ecc, MPFR_RNDN);
    mpfr_add(I_tot_ecc, (temp_var_724), I_K_tot_ecc, MPFR_RNDN);
 
  mpfr_sub(temp_var_725, 	I_app_ecc, I_tot_ecc, MPFR_RNDN );
  finavalu[offset_39] = mpfr_get_d(temp_var_725,MPFR_RNDN);
  finavalu[offset_41] = 0;
  finavalu[offset_42] = 0;
}

double cam(double timeinst, double *initvalu, int initvalu_offset, double *parameter, int parameter_offset, double *finavalu, double Ca)
{
  int offset_1;
  int offset_2;
  int offset_3;
  int offset_4;
  int offset_5;
  int offset_6;
  int offset_7;
  int offset_8;
  int offset_9;
  int offset_10;
  int offset_11;
  int offset_12;
  int offset_13;
  int offset_14;
  int offset_15;
  int parameter_offset_1;
  int parameter_offset_2;
  int parameter_offset_3;
  int parameter_offset_4;
  int parameter_offset_5;
  offset_1 = initvalu_offset;
  offset_2 = initvalu_offset + 1;
  offset_3 = initvalu_offset + 2;
  offset_4 = initvalu_offset + 3;
  offset_5 = initvalu_offset + 4;
  offset_6 = initvalu_offset + 5;
  offset_7 = initvalu_offset + 6;
  offset_8 = initvalu_offset + 7;
  offset_9 = initvalu_offset + 8;
  offset_10 = initvalu_offset + 9;
  offset_11 = initvalu_offset + 10;
  offset_12 = initvalu_offset + 11;
  offset_13 = initvalu_offset + 12;
  offset_14 = initvalu_offset + 13;
  offset_15 = initvalu_offset + 14;
  parameter_offset_1 = parameter_offset;
  parameter_offset_2 = parameter_offset + 1;
  parameter_offset_3 = parameter_offset + 2;
  parameter_offset_4 = parameter_offset + 3;
  parameter_offset_5 = parameter_offset + 4;
  mpfr_set_d(CaM_cam, initvalu[offset_1], MPFR_RNDN);
  mpfr_set_d(Ca2CaM_cam, initvalu[offset_2], MPFR_RNDN);
  mpfr_set_d(Ca4CaM_cam, initvalu[offset_3], MPFR_RNDN);
  mpfr_set_d(CaMB_cam, initvalu[offset_4], MPFR_RNDN);
  mpfr_set_d(Ca2CaMB_cam, initvalu[offset_5], MPFR_RNDN);
  mpfr_set_d(Ca4CaMB_cam, initvalu[offset_6], MPFR_RNDN);
  mpfr_set_d(Pb2_cam, initvalu[offset_7], MPFR_RNDN);
  mpfr_set_d(Pb_cam, initvalu[offset_8], MPFR_RNDN);
  mpfr_set_d(Pt_cam, initvalu[offset_9], MPFR_RNDN);
  mpfr_set_d(Pt2_cam, initvalu[offset_10], MPFR_RNDN);
  mpfr_set_d(Pa_cam, initvalu[offset_11], MPFR_RNDN);
  mpfr_set_d(Ca4CaN_cam, initvalu[offset_12], MPFR_RNDN);
  mpfr_set_d(CaMCa4CaN_cam, initvalu[offset_13], MPFR_RNDN);
  mpfr_set_d(Ca2CaMCa4CaN_cam, initvalu[offset_14], MPFR_RNDN);
  mpfr_set_d(Ca4CaMCa4CaN_cam, initvalu[offset_15], MPFR_RNDN);
  mpfr_set_d(CaMtot_cam, parameter[parameter_offset_1], MPFR_RNDN);
  mpfr_set_d(Btot_cam, parameter[parameter_offset_2], MPFR_RNDN);
  mpfr_set_d(CaMKIItot_cam, parameter[parameter_offset_3], MPFR_RNDN);
  mpfr_set_d(CaNtot_cam, parameter[parameter_offset_4], MPFR_RNDN);
  mpfr_set_d(PP1tot_cam, parameter[parameter_offset_5], MPFR_RNDN);
  mpfr_set_d(K_cam, 135, MPFR_RNDN);
  mpfr_set_d(Mg_cam, 1, MPFR_RNDN);
  if ( mpfr_cmp_d(Mg_cam, 1) <= 0)
  {
    
        mpfr_div_d(temp_var_744, K_cam, 0.94, MPFR_RNDN);

        mpfr_add_si(temp_var_745, (temp_var_744), 1, MPFR_RNDN);

        mpfr_div_d(temp_var_746, Mg_cam, 0.012, MPFR_RNDN);

        mpfr_sub(temp_var_747, (temp_var_745), (temp_var_746), MPFR_RNDN);

        mpfr_mul_d(temp_var_748, (temp_var_747), 0.0025, MPFR_RNDN);

        mpfr_div_d(temp_var_749, K_cam, 8.1, MPFR_RNDN);

        mpfr_add_si(temp_var_750, (temp_var_749), 1, MPFR_RNDN);

        mpfr_div_d(temp_var_751, Mg_cam, 0.022, MPFR_RNDN);

        mpfr_add(temp_var_752, (temp_var_750), (temp_var_751), MPFR_RNDN);
        mpfr_mul(Kd02_cam, (temp_var_748), (temp_var_752), MPFR_RNDN);
;
    
        mpfr_div_d(temp_var_753, K_cam, 0.64, MPFR_RNDN);

        mpfr_add_si(temp_var_754, (temp_var_753), 1, MPFR_RNDN);

        mpfr_div_d(temp_var_755, Mg_cam, 0.0014, MPFR_RNDN);

        mpfr_add(temp_var_756, (temp_var_754), (temp_var_755), MPFR_RNDN);

        mpfr_mul_d(temp_var_757, (temp_var_756), 0.128, MPFR_RNDN);

        mpfr_div_d(temp_var_758, K_cam, 13.0, MPFR_RNDN);

        mpfr_add_si(temp_var_759, (temp_var_758), 1, MPFR_RNDN);

        mpfr_div_d(temp_var_760, Mg_cam, 0.153, MPFR_RNDN);

        mpfr_sub(temp_var_761, (temp_var_759), (temp_var_760), MPFR_RNDN);
        mpfr_mul(Kd24_cam, (temp_var_757), (temp_var_761), MPFR_RNDN);
;
  }
  else
  {
    
        mpfr_div_d(temp_var_762, K_cam, 0.94, MPFR_RNDN);

        mpfr_add_si(temp_var_763, (temp_var_762), 1, MPFR_RNDN);

        mpfr_sub_d(temp_var_764, (temp_var_763), (1 / 0.012), MPFR_RNDN);

        mpfr_sub_si(temp_var_765, Mg_cam, 1, MPFR_RNDN);

        mpfr_div_d(temp_var_766, (temp_var_765), 0.060, MPFR_RNDN);

        mpfr_add(temp_var_767, (temp_var_764), (temp_var_766), MPFR_RNDN);

        mpfr_mul_d(temp_var_768, (temp_var_767), 0.0025, MPFR_RNDN);

        mpfr_div_d(temp_var_769, K_cam, 8.1, MPFR_RNDN);

        mpfr_add_si(temp_var_770, (temp_var_769), 1, MPFR_RNDN);

        mpfr_add_d(temp_var_771, (temp_var_770), (1 / 0.022), MPFR_RNDN);

        mpfr_sub_si(temp_var_772, Mg_cam, 1, MPFR_RNDN);

        mpfr_div_d(temp_var_773, (temp_var_772), 0.068, MPFR_RNDN);

        mpfr_add(temp_var_774, (temp_var_771), (temp_var_773), MPFR_RNDN);
        mpfr_mul(Kd02_cam, (temp_var_768), (temp_var_774), MPFR_RNDN);
;
    
        mpfr_div_d(temp_var_775, K_cam, 0.64, MPFR_RNDN);

        mpfr_add_si(temp_var_776, (temp_var_775), 1, MPFR_RNDN);

        mpfr_add_d(temp_var_777, (temp_var_776), (1 / 0.0014), MPFR_RNDN);

        mpfr_sub_si(temp_var_778, Mg_cam, 1, MPFR_RNDN);

        mpfr_div_d(temp_var_779, (temp_var_778), 0.005, MPFR_RNDN);

        mpfr_add(temp_var_780, (temp_var_777), (temp_var_779), MPFR_RNDN);

        mpfr_mul_d(temp_var_781, (temp_var_780), 0.128, MPFR_RNDN);

        mpfr_div_d(temp_var_782, K_cam, 13.0, MPFR_RNDN);

        mpfr_add_si(temp_var_783, (temp_var_782), 1, MPFR_RNDN);

        mpfr_sub_d(temp_var_784, (temp_var_783), (1 / 0.153), MPFR_RNDN);

        mpfr_sub_si(temp_var_785, Mg_cam, 1, MPFR_RNDN);

        mpfr_div_d(temp_var_786, (temp_var_785), 0.150, MPFR_RNDN);

        mpfr_add(temp_var_787, (temp_var_784), (temp_var_786), MPFR_RNDN);
        mpfr_mul(Kd24_cam, (temp_var_781), (temp_var_787), MPFR_RNDN);
;
  }

  mpfr_set_d(k20_cam, 10, MPFR_RNDN);
      mpfr_div(k02_cam, k20_cam, Kd02_cam, MPFR_RNDN);
;
  mpfr_set_d(k42_cam, 500, MPFR_RNDN);
      mpfr_div(k24_cam, k42_cam, Kd24_cam, MPFR_RNDN);
;
  mpfr_set_d(k0Boff_cam, 0.0014, MPFR_RNDN);
      mpfr_div_d(k0Bon_cam, k0Boff_cam, 0.2, MPFR_RNDN);
;
      mpfr_div_si(k2Boff_cam, k0Boff_cam, 100, MPFR_RNDN);
;
  mpfr_set(k2Bon_cam, k0Bon_cam, MPFR_RNDN);
  mpfr_set(k4Boff_cam, k2Boff_cam, MPFR_RNDN);
  mpfr_set(k4Bon_cam, k0Bon_cam, MPFR_RNDN);
      mpfr_div_si(k20B_cam, k20_cam, 100, MPFR_RNDN);
;
  mpfr_set(k02B_cam, k02_cam, MPFR_RNDN);
  mpfr_set(k42B_cam, k42_cam, MPFR_RNDN);
  mpfr_set(k24B_cam, k24_cam, MPFR_RNDN);
  mpfr_set_d(kbi_cam, 2.2, MPFR_RNDN);
      mpfr_div_d(kib_cam, kbi_cam, 33.5e-3, MPFR_RNDN);
;
  mpfr_set_d(kpp1_cam, 1.72, MPFR_RNDN);
  mpfr_set_d(Kmpp1_cam, 11.5, MPFR_RNDN);
  mpfr_set(kib2_cam, kib_cam, MPFR_RNDN);
      mpfr_mul_si(kb2i_cam, kib2_cam, 5, MPFR_RNDN);
;
  mpfr_set(kb24_cam, k24_cam, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_788, k42_cam, 33.5e-3, MPFR_RNDN);
    mpfr_div_si(kb42_cam, (temp_var_788), 5, MPFR_RNDN);
;
      mpfr_div_si(kta_cam, kbi_cam, 1000, MPFR_RNDN);
;
  mpfr_set(kat_cam, kib_cam, MPFR_RNDN);
  
    mpfr_mul_d(temp_var_789, k42_cam, 33.5e-6, MPFR_RNDN);
    mpfr_div_si(kt42_cam, (temp_var_789), 5, MPFR_RNDN);
;
  mpfr_set(kt24_cam, k24_cam, MPFR_RNDN);
  mpfr_set(kat2_cam, kib_cam, MPFR_RNDN);
      mpfr_mul_si(kt2a_cam, kib_cam, 5, MPFR_RNDN);
;
  mpfr_set_d(kcanCaoff_cam, 1, MPFR_RNDN);
      mpfr_div_d(kcanCaon_cam, kcanCaoff_cam, 0.5, MPFR_RNDN);
;
  mpfr_set_d(kcanCaM4on_cam, 46, MPFR_RNDN);
  mpfr_set_d(kcanCaM4off_cam, 0.0013, MPFR_RNDN);
  mpfr_set(kcanCaM2on_cam, kcanCaM4on_cam, MPFR_RNDN);
      mpfr_mul_si(kcanCaM2off_cam, kcanCaM4off_cam, 2508, MPFR_RNDN);
;
  mpfr_set(kcanCaM0on_cam, kcanCaM4on_cam, MPFR_RNDN);
      mpfr_mul_si(kcanCaM0off_cam, kcanCaM2off_cam, 165, MPFR_RNDN);
;
  mpfr_set(k02can_cam, k02_cam, MPFR_RNDN);
      mpfr_div_si(k20can_cam, k20_cam, 165, MPFR_RNDN);
;
  mpfr_set(k24can_cam, k24_cam, MPFR_RNDN);
      mpfr_div_si(k42can_cam, k20_cam, 2508, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_790, k02_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_791, (temp_var_790), CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_792, k20_cam, Ca2CaM_cam, MPFR_RNDN);
    mpfr_sub(rcn02_cam, (temp_var_791), (temp_var_792), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_793, k24_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_794, (temp_var_793), Ca2CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_795, k42_cam, Ca4CaM_cam, MPFR_RNDN);
    mpfr_sub(rcn24_cam, (temp_var_794), (temp_var_795), MPFR_RNDN);
;
  
    mpfr_sub(temp_var_796, Btot_cam, CaMB_cam, MPFR_RNDN);

    mpfr_sub(temp_var_797, (temp_var_796), Ca2CaMB_cam, MPFR_RNDN);
    mpfr_sub(B_cam, (temp_var_797), Ca4CaMB_cam, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_798, k02B_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_799, (temp_var_798), CaMB_cam, MPFR_RNDN);

    mpfr_mul(temp_var_800, k20B_cam, Ca2CaMB_cam, MPFR_RNDN);
    mpfr_sub(rcn02B_cam, (temp_var_799), (temp_var_800), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_801, k24B_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_802, (temp_var_801), Ca2CaMB_cam, MPFR_RNDN);

    mpfr_mul(temp_var_803, k42B_cam, Ca4CaMB_cam, MPFR_RNDN);
    mpfr_sub(rcn24B_cam, (temp_var_802), (temp_var_803), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_804, k0Bon_cam, CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_805, (temp_var_804), B_cam, MPFR_RNDN);

    mpfr_mul(temp_var_806, k0Boff_cam, CaMB_cam, MPFR_RNDN);
    mpfr_sub(rcn0B_cam, (temp_var_805), (temp_var_806), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_807, k2Bon_cam, Ca2CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_808, (temp_var_807), B_cam, MPFR_RNDN);

    mpfr_mul(temp_var_809, k2Boff_cam, Ca2CaMB_cam, MPFR_RNDN);
    mpfr_sub(rcn2B_cam, (temp_var_808), (temp_var_809), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_810, k4Bon_cam, Ca4CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_811, (temp_var_810), B_cam, MPFR_RNDN);

    mpfr_mul(temp_var_812, k4Boff_cam, Ca4CaMB_cam, MPFR_RNDN);
    mpfr_sub(rcn4B_cam, (temp_var_811), (temp_var_812), MPFR_RNDN);
;
  
    mpfr_sub(temp_var_813, CaNtot_cam, Ca4CaN_cam, MPFR_RNDN);

    mpfr_sub(temp_var_814, (temp_var_813), CaMCa4CaN_cam, MPFR_RNDN);

    mpfr_sub(temp_var_815, (temp_var_814), Ca2CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(Ca2CaN_cam, (temp_var_815), Ca4CaMCa4CaN_cam, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_816, kcanCaon_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_817, (temp_var_816), Ca2CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_818, kcanCaoff_cam, Ca4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcnCa4CaN_cam, (temp_var_817), (temp_var_818), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_819, k02can_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_820, (temp_var_819), CaMCa4CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_821, k20can_cam, Ca2CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcn02CaN_cam, (temp_var_820), (temp_var_821), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_822, k24can_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_823, (temp_var_822), Ca2CaMCa4CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_824, k42can_cam, Ca4CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcn24CaN_cam, (temp_var_823), (temp_var_824), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_825, kcanCaM0on_cam, CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_826, (temp_var_825), Ca4CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_827, kcanCaM0off_cam, CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcn0CaN_cam, (temp_var_826), (temp_var_827), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_828, kcanCaM2on_cam, Ca2CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_829, (temp_var_828), Ca4CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_830, kcanCaM2off_cam, Ca2CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcn2CaN_cam, (temp_var_829), (temp_var_830), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_831, kcanCaM4on_cam, Ca4CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_832, (temp_var_831), Ca4CaN_cam, MPFR_RNDN);

    mpfr_mul(temp_var_833, kcanCaM4off_cam, Ca4CaMCa4CaN_cam, MPFR_RNDN);
    mpfr_sub(rcn4CaN_cam, (temp_var_832), (temp_var_833), MPFR_RNDN);
;
  
    mpfr_si_sub(temp_var_834, 1, Pb2_cam, MPFR_RNDN);

    mpfr_sub(temp_var_835, (temp_var_834), Pb_cam, MPFR_RNDN);

    mpfr_sub(temp_var_836, (temp_var_835), Pt_cam, MPFR_RNDN);

    mpfr_sub(temp_var_837, (temp_var_836), Pt2_cam, MPFR_RNDN);
    mpfr_sub(Pix_cam, (temp_var_837), Pa_cam, MPFR_RNDN);
;
  
    mpfr_mul(temp_var_838, kib2_cam, Ca2CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_839, (temp_var_838), Pix_cam, MPFR_RNDN);

    mpfr_mul(temp_var_840, kb2i_cam, Pb2_cam, MPFR_RNDN);
    mpfr_sub(rcnCKib2_cam, (temp_var_839), (temp_var_840), MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_841, kb24_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_842, (temp_var_841), Pb2_cam, MPFR_RNDN);

    mpfr_mul(temp_var_843, kb42_cam, Pb_cam, MPFR_RNDN);
    mpfr_sub(rcnCKb2b_cam, (temp_var_842), (temp_var_843), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_844, kib_cam, Ca4CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_845, (temp_var_844), Pix_cam, MPFR_RNDN);

    mpfr_mul(temp_var_846, kbi_cam, Pb_cam, MPFR_RNDN);
    mpfr_sub(rcnCKib_cam, (temp_var_845), (temp_var_846), MPFR_RNDN);
;
  
    mpfr_add(temp_var_847, Pb_cam, Pt_cam, MPFR_RNDN);

    mpfr_add(temp_var_848, (temp_var_847), Pt2_cam, MPFR_RNDN);
    mpfr_add(T_cam, (temp_var_848), Pa_cam, MPFR_RNDN);
;
  
    mpfr_mul_d(temp_var_849, T_cam, 0.055, MPFR_RNDN);


    mpfr_add_d(temp_var_851, (temp_var_849), (0.0074 * pow(mpfr_get_d(T_cam, MPFR_RNDN), 2)), MPFR_RNDN);

    mpfr_add_d(kbt_cam, (temp_var_851), (0.015 * pow(mpfr_get_d(T_cam, MPFR_RNDN), 3)), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_853, kbt_cam, Pb_cam, MPFR_RNDN);

    mpfr_mul(temp_var_854, kpp1_cam, PP1tot_cam, MPFR_RNDN);

    mpfr_mul(temp_var_855, (temp_var_854), Pt_cam, MPFR_RNDN);

    mpfr_mul(temp_var_856, CaMKIItot_cam, Pt_cam, MPFR_RNDN);

    mpfr_add(temp_var_857, Kmpp1_cam, (temp_var_856), MPFR_RNDN);

    mpfr_div(temp_var_858, (temp_var_855), (temp_var_857), MPFR_RNDN);
    mpfr_sub(rcnCKbt_cam, (temp_var_853), (temp_var_858), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_859, kt42_cam, Pt_cam, MPFR_RNDN);

    mpfr_mul_d(temp_var_860, kt24_cam, pow(Ca, 2), MPFR_RNDN);

    mpfr_mul(temp_var_861, (temp_var_860), Pt2_cam, MPFR_RNDN);
    mpfr_sub(rcnCKtt2_cam, (temp_var_859), (temp_var_861), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_862, kta_cam, Pt_cam, MPFR_RNDN);

    mpfr_mul(temp_var_863, kat_cam, Ca4CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_864, (temp_var_863), Pa_cam, MPFR_RNDN);
    mpfr_sub(rcnCKta_cam, (temp_var_862), (temp_var_864), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_865, kt2a_cam, Pt2_cam, MPFR_RNDN);

    mpfr_mul(temp_var_866, kat2_cam, Ca2CaM_cam, MPFR_RNDN);

    mpfr_mul(temp_var_867, (temp_var_866), Pa_cam, MPFR_RNDN);
    mpfr_sub(rcnCKt2a_cam, (temp_var_865), (temp_var_867), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_868, kpp1_cam, PP1tot_cam, MPFR_RNDN);

    mpfr_mul(temp_var_869, (temp_var_868), Pt2_cam, MPFR_RNDN);

    mpfr_mul(temp_var_870, CaMKIItot_cam, Pt2_cam, MPFR_RNDN);

    mpfr_add(temp_var_871, Kmpp1_cam, (temp_var_870), MPFR_RNDN);
    mpfr_div(rcnCKt2b2_cam, (temp_var_869), (temp_var_871), MPFR_RNDN);
;
  
    mpfr_mul(temp_var_872, kpp1_cam, PP1tot_cam, MPFR_RNDN);

    mpfr_mul(temp_var_873, (temp_var_872), Pa_cam, MPFR_RNDN);

    mpfr_mul(temp_var_874, CaMKIItot_cam, Pa_cam, MPFR_RNDN);

    mpfr_add(temp_var_875, Kmpp1_cam, (temp_var_874), MPFR_RNDN);
    mpfr_div(rcnCKai_cam, (temp_var_873), (temp_var_875), MPFR_RNDN);
;
  
    
mpfr_neg (temp_var_877, (rcn02_cam), MPFR_RNDN);
mpfr_sub(temp_var_876, temp_var_877, rcn0B_cam, MPFR_RNDN);

    mpfr_sub(temp_var_878, (temp_var_876), rcn0CaN_cam, MPFR_RNDN);
    mpfr_mul_d(dCaM_cam, (temp_var_878), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_879, rcn02_cam, rcn24_cam, MPFR_RNDN);

    mpfr_sub(temp_var_880, (temp_var_879), rcn2B_cam, MPFR_RNDN);

    mpfr_sub(temp_var_881, (temp_var_880), rcn2CaN_cam, MPFR_RNDN);

    
mpfr_neg (temp_var_883, (rcnCKib2_cam), MPFR_RNDN);
mpfr_add(temp_var_882, temp_var_883, rcnCKt2a_cam, MPFR_RNDN);

    mpfr_mul(temp_var_884, CaMKIItot_cam, (temp_var_882), MPFR_RNDN);

    mpfr_add(temp_var_885, (temp_var_881), (temp_var_884), MPFR_RNDN);
    mpfr_mul_d(dCa2CaM_cam, (temp_var_885), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_886, rcn24_cam, rcn4B_cam, MPFR_RNDN);

    mpfr_sub(temp_var_887, (temp_var_886), rcn4CaN_cam, MPFR_RNDN);

    
mpfr_neg (temp_var_889, (rcnCKib_cam), MPFR_RNDN);
mpfr_add(temp_var_888, temp_var_889, rcnCKta_cam, MPFR_RNDN);

    mpfr_mul(temp_var_890, CaMKIItot_cam, (temp_var_888), MPFR_RNDN);

    mpfr_add(temp_var_891, (temp_var_887), (temp_var_890), MPFR_RNDN);
    mpfr_mul_d(dCa4CaM_cam, (temp_var_891), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_892, rcn0B_cam, rcn02B_cam, MPFR_RNDN);
    mpfr_mul_d(dCaMB_cam, (temp_var_892), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_893, rcn02B_cam, rcn2B_cam, MPFR_RNDN);

    mpfr_sub(temp_var_894, (temp_var_893), rcn24B_cam, MPFR_RNDN);
    mpfr_mul_d(dCa2CaMB_cam, (temp_var_894), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_895, rcn24B_cam, rcn4B_cam, MPFR_RNDN);
    mpfr_mul_d(dCa4CaMB_cam, (temp_var_895), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_896, rcnCKib2_cam, rcnCKb2b_cam, MPFR_RNDN);

    mpfr_add(temp_var_897, (temp_var_896), rcnCKt2b2_cam, MPFR_RNDN);
    mpfr_mul_d(dPb2_cam, (temp_var_897), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_898, rcnCKib_cam, rcnCKb2b_cam, MPFR_RNDN);

    mpfr_sub(temp_var_899, (temp_var_898), rcnCKbt_cam, MPFR_RNDN);
    mpfr_mul_d(dPb_cam, (temp_var_899), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_900, rcnCKbt_cam, rcnCKta_cam, MPFR_RNDN);

    mpfr_sub(temp_var_901, (temp_var_900), rcnCKtt2_cam, MPFR_RNDN);
    mpfr_mul_d(dPt_cam, (temp_var_901), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_902, rcnCKtt2_cam, rcnCKt2a_cam, MPFR_RNDN);

    mpfr_sub(temp_var_903, (temp_var_902), rcnCKt2b2_cam, MPFR_RNDN);
    mpfr_mul_d(dPt2_cam, (temp_var_903), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_904, rcnCKta_cam, rcnCKt2a_cam, MPFR_RNDN);

    mpfr_sub(temp_var_905, (temp_var_904), rcnCKai_cam, MPFR_RNDN);
    mpfr_mul_d(dPa_cam, (temp_var_905), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_906, rcnCa4CaN_cam, rcn0CaN_cam, MPFR_RNDN);

    mpfr_sub(temp_var_907, (temp_var_906), rcn2CaN_cam, MPFR_RNDN);

    mpfr_sub(temp_var_908, (temp_var_907), rcn4CaN_cam, MPFR_RNDN);
    mpfr_mul_d(dCa4CaN_cam, (temp_var_908), 1e-3, MPFR_RNDN);
;
  
    mpfr_sub(temp_var_909, rcn0CaN_cam, rcn02CaN_cam, MPFR_RNDN);
    mpfr_mul_d(dCaMCa4CaN_cam, (temp_var_909), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_910, rcn2CaN_cam, rcn02CaN_cam, MPFR_RNDN);

    mpfr_sub(temp_var_911, (temp_var_910), rcn24CaN_cam, MPFR_RNDN);
    mpfr_mul_d(dCa2CaMCa4CaN_cam, (temp_var_911), 1e-3, MPFR_RNDN);
;
  
    mpfr_add(temp_var_912, rcn4CaN_cam, rcn24CaN_cam, MPFR_RNDN);
    mpfr_mul_d(dCa4CaMCa4CaN_cam, (temp_var_912), 1e-3, MPFR_RNDN);
;
  
finavalu[offset_1] = mpfr_get_d(dCaM_cam, MPFR_RNDN);
  
finavalu[offset_2] = mpfr_get_d(dCa2CaM_cam, MPFR_RNDN);
  
finavalu[offset_3] = mpfr_get_d(dCa4CaM_cam, MPFR_RNDN);
  
finavalu[offset_4] = mpfr_get_d(dCaMB_cam, MPFR_RNDN);
  
finavalu[offset_5] = mpfr_get_d(dCa2CaMB_cam, MPFR_RNDN);
  
finavalu[offset_6] = mpfr_get_d(dCa4CaMB_cam, MPFR_RNDN);
  
finavalu[offset_7] = mpfr_get_d(dPb2_cam, MPFR_RNDN);
  
finavalu[offset_8] = mpfr_get_d(dPb_cam, MPFR_RNDN);
  
finavalu[offset_9] = mpfr_get_d(dPt_cam, MPFR_RNDN);
  
finavalu[offset_10] = mpfr_get_d(dPt2_cam, MPFR_RNDN);
  
finavalu[offset_11] = mpfr_get_d(dPa_cam, MPFR_RNDN);
  
finavalu[offset_12] = mpfr_get_d(dCa4CaN_cam, MPFR_RNDN);
  
finavalu[offset_13] = mpfr_get_d(dCaMCa4CaN_cam, MPFR_RNDN);
  
finavalu[offset_14] = mpfr_get_d(dCa2CaMCa4CaN_cam, MPFR_RNDN);
  
finavalu[offset_15] = mpfr_get_d(dCa4CaMCa4CaN_cam, MPFR_RNDN);
  
    mpfr_mul_si(temp_var_913, CaMKIItot_cam, 2, MPFR_RNDN);

    mpfr_sub(temp_var_914, rcnCKtt2_cam, rcnCKb2b_cam, MPFR_RNDN);

    mpfr_mul(temp_var_915, (temp_var_913), (temp_var_914), MPFR_RNDN);

    mpfr_add(temp_var_916, rcn02_cam, rcn24_cam, MPFR_RNDN);

    mpfr_add(temp_var_917, (temp_var_916), rcn02B_cam, MPFR_RNDN);

    mpfr_add(temp_var_918, (temp_var_917), rcn24B_cam, MPFR_RNDN);

    mpfr_add(temp_var_919, (temp_var_918), rcnCa4CaN_cam, MPFR_RNDN);

    mpfr_add(temp_var_920, (temp_var_919), rcn02CaN_cam, MPFR_RNDN);

    mpfr_add(temp_var_921, (temp_var_920), rcn24CaN_cam, MPFR_RNDN);

    mpfr_mul_si(temp_var_922, (temp_var_921), 2, MPFR_RNDN);

    mpfr_sub(temp_var_923, (temp_var_915), (temp_var_922), MPFR_RNDN);
    mpfr_mul_d(JCa_cam, (temp_var_923), 1e-3, MPFR_RNDN);
;
  return mpfr_get_d(JCa_cam, MPFR_RNDN);
}

void fin(double *initvalu, int initvalu_offset_ecc, int initvalu_offset_Dyad, int initvalu_offset_SL, int initvalu_offset_Cyt, double *parameter, double *finavalu, double JCaDyad, double JCaSL, double JCaCyt)
{
  mpfr_set_d(BtotDyad_fin, parameter[2], MPFR_RNDN);
  mpfr_set_d(CaMKIItotDyad_fin, parameter[3], MPFR_RNDN);
  mpfr_set_d(Vmyo_fin, 2.1454e-11, MPFR_RNDN);
  mpfr_set_d(Vdyad_fin, 1.7790e-14, MPFR_RNDN);
  mpfr_set_d(VSL_fin, 6.6013e-13, MPFR_RNDN);
  mpfr_set_d(kDyadSL_fin, 3.6363e-16, MPFR_RNDN);
  mpfr_set_d(kSLmyo_fin, 8.587e-15, MPFR_RNDN);
  mpfr_set_d(k0Boff_fin, 0.0014, MPFR_RNDN);
      mpfr_div_d(k0Bon_fin, k0Boff_fin, 0.2, MPFR_RNDN);
;
      mpfr_div_si(k2Boff_fin, k0Boff_fin, 100, MPFR_RNDN);
;
  mpfr_set(k2Bon_fin, k0Bon_fin, MPFR_RNDN);
  mpfr_set(k4Boff_fin, k2Boff_fin, MPFR_RNDN);
  mpfr_set(k4Bon_fin, k0Bon_fin, MPFR_RNDN);
  
    mpfr_set_d(temp_var_924, JCaDyad, MPFR_RNDN);

    mpfr_mul_d(temp_var_925, temp_var_924, 1e-3, MPFR_RNDN);

    mpfr_set_d(temp_var_926, finavalu[initvalu_offset_ecc + 35], MPFR_RNDN);

    mpfr_add(temp_var_927, temp_var_926, (temp_var_925), MPFR_RNDN);

finavalu[initvalu_offset_ecc + 35] = mpfr_get_d(temp_var_927, MPFR_RNDN);
  
    mpfr_set_d(temp_var_928, JCaSL, MPFR_RNDN);

    mpfr_mul_d(temp_var_929, temp_var_928, 1e-3, MPFR_RNDN);

    mpfr_set_d(temp_var_930, finavalu[initvalu_offset_ecc + 36], MPFR_RNDN);

    mpfr_add(temp_var_931, temp_var_930, (temp_var_929), MPFR_RNDN);

finavalu[initvalu_offset_ecc + 36] = mpfr_get_d(temp_var_931, MPFR_RNDN);
  
    mpfr_set_d(temp_var_932, JCaCyt, MPFR_RNDN);

    mpfr_mul_d(temp_var_933, temp_var_932, 1e-3, MPFR_RNDN);

    mpfr_set_d(temp_var_934, finavalu[initvalu_offset_ecc + 37], MPFR_RNDN);

    mpfr_add(temp_var_935, temp_var_934, (temp_var_933), MPFR_RNDN);

finavalu[initvalu_offset_ecc + 37] = mpfr_get_d(temp_var_935, MPFR_RNDN);
  
    mpfr_set_d(temp_var_936, initvalu[initvalu_offset_Dyad + 0], MPFR_RNDN);

    mpfr_set_d(temp_var_937, initvalu[initvalu_offset_Dyad + 1], MPFR_RNDN);

    mpfr_add(temp_var_938, temp_var_936, temp_var_937, MPFR_RNDN);

    mpfr_set_d(temp_var_939, initvalu[initvalu_offset_Dyad + 2], MPFR_RNDN);

    mpfr_add(temp_var_940, (temp_var_938), temp_var_939, MPFR_RNDN);

    mpfr_set_d(temp_var_941, initvalu[initvalu_offset_Dyad + 3], MPFR_RNDN);

    mpfr_add(temp_var_942, (temp_var_940), temp_var_941, MPFR_RNDN);

    mpfr_set_d(temp_var_943, initvalu[initvalu_offset_Dyad + 4], MPFR_RNDN);

    mpfr_add(temp_var_944, (temp_var_942), temp_var_943, MPFR_RNDN);

    mpfr_set_d(temp_var_945, initvalu[initvalu_offset_Dyad + 5], MPFR_RNDN);

    mpfr_add(temp_var_946, (temp_var_944), temp_var_945, MPFR_RNDN);

    mpfr_set_d(temp_var_947, initvalu[initvalu_offset_Dyad + 6], MPFR_RNDN);

    mpfr_set_d(temp_var_948, initvalu[initvalu_offset_Dyad + 7], MPFR_RNDN);

    mpfr_add(temp_var_949, temp_var_947, temp_var_948, MPFR_RNDN);

    mpfr_set_d(temp_var_950, initvalu[initvalu_offset_Dyad + 8], MPFR_RNDN);

    mpfr_add(temp_var_951, (temp_var_949), temp_var_950, MPFR_RNDN);

    mpfr_set_d(temp_var_952, initvalu[initvalu_offset_Dyad + 9], MPFR_RNDN);

    mpfr_add(temp_var_953, (temp_var_951), temp_var_952, MPFR_RNDN);

    mpfr_mul(temp_var_954, CaMKIItotDyad_fin, (temp_var_953), MPFR_RNDN);

    mpfr_add(temp_var_955, (temp_var_946), (temp_var_954), MPFR_RNDN);

    mpfr_set_d(temp_var_956, initvalu[initvalu_offset_Dyad + 12], MPFR_RNDN);

    mpfr_add(temp_var_957, (temp_var_955), temp_var_956, MPFR_RNDN);

    mpfr_set_d(temp_var_958, initvalu[initvalu_offset_Dyad + 13], MPFR_RNDN);

    mpfr_add(temp_var_959, (temp_var_957), temp_var_958, MPFR_RNDN);

    mpfr_set_d(temp_var_960, initvalu[initvalu_offset_Dyad + 14], MPFR_RNDN);
    mpfr_add(CaMtotDyad_fin, (temp_var_959), temp_var_960, MPFR_RNDN);
;
      mpfr_sub(Bdyad_fin, BtotDyad_fin, CaMtotDyad_fin, MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_961, initvalu[initvalu_offset_Dyad + 0], MPFR_RNDN);

    mpfr_mul(temp_var_962, k0Boff_fin, temp_var_961, MPFR_RNDN);

    mpfr_mul(temp_var_963, k0Bon_fin, Bdyad_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_964, initvalu[initvalu_offset_SL + 0], MPFR_RNDN);

    mpfr_mul(temp_var_965, (temp_var_963), temp_var_964, MPFR_RNDN);

    mpfr_sub(temp_var_966, (temp_var_962), (temp_var_965), MPFR_RNDN);
    mpfr_mul_d(J_cam_dyadSL_fin, (temp_var_966), 1e-3, MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_967, initvalu[initvalu_offset_Dyad + 1], MPFR_RNDN);

    mpfr_mul(temp_var_968, k2Boff_fin, temp_var_967, MPFR_RNDN);

    mpfr_mul(temp_var_969, k2Bon_fin, Bdyad_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_970, initvalu[initvalu_offset_SL + 1], MPFR_RNDN);

    mpfr_mul(temp_var_971, (temp_var_969), temp_var_970, MPFR_RNDN);

    mpfr_sub(temp_var_972, (temp_var_968), (temp_var_971), MPFR_RNDN);
    mpfr_mul_d(J_ca2cam_dyadSL_fin, (temp_var_972), 1e-3, MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_973, initvalu[initvalu_offset_Dyad + 2], MPFR_RNDN);

    mpfr_mul(temp_var_974, k2Boff_fin, temp_var_973, MPFR_RNDN);

    mpfr_mul(temp_var_975, k4Bon_fin, Bdyad_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_976, initvalu[initvalu_offset_SL + 2], MPFR_RNDN);

    mpfr_mul(temp_var_977, (temp_var_975), temp_var_976, MPFR_RNDN);

    mpfr_sub(temp_var_978, (temp_var_974), (temp_var_977), MPFR_RNDN);
    mpfr_mul_d(J_ca4cam_dyadSL_fin, (temp_var_978), 1e-3, MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_979, initvalu[initvalu_offset_SL + 0], MPFR_RNDN);

    mpfr_set_d(temp_var_980, initvalu[initvalu_offset_Cyt + 0], MPFR_RNDN);

    mpfr_sub(temp_var_981, temp_var_979, temp_var_980, MPFR_RNDN);
    mpfr_mul(J_cam_SLmyo_fin, kSLmyo_fin, (temp_var_981), MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_982, initvalu[initvalu_offset_SL + 1], MPFR_RNDN);

    mpfr_set_d(temp_var_983, initvalu[initvalu_offset_Cyt + 1], MPFR_RNDN);

    mpfr_sub(temp_var_984, temp_var_982, temp_var_983, MPFR_RNDN);
    mpfr_mul(J_ca2cam_SLmyo_fin, kSLmyo_fin, (temp_var_984), MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_985, initvalu[initvalu_offset_SL + 2], MPFR_RNDN);

    mpfr_set_d(temp_var_986, initvalu[initvalu_offset_Cyt + 2], MPFR_RNDN);

    mpfr_sub(temp_var_987, temp_var_985, temp_var_986, MPFR_RNDN);
    mpfr_mul(J_ca4cam_SLmyo_fin, kSLmyo_fin, (temp_var_987), MPFR_RNDN);
;
  
    mpfr_set_d(temp_var_988, finavalu[initvalu_offset_Dyad + 0], MPFR_RNDN);

    mpfr_sub(temp_var_989, temp_var_988, J_cam_dyadSL_fin, MPFR_RNDN);

finavalu[initvalu_offset_Dyad + 0] = mpfr_get_d(temp_var_989, MPFR_RNDN);
  
    mpfr_set_d(temp_var_990, finavalu[initvalu_offset_Dyad + 1], MPFR_RNDN);

    mpfr_sub(temp_var_991, temp_var_990, J_ca2cam_dyadSL_fin, MPFR_RNDN);

finavalu[initvalu_offset_Dyad + 1] = mpfr_get_d(temp_var_991, MPFR_RNDN);
  
    mpfr_set_d(temp_var_992, finavalu[initvalu_offset_Dyad + 2], MPFR_RNDN);

    mpfr_sub(temp_var_993, temp_var_992, J_ca4cam_dyadSL_fin, MPFR_RNDN);

finavalu[initvalu_offset_Dyad + 2] = mpfr_get_d(temp_var_993, MPFR_RNDN);
  
    mpfr_mul(temp_var_994, J_cam_dyadSL_fin, Vdyad_fin, MPFR_RNDN);

    mpfr_div(temp_var_995, (temp_var_994), VSL_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_996, finavalu[initvalu_offset_SL + 0], MPFR_RNDN);

    mpfr_add(temp_var_997, temp_var_996, (temp_var_995), MPFR_RNDN);

    mpfr_div(temp_var_998, J_cam_SLmyo_fin, VSL_fin, MPFR_RNDN);

    mpfr_sub(temp_var_999, (temp_var_997), (temp_var_998), MPFR_RNDN);

finavalu[initvalu_offset_SL + 0] = mpfr_get_d(temp_var_999, MPFR_RNDN);
  
    mpfr_mul(temp_var_1000, J_ca2cam_dyadSL_fin, Vdyad_fin, MPFR_RNDN);

    mpfr_div(temp_var_1001, (temp_var_1000), VSL_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_1002, finavalu[initvalu_offset_SL + 1], MPFR_RNDN);

    mpfr_add(temp_var_1003, temp_var_1002, (temp_var_1001), MPFR_RNDN);

    mpfr_div(temp_var_1004, J_ca2cam_SLmyo_fin, VSL_fin, MPFR_RNDN);

    mpfr_sub(temp_var_1005, (temp_var_1003), (temp_var_1004), MPFR_RNDN);

finavalu[initvalu_offset_SL + 1] = mpfr_get_d(temp_var_1005, MPFR_RNDN);
  
    mpfr_mul(temp_var_1006, J_ca4cam_dyadSL_fin, Vdyad_fin, MPFR_RNDN);

    mpfr_div(temp_var_1007, (temp_var_1006), VSL_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_1008, finavalu[initvalu_offset_SL + 2], MPFR_RNDN);

    mpfr_add(temp_var_1009, temp_var_1008, (temp_var_1007), MPFR_RNDN);

    mpfr_div(temp_var_1010, J_ca4cam_SLmyo_fin, VSL_fin, MPFR_RNDN);

    mpfr_sub(temp_var_1011, (temp_var_1009), (temp_var_1010), MPFR_RNDN);

finavalu[initvalu_offset_SL + 2] = mpfr_get_d(temp_var_1011, MPFR_RNDN);
  
    mpfr_div(temp_var_1012, J_cam_SLmyo_fin, Vmyo_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_1013, finavalu[initvalu_offset_Cyt + 0], MPFR_RNDN);

    mpfr_add(temp_var_1014, temp_var_1013, (temp_var_1012), MPFR_RNDN);

finavalu[initvalu_offset_Cyt + 0] = mpfr_get_d(temp_var_1014, MPFR_RNDN);
  
    mpfr_div(temp_var_1015, J_ca2cam_SLmyo_fin, Vmyo_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_1016, finavalu[initvalu_offset_Cyt + 1], MPFR_RNDN);

    mpfr_add(temp_var_1017, temp_var_1016, (temp_var_1015), MPFR_RNDN);

finavalu[initvalu_offset_Cyt + 1] = mpfr_get_d(temp_var_1017, MPFR_RNDN);
  
    mpfr_div(temp_var_1018, J_ca4cam_SLmyo_fin, Vmyo_fin, MPFR_RNDN);

    mpfr_set_d(temp_var_1019, finavalu[initvalu_offset_Cyt + 2], MPFR_RNDN);

    mpfr_add(temp_var_1020, temp_var_1019, (temp_var_1018), MPFR_RNDN);

finavalu[initvalu_offset_Cyt + 2] = mpfr_get_d(temp_var_1020, MPFR_RNDN);
}

//end of conversion, hopefully it will work :)
