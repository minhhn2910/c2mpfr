#include <mpfr.h>
#define LEN 22 
 int config_vals[LEN];
  mpfr_t BtotDyad_fin;
  mpfr_t CaMKIItotDyad_fin;
  mpfr_t Vmyo_fin;
  mpfr_t Vdyad_fin;
  mpfr_t VSL_fin;
  mpfr_t kDyadSL_fin;
  mpfr_t kSLmyo_fin;
  mpfr_t k0Boff_fin;
  mpfr_t k0Bon_fin;
  mpfr_t k2Boff_fin;
  mpfr_t k2Bon_fin;
  mpfr_t k4Boff_fin;
  mpfr_t k4Bon_fin;
  mpfr_t CaMtotDyad_fin;
  mpfr_t Bdyad_fin;
  mpfr_t J_cam_dyadSL_fin;
  mpfr_t J_ca2cam_dyadSL_fin;
  mpfr_t J_ca4cam_dyadSL_fin;
  mpfr_t J_cam_SLmyo_fin;
  mpfr_t J_ca2cam_SLmyo_fin;
  mpfr_t J_ca4cam_SLmyo_fin;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
    mpfr_t temp_var_11;
    mpfr_t temp_var_12;
    mpfr_t temp_var_13;
    mpfr_t temp_var_14;
    mpfr_t temp_var_15;
    mpfr_t temp_var_16;
    mpfr_t temp_var_17;
    mpfr_t temp_var_18;
    mpfr_t temp_var_19;
    mpfr_t temp_var_20;
    mpfr_t temp_var_21;
    mpfr_t temp_var_22;
    mpfr_t temp_var_23;
    mpfr_t temp_var_24;
    mpfr_t temp_var_25;
    mpfr_t temp_var_26;
    mpfr_t temp_var_27;
    mpfr_t temp_var_28;
    mpfr_t temp_var_29;
    mpfr_t temp_var_30;
    mpfr_t temp_var_31;
    mpfr_t temp_var_32;
    mpfr_t temp_var_33;
    mpfr_t temp_var_34;
    mpfr_t temp_var_35;
    mpfr_t temp_var_36;
    mpfr_t temp_var_37;
    mpfr_t temp_var_38;
    mpfr_t temp_var_39;
    mpfr_t temp_var_40;
    mpfr_t temp_var_41;
    mpfr_t temp_var_42;
    mpfr_t temp_var_43;
    mpfr_t temp_var_44;
    mpfr_t temp_var_45;
    mpfr_t temp_var_46;
    mpfr_t temp_var_47;
    mpfr_t temp_var_48;
    mpfr_t temp_var_49;
    mpfr_t temp_var_50;
    mpfr_t temp_var_51;
    mpfr_t temp_var_52;
    mpfr_t temp_var_53;
    mpfr_t temp_var_54;
    mpfr_t temp_var_55;
    mpfr_t temp_var_56;
    mpfr_t temp_var_57;
    mpfr_t temp_var_58;
    mpfr_t temp_var_59;
    mpfr_t temp_var_60;
    mpfr_t temp_var_61;
    mpfr_t temp_var_62;
    mpfr_t temp_var_63;
    mpfr_t temp_var_64;
    mpfr_t temp_var_65;
    mpfr_t temp_var_66;
    mpfr_t temp_var_67;
    mpfr_t temp_var_68;
    mpfr_t temp_var_69;
    mpfr_t temp_var_70;
    mpfr_t temp_var_71;
    mpfr_t temp_var_72;
    mpfr_t temp_var_73;
    mpfr_t temp_var_74;
    mpfr_t temp_var_75;
    mpfr_t temp_var_76;
    mpfr_t temp_var_77;
    mpfr_t temp_var_78;
    mpfr_t temp_var_79;
    mpfr_t temp_var_80;
    mpfr_t temp_var_81;
    mpfr_t temp_var_82;
    mpfr_t temp_var_83;
    mpfr_t temp_var_84;
    mpfr_t temp_var_85;
    mpfr_t temp_var_86;
    mpfr_t temp_var_87;
    mpfr_t temp_var_88;
    mpfr_t temp_var_89;
    mpfr_t temp_var_90;
    mpfr_t temp_var_91;
    mpfr_t temp_var_92;
    mpfr_t temp_var_93;
    mpfr_t temp_var_94;
    mpfr_t temp_var_95;
    mpfr_t temp_var_96;
    mpfr_t temp_var_97;
int init_mpfr() { 
  mpfr_init2(BtotDyad_fin, config_vals[1]);
  mpfr_init2(CaMKIItotDyad_fin, config_vals[2]);
  mpfr_init2(Vmyo_fin, config_vals[3]);
  mpfr_init2(Vdyad_fin, config_vals[4]);
  mpfr_init2(VSL_fin, config_vals[5]);
  mpfr_init2(kDyadSL_fin, config_vals[6]);
  mpfr_init2(kSLmyo_fin, config_vals[7]);
  mpfr_init2(k0Boff_fin, config_vals[8]);
  mpfr_init2(k0Bon_fin, config_vals[9]);
  mpfr_init2(k2Boff_fin, config_vals[10]);
  mpfr_init2(k2Bon_fin, config_vals[11]);
  mpfr_init2(k4Boff_fin, config_vals[12]);
  mpfr_init2(k4Bon_fin, config_vals[13]);
  mpfr_init2(CaMtotDyad_fin, config_vals[14]);
  mpfr_init2(Bdyad_fin, config_vals[15]);
  mpfr_init2(J_cam_dyadSL_fin, config_vals[16]);
  mpfr_init2(J_ca2cam_dyadSL_fin, config_vals[17]);
  mpfr_init2(J_ca4cam_dyadSL_fin, config_vals[18]);
  mpfr_init2(J_cam_SLmyo_fin, config_vals[19]);
  mpfr_init2(J_ca2cam_SLmyo_fin, config_vals[20]);
  mpfr_init2(J_ca4cam_SLmyo_fin, config_vals[21]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
    mpfr_init2 (temp_var_4, config_vals[0]);
    mpfr_init2 (temp_var_5, config_vals[0]);
    mpfr_init2 (temp_var_6, config_vals[0]);
    mpfr_init2 (temp_var_7, config_vals[0]);
    mpfr_init2 (temp_var_8, config_vals[0]);
    mpfr_init2 (temp_var_9, config_vals[0]);
    mpfr_init2 (temp_var_10, config_vals[0]);
    mpfr_init2 (temp_var_11, config_vals[0]);
    mpfr_init2 (temp_var_12, config_vals[0]);
    mpfr_init2 (temp_var_13, config_vals[0]);
    mpfr_init2 (temp_var_14, config_vals[0]);
    mpfr_init2 (temp_var_15, config_vals[0]);
    mpfr_init2 (temp_var_16, config_vals[0]);
    mpfr_init2 (temp_var_17, config_vals[0]);
    mpfr_init2 (temp_var_18, config_vals[0]);
    mpfr_init2 (temp_var_19, config_vals[0]);
    mpfr_init2 (temp_var_20, config_vals[0]);
    mpfr_init2 (temp_var_21, config_vals[0]);
    mpfr_init2 (temp_var_22, config_vals[0]);
    mpfr_init2 (temp_var_23, config_vals[0]);
    mpfr_init2 (temp_var_24, config_vals[0]);
    mpfr_init2 (temp_var_25, config_vals[0]);
    mpfr_init2 (temp_var_26, config_vals[0]);
    mpfr_init2 (temp_var_27, config_vals[0]);
    mpfr_init2 (temp_var_28, config_vals[0]);
    mpfr_init2 (temp_var_29, config_vals[0]);
    mpfr_init2 (temp_var_30, config_vals[0]);
    mpfr_init2 (temp_var_31, config_vals[0]);
    mpfr_init2 (temp_var_32, config_vals[0]);
    mpfr_init2 (temp_var_33, config_vals[0]);
    mpfr_init2 (temp_var_34, config_vals[0]);
    mpfr_init2 (temp_var_35, config_vals[0]);
    mpfr_init2 (temp_var_36, config_vals[0]);
    mpfr_init2 (temp_var_37, config_vals[0]);
    mpfr_init2 (temp_var_38, config_vals[0]);
    mpfr_init2 (temp_var_39, config_vals[0]);
    mpfr_init2 (temp_var_40, config_vals[0]);
    mpfr_init2 (temp_var_41, config_vals[0]);
    mpfr_init2 (temp_var_42, config_vals[0]);
    mpfr_init2 (temp_var_43, config_vals[0]);
    mpfr_init2 (temp_var_44, config_vals[0]);
    mpfr_init2 (temp_var_45, config_vals[0]);
    mpfr_init2 (temp_var_46, config_vals[0]);
    mpfr_init2 (temp_var_47, config_vals[0]);
    mpfr_init2 (temp_var_48, config_vals[0]);
    mpfr_init2 (temp_var_49, config_vals[0]);
    mpfr_init2 (temp_var_50, config_vals[0]);
    mpfr_init2 (temp_var_51, config_vals[0]);
    mpfr_init2 (temp_var_52, config_vals[0]);
    mpfr_init2 (temp_var_53, config_vals[0]);
    mpfr_init2 (temp_var_54, config_vals[0]);
    mpfr_init2 (temp_var_55, config_vals[0]);
    mpfr_init2 (temp_var_56, config_vals[0]);
    mpfr_init2 (temp_var_57, config_vals[0]);
    mpfr_init2 (temp_var_58, config_vals[0]);
    mpfr_init2 (temp_var_59, config_vals[0]);
    mpfr_init2 (temp_var_60, config_vals[0]);
    mpfr_init2 (temp_var_61, config_vals[0]);
    mpfr_init2 (temp_var_62, config_vals[0]);
    mpfr_init2 (temp_var_63, config_vals[0]);
    mpfr_init2 (temp_var_64, config_vals[0]);
    mpfr_init2 (temp_var_65, config_vals[0]);
    mpfr_init2 (temp_var_66, config_vals[0]);
    mpfr_init2 (temp_var_67, config_vals[0]);
    mpfr_init2 (temp_var_68, config_vals[0]);
    mpfr_init2 (temp_var_69, config_vals[0]);
    mpfr_init2 (temp_var_70, config_vals[0]);
    mpfr_init2 (temp_var_71, config_vals[0]);
    mpfr_init2 (temp_var_72, config_vals[0]);
    mpfr_init2 (temp_var_73, config_vals[0]);
    mpfr_init2 (temp_var_74, config_vals[0]);
    mpfr_init2 (temp_var_75, config_vals[0]);
    mpfr_init2 (temp_var_76, config_vals[0]);
    mpfr_init2 (temp_var_77, config_vals[0]);
    mpfr_init2 (temp_var_78, config_vals[0]);
    mpfr_init2 (temp_var_79, config_vals[0]);
    mpfr_init2 (temp_var_80, config_vals[0]);
    mpfr_init2 (temp_var_81, config_vals[0]);
    mpfr_init2 (temp_var_82, config_vals[0]);
    mpfr_init2 (temp_var_83, config_vals[0]);
    mpfr_init2 (temp_var_84, config_vals[0]);
    mpfr_init2 (temp_var_85, config_vals[0]);
    mpfr_init2 (temp_var_86, config_vals[0]);
    mpfr_init2 (temp_var_87, config_vals[0]);
    mpfr_init2 (temp_var_88, config_vals[0]);
    mpfr_init2 (temp_var_89, config_vals[0]);
    mpfr_init2 (temp_var_90, config_vals[0]);
    mpfr_init2 (temp_var_91, config_vals[0]);
    mpfr_init2 (temp_var_92, config_vals[0]);
    mpfr_init2 (temp_var_93, config_vals[0]);
    mpfr_init2 (temp_var_94, config_vals[0]);
    mpfr_init2 (temp_var_95, config_vals[0]);
    mpfr_init2 (temp_var_96, config_vals[0]);
    mpfr_init2 (temp_var_97, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

void fin(float *initvalu, int initvalu_offset_ecc, int initvalu_offset_Dyad, int initvalu_offset_SL, int initvalu_offset_Cyt, float *parameter, float *finavalu, float JCaDyad, float JCaSL, float JCaCyt)
{
  mpfr_set_d(BtotDyad_fin, parameter[2], MPFR_RNDZ);
  mpfr_set_d(CaMKIItotDyad_fin, parameter[3], MPFR_RNDZ);
  mpfr_set_d(Vmyo_fin, 2.1454e-11, MPFR_RNDZ);
  mpfr_set_d(Vdyad_fin, 1.7790e-14, MPFR_RNDZ);
  mpfr_set_d(VSL_fin, 6.6013e-13, MPFR_RNDZ);
  mpfr_set_d(kDyadSL_fin, 3.6363e-16, MPFR_RNDZ);
  mpfr_set_d(kSLmyo_fin, 8.587e-15, MPFR_RNDZ);
  mpfr_set_d(k0Boff_fin, 0.0014, MPFR_RNDZ);
      mpfr_div_d(k0Bon_fin, k0Boff_fin, 0.2, MPFR_RNDZ);
;
      mpfr_div_si(k2Boff_fin, k0Boff_fin, 100, MPFR_RNDZ);
;
  mpfr_set(k2Bon_fin, k0Bon_fin, MPFR_RNDZ);
  mpfr_set(k4Boff_fin, k2Boff_fin, MPFR_RNDZ);
  mpfr_set(k4Bon_fin, k0Bon_fin, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_1, JCaDyad, MPFR_RNDZ);

    mpfr_mul_d(temp_var_2, temp_var_1, 1e-3, MPFR_RNDZ);

    mpfr_set_d(temp_var_3, finavalu[initvalu_offset_ecc + 35], MPFR_RNDZ);

    mpfr_add(temp_var_4, temp_var_3, (temp_var_2), MPFR_RNDZ);

finavalu[initvalu_offset_ecc + 35] = mpfr_get_d(temp_var_4, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_5, JCaSL, MPFR_RNDZ);

    mpfr_mul_d(temp_var_6, temp_var_5, 1e-3, MPFR_RNDZ);

    mpfr_set_d(temp_var_7, finavalu[initvalu_offset_ecc + 36], MPFR_RNDZ);

    mpfr_add(temp_var_8, temp_var_7, (temp_var_6), MPFR_RNDZ);

finavalu[initvalu_offset_ecc + 36] = mpfr_get_d(temp_var_8, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_9, JCaCyt, MPFR_RNDZ);

    mpfr_mul_d(temp_var_10, temp_var_9, 1e-3, MPFR_RNDZ);

    mpfr_set_d(temp_var_11, finavalu[initvalu_offset_ecc + 37], MPFR_RNDZ);

    mpfr_add(temp_var_12, temp_var_11, (temp_var_10), MPFR_RNDZ);

finavalu[initvalu_offset_ecc + 37] = mpfr_get_d(temp_var_12, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_13, initvalu[initvalu_offset_Dyad + 0], MPFR_RNDZ);

    mpfr_set_d(temp_var_14, initvalu[initvalu_offset_Dyad + 1], MPFR_RNDZ);

    mpfr_add(temp_var_15, temp_var_13, temp_var_14, MPFR_RNDZ);

    mpfr_set_d(temp_var_16, initvalu[initvalu_offset_Dyad + 2], MPFR_RNDZ);

    mpfr_add(temp_var_17, (temp_var_15), temp_var_16, MPFR_RNDZ);

    mpfr_set_d(temp_var_18, initvalu[initvalu_offset_Dyad + 3], MPFR_RNDZ);

    mpfr_add(temp_var_19, (temp_var_17), temp_var_18, MPFR_RNDZ);

    mpfr_set_d(temp_var_20, initvalu[initvalu_offset_Dyad + 4], MPFR_RNDZ);

    mpfr_add(temp_var_21, (temp_var_19), temp_var_20, MPFR_RNDZ);

    mpfr_set_d(temp_var_22, initvalu[initvalu_offset_Dyad + 5], MPFR_RNDZ);

    mpfr_add(temp_var_23, (temp_var_21), temp_var_22, MPFR_RNDZ);

    mpfr_set_d(temp_var_24, initvalu[initvalu_offset_Dyad + 6], MPFR_RNDZ);

    mpfr_set_d(temp_var_25, initvalu[initvalu_offset_Dyad + 7], MPFR_RNDZ);

    mpfr_add(temp_var_26, temp_var_24, temp_var_25, MPFR_RNDZ);

    mpfr_set_d(temp_var_27, initvalu[initvalu_offset_Dyad + 8], MPFR_RNDZ);

    mpfr_add(temp_var_28, (temp_var_26), temp_var_27, MPFR_RNDZ);

    mpfr_set_d(temp_var_29, initvalu[initvalu_offset_Dyad + 9], MPFR_RNDZ);

    mpfr_add(temp_var_30, (temp_var_28), temp_var_29, MPFR_RNDZ);

    mpfr_mul(temp_var_31, CaMKIItotDyad_fin, (temp_var_30), MPFR_RNDZ);

    mpfr_add(temp_var_32, (temp_var_23), (temp_var_31), MPFR_RNDZ);

    mpfr_set_d(temp_var_33, initvalu[initvalu_offset_Dyad + 12], MPFR_RNDZ);

    mpfr_add(temp_var_34, (temp_var_32), temp_var_33, MPFR_RNDZ);

    mpfr_set_d(temp_var_35, initvalu[initvalu_offset_Dyad + 13], MPFR_RNDZ);

    mpfr_add(temp_var_36, (temp_var_34), temp_var_35, MPFR_RNDZ);

    mpfr_set_d(temp_var_37, initvalu[initvalu_offset_Dyad + 14], MPFR_RNDZ);
    mpfr_add(CaMtotDyad_fin, (temp_var_36), temp_var_37, MPFR_RNDZ);
;
      mpfr_sub(Bdyad_fin, BtotDyad_fin, CaMtotDyad_fin, MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_38, initvalu[initvalu_offset_Dyad + 0], MPFR_RNDZ);

    mpfr_mul(temp_var_39, k0Boff_fin, temp_var_38, MPFR_RNDZ);

    mpfr_mul(temp_var_40, k0Bon_fin, Bdyad_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_41, initvalu[initvalu_offset_SL + 0], MPFR_RNDZ);

    mpfr_mul(temp_var_42, (temp_var_40), temp_var_41, MPFR_RNDZ);

    mpfr_sub(temp_var_43, (temp_var_39), (temp_var_42), MPFR_RNDZ);
    mpfr_mul_d(J_cam_dyadSL_fin, (temp_var_43), 1e-3, MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_44, initvalu[initvalu_offset_Dyad + 1], MPFR_RNDZ);

    mpfr_mul(temp_var_45, k2Boff_fin, temp_var_44, MPFR_RNDZ);

    mpfr_mul(temp_var_46, k2Bon_fin, Bdyad_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_47, initvalu[initvalu_offset_SL + 1], MPFR_RNDZ);

    mpfr_mul(temp_var_48, (temp_var_46), temp_var_47, MPFR_RNDZ);

    mpfr_sub(temp_var_49, (temp_var_45), (temp_var_48), MPFR_RNDZ);
    mpfr_mul_d(J_ca2cam_dyadSL_fin, (temp_var_49), 1e-3, MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_50, initvalu[initvalu_offset_Dyad + 2], MPFR_RNDZ);

    mpfr_mul(temp_var_51, k2Boff_fin, temp_var_50, MPFR_RNDZ);

    mpfr_mul(temp_var_52, k4Bon_fin, Bdyad_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_53, initvalu[initvalu_offset_SL + 2], MPFR_RNDZ);

    mpfr_mul(temp_var_54, (temp_var_52), temp_var_53, MPFR_RNDZ);

    mpfr_sub(temp_var_55, (temp_var_51), (temp_var_54), MPFR_RNDZ);
    mpfr_mul_d(J_ca4cam_dyadSL_fin, (temp_var_55), 1e-3, MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_56, initvalu[initvalu_offset_SL + 0], MPFR_RNDZ);

    mpfr_set_d(temp_var_57, initvalu[initvalu_offset_Cyt + 0], MPFR_RNDZ);

    mpfr_sub(temp_var_58, temp_var_56, temp_var_57, MPFR_RNDZ);
    mpfr_mul(J_cam_SLmyo_fin, kSLmyo_fin, (temp_var_58), MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_59, initvalu[initvalu_offset_SL + 1], MPFR_RNDZ);

    mpfr_set_d(temp_var_60, initvalu[initvalu_offset_Cyt + 1], MPFR_RNDZ);

    mpfr_sub(temp_var_61, temp_var_59, temp_var_60, MPFR_RNDZ);
    mpfr_mul(J_ca2cam_SLmyo_fin, kSLmyo_fin, (temp_var_61), MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_62, initvalu[initvalu_offset_SL + 2], MPFR_RNDZ);

    mpfr_set_d(temp_var_63, initvalu[initvalu_offset_Cyt + 2], MPFR_RNDZ);

    mpfr_sub(temp_var_64, temp_var_62, temp_var_63, MPFR_RNDZ);
    mpfr_mul(J_ca4cam_SLmyo_fin, kSLmyo_fin, (temp_var_64), MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_65, finavalu[initvalu_offset_Dyad + 0], MPFR_RNDZ);

    mpfr_sub(temp_var_66, temp_var_65, J_cam_dyadSL_fin, MPFR_RNDZ);

finavalu[initvalu_offset_Dyad + 0] = mpfr_get_d(temp_var_66, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_67, finavalu[initvalu_offset_Dyad + 1], MPFR_RNDZ);

    mpfr_sub(temp_var_68, temp_var_67, J_ca2cam_dyadSL_fin, MPFR_RNDZ);

finavalu[initvalu_offset_Dyad + 1] = mpfr_get_d(temp_var_68, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_69, finavalu[initvalu_offset_Dyad + 2], MPFR_RNDZ);

    mpfr_sub(temp_var_70, temp_var_69, J_ca4cam_dyadSL_fin, MPFR_RNDZ);

finavalu[initvalu_offset_Dyad + 2] = mpfr_get_d(temp_var_70, MPFR_RNDZ);
  
    mpfr_mul(temp_var_71, J_cam_dyadSL_fin, Vdyad_fin, MPFR_RNDZ);

    mpfr_div(temp_var_72, (temp_var_71), VSL_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_73, finavalu[initvalu_offset_SL + 0], MPFR_RNDZ);

    mpfr_add(temp_var_74, temp_var_73, (temp_var_72), MPFR_RNDZ);

    mpfr_div(temp_var_75, J_cam_SLmyo_fin, VSL_fin, MPFR_RNDZ);

    mpfr_sub(temp_var_76, (temp_var_74), (temp_var_75), MPFR_RNDZ);

finavalu[initvalu_offset_SL + 0] = mpfr_get_d(temp_var_76, MPFR_RNDZ);
  
    mpfr_mul(temp_var_77, J_ca2cam_dyadSL_fin, Vdyad_fin, MPFR_RNDZ);

    mpfr_div(temp_var_78, (temp_var_77), VSL_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_79, finavalu[initvalu_offset_SL + 1], MPFR_RNDZ);

    mpfr_add(temp_var_80, temp_var_79, (temp_var_78), MPFR_RNDZ);

    mpfr_div(temp_var_81, J_ca2cam_SLmyo_fin, VSL_fin, MPFR_RNDZ);

    mpfr_sub(temp_var_82, (temp_var_80), (temp_var_81), MPFR_RNDZ);

finavalu[initvalu_offset_SL + 1] = mpfr_get_d(temp_var_82, MPFR_RNDZ);
  
    mpfr_mul(temp_var_83, J_ca4cam_dyadSL_fin, Vdyad_fin, MPFR_RNDZ);

    mpfr_div(temp_var_84, (temp_var_83), VSL_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_85, finavalu[initvalu_offset_SL + 2], MPFR_RNDZ);

    mpfr_add(temp_var_86, temp_var_85, (temp_var_84), MPFR_RNDZ);

    mpfr_div(temp_var_87, J_ca4cam_SLmyo_fin, VSL_fin, MPFR_RNDZ);

    mpfr_sub(temp_var_88, (temp_var_86), (temp_var_87), MPFR_RNDZ);

finavalu[initvalu_offset_SL + 2] = mpfr_get_d(temp_var_88, MPFR_RNDZ);
  
    mpfr_div(temp_var_89, J_cam_SLmyo_fin, Vmyo_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_90, finavalu[initvalu_offset_Cyt + 0], MPFR_RNDZ);

    mpfr_add(temp_var_91, temp_var_90, (temp_var_89), MPFR_RNDZ);

finavalu[initvalu_offset_Cyt + 0] = mpfr_get_d(temp_var_91, MPFR_RNDZ);
  
    mpfr_div(temp_var_92, J_ca2cam_SLmyo_fin, Vmyo_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_93, finavalu[initvalu_offset_Cyt + 1], MPFR_RNDZ);

    mpfr_add(temp_var_94, temp_var_93, (temp_var_92), MPFR_RNDZ);

finavalu[initvalu_offset_Cyt + 1] = mpfr_get_d(temp_var_94, MPFR_RNDZ);
  
    mpfr_div(temp_var_95, J_ca4cam_SLmyo_fin, Vmyo_fin, MPFR_RNDZ);

    mpfr_set_d(temp_var_96, finavalu[initvalu_offset_Cyt + 2], MPFR_RNDZ);

    mpfr_add(temp_var_97, temp_var_96, (temp_var_95), MPFR_RNDZ);

finavalu[initvalu_offset_Cyt + 2] = mpfr_get_d(temp_var_97, MPFR_RNDZ);
}

//end of conversion, hopefully it will work :)