#include <math.h>
#include <mpfr.h>
#define LEN 8 
#define MIN_SCALE_FACTOR 0.125
#define MAX_SCALE_FACTOR 4.0
 int config_vals[LEN];
  mpfr_t err_exponent_solver;
  mpfr_t last_interval_solver;
  mpfr_t h_solver;
  mpfr_t h_init_solver;
  mpfr_t tolerance_solver;
  mpfr_t scale_min_solver;
  mpfr_t scale_fina_solver;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
        mpfr_t temp_var_4;
                mpfr_t temp_var_5;
                mpfr_t temp_var_6;
                mpfr_t temp_var_7;
                mpfr_t temp_var_8;
                mpfr_t temp_var_9;
                mpfr_t temp_var_10;
                mpfr_t temp_var_11;
                mpfr_t temp_var_12;
                mpfr_t temp_var_13;
            mpfr_t temp_var_14;
            mpfr_t temp_var_15;
            mpfr_t temp_var_16;
                mpfr_t temp_var_17;
            mpfr_t temp_var_18;
            mpfr_t temp_var_19;
            mpfr_t temp_var_20;
            mpfr_t temp_var_21;
            mpfr_t temp_var_22;
        mpfr_t temp_var_23;
        mpfr_t temp_var_24;
        mpfr_t temp_var_25;
        mpfr_t temp_var_26;
        mpfr_t temp_var_27;
          mpfr_t temp_var_28;      
int init_mpfr() { 
  mpfr_init2(err_exponent_solver, config_vals[0]);
  mpfr_init2(last_interval_solver, config_vals[1]);
  mpfr_init2(h_solver, config_vals[2]);
  mpfr_init2(h_init_solver, config_vals[3]);
  mpfr_init2(tolerance_solver, config_vals[4]);
  mpfr_init2(scale_min_solver, config_vals[5]);
  mpfr_init2(scale_fina_solver, config_vals[6]);
    mpfr_init2 (temp_var_1, config_vals[7]);
    mpfr_init2 (temp_var_2, config_vals[7]);
    mpfr_init2 (temp_var_3, config_vals[7]);
        mpfr_init2 (temp_var_4, config_vals[7]);
                mpfr_init2 (temp_var_5, config_vals[7]);
                mpfr_init2 (temp_var_6, config_vals[7]);
                mpfr_init2 (temp_var_7, config_vals[7]);
                mpfr_init2 (temp_var_8, config_vals[7]);
                mpfr_init2 (temp_var_9, config_vals[7]);
                mpfr_init2 (temp_var_10, config_vals[7]);
                mpfr_init2 (temp_var_11, config_vals[7]);
                mpfr_init2 (temp_var_12, config_vals[7]);
                mpfr_init2 (temp_var_13, config_vals[7]);
            mpfr_init2 (temp_var_14, config_vals[7]);
            mpfr_init2 (temp_var_15, config_vals[7]);
            mpfr_init2 (temp_var_16, config_vals[7]);
                mpfr_init2 (temp_var_17, config_vals[7]);
            mpfr_init2 (temp_var_18, config_vals[7]);
            mpfr_init2 (temp_var_19, config_vals[7]);
            mpfr_init2 (temp_var_20, config_vals[7]);
            mpfr_init2 (temp_var_21, config_vals[7]);
            mpfr_init2 (temp_var_22, config_vals[7]);
        mpfr_init2 (temp_var_23, config_vals[7]);
        mpfr_init2 (temp_var_24, config_vals[7]);
        mpfr_init2 (temp_var_25, config_vals[7]);
        mpfr_init2 (temp_var_26, config_vals[7]);
         mpfr_init2 (temp_var_27, config_vals[7]);
         mpfr_init2 (temp_var_28, config_vals[7]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
            printf("%d ", config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

int solver(float **y, float *x, int xmax, float *params, int mode)
{
  int error;
  int outside;
  int xmin;
  float *err = (float *) malloc(EQUATIONS * (sizeof(float)));
  float *scale = (float *) malloc(EQUATIONS * (sizeof(float)));
  float *yy = (float *) malloc(EQUATIONS * (sizeof(float)));
  int i;
  int j;
  int k;
  mpfr_set_d(err_exponent_solver, 1.0 / 7.0, MPFR_RNDZ);
  mpfr_set_d(last_interval_solver, 0, MPFR_RNDZ);
  mpfr_set_d(h_init_solver, 1, MPFR_RNDZ);
  mpfr_set(h_solver, h_init_solver, MPFR_RNDZ);
  xmin = 0;
  mpfr_set_d(tolerance_solver, 10 / ((float) (xmax - xmin)), MPFR_RNDZ);
  x[0] = 0;
  mpfr_set_d(temp_var_25, 0.0, MPFR_RNDZ);
  if (xmax < xmin || mpfr_lessequal_p(h_solver, temp_var_25))
  {
    return -2;
  }

  if (xmax == xmin)
  {
    return 0;
  }
	
	mpfr_set_d(temp_var_25, ((float) xmax) - ((float) xmin), MPFR_RNDZ);

  if ( mpfr_greater_p (h_solver,temp_var_25))
  {
    mpfr_set_d(h_solver, ((float) xmax) - ((float) xmin), MPFR_RNDZ);
    mpfr_set_d(last_interval_solver, 1, MPFR_RNDZ);
  }

  for (k = 1; k <= xmax; k++){
  {
    x[k] = k - 1;
    mpfr_set(h_solver, h_init_solver, MPFR_RNDZ);
    mpfr_set_d(scale_fina_solver, 1.0, MPFR_RNDZ);
    for (j = 0; j < 12; j++){
    {
      error = 0;
      outside = 0;
      mpfr_set_d(scale_min_solver, 4.0, MPFR_RNDZ);
      embedded_fehlberg_7_8(x[k], mpfr_get_d(h_solver, MPFR_RNDZ), y[k - 1], y[k], err, params, mode);
      for (i = 0; i < EQUATIONS; i++){
      {
        if (err[i] > 0)
        {
          error = 1;
        }

      }

            }

      if (error != 1)
      {
        mpfr_set_d(scale_fina_solver, 4.0, MPFR_RNDZ);
        break;
      }

      for (i = 0; i < EQUATIONS; i++){
      {
        if (y[k - 1][i] == 0.0)
        {
          
			yy[i] = mpfr_get_d(tolerance_solver, MPFR_RNDZ);
        }
        else
        {
			yy[i] = fabs(y[k - 1][i]);
        }
                mpfr_set_d(temp_var_5, yy[i], MPFR_RNDZ);

                mpfr_mul_d(temp_var_6, temp_var_5, mpfr_get_d(tolerance_solver, MPFR_RNDZ), MPFR_RNDZ);

                mpfr_set_d(temp_var_7, err[i], MPFR_RNDZ);

                mpfr_div(temp_var_8, (temp_var_6), temp_var_7, MPFR_RNDZ);
                
        scale[i] = 0.8 * pow(mpfr_get_d(temp_var_8, MPFR_RNDZ), mpfr_get_d(err_exponent_solver, MPFR_RNDZ));

        mpfr_set_d(temp_var_26, scale[i], MPFR_RNDZ);

        if (mpfr_less_p(temp_var_26, scale_min_solver))
        {
          mpfr_set_d(scale_min_solver, scale[i], MPFR_RNDZ);
        }

      }

            }

//      mpfr_set_d(scale_fina_solver, (scale_min_solver < 0.125 ? 0.125 : scale_min_solver) < 4.0 ? scale_min_solver < 0.125 ? 0.125 : scale_min_solver : 4.0, MPFR_RNDZ);
	//this macro's too complex for the conversion	
//			scale_fina = min( max(scale_min,MIN_SCALE_FACTOR), MAX_SCALE_FACTOR);
		float compare;
		if (mpfr_cmp_d (scale_min_solver, 0.125) < 0)
			compare = 0.125f;
		else
			compare = mpfr_get_d(scale_min_solver, MPFR_RNDZ);
		
		if (compare < MAX_SCALE_FACTOR)
			mpfr_set_d(scale_fina_solver,compare, MPFR_RNDZ);
		else
			mpfr_set_d(scale_fina_solver,MAX_SCALE_FACTOR, MPFR_RNDZ);
			

      for (i = 0; i < EQUATIONS; i++){
      {
		mpfr_set_d(temp_var_10, yy[i], MPFR_RNDZ);

        mpfr_mul(temp_var_11, tolerance_solver, temp_var_10, MPFR_RNDZ);
         mpfr_set_d(temp_var_12, err[i], MPFR_RNDZ);
        if (  mpfr_greater_p(temp_var_12, temp_var_11))
        {
          outside = 1;
        }

      }

            }

      if (outside == 0)
      {
        break;
      }

                  mpfr_mul(h_solver, h_solver, scale_fina_solver, MPFR_RNDZ);
;


mpfr_set_d(temp_var_28, 0.9, MPFR_RNDZ);
      if (mpfr_greaterequal_p(h_solver, temp_var_28))
      {
        mpfr_set_d(h_solver, 0.9, MPFR_RNDZ);
      }
//if ( x[k] + h > (fp)xmax )
            mpfr_set_d(temp_var_14, x[k], MPFR_RNDZ);

            mpfr_add(temp_var_15, temp_var_14, h_solver, MPFR_RNDZ);
            
      if (  mpfr_cmp_d (temp_var_15,(float)xmax) >  0  )
      {
        
                mpfr_set_d(temp_var_17, x[k], MPFR_RNDZ);
                mpfr_d_sub(h_solver, ((float) xmax), temp_var_17, MPFR_RNDZ);
;
      }
      else
      {
              mpfr_set_d(temp_var_18, x[k], MPFR_RNDZ);

            mpfr_add(temp_var_19, temp_var_18, h_solver, MPFR_RNDZ);

            mpfr_mul_d(temp_var_20, h_solver, 0.5, MPFR_RNDZ);

            mpfr_add(temp_var_21, (temp_var_19), (temp_var_20), MPFR_RNDZ);
  
      
       if ( mpfr_cmp_d (temp_var_21,(float)xmax) >  0)
      {
                        mpfr_mul_d(h_solver, h_solver, 0.5, MPFR_RNDZ);
      }
  }


    }

        }

    
        mpfr_set_d(temp_var_23, x[k], MPFR_RNDZ);

        mpfr_add(temp_var_24, temp_var_23, h_solver, MPFR_RNDZ);

x[k] = mpfr_get_d(temp_var_24, MPFR_RNDZ);
    if (j >= 12)
    {
      return -1;
    }

  }

    }

  free(err);
  free(scale);
  free(yy);
  return 0;
}

//end of conversion, hopefully it will work :)
