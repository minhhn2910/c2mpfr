#include <mpfr.h>
#define LEN 108 
 int config_vals[LEN];
  mpfr_t JCa_cam;
  mpfr_t CaM_cam;
  mpfr_t Ca2CaM_cam;
  mpfr_t Ca4CaM_cam;
  mpfr_t CaMB_cam;
  mpfr_t Ca2CaMB_cam;
  mpfr_t Ca4CaMB_cam;
  mpfr_t Pb2_cam;
  mpfr_t Pb_cam;
  mpfr_t Pt_cam;
  mpfr_t Pt2_cam;
  mpfr_t Pa_cam;
  mpfr_t Ca4CaN_cam;
  mpfr_t CaMCa4CaN_cam;
  mpfr_t Ca2CaMCa4CaN_cam;
  mpfr_t Ca4CaMCa4CaN_cam;
  mpfr_t CaMtot_cam;
  mpfr_t Btot_cam;
  mpfr_t CaMKIItot_cam;
  mpfr_t CaNtot_cam;
  mpfr_t PP1tot_cam;
  mpfr_t K_cam;
  mpfr_t Mg_cam;
  mpfr_t Kd02_cam;
  mpfr_t Kd24_cam;
  mpfr_t k20_cam;
  mpfr_t k02_cam;
  mpfr_t k42_cam;
  mpfr_t k24_cam;
  mpfr_t k0Boff_cam;
  mpfr_t k0Bon_cam;
  mpfr_t k2Boff_cam;
  mpfr_t k2Bon_cam;
  mpfr_t k4Boff_cam;
  mpfr_t k4Bon_cam;
  mpfr_t k20B_cam;
  mpfr_t k02B_cam;
  mpfr_t k42B_cam;
  mpfr_t k24B_cam;
  mpfr_t kbi_cam;
  mpfr_t kib_cam;
  mpfr_t kpp1_cam;
  mpfr_t Kmpp1_cam;
  mpfr_t kib2_cam;
  mpfr_t kb2i_cam;
  mpfr_t kb24_cam;
  mpfr_t kb42_cam;
  mpfr_t kta_cam;
  mpfr_t kat_cam;
  mpfr_t kt42_cam;
  mpfr_t kt24_cam;
  mpfr_t kat2_cam;
  mpfr_t kt2a_cam;
  mpfr_t kcanCaoff_cam;
  mpfr_t kcanCaon_cam;
  mpfr_t kcanCaM4on_cam;
  mpfr_t kcanCaM4off_cam;
  mpfr_t kcanCaM2on_cam;
  mpfr_t kcanCaM2off_cam;
  mpfr_t kcanCaM0on_cam;
  mpfr_t kcanCaM0off_cam;
  mpfr_t k02can_cam;
  mpfr_t k20can_cam;
  mpfr_t k24can_cam;
  mpfr_t k42can_cam;
  mpfr_t rcn02_cam;
  mpfr_t rcn24_cam;
  mpfr_t B_cam;
  mpfr_t rcn02B_cam;
  mpfr_t rcn24B_cam;
  mpfr_t rcn0B_cam;
  mpfr_t rcn2B_cam;
  mpfr_t rcn4B_cam;
  mpfr_t Ca2CaN_cam;
  mpfr_t rcnCa4CaN_cam;
  mpfr_t rcn02CaN_cam;
  mpfr_t rcn24CaN_cam;
  mpfr_t rcn0CaN_cam;
  mpfr_t rcn2CaN_cam;
  mpfr_t rcn4CaN_cam;
  mpfr_t Pix_cam;
  mpfr_t rcnCKib2_cam;
  mpfr_t rcnCKb2b_cam;
  mpfr_t rcnCKib_cam;
  mpfr_t T_cam;
  mpfr_t kbt_cam;
  mpfr_t rcnCKbt_cam;
  mpfr_t rcnCKtt2_cam;
  mpfr_t rcnCKta_cam;
  mpfr_t rcnCKt2a_cam;
  mpfr_t rcnCKt2b2_cam;
  mpfr_t rcnCKai_cam;
  mpfr_t dCaM_cam;
  mpfr_t dCa2CaM_cam;
  mpfr_t dCa4CaM_cam;
  mpfr_t dCaMB_cam;
  mpfr_t dCa2CaMB_cam;
  mpfr_t dCa4CaMB_cam;
  mpfr_t dPb2_cam;
  mpfr_t dPb_cam;
  mpfr_t dPt_cam;
  mpfr_t dPt2_cam;
  mpfr_t dPa_cam;
  mpfr_t dCa4CaN_cam;
  mpfr_t dCaMCa4CaN_cam;
  mpfr_t dCa2CaMCa4CaN_cam;
  mpfr_t dCa4CaMCa4CaN_cam;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
    mpfr_t temp_var_11;
    mpfr_t temp_var_12;
    mpfr_t temp_var_13;
    mpfr_t temp_var_14;
    mpfr_t temp_var_15;
    mpfr_t temp_var_16;
    mpfr_t temp_var_17;
    mpfr_t temp_var_18;
        mpfr_t temp_var_19;
        mpfr_t temp_var_20;
        mpfr_t temp_var_21;
        mpfr_t temp_var_22;
        mpfr_t temp_var_23;
        mpfr_t temp_var_24;
        mpfr_t temp_var_25;
        mpfr_t temp_var_26;
        mpfr_t temp_var_27;
        mpfr_t temp_var_28;
        mpfr_t temp_var_29;
        mpfr_t temp_var_30;
        mpfr_t temp_var_31;
        mpfr_t temp_var_32;
        mpfr_t temp_var_33;
        mpfr_t temp_var_34;
        mpfr_t temp_var_35;
        mpfr_t temp_var_36;
        mpfr_t temp_var_37;
        mpfr_t temp_var_38;
        mpfr_t temp_var_39;
        mpfr_t temp_var_40;
        mpfr_t temp_var_41;
        mpfr_t temp_var_42;
        mpfr_t temp_var_43;
        mpfr_t temp_var_44;
        mpfr_t temp_var_45;
        mpfr_t temp_var_46;
        mpfr_t temp_var_47;
        mpfr_t temp_var_48;
        mpfr_t temp_var_49;
        mpfr_t temp_var_50;
        mpfr_t temp_var_51;
        mpfr_t temp_var_52;
        mpfr_t temp_var_53;
        mpfr_t temp_var_54;
        mpfr_t temp_var_55;
        mpfr_t temp_var_56;
        mpfr_t temp_var_57;
        mpfr_t temp_var_58;
        mpfr_t temp_var_59;
        mpfr_t temp_var_60;
        mpfr_t temp_var_61;
        mpfr_t temp_var_62;
    mpfr_t temp_var_63;
    mpfr_t temp_var_64;
    mpfr_t temp_var_65;
    mpfr_t temp_var_66;
    mpfr_t temp_var_67;
    mpfr_t temp_var_68;
    mpfr_t temp_var_69;
    mpfr_t temp_var_70;
    mpfr_t temp_var_71;
    mpfr_t temp_var_72;
    mpfr_t temp_var_73;
    mpfr_t temp_var_74;
    mpfr_t temp_var_75;
    mpfr_t temp_var_76;
    mpfr_t temp_var_77;
    mpfr_t temp_var_78;
    mpfr_t temp_var_79;
    mpfr_t temp_var_80;
    mpfr_t temp_var_81;
    mpfr_t temp_var_82;
    mpfr_t temp_var_83;
    mpfr_t temp_var_84;
    mpfr_t temp_var_85;
    mpfr_t temp_var_86;
    mpfr_t temp_var_87;
    mpfr_t temp_var_88;
    mpfr_t temp_var_89;
    mpfr_t temp_var_90;
    mpfr_t temp_var_91;
    mpfr_t temp_var_92;
    mpfr_t temp_var_93;
    mpfr_t temp_var_94;
    mpfr_t temp_var_95;
    mpfr_t temp_var_96;
    mpfr_t temp_var_97;
    mpfr_t temp_var_98;
    mpfr_t temp_var_99;
    mpfr_t temp_var_100;
    mpfr_t temp_var_101;
    mpfr_t temp_var_102;
    mpfr_t temp_var_103;
    mpfr_t temp_var_104;
    mpfr_t temp_var_105;
    mpfr_t temp_var_106;
    mpfr_t temp_var_107;
    mpfr_t temp_var_108;
    mpfr_t temp_var_109;
    mpfr_t temp_var_110;
    mpfr_t temp_var_111;
    mpfr_t temp_var_112;
    mpfr_t temp_var_113;
    mpfr_t temp_var_114;
    mpfr_t temp_var_115;
    mpfr_t temp_var_116;
    mpfr_t temp_var_117;
    mpfr_t temp_var_118;
    mpfr_t temp_var_119;
    mpfr_t temp_var_120;
    mpfr_t temp_var_121;
    mpfr_t temp_var_122;
    mpfr_t temp_var_123;
    mpfr_t temp_var_124;
    mpfr_t temp_var_125;
    mpfr_t temp_var_126;
    mpfr_t temp_var_127;
    mpfr_t temp_var_128;
    mpfr_t temp_var_129;
    mpfr_t temp_var_130;
    mpfr_t temp_var_131;
    mpfr_t temp_var_132;
    mpfr_t temp_var_133;
    mpfr_t temp_var_134;
    mpfr_t temp_var_135;
    mpfr_t temp_var_136;
    mpfr_t temp_var_137;
    mpfr_t temp_var_138;
    mpfr_t temp_var_139;
    mpfr_t temp_var_140;
    mpfr_t temp_var_141;
    mpfr_t temp_var_142;
    mpfr_t temp_var_143;
    mpfr_t temp_var_144;
    mpfr_t temp_var_145;
    mpfr_t temp_var_146;
    mpfr_t temp_var_147;
    mpfr_t temp_var_148;
    mpfr_t temp_var_149;
    mpfr_t temp_var_150;
    mpfr_t temp_var_151;
    mpfr_t temp_var_152;
    mpfr_t temp_var_153;
    mpfr_t temp_var_154;
    mpfr_t temp_var_155;
    mpfr_t temp_var_156;
    mpfr_t temp_var_157;
    mpfr_t temp_var_158;
    mpfr_t temp_var_159;
    mpfr_t temp_var_160;
    mpfr_t temp_var_161;
    mpfr_t temp_var_162;
    mpfr_t temp_var_163;
    mpfr_t temp_var_164;
    mpfr_t temp_var_165;
    mpfr_t temp_var_166;
    mpfr_t temp_var_167;
    mpfr_t temp_var_168;
    mpfr_t temp_var_169;
    mpfr_t temp_var_170;
    mpfr_t temp_var_171;
    mpfr_t temp_var_172;
    mpfr_t temp_var_173;
    mpfr_t temp_var_174;
    mpfr_t temp_var_175;
    mpfr_t temp_var_176;
    mpfr_t temp_var_177;
    mpfr_t temp_var_178;
    mpfr_t temp_var_179;
    mpfr_t temp_var_180;
    mpfr_t temp_var_181;
    mpfr_t temp_var_182;
    mpfr_t temp_var_183;
    mpfr_t temp_var_184;
    mpfr_t temp_var_185;
    mpfr_t temp_var_186;
    mpfr_t temp_var_187;
    mpfr_t temp_var_188;
    mpfr_t temp_var_189;
    mpfr_t temp_var_190;
    mpfr_t temp_var_191;
    mpfr_t temp_var_192;
    mpfr_t temp_var_193;
    mpfr_t temp_var_194;
    mpfr_t temp_var_195;
    mpfr_t temp_var_196;
    mpfr_t temp_var_197;
    mpfr_t temp_var_198;
int init_mpfr() { 
  mpfr_init2(JCa_cam, config_vals[1]);
  mpfr_init2(CaM_cam, config_vals[2]);
  mpfr_init2(Ca2CaM_cam, config_vals[3]);
  mpfr_init2(Ca4CaM_cam, config_vals[4]);
  mpfr_init2(CaMB_cam, config_vals[5]);
  mpfr_init2(Ca2CaMB_cam, config_vals[6]);
  mpfr_init2(Ca4CaMB_cam, config_vals[7]);
  mpfr_init2(Pb2_cam, config_vals[8]);
  mpfr_init2(Pb_cam, config_vals[9]);
  mpfr_init2(Pt_cam, config_vals[10]);
  mpfr_init2(Pt2_cam, config_vals[11]);
  mpfr_init2(Pa_cam, config_vals[12]);
  mpfr_init2(Ca4CaN_cam, config_vals[13]);
  mpfr_init2(CaMCa4CaN_cam, config_vals[14]);
  mpfr_init2(Ca2CaMCa4CaN_cam, config_vals[15]);
  mpfr_init2(Ca4CaMCa4CaN_cam, config_vals[16]);
  mpfr_init2(CaMtot_cam, config_vals[17]);
  mpfr_init2(Btot_cam, config_vals[18]);
  mpfr_init2(CaMKIItot_cam, config_vals[19]);
  mpfr_init2(CaNtot_cam, config_vals[20]);
  mpfr_init2(PP1tot_cam, config_vals[21]);
  mpfr_init2(K_cam, config_vals[22]);
  mpfr_init2(Mg_cam, config_vals[23]);
  mpfr_init2(Kd02_cam, config_vals[24]);
  mpfr_init2(Kd24_cam, config_vals[25]);
  mpfr_init2(k20_cam, config_vals[26]);
  mpfr_init2(k02_cam, config_vals[27]);
  mpfr_init2(k42_cam, config_vals[28]);
  mpfr_init2(k24_cam, config_vals[29]);
  mpfr_init2(k0Boff_cam, config_vals[30]);
  mpfr_init2(k0Bon_cam, config_vals[31]);
  mpfr_init2(k2Boff_cam, config_vals[32]);
  mpfr_init2(k2Bon_cam, config_vals[33]);
  mpfr_init2(k4Boff_cam, config_vals[34]);
  mpfr_init2(k4Bon_cam, config_vals[35]);
  mpfr_init2(k20B_cam, config_vals[36]);
  mpfr_init2(k02B_cam, config_vals[37]);
  mpfr_init2(k42B_cam, config_vals[38]);
  mpfr_init2(k24B_cam, config_vals[39]);
  mpfr_init2(kbi_cam, config_vals[40]);
  mpfr_init2(kib_cam, config_vals[41]);
  mpfr_init2(kpp1_cam, config_vals[42]);
  mpfr_init2(Kmpp1_cam, config_vals[43]);
  mpfr_init2(kib2_cam, config_vals[44]);
  mpfr_init2(kb2i_cam, config_vals[45]);
  mpfr_init2(kb24_cam, config_vals[46]);
  mpfr_init2(kb42_cam, config_vals[47]);
  mpfr_init2(kta_cam, config_vals[48]);
  mpfr_init2(kat_cam, config_vals[49]);
  mpfr_init2(kt42_cam, config_vals[50]);
  mpfr_init2(kt24_cam, config_vals[51]);
  mpfr_init2(kat2_cam, config_vals[52]);
  mpfr_init2(kt2a_cam, config_vals[53]);
  mpfr_init2(kcanCaoff_cam, config_vals[54]);
  mpfr_init2(kcanCaon_cam, config_vals[55]);
  mpfr_init2(kcanCaM4on_cam, config_vals[56]);
  mpfr_init2(kcanCaM4off_cam, config_vals[57]);
  mpfr_init2(kcanCaM2on_cam, config_vals[58]);
  mpfr_init2(kcanCaM2off_cam, config_vals[59]);
  mpfr_init2(kcanCaM0on_cam, config_vals[60]);
  mpfr_init2(kcanCaM0off_cam, config_vals[61]);
  mpfr_init2(k02can_cam, config_vals[62]);
  mpfr_init2(k20can_cam, config_vals[63]);
  mpfr_init2(k24can_cam, config_vals[64]);
  mpfr_init2(k42can_cam, config_vals[65]);
  mpfr_init2(rcn02_cam, config_vals[66]);
  mpfr_init2(rcn24_cam, config_vals[67]);
  mpfr_init2(B_cam, config_vals[68]);
  mpfr_init2(rcn02B_cam, config_vals[69]);
  mpfr_init2(rcn24B_cam, config_vals[70]);
  mpfr_init2(rcn0B_cam, config_vals[71]);
  mpfr_init2(rcn2B_cam, config_vals[72]);
  mpfr_init2(rcn4B_cam, config_vals[73]);
  mpfr_init2(Ca2CaN_cam, config_vals[74]);
  mpfr_init2(rcnCa4CaN_cam, config_vals[75]);
  mpfr_init2(rcn02CaN_cam, config_vals[76]);
  mpfr_init2(rcn24CaN_cam, config_vals[77]);
  mpfr_init2(rcn0CaN_cam, config_vals[78]);
  mpfr_init2(rcn2CaN_cam, config_vals[79]);
  mpfr_init2(rcn4CaN_cam, config_vals[80]);
  mpfr_init2(Pix_cam, config_vals[81]);
  mpfr_init2(rcnCKib2_cam, config_vals[82]);
  mpfr_init2(rcnCKb2b_cam, config_vals[83]);
  mpfr_init2(rcnCKib_cam, config_vals[84]);
  mpfr_init2(T_cam, config_vals[85]);
  mpfr_init2(kbt_cam, config_vals[86]);
  mpfr_init2(rcnCKbt_cam, config_vals[87]);
  mpfr_init2(rcnCKtt2_cam, config_vals[88]);
  mpfr_init2(rcnCKta_cam, config_vals[89]);
  mpfr_init2(rcnCKt2a_cam, config_vals[90]);
  mpfr_init2(rcnCKt2b2_cam, config_vals[91]);
  mpfr_init2(rcnCKai_cam, config_vals[92]);
  mpfr_init2(dCaM_cam, config_vals[93]);
  mpfr_init2(dCa2CaM_cam, config_vals[94]);
  mpfr_init2(dCa4CaM_cam, config_vals[95]);
  mpfr_init2(dCaMB_cam, config_vals[96]);
  mpfr_init2(dCa2CaMB_cam, config_vals[97]);
  mpfr_init2(dCa4CaMB_cam, config_vals[98]);
  mpfr_init2(dPb2_cam, config_vals[99]);
  mpfr_init2(dPb_cam, config_vals[100]);
  mpfr_init2(dPt_cam, config_vals[101]);
  mpfr_init2(dPt2_cam, config_vals[102]);
  mpfr_init2(dPa_cam, config_vals[103]);
  mpfr_init2(dCa4CaN_cam, config_vals[104]);
  mpfr_init2(dCaMCa4CaN_cam, config_vals[105]);
  mpfr_init2(dCa2CaMCa4CaN_cam, config_vals[106]);
  mpfr_init2(dCa4CaMCa4CaN_cam, config_vals[107]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
    mpfr_init2 (temp_var_4, config_vals[0]);
    mpfr_init2 (temp_var_5, config_vals[0]);
    mpfr_init2 (temp_var_6, config_vals[0]);
    mpfr_init2 (temp_var_7, config_vals[0]);
    mpfr_init2 (temp_var_8, config_vals[0]);
    mpfr_init2 (temp_var_9, config_vals[0]);
    mpfr_init2 (temp_var_10, config_vals[0]);
    mpfr_init2 (temp_var_11, config_vals[0]);
    mpfr_init2 (temp_var_12, config_vals[0]);
    mpfr_init2 (temp_var_13, config_vals[0]);
    mpfr_init2 (temp_var_14, config_vals[0]);
    mpfr_init2 (temp_var_15, config_vals[0]);
    mpfr_init2 (temp_var_16, config_vals[0]);
    mpfr_init2 (temp_var_17, config_vals[0]);
    mpfr_init2 (temp_var_18, config_vals[0]);
        mpfr_init2 (temp_var_19, config_vals[0]);
        mpfr_init2 (temp_var_20, config_vals[0]);
        mpfr_init2 (temp_var_21, config_vals[0]);
        mpfr_init2 (temp_var_22, config_vals[0]);
        mpfr_init2 (temp_var_23, config_vals[0]);
        mpfr_init2 (temp_var_24, config_vals[0]);
        mpfr_init2 (temp_var_25, config_vals[0]);
        mpfr_init2 (temp_var_26, config_vals[0]);
        mpfr_init2 (temp_var_27, config_vals[0]);
        mpfr_init2 (temp_var_28, config_vals[0]);
        mpfr_init2 (temp_var_29, config_vals[0]);
        mpfr_init2 (temp_var_30, config_vals[0]);
        mpfr_init2 (temp_var_31, config_vals[0]);
        mpfr_init2 (temp_var_32, config_vals[0]);
        mpfr_init2 (temp_var_33, config_vals[0]);
        mpfr_init2 (temp_var_34, config_vals[0]);
        mpfr_init2 (temp_var_35, config_vals[0]);
        mpfr_init2 (temp_var_36, config_vals[0]);
        mpfr_init2 (temp_var_37, config_vals[0]);
        mpfr_init2 (temp_var_38, config_vals[0]);
        mpfr_init2 (temp_var_39, config_vals[0]);
        mpfr_init2 (temp_var_40, config_vals[0]);
        mpfr_init2 (temp_var_41, config_vals[0]);
        mpfr_init2 (temp_var_42, config_vals[0]);
        mpfr_init2 (temp_var_43, config_vals[0]);
        mpfr_init2 (temp_var_44, config_vals[0]);
        mpfr_init2 (temp_var_45, config_vals[0]);
        mpfr_init2 (temp_var_46, config_vals[0]);
        mpfr_init2 (temp_var_47, config_vals[0]);
        mpfr_init2 (temp_var_48, config_vals[0]);
        mpfr_init2 (temp_var_49, config_vals[0]);
        mpfr_init2 (temp_var_50, config_vals[0]);
        mpfr_init2 (temp_var_51, config_vals[0]);
        mpfr_init2 (temp_var_52, config_vals[0]);
        mpfr_init2 (temp_var_53, config_vals[0]);
        mpfr_init2 (temp_var_54, config_vals[0]);
        mpfr_init2 (temp_var_55, config_vals[0]);
        mpfr_init2 (temp_var_56, config_vals[0]);
        mpfr_init2 (temp_var_57, config_vals[0]);
        mpfr_init2 (temp_var_58, config_vals[0]);
        mpfr_init2 (temp_var_59, config_vals[0]);
        mpfr_init2 (temp_var_60, config_vals[0]);
        mpfr_init2 (temp_var_61, config_vals[0]);
        mpfr_init2 (temp_var_62, config_vals[0]);
    mpfr_init2 (temp_var_63, config_vals[0]);
    mpfr_init2 (temp_var_64, config_vals[0]);
    mpfr_init2 (temp_var_65, config_vals[0]);
    mpfr_init2 (temp_var_66, config_vals[0]);
    mpfr_init2 (temp_var_67, config_vals[0]);
    mpfr_init2 (temp_var_68, config_vals[0]);
    mpfr_init2 (temp_var_69, config_vals[0]);
    mpfr_init2 (temp_var_70, config_vals[0]);
    mpfr_init2 (temp_var_71, config_vals[0]);
    mpfr_init2 (temp_var_72, config_vals[0]);
    mpfr_init2 (temp_var_73, config_vals[0]);
    mpfr_init2 (temp_var_74, config_vals[0]);
    mpfr_init2 (temp_var_75, config_vals[0]);
    mpfr_init2 (temp_var_76, config_vals[0]);
    mpfr_init2 (temp_var_77, config_vals[0]);
    mpfr_init2 (temp_var_78, config_vals[0]);
    mpfr_init2 (temp_var_79, config_vals[0]);
    mpfr_init2 (temp_var_80, config_vals[0]);
    mpfr_init2 (temp_var_81, config_vals[0]);
    mpfr_init2 (temp_var_82, config_vals[0]);
    mpfr_init2 (temp_var_83, config_vals[0]);
    mpfr_init2 (temp_var_84, config_vals[0]);
    mpfr_init2 (temp_var_85, config_vals[0]);
    mpfr_init2 (temp_var_86, config_vals[0]);
    mpfr_init2 (temp_var_87, config_vals[0]);
    mpfr_init2 (temp_var_88, config_vals[0]);
    mpfr_init2 (temp_var_89, config_vals[0]);
    mpfr_init2 (temp_var_90, config_vals[0]);
    mpfr_init2 (temp_var_91, config_vals[0]);
    mpfr_init2 (temp_var_92, config_vals[0]);
    mpfr_init2 (temp_var_93, config_vals[0]);
    mpfr_init2 (temp_var_94, config_vals[0]);
    mpfr_init2 (temp_var_95, config_vals[0]);
    mpfr_init2 (temp_var_96, config_vals[0]);
    mpfr_init2 (temp_var_97, config_vals[0]);
    mpfr_init2 (temp_var_98, config_vals[0]);
    mpfr_init2 (temp_var_99, config_vals[0]);
    mpfr_init2 (temp_var_100, config_vals[0]);
    mpfr_init2 (temp_var_101, config_vals[0]);
    mpfr_init2 (temp_var_102, config_vals[0]);
    mpfr_init2 (temp_var_103, config_vals[0]);
    mpfr_init2 (temp_var_104, config_vals[0]);
    mpfr_init2 (temp_var_105, config_vals[0]);
    mpfr_init2 (temp_var_106, config_vals[0]);
    mpfr_init2 (temp_var_107, config_vals[0]);
    mpfr_init2 (temp_var_108, config_vals[0]);
    mpfr_init2 (temp_var_109, config_vals[0]);
    mpfr_init2 (temp_var_110, config_vals[0]);
    mpfr_init2 (temp_var_111, config_vals[0]);
    mpfr_init2 (temp_var_112, config_vals[0]);
    mpfr_init2 (temp_var_113, config_vals[0]);
    mpfr_init2 (temp_var_114, config_vals[0]);
    mpfr_init2 (temp_var_115, config_vals[0]);
    mpfr_init2 (temp_var_116, config_vals[0]);
    mpfr_init2 (temp_var_117, config_vals[0]);
    mpfr_init2 (temp_var_118, config_vals[0]);
    mpfr_init2 (temp_var_119, config_vals[0]);
    mpfr_init2 (temp_var_120, config_vals[0]);
    mpfr_init2 (temp_var_121, config_vals[0]);
    mpfr_init2 (temp_var_122, config_vals[0]);
    mpfr_init2 (temp_var_123, config_vals[0]);
    mpfr_init2 (temp_var_124, config_vals[0]);
    mpfr_init2 (temp_var_125, config_vals[0]);
    mpfr_init2 (temp_var_126, config_vals[0]);
    mpfr_init2 (temp_var_127, config_vals[0]);
    mpfr_init2 (temp_var_128, config_vals[0]);
    mpfr_init2 (temp_var_129, config_vals[0]);
    mpfr_init2 (temp_var_130, config_vals[0]);
    mpfr_init2 (temp_var_131, config_vals[0]);
    mpfr_init2 (temp_var_132, config_vals[0]);
    mpfr_init2 (temp_var_133, config_vals[0]);
    mpfr_init2 (temp_var_134, config_vals[0]);
    mpfr_init2 (temp_var_135, config_vals[0]);
    mpfr_init2 (temp_var_136, config_vals[0]);
    mpfr_init2 (temp_var_137, config_vals[0]);
    mpfr_init2 (temp_var_138, config_vals[0]);
    mpfr_init2 (temp_var_139, config_vals[0]);
    mpfr_init2 (temp_var_140, config_vals[0]);
    mpfr_init2 (temp_var_141, config_vals[0]);
    mpfr_init2 (temp_var_142, config_vals[0]);
    mpfr_init2 (temp_var_143, config_vals[0]);
    mpfr_init2 (temp_var_144, config_vals[0]);
    mpfr_init2 (temp_var_145, config_vals[0]);
    mpfr_init2 (temp_var_146, config_vals[0]);
    mpfr_init2 (temp_var_147, config_vals[0]);
    mpfr_init2 (temp_var_148, config_vals[0]);
    mpfr_init2 (temp_var_149, config_vals[0]);
    mpfr_init2 (temp_var_150, config_vals[0]);
    mpfr_init2 (temp_var_151, config_vals[0]);
    mpfr_init2 (temp_var_152, config_vals[0]);
    mpfr_init2 (temp_var_153, config_vals[0]);
    mpfr_init2 (temp_var_154, config_vals[0]);
    mpfr_init2 (temp_var_155, config_vals[0]);
    mpfr_init2 (temp_var_156, config_vals[0]);
    mpfr_init2 (temp_var_157, config_vals[0]);
    mpfr_init2 (temp_var_158, config_vals[0]);
    mpfr_init2 (temp_var_159, config_vals[0]);
    mpfr_init2 (temp_var_160, config_vals[0]);
    mpfr_init2 (temp_var_161, config_vals[0]);
    mpfr_init2 (temp_var_162, config_vals[0]);
    mpfr_init2 (temp_var_163, config_vals[0]);
    mpfr_init2 (temp_var_164, config_vals[0]);
    mpfr_init2 (temp_var_165, config_vals[0]);
    mpfr_init2 (temp_var_166, config_vals[0]);
    mpfr_init2 (temp_var_167, config_vals[0]);
    mpfr_init2 (temp_var_168, config_vals[0]);
    mpfr_init2 (temp_var_169, config_vals[0]);
    mpfr_init2 (temp_var_170, config_vals[0]);
    mpfr_init2 (temp_var_171, config_vals[0]);
    mpfr_init2 (temp_var_172, config_vals[0]);
    mpfr_init2 (temp_var_173, config_vals[0]);
    mpfr_init2 (temp_var_174, config_vals[0]);
    mpfr_init2 (temp_var_175, config_vals[0]);
    mpfr_init2 (temp_var_176, config_vals[0]);
    mpfr_init2 (temp_var_177, config_vals[0]);
    mpfr_init2 (temp_var_178, config_vals[0]);
    mpfr_init2 (temp_var_179, config_vals[0]);
    mpfr_init2 (temp_var_180, config_vals[0]);
    mpfr_init2 (temp_var_181, config_vals[0]);
    mpfr_init2 (temp_var_182, config_vals[0]);
    mpfr_init2 (temp_var_183, config_vals[0]);
    mpfr_init2 (temp_var_184, config_vals[0]);
    mpfr_init2 (temp_var_185, config_vals[0]);
    mpfr_init2 (temp_var_186, config_vals[0]);
    mpfr_init2 (temp_var_187, config_vals[0]);
    mpfr_init2 (temp_var_188, config_vals[0]);
    mpfr_init2 (temp_var_189, config_vals[0]);
    mpfr_init2 (temp_var_190, config_vals[0]);
    mpfr_init2 (temp_var_191, config_vals[0]);
    mpfr_init2 (temp_var_192, config_vals[0]);
    mpfr_init2 (temp_var_193, config_vals[0]);
    mpfr_init2 (temp_var_194, config_vals[0]);
    mpfr_init2 (temp_var_195, config_vals[0]);
    mpfr_init2 (temp_var_196, config_vals[0]);
    mpfr_init2 (temp_var_197, config_vals[0]);
    mpfr_init2 (temp_var_198, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

float cam(float timeinst, float *initvalu, int initvalu_offset, float *parameter, int parameter_offset, float *finavalu, float Ca)
{
  int offset_1;
  int offset_2;
  int offset_3;
  int offset_4;
  int offset_5;
  int offset_6;
  int offset_7;
  int offset_8;
  int offset_9;
  int offset_10;
  int offset_11;
  int offset_12;
  int offset_13;
  int offset_14;
  int offset_15;
  int parameter_offset_1;
  int parameter_offset_2;
  int parameter_offset_3;
  int parameter_offset_4;
  int parameter_offset_5;
  offset_1 = initvalu_offset;
  offset_2 = initvalu_offset + 1;
  offset_3 = initvalu_offset + 2;
  offset_4 = initvalu_offset + 3;
  offset_5 = initvalu_offset + 4;
  offset_6 = initvalu_offset + 5;
  offset_7 = initvalu_offset + 6;
  offset_8 = initvalu_offset + 7;
  offset_9 = initvalu_offset + 8;
  offset_10 = initvalu_offset + 9;
  offset_11 = initvalu_offset + 10;
  offset_12 = initvalu_offset + 11;
  offset_13 = initvalu_offset + 12;
  offset_14 = initvalu_offset + 13;
  offset_15 = initvalu_offset + 14;
  parameter_offset_1 = parameter_offset;
  parameter_offset_2 = parameter_offset + 1;
  parameter_offset_3 = parameter_offset + 2;
  parameter_offset_4 = parameter_offset + 3;
  parameter_offset_5 = parameter_offset + 4;
  mpfr_set_d(CaM_cam, initvalu[offset_1], MPFR_RNDZ);
  mpfr_set_d(Ca2CaM_cam, initvalu[offset_2], MPFR_RNDZ);
  mpfr_set_d(Ca4CaM_cam, initvalu[offset_3], MPFR_RNDZ);
  mpfr_set_d(CaMB_cam, initvalu[offset_4], MPFR_RNDZ);
  mpfr_set_d(Ca2CaMB_cam, initvalu[offset_5], MPFR_RNDZ);
  mpfr_set_d(Ca4CaMB_cam, initvalu[offset_6], MPFR_RNDZ);
  mpfr_set_d(Pb2_cam, initvalu[offset_7], MPFR_RNDZ);
  mpfr_set_d(Pb_cam, initvalu[offset_8], MPFR_RNDZ);
  mpfr_set_d(Pt_cam, initvalu[offset_9], MPFR_RNDZ);
  mpfr_set_d(Pt2_cam, initvalu[offset_10], MPFR_RNDZ);
  mpfr_set_d(Pa_cam, initvalu[offset_11], MPFR_RNDZ);
  mpfr_set_d(Ca4CaN_cam, initvalu[offset_12], MPFR_RNDZ);
  mpfr_set_d(CaMCa4CaN_cam, initvalu[offset_13], MPFR_RNDZ);
  mpfr_set_d(Ca2CaMCa4CaN_cam, initvalu[offset_14], MPFR_RNDZ);
  mpfr_set_d(Ca4CaMCa4CaN_cam, initvalu[offset_15], MPFR_RNDZ);
  mpfr_set_d(CaMtot_cam, parameter[parameter_offset_1], MPFR_RNDZ);
  mpfr_set_d(Btot_cam, parameter[parameter_offset_2], MPFR_RNDZ);
  mpfr_set_d(CaMKIItot_cam, parameter[parameter_offset_3], MPFR_RNDZ);
  mpfr_set_d(CaNtot_cam, parameter[parameter_offset_4], MPFR_RNDZ);
  mpfr_set_d(PP1tot_cam, parameter[parameter_offset_5], MPFR_RNDZ);
  mpfr_set_d(K_cam, 135, MPFR_RNDZ);
  mpfr_set_d(Mg_cam, 1, MPFR_RNDZ);
  if ( mpfr_cmp_d(Mg_cam, 1) <= 0)
  {
    
        mpfr_div_d(temp_var_19, K_cam, 0.94, MPFR_RNDZ);

        mpfr_add_si(temp_var_20, (temp_var_19), 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_21, Mg_cam, 0.012, MPFR_RNDZ);

        mpfr_sub(temp_var_22, (temp_var_20), (temp_var_21), MPFR_RNDZ);

        mpfr_mul_d(temp_var_23, (temp_var_22), 0.0025, MPFR_RNDZ);

        mpfr_div_d(temp_var_24, K_cam, 8.1, MPFR_RNDZ);

        mpfr_add_si(temp_var_25, (temp_var_24), 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_26, Mg_cam, 0.022, MPFR_RNDZ);

        mpfr_add(temp_var_27, (temp_var_25), (temp_var_26), MPFR_RNDZ);
        mpfr_mul(Kd02_cam, (temp_var_23), (temp_var_27), MPFR_RNDZ);
;
    
        mpfr_div_d(temp_var_28, K_cam, 0.64, MPFR_RNDZ);

        mpfr_add_si(temp_var_29, (temp_var_28), 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_30, Mg_cam, 0.0014, MPFR_RNDZ);

        mpfr_add(temp_var_31, (temp_var_29), (temp_var_30), MPFR_RNDZ);

        mpfr_mul_d(temp_var_32, (temp_var_31), 0.128, MPFR_RNDZ);

        mpfr_div_d(temp_var_33, K_cam, 13.0, MPFR_RNDZ);

        mpfr_add_si(temp_var_34, (temp_var_33), 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_35, Mg_cam, 0.153, MPFR_RNDZ);

        mpfr_sub(temp_var_36, (temp_var_34), (temp_var_35), MPFR_RNDZ);
        mpfr_mul(Kd24_cam, (temp_var_32), (temp_var_36), MPFR_RNDZ);
;
  }
  else
  {
    
        mpfr_div_d(temp_var_37, K_cam, 0.94, MPFR_RNDZ);

        mpfr_add_si(temp_var_38, (temp_var_37), 1, MPFR_RNDZ);

        mpfr_sub_d(temp_var_39, (temp_var_38), (1 / 0.012), MPFR_RNDZ);

        mpfr_sub_si(temp_var_40, Mg_cam, 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_41, (temp_var_40), 0.060, MPFR_RNDZ);

        mpfr_add(temp_var_42, (temp_var_39), (temp_var_41), MPFR_RNDZ);

        mpfr_mul_d(temp_var_43, (temp_var_42), 0.0025, MPFR_RNDZ);

        mpfr_div_d(temp_var_44, K_cam, 8.1, MPFR_RNDZ);

        mpfr_add_si(temp_var_45, (temp_var_44), 1, MPFR_RNDZ);

        mpfr_add_d(temp_var_46, (temp_var_45), (1 / 0.022), MPFR_RNDZ);

        mpfr_sub_si(temp_var_47, Mg_cam, 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_48, (temp_var_47), 0.068, MPFR_RNDZ);

        mpfr_add(temp_var_49, (temp_var_46), (temp_var_48), MPFR_RNDZ);
        mpfr_mul(Kd02_cam, (temp_var_43), (temp_var_49), MPFR_RNDZ);
;
    
        mpfr_div_d(temp_var_50, K_cam, 0.64, MPFR_RNDZ);

        mpfr_add_si(temp_var_51, (temp_var_50), 1, MPFR_RNDZ);

        mpfr_add_d(temp_var_52, (temp_var_51), (1 / 0.0014), MPFR_RNDZ);

        mpfr_sub_si(temp_var_53, Mg_cam, 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_54, (temp_var_53), 0.005, MPFR_RNDZ);

        mpfr_add(temp_var_55, (temp_var_52), (temp_var_54), MPFR_RNDZ);

        mpfr_mul_d(temp_var_56, (temp_var_55), 0.128, MPFR_RNDZ);

        mpfr_div_d(temp_var_57, K_cam, 13.0, MPFR_RNDZ);

        mpfr_add_si(temp_var_58, (temp_var_57), 1, MPFR_RNDZ);

        mpfr_sub_d(temp_var_59, (temp_var_58), (1 / 0.153), MPFR_RNDZ);

        mpfr_sub_si(temp_var_60, Mg_cam, 1, MPFR_RNDZ);

        mpfr_div_d(temp_var_61, (temp_var_60), 0.150, MPFR_RNDZ);

        mpfr_add(temp_var_62, (temp_var_59), (temp_var_61), MPFR_RNDZ);
        mpfr_mul(Kd24_cam, (temp_var_56), (temp_var_62), MPFR_RNDZ);
;
  }

  mpfr_set_d(k20_cam, 10, MPFR_RNDZ);
      mpfr_div(k02_cam, k20_cam, Kd02_cam, MPFR_RNDZ);
;
  mpfr_set_d(k42_cam, 500, MPFR_RNDZ);
      mpfr_div(k24_cam, k42_cam, Kd24_cam, MPFR_RNDZ);
;
  mpfr_set_d(k0Boff_cam, 0.0014, MPFR_RNDZ);
      mpfr_div_d(k0Bon_cam, k0Boff_cam, 0.2, MPFR_RNDZ);
;
      mpfr_div_si(k2Boff_cam, k0Boff_cam, 100, MPFR_RNDZ);
;
  mpfr_set(k2Bon_cam, k0Bon_cam, MPFR_RNDZ);
  mpfr_set(k4Boff_cam, k2Boff_cam, MPFR_RNDZ);
  mpfr_set(k4Bon_cam, k0Bon_cam, MPFR_RNDZ);
      mpfr_div_si(k20B_cam, k20_cam, 100, MPFR_RNDZ);
;
  mpfr_set(k02B_cam, k02_cam, MPFR_RNDZ);
  mpfr_set(k42B_cam, k42_cam, MPFR_RNDZ);
  mpfr_set(k24B_cam, k24_cam, MPFR_RNDZ);
  mpfr_set_d(kbi_cam, 2.2, MPFR_RNDZ);
      mpfr_div_d(kib_cam, kbi_cam, 33.5e-3, MPFR_RNDZ);
;
  mpfr_set_d(kpp1_cam, 1.72, MPFR_RNDZ);
  mpfr_set_d(Kmpp1_cam, 11.5, MPFR_RNDZ);
  mpfr_set(kib2_cam, kib_cam, MPFR_RNDZ);
      mpfr_mul_si(kb2i_cam, kib2_cam, 5, MPFR_RNDZ);
;
  mpfr_set(kb24_cam, k24_cam, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_63, k42_cam, 33.5e-3, MPFR_RNDZ);
    mpfr_div_si(kb42_cam, (temp_var_63), 5, MPFR_RNDZ);
;
      mpfr_div_si(kta_cam, kbi_cam, 1000, MPFR_RNDZ);
;
  mpfr_set(kat_cam, kib_cam, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_64, k42_cam, 33.5e-6, MPFR_RNDZ);
    mpfr_div_si(kt42_cam, (temp_var_64), 5, MPFR_RNDZ);
;
  mpfr_set(kt24_cam, k24_cam, MPFR_RNDZ);
  mpfr_set(kat2_cam, kib_cam, MPFR_RNDZ);
      mpfr_mul_si(kt2a_cam, kib_cam, 5, MPFR_RNDZ);
;
  mpfr_set_d(kcanCaoff_cam, 1, MPFR_RNDZ);
      mpfr_div_d(kcanCaon_cam, kcanCaoff_cam, 0.5, MPFR_RNDZ);
;
  mpfr_set_d(kcanCaM4on_cam, 46, MPFR_RNDZ);
  mpfr_set_d(kcanCaM4off_cam, 0.0013, MPFR_RNDZ);
  mpfr_set(kcanCaM2on_cam, kcanCaM4on_cam, MPFR_RNDZ);
      mpfr_mul_si(kcanCaM2off_cam, kcanCaM4off_cam, 2508, MPFR_RNDZ);
;
  mpfr_set(kcanCaM0on_cam, kcanCaM4on_cam, MPFR_RNDZ);
      mpfr_mul_si(kcanCaM0off_cam, kcanCaM2off_cam, 165, MPFR_RNDZ);
;
  mpfr_set(k02can_cam, k02_cam, MPFR_RNDZ);
      mpfr_div_si(k20can_cam, k20_cam, 165, MPFR_RNDZ);
;
  mpfr_set(k24can_cam, k24_cam, MPFR_RNDZ);
      mpfr_div_si(k42can_cam, k20_cam, 2508, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_65, k02_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_66, (temp_var_65), CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_67, k20_cam, Ca2CaM_cam, MPFR_RNDZ);
    mpfr_sub(rcn02_cam, (temp_var_66), (temp_var_67), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_68, k24_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_69, (temp_var_68), Ca2CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_70, k42_cam, Ca4CaM_cam, MPFR_RNDZ);
    mpfr_sub(rcn24_cam, (temp_var_69), (temp_var_70), MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_71, Btot_cam, CaMB_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_72, (temp_var_71), Ca2CaMB_cam, MPFR_RNDZ);
    mpfr_sub(B_cam, (temp_var_72), Ca4CaMB_cam, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_73, k02B_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_74, (temp_var_73), CaMB_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_75, k20B_cam, Ca2CaMB_cam, MPFR_RNDZ);
    mpfr_sub(rcn02B_cam, (temp_var_74), (temp_var_75), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_76, k24B_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_77, (temp_var_76), Ca2CaMB_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_78, k42B_cam, Ca4CaMB_cam, MPFR_RNDZ);
    mpfr_sub(rcn24B_cam, (temp_var_77), (temp_var_78), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_79, k0Bon_cam, CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_80, (temp_var_79), B_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_81, k0Boff_cam, CaMB_cam, MPFR_RNDZ);
    mpfr_sub(rcn0B_cam, (temp_var_80), (temp_var_81), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_82, k2Bon_cam, Ca2CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_83, (temp_var_82), B_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_84, k2Boff_cam, Ca2CaMB_cam, MPFR_RNDZ);
    mpfr_sub(rcn2B_cam, (temp_var_83), (temp_var_84), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_85, k4Bon_cam, Ca4CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_86, (temp_var_85), B_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_87, k4Boff_cam, Ca4CaMB_cam, MPFR_RNDZ);
    mpfr_sub(rcn4B_cam, (temp_var_86), (temp_var_87), MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_88, CaNtot_cam, Ca4CaN_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_89, (temp_var_88), CaMCa4CaN_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_90, (temp_var_89), Ca2CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(Ca2CaN_cam, (temp_var_90), Ca4CaMCa4CaN_cam, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_91, kcanCaon_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_92, (temp_var_91), Ca2CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_93, kcanCaoff_cam, Ca4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcnCa4CaN_cam, (temp_var_92), (temp_var_93), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_94, k02can_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_95, (temp_var_94), CaMCa4CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_96, k20can_cam, Ca2CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcn02CaN_cam, (temp_var_95), (temp_var_96), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_97, k24can_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_98, (temp_var_97), Ca2CaMCa4CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_99, k42can_cam, Ca4CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcn24CaN_cam, (temp_var_98), (temp_var_99), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_100, kcanCaM0on_cam, CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_101, (temp_var_100), Ca4CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_102, kcanCaM0off_cam, CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcn0CaN_cam, (temp_var_101), (temp_var_102), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_103, kcanCaM2on_cam, Ca2CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_104, (temp_var_103), Ca4CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_105, kcanCaM2off_cam, Ca2CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcn2CaN_cam, (temp_var_104), (temp_var_105), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_106, kcanCaM4on_cam, Ca4CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_107, (temp_var_106), Ca4CaN_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_108, kcanCaM4off_cam, Ca4CaMCa4CaN_cam, MPFR_RNDZ);
    mpfr_sub(rcn4CaN_cam, (temp_var_107), (temp_var_108), MPFR_RNDZ);
;
  
    mpfr_si_sub(temp_var_109, 1, Pb2_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_110, (temp_var_109), Pb_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_111, (temp_var_110), Pt_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_112, (temp_var_111), Pt2_cam, MPFR_RNDZ);
    mpfr_sub(Pix_cam, (temp_var_112), Pa_cam, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_113, kib2_cam, Ca2CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_114, (temp_var_113), Pix_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_115, kb2i_cam, Pb2_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKib2_cam, (temp_var_114), (temp_var_115), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_116, kb24_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_117, (temp_var_116), Pb2_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_118, kb42_cam, Pb_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKb2b_cam, (temp_var_117), (temp_var_118), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_119, kib_cam, Ca4CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_120, (temp_var_119), Pix_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_121, kbi_cam, Pb_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKib_cam, (temp_var_120), (temp_var_121), MPFR_RNDZ);
;
  
    mpfr_add(temp_var_122, Pb_cam, Pt_cam, MPFR_RNDZ);

    mpfr_add(temp_var_123, (temp_var_122), Pt2_cam, MPFR_RNDZ);
    mpfr_add(T_cam, (temp_var_123), Pa_cam, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_124, T_cam, 0.055, MPFR_RNDZ);


    mpfr_add_d(temp_var_126, (temp_var_124), (0.0074 * pow(mpfr_get_d(T_cam, MPFR_RNDZ), 2)), MPFR_RNDZ);

    mpfr_add_d(kbt_cam, (temp_var_126), (0.015 * pow(mpfr_get_d(T_cam, MPFR_RNDZ), 3)), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_128, kbt_cam, Pb_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_129, kpp1_cam, PP1tot_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_130, (temp_var_129), Pt_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_131, CaMKIItot_cam, Pt_cam, MPFR_RNDZ);

    mpfr_add(temp_var_132, Kmpp1_cam, (temp_var_131), MPFR_RNDZ);

    mpfr_div(temp_var_133, (temp_var_130), (temp_var_132), MPFR_RNDZ);
    mpfr_sub(rcnCKbt_cam, (temp_var_128), (temp_var_133), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_134, kt42_cam, Pt_cam, MPFR_RNDZ);

    mpfr_mul_d(temp_var_135, kt24_cam, pow(Ca, 2), MPFR_RNDZ);

    mpfr_mul(temp_var_136, (temp_var_135), Pt2_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKtt2_cam, (temp_var_134), (temp_var_136), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_137, kta_cam, Pt_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_138, kat_cam, Ca4CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_139, (temp_var_138), Pa_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKta_cam, (temp_var_137), (temp_var_139), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_140, kt2a_cam, Pt2_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_141, kat2_cam, Ca2CaM_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_142, (temp_var_141), Pa_cam, MPFR_RNDZ);
    mpfr_sub(rcnCKt2a_cam, (temp_var_140), (temp_var_142), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_143, kpp1_cam, PP1tot_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_144, (temp_var_143), Pt2_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_145, CaMKIItot_cam, Pt2_cam, MPFR_RNDZ);

    mpfr_add(temp_var_146, Kmpp1_cam, (temp_var_145), MPFR_RNDZ);
    mpfr_div(rcnCKt2b2_cam, (temp_var_144), (temp_var_146), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_147, kpp1_cam, PP1tot_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_148, (temp_var_147), Pa_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_149, CaMKIItot_cam, Pa_cam, MPFR_RNDZ);

    mpfr_add(temp_var_150, Kmpp1_cam, (temp_var_149), MPFR_RNDZ);
    mpfr_div(rcnCKai_cam, (temp_var_148), (temp_var_150), MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_152, (rcn02_cam), MPFR_RNDZ);
mpfr_sub(temp_var_151, temp_var_152, rcn0B_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_153, (temp_var_151), rcn0CaN_cam, MPFR_RNDZ);
    mpfr_mul_d(dCaM_cam, (temp_var_153), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_154, rcn02_cam, rcn24_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_155, (temp_var_154), rcn2B_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_156, (temp_var_155), rcn2CaN_cam, MPFR_RNDZ);

    
mpfr_neg (temp_var_158, (rcnCKib2_cam), MPFR_RNDZ);
mpfr_add(temp_var_157, temp_var_158, rcnCKt2a_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_159, CaMKIItot_cam, (temp_var_157), MPFR_RNDZ);

    mpfr_add(temp_var_160, (temp_var_156), (temp_var_159), MPFR_RNDZ);
    mpfr_mul_d(dCa2CaM_cam, (temp_var_160), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_161, rcn24_cam, rcn4B_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_162, (temp_var_161), rcn4CaN_cam, MPFR_RNDZ);

    
mpfr_neg (temp_var_164, (rcnCKib_cam), MPFR_RNDZ);
mpfr_add(temp_var_163, temp_var_164, rcnCKta_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_165, CaMKIItot_cam, (temp_var_163), MPFR_RNDZ);

    mpfr_add(temp_var_166, (temp_var_162), (temp_var_165), MPFR_RNDZ);
    mpfr_mul_d(dCa4CaM_cam, (temp_var_166), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_167, rcn0B_cam, rcn02B_cam, MPFR_RNDZ);
    mpfr_mul_d(dCaMB_cam, (temp_var_167), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_168, rcn02B_cam, rcn2B_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_169, (temp_var_168), rcn24B_cam, MPFR_RNDZ);
    mpfr_mul_d(dCa2CaMB_cam, (temp_var_169), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_170, rcn24B_cam, rcn4B_cam, MPFR_RNDZ);
    mpfr_mul_d(dCa4CaMB_cam, (temp_var_170), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_171, rcnCKib2_cam, rcnCKb2b_cam, MPFR_RNDZ);

    mpfr_add(temp_var_172, (temp_var_171), rcnCKt2b2_cam, MPFR_RNDZ);
    mpfr_mul_d(dPb2_cam, (temp_var_172), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_173, rcnCKib_cam, rcnCKb2b_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_174, (temp_var_173), rcnCKbt_cam, MPFR_RNDZ);
    mpfr_mul_d(dPb_cam, (temp_var_174), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_175, rcnCKbt_cam, rcnCKta_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_176, (temp_var_175), rcnCKtt2_cam, MPFR_RNDZ);
    mpfr_mul_d(dPt_cam, (temp_var_176), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_177, rcnCKtt2_cam, rcnCKt2a_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_178, (temp_var_177), rcnCKt2b2_cam, MPFR_RNDZ);
    mpfr_mul_d(dPt2_cam, (temp_var_178), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_179, rcnCKta_cam, rcnCKt2a_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_180, (temp_var_179), rcnCKai_cam, MPFR_RNDZ);
    mpfr_mul_d(dPa_cam, (temp_var_180), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_181, rcnCa4CaN_cam, rcn0CaN_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_182, (temp_var_181), rcn2CaN_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_183, (temp_var_182), rcn4CaN_cam, MPFR_RNDZ);
    mpfr_mul_d(dCa4CaN_cam, (temp_var_183), 1e-3, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_184, rcn0CaN_cam, rcn02CaN_cam, MPFR_RNDZ);
    mpfr_mul_d(dCaMCa4CaN_cam, (temp_var_184), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_185, rcn2CaN_cam, rcn02CaN_cam, MPFR_RNDZ);

    mpfr_sub(temp_var_186, (temp_var_185), rcn24CaN_cam, MPFR_RNDZ);
    mpfr_mul_d(dCa2CaMCa4CaN_cam, (temp_var_186), 1e-3, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_187, rcn4CaN_cam, rcn24CaN_cam, MPFR_RNDZ);
    mpfr_mul_d(dCa4CaMCa4CaN_cam, (temp_var_187), 1e-3, MPFR_RNDZ);
;
  
finavalu[offset_1] = mpfr_get_d(dCaM_cam, MPFR_RNDZ);
  
finavalu[offset_2] = mpfr_get_d(dCa2CaM_cam, MPFR_RNDZ);
  
finavalu[offset_3] = mpfr_get_d(dCa4CaM_cam, MPFR_RNDZ);
  
finavalu[offset_4] = mpfr_get_d(dCaMB_cam, MPFR_RNDZ);
  
finavalu[offset_5] = mpfr_get_d(dCa2CaMB_cam, MPFR_RNDZ);
  
finavalu[offset_6] = mpfr_get_d(dCa4CaMB_cam, MPFR_RNDZ);
  
finavalu[offset_7] = mpfr_get_d(dPb2_cam, MPFR_RNDZ);
  
finavalu[offset_8] = mpfr_get_d(dPb_cam, MPFR_RNDZ);
  
finavalu[offset_9] = mpfr_get_d(dPt_cam, MPFR_RNDZ);
  
finavalu[offset_10] = mpfr_get_d(dPt2_cam, MPFR_RNDZ);
  
finavalu[offset_11] = mpfr_get_d(dPa_cam, MPFR_RNDZ);
  
finavalu[offset_12] = mpfr_get_d(dCa4CaN_cam, MPFR_RNDZ);
  
finavalu[offset_13] = mpfr_get_d(dCaMCa4CaN_cam, MPFR_RNDZ);
  
finavalu[offset_14] = mpfr_get_d(dCa2CaMCa4CaN_cam, MPFR_RNDZ);
  
finavalu[offset_15] = mpfr_get_d(dCa4CaMCa4CaN_cam, MPFR_RNDZ);
  
    mpfr_mul_si(temp_var_188, CaMKIItot_cam, 2, MPFR_RNDZ);

    mpfr_sub(temp_var_189, rcnCKtt2_cam, rcnCKb2b_cam, MPFR_RNDZ);

    mpfr_mul(temp_var_190, (temp_var_188), (temp_var_189), MPFR_RNDZ);

    mpfr_add(temp_var_191, rcn02_cam, rcn24_cam, MPFR_RNDZ);

    mpfr_add(temp_var_192, (temp_var_191), rcn02B_cam, MPFR_RNDZ);

    mpfr_add(temp_var_193, (temp_var_192), rcn24B_cam, MPFR_RNDZ);

    mpfr_add(temp_var_194, (temp_var_193), rcnCa4CaN_cam, MPFR_RNDZ);

    mpfr_add(temp_var_195, (temp_var_194), rcn02CaN_cam, MPFR_RNDZ);

    mpfr_add(temp_var_196, (temp_var_195), rcn24CaN_cam, MPFR_RNDZ);

    mpfr_mul_si(temp_var_197, (temp_var_196), 2, MPFR_RNDZ);

    mpfr_sub(temp_var_198, (temp_var_190), (temp_var_197), MPFR_RNDZ);
    mpfr_mul_d(JCa_cam, (temp_var_198), 1e-3, MPFR_RNDZ);
;
  return mpfr_get_d(JCa_cam, MPFR_RNDZ);
}

//end of conversion, hopefully it will work :)
