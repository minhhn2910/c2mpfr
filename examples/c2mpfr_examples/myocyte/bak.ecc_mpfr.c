#include <mpfr.h>
#define LEN 289 
 int config_vals[LEN];
  mpfr_t initvalu_1_ecc;
  mpfr_t initvalu_2_ecc;
  mpfr_t initvalu_3_ecc;
  mpfr_t initvalu_4_ecc;
  mpfr_t initvalu_5_ecc;
  mpfr_t initvalu_6_ecc;
  mpfr_t initvalu_7_ecc;
  mpfr_t initvalu_8_ecc;
  mpfr_t initvalu_9_ecc;
  mpfr_t initvalu_10_ecc;
  mpfr_t initvalu_11_ecc;
  mpfr_t initvalu_12_ecc;
  mpfr_t initvalu_13_ecc;
  mpfr_t initvalu_14_ecc;
  mpfr_t initvalu_15_ecc;
  mpfr_t initvalu_16_ecc;
  mpfr_t initvalu_17_ecc;
  mpfr_t initvalu_18_ecc;
  mpfr_t initvalu_19_ecc;
  mpfr_t initvalu_20_ecc;
  mpfr_t initvalu_21_ecc;
  mpfr_t initvalu_22_ecc;
  mpfr_t initvalu_23_ecc;
  mpfr_t initvalu_24_ecc;
  mpfr_t initvalu_25_ecc;
  mpfr_t initvalu_26_ecc;
  mpfr_t initvalu_27_ecc;
  mpfr_t initvalu_28_ecc;
  mpfr_t initvalu_29_ecc;
  mpfr_t initvalu_30_ecc;
  mpfr_t initvalu_31_ecc;
  mpfr_t initvalu_32_ecc;
  mpfr_t initvalu_33_ecc;
  mpfr_t initvalu_34_ecc;
  mpfr_t initvalu_35_ecc;
  mpfr_t initvalu_36_ecc;
  mpfr_t initvalu_37_ecc;
  mpfr_t initvalu_38_ecc;
  mpfr_t initvalu_39_ecc;
  mpfr_t initvalu_40_ecc;
  mpfr_t initvalu_41_ecc;
  mpfr_t initvalu_42_ecc;
  mpfr_t initvalu_43_ecc;
  mpfr_t initvalu_44_ecc;
  mpfr_t initvalu_45_ecc;
  mpfr_t initvalu_46_ecc;
  mpfr_t parameter_1_ecc;
  mpfr_t pi_ecc;
  mpfr_t R_ecc;
  mpfr_t Frdy_ecc;
  mpfr_t Temp_ecc;
  mpfr_t FoRT_ecc;
  mpfr_t Cmem_ecc;
  mpfr_t Qpow_ecc;
  mpfr_t cellLength_ecc;
  mpfr_t cellRadius_ecc;
  mpfr_t junctionLength_ecc;
  mpfr_t junctionRadius_ecc;
  mpfr_t distSLcyto_ecc;
  mpfr_t distJuncSL_ecc;
  mpfr_t DcaJuncSL_ecc;
  mpfr_t DcaSLcyto_ecc;
  mpfr_t DnaJuncSL_ecc;
  mpfr_t DnaSLcyto_ecc;
  mpfr_t Vcell_ecc;
  mpfr_t Vmyo_ecc;
  mpfr_t Vsr_ecc;
  mpfr_t Vsl_ecc;
  mpfr_t Vjunc_ecc;
  mpfr_t SAjunc_ecc;
  mpfr_t SAsl_ecc;
  mpfr_t J_ca_juncsl_ecc;
  mpfr_t J_ca_slmyo_ecc;
  mpfr_t J_na_juncsl_ecc;
  mpfr_t J_na_slmyo_ecc;
  mpfr_t Fjunc_ecc;
  mpfr_t Fsl_ecc;
  mpfr_t Fjunc_CaL_ecc;
  mpfr_t Fsl_CaL_ecc;
  mpfr_t Cli_ecc;
  mpfr_t Clo_ecc;
  mpfr_t Ko_ecc;
  mpfr_t Nao_ecc;
  mpfr_t Cao_ecc;
  mpfr_t Mgi_ecc;
  mpfr_t ena_junc_ecc;
  mpfr_t ena_sl_ecc;
  mpfr_t ek_ecc;
  mpfr_t eca_junc_ecc;
  mpfr_t eca_sl_ecc;
  mpfr_t ecl_ecc;
  mpfr_t GNa_ecc;
  mpfr_t GNaB_ecc;
  mpfr_t IbarNaK_ecc;
  mpfr_t KmNaip_ecc;
  mpfr_t KmKo_ecc;
  mpfr_t Q10NaK_ecc;
  mpfr_t Q10KmNai_ecc;
  mpfr_t pNaK_ecc;
  mpfr_t GtoSlow_ecc;
  mpfr_t GtoFast_ecc;
  mpfr_t gkp_ecc;
  mpfr_t GClCa_ecc;
  mpfr_t GClB_ecc;
  mpfr_t KdClCa_ecc;
  mpfr_t pNa_ecc;
  mpfr_t pCa_ecc;
  mpfr_t pK_ecc;
  mpfr_t KmCa_ecc;
  mpfr_t Q10CaL_ecc;
  mpfr_t IbarNCX_ecc;
  mpfr_t KmCai_ecc;
  mpfr_t KmCao_ecc;
  mpfr_t KmNai_ecc;
  mpfr_t KmNao_ecc;
  mpfr_t ksat_ecc;
  mpfr_t nu_ecc;
  mpfr_t Kdact_ecc;
  mpfr_t Q10NCX_ecc;
  mpfr_t IbarSLCaP_ecc;
  mpfr_t KmPCa_ecc;
  mpfr_t GCaB_ecc;
  mpfr_t Q10SLCaP_ecc;
  mpfr_t Q10SRCaP_ecc;
  mpfr_t Vmax_SRCaP_ecc;
  mpfr_t Kmf_ecc;
  mpfr_t Kmr_ecc;
  mpfr_t hillSRCaP_ecc;
  mpfr_t ks_ecc;
  mpfr_t koCa_ecc;
  mpfr_t kom_ecc;
  mpfr_t kiCa_ecc;
  mpfr_t kim_ecc;
  mpfr_t ec50SR_ecc;
  mpfr_t Bmax_Naj_ecc;
  mpfr_t Bmax_Nasl_ecc;
  mpfr_t koff_na_ecc;
  mpfr_t kon_na_ecc;
  mpfr_t Bmax_TnClow_ecc;
  mpfr_t koff_tncl_ecc;
  mpfr_t kon_tncl_ecc;
  mpfr_t Bmax_TnChigh_ecc;
  mpfr_t koff_tnchca_ecc;
  mpfr_t kon_tnchca_ecc;
  mpfr_t koff_tnchmg_ecc;
  mpfr_t kon_tnchmg_ecc;
  mpfr_t Bmax_CaM_ecc;
  mpfr_t koff_cam_ecc;
  mpfr_t kon_cam_ecc;
  mpfr_t Bmax_myosin_ecc;
  mpfr_t koff_myoca_ecc;
  mpfr_t kon_myoca_ecc;
  mpfr_t koff_myomg_ecc;
  mpfr_t kon_myomg_ecc;
  mpfr_t Bmax_SR_ecc;
  mpfr_t koff_sr_ecc;
  mpfr_t kon_sr_ecc;
  mpfr_t Bmax_SLlowsl_ecc;
  mpfr_t Bmax_SLlowj_ecc;
  mpfr_t koff_sll_ecc;
  mpfr_t kon_sll_ecc;
  mpfr_t Bmax_SLhighsl_ecc;
  mpfr_t Bmax_SLhighj_ecc;
  mpfr_t koff_slh_ecc;
  mpfr_t kon_slh_ecc;
  mpfr_t Bmax_Csqn_ecc;
  mpfr_t koff_csqn_ecc;
  mpfr_t kon_csqn_ecc;
  mpfr_t am_ecc;
  mpfr_t bm_ecc;
  mpfr_t ah_ecc;
  mpfr_t bh_ecc;
  mpfr_t aj_ecc;
  mpfr_t bj_ecc;
  mpfr_t I_Na_junc_ecc;
  mpfr_t I_Na_sl_ecc;
  mpfr_t I_Na_ecc;
  mpfr_t I_nabk_junc_ecc;
  mpfr_t I_nabk_sl_ecc;
  mpfr_t I_nabk_ecc;
  mpfr_t sigma_ecc;
  mpfr_t fnak_ecc;
  mpfr_t I_nak_junc_ecc;
  mpfr_t I_nak_sl_ecc;
  mpfr_t I_nak_ecc;
  mpfr_t gkr_ecc;
  mpfr_t xrss_ecc;
  mpfr_t tauxr_ecc;
  mpfr_t rkr_ecc;
  mpfr_t I_kr_ecc;
  mpfr_t pcaks_junc_ecc;
  mpfr_t pcaks_sl_ecc;
  mpfr_t gks_junc_ecc;
  mpfr_t gks_sl_ecc;
  mpfr_t eks_ecc;
  mpfr_t xsss_ecc;
  mpfr_t tauxs_ecc;
  mpfr_t I_ks_junc_ecc;
  mpfr_t I_ks_sl_ecc;
  mpfr_t I_ks_ecc;
  mpfr_t kp_kp_ecc;
  mpfr_t I_kp_junc_ecc;
  mpfr_t I_kp_sl_ecc;
  mpfr_t I_kp_ecc;
  mpfr_t xtoss_ecc;
  mpfr_t ytoss_ecc;
  mpfr_t rtoss_ecc;
  mpfr_t tauxtos_ecc;
  mpfr_t tauytos_ecc;
  mpfr_t taurtos_ecc;
  mpfr_t I_tos_ecc;
  mpfr_t tauxtof_ecc;
  mpfr_t tauytof_ecc;
  mpfr_t I_tof_ecc;
  mpfr_t I_to_ecc;
  mpfr_t aki_ecc;
  mpfr_t bki_ecc;
  mpfr_t kiss_ecc;
  mpfr_t I_ki_ecc;
  mpfr_t I_ClCa_junc_ecc;
  mpfr_t I_ClCa_sl_ecc;
  mpfr_t I_ClCa_ecc;
  mpfr_t I_Clbk_ecc;
  mpfr_t dss_ecc;
  mpfr_t taud_ecc;
  mpfr_t fss_ecc;
  mpfr_t tauf_ecc;
  mpfr_t ibarca_j_ecc;
  mpfr_t ibarca_sl_ecc;
  mpfr_t ibark_ecc;
  mpfr_t ibarna_j_ecc;
  mpfr_t ibarna_sl_ecc;
  mpfr_t I_Ca_junc_ecc;
  mpfr_t I_Ca_sl_ecc;
  mpfr_t I_Ca_ecc;
  mpfr_t I_CaK_ecc;
  mpfr_t I_CaNa_junc_ecc;
  mpfr_t I_CaNa_sl_ecc;
  mpfr_t I_CaNa_ecc;
  mpfr_t I_Catot_ecc;
  mpfr_t Ka_junc_ecc;
  mpfr_t Ka_sl_ecc;
  mpfr_t s1_junc_ecc;
  mpfr_t s1_sl_ecc;
  mpfr_t s2_junc_ecc;
  mpfr_t s3_junc_ecc;
  mpfr_t s2_sl_ecc;
  mpfr_t s3_sl_ecc;
  mpfr_t I_ncx_junc_ecc;
  mpfr_t I_ncx_sl_ecc;
  mpfr_t I_ncx_ecc;
  mpfr_t I_pca_junc_ecc;
  mpfr_t I_pca_sl_ecc;
  mpfr_t I_pca_ecc;
  mpfr_t I_cabk_junc_ecc;
  mpfr_t I_cabk_sl_ecc;
  mpfr_t I_cabk_ecc;
  mpfr_t MaxSR_ecc;
  mpfr_t MinSR_ecc;
  mpfr_t kCaSR_ecc;
  mpfr_t koSRCa_ecc;
  mpfr_t kiSRCa_ecc;
  mpfr_t RI_ecc;
  mpfr_t J_SRCarel_ecc;
  mpfr_t J_serca_ecc;
  mpfr_t J_SRleak_ecc;
  mpfr_t J_CaB_cytosol_ecc;
  mpfr_t J_CaB_junction_ecc;
  mpfr_t J_CaB_sl_ecc;
  mpfr_t oneovervsr_ecc;
  mpfr_t I_Na_tot_junc_ecc;
  mpfr_t I_Na_tot_sl_ecc;
  mpfr_t oneovervsl_ecc;
  mpfr_t I_K_tot_ecc;
  mpfr_t I_Ca_tot_junc_ecc;
  mpfr_t I_Ca_tot_sl_ecc;
  mpfr_t junc_sl_ecc;
  mpfr_t sl_junc_ecc;
  mpfr_t sl_myo_ecc;
  mpfr_t myo_sl_ecc;
  mpfr_t I_app_ecc;
  mpfr_t V_hold_ecc;
  mpfr_t V_test_ecc;
  mpfr_t V_clamp_ecc;
  mpfr_t R_clamp_ecc;
  mpfr_t I_Na_tot_ecc;
  mpfr_t I_Cl_tot_ecc;
  mpfr_t I_Ca_tot_ecc;
  mpfr_t I_tot_ecc;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
    mpfr_t temp_var_11;
    mpfr_t temp_var_12;
    mpfr_t temp_var_13;
    mpfr_t temp_var_14;
    mpfr_t temp_var_15;
    mpfr_t temp_var_16;
    mpfr_t temp_var_17;
    mpfr_t temp_var_18;
    mpfr_t temp_var_19;
    mpfr_t temp_var_20;
    mpfr_t temp_var_21;
    mpfr_t temp_var_22;
    mpfr_t temp_var_23;
    mpfr_t temp_var_24;
    mpfr_t temp_var_25;
    mpfr_t temp_var_26;
    mpfr_t temp_var_27;
    mpfr_t temp_var_28;
    mpfr_t temp_var_29;
    mpfr_t temp_var_30;
    mpfr_t temp_var_31;
    mpfr_t temp_var_32;
    mpfr_t temp_var_33;
    mpfr_t temp_var_34;
    mpfr_t temp_var_35;
    mpfr_t temp_var_36;
    mpfr_t temp_var_37;
    mpfr_t temp_var_38;
    mpfr_t temp_var_39;
    mpfr_t temp_var_40;
    mpfr_t temp_var_41;
    mpfr_t temp_var_42;
    mpfr_t temp_var_43;
    mpfr_t temp_var_44;
    mpfr_t temp_var_45;
    mpfr_t temp_var_46;
    mpfr_t temp_var_47;
    mpfr_t temp_var_48;
    mpfr_t temp_var_49;
    mpfr_t temp_var_50;
    mpfr_t temp_var_51;
    mpfr_t temp_var_52;
    mpfr_t temp_var_53;
    mpfr_t temp_var_54;
    mpfr_t temp_var_55;
    mpfr_t temp_var_56;
    mpfr_t temp_var_57;
    mpfr_t temp_var_58;
    mpfr_t temp_var_59;
    mpfr_t temp_var_60;
    mpfr_t temp_var_61;
    mpfr_t temp_var_62;
    mpfr_t temp_var_63;
    mpfr_t temp_var_64;
    mpfr_t temp_var_65;
    mpfr_t temp_var_66;
    mpfr_t temp_var_67;
    mpfr_t temp_var_68;
    mpfr_t temp_var_69;
    mpfr_t temp_var_70;
    mpfr_t temp_var_71;
    mpfr_t temp_var_72;
    mpfr_t temp_var_73;
    mpfr_t temp_var_74;
    mpfr_t temp_var_75;
    mpfr_t temp_var_76;
    mpfr_t temp_var_77;
    mpfr_t temp_var_78;
    mpfr_t temp_var_79;
    mpfr_t temp_var_80;
        mpfr_t temp_var_81;
        mpfr_t temp_var_82;
        mpfr_t temp_var_83;
        mpfr_t temp_var_84;
        mpfr_t temp_var_85;
        mpfr_t temp_var_86;
        mpfr_t temp_var_87;
        mpfr_t temp_var_88;
        mpfr_t temp_var_89;
        mpfr_t temp_var_90;
        mpfr_t temp_var_91;
        mpfr_t temp_var_92;
        mpfr_t temp_var_93;
        mpfr_t temp_var_94;
        mpfr_t temp_var_95;
        mpfr_t temp_var_96;
        mpfr_t temp_var_97;
        mpfr_t temp_var_98;
        mpfr_t temp_var_99;
        mpfr_t temp_var_100;
        mpfr_t temp_var_101;
        mpfr_t temp_var_102;
        mpfr_t temp_var_103;
        mpfr_t temp_var_104;
        mpfr_t temp_var_105;
    mpfr_t temp_var_106;
    mpfr_t temp_var_107;
    mpfr_t temp_var_108;
    mpfr_t temp_var_109;
    mpfr_t temp_var_110;
    mpfr_t temp_var_111;
    mpfr_t temp_var_112;
    mpfr_t temp_var_113;
    mpfr_t temp_var_114;
    mpfr_t temp_var_115;
    mpfr_t temp_var_116;
    mpfr_t temp_var_117;
    mpfr_t temp_var_118;
    mpfr_t temp_var_119;
    mpfr_t temp_var_120;
    mpfr_t temp_var_121;
    mpfr_t temp_var_122;
    mpfr_t temp_var_123;
    mpfr_t temp_var_124;
    mpfr_t temp_var_125;
    mpfr_t temp_var_126;
    mpfr_t temp_var_127;
    mpfr_t temp_var_128;
    mpfr_t temp_var_129;
    mpfr_t temp_var_130;
    mpfr_t temp_var_131;
    mpfr_t temp_var_132;
    mpfr_t temp_var_133;
    mpfr_t temp_var_134;
    mpfr_t temp_var_135;
    mpfr_t temp_var_136;
    mpfr_t temp_var_137;
    mpfr_t temp_var_138;
    mpfr_t temp_var_139;
    mpfr_t temp_var_140;
    mpfr_t temp_var_141;
    mpfr_t temp_var_142;
    mpfr_t temp_var_143;
    mpfr_t temp_var_144;
    mpfr_t temp_var_145;
    mpfr_t temp_var_146;
    mpfr_t temp_var_147;
    mpfr_t temp_var_148;
    mpfr_t temp_var_149;
    mpfr_t temp_var_150;
    mpfr_t temp_var_151;
    mpfr_t temp_var_152;
    mpfr_t temp_var_153;
    mpfr_t temp_var_154;
    mpfr_t temp_var_155;
    mpfr_t temp_var_156;
    mpfr_t temp_var_157;
    mpfr_t temp_var_158;
    mpfr_t temp_var_159;
    mpfr_t temp_var_160;
    mpfr_t temp_var_161;
    mpfr_t temp_var_162;
    mpfr_t temp_var_163;
    mpfr_t temp_var_164;
    mpfr_t temp_var_165;
    mpfr_t temp_var_166;
    mpfr_t temp_var_167;
    mpfr_t temp_var_168;
    mpfr_t temp_var_169;
    mpfr_t temp_var_170;
    mpfr_t temp_var_171;
    mpfr_t temp_var_172;
    mpfr_t temp_var_173;
    mpfr_t temp_var_174;
    mpfr_t temp_var_175;
    mpfr_t temp_var_176;
    mpfr_t temp_var_177;
    mpfr_t temp_var_178;
    mpfr_t temp_var_179;
    mpfr_t temp_var_180;
    mpfr_t temp_var_181;
    mpfr_t temp_var_182;
    mpfr_t temp_var_183;
    mpfr_t temp_var_184;
    mpfr_t temp_var_185;
    mpfr_t temp_var_186;
    mpfr_t temp_var_187;
    mpfr_t temp_var_188;
    mpfr_t temp_var_189;
    mpfr_t temp_var_190;
    mpfr_t temp_var_191;
    mpfr_t temp_var_192;
    mpfr_t temp_var_193;
    mpfr_t temp_var_194;
    mpfr_t temp_var_195;
    mpfr_t temp_var_196;
    mpfr_t temp_var_197;
    mpfr_t temp_var_198;
    mpfr_t temp_var_199;
    mpfr_t temp_var_200;
    mpfr_t temp_var_201;
    mpfr_t temp_var_202;
    mpfr_t temp_var_203;
    mpfr_t temp_var_204;
    mpfr_t temp_var_205;
    mpfr_t temp_var_206;
    mpfr_t temp_var_207;
    mpfr_t temp_var_208;
    mpfr_t temp_var_209;
    mpfr_t temp_var_210;
    mpfr_t temp_var_211;
    mpfr_t temp_var_212;
    mpfr_t temp_var_213;
    mpfr_t temp_var_214;
    mpfr_t temp_var_215;
    mpfr_t temp_var_216;
    mpfr_t temp_var_217;
    mpfr_t temp_var_218;
    mpfr_t temp_var_219;
    mpfr_t temp_var_220;
    mpfr_t temp_var_221;
    mpfr_t temp_var_222;
    mpfr_t temp_var_223;
    mpfr_t temp_var_224;
    mpfr_t temp_var_225;
    mpfr_t temp_var_226;
    mpfr_t temp_var_227;
    mpfr_t temp_var_228;
    mpfr_t temp_var_229;
    mpfr_t temp_var_230;
    mpfr_t temp_var_231;
    mpfr_t temp_var_232;
    mpfr_t temp_var_233;
    mpfr_t temp_var_234;
    mpfr_t temp_var_235;
    mpfr_t temp_var_236;
    mpfr_t temp_var_237;
    mpfr_t temp_var_238;
    mpfr_t temp_var_239;
    mpfr_t temp_var_240;
    mpfr_t temp_var_241;
    mpfr_t temp_var_242;
    mpfr_t temp_var_243;
    mpfr_t temp_var_244;
    mpfr_t temp_var_245;
    mpfr_t temp_var_246;
    mpfr_t temp_var_247;
    mpfr_t temp_var_248;
    mpfr_t temp_var_249;
    mpfr_t temp_var_250;
    mpfr_t temp_var_251;
    mpfr_t temp_var_252;
    mpfr_t temp_var_253;
    mpfr_t temp_var_254;
    mpfr_t temp_var_255;
    mpfr_t temp_var_256;
    mpfr_t temp_var_257;
    mpfr_t temp_var_258;
    mpfr_t temp_var_259;
    mpfr_t temp_var_260;
    mpfr_t temp_var_261;
    mpfr_t temp_var_262;
    mpfr_t temp_var_263;
    mpfr_t temp_var_264;
    mpfr_t temp_var_265;
    mpfr_t temp_var_266;
    mpfr_t temp_var_267;
    mpfr_t temp_var_268;
    mpfr_t temp_var_269;
    mpfr_t temp_var_270;
    mpfr_t temp_var_271;
    mpfr_t temp_var_272;
    mpfr_t temp_var_273;
    mpfr_t temp_var_274;
    mpfr_t temp_var_275;
    mpfr_t temp_var_276;
    mpfr_t temp_var_277;
    mpfr_t temp_var_278;
    mpfr_t temp_var_279;
    mpfr_t temp_var_280;
    mpfr_t temp_var_281;
    mpfr_t temp_var_282;
    mpfr_t temp_var_283;
    mpfr_t temp_var_284;
    mpfr_t temp_var_285;
    mpfr_t temp_var_286;
    mpfr_t temp_var_287;
    mpfr_t temp_var_288;
    mpfr_t temp_var_289;
    mpfr_t temp_var_290;
    mpfr_t temp_var_291;
    mpfr_t temp_var_292;
    mpfr_t temp_var_293;
    mpfr_t temp_var_294;
    mpfr_t temp_var_295;
    mpfr_t temp_var_296;
    mpfr_t temp_var_297;
    mpfr_t temp_var_298;
    mpfr_t temp_var_299;
    mpfr_t temp_var_300;
    mpfr_t temp_var_301;
    mpfr_t temp_var_302;
    mpfr_t temp_var_303;
    mpfr_t temp_var_304;
    mpfr_t temp_var_305;
    mpfr_t temp_var_306;
    mpfr_t temp_var_307;
    mpfr_t temp_var_308;
    mpfr_t temp_var_309;
    mpfr_t temp_var_310;
    mpfr_t temp_var_311;
    mpfr_t temp_var_312;
    mpfr_t temp_var_313;
    mpfr_t temp_var_314;
    mpfr_t temp_var_315;
    mpfr_t temp_var_316;
    mpfr_t temp_var_317;
    mpfr_t temp_var_318;
    mpfr_t temp_var_319;
    mpfr_t temp_var_320;
    mpfr_t temp_var_321;
    mpfr_t temp_var_322;
    mpfr_t temp_var_323;
    mpfr_t temp_var_324;
    mpfr_t temp_var_325;
    mpfr_t temp_var_326;
    mpfr_t temp_var_327;
    mpfr_t temp_var_328;
    mpfr_t temp_var_329;
    mpfr_t temp_var_330;
    mpfr_t temp_var_331;
    mpfr_t temp_var_332;
    mpfr_t temp_var_333;
    mpfr_t temp_var_334;
    mpfr_t temp_var_335;
    mpfr_t temp_var_336;
    mpfr_t temp_var_337;
    mpfr_t temp_var_338;
    mpfr_t temp_var_339;
    mpfr_t temp_var_340;
    mpfr_t temp_var_341;
    mpfr_t temp_var_342;
    mpfr_t temp_var_343;
    mpfr_t temp_var_344;
    mpfr_t temp_var_345;
    mpfr_t temp_var_346;
    mpfr_t temp_var_347;
    mpfr_t temp_var_348;
    mpfr_t temp_var_349;
    mpfr_t temp_var_350;
    mpfr_t temp_var_351;
    mpfr_t temp_var_352;
    mpfr_t temp_var_353;
    mpfr_t temp_var_354;
    mpfr_t temp_var_355;
    mpfr_t temp_var_356;
    mpfr_t temp_var_357;
    mpfr_t temp_var_358;
    mpfr_t temp_var_359;
    mpfr_t temp_var_360;
    mpfr_t temp_var_361;
    mpfr_t temp_var_362;
    mpfr_t temp_var_363;
    mpfr_t temp_var_364;
    mpfr_t temp_var_365;
    mpfr_t temp_var_366;
    mpfr_t temp_var_367;
    mpfr_t temp_var_368;
    mpfr_t temp_var_369;
    mpfr_t temp_var_370;
    mpfr_t temp_var_371;
    mpfr_t temp_var_372;
    mpfr_t temp_var_373;
    mpfr_t temp_var_374;
    mpfr_t temp_var_375;
    mpfr_t temp_var_376;
    mpfr_t temp_var_377;
    mpfr_t temp_var_378;
    mpfr_t temp_var_379;
    mpfr_t temp_var_380;
    mpfr_t temp_var_381;
    mpfr_t temp_var_382;
    mpfr_t temp_var_383;
    mpfr_t temp_var_384;
    mpfr_t temp_var_385;
    mpfr_t temp_var_386;
    mpfr_t temp_var_387;
    mpfr_t temp_var_388;
    mpfr_t temp_var_389;
    mpfr_t temp_var_390;
    mpfr_t temp_var_391;
    mpfr_t temp_var_392;
    mpfr_t temp_var_393;
    mpfr_t temp_var_394;
    mpfr_t temp_var_395;
    mpfr_t temp_var_396;
    mpfr_t temp_var_397;
    mpfr_t temp_var_398;
    mpfr_t temp_var_399;
    mpfr_t temp_var_400;
    mpfr_t temp_var_401;
    mpfr_t temp_var_402;
    mpfr_t temp_var_403;
    mpfr_t temp_var_404;
    mpfr_t temp_var_405;
    mpfr_t temp_var_406;
    mpfr_t temp_var_407;
    mpfr_t temp_var_408;
    mpfr_t temp_var_409;
    mpfr_t temp_var_410;
    mpfr_t temp_var_411;
    mpfr_t temp_var_412;
    mpfr_t temp_var_413;
    mpfr_t temp_var_414;
    mpfr_t temp_var_415;
    mpfr_t temp_var_416;
    mpfr_t temp_var_417;
    mpfr_t temp_var_418;
    mpfr_t temp_var_419;
    mpfr_t temp_var_420;
    mpfr_t temp_var_421;
    mpfr_t temp_var_422;
    mpfr_t temp_var_423;
    mpfr_t temp_var_424;
    mpfr_t temp_var_425;
    mpfr_t temp_var_426;
    mpfr_t temp_var_427;
    mpfr_t temp_var_428;
    mpfr_t temp_var_429;
    mpfr_t temp_var_430;
    mpfr_t temp_var_431;
    mpfr_t temp_var_432;
    mpfr_t temp_var_433;
    mpfr_t temp_var_434;
    mpfr_t temp_var_435;
    mpfr_t temp_var_436;
    mpfr_t temp_var_437;
    mpfr_t temp_var_438;
    mpfr_t temp_var_439;
    mpfr_t temp_var_440;
    mpfr_t temp_var_441;
    mpfr_t temp_var_442;
    mpfr_t temp_var_443;
    mpfr_t temp_var_444;
    mpfr_t temp_var_445;
    mpfr_t temp_var_446;
    mpfr_t temp_var_447;
    mpfr_t temp_var_448;
    mpfr_t temp_var_449;
    mpfr_t temp_var_450;
    mpfr_t temp_var_451;
    mpfr_t temp_var_452;
    mpfr_t temp_var_453;
    mpfr_t temp_var_454;
    mpfr_t temp_var_455;
    mpfr_t temp_var_456;
    mpfr_t temp_var_457;
    mpfr_t temp_var_458;
    mpfr_t temp_var_459;
    mpfr_t temp_var_460;
    mpfr_t temp_var_461;
    mpfr_t temp_var_462;
    mpfr_t temp_var_463;
    mpfr_t temp_var_464;
    mpfr_t temp_var_465;
    mpfr_t temp_var_466;
    mpfr_t temp_var_467;
    mpfr_t temp_var_468;
    mpfr_t temp_var_469;
    mpfr_t temp_var_470;
    mpfr_t temp_var_471;
    mpfr_t temp_var_472;
    mpfr_t temp_var_473;
    mpfr_t temp_var_474;
    mpfr_t temp_var_475;
    mpfr_t temp_var_476;
    mpfr_t temp_var_477;
    mpfr_t temp_var_478;
    mpfr_t temp_var_479;
    mpfr_t temp_var_480;
    mpfr_t temp_var_481;
    mpfr_t temp_var_482;
    mpfr_t temp_var_483;
    mpfr_t temp_var_484;
    mpfr_t temp_var_485;
    mpfr_t temp_var_486;
    mpfr_t temp_var_487;
    mpfr_t temp_var_488;
    mpfr_t temp_var_489;
    mpfr_t temp_var_490;
    mpfr_t temp_var_491;
    mpfr_t temp_var_492;
    mpfr_t temp_var_493;
    mpfr_t temp_var_494;
    mpfr_t temp_var_495;
    mpfr_t temp_var_496;
    mpfr_t temp_var_497;
    mpfr_t temp_var_498;
    mpfr_t temp_var_499;
    mpfr_t temp_var_500;
    mpfr_t temp_var_501;
    mpfr_t temp_var_502;
    mpfr_t temp_var_503;
    mpfr_t temp_var_504;
    mpfr_t temp_var_505;
    mpfr_t temp_var_506;
    mpfr_t temp_var_507;
    mpfr_t temp_var_508;
    mpfr_t temp_var_509;
    mpfr_t temp_var_510;
    mpfr_t temp_var_511;
    mpfr_t temp_var_512;
    mpfr_t temp_var_513;
    mpfr_t temp_var_514;
    mpfr_t temp_var_515;
    mpfr_t temp_var_516;
    mpfr_t temp_var_517;
    mpfr_t temp_var_518;
    mpfr_t temp_var_519;
    mpfr_t temp_var_520;
    mpfr_t temp_var_521;
    mpfr_t temp_var_522;
    mpfr_t temp_var_523;
    mpfr_t temp_var_524;
    mpfr_t temp_var_525;
    mpfr_t temp_var_526;
    mpfr_t temp_var_527;
    mpfr_t temp_var_528;
    mpfr_t temp_var_529;
    mpfr_t temp_var_530;
    mpfr_t temp_var_531;
    mpfr_t temp_var_532;
    mpfr_t temp_var_533;
    mpfr_t temp_var_534;
    mpfr_t temp_var_535;
    mpfr_t temp_var_536;
    mpfr_t temp_var_537;
    mpfr_t temp_var_538;
    mpfr_t temp_var_539;
    mpfr_t temp_var_540;
    mpfr_t temp_var_541;
    mpfr_t temp_var_542;
    mpfr_t temp_var_543;
    mpfr_t temp_var_544;
    mpfr_t temp_var_545;
    mpfr_t temp_var_546;
    mpfr_t temp_var_547;
    mpfr_t temp_var_548;
    mpfr_t temp_var_549;
    mpfr_t temp_var_550;
    mpfr_t temp_var_551;
    mpfr_t temp_var_552;
    mpfr_t temp_var_553;
    mpfr_t temp_var_554;
    mpfr_t temp_var_555;
    mpfr_t temp_var_556;
    mpfr_t temp_var_557;
    mpfr_t temp_var_558;
    mpfr_t temp_var_559;
    mpfr_t temp_var_560;
    mpfr_t temp_var_561;
    mpfr_t temp_var_562;
    mpfr_t temp_var_563;
    mpfr_t temp_var_564;
    mpfr_t temp_var_565;
    mpfr_t temp_var_566;
    mpfr_t temp_var_567;
    mpfr_t temp_var_568;
    mpfr_t temp_var_569;
    mpfr_t temp_var_570;
    mpfr_t temp_var_571;
    mpfr_t temp_var_572;
    mpfr_t temp_var_573;
    mpfr_t temp_var_574;
    mpfr_t temp_var_575;
    mpfr_t temp_var_576;
    mpfr_t temp_var_577;
    mpfr_t temp_var_578;
    mpfr_t temp_var_579;
    mpfr_t temp_var_580;
    mpfr_t temp_var_581;
    mpfr_t temp_var_582;
    mpfr_t temp_var_583;
    mpfr_t temp_var_584;
    mpfr_t temp_var_585;
    mpfr_t temp_var_586;
    mpfr_t temp_var_587;
    mpfr_t temp_var_588;
    mpfr_t temp_var_589;
    mpfr_t temp_var_590;
    mpfr_t temp_var_591;
    mpfr_t temp_var_592;
    mpfr_t temp_var_593;
    mpfr_t temp_var_594;
    mpfr_t temp_var_595;
    mpfr_t temp_var_596;
    mpfr_t temp_var_597;
    mpfr_t temp_var_598;
    mpfr_t temp_var_599;
    mpfr_t temp_var_600;
    mpfr_t temp_var_601;
    mpfr_t temp_var_602;
    mpfr_t temp_var_603;
    mpfr_t temp_var_604;
    mpfr_t temp_var_605;
    mpfr_t temp_var_606;
    mpfr_t temp_var_607;
    mpfr_t temp_var_608;
    mpfr_t temp_var_609;
    mpfr_t temp_var_610;
    mpfr_t temp_var_611;
    mpfr_t temp_var_612;
    mpfr_t temp_var_613;
    mpfr_t temp_var_614;
    mpfr_t temp_var_615;
    mpfr_t temp_var_616;
    mpfr_t temp_var_617;
    mpfr_t temp_var_618;
    mpfr_t temp_var_619;
    mpfr_t temp_var_620;
    mpfr_t temp_var_621;
    mpfr_t temp_var_622;
    mpfr_t temp_var_623;
    mpfr_t temp_var_624;
    mpfr_t temp_var_625;
    mpfr_t temp_var_626;
    mpfr_t temp_var_627;
    mpfr_t temp_var_628;
    mpfr_t temp_var_629;
    mpfr_t temp_var_630;
    mpfr_t temp_var_631;
    mpfr_t temp_var_632;
    mpfr_t temp_var_633;
    mpfr_t temp_var_634;
    mpfr_t temp_var_635;
    mpfr_t temp_var_636;
    mpfr_t temp_var_637;
    mpfr_t temp_var_638;
    mpfr_t temp_var_639;
    mpfr_t temp_var_640;
    mpfr_t temp_var_641;
    mpfr_t temp_var_642;
    mpfr_t temp_var_643;
    mpfr_t temp_var_644;
    mpfr_t temp_var_645;
    mpfr_t temp_var_646;
    mpfr_t temp_var_647;
    mpfr_t temp_var_648;
    mpfr_t temp_var_649;
    mpfr_t temp_var_650;
    mpfr_t temp_var_651;
    mpfr_t temp_var_652;
    mpfr_t temp_var_653;
    mpfr_t temp_var_654;
    mpfr_t temp_var_655;
    mpfr_t temp_var_656;
    mpfr_t temp_var_657;
    mpfr_t temp_var_658;
    mpfr_t temp_var_659;
    mpfr_t temp_var_660;
    mpfr_t temp_var_661;
    mpfr_t temp_var_662;
    mpfr_t temp_var_663;
    mpfr_t temp_var_664;
    mpfr_t temp_var_665;
    mpfr_t temp_var_666;
    mpfr_t temp_var_667;
    mpfr_t temp_var_668;
    mpfr_t temp_var_669;
    mpfr_t temp_var_670;
    mpfr_t temp_var_671;
    mpfr_t temp_var_672;
    mpfr_t temp_var_673;
    mpfr_t temp_var_674;
    mpfr_t temp_var_675;
    mpfr_t temp_var_676;
    mpfr_t temp_var_677;
    mpfr_t temp_var_678;
    mpfr_t temp_var_679;
    mpfr_t temp_var_680;
    mpfr_t temp_var_681;
    mpfr_t temp_var_682;
    mpfr_t temp_var_683;
    mpfr_t temp_var_684;
    mpfr_t temp_var_685;
    mpfr_t temp_var_686;
    mpfr_t temp_var_687;
    mpfr_t temp_var_688;
    mpfr_t temp_var_689;
    mpfr_t temp_var_690;
    mpfr_t temp_var_691;
    mpfr_t temp_var_692;
    mpfr_t temp_var_693;
    mpfr_t temp_var_694;
    mpfr_t temp_var_695;
    mpfr_t temp_var_696;
    mpfr_t temp_var_697;
    mpfr_t temp_var_698;
    mpfr_t temp_var_699;
    mpfr_t temp_var_700;
    mpfr_t temp_var_701;
    mpfr_t temp_var_702;
    mpfr_t temp_var_703;
    mpfr_t temp_var_704;
    mpfr_t temp_var_705;
    mpfr_t temp_var_706;
    mpfr_t temp_var_707;
    mpfr_t temp_var_708;
    mpfr_t temp_var_709;
    mpfr_t temp_var_710;
    mpfr_t temp_var_711;
    mpfr_t temp_var_712;
    mpfr_t temp_var_713;
    mpfr_t temp_var_714;
    mpfr_t temp_var_715;
    mpfr_t temp_var_716;
    mpfr_t temp_var_717;
    mpfr_t temp_var_718;
    mpfr_t temp_var_719;
    mpfr_t temp_var_720;
    mpfr_t temp_var_721;
        mpfr_t temp_var_722;
    mpfr_t temp_var_723;
    mpfr_t temp_var_724;
    mpfr_t temp_var_725;
int init_mpfr() { 
  mpfr_init2(initvalu_1_ecc, config_vals[0]);
  mpfr_init2(initvalu_2_ecc, config_vals[1]);
  mpfr_init2(initvalu_3_ecc, config_vals[2]);
  mpfr_init2(initvalu_4_ecc, config_vals[3]);
  mpfr_init2(initvalu_5_ecc, config_vals[4]);
  mpfr_init2(initvalu_6_ecc, config_vals[5]);
  mpfr_init2(initvalu_7_ecc, config_vals[6]);
  mpfr_init2(initvalu_8_ecc, config_vals[7]);
  mpfr_init2(initvalu_9_ecc, config_vals[8]);
  mpfr_init2(initvalu_10_ecc, config_vals[9]);
  mpfr_init2(initvalu_11_ecc, config_vals[10]);
  mpfr_init2(initvalu_12_ecc, config_vals[11]);
  mpfr_init2(initvalu_13_ecc, config_vals[12]);
  mpfr_init2(initvalu_14_ecc, config_vals[13]);
  mpfr_init2(initvalu_15_ecc, config_vals[14]);
  mpfr_init2(initvalu_16_ecc, config_vals[15]);
  mpfr_init2(initvalu_17_ecc, config_vals[16]);
  mpfr_init2(initvalu_18_ecc, config_vals[17]);
  mpfr_init2(initvalu_19_ecc, config_vals[18]);
  mpfr_init2(initvalu_20_ecc, config_vals[19]);
  mpfr_init2(initvalu_21_ecc, config_vals[20]);
  mpfr_init2(initvalu_22_ecc, config_vals[21]);
  mpfr_init2(initvalu_23_ecc, config_vals[22]);
  mpfr_init2(initvalu_24_ecc, config_vals[23]);
  mpfr_init2(initvalu_25_ecc, config_vals[24]);
  mpfr_init2(initvalu_26_ecc, config_vals[25]);
  mpfr_init2(initvalu_27_ecc, config_vals[26]);
  mpfr_init2(initvalu_28_ecc, config_vals[27]);
  mpfr_init2(initvalu_29_ecc, config_vals[28]);
  mpfr_init2(initvalu_30_ecc, config_vals[29]);
  mpfr_init2(initvalu_31_ecc, config_vals[30]);
  mpfr_init2(initvalu_32_ecc, config_vals[31]);
  mpfr_init2(initvalu_33_ecc, config_vals[32]);
  mpfr_init2(initvalu_34_ecc, config_vals[33]);
  mpfr_init2(initvalu_35_ecc, config_vals[34]);
  mpfr_init2(initvalu_36_ecc, config_vals[35]);
  mpfr_init2(initvalu_37_ecc, config_vals[36]);
  mpfr_init2(initvalu_38_ecc, config_vals[37]);
  mpfr_init2(initvalu_39_ecc, config_vals[38]);
  mpfr_init2(initvalu_40_ecc, config_vals[39]);
  mpfr_init2(initvalu_41_ecc, config_vals[40]);
  mpfr_init2(initvalu_42_ecc, config_vals[41]);
  mpfr_init2(initvalu_43_ecc, config_vals[42]);
  mpfr_init2(initvalu_44_ecc, config_vals[43]);
  mpfr_init2(initvalu_45_ecc, config_vals[44]);
  mpfr_init2(initvalu_46_ecc, config_vals[45]);
  mpfr_init2(parameter_1_ecc, config_vals[46]);
  mpfr_init2(pi_ecc, config_vals[47]);
  mpfr_init2(R_ecc, config_vals[48]);
  mpfr_init2(Frdy_ecc, config_vals[49]);
  mpfr_init2(Temp_ecc, config_vals[50]);
  mpfr_init2(FoRT_ecc, config_vals[51]);
  mpfr_init2(Cmem_ecc, config_vals[52]);
  mpfr_init2(Qpow_ecc, config_vals[53]);
  mpfr_init2(cellLength_ecc, config_vals[54]);
  mpfr_init2(cellRadius_ecc, config_vals[55]);
  mpfr_init2(junctionLength_ecc, config_vals[56]);
  mpfr_init2(junctionRadius_ecc, config_vals[57]);
  mpfr_init2(distSLcyto_ecc, config_vals[58]);
  mpfr_init2(distJuncSL_ecc, config_vals[59]);
  mpfr_init2(DcaJuncSL_ecc, config_vals[60]);
  mpfr_init2(DcaSLcyto_ecc, config_vals[61]);
  mpfr_init2(DnaJuncSL_ecc, config_vals[62]);
  mpfr_init2(DnaSLcyto_ecc, config_vals[63]);
  mpfr_init2(Vcell_ecc, config_vals[64]);
  mpfr_init2(Vmyo_ecc, config_vals[65]);
  mpfr_init2(Vsr_ecc, config_vals[66]);
  mpfr_init2(Vsl_ecc, config_vals[67]);
  mpfr_init2(Vjunc_ecc, config_vals[68]);
  mpfr_init2(SAjunc_ecc, config_vals[69]);
  mpfr_init2(SAsl_ecc, config_vals[70]);
  mpfr_init2(J_ca_juncsl_ecc, config_vals[71]);
  mpfr_init2(J_ca_slmyo_ecc, config_vals[72]);
  mpfr_init2(J_na_juncsl_ecc, config_vals[73]);
  mpfr_init2(J_na_slmyo_ecc, config_vals[74]);
  mpfr_init2(Fjunc_ecc, config_vals[75]);
  mpfr_init2(Fsl_ecc, config_vals[76]);
  mpfr_init2(Fjunc_CaL_ecc, config_vals[77]);
  mpfr_init2(Fsl_CaL_ecc, config_vals[78]);
  mpfr_init2(Cli_ecc, config_vals[79]);
  mpfr_init2(Clo_ecc, config_vals[80]);
  mpfr_init2(Ko_ecc, config_vals[81]);
  mpfr_init2(Nao_ecc, config_vals[82]);
  mpfr_init2(Cao_ecc, config_vals[83]);
  mpfr_init2(Mgi_ecc, config_vals[84]);
  mpfr_init2(ena_junc_ecc, config_vals[85]);
  mpfr_init2(ena_sl_ecc, config_vals[86]);
  mpfr_init2(ek_ecc, config_vals[87]);
  mpfr_init2(eca_junc_ecc, config_vals[88]);
  mpfr_init2(eca_sl_ecc, config_vals[89]);
  mpfr_init2(ecl_ecc, config_vals[90]);
  mpfr_init2(GNa_ecc, config_vals[91]);
  mpfr_init2(GNaB_ecc, config_vals[92]);
  mpfr_init2(IbarNaK_ecc, config_vals[93]);
  mpfr_init2(KmNaip_ecc, config_vals[94]);
  mpfr_init2(KmKo_ecc, config_vals[95]);
  mpfr_init2(Q10NaK_ecc, config_vals[96]);
  mpfr_init2(Q10KmNai_ecc, config_vals[97]);
  mpfr_init2(pNaK_ecc, config_vals[98]);
  mpfr_init2(GtoSlow_ecc, config_vals[99]);
  mpfr_init2(GtoFast_ecc, config_vals[100]);
  mpfr_init2(gkp_ecc, config_vals[101]);
  mpfr_init2(GClCa_ecc, config_vals[102]);
  mpfr_init2(GClB_ecc, config_vals[103]);
  mpfr_init2(KdClCa_ecc, config_vals[104]);
  mpfr_init2(pNa_ecc, config_vals[105]);
  mpfr_init2(pCa_ecc, config_vals[106]);
  mpfr_init2(pK_ecc, config_vals[107]);
  mpfr_init2(KmCa_ecc, config_vals[108]);
  mpfr_init2(Q10CaL_ecc, config_vals[109]);
  mpfr_init2(IbarNCX_ecc, config_vals[110]);
  mpfr_init2(KmCai_ecc, config_vals[111]);
  mpfr_init2(KmCao_ecc, config_vals[112]);
  mpfr_init2(KmNai_ecc, config_vals[113]);
  mpfr_init2(KmNao_ecc, config_vals[114]);
  mpfr_init2(ksat_ecc, config_vals[115]);
  mpfr_init2(nu_ecc, config_vals[116]);
  mpfr_init2(Kdact_ecc, config_vals[117]);
  mpfr_init2(Q10NCX_ecc, config_vals[118]);
  mpfr_init2(IbarSLCaP_ecc, config_vals[119]);
  mpfr_init2(KmPCa_ecc, config_vals[120]);
  mpfr_init2(GCaB_ecc, config_vals[121]);
  mpfr_init2(Q10SLCaP_ecc, config_vals[122]);
  mpfr_init2(Q10SRCaP_ecc, config_vals[123]);
  mpfr_init2(Vmax_SRCaP_ecc, config_vals[124]);
  mpfr_init2(Kmf_ecc, config_vals[125]);
  mpfr_init2(Kmr_ecc, config_vals[126]);
  mpfr_init2(hillSRCaP_ecc, config_vals[127]);
  mpfr_init2(ks_ecc, config_vals[128]);
  mpfr_init2(koCa_ecc, config_vals[129]);
  mpfr_init2(kom_ecc, config_vals[130]);
  mpfr_init2(kiCa_ecc, config_vals[131]);
  mpfr_init2(kim_ecc, config_vals[132]);
  mpfr_init2(ec50SR_ecc, config_vals[133]);
  mpfr_init2(Bmax_Naj_ecc, config_vals[134]);
  mpfr_init2(Bmax_Nasl_ecc, config_vals[135]);
  mpfr_init2(koff_na_ecc, config_vals[136]);
  mpfr_init2(kon_na_ecc, config_vals[137]);
  mpfr_init2(Bmax_TnClow_ecc, config_vals[138]);
  mpfr_init2(koff_tncl_ecc, config_vals[139]);
  mpfr_init2(kon_tncl_ecc, config_vals[140]);
  mpfr_init2(Bmax_TnChigh_ecc, config_vals[141]);
  mpfr_init2(koff_tnchca_ecc, config_vals[142]);
  mpfr_init2(kon_tnchca_ecc, config_vals[143]);
  mpfr_init2(koff_tnchmg_ecc, config_vals[144]);
  mpfr_init2(kon_tnchmg_ecc, config_vals[145]);
  mpfr_init2(Bmax_CaM_ecc, config_vals[146]);
  mpfr_init2(koff_cam_ecc, config_vals[147]);
  mpfr_init2(kon_cam_ecc, config_vals[148]);
  mpfr_init2(Bmax_myosin_ecc, config_vals[149]);
  mpfr_init2(koff_myoca_ecc, config_vals[150]);
  mpfr_init2(kon_myoca_ecc, config_vals[151]);
  mpfr_init2(koff_myomg_ecc, config_vals[152]);
  mpfr_init2(kon_myomg_ecc, config_vals[153]);
  mpfr_init2(Bmax_SR_ecc, config_vals[154]);
  mpfr_init2(koff_sr_ecc, config_vals[155]);
  mpfr_init2(kon_sr_ecc, config_vals[156]);
  mpfr_init2(Bmax_SLlowsl_ecc, config_vals[157]);
  mpfr_init2(Bmax_SLlowj_ecc, config_vals[158]);
  mpfr_init2(koff_sll_ecc, config_vals[159]);
  mpfr_init2(kon_sll_ecc, config_vals[160]);
  mpfr_init2(Bmax_SLhighsl_ecc, config_vals[161]);
  mpfr_init2(Bmax_SLhighj_ecc, config_vals[162]);
  mpfr_init2(koff_slh_ecc, config_vals[163]);
  mpfr_init2(kon_slh_ecc, config_vals[164]);
  mpfr_init2(Bmax_Csqn_ecc, config_vals[165]);
  mpfr_init2(koff_csqn_ecc, config_vals[166]);
  mpfr_init2(kon_csqn_ecc, config_vals[167]);
  mpfr_init2(am_ecc, config_vals[168]);
  mpfr_init2(bm_ecc, config_vals[169]);
  mpfr_init2(ah_ecc, config_vals[170]);
  mpfr_init2(bh_ecc, config_vals[171]);
  mpfr_init2(aj_ecc, config_vals[172]);
  mpfr_init2(bj_ecc, config_vals[173]);
  mpfr_init2(I_Na_junc_ecc, config_vals[174]);
  mpfr_init2(I_Na_sl_ecc, config_vals[175]);
  mpfr_init2(I_Na_ecc, config_vals[176]);
  mpfr_init2(I_nabk_junc_ecc, config_vals[177]);
  mpfr_init2(I_nabk_sl_ecc, config_vals[178]);
  mpfr_init2(I_nabk_ecc, config_vals[179]);
  mpfr_init2(sigma_ecc, config_vals[180]);
  mpfr_init2(fnak_ecc, config_vals[181]);
  mpfr_init2(I_nak_junc_ecc, config_vals[182]);
  mpfr_init2(I_nak_sl_ecc, config_vals[183]);
  mpfr_init2(I_nak_ecc, config_vals[184]);
  mpfr_init2(gkr_ecc, config_vals[185]);
  mpfr_init2(xrss_ecc, config_vals[186]);
  mpfr_init2(tauxr_ecc, config_vals[187]);
  mpfr_init2(rkr_ecc, config_vals[188]);
  mpfr_init2(I_kr_ecc, config_vals[189]);
  mpfr_init2(pcaks_junc_ecc, config_vals[190]);
  mpfr_init2(pcaks_sl_ecc, config_vals[191]);
  mpfr_init2(gks_junc_ecc, config_vals[192]);
  mpfr_init2(gks_sl_ecc, config_vals[193]);
  mpfr_init2(eks_ecc, config_vals[194]);
  mpfr_init2(xsss_ecc, config_vals[195]);
  mpfr_init2(tauxs_ecc, config_vals[196]);
  mpfr_init2(I_ks_junc_ecc, config_vals[197]);
  mpfr_init2(I_ks_sl_ecc, config_vals[198]);
  mpfr_init2(I_ks_ecc, config_vals[199]);
  mpfr_init2(kp_kp_ecc, config_vals[200]);
  mpfr_init2(I_kp_junc_ecc, config_vals[201]);
  mpfr_init2(I_kp_sl_ecc, config_vals[202]);
  mpfr_init2(I_kp_ecc, config_vals[203]);
  mpfr_init2(xtoss_ecc, config_vals[204]);
  mpfr_init2(ytoss_ecc, config_vals[205]);
  mpfr_init2(rtoss_ecc, config_vals[206]);
  mpfr_init2(tauxtos_ecc, config_vals[207]);
  mpfr_init2(tauytos_ecc, config_vals[208]);
  mpfr_init2(taurtos_ecc, config_vals[209]);
  mpfr_init2(I_tos_ecc, config_vals[210]);
  mpfr_init2(tauxtof_ecc, config_vals[211]);
  mpfr_init2(tauytof_ecc, config_vals[212]);
  mpfr_init2(I_tof_ecc, config_vals[213]);
  mpfr_init2(I_to_ecc, config_vals[214]);
  mpfr_init2(aki_ecc, config_vals[215]);
  mpfr_init2(bki_ecc, config_vals[216]);
  mpfr_init2(kiss_ecc, config_vals[217]);
  mpfr_init2(I_ki_ecc, config_vals[218]);
  mpfr_init2(I_ClCa_junc_ecc, config_vals[219]);
  mpfr_init2(I_ClCa_sl_ecc, config_vals[220]);
  mpfr_init2(I_ClCa_ecc, config_vals[221]);
  mpfr_init2(I_Clbk_ecc, config_vals[222]);
  mpfr_init2(dss_ecc, config_vals[223]);
  mpfr_init2(taud_ecc, config_vals[224]);
  mpfr_init2(fss_ecc, config_vals[225]);
  mpfr_init2(tauf_ecc, config_vals[226]);
  mpfr_init2(ibarca_j_ecc, config_vals[227]);
  mpfr_init2(ibarca_sl_ecc, config_vals[228]);
  mpfr_init2(ibark_ecc, config_vals[229]);
  mpfr_init2(ibarna_j_ecc, config_vals[230]);
  mpfr_init2(ibarna_sl_ecc, config_vals[231]);
  mpfr_init2(I_Ca_junc_ecc, config_vals[232]);
  mpfr_init2(I_Ca_sl_ecc, config_vals[233]);
  mpfr_init2(I_Ca_ecc, config_vals[234]);
  mpfr_init2(I_CaK_ecc, config_vals[235]);
  mpfr_init2(I_CaNa_junc_ecc, config_vals[236]);
  mpfr_init2(I_CaNa_sl_ecc, config_vals[237]);
  mpfr_init2(I_CaNa_ecc, config_vals[238]);
  mpfr_init2(I_Catot_ecc, config_vals[239]);
  mpfr_init2(Ka_junc_ecc, config_vals[240]);
  mpfr_init2(Ka_sl_ecc, config_vals[241]);
  mpfr_init2(s1_junc_ecc, config_vals[242]);
  mpfr_init2(s1_sl_ecc, config_vals[243]);
  mpfr_init2(s2_junc_ecc, config_vals[244]);
  mpfr_init2(s3_junc_ecc, config_vals[245]);
  mpfr_init2(s2_sl_ecc, config_vals[246]);
  mpfr_init2(s3_sl_ecc, config_vals[247]);
  mpfr_init2(I_ncx_junc_ecc, config_vals[248]);
  mpfr_init2(I_ncx_sl_ecc, config_vals[249]);
  mpfr_init2(I_ncx_ecc, config_vals[250]);
  mpfr_init2(I_pca_junc_ecc, config_vals[251]);
  mpfr_init2(I_pca_sl_ecc, config_vals[252]);
  mpfr_init2(I_pca_ecc, config_vals[253]);
  mpfr_init2(I_cabk_junc_ecc, config_vals[254]);
  mpfr_init2(I_cabk_sl_ecc, config_vals[255]);
  mpfr_init2(I_cabk_ecc, config_vals[256]);
  mpfr_init2(MaxSR_ecc, config_vals[257]);
  mpfr_init2(MinSR_ecc, config_vals[258]);
  mpfr_init2(kCaSR_ecc, config_vals[259]);
  mpfr_init2(koSRCa_ecc, config_vals[260]);
  mpfr_init2(kiSRCa_ecc, config_vals[261]);
  mpfr_init2(RI_ecc, config_vals[262]);
  mpfr_init2(J_SRCarel_ecc, config_vals[263]);
  mpfr_init2(J_serca_ecc, config_vals[264]);
  mpfr_init2(J_SRleak_ecc, config_vals[265]);
  mpfr_init2(J_CaB_cytosol_ecc, config_vals[266]);
  mpfr_init2(J_CaB_junction_ecc, config_vals[267]);
  mpfr_init2(J_CaB_sl_ecc, config_vals[268]);
  mpfr_init2(oneovervsr_ecc, config_vals[269]);
  mpfr_init2(I_Na_tot_junc_ecc, config_vals[270]);
  mpfr_init2(I_Na_tot_sl_ecc, config_vals[271]);
  mpfr_init2(oneovervsl_ecc, config_vals[272]);
  mpfr_init2(I_K_tot_ecc, config_vals[273]);
  mpfr_init2(I_Ca_tot_junc_ecc, config_vals[274]);
  mpfr_init2(I_Ca_tot_sl_ecc, config_vals[275]);
  mpfr_init2(junc_sl_ecc, config_vals[276]);
  mpfr_init2(sl_junc_ecc, config_vals[277]);
  mpfr_init2(sl_myo_ecc, config_vals[278]);
  mpfr_init2(myo_sl_ecc, config_vals[279]);
  mpfr_init2(I_app_ecc, config_vals[280]);
  mpfr_init2(V_hold_ecc, config_vals[281]);
  mpfr_init2(V_test_ecc, config_vals[282]);
  mpfr_init2(V_clamp_ecc, config_vals[283]);
  mpfr_init2(R_clamp_ecc, config_vals[284]);
  mpfr_init2(I_Na_tot_ecc, config_vals[285]);
  mpfr_init2(I_Cl_tot_ecc, config_vals[286]);
  mpfr_init2(I_Ca_tot_ecc, config_vals[287]);
  mpfr_init2(I_tot_ecc, config_vals[288]);
    mpfr_init2 (temp_var_1, 64);
    mpfr_init2 (temp_var_2, 64);
    mpfr_init2 (temp_var_3, 64);
    mpfr_init2 (temp_var_4, 64);
    mpfr_init2 (temp_var_5, 64);
    mpfr_init2 (temp_var_6, 64);
    mpfr_init2 (temp_var_7, 64);
    mpfr_init2 (temp_var_8, 64);
    mpfr_init2 (temp_var_9, 64);
    mpfr_init2 (temp_var_10, 64);
    mpfr_init2 (temp_var_11, 64);
    mpfr_init2 (temp_var_12, 64);
    mpfr_init2 (temp_var_13, 64);
    mpfr_init2 (temp_var_14, 64);
    mpfr_init2 (temp_var_15, 64);
    mpfr_init2 (temp_var_16, 64);
    mpfr_init2 (temp_var_17, 64);
    mpfr_init2 (temp_var_18, 64);
    mpfr_init2 (temp_var_19, 64);
    mpfr_init2 (temp_var_20, 64);
    mpfr_init2 (temp_var_21, 64);
    mpfr_init2 (temp_var_22, 64);
    mpfr_init2 (temp_var_23, 64);
    mpfr_init2 (temp_var_24, 64);
    mpfr_init2 (temp_var_25, 64);
    mpfr_init2 (temp_var_26, 64);
    mpfr_init2 (temp_var_27, 64);
    mpfr_init2 (temp_var_28, 64);
    mpfr_init2 (temp_var_29, 64);
    mpfr_init2 (temp_var_30, 64);
    mpfr_init2 (temp_var_31, 64);
    mpfr_init2 (temp_var_32, 64);
    mpfr_init2 (temp_var_33, 64);
    mpfr_init2 (temp_var_34, 64);
    mpfr_init2 (temp_var_35, 64);
    mpfr_init2 (temp_var_36, 64);
    mpfr_init2 (temp_var_37, 64);
    mpfr_init2 (temp_var_38, 64);
    mpfr_init2 (temp_var_39, 64);
    mpfr_init2 (temp_var_40, 64);
    mpfr_init2 (temp_var_41, 64);
    mpfr_init2 (temp_var_42, 64);
    mpfr_init2 (temp_var_43, 64);
    mpfr_init2 (temp_var_44, 64);
    mpfr_init2 (temp_var_45, 64);
    mpfr_init2 (temp_var_46, 64);
    mpfr_init2 (temp_var_47, 64);
    mpfr_init2 (temp_var_48, 64);
    mpfr_init2 (temp_var_49, 64);
    mpfr_init2 (temp_var_50, 64);
    mpfr_init2 (temp_var_51, 64);
    mpfr_init2 (temp_var_52, 64);
    mpfr_init2 (temp_var_53, 64);
    mpfr_init2 (temp_var_54, 64);
    mpfr_init2 (temp_var_55, 64);
    mpfr_init2 (temp_var_56, 64);
    mpfr_init2 (temp_var_57, 64);
    mpfr_init2 (temp_var_58, 64);
    mpfr_init2 (temp_var_59, 64);
    mpfr_init2 (temp_var_60, 64);
    mpfr_init2 (temp_var_61, 64);
    mpfr_init2 (temp_var_62, 64);
    mpfr_init2 (temp_var_63, 64);
    mpfr_init2 (temp_var_64, 64);
    mpfr_init2 (temp_var_65, 64);
    mpfr_init2 (temp_var_66, 64);
    mpfr_init2 (temp_var_67, 64);
    mpfr_init2 (temp_var_68, 64);
    mpfr_init2 (temp_var_69, 64);
    mpfr_init2 (temp_var_70, 64);
    mpfr_init2 (temp_var_71, 64);
    mpfr_init2 (temp_var_72, 64);
    mpfr_init2 (temp_var_73, 64);
    mpfr_init2 (temp_var_74, 64);
    mpfr_init2 (temp_var_75, 64);
    mpfr_init2 (temp_var_76, 64);
    mpfr_init2 (temp_var_77, 64);
    mpfr_init2 (temp_var_78, 64);
    mpfr_init2 (temp_var_79, 64);
    mpfr_init2 (temp_var_80, 64);
        mpfr_init2 (temp_var_81, 64);
        mpfr_init2 (temp_var_82, 64);
        mpfr_init2 (temp_var_83, 64);
        mpfr_init2 (temp_var_84, 64);
        mpfr_init2 (temp_var_85, 64);
        mpfr_init2 (temp_var_86, 64);
        mpfr_init2 (temp_var_87, 64);
        mpfr_init2 (temp_var_88, 64);
        mpfr_init2 (temp_var_89, 64);
        mpfr_init2 (temp_var_90, 64);
        mpfr_init2 (temp_var_91, 64);
        mpfr_init2 (temp_var_92, 64);
        mpfr_init2 (temp_var_93, 64);
        mpfr_init2 (temp_var_94, 64);
        mpfr_init2 (temp_var_95, 64);
        mpfr_init2 (temp_var_96, 64);
        mpfr_init2 (temp_var_97, 64);
        mpfr_init2 (temp_var_98, 64);
        mpfr_init2 (temp_var_99, 64);
        mpfr_init2 (temp_var_100, 64);
        mpfr_init2 (temp_var_101, 64);
        mpfr_init2 (temp_var_102, 64);
        mpfr_init2 (temp_var_103, 64);
        mpfr_init2 (temp_var_104, 64);
        mpfr_init2 (temp_var_105, 64);
    mpfr_init2 (temp_var_106, 64);
    mpfr_init2 (temp_var_107, 64);
    mpfr_init2 (temp_var_108, 64);
    mpfr_init2 (temp_var_109, 64);
    mpfr_init2 (temp_var_110, 64);
    mpfr_init2 (temp_var_111, 64);
    mpfr_init2 (temp_var_112, 64);
    mpfr_init2 (temp_var_113, 64);
    mpfr_init2 (temp_var_114, 64);
    mpfr_init2 (temp_var_115, 64);
    mpfr_init2 (temp_var_116, 64);
    mpfr_init2 (temp_var_117, 64);
    mpfr_init2 (temp_var_118, 64);
    mpfr_init2 (temp_var_119, 64);
    mpfr_init2 (temp_var_120, 64);
    mpfr_init2 (temp_var_121, 64);
    mpfr_init2 (temp_var_122, 64);
    mpfr_init2 (temp_var_123, 64);
    mpfr_init2 (temp_var_124, 64);
    mpfr_init2 (temp_var_125, 64);
    mpfr_init2 (temp_var_126, 64);
    mpfr_init2 (temp_var_127, 64);
    mpfr_init2 (temp_var_128, 64);
    mpfr_init2 (temp_var_129, 64);
    mpfr_init2 (temp_var_130, 64);
    mpfr_init2 (temp_var_131, 64);
    mpfr_init2 (temp_var_132, 64);
    mpfr_init2 (temp_var_133, 64);
    mpfr_init2 (temp_var_134, 64);
    mpfr_init2 (temp_var_135, 64);
    mpfr_init2 (temp_var_136, 64);
    mpfr_init2 (temp_var_137, 64);
    mpfr_init2 (temp_var_138, 64);
    mpfr_init2 (temp_var_139, 64);
    mpfr_init2 (temp_var_140, 64);
    mpfr_init2 (temp_var_141, 64);
    mpfr_init2 (temp_var_142, 64);
    mpfr_init2 (temp_var_143, 64);
    mpfr_init2 (temp_var_144, 64);
    mpfr_init2 (temp_var_145, 64);
    mpfr_init2 (temp_var_146, 64);
    mpfr_init2 (temp_var_147, 64);
    mpfr_init2 (temp_var_148, 64);
    mpfr_init2 (temp_var_149, 64);
    mpfr_init2 (temp_var_150, 64);
    mpfr_init2 (temp_var_151, 64);
    mpfr_init2 (temp_var_152, 64);
    mpfr_init2 (temp_var_153, 64);
    mpfr_init2 (temp_var_154, 64);
    mpfr_init2 (temp_var_155, 64);
    mpfr_init2 (temp_var_156, 64);
    mpfr_init2 (temp_var_157, 64);
    mpfr_init2 (temp_var_158, 64);
    mpfr_init2 (temp_var_159, 64);
    mpfr_init2 (temp_var_160, 64);
    mpfr_init2 (temp_var_161, 64);
    mpfr_init2 (temp_var_162, 64);
    mpfr_init2 (temp_var_163, 64);
    mpfr_init2 (temp_var_164, 64);
    mpfr_init2 (temp_var_165, 64);
    mpfr_init2 (temp_var_166, 64);
    mpfr_init2 (temp_var_167, 64);
    mpfr_init2 (temp_var_168, 64);
    mpfr_init2 (temp_var_169, 64);
    mpfr_init2 (temp_var_170, 64);
    mpfr_init2 (temp_var_171, 64);
    mpfr_init2 (temp_var_172, 64);
    mpfr_init2 (temp_var_173, 64);
    mpfr_init2 (temp_var_174, 64);
    mpfr_init2 (temp_var_175, 64);
    mpfr_init2 (temp_var_176, 64);
    mpfr_init2 (temp_var_177, 64);
    mpfr_init2 (temp_var_178, 64);
    mpfr_init2 (temp_var_179, 64);
    mpfr_init2 (temp_var_180, 64);
    mpfr_init2 (temp_var_181, 64);
    mpfr_init2 (temp_var_182, 64);
    mpfr_init2 (temp_var_183, 64);
    mpfr_init2 (temp_var_184, 64);
    mpfr_init2 (temp_var_185, 64);
    mpfr_init2 (temp_var_186, 64);
    mpfr_init2 (temp_var_187, 64);
    mpfr_init2 (temp_var_188, 64);
    mpfr_init2 (temp_var_189, 64);
    mpfr_init2 (temp_var_190, 64);
    mpfr_init2 (temp_var_191, 64);
    mpfr_init2 (temp_var_192, 64);
    mpfr_init2 (temp_var_193, 64);
    mpfr_init2 (temp_var_194, 64);
    mpfr_init2 (temp_var_195, 64);
    mpfr_init2 (temp_var_196, 64);
    mpfr_init2 (temp_var_197, 64);
    mpfr_init2 (temp_var_198, 64);
    mpfr_init2 (temp_var_199, 64);
    mpfr_init2 (temp_var_200, 64);
    mpfr_init2 (temp_var_201, 64);
    mpfr_init2 (temp_var_202, 64);
    mpfr_init2 (temp_var_203, 64);
    mpfr_init2 (temp_var_204, 64);
    mpfr_init2 (temp_var_205, 64);
    mpfr_init2 (temp_var_206, 64);
    mpfr_init2 (temp_var_207, 64);
    mpfr_init2 (temp_var_208, 64);
    mpfr_init2 (temp_var_209, 64);
    mpfr_init2 (temp_var_210, 64);
    mpfr_init2 (temp_var_211, 64);
    mpfr_init2 (temp_var_212, 64);
    mpfr_init2 (temp_var_213, 64);
    mpfr_init2 (temp_var_214, 64);
    mpfr_init2 (temp_var_215, 64);
    mpfr_init2 (temp_var_216, 64);
    mpfr_init2 (temp_var_217, 64);
    mpfr_init2 (temp_var_218, 64);
    mpfr_init2 (temp_var_219, 64);
    mpfr_init2 (temp_var_220, 64);
    mpfr_init2 (temp_var_221, 64);
    mpfr_init2 (temp_var_222, 64);
    mpfr_init2 (temp_var_223, 64);
    mpfr_init2 (temp_var_224, 64);
    mpfr_init2 (temp_var_225, 64);
    mpfr_init2 (temp_var_226, 64);
    mpfr_init2 (temp_var_227, 64);
    mpfr_init2 (temp_var_228, 64);
    mpfr_init2 (temp_var_229, 64);
    mpfr_init2 (temp_var_230, 64);
    mpfr_init2 (temp_var_231, 64);
    mpfr_init2 (temp_var_232, 64);
    mpfr_init2 (temp_var_233, 64);
    mpfr_init2 (temp_var_234, 64);
    mpfr_init2 (temp_var_235, 64);
    mpfr_init2 (temp_var_236, 64);
    mpfr_init2 (temp_var_237, 64);
    mpfr_init2 (temp_var_238, 64);
    mpfr_init2 (temp_var_239, 64);
    mpfr_init2 (temp_var_240, 64);
    mpfr_init2 (temp_var_241, 64);
    mpfr_init2 (temp_var_242, 64);
    mpfr_init2 (temp_var_243, 64);
    mpfr_init2 (temp_var_244, 64);
    mpfr_init2 (temp_var_245, 64);
    mpfr_init2 (temp_var_246, 64);
    mpfr_init2 (temp_var_247, 64);
    mpfr_init2 (temp_var_248, 64);
    mpfr_init2 (temp_var_249, 64);
    mpfr_init2 (temp_var_250, 64);
    mpfr_init2 (temp_var_251, 64);
    mpfr_init2 (temp_var_252, 64);
    mpfr_init2 (temp_var_253, 64);
    mpfr_init2 (temp_var_254, 64);
    mpfr_init2 (temp_var_255, 64);
    mpfr_init2 (temp_var_256, 64);
    mpfr_init2 (temp_var_257, 64);
    mpfr_init2 (temp_var_258, 64);
    mpfr_init2 (temp_var_259, 64);
    mpfr_init2 (temp_var_260, 64);
    mpfr_init2 (temp_var_261, 64);
    mpfr_init2 (temp_var_262, 64);
    mpfr_init2 (temp_var_263, 64);
    mpfr_init2 (temp_var_264, 64);
    mpfr_init2 (temp_var_265, 64);
    mpfr_init2 (temp_var_266, 64);
    mpfr_init2 (temp_var_267, 64);
    mpfr_init2 (temp_var_268, 64);
    mpfr_init2 (temp_var_269, 64);
    mpfr_init2 (temp_var_270, 64);
    mpfr_init2 (temp_var_271, 64);
    mpfr_init2 (temp_var_272, 64);
    mpfr_init2 (temp_var_273, 64);
    mpfr_init2 (temp_var_274, 64);
    mpfr_init2 (temp_var_275, 64);
    mpfr_init2 (temp_var_276, 64);
    mpfr_init2 (temp_var_277, 64);
    mpfr_init2 (temp_var_278, 64);
    mpfr_init2 (temp_var_279, 64);
    mpfr_init2 (temp_var_280, 64);
    mpfr_init2 (temp_var_281, 64);
    mpfr_init2 (temp_var_282, 64);
    mpfr_init2 (temp_var_283, 64);
    mpfr_init2 (temp_var_284, 64);
    mpfr_init2 (temp_var_285, 64);
    mpfr_init2 (temp_var_286, 64);
    mpfr_init2 (temp_var_287, 64);
    mpfr_init2 (temp_var_288, 64);
    mpfr_init2 (temp_var_289, 64);
    mpfr_init2 (temp_var_290, 64);
    mpfr_init2 (temp_var_291, 64);
    mpfr_init2 (temp_var_292, 64);
    mpfr_init2 (temp_var_293, 64);
    mpfr_init2 (temp_var_294, 64);
    mpfr_init2 (temp_var_295, 64);
    mpfr_init2 (temp_var_296, 64);
    mpfr_init2 (temp_var_297, 64);
    mpfr_init2 (temp_var_298, 64);
    mpfr_init2 (temp_var_299, 64);
    mpfr_init2 (temp_var_300, 64);
    mpfr_init2 (temp_var_301, 64);
    mpfr_init2 (temp_var_302, 64);
    mpfr_init2 (temp_var_303, 64);
    mpfr_init2 (temp_var_304, 64);
    mpfr_init2 (temp_var_305, 64);
    mpfr_init2 (temp_var_306, 64);
    mpfr_init2 (temp_var_307, 64);
    mpfr_init2 (temp_var_308, 64);
    mpfr_init2 (temp_var_309, 64);
    mpfr_init2 (temp_var_310, 64);
    mpfr_init2 (temp_var_311, 64);
    mpfr_init2 (temp_var_312, 64);
    mpfr_init2 (temp_var_313, 64);
    mpfr_init2 (temp_var_314, 64);
    mpfr_init2 (temp_var_315, 64);
    mpfr_init2 (temp_var_316, 64);
    mpfr_init2 (temp_var_317, 64);
    mpfr_init2 (temp_var_318, 64);
    mpfr_init2 (temp_var_319, 64);
    mpfr_init2 (temp_var_320, 64);
    mpfr_init2 (temp_var_321, 64);
    mpfr_init2 (temp_var_322, 64);
    mpfr_init2 (temp_var_323, 64);
    mpfr_init2 (temp_var_324, 64);
    mpfr_init2 (temp_var_325, 64);
    mpfr_init2 (temp_var_326, 64);
    mpfr_init2 (temp_var_327, 64);
    mpfr_init2 (temp_var_328, 64);
    mpfr_init2 (temp_var_329, 64);
    mpfr_init2 (temp_var_330, 64);
    mpfr_init2 (temp_var_331, 64);
    mpfr_init2 (temp_var_332, 64);
    mpfr_init2 (temp_var_333, 64);
    mpfr_init2 (temp_var_334, 64);
    mpfr_init2 (temp_var_335, 64);
    mpfr_init2 (temp_var_336, 64);
    mpfr_init2 (temp_var_337, 64);
    mpfr_init2 (temp_var_338, 64);
    mpfr_init2 (temp_var_339, 64);
    mpfr_init2 (temp_var_340, 64);
    mpfr_init2 (temp_var_341, 64);
    mpfr_init2 (temp_var_342, 64);
    mpfr_init2 (temp_var_343, 64);
    mpfr_init2 (temp_var_344, 64);
    mpfr_init2 (temp_var_345, 64);
    mpfr_init2 (temp_var_346, 64);
    mpfr_init2 (temp_var_347, 64);
    mpfr_init2 (temp_var_348, 64);
    mpfr_init2 (temp_var_349, 64);
    mpfr_init2 (temp_var_350, 64);
    mpfr_init2 (temp_var_351, 64);
    mpfr_init2 (temp_var_352, 64);
    mpfr_init2 (temp_var_353, 64);
    mpfr_init2 (temp_var_354, 64);
    mpfr_init2 (temp_var_355, 64);
    mpfr_init2 (temp_var_356, 64);
    mpfr_init2 (temp_var_357, 64);
    mpfr_init2 (temp_var_358, 64);
    mpfr_init2 (temp_var_359, 64);
    mpfr_init2 (temp_var_360, 64);
    mpfr_init2 (temp_var_361, 64);
    mpfr_init2 (temp_var_362, 64);
    mpfr_init2 (temp_var_363, 64);
    mpfr_init2 (temp_var_364, 64);
    mpfr_init2 (temp_var_365, 64);
    mpfr_init2 (temp_var_366, 64);
    mpfr_init2 (temp_var_367, 64);
    mpfr_init2 (temp_var_368, 64);
    mpfr_init2 (temp_var_369, 64);
    mpfr_init2 (temp_var_370, 64);
    mpfr_init2 (temp_var_371, 64);
    mpfr_init2 (temp_var_372, 64);
    mpfr_init2 (temp_var_373, 64);
    mpfr_init2 (temp_var_374, 64);
    mpfr_init2 (temp_var_375, 64);
    mpfr_init2 (temp_var_376, 64);
    mpfr_init2 (temp_var_377, 64);
    mpfr_init2 (temp_var_378, 64);
    mpfr_init2 (temp_var_379, 64);
    mpfr_init2 (temp_var_380, 64);
    mpfr_init2 (temp_var_381, 64);
    mpfr_init2 (temp_var_382, 64);
    mpfr_init2 (temp_var_383, 64);
    mpfr_init2 (temp_var_384, 64);
    mpfr_init2 (temp_var_385, 64);
    mpfr_init2 (temp_var_386, 64);
    mpfr_init2 (temp_var_387, 64);
    mpfr_init2 (temp_var_388, 64);
    mpfr_init2 (temp_var_389, 64);
    mpfr_init2 (temp_var_390, 64);
    mpfr_init2 (temp_var_391, 64);
    mpfr_init2 (temp_var_392, 64);
    mpfr_init2 (temp_var_393, 64);
    mpfr_init2 (temp_var_394, 64);
    mpfr_init2 (temp_var_395, 64);
    mpfr_init2 (temp_var_396, 64);
    mpfr_init2 (temp_var_397, 64);
    mpfr_init2 (temp_var_398, 64);
    mpfr_init2 (temp_var_399, 64);
    mpfr_init2 (temp_var_400, 64);
    mpfr_init2 (temp_var_401, 64);
    mpfr_init2 (temp_var_402, 64);
    mpfr_init2 (temp_var_403, 64);
    mpfr_init2 (temp_var_404, 64);
    mpfr_init2 (temp_var_405, 64);
    mpfr_init2 (temp_var_406, 64);
    mpfr_init2 (temp_var_407, 64);
    mpfr_init2 (temp_var_408, 64);
    mpfr_init2 (temp_var_409, 64);
    mpfr_init2 (temp_var_410, 64);
    mpfr_init2 (temp_var_411, 64);
    mpfr_init2 (temp_var_412, 64);
    mpfr_init2 (temp_var_413, 64);
    mpfr_init2 (temp_var_414, 64);
    mpfr_init2 (temp_var_415, 64);
    mpfr_init2 (temp_var_416, 64);
    mpfr_init2 (temp_var_417, 64);
    mpfr_init2 (temp_var_418, 64);
    mpfr_init2 (temp_var_419, 64);
    mpfr_init2 (temp_var_420, 64);
    mpfr_init2 (temp_var_421, 64);
    mpfr_init2 (temp_var_422, 64);
    mpfr_init2 (temp_var_423, 64);
    mpfr_init2 (temp_var_424, 64);
    mpfr_init2 (temp_var_425, 64);
    mpfr_init2 (temp_var_426, 64);
    mpfr_init2 (temp_var_427, 64);
    mpfr_init2 (temp_var_428, 64);
    mpfr_init2 (temp_var_429, 64);
    mpfr_init2 (temp_var_430, 64);
    mpfr_init2 (temp_var_431, 64);
    mpfr_init2 (temp_var_432, 64);
    mpfr_init2 (temp_var_433, 64);
    mpfr_init2 (temp_var_434, 64);
    mpfr_init2 (temp_var_435, 64);
    mpfr_init2 (temp_var_436, 64);
    mpfr_init2 (temp_var_437, 64);
    mpfr_init2 (temp_var_438, 64);
    mpfr_init2 (temp_var_439, 64);
    mpfr_init2 (temp_var_440, 64);
    mpfr_init2 (temp_var_441, 64);
    mpfr_init2 (temp_var_442, 64);
    mpfr_init2 (temp_var_443, 64);
    mpfr_init2 (temp_var_444, 64);
    mpfr_init2 (temp_var_445, 64);
    mpfr_init2 (temp_var_446, 64);
    mpfr_init2 (temp_var_447, 64);
    mpfr_init2 (temp_var_448, 64);
    mpfr_init2 (temp_var_449, 64);
    mpfr_init2 (temp_var_450, 64);
    mpfr_init2 (temp_var_451, 64);
    mpfr_init2 (temp_var_452, 64);
    mpfr_init2 (temp_var_453, 64);
    mpfr_init2 (temp_var_454, 64);
    mpfr_init2 (temp_var_455, 64);
    mpfr_init2 (temp_var_456, 64);
    mpfr_init2 (temp_var_457, 64);
    mpfr_init2 (temp_var_458, 64);
    mpfr_init2 (temp_var_459, 64);
    mpfr_init2 (temp_var_460, 64);
    mpfr_init2 (temp_var_461, 64);
    mpfr_init2 (temp_var_462, 64);
    mpfr_init2 (temp_var_463, 64);
    mpfr_init2 (temp_var_464, 64);
    mpfr_init2 (temp_var_465, 64);
    mpfr_init2 (temp_var_466, 64);
    mpfr_init2 (temp_var_467, 64);
    mpfr_init2 (temp_var_468, 64);
    mpfr_init2 (temp_var_469, 64);
    mpfr_init2 (temp_var_470, 64);
    mpfr_init2 (temp_var_471, 64);
    mpfr_init2 (temp_var_472, 64);
    mpfr_init2 (temp_var_473, 64);
    mpfr_init2 (temp_var_474, 64);
    mpfr_init2 (temp_var_475, 64);
    mpfr_init2 (temp_var_476, 64);
    mpfr_init2 (temp_var_477, 64);
    mpfr_init2 (temp_var_478, 64);
    mpfr_init2 (temp_var_479, 64);
    mpfr_init2 (temp_var_480, 64);
    mpfr_init2 (temp_var_481, 64);
    mpfr_init2 (temp_var_482, 64);
    mpfr_init2 (temp_var_483, 64);
    mpfr_init2 (temp_var_484, 64);
    mpfr_init2 (temp_var_485, 64);
    mpfr_init2 (temp_var_486, 64);
    mpfr_init2 (temp_var_487, 64);
    mpfr_init2 (temp_var_488, 64);
    mpfr_init2 (temp_var_489, 64);
    mpfr_init2 (temp_var_490, 64);
    mpfr_init2 (temp_var_491, 64);
    mpfr_init2 (temp_var_492, 64);
    mpfr_init2 (temp_var_493, 64);
    mpfr_init2 (temp_var_494, 64);
    mpfr_init2 (temp_var_495, 64);
    mpfr_init2 (temp_var_496, 64);
    mpfr_init2 (temp_var_497, 64);
    mpfr_init2 (temp_var_498, 64);
    mpfr_init2 (temp_var_499, 64);
    mpfr_init2 (temp_var_500, 64);
    mpfr_init2 (temp_var_501, 64);
    mpfr_init2 (temp_var_502, 64);
    mpfr_init2 (temp_var_503, 64);
    mpfr_init2 (temp_var_504, 64);
    mpfr_init2 (temp_var_505, 64);
    mpfr_init2 (temp_var_506, 64);
    mpfr_init2 (temp_var_507, 64);
    mpfr_init2 (temp_var_508, 64);
    mpfr_init2 (temp_var_509, 64);
    mpfr_init2 (temp_var_510, 64);
    mpfr_init2 (temp_var_511, 64);
    mpfr_init2 (temp_var_512, 64);
    mpfr_init2 (temp_var_513, 64);
    mpfr_init2 (temp_var_514, 64);
    mpfr_init2 (temp_var_515, 64);
    mpfr_init2 (temp_var_516, 64);
    mpfr_init2 (temp_var_517, 64);
    mpfr_init2 (temp_var_518, 64);
    mpfr_init2 (temp_var_519, 64);
    mpfr_init2 (temp_var_520, 64);
    mpfr_init2 (temp_var_521, 64);
    mpfr_init2 (temp_var_522, 64);
    mpfr_init2 (temp_var_523, 64);
    mpfr_init2 (temp_var_524, 64);
    mpfr_init2 (temp_var_525, 64);
    mpfr_init2 (temp_var_526, 64);
    mpfr_init2 (temp_var_527, 64);
    mpfr_init2 (temp_var_528, 64);
    mpfr_init2 (temp_var_529, 64);
    mpfr_init2 (temp_var_530, 64);
    mpfr_init2 (temp_var_531, 64);
    mpfr_init2 (temp_var_532, 64);
    mpfr_init2 (temp_var_533, 64);
    mpfr_init2 (temp_var_534, 64);
    mpfr_init2 (temp_var_535, 64);
    mpfr_init2 (temp_var_536, 64);
    mpfr_init2 (temp_var_537, 64);
    mpfr_init2 (temp_var_538, 64);
    mpfr_init2 (temp_var_539, 64);
    mpfr_init2 (temp_var_540, 64);
    mpfr_init2 (temp_var_541, 64);
    mpfr_init2 (temp_var_542, 64);
    mpfr_init2 (temp_var_543, 64);
    mpfr_init2 (temp_var_544, 64);
    mpfr_init2 (temp_var_545, 64);
    mpfr_init2 (temp_var_546, 64);
    mpfr_init2 (temp_var_547, 64);
    mpfr_init2 (temp_var_548, 64);
    mpfr_init2 (temp_var_549, 64);
    mpfr_init2 (temp_var_550, 64);
    mpfr_init2 (temp_var_551, 64);
    mpfr_init2 (temp_var_552, 64);
    mpfr_init2 (temp_var_553, 64);
    mpfr_init2 (temp_var_554, 64);
    mpfr_init2 (temp_var_555, 64);
    mpfr_init2 (temp_var_556, 64);
    mpfr_init2 (temp_var_557, 64);
    mpfr_init2 (temp_var_558, 64);
    mpfr_init2 (temp_var_559, 64);
    mpfr_init2 (temp_var_560, 64);
    mpfr_init2 (temp_var_561, 64);
    mpfr_init2 (temp_var_562, 64);
    mpfr_init2 (temp_var_563, 64);
    mpfr_init2 (temp_var_564, 64);
    mpfr_init2 (temp_var_565, 64);
    mpfr_init2 (temp_var_566, 64);
    mpfr_init2 (temp_var_567, 64);
    mpfr_init2 (temp_var_568, 64);
    mpfr_init2 (temp_var_569, 64);
    mpfr_init2 (temp_var_570, 64);
    mpfr_init2 (temp_var_571, 64);
    mpfr_init2 (temp_var_572, 64);
    mpfr_init2 (temp_var_573, 64);
    mpfr_init2 (temp_var_574, 64);
    mpfr_init2 (temp_var_575, 64);
    mpfr_init2 (temp_var_576, 64);
    mpfr_init2 (temp_var_577, 64);
    mpfr_init2 (temp_var_578, 64);
    mpfr_init2 (temp_var_579, 64);
    mpfr_init2 (temp_var_580, 64);
    mpfr_init2 (temp_var_581, 64);
    mpfr_init2 (temp_var_582, 64);
    mpfr_init2 (temp_var_583, 64);
    mpfr_init2 (temp_var_584, 64);
    mpfr_init2 (temp_var_585, 64);
    mpfr_init2 (temp_var_586, 64);
    mpfr_init2 (temp_var_587, 64);
    mpfr_init2 (temp_var_588, 64);
    mpfr_init2 (temp_var_589, 64);
    mpfr_init2 (temp_var_590, 64);
    mpfr_init2 (temp_var_591, 64);
    mpfr_init2 (temp_var_592, 64);
    mpfr_init2 (temp_var_593, 64);
    mpfr_init2 (temp_var_594, 64);
    mpfr_init2 (temp_var_595, 64);
    mpfr_init2 (temp_var_596, 64);
    mpfr_init2 (temp_var_597, 64);
    mpfr_init2 (temp_var_598, 64);
    mpfr_init2 (temp_var_599, 64);
    mpfr_init2 (temp_var_600, 64);
    mpfr_init2 (temp_var_601, 64);
    mpfr_init2 (temp_var_602, 64);
    mpfr_init2 (temp_var_603, 64);
    mpfr_init2 (temp_var_604, 64);
    mpfr_init2 (temp_var_605, 64);
    mpfr_init2 (temp_var_606, 64);
    mpfr_init2 (temp_var_607, 64);
    mpfr_init2 (temp_var_608, 64);
    mpfr_init2 (temp_var_609, 64);
    mpfr_init2 (temp_var_610, 64);
    mpfr_init2 (temp_var_611, 64);
    mpfr_init2 (temp_var_612, 64);
    mpfr_init2 (temp_var_613, 64);
    mpfr_init2 (temp_var_614, 64);
    mpfr_init2 (temp_var_615, 64);
    mpfr_init2 (temp_var_616, 64);
    mpfr_init2 (temp_var_617, 64);
    mpfr_init2 (temp_var_618, 64);
    mpfr_init2 (temp_var_619, 64);
    mpfr_init2 (temp_var_620, 64);
    mpfr_init2 (temp_var_621, 64);
    mpfr_init2 (temp_var_622, 64);
    mpfr_init2 (temp_var_623, 64);
    mpfr_init2 (temp_var_624, 64);
    mpfr_init2 (temp_var_625, 64);
    mpfr_init2 (temp_var_626, 64);
    mpfr_init2 (temp_var_627, 64);
    mpfr_init2 (temp_var_628, 64);
    mpfr_init2 (temp_var_629, 64);
    mpfr_init2 (temp_var_630, 64);
    mpfr_init2 (temp_var_631, 64);
    mpfr_init2 (temp_var_632, 64);
    mpfr_init2 (temp_var_633, 64);
    mpfr_init2 (temp_var_634, 64);
    mpfr_init2 (temp_var_635, 64);
    mpfr_init2 (temp_var_636, 64);
    mpfr_init2 (temp_var_637, 64);
    mpfr_init2 (temp_var_638, 64);
    mpfr_init2 (temp_var_639, 64);
    mpfr_init2 (temp_var_640, 64);
    mpfr_init2 (temp_var_641, 64);
    mpfr_init2 (temp_var_642, 64);
    mpfr_init2 (temp_var_643, 64);
    mpfr_init2 (temp_var_644, 64);
    mpfr_init2 (temp_var_645, 64);
    mpfr_init2 (temp_var_646, 64);
    mpfr_init2 (temp_var_647, 64);
    mpfr_init2 (temp_var_648, 64);
    mpfr_init2 (temp_var_649, 64);
    mpfr_init2 (temp_var_650, 64);
    mpfr_init2 (temp_var_651, 64);
    mpfr_init2 (temp_var_652, 64);
    mpfr_init2 (temp_var_653, 64);
    mpfr_init2 (temp_var_654, 64);
    mpfr_init2 (temp_var_655, 64);
    mpfr_init2 (temp_var_656, 64);
    mpfr_init2 (temp_var_657, 64);
    mpfr_init2 (temp_var_658, 64);
    mpfr_init2 (temp_var_659, 64);
    mpfr_init2 (temp_var_660, 64);
    mpfr_init2 (temp_var_661, 64);
    mpfr_init2 (temp_var_662, 64);
    mpfr_init2 (temp_var_663, 64);
    mpfr_init2 (temp_var_664, 64);
    mpfr_init2 (temp_var_665, 64);
    mpfr_init2 (temp_var_666, 64);
    mpfr_init2 (temp_var_667, 64);
    mpfr_init2 (temp_var_668, 64);
    mpfr_init2 (temp_var_669, 64);
    mpfr_init2 (temp_var_670, 64);
    mpfr_init2 (temp_var_671, 64);
    mpfr_init2 (temp_var_672, 64);
    mpfr_init2 (temp_var_673, 64);
    mpfr_init2 (temp_var_674, 64);
    mpfr_init2 (temp_var_675, 64);
    mpfr_init2 (temp_var_676, 64);
    mpfr_init2 (temp_var_677, 64);
    mpfr_init2 (temp_var_678, 64);
    mpfr_init2 (temp_var_679, 64);
    mpfr_init2 (temp_var_680, 64);
    mpfr_init2 (temp_var_681, 64);
    mpfr_init2 (temp_var_682, 64);
    mpfr_init2 (temp_var_683, 64);
    mpfr_init2 (temp_var_684, 64);
    mpfr_init2 (temp_var_685, 64);
    mpfr_init2 (temp_var_686, 64);
    mpfr_init2 (temp_var_687, 64);
    mpfr_init2 (temp_var_688, 64);
    mpfr_init2 (temp_var_689, 64);
    mpfr_init2 (temp_var_690, 64);
    mpfr_init2 (temp_var_691, 64);
    mpfr_init2 (temp_var_692, 64);
    mpfr_init2 (temp_var_693, 64);
    mpfr_init2 (temp_var_694, 64);
    mpfr_init2 (temp_var_695, 64);
    mpfr_init2 (temp_var_696, 64);
    mpfr_init2 (temp_var_697, 64);
    mpfr_init2 (temp_var_698, 64);
    mpfr_init2 (temp_var_699, 64);
    mpfr_init2 (temp_var_700, 64);
    mpfr_init2 (temp_var_701, 64);
    mpfr_init2 (temp_var_702, 64);
    mpfr_init2 (temp_var_703, 64);
    mpfr_init2 (temp_var_704, 64);
    mpfr_init2 (temp_var_705, 64);
    mpfr_init2 (temp_var_706, 64);
    mpfr_init2 (temp_var_707, 64);
    mpfr_init2 (temp_var_708, 64);
    mpfr_init2 (temp_var_709, 64);
    mpfr_init2 (temp_var_710, 64);
    mpfr_init2 (temp_var_711, 64);
    mpfr_init2 (temp_var_712, 64);
    mpfr_init2 (temp_var_713, 64);
    mpfr_init2 (temp_var_714, 64);
    mpfr_init2 (temp_var_715, 64);
    mpfr_init2 (temp_var_716, 64);
    mpfr_init2 (temp_var_717, 64);
    mpfr_init2 (temp_var_718, 64);
    mpfr_init2 (temp_var_719, 64);
    mpfr_init2 (temp_var_720, 64);
    mpfr_init2 (temp_var_721, 64);
        mpfr_init2 (temp_var_722, 64);
    mpfr_init2 (temp_var_723, 64);
    mpfr_init2 (temp_var_724, 64);
    mpfr_init2 (temp_var_725, 64);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

void ecc(float timeinst, float *initvalu, int initvalu_offset, float *parameter, int parameter_offset, float *finavalu)
{
  int offset_1;
  int offset_2;
  int offset_3;
  int offset_4;
  int offset_5;
  int offset_6;
  int offset_7;
  int offset_8;
  int offset_9;
  int offset_10;
  int offset_11;
  int offset_12;
  int offset_13;
  int offset_14;
  int offset_15;
  int offset_16;
  int offset_17;
  int offset_18;
  int offset_19;
  int offset_20;
  int offset_21;
  int offset_22;
  int offset_23;
  int offset_24;
  int offset_25;
  int offset_26;
  int offset_27;
  int offset_28;
  int offset_29;
  int offset_30;
  int offset_31;
  int offset_32;
  int offset_33;
  int offset_34;
  int offset_35;
  int offset_36;
  int offset_37;
  int offset_38;
  int offset_39;
  int offset_40;
  int offset_41;
  int offset_42;
  int offset_43;
  int offset_44;
  int offset_45;
  int offset_46;
  int parameter_offset_1;
  int state;
  offset_1 = initvalu_offset;
  offset_2 = initvalu_offset + 1;
  offset_3 = initvalu_offset + 2;
  offset_4 = initvalu_offset + 3;
  offset_5 = initvalu_offset + 4;
  offset_6 = initvalu_offset + 5;
  offset_7 = initvalu_offset + 6;
  offset_8 = initvalu_offset + 7;
  offset_9 = initvalu_offset + 8;
  offset_10 = initvalu_offset + 9;
  offset_11 = initvalu_offset + 10;
  offset_12 = initvalu_offset + 11;
  offset_13 = initvalu_offset + 12;
  offset_14 = initvalu_offset + 13;
  offset_15 = initvalu_offset + 14;
  offset_16 = initvalu_offset + 15;
  offset_17 = initvalu_offset + 16;
  offset_18 = initvalu_offset + 17;
  offset_19 = initvalu_offset + 18;
  offset_20 = initvalu_offset + 19;
  offset_21 = initvalu_offset + 20;
  offset_22 = initvalu_offset + 21;
  offset_23 = initvalu_offset + 22;
  offset_24 = initvalu_offset + 23;
  offset_25 = initvalu_offset + 24;
  offset_26 = initvalu_offset + 25;
  offset_27 = initvalu_offset + 26;
  offset_28 = initvalu_offset + 27;
  offset_29 = initvalu_offset + 28;
  offset_30 = initvalu_offset + 29;
  offset_31 = initvalu_offset + 30;
  offset_32 = initvalu_offset + 31;
  offset_33 = initvalu_offset + 32;
  offset_34 = initvalu_offset + 33;
  offset_35 = initvalu_offset + 34;
  offset_36 = initvalu_offset + 35;
  offset_37 = initvalu_offset + 36;
  offset_38 = initvalu_offset + 37;
  offset_39 = initvalu_offset + 38;
  offset_40 = initvalu_offset + 39;
  offset_41 = initvalu_offset + 40;
  offset_42 = initvalu_offset + 41;
  offset_43 = initvalu_offset + 42;
  offset_44 = initvalu_offset + 43;
  offset_45 = initvalu_offset + 44;
  offset_46 = initvalu_offset + 45;
  parameter_offset_1 = parameter_offset;
  mpfr_set_d(initvalu_1_ecc, initvalu[offset_1], MPFR_RNDZ);
  mpfr_set_d(initvalu_2_ecc, initvalu[offset_2], MPFR_RNDZ);
  mpfr_set_d(initvalu_3_ecc, initvalu[offset_3], MPFR_RNDZ);
  mpfr_set_d(initvalu_4_ecc, initvalu[offset_4], MPFR_RNDZ);
  mpfr_set_d(initvalu_5_ecc, initvalu[offset_5], MPFR_RNDZ);
  mpfr_set_d(initvalu_6_ecc, initvalu[offset_6], MPFR_RNDZ);
  mpfr_set_d(initvalu_7_ecc, initvalu[offset_7], MPFR_RNDZ);
  mpfr_set_d(initvalu_8_ecc, initvalu[offset_8], MPFR_RNDZ);
  mpfr_set_d(initvalu_9_ecc, initvalu[offset_9], MPFR_RNDZ);
  mpfr_set_d(initvalu_10_ecc, initvalu[offset_10], MPFR_RNDZ);
  mpfr_set_d(initvalu_11_ecc, initvalu[offset_11], MPFR_RNDZ);
  mpfr_set_d(initvalu_12_ecc, initvalu[offset_12], MPFR_RNDZ);
  mpfr_set_d(initvalu_13_ecc, initvalu[offset_13], MPFR_RNDZ);
  mpfr_set_d(initvalu_14_ecc, initvalu[offset_14], MPFR_RNDZ);
  mpfr_set_d(initvalu_15_ecc, initvalu[offset_15], MPFR_RNDZ);
  mpfr_set_d(initvalu_16_ecc, initvalu[offset_16], MPFR_RNDZ);
  mpfr_set_d(initvalu_17_ecc, initvalu[offset_17], MPFR_RNDZ);
  mpfr_set_d(initvalu_18_ecc, initvalu[offset_18], MPFR_RNDZ);
  mpfr_set_d(initvalu_19_ecc, initvalu[offset_19], MPFR_RNDZ);
  mpfr_set_d(initvalu_20_ecc, initvalu[offset_20], MPFR_RNDZ);
  mpfr_set_d(initvalu_21_ecc, initvalu[offset_21], MPFR_RNDZ);
  mpfr_set_d(initvalu_22_ecc, initvalu[offset_22], MPFR_RNDZ);
  mpfr_set_d(initvalu_23_ecc, initvalu[offset_23], MPFR_RNDZ);
  mpfr_set_d(initvalu_24_ecc, initvalu[offset_24], MPFR_RNDZ);
  mpfr_set_d(initvalu_25_ecc, initvalu[offset_25], MPFR_RNDZ);
  mpfr_set_d(initvalu_26_ecc, initvalu[offset_26], MPFR_RNDZ);
  mpfr_set_d(initvalu_27_ecc, initvalu[offset_27], MPFR_RNDZ);
  mpfr_set_d(initvalu_28_ecc, initvalu[offset_28], MPFR_RNDZ);
  mpfr_set_d(initvalu_29_ecc, initvalu[offset_29], MPFR_RNDZ);
  mpfr_set_d(initvalu_30_ecc, initvalu[offset_30], MPFR_RNDZ);
  mpfr_set_d(initvalu_31_ecc, initvalu[offset_31], MPFR_RNDZ);
  mpfr_set_d(initvalu_32_ecc, initvalu[offset_32], MPFR_RNDZ);
  mpfr_set_d(initvalu_33_ecc, initvalu[offset_33], MPFR_RNDZ);
  mpfr_set_d(initvalu_34_ecc, initvalu[offset_34], MPFR_RNDZ);
  mpfr_set_d(initvalu_35_ecc, initvalu[offset_35], MPFR_RNDZ);
  mpfr_set_d(initvalu_36_ecc, initvalu[offset_36], MPFR_RNDZ);
  mpfr_set_d(initvalu_37_ecc, initvalu[offset_37], MPFR_RNDZ);
  mpfr_set_d(initvalu_38_ecc, initvalu[offset_38], MPFR_RNDZ);
  mpfr_set_d(initvalu_39_ecc, initvalu[offset_39], MPFR_RNDZ);
  mpfr_set_d(initvalu_40_ecc, initvalu[offset_40], MPFR_RNDZ);
  mpfr_set_d(initvalu_41_ecc, initvalu[offset_41], MPFR_RNDZ);
  mpfr_set_d(initvalu_42_ecc, initvalu[offset_42], MPFR_RNDZ);
  mpfr_set_d(initvalu_43_ecc, initvalu[offset_43], MPFR_RNDZ);
  mpfr_set_d(initvalu_44_ecc, initvalu[offset_44], MPFR_RNDZ);
  mpfr_set_d(initvalu_45_ecc, initvalu[offset_45], MPFR_RNDZ);
  mpfr_set_d(initvalu_46_ecc, initvalu[offset_46], MPFR_RNDZ);
  mpfr_set_d(parameter_1_ecc, parameter[parameter_offset_1], MPFR_RNDZ);
  mpfr_set_d(pi_ecc, 3.1416, MPFR_RNDZ);
  mpfr_set_d(R_ecc, 8314, MPFR_RNDZ);
  mpfr_set_d(Frdy_ecc, 96485, MPFR_RNDZ);
  mpfr_set_d(Temp_ecc, 310, MPFR_RNDZ);
  













































    mpfr_div(temp_var_46, Frdy_ecc, R_ecc, MPFR_RNDZ);
    mpfr_div(FoRT_ecc, (temp_var_46), Temp_ecc, MPFR_RNDZ);
;
  mpfr_set_d(Cmem_ecc, 1.3810e-10, MPFR_RNDZ);
  
    mpfr_sub_si(temp_var_47, Temp_ecc, 310, MPFR_RNDZ);
    mpfr_div_si(Qpow_ecc, (temp_var_47), 10, MPFR_RNDZ);
;
  mpfr_set_d(cellLength_ecc, 100, MPFR_RNDZ);
  mpfr_set_d(cellRadius_ecc, 10.25, MPFR_RNDZ);
  mpfr_set_d(junctionLength_ecc, 160e-3, MPFR_RNDZ);
  mpfr_set_d(junctionRadius_ecc, 15e-3, MPFR_RNDZ);
  mpfr_set_d(distSLcyto_ecc, 0.45, MPFR_RNDZ);
  mpfr_set_d(distJuncSL_ecc, 0.5, MPFR_RNDZ);
  mpfr_set_d(DcaJuncSL_ecc, 1.64e-6, MPFR_RNDZ);
  mpfr_set_d(DcaSLcyto_ecc, 1.22e-6, MPFR_RNDZ);
  mpfr_set_d(DnaJuncSL_ecc, 1.09e-5, MPFR_RNDZ);
  mpfr_set_d(DnaSLcyto_ecc, 1.79e-5, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_48, pi_ecc, pow(mpfr_get_d(cellRadius_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_mul(temp_var_49, (temp_var_48), cellLength_ecc, MPFR_RNDZ);
    mpfr_mul_d(Vcell_ecc, (temp_var_49), 1e-15, MPFR_RNDZ);
;
      mpfr_mul_d(Vmyo_ecc, Vcell_ecc, 0.65, MPFR_RNDZ);
;
      mpfr_mul_d(Vsr_ecc, Vcell_ecc, 0.035, MPFR_RNDZ);
;
      mpfr_mul_d(Vsl_ecc, Vcell_ecc, 0.02, MPFR_RNDZ);
;
      mpfr_mul_d(Vjunc_ecc, Vcell_ecc, (0.0539 * 0.01), MPFR_RNDZ);
;
  
    mpfr_mul_si(temp_var_50, pi_ecc, 20150, MPFR_RNDZ);

    mpfr_mul_si(temp_var_51, (temp_var_50), 2, MPFR_RNDZ);

    mpfr_mul(temp_var_52, (temp_var_51), junctionLength_ecc, MPFR_RNDZ);
    mpfr_mul(SAjunc_ecc, (temp_var_52), junctionRadius_ecc, MPFR_RNDZ);
;
  
    mpfr_mul_si(temp_var_53, pi_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_54, (temp_var_53), cellRadius_ecc, MPFR_RNDZ);
    mpfr_mul(SAsl_ecc, (temp_var_54), cellLength_ecc, MPFR_RNDZ);
;
  mpfr_set_d(J_ca_juncsl_ecc, 1 / 1.2134e12, MPFR_RNDZ);
  mpfr_set_d(J_ca_slmyo_ecc, 1 / 2.68510e11, MPFR_RNDZ);
  mpfr_set_d(J_na_juncsl_ecc, 1 / ((1.6382e12 / 3) * 100), MPFR_RNDZ);
  mpfr_set_d(J_na_slmyo_ecc, 1 / ((1.8308e10 / 3) * 100), MPFR_RNDZ);
  mpfr_set_d(Fjunc_ecc, 0.11, MPFR_RNDZ);
      mpfr_si_sub(Fsl_ecc, 1, Fjunc_ecc, MPFR_RNDZ);
;
  mpfr_set_d(Fjunc_CaL_ecc, 0.9, MPFR_RNDZ);
      mpfr_si_sub(Fsl_CaL_ecc, 1, Fjunc_CaL_ecc, MPFR_RNDZ);
;
  mpfr_set_d(Cli_ecc, 15, MPFR_RNDZ);
  mpfr_set_d(Clo_ecc, 150, MPFR_RNDZ);
  mpfr_set_d(Ko_ecc, 5.4, MPFR_RNDZ);
  mpfr_set_d(Nao_ecc, 140, MPFR_RNDZ);
  mpfr_set_d(Cao_ecc, 1.8, MPFR_RNDZ);
  mpfr_set_d(Mgi_ecc, 1, MPFR_RNDZ);
  
    mpfr_si_div(temp_var_55, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_mul_d(ena_junc_ecc, (temp_var_55), log(mpfr_get_d(Nao_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  
    mpfr_si_div(temp_var_57, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_mul_d(ena_sl_ecc, (temp_var_57), log(mpfr_get_d(Nao_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  
    mpfr_si_div(temp_var_59, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_mul_d(ek_ecc, (temp_var_59), log(mpfr_get_d(Ko_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_35_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  
    mpfr_si_div(temp_var_61, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_div_si(temp_var_62, (temp_var_61), 2, MPFR_RNDZ);

    mpfr_mul_d(eca_junc_ecc, (temp_var_62), log(mpfr_get_d(Cao_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  
    mpfr_si_div(temp_var_64, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_div_si(temp_var_65, (temp_var_64), 2, MPFR_RNDZ);

    mpfr_mul_d(eca_sl_ecc, (temp_var_65), log(mpfr_get_d(Cao_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_37_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  
    mpfr_si_div(temp_var_67, 1, FoRT_ecc, MPFR_RNDZ);

    mpfr_mul_d(ecl_ecc, (temp_var_67), log(mpfr_get_d(Cli_ecc, MPFR_RNDZ) / mpfr_get_d(Clo_ecc, MPFR_RNDZ)), MPFR_RNDZ);
;
  mpfr_set_d(GNa_ecc, 16.0, MPFR_RNDZ);
  mpfr_set_d(GNaB_ecc, 0.297e-3, MPFR_RNDZ);
  mpfr_set_d(IbarNaK_ecc, 1.90719, MPFR_RNDZ);
  mpfr_set_d(KmNaip_ecc, 11, MPFR_RNDZ);
  mpfr_set_d(KmKo_ecc, 1.5, MPFR_RNDZ);
  mpfr_set_d(Q10NaK_ecc, 1.63, MPFR_RNDZ);
  mpfr_set_d(Q10KmNai_ecc, 1.39, MPFR_RNDZ);
  mpfr_set_d(pNaK_ecc, 0.01833, MPFR_RNDZ);
  mpfr_set_d(GtoSlow_ecc, 0.06, MPFR_RNDZ);
  mpfr_set_d(GtoFast_ecc, 0.02, MPFR_RNDZ);
  mpfr_set_d(gkp_ecc, 0.001, MPFR_RNDZ);
  mpfr_set_d(GClCa_ecc, 0.109625, MPFR_RNDZ);
  mpfr_set_d(GClB_ecc, 9e-3, MPFR_RNDZ);
  mpfr_set_d(KdClCa_ecc, 100e-3, MPFR_RNDZ);
  mpfr_set_d(pNa_ecc, 1.5e-8, MPFR_RNDZ);
  mpfr_set_d(pCa_ecc, 5.4e-4, MPFR_RNDZ);
  mpfr_set_d(pK_ecc, 2.7e-7, MPFR_RNDZ);
  mpfr_set_d(KmCa_ecc, 0.6e-3, MPFR_RNDZ);
  mpfr_set_d(Q10CaL_ecc, 1.8, MPFR_RNDZ);
  mpfr_set_d(IbarNCX_ecc, 9.0, MPFR_RNDZ);
  mpfr_set_d(KmCai_ecc, 3.59e-3, MPFR_RNDZ);
  mpfr_set_d(KmCao_ecc, 1.3, MPFR_RNDZ);
  mpfr_set_d(KmNai_ecc, 12.29, MPFR_RNDZ);
  mpfr_set_d(KmNao_ecc, 87.5, MPFR_RNDZ);
  mpfr_set_d(ksat_ecc, 0.27, MPFR_RNDZ);
  mpfr_set_d(nu_ecc, 0.35, MPFR_RNDZ);
  mpfr_set_d(Kdact_ecc, 0.256e-3, MPFR_RNDZ);
  mpfr_set_d(Q10NCX_ecc, 1.57, MPFR_RNDZ);
  mpfr_set_d(IbarSLCaP_ecc, 0.0673, MPFR_RNDZ);
  mpfr_set_d(KmPCa_ecc, 0.5e-3, MPFR_RNDZ);
  mpfr_set_d(GCaB_ecc, 2.513e-4, MPFR_RNDZ);
  mpfr_set_d(Q10SLCaP_ecc, 2.35, MPFR_RNDZ);
  mpfr_set_d(Q10SRCaP_ecc, 2.6, MPFR_RNDZ);
  mpfr_set_d(Vmax_SRCaP_ecc, 2.86e-4, MPFR_RNDZ);
  mpfr_set_d(Kmf_ecc, 0.246e-3, MPFR_RNDZ);
  mpfr_set_d(Kmr_ecc, 1.7, MPFR_RNDZ);
  mpfr_set_d(hillSRCaP_ecc, 1.787, MPFR_RNDZ);
  mpfr_set_d(ks_ecc, 25, MPFR_RNDZ);
  mpfr_set_d(koCa_ecc, 10, MPFR_RNDZ);
  mpfr_set_d(kom_ecc, 0.06, MPFR_RNDZ);
  mpfr_set_d(kiCa_ecc, 0.5, MPFR_RNDZ);
  mpfr_set_d(kim_ecc, 0.005, MPFR_RNDZ);
  mpfr_set_d(ec50SR_ecc, 0.45, MPFR_RNDZ);
  mpfr_set_d(Bmax_Naj_ecc, 7.561, MPFR_RNDZ);
  mpfr_set_d(Bmax_Nasl_ecc, 1.65, MPFR_RNDZ);
  mpfr_set_d(koff_na_ecc, 1e-3, MPFR_RNDZ);
  mpfr_set_d(kon_na_ecc, 0.1e-3, MPFR_RNDZ);
  mpfr_set_d(Bmax_TnClow_ecc, 70e-3, MPFR_RNDZ);
  mpfr_set_d(koff_tncl_ecc, 19.6e-3, MPFR_RNDZ);
  mpfr_set_d(kon_tncl_ecc, 32.7, MPFR_RNDZ);
  mpfr_set_d(Bmax_TnChigh_ecc, 140e-3, MPFR_RNDZ);
  mpfr_set_d(koff_tnchca_ecc, 0.032e-3, MPFR_RNDZ);
  mpfr_set_d(kon_tnchca_ecc, 2.37, MPFR_RNDZ);
  mpfr_set_d(koff_tnchmg_ecc, 3.33e-3, MPFR_RNDZ);
  mpfr_set_d(kon_tnchmg_ecc, 3e-3, MPFR_RNDZ);
  mpfr_set_d(Bmax_CaM_ecc, 24e-3, MPFR_RNDZ);
  mpfr_set_d(koff_cam_ecc, 238e-3, MPFR_RNDZ);
  mpfr_set_d(kon_cam_ecc, 34, MPFR_RNDZ);
  mpfr_set_d(Bmax_myosin_ecc, 140e-3, MPFR_RNDZ);
  mpfr_set_d(koff_myoca_ecc, 0.46e-3, MPFR_RNDZ);
  mpfr_set_d(kon_myoca_ecc, 13.8, MPFR_RNDZ);
  mpfr_set_d(koff_myomg_ecc, 0.057e-3, MPFR_RNDZ);
  mpfr_set_d(kon_myomg_ecc, 0.0157, MPFR_RNDZ);
  mpfr_set_d(Bmax_SR_ecc, 19 * 0.9e-3, MPFR_RNDZ);
  mpfr_set_d(koff_sr_ecc, 60e-3, MPFR_RNDZ);
  mpfr_set_d(kon_sr_ecc, 100, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_69, Vmyo_ecc, 37.38e-3, MPFR_RNDZ);
    mpfr_div(Bmax_SLlowsl_ecc, (temp_var_69), Vsl_ecc, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_70, Vmyo_ecc, 4.62e-3, MPFR_RNDZ);

    mpfr_div(temp_var_71, (temp_var_70), Vjunc_ecc, MPFR_RNDZ);
    mpfr_mul_d(Bmax_SLlowj_ecc, (temp_var_71), 0.1, MPFR_RNDZ);
;
  mpfr_set_d(koff_sll_ecc, 1300e-3, MPFR_RNDZ);
  mpfr_set_d(kon_sll_ecc, 100, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_72, Vmyo_ecc, 13.35e-3, MPFR_RNDZ);
    mpfr_div(Bmax_SLhighsl_ecc, (temp_var_72), Vsl_ecc, MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_73, Vmyo_ecc, 1.65e-3, MPFR_RNDZ);

    mpfr_div(temp_var_74, (temp_var_73), Vjunc_ecc, MPFR_RNDZ);
    mpfr_mul_d(Bmax_SLhighj_ecc, (temp_var_74), 0.1, MPFR_RNDZ);
;
  mpfr_set_d(koff_slh_ecc, 30e-3, MPFR_RNDZ);
  mpfr_set_d(kon_slh_ecc, 100, MPFR_RNDZ);
  mpfr_set_d(Bmax_Csqn_ecc, 2.7, MPFR_RNDZ);
  mpfr_set_d(koff_csqn_ecc, 65, MPFR_RNDZ);
  mpfr_set_d(kon_csqn_ecc, 100, MPFR_RNDZ);
  
    mpfr_add_d(temp_var_75, initvalu_39_ecc, 47.13, MPFR_RNDZ);

    mpfr_mul_d(temp_var_76, (temp_var_75), 0.32, MPFR_RNDZ);


    mpfr_div_d(am_ecc, (temp_var_76), (1 - exp((-0.1) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 47.13))), MPFR_RNDZ);
;
  mpfr_set_d(bm_ecc, 0.08 * exp((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) / 11), MPFR_RNDZ);
 //initvalu_39 >= -40
  if (  mpfr_cmp_d (initvalu_39_ecc, (float)(-40)) >=0)
  {
    mpfr_set_d(ah_ecc, 0, MPFR_RNDZ);
    mpfr_set_d(aj_ecc, 0, MPFR_RNDZ);
    mpfr_set_d(bh_ecc, 1 / (0.13 * (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 10.66)) / 11.1))), MPFR_RNDZ);
    mpfr_set_d(bj_ecc, (0.3 * exp((-2.535e-7) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ))) / (1 + exp((-0.1) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 32))), MPFR_RNDZ);
  }
  else
  {
    mpfr_set_d(ah_ecc, 0.135 * exp((80 + mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) / (-6.8)), MPFR_RNDZ);
    mpfr_set_d(bh_ecc, (3.56 * exp(0.079 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ))) + (3.1e5 * exp(0.35 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ))), MPFR_RNDZ);
    

















        mpfr_add_d(temp_var_98, initvalu_39_ecc, 37.78, MPFR_RNDZ);

        mpfr_mul_d(temp_var_99, (temp_var_98), (((-127140) * exp(0.2444 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ))) - (3.474e-5 * exp((-0.04391) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)))), MPFR_RNDZ);


        mpfr_div_d(aj_ecc, (temp_var_99), (1 + exp(0.311 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 79.23))), MPFR_RNDZ);
;
    mpfr_set_d(bj_ecc, (0.1212 * exp((-0.01052) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ))) / (1 + exp((-0.1378) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 40.14))), MPFR_RNDZ);
  }

  




    mpfr_si_sub(temp_var_106, 1, initvalu_1_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_107, am_ecc, (temp_var_106), MPFR_RNDZ);

    mpfr_mul(temp_var_108, bm_ecc, initvalu_1_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_109, (temp_var_107), (temp_var_108), MPFR_RNDZ);

finavalu[offset_1] = mpfr_get_d(temp_var_109, MPFR_RNDZ);
  
    mpfr_si_sub(temp_var_110, 1, initvalu_2_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_111, ah_ecc, (temp_var_110), MPFR_RNDZ);

    mpfr_mul(temp_var_112, bh_ecc, initvalu_2_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_113, (temp_var_111), (temp_var_112), MPFR_RNDZ);

finavalu[offset_2] = mpfr_get_d(temp_var_113, MPFR_RNDZ);
  
    mpfr_si_sub(temp_var_114, 1, initvalu_3_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_115, aj_ecc, (temp_var_114), MPFR_RNDZ);

    mpfr_mul(temp_var_116, bj_ecc, initvalu_3_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_117, (temp_var_115), (temp_var_116), MPFR_RNDZ);

finavalu[offset_3] = mpfr_get_d(temp_var_117, MPFR_RNDZ);
  
    mpfr_mul(temp_var_118, Fjunc_ecc, GNa_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_119, (temp_var_118), pow(mpfr_get_d(initvalu_1_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_mul(temp_var_120, (temp_var_119), initvalu_2_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_121, (temp_var_120), initvalu_3_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_122, initvalu_39_ecc, ena_junc_ecc, MPFR_RNDZ);
    mpfr_mul(I_Na_junc_ecc, (temp_var_121), (temp_var_122), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_123, Fsl_ecc, GNa_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_124, (temp_var_123), pow(mpfr_get_d(initvalu_1_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_mul(temp_var_125, (temp_var_124), initvalu_2_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_126, (temp_var_125), initvalu_3_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_127, initvalu_39_ecc, ena_sl_ecc, MPFR_RNDZ);
    mpfr_mul(I_Na_sl_ecc, (temp_var_126), (temp_var_127), MPFR_RNDZ);
;
      mpfr_add(I_Na_ecc, I_Na_junc_ecc, I_Na_sl_ecc, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_128, Fjunc_ecc, GNaB_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_129, initvalu_39_ecc, ena_junc_ecc, MPFR_RNDZ);
    mpfr_mul(I_nabk_junc_ecc, (temp_var_128), (temp_var_129), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_130, Fsl_ecc, GNaB_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_131, initvalu_39_ecc, ena_sl_ecc, MPFR_RNDZ);
    mpfr_mul(I_nabk_sl_ecc, (temp_var_130), (temp_var_131), MPFR_RNDZ);
;
      mpfr_add(I_nabk_ecc, I_nabk_junc_ecc, I_nabk_sl_ecc, MPFR_RNDZ);
;
  mpfr_set_d(sigma_ecc, (exp(mpfr_get_d(Nao_ecc, MPFR_RNDZ) / 67.3) - 1) / 7, MPFR_RNDZ);
  




    mpfr_mul_d(temp_var_136, sigma_ecc, 0.0365, MPFR_RNDZ);


    mpfr_mul_d(temp_var_138, (temp_var_136), exp((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_add_d(temp_var_139, (temp_var_138), (1 + (0.1245 * exp(((-0.1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)))), MPFR_RNDZ);
    mpfr_si_div(fnak_ecc, 1, (temp_var_139), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_140, Fjunc_ecc, IbarNaK_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_141, (temp_var_140), fnak_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_142, (temp_var_141), Ko_ecc, MPFR_RNDZ);



    mpfr_div_d(temp_var_145, (temp_var_142), (1 + pow(mpfr_get_d(KmNaip_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ), 4)), MPFR_RNDZ);

    mpfr_add(temp_var_146, Ko_ecc, KmKo_ecc, MPFR_RNDZ);
    mpfr_div(I_nak_junc_ecc, (temp_var_145), (temp_var_146), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_147, Fsl_ecc, IbarNaK_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_148, (temp_var_147), fnak_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_149, (temp_var_148), Ko_ecc, MPFR_RNDZ);



    mpfr_div_d(temp_var_152, (temp_var_149), (1 + pow(mpfr_get_d(KmNaip_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ), 4)), MPFR_RNDZ);

    mpfr_add(temp_var_153, Ko_ecc, KmKo_ecc, MPFR_RNDZ);
    mpfr_div(I_nak_sl_ecc, (temp_var_152), (temp_var_153), MPFR_RNDZ);
;
      mpfr_add(I_nak_ecc, I_nak_junc_ecc, I_nak_sl_ecc, MPFR_RNDZ);
;
  mpfr_set_d(gkr_ecc, 0.03 * sqrt(mpfr_get_d(Ko_ecc, MPFR_RNDZ) / 5.4), MPFR_RNDZ);
  mpfr_set_d(xrss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 50)) / 7.5)), MPFR_RNDZ);
  





    mpfr_add_si(temp_var_159, initvalu_39_ecc, 7, MPFR_RNDZ);

    mpfr_mul_d(temp_var_160, (temp_var_159), 0.00138, MPFR_RNDZ);



    mpfr_div_d(temp_var_163, (temp_var_160), (1 - exp((-0.123) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 7))), MPFR_RNDZ);

    mpfr_add_si(temp_var_164, initvalu_39_ecc, 10, MPFR_RNDZ);

    mpfr_mul_d(temp_var_165, (temp_var_164), 6.1e-4, MPFR_RNDZ);



    mpfr_div_d(temp_var_168, (temp_var_165), (exp(0.145 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 10)) - 1), MPFR_RNDZ);

    mpfr_add(temp_var_169, (temp_var_163), (temp_var_168), MPFR_RNDZ);
    mpfr_si_div(tauxr_ecc, 1, (temp_var_169), MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_170, xrss_ecc, initvalu_12_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_171, (temp_var_170), tauxr_ecc, MPFR_RNDZ);

finavalu[offset_12] = mpfr_get_d(temp_var_171, MPFR_RNDZ);
  mpfr_set_d(rkr_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 33) / 22.4)), MPFR_RNDZ);
  


    mpfr_mul(temp_var_174, gkr_ecc, initvalu_12_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_175, (temp_var_174), rkr_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_176, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_kr_ecc, (temp_var_175), (temp_var_176), MPFR_RNDZ);
;
  mpfr_set_d(pcaks_junc_ecc, (-log10(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ))) + 3.0, MPFR_RNDZ);
  mpfr_set_d(pcaks_sl_ecc, (-log10(mpfr_get_d(initvalu_37_ecc, MPFR_RNDZ))) + 3.0, MPFR_RNDZ);
  mpfr_set_d(gks_junc_ecc, 0.07 * (0.057 + (0.19 / (1 + exp(((-7.2) + mpfr_get_d(pcaks_junc_ecc, MPFR_RNDZ)) / 0.6)))), MPFR_RNDZ);
  mpfr_set_d(gks_sl_ecc, 0.07 * (0.057 + (0.19 / (1 + exp(((-7.2) + mpfr_get_d(pcaks_sl_ecc, MPFR_RNDZ)) / 0.6)))), MPFR_RNDZ);
  






    mpfr_si_div(temp_var_183, 1, FoRT_ecc, MPFR_RNDZ);


    mpfr_mul_d(eks_ecc, (temp_var_183), log((mpfr_get_d(Ko_ecc, MPFR_RNDZ) + (mpfr_get_d(pNaK_ecc, MPFR_RNDZ) * mpfr_get_d(Nao_ecc, MPFR_RNDZ))) / (mpfr_get_d(initvalu_35_ecc, MPFR_RNDZ) + (mpfr_get_d(pNaK_ecc, MPFR_RNDZ) * mpfr_get_d(initvalu_34_ecc, MPFR_RNDZ)))), MPFR_RNDZ);
;
  mpfr_set_d(xsss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) - 1.5)) / 16.7)), MPFR_RNDZ);
  



    mpfr_add_si(temp_var_189, initvalu_39_ecc, 30, MPFR_RNDZ);

    mpfr_mul_d(temp_var_190, (temp_var_189), 7.19e-5, MPFR_RNDZ);



    mpfr_div_d(temp_var_193, (temp_var_190), (1 - exp((-0.148) * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 30))), MPFR_RNDZ);

    mpfr_add_si(temp_var_194, initvalu_39_ecc, 30, MPFR_RNDZ);

    mpfr_mul_d(temp_var_195, (temp_var_194), 1.31e-4, MPFR_RNDZ);



    mpfr_div_d(temp_var_198, (temp_var_195), (exp(0.0687 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 30)) - 1), MPFR_RNDZ);

    mpfr_add(temp_var_199, (temp_var_193), (temp_var_198), MPFR_RNDZ);
    mpfr_si_div(tauxs_ecc, 1, (temp_var_199), MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_200, xsss_ecc, initvalu_13_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_201, (temp_var_200), tauxs_ecc, MPFR_RNDZ);

finavalu[offset_13] = mpfr_get_d(temp_var_201, MPFR_RNDZ);
  
    mpfr_mul(temp_var_202, Fjunc_ecc, gks_junc_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_203, (temp_var_202), pow(mpfr_get_d(initvalu_12_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_sub(temp_var_204, initvalu_39_ecc, eks_ecc, MPFR_RNDZ);
    mpfr_mul(I_ks_junc_ecc, (temp_var_203), (temp_var_204), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_205, Fsl_ecc, gks_sl_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_206, (temp_var_205), pow(mpfr_get_d(initvalu_13_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_sub(temp_var_207, initvalu_39_ecc, eks_ecc, MPFR_RNDZ);
    mpfr_mul(I_ks_sl_ecc, (temp_var_206), (temp_var_207), MPFR_RNDZ);
;
      mpfr_add(I_ks_ecc, I_ks_junc_ecc, I_ks_sl_ecc, MPFR_RNDZ);
;
  mpfr_set_d(kp_kp_ecc, 1 / (1 + exp(7.488 - (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) / 5.98))), MPFR_RNDZ);
  


    mpfr_mul(temp_var_210, Fjunc_ecc, gkp_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_211, (temp_var_210), kp_kp_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_212, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_kp_junc_ecc, (temp_var_211), (temp_var_212), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_213, Fsl_ecc, gkp_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_214, (temp_var_213), kp_kp_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_215, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_kp_sl_ecc, (temp_var_214), (temp_var_215), MPFR_RNDZ);
;
      mpfr_add(I_kp_ecc, I_kp_junc_ecc, I_kp_sl_ecc, MPFR_RNDZ);
;
  mpfr_set_d(xtoss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 3.0)) / 15)), MPFR_RNDZ);
  mpfr_set_d(ytoss_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 33.5) / 10)), MPFR_RNDZ);
  mpfr_set_d(rtoss_ecc, 1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 33.5) / 10)), MPFR_RNDZ);
  mpfr_set_d(tauxtos_ecc, (9 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 3.0) / 15))) + 0.5, MPFR_RNDZ);
  mpfr_set_d(tauytos_ecc, (3e3 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 60.0) / 10))) + 30, MPFR_RNDZ);
  mpfr_set_d(taurtos_ecc, (2800 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 60.0) / 10))) + 220, MPFR_RNDZ);
  













    mpfr_sub(temp_var_229, xtoss_ecc, initvalu_8_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_230, (temp_var_229), tauxtos_ecc, MPFR_RNDZ);

finavalu[offset_8] = mpfr_get_d(temp_var_230, MPFR_RNDZ);
  
    mpfr_sub(temp_var_231, ytoss_ecc, initvalu_9_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_232, (temp_var_231), tauytos_ecc, MPFR_RNDZ);

finavalu[offset_9] = mpfr_get_d(temp_var_232, MPFR_RNDZ);
  
    mpfr_sub(temp_var_233, rtoss_ecc, initvalu_40_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_234, (temp_var_233), taurtos_ecc, MPFR_RNDZ);

finavalu[offset_40] = mpfr_get_d(temp_var_234, MPFR_RNDZ);
  
    mpfr_mul(temp_var_235, GtoSlow_ecc, initvalu_8_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_236, initvalu_40_ecc, 0.5, MPFR_RNDZ);

    mpfr_add(temp_var_237, initvalu_9_ecc, (temp_var_236), MPFR_RNDZ);

    mpfr_mul(temp_var_238, (temp_var_235), (temp_var_237), MPFR_RNDZ);

    mpfr_sub(temp_var_239, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_tos_ecc, (temp_var_238), (temp_var_239), MPFR_RNDZ);
;
  mpfr_set_d(tauxtof_ecc, (3.5 * exp((((-mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) / 30) / 30)) + 1.5, MPFR_RNDZ);
  mpfr_set_d(tauytof_ecc, (20.0 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 33.5) / 10))) + 20.0, MPFR_RNDZ);
  




    mpfr_sub(temp_var_244, xtoss_ecc, initvalu_10_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_245, (temp_var_244), tauxtof_ecc, MPFR_RNDZ);

finavalu[offset_10] = mpfr_get_d(temp_var_245, MPFR_RNDZ);
  
    mpfr_sub(temp_var_246, ytoss_ecc, initvalu_11_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_247, (temp_var_246), tauytof_ecc, MPFR_RNDZ);

finavalu[offset_11] = mpfr_get_d(temp_var_247, MPFR_RNDZ);
  
    mpfr_mul(temp_var_248, GtoFast_ecc, initvalu_10_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_249, (temp_var_248), initvalu_11_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_250, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_tof_ecc, (temp_var_249), (temp_var_250), MPFR_RNDZ);
;
      mpfr_add(I_to_ecc, I_tos_ecc, I_tof_ecc, MPFR_RNDZ);
;
  mpfr_set_d(aki_ecc, 1.02 / (1 + exp(0.2385 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) - mpfr_get_d(ek_ecc, MPFR_RNDZ)) - 59.215))), MPFR_RNDZ);
  mpfr_set_d(bki_ecc, ((0.49124 * exp(0.08032 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 5.476) - mpfr_get_d(ek_ecc, MPFR_RNDZ)))) + exp(0.06175 * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) - mpfr_get_d(ek_ecc, MPFR_RNDZ)) - 594.31))) / (1 + exp((-0.5143) * ((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) - mpfr_get_d(ek_ecc, MPFR_RNDZ)) + 4.753))), MPFR_RNDZ);
  







    mpfr_add(temp_var_258, aki_ecc, bki_ecc, MPFR_RNDZ);
    mpfr_div(kiss_ecc, aki_ecc, (temp_var_258), MPFR_RNDZ);
;
  


    mpfr_mul_d(temp_var_261, kiss_ecc, (0.9 * sqrt(mpfr_get_d(Ko_ecc, MPFR_RNDZ) / 5.4)), MPFR_RNDZ);

    mpfr_sub(temp_var_262, initvalu_39_ecc, ek_ecc, MPFR_RNDZ);
    mpfr_mul(I_ki_ecc, (temp_var_261), (temp_var_262), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_263, Fjunc_ecc, GClCa_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_264, KdClCa_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_add_si(temp_var_265, (temp_var_264), 1, MPFR_RNDZ);

    mpfr_div(temp_var_266, (temp_var_263), (temp_var_265), MPFR_RNDZ);

    mpfr_sub(temp_var_267, initvalu_39_ecc, ecl_ecc, MPFR_RNDZ);
    mpfr_mul(I_ClCa_junc_ecc, (temp_var_266), (temp_var_267), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_268, Fsl_ecc, GClCa_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_269, KdClCa_ecc, initvalu_37_ecc, MPFR_RNDZ);

    mpfr_add_si(temp_var_270, (temp_var_269), 1, MPFR_RNDZ);

    mpfr_div(temp_var_271, (temp_var_268), (temp_var_270), MPFR_RNDZ);

    mpfr_sub(temp_var_272, initvalu_39_ecc, ecl_ecc, MPFR_RNDZ);
    mpfr_mul(I_ClCa_sl_ecc, (temp_var_271), (temp_var_272), MPFR_RNDZ);
;
      mpfr_add(I_ClCa_ecc, I_ClCa_junc_ecc, I_ClCa_sl_ecc, MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_273, initvalu_39_ecc, ecl_ecc, MPFR_RNDZ);
    mpfr_mul(I_Clbk_ecc, GClB_ecc, (temp_var_273), MPFR_RNDZ);
;
  mpfr_set_d(dss_ecc, 1 / (1 + exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 14.5)) / 6.0)), MPFR_RNDZ);
  






    mpfr_mul_d(temp_var_280, dss_ecc, (1 - exp((-(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 14.5)) / 6.0)), MPFR_RNDZ);

    mpfr_add_d(temp_var_281, initvalu_39_ecc, 14.5, MPFR_RNDZ);

    mpfr_mul_d(temp_var_282, (temp_var_281), 0.035, MPFR_RNDZ);
    mpfr_div(taud_ecc, (temp_var_280), (temp_var_282), MPFR_RNDZ);
;
  mpfr_set_d(fss_ecc, (1 / (1 + exp((mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 35.06) / 3.6))) + (0.6 / (1 + exp((50 - mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) / 20))), MPFR_RNDZ);
  mpfr_set_d(tauf_ecc, 1 / ((0.0197 * exp(-pow(0.0337 * (mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) + 14.5), 2))) + 0.02), MPFR_RNDZ);
  






    mpfr_sub(temp_var_289, dss_ecc, initvalu_4_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_290, (temp_var_289), taud_ecc, MPFR_RNDZ);

finavalu[offset_4] = mpfr_get_d(temp_var_290, MPFR_RNDZ);
  
    mpfr_sub(temp_var_291, fss_ecc, initvalu_5_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_292, (temp_var_291), tauf_ecc, MPFR_RNDZ);

finavalu[offset_5] = mpfr_get_d(temp_var_292, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_293, initvalu_36_ecc, 1.7, MPFR_RNDZ);

    mpfr_si_sub(temp_var_294, 1, initvalu_6_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_295, (temp_var_293), (temp_var_294), MPFR_RNDZ);

    mpfr_mul_d(temp_var_296, initvalu_6_ecc, 11.9e-3, MPFR_RNDZ);

    mpfr_sub(temp_var_297, (temp_var_295), (temp_var_296), MPFR_RNDZ);

finavalu[offset_6] = mpfr_get_d(temp_var_297, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_298, initvalu_37_ecc, 1.7, MPFR_RNDZ);

    mpfr_si_sub(temp_var_299, 1, initvalu_7_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_300, (temp_var_298), (temp_var_299), MPFR_RNDZ);

    mpfr_mul_d(temp_var_301, initvalu_7_ecc, 11.9e-3, MPFR_RNDZ);

    mpfr_sub(temp_var_302, (temp_var_300), (temp_var_301), MPFR_RNDZ);

finavalu[offset_7] = mpfr_get_d(temp_var_302, MPFR_RNDZ);
  
    mpfr_mul_si(temp_var_303, pCa_ecc, 4, MPFR_RNDZ);

    mpfr_mul(temp_var_304, initvalu_39_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_305, (temp_var_304), FoRT_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_306, (temp_var_303), (temp_var_305), MPFR_RNDZ);

    mpfr_mul_d(temp_var_307, initvalu_36_ecc, 0.341, MPFR_RNDZ);


    mpfr_mul_d(temp_var_309, (temp_var_307), exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_310, Cao_ecc, 0.341, MPFR_RNDZ);

    mpfr_sub(temp_var_311, (temp_var_309), (temp_var_310), MPFR_RNDZ);

    mpfr_mul(temp_var_312, (temp_var_306), (temp_var_311), MPFR_RNDZ);


    mpfr_div_d(ibarca_j_ecc, (temp_var_312), (exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) - 1), MPFR_RNDZ);
;
  
    mpfr_mul_si(temp_var_315, pCa_ecc, 4, MPFR_RNDZ);

    mpfr_mul(temp_var_316, initvalu_39_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_317, (temp_var_316), FoRT_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_318, (temp_var_315), (temp_var_317), MPFR_RNDZ);

    mpfr_mul_d(temp_var_319, initvalu_37_ecc, 0.341, MPFR_RNDZ);


    mpfr_mul_d(temp_var_321, (temp_var_319), exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_322, Cao_ecc, 0.341, MPFR_RNDZ);

    mpfr_sub(temp_var_323, (temp_var_321), (temp_var_322), MPFR_RNDZ);

    mpfr_mul(temp_var_324, (temp_var_318), (temp_var_323), MPFR_RNDZ);


    mpfr_div_d(ibarca_sl_ecc, (temp_var_324), (exp((2 * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) - 1), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_327, initvalu_39_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_328, (temp_var_327), FoRT_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_329, pK_ecc, (temp_var_328), MPFR_RNDZ);

    mpfr_mul_d(temp_var_330, initvalu_35_ecc, 0.75, MPFR_RNDZ);


    mpfr_mul_d(temp_var_332, (temp_var_330), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_333, Ko_ecc, 0.75, MPFR_RNDZ);

    mpfr_sub(temp_var_334, (temp_var_332), (temp_var_333), MPFR_RNDZ);

    mpfr_mul(temp_var_335, (temp_var_329), (temp_var_334), MPFR_RNDZ);


    mpfr_div_d(ibark_ecc, (temp_var_335), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) - 1), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_338, initvalu_39_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_339, (temp_var_338), FoRT_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_340, pNa_ecc, (temp_var_339), MPFR_RNDZ);

    mpfr_mul_d(temp_var_341, initvalu_32_ecc, 0.75, MPFR_RNDZ);


    mpfr_mul_d(temp_var_343, (temp_var_341), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_344, Nao_ecc, 0.75, MPFR_RNDZ);

    mpfr_sub(temp_var_345, (temp_var_343), (temp_var_344), MPFR_RNDZ);

    mpfr_mul(temp_var_346, (temp_var_340), (temp_var_345), MPFR_RNDZ);


    mpfr_div_d(ibarna_j_ecc, (temp_var_346), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) - 1), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_349, initvalu_39_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_350, (temp_var_349), FoRT_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_351, pNa_ecc, (temp_var_350), MPFR_RNDZ);

    mpfr_mul_d(temp_var_352, initvalu_33_ecc, 0.75, MPFR_RNDZ);


    mpfr_mul_d(temp_var_354, (temp_var_352), exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_355, Nao_ecc, 0.75, MPFR_RNDZ);

    mpfr_sub(temp_var_356, (temp_var_354), (temp_var_355), MPFR_RNDZ);

    mpfr_mul(temp_var_357, (temp_var_351), (temp_var_356), MPFR_RNDZ);


    mpfr_div_d(ibarna_sl_ecc, (temp_var_357), (exp(mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) - 1), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_360, Fjunc_CaL_ecc, ibarca_j_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_361, (temp_var_360), initvalu_4_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_362, (temp_var_361), initvalu_5_ecc, MPFR_RNDZ);

    mpfr_si_sub(temp_var_363, 1, initvalu_6_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_364, (temp_var_362), (temp_var_363), MPFR_RNDZ);

    mpfr_mul_d(temp_var_365, (temp_var_364), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);
    mpfr_mul_d(I_Ca_junc_ecc, (temp_var_365), 0.45, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_366, Fsl_CaL_ecc, ibarca_sl_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_367, (temp_var_366), initvalu_4_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_368, (temp_var_367), initvalu_5_ecc, MPFR_RNDZ);

    mpfr_si_sub(temp_var_369, 1, initvalu_7_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_370, (temp_var_368), (temp_var_369), MPFR_RNDZ);

    mpfr_mul_d(temp_var_371, (temp_var_370), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);
    mpfr_mul_d(I_Ca_sl_ecc, (temp_var_371), 0.45, MPFR_RNDZ);
;
      mpfr_add(I_Ca_ecc, I_Ca_junc_ecc, I_Ca_sl_ecc, MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_373, (I_Ca_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_372, temp_var_373, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_374, Vmyo_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_375, (temp_var_374), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_376, (temp_var_372), (temp_var_375), MPFR_RNDZ);

    mpfr_mul_d(temp_var_377, (temp_var_376), 1e3, MPFR_RNDZ);

finavalu[offset_43] = mpfr_get_d(temp_var_377, MPFR_RNDZ);
  
    mpfr_mul(temp_var_378, ibark_ecc, initvalu_4_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_379, (temp_var_378), initvalu_5_ecc, MPFR_RNDZ);

    mpfr_si_sub(temp_var_380, 1, initvalu_6_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_381, Fjunc_CaL_ecc, (temp_var_380), MPFR_RNDZ);

    mpfr_si_sub(temp_var_382, 1, initvalu_7_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_383, Fsl_CaL_ecc, (temp_var_382), MPFR_RNDZ);

    mpfr_add(temp_var_384, (temp_var_381), (temp_var_383), MPFR_RNDZ);

    mpfr_mul(temp_var_385, (temp_var_379), (temp_var_384), MPFR_RNDZ);

    mpfr_mul_d(temp_var_386, (temp_var_385), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);
    mpfr_mul_d(I_CaK_ecc, (temp_var_386), 0.45, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_387, Fjunc_CaL_ecc, ibarna_j_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_388, (temp_var_387), initvalu_4_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_389, (temp_var_388), initvalu_5_ecc, MPFR_RNDZ);

    mpfr_si_sub(temp_var_390, 1, initvalu_6_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_391, (temp_var_389), (temp_var_390), MPFR_RNDZ);

    mpfr_mul_d(temp_var_392, (temp_var_391), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);
    mpfr_mul_d(I_CaNa_junc_ecc, (temp_var_392), 0.45, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_393, Fsl_CaL_ecc, ibarna_sl_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_394, (temp_var_393), initvalu_4_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_395, (temp_var_394), initvalu_5_ecc, MPFR_RNDZ);

    mpfr_si_sub(temp_var_396, 1, initvalu_7_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_397, (temp_var_395), (temp_var_396), MPFR_RNDZ);

    mpfr_mul_d(temp_var_398, (temp_var_397), pow(mpfr_get_d(Q10CaL_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);
    mpfr_mul_d(I_CaNa_sl_ecc, (temp_var_398), 0.45, MPFR_RNDZ);
;
      mpfr_add(I_CaNa_ecc, I_CaNa_junc_ecc, I_CaNa_sl_ecc, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_399, I_Ca_ecc, I_CaK_ecc, MPFR_RNDZ);
    mpfr_add(I_Catot_ecc, (temp_var_399), I_CaNa_ecc, MPFR_RNDZ);
;
  mpfr_set_d(Ka_junc_ecc, 1 / (1 + pow(mpfr_get_d(Kdact_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
  mpfr_set_d(Ka_sl_ecc, 1 / (1 + pow(mpfr_get_d(Kdact_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_37_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
  





    mpfr_mul_d(s1_junc_ecc, Cao_ecc, (exp((mpfr_get_d(nu_ecc, MPFR_RNDZ) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) * pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
;
  

    mpfr_mul_d(s1_sl_ecc, Cao_ecc, (exp((mpfr_get_d(nu_ecc, MPFR_RNDZ) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) * pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
;
  

    mpfr_mul_d(s2_junc_ecc, initvalu_36_ecc, (exp(((mpfr_get_d(nu_ecc, MPFR_RNDZ) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) * pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_410, KmCai_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);



    mpfr_mul_d(temp_var_413, (temp_var_410), (1 + pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ) / mpfr_get_d(KmNai_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_414, initvalu_36_ecc, pow(mpfr_get_d(KmNao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_415, (temp_var_413), (temp_var_414), MPFR_RNDZ);

    mpfr_mul_d(temp_var_416, Cao_ecc, pow(mpfr_get_d(KmNai_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_div(temp_var_417, initvalu_36_ecc, KmCai_ecc, MPFR_RNDZ);

    mpfr_add_si(temp_var_418, (temp_var_417), 1, MPFR_RNDZ);

    mpfr_mul(temp_var_419, (temp_var_416), (temp_var_418), MPFR_RNDZ);

    mpfr_add(temp_var_420, (temp_var_415), (temp_var_419), MPFR_RNDZ);

    mpfr_mul_d(temp_var_421, KmCao_ecc, pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_422, (temp_var_420), (temp_var_421), MPFR_RNDZ);

    mpfr_mul_d(temp_var_423, Cao_ecc, pow(mpfr_get_d(initvalu_32_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_424, (temp_var_422), (temp_var_423), MPFR_RNDZ);

    mpfr_mul_d(temp_var_425, initvalu_36_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_426, (temp_var_424), (temp_var_425), MPFR_RNDZ);


    mpfr_mul_d(temp_var_428, ksat_ecc, exp(((mpfr_get_d(nu_ecc, MPFR_RNDZ) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_add_si(temp_var_429, (temp_var_428), 1, MPFR_RNDZ);
    mpfr_mul(s3_junc_ecc, (temp_var_426), (temp_var_429), MPFR_RNDZ);
;
  

    mpfr_mul_d(s2_sl_ecc, initvalu_37_ecc, (exp(((mpfr_get_d(nu_ecc, MPFR_RNDZ) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)) * pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_432, KmCai_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);



    mpfr_mul_d(temp_var_435, (temp_var_432), (1 + pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ) / mpfr_get_d(KmNai_ecc, MPFR_RNDZ), 3)), MPFR_RNDZ);

    mpfr_mul_d(temp_var_436, initvalu_37_ecc, pow(mpfr_get_d(KmNao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_437, (temp_var_435), (temp_var_436), MPFR_RNDZ);

    mpfr_mul_d(temp_var_438, Cao_ecc, pow(mpfr_get_d(KmNai_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_div(temp_var_439, initvalu_37_ecc, KmCai_ecc, MPFR_RNDZ);

    mpfr_add_si(temp_var_440, (temp_var_439), 1, MPFR_RNDZ);

    mpfr_mul(temp_var_441, (temp_var_438), (temp_var_440), MPFR_RNDZ);

    mpfr_add(temp_var_442, (temp_var_437), (temp_var_441), MPFR_RNDZ);

    mpfr_mul_d(temp_var_443, KmCao_ecc, pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_444, (temp_var_442), (temp_var_443), MPFR_RNDZ);

    mpfr_mul_d(temp_var_445, Cao_ecc, pow(mpfr_get_d(initvalu_33_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_446, (temp_var_444), (temp_var_445), MPFR_RNDZ);

    mpfr_mul_d(temp_var_447, initvalu_37_ecc, pow(mpfr_get_d(Nao_ecc, MPFR_RNDZ), 3), MPFR_RNDZ);

    mpfr_add(temp_var_448, (temp_var_446), (temp_var_447), MPFR_RNDZ);


    mpfr_mul_d(temp_var_450, ksat_ecc, exp(((mpfr_get_d(nu_ecc, MPFR_RNDZ) - 1) * mpfr_get_d(initvalu_39_ecc, MPFR_RNDZ)) * mpfr_get_d(FoRT_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_add_si(temp_var_451, (temp_var_450), 1, MPFR_RNDZ);
    mpfr_mul(s3_sl_ecc, (temp_var_448), (temp_var_451), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_452, Fjunc_ecc, IbarNCX_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_453, (temp_var_452), pow(mpfr_get_d(Q10NCX_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul(temp_var_454, (temp_var_453), Ka_junc_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_455, s1_junc_ecc, s2_junc_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_456, (temp_var_454), (temp_var_455), MPFR_RNDZ);
    mpfr_div(I_ncx_junc_ecc, (temp_var_456), s3_junc_ecc, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_457, Fsl_ecc, IbarNCX_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_458, (temp_var_457), pow(mpfr_get_d(Q10NCX_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul(temp_var_459, (temp_var_458), Ka_sl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_460, s1_sl_ecc, s2_sl_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_461, (temp_var_459), (temp_var_460), MPFR_RNDZ);
    mpfr_div(I_ncx_sl_ecc, (temp_var_461), s3_sl_ecc, MPFR_RNDZ);
;
      mpfr_add(I_ncx_ecc, I_ncx_junc_ecc, I_ncx_sl_ecc, MPFR_RNDZ);
;
  
    mpfr_mul_si(temp_var_462, I_ncx_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_463, (temp_var_462), Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_464, Vmyo_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_465, (temp_var_464), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_466, (temp_var_463), (temp_var_465), MPFR_RNDZ);

    mpfr_mul_d(temp_var_467, (temp_var_466), 1e3, MPFR_RNDZ);

finavalu[offset_45] = mpfr_get_d(temp_var_467, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_468, Fjunc_ecc, pow(mpfr_get_d(Q10SLCaP_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul(temp_var_469, (temp_var_468), IbarSLCaP_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_470, (temp_var_469), pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 1.6), MPFR_RNDZ);

    mpfr_div_d(I_pca_junc_ecc, (temp_var_470), (pow(mpfr_get_d(KmPCa_ecc, MPFR_RNDZ), 1.6) + pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 1.6)), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_472, Fsl_ecc, pow(mpfr_get_d(Q10SLCaP_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);

    mpfr_mul(temp_var_473, (temp_var_472), IbarSLCaP_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_474, (temp_var_473), pow(mpfr_get_d(initvalu_37_ecc, MPFR_RNDZ), 1.6), MPFR_RNDZ);

    mpfr_div_d(I_pca_sl_ecc, (temp_var_474), (pow(mpfr_get_d(KmPCa_ecc, MPFR_RNDZ), 1.6) + pow(mpfr_get_d(initvalu_37_ecc, MPFR_RNDZ), 1.6)), MPFR_RNDZ);
;
      mpfr_add(I_pca_ecc, I_pca_junc_ecc, I_pca_sl_ecc, MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_477, (I_pca_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_476, temp_var_477, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_478, Vmyo_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_479, (temp_var_478), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_480, (temp_var_476), (temp_var_479), MPFR_RNDZ);

    mpfr_mul_d(temp_var_481, (temp_var_480), 1e3, MPFR_RNDZ);

finavalu[offset_44] = mpfr_get_d(temp_var_481, MPFR_RNDZ);
  
    mpfr_mul(temp_var_482, Fjunc_ecc, GCaB_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_483, initvalu_39_ecc, eca_junc_ecc, MPFR_RNDZ);
    mpfr_mul(I_cabk_junc_ecc, (temp_var_482), (temp_var_483), MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_484, Fsl_ecc, GCaB_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_485, initvalu_39_ecc, eca_sl_ecc, MPFR_RNDZ);
    mpfr_mul(I_cabk_sl_ecc, (temp_var_484), (temp_var_485), MPFR_RNDZ);
;
      mpfr_add(I_cabk_ecc, I_cabk_junc_ecc, I_cabk_sl_ecc, MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_487, (I_cabk_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_486, temp_var_487, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_488, Vmyo_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_489, (temp_var_488), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_490, (temp_var_486), (temp_var_489), MPFR_RNDZ);

    mpfr_mul_d(temp_var_491, (temp_var_490), 1e3, MPFR_RNDZ);

finavalu[offset_46] = mpfr_get_d(temp_var_491, MPFR_RNDZ);
  mpfr_set_d(MaxSR_ecc, 15, MPFR_RNDZ);
  mpfr_set_d(MinSR_ecc, 1, MPFR_RNDZ);
  
    mpfr_sub(temp_var_492, MaxSR_ecc, MinSR_ecc, MPFR_RNDZ);



    mpfr_div_d(temp_var_495, (temp_var_492), (1 + pow(mpfr_get_d(ec50SR_ecc, MPFR_RNDZ) / mpfr_get_d(initvalu_31_ecc, MPFR_RNDZ), 2.5)), MPFR_RNDZ);
    mpfr_sub(kCaSR_ecc, MaxSR_ecc, (temp_var_495), MPFR_RNDZ);
;
      mpfr_div(koSRCa_ecc, koCa_ecc, kCaSR_ecc, MPFR_RNDZ);
;
      mpfr_mul(kiSRCa_ecc, kiCa_ecc, kCaSR_ecc, MPFR_RNDZ);
;
  
    mpfr_si_sub(temp_var_496, 1, initvalu_14_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_497, (temp_var_496), initvalu_15_ecc, MPFR_RNDZ);
    mpfr_sub(RI_ecc, (temp_var_497), initvalu_16_ecc, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_498, kim_ecc, RI_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_499, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_500, (temp_var_499), initvalu_14_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_501, (temp_var_498), (temp_var_500), MPFR_RNDZ);

    mpfr_mul_d(temp_var_502, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_mul(temp_var_503, (temp_var_502), initvalu_14_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_504, kom_ecc, initvalu_15_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_505, (temp_var_503), (temp_var_504), MPFR_RNDZ);

    mpfr_sub(temp_var_506, (temp_var_501), (temp_var_505), MPFR_RNDZ);

finavalu[offset_14] = mpfr_get_d(temp_var_506, MPFR_RNDZ);
  
    mpfr_mul_d(temp_var_507, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_mul(temp_var_508, (temp_var_507), initvalu_14_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_509, kom_ecc, initvalu_15_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_510, (temp_var_508), (temp_var_509), MPFR_RNDZ);

    mpfr_mul(temp_var_511, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_512, (temp_var_511), initvalu_15_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_513, kim_ecc, initvalu_16_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_514, (temp_var_512), (temp_var_513), MPFR_RNDZ);

    mpfr_sub(temp_var_515, (temp_var_510), (temp_var_514), MPFR_RNDZ);

finavalu[offset_15] = mpfr_get_d(temp_var_515, MPFR_RNDZ);
  
    mpfr_mul(temp_var_516, kiSRCa_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_517, (temp_var_516), initvalu_15_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_518, kim_ecc, initvalu_16_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_519, (temp_var_517), (temp_var_518), MPFR_RNDZ);

    mpfr_mul(temp_var_520, kom_ecc, initvalu_16_ecc, MPFR_RNDZ);

    mpfr_mul_d(temp_var_521, koSRCa_ecc, pow(mpfr_get_d(initvalu_36_ecc, MPFR_RNDZ), 2), MPFR_RNDZ);

    mpfr_mul(temp_var_522, (temp_var_521), RI_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_523, (temp_var_520), (temp_var_522), MPFR_RNDZ);

    mpfr_sub(temp_var_524, (temp_var_519), (temp_var_523), MPFR_RNDZ);

finavalu[offset_16] = mpfr_get_d(temp_var_524, MPFR_RNDZ);
  
    mpfr_mul(temp_var_525, ks_ecc, initvalu_15_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_526, initvalu_31_ecc, initvalu_36_ecc, MPFR_RNDZ);
    mpfr_mul(J_SRCarel_ecc, (temp_var_525), (temp_var_526), MPFR_RNDZ);
;
  
    mpfr_mul_d(temp_var_527, Vmax_SRCaP_ecc, pow(mpfr_get_d(Q10SRCaP_ecc, MPFR_RNDZ), mpfr_get_d(Qpow_ecc, MPFR_RNDZ)), MPFR_RNDZ);




    mpfr_mul_d(temp_var_531, (temp_var_527), (pow(mpfr_get_d(initvalu_38_ecc, MPFR_RNDZ) / mpfr_get_d(Kmf_ecc, MPFR_RNDZ), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDZ)) - pow(mpfr_get_d(initvalu_31_ecc, MPFR_RNDZ) / mpfr_get_d(Kmr_ecc, MPFR_RNDZ), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDZ))), MPFR_RNDZ);



    mpfr_div_d(J_serca_ecc, (temp_var_531), ((1 + pow(mpfr_get_d(initvalu_38_ecc, MPFR_RNDZ) / mpfr_get_d(Kmf_ecc, MPFR_RNDZ), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDZ))) + pow(mpfr_get_d(initvalu_31_ecc, MPFR_RNDZ) / mpfr_get_d(Kmr_ecc, MPFR_RNDZ), mpfr_get_d(hillSRCaP_ecc, MPFR_RNDZ))), MPFR_RNDZ);
;
  
    mpfr_sub(temp_var_535, initvalu_31_ecc, initvalu_36_ecc, MPFR_RNDZ);
    mpfr_mul_d(J_SRleak_ecc, (temp_var_535), 5.348e-6, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_536, kon_na_ecc, initvalu_32_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_537, Bmax_Naj_ecc, initvalu_17_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_538, (temp_var_536), (temp_var_537), MPFR_RNDZ);

    mpfr_mul(temp_var_539, koff_na_ecc, initvalu_17_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_540, (temp_var_538), (temp_var_539), MPFR_RNDZ);

finavalu[offset_17] = mpfr_get_d(temp_var_540, MPFR_RNDZ);
  
    mpfr_mul(temp_var_541, kon_na_ecc, initvalu_33_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_542, Bmax_Nasl_ecc, initvalu_18_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_543, (temp_var_541), (temp_var_542), MPFR_RNDZ);

    mpfr_mul(temp_var_544, koff_na_ecc, initvalu_18_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_545, (temp_var_543), (temp_var_544), MPFR_RNDZ);

finavalu[offset_18] = mpfr_get_d(temp_var_545, MPFR_RNDZ);
  
    mpfr_mul(temp_var_546, kon_tncl_ecc, initvalu_38_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_547, Bmax_TnClow_ecc, initvalu_19_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_548, (temp_var_546), (temp_var_547), MPFR_RNDZ);

    mpfr_mul(temp_var_549, koff_tncl_ecc, initvalu_19_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_550, (temp_var_548), (temp_var_549), MPFR_RNDZ);

finavalu[offset_19] = mpfr_get_d(temp_var_550, MPFR_RNDZ);
  
    mpfr_mul(temp_var_551, kon_tnchca_ecc, initvalu_38_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_552, Bmax_TnChigh_ecc, initvalu_20_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_553, (temp_var_552), initvalu_21_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_554, (temp_var_551), (temp_var_553), MPFR_RNDZ);

    mpfr_mul(temp_var_555, koff_tnchca_ecc, initvalu_20_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_556, (temp_var_554), (temp_var_555), MPFR_RNDZ);

finavalu[offset_20] = mpfr_get_d(temp_var_556, MPFR_RNDZ);
  
    mpfr_mul(temp_var_557, kon_tnchmg_ecc, Mgi_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_558, Bmax_TnChigh_ecc, initvalu_20_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_559, (temp_var_558), initvalu_21_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_560, (temp_var_557), (temp_var_559), MPFR_RNDZ);

    mpfr_mul(temp_var_561, koff_tnchmg_ecc, initvalu_21_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_562, (temp_var_560), (temp_var_561), MPFR_RNDZ);

finavalu[offset_21] = mpfr_get_d(temp_var_562, MPFR_RNDZ);
  finavalu[offset_22] = 0;
  
    mpfr_mul(temp_var_563, kon_myoca_ecc, initvalu_38_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_564, Bmax_myosin_ecc, initvalu_23_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_565, (temp_var_564), initvalu_24_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_566, (temp_var_563), (temp_var_565), MPFR_RNDZ);

    mpfr_mul(temp_var_567, koff_myoca_ecc, initvalu_23_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_568, (temp_var_566), (temp_var_567), MPFR_RNDZ);

finavalu[offset_23] = mpfr_get_d(temp_var_568, MPFR_RNDZ);
  
    mpfr_mul(temp_var_569, kon_myomg_ecc, Mgi_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_570, Bmax_myosin_ecc, initvalu_23_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_571, (temp_var_570), initvalu_24_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_572, (temp_var_569), (temp_var_571), MPFR_RNDZ);

    mpfr_mul(temp_var_573, koff_myomg_ecc, initvalu_24_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_574, (temp_var_572), (temp_var_573), MPFR_RNDZ);

finavalu[offset_24] = mpfr_get_d(temp_var_574, MPFR_RNDZ);
  
    mpfr_mul(temp_var_575, kon_sr_ecc, initvalu_38_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_576, Bmax_SR_ecc, initvalu_25_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_577, (temp_var_575), (temp_var_576), MPFR_RNDZ);

    mpfr_mul(temp_var_578, koff_sr_ecc, initvalu_25_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_579, (temp_var_577), (temp_var_578), MPFR_RNDZ);

finavalu[offset_25] = mpfr_get_d(temp_var_579, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_580, finavalu[offset_19], MPFR_RNDZ);

    mpfr_set_d(temp_var_581, finavalu[offset_20], MPFR_RNDZ);

    mpfr_add(temp_var_582, temp_var_580, temp_var_581, MPFR_RNDZ);

    mpfr_set_d(temp_var_583, finavalu[offset_21], MPFR_RNDZ);

    mpfr_add(temp_var_584, (temp_var_582), temp_var_583, MPFR_RNDZ);

    mpfr_set_d(temp_var_585, finavalu[offset_22], MPFR_RNDZ);

    mpfr_add(temp_var_586, (temp_var_584), temp_var_585, MPFR_RNDZ);

    mpfr_set_d(temp_var_587, finavalu[offset_23], MPFR_RNDZ);

    mpfr_add(temp_var_588, (temp_var_586), temp_var_587, MPFR_RNDZ);

    mpfr_set_d(temp_var_589, finavalu[offset_24], MPFR_RNDZ);

    mpfr_add(temp_var_590, (temp_var_588), temp_var_589, MPFR_RNDZ);

    mpfr_set_d(temp_var_591, finavalu[offset_25], MPFR_RNDZ);
    mpfr_add(J_CaB_cytosol_ecc, (temp_var_590), temp_var_591, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_592, kon_sll_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_593, Bmax_SLlowj_ecc, initvalu_26_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_594, (temp_var_592), (temp_var_593), MPFR_RNDZ);

    mpfr_mul(temp_var_595, koff_sll_ecc, initvalu_26_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_596, (temp_var_594), (temp_var_595), MPFR_RNDZ);

finavalu[offset_26] = mpfr_get_d(temp_var_596, MPFR_RNDZ);
  
    mpfr_mul(temp_var_597, kon_sll_ecc, initvalu_37_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_598, Bmax_SLlowsl_ecc, initvalu_27_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_599, (temp_var_597), (temp_var_598), MPFR_RNDZ);

    mpfr_mul(temp_var_600, koff_sll_ecc, initvalu_27_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_601, (temp_var_599), (temp_var_600), MPFR_RNDZ);

finavalu[offset_27] = mpfr_get_d(temp_var_601, MPFR_RNDZ);
  
    mpfr_mul(temp_var_602, kon_slh_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_603, Bmax_SLhighj_ecc, initvalu_28_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_604, (temp_var_602), (temp_var_603), MPFR_RNDZ);

    mpfr_mul(temp_var_605, koff_slh_ecc, initvalu_28_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_606, (temp_var_604), (temp_var_605), MPFR_RNDZ);

finavalu[offset_28] = mpfr_get_d(temp_var_606, MPFR_RNDZ);
  
    mpfr_mul(temp_var_607, kon_slh_ecc, initvalu_37_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_608, Bmax_SLhighsl_ecc, initvalu_29_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_609, (temp_var_607), (temp_var_608), MPFR_RNDZ);

    mpfr_mul(temp_var_610, koff_slh_ecc, initvalu_29_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_611, (temp_var_609), (temp_var_610), MPFR_RNDZ);

finavalu[offset_29] = mpfr_get_d(temp_var_611, MPFR_RNDZ);
  
    mpfr_set_d(temp_var_612, finavalu[offset_26], MPFR_RNDZ);

    mpfr_set_d(temp_var_613, finavalu[offset_28], MPFR_RNDZ);
    mpfr_add(J_CaB_junction_ecc, temp_var_612, temp_var_613, MPFR_RNDZ);
;
  
    mpfr_set_d(temp_var_614, finavalu[offset_27], MPFR_RNDZ);

    mpfr_set_d(temp_var_615, finavalu[offset_29], MPFR_RNDZ);
    mpfr_add(J_CaB_sl_ecc, temp_var_614, temp_var_615, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_616, kon_csqn_ecc, initvalu_31_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_617, Bmax_Csqn_ecc, initvalu_30_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_618, (temp_var_616), (temp_var_617), MPFR_RNDZ);

    mpfr_mul(temp_var_619, koff_csqn_ecc, initvalu_30_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_620, (temp_var_618), (temp_var_619), MPFR_RNDZ);

finavalu[offset_30] = mpfr_get_d(temp_var_620, MPFR_RNDZ);
      mpfr_si_div(oneovervsr_ecc, 1, Vsr_ecc, MPFR_RNDZ);
;
  
    mpfr_mul(temp_var_621, J_serca_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_622, (temp_var_621), oneovervsr_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_623, J_SRleak_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_624, (temp_var_623), oneovervsr_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_625, (temp_var_624), J_SRCarel_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_626, (temp_var_622), (temp_var_625), MPFR_RNDZ);

    mpfr_set_d(temp_var_627, finavalu[offset_30], MPFR_RNDZ);

    mpfr_sub(temp_var_628, (temp_var_626), temp_var_627, MPFR_RNDZ);

finavalu[offset_31] = mpfr_get_d(temp_var_628, MPFR_RNDZ);
  
    mpfr_add(temp_var_629, I_Na_junc_ecc, I_nabk_junc_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_630, I_ncx_junc_ecc, 3, MPFR_RNDZ);

    mpfr_add(temp_var_631, (temp_var_629), (temp_var_630), MPFR_RNDZ);

    mpfr_mul_si(temp_var_632, I_nak_junc_ecc, 3, MPFR_RNDZ);

    mpfr_add(temp_var_633, (temp_var_631), (temp_var_632), MPFR_RNDZ);
    mpfr_add(I_Na_tot_junc_ecc, (temp_var_633), I_CaNa_junc_ecc, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_634, I_Na_sl_ecc, I_nabk_sl_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_635, I_ncx_sl_ecc, 3, MPFR_RNDZ);

    mpfr_add(temp_var_636, (temp_var_634), (temp_var_635), MPFR_RNDZ);

    mpfr_mul_si(temp_var_637, I_nak_sl_ecc, 3, MPFR_RNDZ);

    mpfr_add(temp_var_638, (temp_var_636), (temp_var_637), MPFR_RNDZ);
    mpfr_add(I_Na_tot_sl_ecc, (temp_var_638), I_CaNa_sl_ecc, MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_640, (I_Na_tot_junc_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_639, temp_var_640, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_641, Vjunc_ecc, Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_642, (temp_var_639), (temp_var_641), MPFR_RNDZ);

    mpfr_div(temp_var_643, J_na_juncsl_ecc, Vjunc_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_644, initvalu_33_ecc, initvalu_32_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_645, (temp_var_643), (temp_var_644), MPFR_RNDZ);

    mpfr_add(temp_var_646, (temp_var_642), (temp_var_645), MPFR_RNDZ);

    mpfr_set_d(temp_var_647, finavalu[offset_17], MPFR_RNDZ);

    mpfr_sub(temp_var_648, (temp_var_646), temp_var_647, MPFR_RNDZ);

finavalu[offset_32] = mpfr_get_d(temp_var_648, MPFR_RNDZ);
      mpfr_si_div(oneovervsl_ecc, 1, Vsl_ecc, MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_650, (I_Na_tot_sl_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_649, temp_var_650, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_651, (temp_var_649), oneovervsl_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_652, (temp_var_651), Frdy_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_653, J_na_juncsl_ecc, oneovervsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_654, initvalu_32_ecc, initvalu_33_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_655, (temp_var_653), (temp_var_654), MPFR_RNDZ);

    mpfr_add(temp_var_656, (temp_var_652), (temp_var_655), MPFR_RNDZ);

    mpfr_mul(temp_var_657, J_na_slmyo_ecc, oneovervsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_658, initvalu_34_ecc, initvalu_33_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_659, (temp_var_657), (temp_var_658), MPFR_RNDZ);

    mpfr_add(temp_var_660, (temp_var_656), (temp_var_659), MPFR_RNDZ);

    mpfr_set_d(temp_var_661, finavalu[offset_18], MPFR_RNDZ);

    mpfr_sub(temp_var_662, (temp_var_660), temp_var_661, MPFR_RNDZ);

finavalu[offset_33] = mpfr_get_d(temp_var_662, MPFR_RNDZ);
  
    mpfr_div(temp_var_663, J_na_slmyo_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_664, initvalu_33_ecc, initvalu_34_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_665, (temp_var_663), (temp_var_664), MPFR_RNDZ);

finavalu[offset_34] = mpfr_get_d(temp_var_665, MPFR_RNDZ);
  
    mpfr_add(temp_var_666, I_to_ecc, I_kr_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_667, (temp_var_666), I_ks_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_668, (temp_var_667), I_ki_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_669, I_nak_ecc, 2, MPFR_RNDZ);

    mpfr_sub(temp_var_670, (temp_var_668), (temp_var_669), MPFR_RNDZ);

    mpfr_add(temp_var_671, (temp_var_670), I_CaK_ecc, MPFR_RNDZ);
    mpfr_add(I_K_tot_ecc, (temp_var_671), I_kp_ecc, MPFR_RNDZ);
;
  finavalu[offset_35] = 0;
  
    mpfr_add(temp_var_672, I_Ca_junc_ecc, I_cabk_junc_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_673, (temp_var_672), I_pca_junc_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_674, I_ncx_junc_ecc, 2, MPFR_RNDZ);
    mpfr_sub(I_Ca_tot_junc_ecc, (temp_var_673), (temp_var_674), MPFR_RNDZ);
;
  
    mpfr_add(temp_var_675, I_Ca_sl_ecc, I_cabk_sl_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_676, (temp_var_675), I_pca_sl_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_677, I_ncx_sl_ecc, 2, MPFR_RNDZ);
    mpfr_sub(I_Ca_tot_sl_ecc, (temp_var_676), (temp_var_677), MPFR_RNDZ);
;
  
    
mpfr_neg (temp_var_679, (I_Ca_tot_junc_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_678, temp_var_679, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_680, Vjunc_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_681, (temp_var_680), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_682, (temp_var_678), (temp_var_681), MPFR_RNDZ);

    mpfr_div(temp_var_683, J_ca_juncsl_ecc, Vjunc_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_684, initvalu_37_ecc, initvalu_36_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_685, (temp_var_683), (temp_var_684), MPFR_RNDZ);

    mpfr_add(temp_var_686, (temp_var_682), (temp_var_685), MPFR_RNDZ);

    mpfr_sub(temp_var_687, (temp_var_686), J_CaB_junction_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_688, J_SRCarel_ecc, Vsr_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_689, (temp_var_688), Vjunc_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_690, (temp_var_687), (temp_var_689), MPFR_RNDZ);

    mpfr_mul(temp_var_691, J_SRleak_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_692, (temp_var_691), Vjunc_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_693, (temp_var_690), (temp_var_692), MPFR_RNDZ);

finavalu[offset_36] = mpfr_get_d(temp_var_693, MPFR_RNDZ);
  
    
mpfr_neg (temp_var_695, (I_Ca_tot_sl_ecc), MPFR_RNDZ);
mpfr_mul(temp_var_694, temp_var_695, Cmem_ecc, MPFR_RNDZ);

    mpfr_mul_si(temp_var_696, Vsl_ecc, 2, MPFR_RNDZ);

    mpfr_mul(temp_var_697, (temp_var_696), Frdy_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_698, (temp_var_694), (temp_var_697), MPFR_RNDZ);

    mpfr_div(temp_var_699, J_ca_juncsl_ecc, Vsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_700, initvalu_36_ecc, initvalu_37_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_701, (temp_var_699), (temp_var_700), MPFR_RNDZ);

    mpfr_add(temp_var_702, (temp_var_698), (temp_var_701), MPFR_RNDZ);

    mpfr_div(temp_var_703, J_ca_slmyo_ecc, Vsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_704, initvalu_38_ecc, initvalu_37_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_705, (temp_var_703), (temp_var_704), MPFR_RNDZ);

    mpfr_add(temp_var_706, (temp_var_702), (temp_var_705), MPFR_RNDZ);

    mpfr_sub(temp_var_707, (temp_var_706), J_CaB_sl_ecc, MPFR_RNDZ);

finavalu[offset_37] = mpfr_get_d(temp_var_707, MPFR_RNDZ);
  
    
mpfr_neg (temp_var_709, (J_serca_ecc), MPFR_RNDZ);
mpfr_sub(temp_var_708, temp_var_709, J_CaB_cytosol_ecc, MPFR_RNDZ);

    mpfr_div(temp_var_710, J_ca_slmyo_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_711, initvalu_37_ecc, initvalu_38_ecc, MPFR_RNDZ);

    mpfr_mul(temp_var_712, (temp_var_710), (temp_var_711), MPFR_RNDZ);

    mpfr_add(temp_var_713, (temp_var_708), (temp_var_712), MPFR_RNDZ);

finavalu[offset_38] = mpfr_get_d(temp_var_713, MPFR_RNDZ);
  
    mpfr_div(temp_var_714, J_ca_juncsl_ecc, Vsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_715, initvalu_36_ecc, initvalu_37_ecc, MPFR_RNDZ);
    mpfr_mul(junc_sl_ecc, (temp_var_714), (temp_var_715), MPFR_RNDZ);
;
  
    mpfr_div(temp_var_716, J_ca_juncsl_ecc, Vjunc_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_717, initvalu_37_ecc, initvalu_36_ecc, MPFR_RNDZ);
    mpfr_mul(sl_junc_ecc, (temp_var_716), (temp_var_717), MPFR_RNDZ);
;
  
    mpfr_div(temp_var_718, J_ca_slmyo_ecc, Vsl_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_719, initvalu_38_ecc, initvalu_37_ecc, MPFR_RNDZ);
    mpfr_mul(sl_myo_ecc, (temp_var_718), (temp_var_719), MPFR_RNDZ);
;
  
    mpfr_div(temp_var_720, J_ca_slmyo_ecc, Vmyo_ecc, MPFR_RNDZ);

    mpfr_sub(temp_var_721, initvalu_37_ecc, initvalu_38_ecc, MPFR_RNDZ);
    mpfr_mul(myo_sl_ecc, (temp_var_720), (temp_var_721), MPFR_RNDZ);
;
  state = 1;
  switch (state)
  {
    case 0:
      mpfr_set_d(I_app_ecc, 0, MPFR_RNDZ);
      break;

    case 1:
      if (fmod(timeinst, mpfr_get_d(parameter_1_ecc, MPFR_RNDZ)) <= 5)
    {
      mpfr_set_d(I_app_ecc, 9.5, MPFR_RNDZ);
    }
    else
    {
      mpfr_set_d(I_app_ecc, 0.0, MPFR_RNDZ);
    }

      break;

    case 2:
      mpfr_set_d(V_hold_ecc, -55, MPFR_RNDZ);
      mpfr_set_d(V_test_ecc, 0, MPFR_RNDZ);
      
      if ((timeinst > 0.5) & (timeinst < 200.5))
    {
      mpfr_set(V_clamp_ecc, V_test_ecc, MPFR_RNDZ);
    }
    else
    {
      mpfr_set(V_clamp_ecc, V_hold_ecc, MPFR_RNDZ);
    }

      mpfr_set_d(R_clamp_ecc, 0.04, MPFR_RNDZ);
      
        mpfr_sub(temp_var_722, V_clamp_ecc, initvalu_39_ecc, MPFR_RNDZ);
        mpfr_div(I_app_ecc, (temp_var_722), R_clamp_ecc, MPFR_RNDZ);
;
      break;

  }

      mpfr_add(I_Na_tot_ecc, I_Na_tot_junc_ecc, I_Na_tot_sl_ecc, MPFR_RNDZ);
;
      mpfr_add(I_Cl_tot_ecc, I_ClCa_ecc, I_Clbk_ecc, MPFR_RNDZ);
;
      mpfr_add(I_Ca_tot_ecc, I_Ca_tot_junc_ecc, I_Ca_tot_sl_ecc, MPFR_RNDZ);
;
  
    mpfr_add(temp_var_723, I_Na_tot_ecc, I_Cl_tot_ecc, MPFR_RNDZ);

    mpfr_add(temp_var_724, (temp_var_723), I_Ca_tot_ecc, MPFR_RNDZ);
    mpfr_add(I_tot_ecc, (temp_var_724), I_K_tot_ecc, MPFR_RNDZ);
;
	
	//finavalu[offset_39] = -(I_tot-I_app); = I_app-I_tot why ?
	mpfr_sub(temp_var_725, 	I_app_ecc, I_tot_ecc, MPFR_RNDZ );
  finavalu[offset_39] = mpfr_get_d(temp_var_725,MPFR_RNDZ);
  finavalu[offset_41] = 0;
  finavalu[offset_42] = 0;
}

//end of conversion, hopefully it will work :)
