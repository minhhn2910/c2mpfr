//~ #include "../../../../logging/cov_log.h"
//~ #include "../../../../logging/cov_checker.h"
//~ #include "../../../../logging/cov_rand.h"
//~ #include "../../../../logging/cov_serializer.h"

//~ #include <stdio.h>
//~ #include <stdlib.h>
//~ #include <inttypes.h>
//~ #include <stdarg.h>
//~ #include <math.h>
#include "gsl_blas.h"




void
cblas_dgemm (const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
             const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
             const int K, const double alpha, const double *A, const int lda,
             const double *B, const int ldb, const double beta, double *C,
             const int ldc)
{

{
  int i, j, k;
  int n1, n2;
  int ldf, ldg;
  int TransF, TransG;
  //~ const double *F, *G;
  double F[], G[];
  
  double A_temp;
  double B_temp;
  double C_temp;
  double alpha_temp = alpha;
  double beta_temp = beta;
  double F_temp;
  double G_temp;
  

  //~ do { int pos = 0 ; { enum CBLAS_TRANSPOSE __transF=CblasNoTrans,__transG=CblasNoTrans; if((Order)==CblasRowMajor) { __transF = ((TransA)!=CblasConjTrans) ? (TransA) : CblasTrans; __transG = ((TransB)!=CblasConjTrans) ? (TransB) : CblasTrans; } else { __transF = ((TransB)!=CblasConjTrans) ? (TransB) : CblasTrans; __transG = ((TransA)!=CblasConjTrans) ? (TransA) : CblasTrans; } if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) pos = 1;; if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&&((TransA)!=CblasConjTrans)) pos = 2;; if(((TransB)!=CblasNoTrans)&&((TransB)!=CblasTrans)&&((TransB)!=CblasConjTrans)) pos = 3;; if((M)<0) pos = 4;; if((N)<0) pos = 5;; if((K)<0) pos = 6;; if((Order)==CblasRowMajor) { if(__transF==CblasNoTrans) { if((lda)<((1) > ((K)) ? (1) : ((K)))) { (pos) = 9; } } else { if((lda)<((1) > ((M)) ? (1) : ((M)))) { (pos) = 9; } } if(__transG==CblasNoTrans) { if((ldb)<((1) > ((N)) ? (1) : ((N)))) { (pos) = 11; } } else { if((ldb)<((1) > ((K)) ? (1) : ((K)))) { (pos) = 11; } } if((ldc)<((1) > ((N)) ? (1) : ((N)))) { (pos) = 14; } } else if((Order)==CblasColMajor) { if(__transF==CblasNoTrans) { if((ldb)<((1) > ((K)) ? (1) : ((K)))) { (pos) = 11; } } else { if((ldb)<((1) > ((N)) ? (1) : ((N)))) { (pos) = 11; } } if(__transG==CblasNoTrans) { if((lda)<((1) > ((M)) ? (1) : ((M)))) { (pos) = 9; } } else { if((lda)<((1) > ((K)) ? (1) : ((K)))) { (pos) = 9; } } if((ldc)<((1) > ((M)) ? (1) : ((M)))) { (pos) = 14; } } } ; if (pos) ;/*cblas_xerbla(pos,"source_gemm_r.h","");*/ } while (0);

  if (alpha_temp == 0.0 && beta_temp == 1.0)
    return;

  if (Order == CblasRowMajor) {
    n1 = M;
    n2 = N;
    F = A;
    ldf = lda;
    TransF = (TransA == CblasConjTrans) ? CblasTrans : TransA;
    G = B;
    ldg = ldb;
    TransG = (TransB == CblasConjTrans) ? CblasTrans : TransB;
  } else {
    n1 = N;
    n2 = M;
    F = B;
    ldf = ldb;
    TransF = (TransB == CblasConjTrans) ? CblasTrans : TransB;
    G = A;
    ldg = lda;
    TransG = (TransA == CblasConjTrans) ? CblasTrans : TransA;
  }


  if (beta_temp == 0.0) {
    for (i = 0; i < n1; i++) {
      for (j = 0; j < n2; j++) {
		  
        //~ C[ldc * i + j] = 0.0;
        C_temp = 0.0;
        C[ldc * i + j] = C_temp;
      }
    }
  } else if (beta_temp != 1.0) {
    for (i = 0; i < n1; i++) {
      for (j = 0; j < n2; j++) {
        //~ C[ldc * i + j] *= beta_temp;
        C_temp  = C[ldc * i + j];
        C_temp = C_temp * beta_temp;
        
        C[ldc * i + j]  = C_temp;
      }
    }
  }

  if (alpha_temp == 0.0)
    return;

  if (TransF == CblasNoTrans && TransG == CblasNoTrans) {



    for (k = 0; k < K; k++) {
      for (i = 0; i < n1; i++) {
		   //~ const double temp = alpha * F[ldf * i + k];
        double temp;
        F_temp = F[ldf * i + k];
        temp = alpha_temp * F_temp;
        
        if (temp != 0.0) {
          for (j = 0; j < n2; j++) {
            //~ C[ldc * i + j] += temp * G[ldg * k + j];
            C_temp = C[ldc * i + j];
            G_temp = G[ldg * k + j];
            C_temp = C_temp + temp*G_temp;
            C[ldc * i + j] = C_temp;
          }
        }
      }
    }

  } else if (TransF == CblasNoTrans && TransG == CblasTrans) {



    for (i = 0; i < n1; i++) {
      for (j = 0; j < n2; j++) {
        double temp1 = 0.0;
        for (k = 0; k < K; k++) {
			F_temp = F[ldf * i + k];
			G_temp = G[ldg * j + k];
			temp1 = temp1 + F_temp * G_temp;
          //~ temp1 += F[ldf * i + k] * G[ldg * j + k];
        }
        C_temp = C[ldc * i + j];
        C_temp = C_temp + alpha_temp * temp1;
        C[ldc * i + j] = C_temp;
        //~ C[ldc * i + j] += alpha_temp * temp1;
      }
    }

  } else if (TransF == CblasTrans && TransG == CblasNoTrans) {

    for (k = 0; k < K; k++) {
      for (i = 0; i < n1; i++) {
		  
        //~ const double temp2 = alpha_temp * F[ldf * k + i];
        double temp2;
        F_temp = F[ldf * k + i];
        temp2 = alpha_temp*F_temp;
        
        if (temp2 != 0.0) {
          for (j = 0; j < n2; j++) {
			G_temp = G[ldg * k + j];
			C_temp = C[ldc * i + j];
			C_temp = C_temp + temp2 *G_temp;
			C[ldc * i + j] = C_temp;
            //~ C[ldc * i + j] += temp2 * G[ldg * k + j];
          }
        }
      }
    }

  } else if (TransF == CblasTrans && TransG == CblasTrans) {

    for (i = 0; i < n1; i++) {
      for (j = 0; j < n2; j++) {
        double temp3 = 0.0;
        for (k = 0; k < K; k++) {
			F_temp = F[ldf * k + i];
			G_temp =  G[ldg * j + k];
			temp3 = temp3 + F_temp*G_temp;
          //~ temp3 += F[ldf * k + i] * G[ldg * j + k];
        }
        C_temp =  C[ldc * i + j];
        C_temp = C_temp + alpha_temp * temp3;
        C[ldc * i + j] = C_temp;
        //~ C[ldc * i + j] += alpha_temp * temp3;
      }
    }

  } else {
	  ;
    //~ cblas_xerbla(0, "source_gemm_r.h", "unrecognized operation");;
  }
}

}


/*
_gsl_matrix_view
gsl_matrix_view_array ( double * array,
                                   const size_t n1, const size_t n2)
{
	//~ printf("new view array\n");
  _gsl_matrix_view view = {{0, 0, 0, 0, 0, 0}};

  if (n1 == 0)
    {
      do { gsl_error ("matrix dimension n1 must be positive integer",
 "view_source.c"
//~ # 28 "view_source.c" 3
      ,
 29
//~ # 28 "view_source.c" 3
      , GSL_EINVAL) ; return view ; } while (0)
                                      ;
    }
  else if (n2 == 0)
    {
      do { gsl_error ("matrix dimension n2 must be positive integer",
 "view_source.c"
//~ # 33 "view_source.c" 3
      ,
 34
//~ # 33 "view_source.c" 3
      , GSL_EINVAL) ; return view ; } while (0)
                                      ;
    }

  {
    gsl_matrix m = {0, 0, 0, 0, 0, 0};

    m.data = (double *)array;
    m.size1 = n1;
    m.size2 = n2;
    m.tda = n2;
    m.block = 0;
    m.owner = 0;

    view.matrix = m;
    return view;
  }
}
*/

#define INPUT_SIZE 1
//~ #define ITERS 200000
#define ITERS 1


//~ extern uint64_t current_time_ns(void);


long double cov_deserialize(char* buf, int length) {
	int i;
	unsigned int u;
	union {
		unsigned char bytes[10];
		long double val;
	} bfconvert;

	for (i = 0; i < length; i++) {
		sscanf(buf, "%02X", &u);
		bfconvert.bytes[i] = u;
		buf += 2;
	}

	return bfconvert.val;
}



int main (void)
{
  //~ uint64_t start, end;
  //~ long int diff = 0;
  //~ long double results[400*INPUT_SIZE];
  //~ double epsilon = -8.0;
  //~ long double threshold;

  double a[600]; 
  double b[600];
  double c[400];

	double a_temp;
	double b_temp;
	double c_temp;

  long a_max = 0.0;
  long b_max = 0.0;
  long c_max = 0.0;

  int i, j;
/*
  FILE* infile = fopen("final_inputs", "r");
  if (!infile)
  {
    printf("Could not open final_inputs\n");
    exit(0);
  }
*/

  for (i = 0; i < INPUT_SIZE; i++)
  {
    //
    // read inputs from final_inputs
    //
    if (!feof(infile))
    {
      for (j = 0; j < 1200; j++) {
        char* s = malloc(100);
        fscanf(infile, "%s", s);
        if (j < 600)
        {
          //~ a[j] = (double) cov_deserialize(s, 10);
          a_temp = (double) cov_deserialize(s, 10);
          a[j] = a_temp;
          
          if (a[j] > a_max) 
          {
            a_max = a[j];
          }
        }
        else 
        {
          //~ b[j-600] = (double) cov_deserialize(s, 10);
          b_temp = (double) cov_deserialize(s, 10);
          b[j-600] = b_temp;
          
          if (b[j-600] > b_max)
          {
            b_max = b[j-600];
          }
        }
      }
    }

    for (i = 0; i < 400; i++)
    {
      //~ c[i] = 0.00;
      c_temp = 0.0;
      c[i] = c_temp;
    }

    //
    // run program; record timing as score
    //
    //~ start = current_time_ns();
    for (j = 0; j < ITERS; j++) {
      gsl_matrix_view A = gsl_matrix_view_array(a, 20, 30);
      gsl_matrix_view B = gsl_matrix_view_array(b, 30, 20);
      gsl_matrix_view C = gsl_matrix_view_array(c, 20, 20);

      /* Compute C = A B */

      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans,
          1.0, &A.matrix, &B.matrix,
          0.0, &C.matrix);
    }
    
    //~ end = current_time_ns();
    //~ diff += (long int)(end-start);

    for (i = 0; i < 400; i++)
    {
      if (c[i] > c_max)
      {
        c_max = c[i];
      }
    }

  }
    printf("%.12lf\n",c_max);
	//~ printf("%.12lf\n",a_max*b_max);
  //~ threshold = (long double) pow(10.0, epsilon)*a_max*b_max;

  //cov_spec_log("spec.cov", threshold, 1, (long double) c_max);
  //~ cov_log("result", "log.cov", 1, (long double) c_max);
  //~ cov_check("log.cov", "spec.cov", 1);
//~ 
  //~ fclose(infile);

  //
  // print score (diff) to score.cov
  //
  //~ FILE* file;
  //~ file = fopen("score.cov", "w");
  //~ fprintf(file, "%ld\n", diff);
  //~ fclose(file);

  return 0;  
}
