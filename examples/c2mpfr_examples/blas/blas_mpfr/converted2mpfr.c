//~ #include "../../../../logging/cov_log.h"
//~ #include "../../../../logging/cov_checker.h"
//~ #include "../../../../logging/cov_rand.h"
//~ #include "../../../../logging/cov_serializer.h"
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdarg.h>
#include <math.h>
#include "gsl_blas.h"
#include <mpfr.h>
#define LEN 15 
 int config_vals[LEN];
    mpfr_t A_temp_cblas_dgemm;
    mpfr_t B_temp_cblas_dgemm;
    mpfr_t C_temp_cblas_dgemm;
    mpfr_t alpha_temp_cblas_dgemm;
    mpfr_t beta_temp_cblas_dgemm;
    mpfr_t F_temp_cblas_dgemm;
    mpfr_t G_temp_cblas_dgemm;
          mpfr_t temp_cblas_dgemm;
                            mpfr_t temp_var_1;
          mpfr_t temp1_cblas_dgemm;
                        mpfr_t temp_var_2;
                    mpfr_t temp_var_3;
          mpfr_t temp2_cblas_dgemm;
                            mpfr_t temp_var_4;
          mpfr_t temp3_cblas_dgemm;
                        mpfr_t temp_var_5;
                    mpfr_t temp_var_6;
  mpfr_t a_temp_main;
  mpfr_t b_temp_main;
  mpfr_t c_temp_main;
int init_mpfr() { 
    mpfr_init2(A_temp_cblas_dgemm, config_vals[1]);
    mpfr_init2(B_temp_cblas_dgemm, config_vals[2]);
    mpfr_init2(C_temp_cblas_dgemm, config_vals[3]);
    mpfr_init2(alpha_temp_cblas_dgemm, config_vals[4]);
    mpfr_init2(beta_temp_cblas_dgemm, config_vals[5]);
    mpfr_init2(F_temp_cblas_dgemm, config_vals[6]);
    mpfr_init2(G_temp_cblas_dgemm, config_vals[7]);
          mpfr_init2(temp_cblas_dgemm, config_vals[8]);
                            mpfr_init2 (temp_var_1, config_vals[0]);
          mpfr_init2(temp1_cblas_dgemm, config_vals[9]);
                        mpfr_init2 (temp_var_2, config_vals[0]);
                    mpfr_init2 (temp_var_3, config_vals[0]);
          mpfr_init2(temp2_cblas_dgemm, config_vals[10]);
                            mpfr_init2 (temp_var_4, config_vals[0]);
          mpfr_init2(temp3_cblas_dgemm, config_vals[11]);
                        mpfr_init2 (temp_var_5, config_vals[0]);
                    mpfr_init2 (temp_var_6, config_vals[0]);
  mpfr_init2(a_temp_main, config_vals[12]);
  mpfr_init2(b_temp_main, config_vals[13]);
  mpfr_init2(c_temp_main, config_vals[14]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB, const int M, const int N, const int K, const double alpha, const double *A, const int lda, const double *B, const int ldb, const double beta, double *C, const int ldc)
{
  {
    int i;
    int j;
    int k;
    int n1;
    int n2;
    int ldf;
    int ldg;
    int TransF;
    int TransG;
    const double *F;
    const double *G;
    mpfr_set_d(alpha_temp_cblas_dgemm, alpha, MPFR_RNDN);
    mpfr_set_d(beta_temp_cblas_dgemm, beta, MPFR_RNDN);
    //~ if ((alpha_temp_cblas_dgemm == 0.0) && (beta_temp_cblas_dgemm == 1.0))
    if ((alpha== 0.0) && (beta == 1.0))
      return ;

    if (Order == CblasRowMajor)
    {
      n1 = M;
      n2 = N;
      F = A;
      ldf = lda;
      TransF = TransA == CblasConjTrans ? CblasTrans : TransA;
      G = B;
      ldg = ldb;
      TransG = TransB == CblasConjTrans ? CblasTrans : TransB;
    }
    else
    {
      n1 = N;
      n2 = M;
      F = B;
      ldf = ldb;
      TransF = TransB == CblasConjTrans ? CblasTrans : TransB;
      G = A;
      ldg = lda;
      TransG = TransA == CblasConjTrans ? CblasTrans : TransA;
    }

//~ if (beta_temp == 0.0)
    if (mpfr_cmp_d(beta_temp_cblas_dgemm,0.0)== 0.0)
    {
      for (i = 0; i < n1; i++){
      {
        for (j = 0; j < n2; j++){
        {
          mpfr_set_d(C_temp_cblas_dgemm, 0.0, MPFR_RNDN);
          
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
        }

                }

      }

            }

    }
    else
      if (beta != 1.0)
    {
      for (i = 0; i < n1; i++){
      {
        for (j = 0; j < n2; j++){
        {
          mpfr_set_d(C_temp_cblas_dgemm, C[(ldc * i) + j], MPFR_RNDN);
                              mpfr_mul(C_temp_cblas_dgemm, C_temp_cblas_dgemm, beta_temp_cblas_dgemm, MPFR_RNDN);
          
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
        }

                }

      }

            }

    }


    if (alpha == 0.0)
      return ;

    if ((TransF == CblasNoTrans) && (TransG == CblasNoTrans))
    {
      for (k = 0; k < K; k++){
      {
        for (i = 0; i < n1; i++){
        {
          mpfr_set_d(F_temp_cblas_dgemm, F[(ldf * i) + k], MPFR_RNDN);
                              mpfr_mul(temp_cblas_dgemm, alpha_temp_cblas_dgemm, F_temp_cblas_dgemm, MPFR_RNDN);
          if (mpfr_cmp_d(temp_cblas_dgemm,0.0) != 0.0)
          {
            for (j = 0; j < n2; j++){
            {
              mpfr_set_d(C_temp_cblas_dgemm, C[(ldc * i) + j], MPFR_RNDN);
              mpfr_set_d(G_temp_cblas_dgemm, G[(ldg * k) + j], MPFR_RNDN);
              
                            mpfr_mul(temp_var_1, temp_cblas_dgemm, G_temp_cblas_dgemm, MPFR_RNDN);
                            mpfr_add(C_temp_cblas_dgemm, C_temp_cblas_dgemm, (temp_var_1), MPFR_RNDN);
              
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
            }

                        }

          }

        }

                }

      }

            }

    }
    else
      if ((TransF == CblasNoTrans) && (TransG == CblasTrans))
    {
      for (i = 0; i < n1; i++){
      {
        for (j = 0; j < n2; j++){
        {
          mpfr_set_d(temp1_cblas_dgemm, 0.0, MPFR_RNDN);
          for (k = 0; k < K; k++){
          {
            mpfr_set_d(F_temp_cblas_dgemm, F[(ldf * i) + k], MPFR_RNDN);
            mpfr_set_d(G_temp_cblas_dgemm, G[(ldg * j) + k], MPFR_RNDN);
            
                        mpfr_mul(temp_var_2, F_temp_cblas_dgemm, G_temp_cblas_dgemm, MPFR_RNDN);
                        mpfr_add(temp1_cblas_dgemm, temp1_cblas_dgemm, (temp_var_2), MPFR_RNDN);
          }

                    }

          mpfr_set_d(C_temp_cblas_dgemm, C[(ldc * i) + j], MPFR_RNDN);
          
                    mpfr_mul(temp_var_3, alpha_temp_cblas_dgemm, temp1_cblas_dgemm, MPFR_RNDN);
                    mpfr_add(C_temp_cblas_dgemm, C_temp_cblas_dgemm, (temp_var_3), MPFR_RNDN);
          
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
        }

                }

      }

            }

    }
    else
      if ((TransF == CblasTrans) && (TransG == CblasNoTrans))
    {
      for (k = 0; k < K; k++){
      {
        for (i = 0; i < n1; i++){
        {
          mpfr_set_d(F_temp_cblas_dgemm, F[(ldf * k) + i], MPFR_RNDN);
                              mpfr_mul(temp2_cblas_dgemm, alpha_temp_cblas_dgemm, F_temp_cblas_dgemm, MPFR_RNDN);
          if (mpfr_cmp_d(temp2_cblas_dgemm,0.0) != 0.0)
          {
            for (j = 0; j < n2; j++){
            {
              mpfr_set_d(G_temp_cblas_dgemm, G[(ldg * k) + j], MPFR_RNDN);
              mpfr_set_d(C_temp_cblas_dgemm, C[(ldc * i) + j], MPFR_RNDN);
              
                            mpfr_mul(temp_var_4, temp2_cblas_dgemm, G_temp_cblas_dgemm, MPFR_RNDN);
                            mpfr_add(C_temp_cblas_dgemm, C_temp_cblas_dgemm, (temp_var_4), MPFR_RNDN);
              
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
            }

                        }

          }

        }

                }

      }

            }

    }
    else
      if ((TransF == CblasTrans) && (TransG == CblasTrans))
    {
      for (i = 0; i < n1; i++){
      {
        for (j = 0; j < n2; j++){
        {
          mpfr_set_d(temp3_cblas_dgemm, 0.0, MPFR_RNDN);
          for (k = 0; k < K; k++){
          {
            mpfr_set_d(F_temp_cblas_dgemm, F[(ldf * k) + i], MPFR_RNDN);
            mpfr_set_d(G_temp_cblas_dgemm, G[(ldg * j) + k], MPFR_RNDN);
            
                        mpfr_mul(temp_var_5, F_temp_cblas_dgemm, G_temp_cblas_dgemm, MPFR_RNDN);
                        mpfr_add(temp3_cblas_dgemm, temp3_cblas_dgemm, (temp_var_5), MPFR_RNDN);
          }

                    }

          mpfr_set_d(C_temp_cblas_dgemm, C[(ldc * i) + j], MPFR_RNDN);
          
                    mpfr_mul(temp_var_6, alpha_temp_cblas_dgemm, temp3_cblas_dgemm, MPFR_RNDN);
                    mpfr_add(C_temp_cblas_dgemm, C_temp_cblas_dgemm, (temp_var_6), MPFR_RNDN);
          
C[(ldc * i) + j] = mpfr_get_d(C_temp_cblas_dgemm, MPFR_RNDN);
        }

                }

      }

            }

    }
    else
    {
      ;
    }




  }
}


long double cov_deserialize(char *buf, int length)
{
  int i;
  unsigned int u;
  	union {
		unsigned char bytes[10];
		long double val;
	} bfconvert;

  for (i = 0; i < length; i++){
  {
    sscanf(buf, "%02X", &u);
    bfconvert.bytes[i] = u;
    buf += 2;
  }

    }

  return bfconvert.val;
}

int main(void)
{
 init_readconfig();
  double a[600];
  double b[600];
  double c[400];
  double a_max = 0.0;
  double b_max = 0.0;
  double c_max = 0.0;
  int i;
  int j;
    FILE* infile = fopen("final_inputs", "r");
  if (!infile)
  {
    printf("Could not open final_inputs\n");
    exit(0);
  }
  for (i = 0; i < 1; i++){
  {
    if (!feof(infile))
    {
      for (j = 0; j < 1200; j++){
      {
        char *s = malloc(100);
        fscanf(infile, "%s", s);
        if (j < 600)
        {
          mpfr_set_d(a_temp_main, (float) cov_deserialize(s, 10), MPFR_RNDN);
          
a[j] = mpfr_get_d(a_temp_main, MPFR_RNDN);
          if (a[j] > a_max)
          {
            a_max = a[j];
          }

        }
        else
        {
          mpfr_set_d(b_temp_main, (float) cov_deserialize(s, 10), MPFR_RNDN);
          
b[j - 600] = mpfr_get_d(b_temp_main, MPFR_RNDN);
          if (b[j - 600] > b_max)
          {
            b_max = b[j - 600];
          }

        }

      }

            }

    }

    for (i = 0; i < 400; i++){
    {
      mpfr_set_d(c_temp_main, 0.0, MPFR_RNDN);
      
c[i] = mpfr_get_d(c_temp_main, MPFR_RNDN);
    }

        }

    for (j = 0; j < 1; j++){
    {
      gsl_matrix_view A = gsl_matrix_view_array(a, 20, 30);
      gsl_matrix_view B = gsl_matrix_view_array(b, 30, 20);
      gsl_matrix_view C = gsl_matrix_view_array(c, 20, 20);
      gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &A.matrix, &B.matrix, 0.0, &C.matrix);
    }

        }

    for (i = 0; i < 400; i++){
    {
      if (c[i] > c_max)
      {
        c_max = c[i];
      }

    }

        }

  }

    }

  printf("%.12lf\n", c_max);
  return 0;
}

//end of conversion, hopefully it will work :)
