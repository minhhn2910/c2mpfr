#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpfr.h>
#define LEN 8 
 int config_vals[LEN];
  mpfr_t t1_fun;
  mpfr_t d1_fun;
        mpfr_t temp_var_1;
        mpfr_t temp_var_2;
  mpfr_t h_main;
  mpfr_t t1_main;
  mpfr_t t2_main;
  mpfr_t dppi_main;
  mpfr_t s1_main;
        mpfr_t temp_var_3;
        mpfr_t temp_var_4;
        mpfr_t temp_var_5;
        mpfr_t temp_var_6;
int init_mpfr() { 
  mpfr_init2(t1_fun, config_vals[1]);
  mpfr_init2(d1_fun, config_vals[2]);
        mpfr_init2 (temp_var_1, config_vals[0]);
        mpfr_init2 (temp_var_2, config_vals[0]);
  mpfr_init2(h_main, config_vals[3]);
  mpfr_init2(t1_main, config_vals[4]);
  mpfr_init2(t2_main, config_vals[5]);
  mpfr_init2(dppi_main, config_vals[6]);
  mpfr_init2(s1_main, config_vals[7]);
        mpfr_init2 (temp_var_3, config_vals[0]);
        mpfr_init2 (temp_var_4, config_vals[0]);
        mpfr_init2 (temp_var_5, config_vals[0]);
        mpfr_init2 (temp_var_6, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 80; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

long double fun(long double x)
{
  int k;
  int n = 5;
  mpfr_set_ld(d1_fun, 1.0L, MPFR_RNDN);
  mpfr_set_ld(t1_fun, x, MPFR_RNDN);
  for (k = 1; k <= n; k++){
  {
            mpfr_mul_d(d1_fun, d1_fun, 2.0, MPFR_RNDN);
    
		
        mpfr_d_div(temp_var_2, sin(mpfr_get_ld(d1_fun, MPFR_RNDN) * x), d1_fun, MPFR_RNDN);
        
        mpfr_add(t1_fun, t1_fun, (temp_var_2), MPFR_RNDN);

  }

    }

  return mpfr_get_ld(t1_fun, MPFR_RNDN);
}

int main()
{
 init_readconfig();
  int i;
  int n = 1000000;
  mpfr_set_ld(t1_main, -1.0, MPFR_RNDN);
  mpfr_set_ld(dppi_main, acos(mpfr_get_ld(t1_main, MPFR_RNDN)), MPFR_RNDN);
  //~ printf("ddpi %.12Lf \n",mpfr_get_ld(dppi_main, MPFR_RNDN));
  mpfr_set_ld(s1_main, 0.0, MPFR_RNDN);
  mpfr_set_ld(t1_main, 0.0, MPFR_RNDN);
      mpfr_div_si(h_main, dppi_main, n, MPFR_RNDN);
  for (i = 1; i <= n; i++){
  {
    mpfr_set_ld(t2_main, fun(i * mpfr_get_ld(h_main, MPFR_RNDN)), MPFR_RNDN);
    
	//~ printf("t2 %.12Lf \n",mpfr_get_ld(t2_main, MPFR_RNDN));


        mpfr_add_d(s1_main, s1_main, sqrt((mpfr_get_ld(h_main, MPFR_RNDN) * mpfr_get_ld(h_main, MPFR_RNDN)) + ((mpfr_get_ld(t2_main, MPFR_RNDN) - mpfr_get_ld(t1_main, MPFR_RNDN)) * (mpfr_get_ld(t2_main, MPFR_RNDN) - mpfr_get_ld(t1_main, MPFR_RNDN)))), MPFR_RNDN);
    mpfr_set(t1_main, t2_main, MPFR_RNDN);
  }

    }

  printf("%.12Lf,", mpfr_get_ld(s1_main, MPFR_RNDN));
  return 0;
}

//end of conversion, hopefully it will work :)
