
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <mpfr.h>

#define LEN 35 
int config_vals[LEN];
   mpfr_t OutputX_CNDF;
  mpfr_t xInput_CNDF;
  mpfr_t xNPrimeofX_CNDF;
  mpfr_t expValues_CNDF;
  mpfr_t xK2_CNDF;
  mpfr_t xK2_2_CNDF;
  mpfr_t xK2_3_CNDF;
  mpfr_t xK2_4_CNDF;
  mpfr_t xK2_5_CNDF;
  mpfr_t xLocal_CNDF;
  mpfr_t xLocal_1_CNDF;
  mpfr_t xLocal_2_CNDF;
  mpfr_t xLocal_3_CNDF;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
  mpfr_t OptionPrice_BlkSchlsEqEuroNoDiv;
  mpfr_t xStockPrice_BlkSchlsEqEuroNoDiv;
  mpfr_t xStrikePrice_BlkSchlsEqEuroNoDiv;
  mpfr_t xRiskFreeRate_BlkSchlsEqEuroNoDiv;
  mpfr_t xVolatility_BlkSchlsEqEuroNoDiv;
  mpfr_t xTime_BlkSchlsEqEuroNoDiv;
  mpfr_t xSqrtTime_BlkSchlsEqEuroNoDiv;
  mpfr_t logValues_BlkSchlsEqEuroNoDiv;
  mpfr_t xLogTerm_BlkSchlsEqEuroNoDiv;
  mpfr_t xD1_BlkSchlsEqEuroNoDiv;
  mpfr_t xD2_BlkSchlsEqEuroNoDiv;
  mpfr_t xPowerTerm_BlkSchlsEqEuroNoDiv;
  mpfr_t xDen_BlkSchlsEqEuroNoDiv;
  mpfr_t d1_BlkSchlsEqEuroNoDiv;
  mpfr_t d2_BlkSchlsEqEuroNoDiv;
  mpfr_t FutureValueX_BlkSchlsEqEuroNoDiv;
  mpfr_t NofXd1_BlkSchlsEqEuroNoDiv;
  mpfr_t NofXd2_BlkSchlsEqEuroNoDiv;
  mpfr_t NegNofXd1_BlkSchlsEqEuroNoDiv;
  mpfr_t NegNofXd2_BlkSchlsEqEuroNoDiv;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
        mpfr_t temp_var_11;
        mpfr_t temp_var_12;
        mpfr_t temp_var_13;
        mpfr_t temp_var_14;
        mpfr_t temp_var_15;
        mpfr_t temp_var_16;
  mpfr_t price_bs_thread;
  mpfr_t priceDelta_bs_thread;
    mpfr_t temp_var_17;
    mpfr_t temp_var_18;
    mpfr_t temp_var_19;
    
int init_mpfr() { 
  mpfr_init2(OutputX_CNDF, config_vals[0]);
  mpfr_init2(xInput_CNDF, config_vals[1]);
  mpfr_init2(xNPrimeofX_CNDF, config_vals[2]);
  mpfr_init2(expValues_CNDF, config_vals[3]);
  mpfr_init2(xK2_CNDF, config_vals[4]);
  mpfr_init2(xK2_2_CNDF, config_vals[5]);
  mpfr_init2(xK2_3_CNDF, config_vals[6]);
  mpfr_init2(xK2_4_CNDF, config_vals[7]);
  mpfr_init2(xK2_5_CNDF, config_vals[8]);
  mpfr_init2(xLocal_CNDF, config_vals[9]);
  mpfr_init2(xLocal_1_CNDF, config_vals[10]);
  mpfr_init2(xLocal_2_CNDF, config_vals[11]);
  mpfr_init2(xLocal_3_CNDF, config_vals[12]);
    mpfr_init2 (temp_var_1,53);
    mpfr_init2 (temp_var_2,53);
    mpfr_init2 (temp_var_3,53);
    mpfr_init2 (temp_var_4,53);
  mpfr_init2(OptionPrice_BlkSchlsEqEuroNoDiv, config_vals[13]);
  mpfr_init2(xStockPrice_BlkSchlsEqEuroNoDiv, config_vals[14]);
  mpfr_init2(xStrikePrice_BlkSchlsEqEuroNoDiv, config_vals[15]);
  mpfr_init2(xRiskFreeRate_BlkSchlsEqEuroNoDiv, config_vals[16]);
  mpfr_init2(xVolatility_BlkSchlsEqEuroNoDiv, config_vals[17]);
  mpfr_init2(xTime_BlkSchlsEqEuroNoDiv, config_vals[18]);
  mpfr_init2(xSqrtTime_BlkSchlsEqEuroNoDiv, config_vals[19]);
  mpfr_init2(logValues_BlkSchlsEqEuroNoDiv, config_vals[20]);
  mpfr_init2(xLogTerm_BlkSchlsEqEuroNoDiv, config_vals[21]);
  mpfr_init2(xD1_BlkSchlsEqEuroNoDiv, config_vals[22]);
  mpfr_init2(xD2_BlkSchlsEqEuroNoDiv, config_vals[23]);
  mpfr_init2(xPowerTerm_BlkSchlsEqEuroNoDiv, config_vals[24]);
  mpfr_init2(xDen_BlkSchlsEqEuroNoDiv, config_vals[25]);
  mpfr_init2(d1_BlkSchlsEqEuroNoDiv, config_vals[26]);
  mpfr_init2(d2_BlkSchlsEqEuroNoDiv, config_vals[27]);
  mpfr_init2(FutureValueX_BlkSchlsEqEuroNoDiv, config_vals[28]);
  mpfr_init2(NofXd1_BlkSchlsEqEuroNoDiv, config_vals[29]);
  mpfr_init2(NofXd2_BlkSchlsEqEuroNoDiv, config_vals[30]);
  mpfr_init2(NegNofXd1_BlkSchlsEqEuroNoDiv, config_vals[31]);
  mpfr_init2(NegNofXd2_BlkSchlsEqEuroNoDiv, config_vals[32]);
    mpfr_init2 (temp_var_5,53);
    mpfr_init2 (temp_var_6,53);
    mpfr_init2 (temp_var_7,53);
    mpfr_init2 (temp_var_8,53);
    mpfr_init2 (temp_var_9,53);
    mpfr_init2 (temp_var_10,53);
        mpfr_init2 (temp_var_11,53);
        mpfr_init2 (temp_var_12,53);
        mpfr_init2 (temp_var_13,53);
        mpfr_init2 (temp_var_14,53);
        mpfr_init2 (temp_var_15,53);
        mpfr_init2 (temp_var_16,53);
  mpfr_init2(price_bs_thread, config_vals[33]);
  mpfr_init2(priceDelta_bs_thread, config_vals[34]);
    mpfr_init2 (temp_var_17,53);
    mpfr_init2 (temp_var_18,53);
    mpfr_init2 (temp_var_19,53);
}


int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        init_mpfr();
        return 0;             
}

typedef struct OptionData_
{
  double s;
  double strike;
  double r;
  double divq;
  double v;
  double t;
  char OptionType;
  double divs;
  double DGrefval;
} OptionData;
OptionData *data;
double *prices;
int numOptions;
int *otype;
double *sptprice;
double *strike;
double *rate;
double *volatility;
double *otime;
int numError = 0;
int nThreads;
double CNDF(double InputX)
{
  int sign;
  if (InputX < 0.0)
  {
    InputX = -InputX;
    sign = 1;
  }
  else
    sign = 0;

  mpfr_set_d(xInput_CNDF, InputX, MPFR_RNDD);
  
      mpfr_set_d(temp_var_1, InputX, MPFR_RNDD);

    mpfr_mul_d(temp_var_2, temp_var_1, (-0.5f), MPFR_RNDD);

    mpfr_set_d(temp_var_3, InputX, MPFR_RNDD);

    mpfr_mul(temp_var_4, (temp_var_2), temp_var_3, MPFR_RNDD);
  
	mpfr_exp(expValues_CNDF,temp_var_4, MPFR_RNDD);
 // mpfr_set_d(expValues_CNDF, exp(mpfr_get_d(temp_var_4, MPFR_RNDD)), MPFR_RNDD);
 
  mpfr_set(xNPrimeofX_CNDF, expValues_CNDF, MPFR_RNDD);
  

    mpfr_mul_d(xNPrimeofX_CNDF, xNPrimeofX_CNDF, 0.39894228040143270286, MPFR_RNDD);
;
      mpfr_mul_d(xK2_CNDF, xInput_CNDF, 0.2316419, MPFR_RNDD);
;
      mpfr_add_d(xK2_CNDF, xK2_CNDF, 1.0, MPFR_RNDD);
;
      mpfr_d_div(xK2_CNDF, 1.0, xK2_CNDF, MPFR_RNDD);
;
      mpfr_mul(xK2_2_CNDF, xK2_CNDF, xK2_CNDF, MPFR_RNDD);
;
      mpfr_mul(xK2_3_CNDF, xK2_2_CNDF, xK2_CNDF, MPFR_RNDD);
;
      mpfr_mul(xK2_4_CNDF, xK2_3_CNDF, xK2_CNDF, MPFR_RNDD);
;
      mpfr_mul(xK2_5_CNDF, xK2_4_CNDF, xK2_CNDF, MPFR_RNDD);
;
      mpfr_mul_d(xLocal_1_CNDF, xK2_CNDF, 0.319381530, MPFR_RNDD);
;
      mpfr_mul_d(xLocal_2_CNDF, xK2_2_CNDF, (-0.356563782), MPFR_RNDD);
;
      mpfr_mul_d(xLocal_3_CNDF, xK2_3_CNDF, 1.781477937, MPFR_RNDD);
;
      mpfr_add(xLocal_2_CNDF, xLocal_2_CNDF, xLocal_3_CNDF, MPFR_RNDD);
;
      mpfr_mul_d(xLocal_3_CNDF, xK2_4_CNDF, (-1.821255978), MPFR_RNDD);
;
      mpfr_add(xLocal_2_CNDF, xLocal_2_CNDF, xLocal_3_CNDF, MPFR_RNDD);
;
      mpfr_mul_d(xLocal_3_CNDF, xK2_5_CNDF, 1.330274429, MPFR_RNDD);
;
      mpfr_add(xLocal_2_CNDF, xLocal_2_CNDF, xLocal_3_CNDF, MPFR_RNDD);
;
      mpfr_add(xLocal_1_CNDF, xLocal_2_CNDF, xLocal_1_CNDF, MPFR_RNDD);
;
      mpfr_mul(xLocal_CNDF, xLocal_1_CNDF, xNPrimeofX_CNDF, MPFR_RNDD);
;
      mpfr_d_sub(xLocal_CNDF, 1.0, xLocal_CNDF, MPFR_RNDD);
;
  mpfr_set(OutputX_CNDF, xLocal_CNDF, MPFR_RNDD);
  if (sign)
  {
            mpfr_d_sub(OutputX_CNDF, 1.0, OutputX_CNDF, MPFR_RNDD);
;
  }

  return mpfr_get_d(OutputX_CNDF, MPFR_RNDD);
}

double BlkSchlsEqEuroNoDiv(double sptprice, double strike, double rate, double volatility, double time, int otype, double timet)
{
  mpfr_set_d(xStockPrice_BlkSchlsEqEuroNoDiv, sptprice, MPFR_RNDD);
  mpfr_set_d(xStrikePrice_BlkSchlsEqEuroNoDiv, strike, MPFR_RNDD);
  mpfr_set_d(xRiskFreeRate_BlkSchlsEqEuroNoDiv, rate, MPFR_RNDD);
  mpfr_set_d(xVolatility_BlkSchlsEqEuroNoDiv, volatility, MPFR_RNDD);
  mpfr_set_d(xTime_BlkSchlsEqEuroNoDiv, time, MPFR_RNDD);
  
  mpfr_sqrt(xSqrtTime_BlkSchlsEqEuroNoDiv, xTime_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
  
 // mpfr_set_d(xSqrtTime_BlkSchlsEqEuroNoDiv, sqrt(mpfr_get_d(xTime_BlkSchlsEqEuroNoDiv, MPFR_RNDD)), MPFR_RNDD);
 
   
    mpfr_set_d(temp_var_5, sptprice, MPFR_RNDD);

    mpfr_set_d(temp_var_6, strike, MPFR_RNDD);

    mpfr_div(temp_var_7, temp_var_5, temp_var_6, MPFR_RNDD);
    
    mpfr_log(logValues_BlkSchlsEqEuroNoDiv, temp_var_7, MPFR_RNDD);
    
 // mpfr_set_d(logValues_BlkSchlsEqEuroNoDiv, log(mpfr_get_d(temp_var_7, MPFR_RNDD)), MPFR_RNDD);
  mpfr_set(xLogTerm_BlkSchlsEqEuroNoDiv, logValues_BlkSchlsEqEuroNoDiv, MPFR_RNDD);

    mpfr_mul(xPowerTerm_BlkSchlsEqEuroNoDiv, xVolatility_BlkSchlsEqEuroNoDiv, xVolatility_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_mul_d(xPowerTerm_BlkSchlsEqEuroNoDiv, xPowerTerm_BlkSchlsEqEuroNoDiv, 0.5, MPFR_RNDD);
;
      mpfr_add(xD1_BlkSchlsEqEuroNoDiv, xRiskFreeRate_BlkSchlsEqEuroNoDiv, xPowerTerm_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_mul(xD1_BlkSchlsEqEuroNoDiv, xD1_BlkSchlsEqEuroNoDiv, xTime_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_add(xD1_BlkSchlsEqEuroNoDiv, xD1_BlkSchlsEqEuroNoDiv, xLogTerm_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_mul(xDen_BlkSchlsEqEuroNoDiv, xVolatility_BlkSchlsEqEuroNoDiv, xSqrtTime_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_div(xD1_BlkSchlsEqEuroNoDiv, xD1_BlkSchlsEqEuroNoDiv, xDen_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
      mpfr_sub(xD2_BlkSchlsEqEuroNoDiv, xD1_BlkSchlsEqEuroNoDiv, xDen_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
  mpfr_set(d1_BlkSchlsEqEuroNoDiv, xD1_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
  mpfr_set(d2_BlkSchlsEqEuroNoDiv, xD2_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
  mpfr_set_d(NofXd1_BlkSchlsEqEuroNoDiv, CNDF(mpfr_get_d(d1_BlkSchlsEqEuroNoDiv, MPFR_RNDD)), MPFR_RNDD);
  mpfr_set_d(NofXd2_BlkSchlsEqEuroNoDiv, CNDF(mpfr_get_d(d2_BlkSchlsEqEuroNoDiv, MPFR_RNDD)), MPFR_RNDD);
  
    mpfr_set_d(temp_var_8, time, MPFR_RNDD);
	
    mpfr_mul_d(temp_var_9, temp_var_8, (-rate), MPFR_RNDD);

    mpfr_set_d(temp_var_10, strike, MPFR_RNDD);
    
    mpfr_exp(temp_var_9,temp_var_9, MPFR_RNDD);
    
    mpfr_mul(FutureValueX_BlkSchlsEqEuroNoDiv, temp_var_10, temp_var_9,MPFR_RNDD)
  //  mpfr_mul_d(FutureValueX_BlkSchlsEqEuroNoDiv, temp_var_10, exp(mpfr_get_d(temp_var_9, MPFR_RNDD)), MPFR_RNDD);
;
  if (otype == 0)
  {
    
        mpfr_set_d(temp_var_11, sptprice, MPFR_RNDD);

        mpfr_mul(temp_var_12, temp_var_11, NofXd1_BlkSchlsEqEuroNoDiv, MPFR_RNDD);

        mpfr_mul(temp_var_13, FutureValueX_BlkSchlsEqEuroNoDiv, NofXd2_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
        mpfr_sub(OptionPrice_BlkSchlsEqEuroNoDiv, (temp_var_12), (temp_var_13), MPFR_RNDD);
;
  }
  else
  {
            mpfr_d_sub(NegNofXd1_BlkSchlsEqEuroNoDiv, 1.0, NofXd1_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
            mpfr_d_sub(NegNofXd2_BlkSchlsEqEuroNoDiv, 1.0, NofXd2_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
;
    
        mpfr_mul(temp_var_14, FutureValueX_BlkSchlsEqEuroNoDiv, NegNofXd2_BlkSchlsEqEuroNoDiv, MPFR_RNDD);

        mpfr_set_d(temp_var_15, sptprice, MPFR_RNDD);

        mpfr_mul(temp_var_16, temp_var_15, NegNofXd1_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
        mpfr_sub(OptionPrice_BlkSchlsEqEuroNoDiv, (temp_var_14), (temp_var_16), MPFR_RNDD);
;
  }

  return mpfr_get_d(OptionPrice_BlkSchlsEqEuroNoDiv, MPFR_RNDD);
}

int bs_thread(void *tid_ptr)
{
  int i;
  int j;
  int tid = *((int *) tid_ptr);
  int start = tid * (numOptions / nThreads);
  int end = start + (numOptions / nThreads);
  for (j = 0; j < 100; j++){
  {
    for (i = start; i < end; i++){

           double price = BlkSchlsEqEuroNoDiv( sptprice[i], strike[i],
                                         rate[i], volatility[i], otime[i], 
                                         otype[i], 0);
            prices[i] = price;

            double   priceDelta = data[i].DGrefval - price;
		printf("%lf,",priceDelta);  
          /*if( fabs(priceDelta) >= 1e-4 ){
                printf("Error on %d. Computed=%.5f, Ref=%.5f, Delta=%.5f\n",
                       i, price, data[i].DGrefval, priceDelta);
                numError ++;
            }
*/

        }

  }

    }

  return 0;
}

int main(int argc, char **argv)
{
  FILE *file;
  int i;
  int loopnum;
  double *buffer;
  int *buffer2;
  int rv;
  init_readconfig();
 // printf("PARSEC Benchmark Suite\n");
  fflush(0);
  if (argc != 4)
  {
    printf("Usage:\n\t%s <nthreads> <inputFile> <outputFile>\n", argv[0]);
    exit(1);
  }

  nThreads = atoi(argv[1]);
  char *inputFile = argv[2];
  char *outputFile = argv[3];
  file = fopen(inputFile, "r");
  if (file == 0)
  {
    printf("ERROR: Unable to open file `%s'.\n", inputFile);
    exit(1);
  }

  rv = fscanf(file, "%i", &numOptions);
  if (rv != 1)
  {
    printf("ERROR: Unable to read from file `%s'.\n", inputFile);
    fclose(file);
    exit(1);
  }

  if (nThreads > numOptions)
  {
    printf("WARNING: Not enough work, reducing number of threads to match number of options.\n");
    nThreads = numOptions;
  }

  if (nThreads != 1)
  {
    printf("Error: <nthreads> must be 1 (serial version)\n");
    exit(1);
  }

  data = (OptionData *) malloc(numOptions * (sizeof(OptionData)));
  prices = (double *) malloc(numOptions * (sizeof(double)));
  for (loopnum = 0; loopnum < numOptions; ++loopnum){
  {
    rv = fscanf(file, "%lf %lf %lf %lf %lf %lf %c %lf %lf", &data[loopnum].s, &data[loopnum].strike, &data[loopnum].r, &data[loopnum].divq, &data[loopnum].v, &data[loopnum].t, &data[loopnum].OptionType, &data[loopnum].divs, &data[loopnum].DGrefval);
    if (rv != 9)
    {
      printf("ERROR: Unable to read from file `%s'.\n", inputFile);
      fclose(file);
      exit(1);
    }

  }

    }

  rv = fclose(file);
  if (rv != 0)
  {
    printf("ERROR: Unable to close file `%s'.\n", inputFile);
    exit(1);
  }

 // printf("Num of Options: %d\n", numOptions);
 // printf("Num of Runs: %d\n", 100);
  buffer = (double *) malloc(((5 * numOptions) * (sizeof(double))) + 256);
  sptprice = (double *) ((((unsigned long long) buffer) + 256) & (~(64 - 1)));
  strike = sptprice + numOptions;
  rate = strike + numOptions;
  volatility = rate + numOptions;
  otime = volatility + numOptions;
  buffer2 = (int *) malloc((numOptions * (sizeof(double))) + 256);
  otype = (int *) ((((unsigned long long) buffer2) + 256) & (~(64 - 1)));
  for (i = 0; i < numOptions; i++){
  {
    otype[i] = data[i].OptionType == 'P' ? 1 : 0;
    sptprice[i] = data[i].s;
    strike[i] = data[i].strike;
    rate[i] = data[i].r;
    volatility[i] = data[i].v;
    otime[i] = data[i].t;
  }

    }

  //printf("Size of data: %d\n", numOptions * ((sizeof(OptionData)) + (sizeof(int))));
  int tid = 0;
  bs_thread(&tid);
  file = fopen(outputFile, "w");
  if (file == NULL)
  {
    printf("ERROR: Unable to open file `%s'.\n", outputFile);
    exit(1);
  }

  rv = fprintf(file, "%i\n", numOptions);
  if (rv < 0)
  {
    printf("ERROR: Unable to write to file `%s'.\n", outputFile);
    fclose(file);
    exit(1);
  }

  for (i = 0; i < numOptions; i++){
  {
    rv = fprintf(file, "%.18f\n", prices[i]);
    if (rv < 0)
    {
      printf("ERROR: Unable to write to file `%s'.\n", outputFile);
      fclose(file);
      exit(1);
    }

  }

    }

  rv = fclose(file);
  if (rv != 0)
  {
    printf("ERROR: Unable to close file `%s'.\n", outputFile);
    exit(1);
  }

  free(data);
  free(prices);
  return 0;
}

//end of conversion, hopefully it will work :)
