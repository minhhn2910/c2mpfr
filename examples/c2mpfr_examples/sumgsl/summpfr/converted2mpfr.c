//~ #include <stdio.h>
//~ #include <stdlib.h>
//~ #include <float.h>
//~ #include <math.h>
//~ #include <time.h>
//~ #include <inttypes.h>
//~ #include <stdarg.h>
#include "config.h"
#include "gsl_sum.h"
#include <gsl/gsl_math.h>
#include <gsl/gsl_test.h>
#include <gsl/gsl_errno.h>
#include <mpfr.h>
#define LEN 32 
#define ITERS 1
 int config_vals[LEN];
  mpfr_t a_temp_gsl_max_dbl_new;
  mpfr_t b_temp_gsl_max_dbl_new;
  mpfr_t array_temp_gsl_sum_levin_u_minmax;
  mpfr_t sum_accel_temp_gsl_sum_levin_u_minmax;
  mpfr_t abserr_temp_gsl_sum_levin_u_minmax;
    mpfr_t temp_var_1;
    mpfr_t SMALL_gsl_sum_levin_u_minmax;
    mpfr_t noise_n_gsl_sum_levin_u_minmax;
        mpfr_t temp_var_2;
    mpfr_t noise_nm1_gsl_sum_levin_u_minmax;
    mpfr_t trunc_n_gsl_sum_levin_u_minmax;
    mpfr_t trunc_nm1_gsl_sum_levin_u_minmax;
    mpfr_t actual_trunc_n_gsl_sum_levin_u_minmax;
    mpfr_t actual_trunc_nm1_gsl_sum_levin_u_minmax;
    mpfr_t result_n_gsl_sum_levin_u_minmax;
    mpfr_t result_nm1_gsl_sum_levin_u_minmax;
    mpfr_t variance_gsl_sum_levin_u_minmax;
    mpfr_t least_trunc_gsl_sum_levin_u_minmax;
    mpfr_t least_trunc_noise_gsl_sum_levin_u_minmax;
    mpfr_t least_trunc_result_gsl_sum_levin_u_minmax;
      mpfr_t t_gsl_sum_levin_u_minmax;
      mpfr_t dn_gsl_sum_levin_u_minmax;
            mpfr_t temp_var_3;
            mpfr_t temp_var_4;
      mpfr_t t1_gsl_sum_levin_u_minmax;
            mpfr_t temp_var_5;
            mpfr_t temp_var_6;
        mpfr_t dn2_gsl_sum_levin_u_minmax;
                mpfr_t temp_var_7;
                mpfr_t temp_var_8;
            mpfr_t temp_var_9;
            mpfr_t temp_var_10;
            mpfr_t temp_var_11;
            mpfr_t temp_var_12;
  mpfr_t sum_accel_temp_gsl_sum_levin_u_step;
  mpfr_t term_temp_gsl_sum_levin_u_step;
        mpfr_t temp_var_13;
        mpfr_t temp_var_14;
        mpfr_t temp_var_15;
    mpfr_t result_gsl_sum_levin_u_step;
    mpfr_t factor_gsl_sum_levin_u_step;
    mpfr_t ratio_gsl_sum_levin_u_step;
        mpfr_t temp_var_16;
        mpfr_t temp_var_17;
        mpfr_t temp_var_18;
        mpfr_t temp_var_19;
        mpfr_t temp_var_20;
        mpfr_t temp_var_21;
        mpfr_t temp_var_22;
        mpfr_t temp_var_23;
      mpfr_t c_gsl_sum_levin_u_step;
        mpfr_t temp_var_24;
            mpfr_t temp_var_25;
            mpfr_t temp_var_26;
            mpfr_t temp_var_27;
            mpfr_t temp_var_28;
            mpfr_t temp_var_29;
            mpfr_t temp_var_30;
            mpfr_t temp_var_31;
                mpfr_t temp_var_32;
                mpfr_t temp_var_33;
                mpfr_t temp_var_34;
                mpfr_t temp_var_35;
        mpfr_t temp_var_36;
            mpfr_t temp_var_37;
            mpfr_t temp_var_38;
            mpfr_t temp_var_39;
  mpfr_t sum_accel_main;
  mpfr_t err_main;
  mpfr_t t_temp_main;
        mpfr_t temp_var_40;
        mpfr_t temp_var_41;
int init_mpfr() { 
  mpfr_init2(a_temp_gsl_max_dbl_new, config_vals[1]);
  mpfr_init2(b_temp_gsl_max_dbl_new, config_vals[2]);
  mpfr_init2(array_temp_gsl_sum_levin_u_minmax, config_vals[3]);
  mpfr_init2(sum_accel_temp_gsl_sum_levin_u_minmax, config_vals[4]);
  mpfr_init2(abserr_temp_gsl_sum_levin_u_minmax, config_vals[5]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2(SMALL_gsl_sum_levin_u_minmax, config_vals[6]);
    mpfr_init2(noise_n_gsl_sum_levin_u_minmax, config_vals[7]);
        mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2(noise_nm1_gsl_sum_levin_u_minmax, config_vals[8]);
    mpfr_init2(trunc_n_gsl_sum_levin_u_minmax, config_vals[9]);
    mpfr_init2(trunc_nm1_gsl_sum_levin_u_minmax, config_vals[10]);
    mpfr_init2(actual_trunc_n_gsl_sum_levin_u_minmax, config_vals[11]);
    mpfr_init2(actual_trunc_nm1_gsl_sum_levin_u_minmax, config_vals[12]);
    mpfr_init2(result_n_gsl_sum_levin_u_minmax, config_vals[13]);
    mpfr_init2(result_nm1_gsl_sum_levin_u_minmax, config_vals[14]);
    mpfr_init2(variance_gsl_sum_levin_u_minmax, config_vals[15]);
    mpfr_init2(least_trunc_gsl_sum_levin_u_minmax, config_vals[16]);
    mpfr_init2(least_trunc_noise_gsl_sum_levin_u_minmax, config_vals[17]);
    mpfr_init2(least_trunc_result_gsl_sum_levin_u_minmax, config_vals[18]);
      mpfr_init2(t_gsl_sum_levin_u_minmax, config_vals[19]);
      mpfr_init2(dn_gsl_sum_levin_u_minmax, config_vals[20]);
            mpfr_init2 (temp_var_3, config_vals[0]);
            mpfr_init2 (temp_var_4, config_vals[0]);
      mpfr_init2(t1_gsl_sum_levin_u_minmax, config_vals[21]);
            mpfr_init2 (temp_var_5, config_vals[0]);
            mpfr_init2 (temp_var_6, config_vals[0]);
        mpfr_init2(dn2_gsl_sum_levin_u_minmax, config_vals[22]);
                mpfr_init2 (temp_var_7, config_vals[0]);
                mpfr_init2 (temp_var_8, config_vals[0]);
            mpfr_init2 (temp_var_9, config_vals[0]);
            mpfr_init2 (temp_var_10, config_vals[0]);
            mpfr_init2 (temp_var_11, config_vals[0]);
            mpfr_init2 (temp_var_12, config_vals[0]);
  mpfr_init2(sum_accel_temp_gsl_sum_levin_u_step, config_vals[23]);
  mpfr_init2(term_temp_gsl_sum_levin_u_step, config_vals[24]);
        mpfr_init2 (temp_var_13, config_vals[0]);
        mpfr_init2 (temp_var_14, config_vals[0]);
        mpfr_init2 (temp_var_15, config_vals[0]);
    mpfr_init2(result_gsl_sum_levin_u_step, config_vals[25]);
    mpfr_init2(factor_gsl_sum_levin_u_step, config_vals[26]);
    mpfr_init2(ratio_gsl_sum_levin_u_step, config_vals[27]);
        mpfr_init2 (temp_var_16, config_vals[0]);
        mpfr_init2 (temp_var_17, config_vals[0]);
        mpfr_init2 (temp_var_18, config_vals[0]);
        mpfr_init2 (temp_var_19, config_vals[0]);
        mpfr_init2 (temp_var_20, config_vals[0]);
        mpfr_init2 (temp_var_21, config_vals[0]);
        mpfr_init2 (temp_var_22, config_vals[0]);
        mpfr_init2 (temp_var_23, config_vals[0]);
      mpfr_init2(c_gsl_sum_levin_u_step, config_vals[28]);
        mpfr_init2 (temp_var_24, config_vals[0]);
            mpfr_init2 (temp_var_25, config_vals[0]);
            mpfr_init2 (temp_var_26, config_vals[0]);
            mpfr_init2 (temp_var_27, config_vals[0]);
            mpfr_init2 (temp_var_28, config_vals[0]);
            mpfr_init2 (temp_var_29, config_vals[0]);
            mpfr_init2 (temp_var_30, config_vals[0]);
            mpfr_init2 (temp_var_31, config_vals[0]);
                mpfr_init2 (temp_var_32, config_vals[0]);
                mpfr_init2 (temp_var_33, config_vals[0]);
                mpfr_init2 (temp_var_34, config_vals[0]);
                mpfr_init2 (temp_var_35, config_vals[0]);
        mpfr_init2 (temp_var_36, config_vals[0]);
            mpfr_init2 (temp_var_37, config_vals[0]);
            mpfr_init2 (temp_var_38, config_vals[0]);
            mpfr_init2 (temp_var_39, config_vals[0]);
  mpfr_init2(sum_accel_main, config_vals[29]);
  mpfr_init2(err_main, config_vals[30]);
  mpfr_init2(t_temp_main, config_vals[31]);
        mpfr_init2 (temp_var_40, config_vals[0]);
        mpfr_init2 (temp_var_41, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

gsl_sum_levin_u_workspace *gsl_sum_levin_u_alloc(size_t n);
void gsl_sum_levin_u_free(gsl_sum_levin_u_workspace *w);
int gsl_sum_levin_u_accel(const double *array, const size_t n, gsl_sum_levin_u_workspace *w, double *sum_accel, double *abserr);
int gsl_sum_levin_u_minmax(const double *array, const size_t n, const size_t min_terms, const size_t max_terms, gsl_sum_levin_u_workspace *w, double *sum_accel, double *abserr);
int gsl_sum_levin_u_step(const double term, const size_t n, const size_t nmax, gsl_sum_levin_u_workspace *w, double *sum_accel);
gsl_sum_levin_utrunc_workspace *gsl_sum_levin_utrunc_alloc(size_t n);
void gsl_sum_levin_utrunc_free(gsl_sum_levin_utrunc_workspace *w);
int gsl_sum_levin_utrunc_accel(const double *array, const size_t n, gsl_sum_levin_utrunc_workspace *w, double *sum_accel, double *abserr_trunc);
int gsl_sum_levin_utrunc_minmax(const double *array, const size_t n, const size_t min_terms, const size_t max_terms, gsl_sum_levin_utrunc_workspace *w, double *sum_accel, double *abserr_trunc);
int gsl_sum_levin_utrunc_step(const double term, const size_t n, gsl_sum_levin_utrunc_workspace *w, double *sum_accel);
double gsl_max_dbl_new(double a, double b)
{
  mpfr_set_d(a_temp_gsl_max_dbl_new, a, MPFR_RNDZ);
  mpfr_set_d(b_temp_gsl_max_dbl_new, b, MPFR_RNDZ);
  if (mpfr_greater_p (a_temp_gsl_max_dbl_new ,b_temp_gsl_max_dbl_new))
    return mpfr_get_d(a_temp_gsl_max_dbl_new, MPFR_RNDZ);
  else
    return mpfr_get_d(b_temp_gsl_max_dbl_new, MPFR_RNDZ);

}

int gsl_sum_levin_u_accel(const double *array, const size_t array_size, gsl_sum_levin_u_workspace *w, double *sum_accel, double *abserr)
{
  return gsl_sum_levin_u_minmax(array, array_size, 0, array_size - 1, w, sum_accel, abserr);
}


int gsl_sum_levin_u_minmax(const double *array, const size_t array_size, const size_t min_terms, const size_t max_terms, gsl_sum_levin_u_workspace *w, double *sum_accel, double *abserr)
{
  size_t size = array_size;
  mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[size - 1], MPFR_RNDZ);
  while ((size > 0) && (mpfr_get_d(array_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ)==0))
  {
    size--;
    mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[size - 1], MPFR_RNDZ);
  }

  if (size == 0)
  {
    mpfr_set_d(sum_accel_temp_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(abserr_temp_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    
*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    
*abserr = mpfr_get_d(abserr_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    w->sum_plain = 0.0;
    w->terms_used = 0;
    return GSL_SUCCESS;
  }
  else
    if (size == 1)
  {
    mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[0], MPFR_RNDZ);
    mpfr_set(sum_accel_temp_gsl_sum_levin_u_minmax, array_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    
*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    mpfr_set_d(abserr_temp_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    
*abserr = mpfr_get_d(abserr_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    
w->sum_plain = mpfr_get_d(array_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    w->terms_used = 1;
    return GSL_SUCCESS;
  }
  else
  {
    mpfr_set_d(SMALL_gsl_sum_levin_u_minmax, 0.01, MPFR_RNDZ);
    const size_t nmax = GSL_MAX(max_terms, array_size) - 1;
    
    mpfr_set_d(noise_nm1_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(trunc_n_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(trunc_nm1_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(actual_trunc_n_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(actual_trunc_nm1_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(result_n_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(result_nm1_gsl_sum_levin_u_minmax, 0.0, MPFR_RNDZ);
    mpfr_set_d(variance_gsl_sum_levin_u_minmax, 0, MPFR_RNDZ);
    size_t n;
    unsigned int i;
    int better = 0;
    int before = 0;
    int converging = 0;
    double result_n_gsl_sum_levin_u_minmax_temp;
    mpfr_set_d(least_trunc_gsl_sum_levin_u_minmax, GSL_DBL_MAX, MPFR_RNDZ);
    mpfr_set_d(least_trunc_noise_gsl_sum_levin_u_minmax, GSL_DBL_MAX, MPFR_RNDZ);
    for (n = 0; n < min_terms; n++){
    {
      mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[n], MPFR_RNDZ);
      
      mpfr_set(t_gsl_sum_levin_u_minmax,array_temp_gsl_sum_levin_u_minmax,MPFR_RNDZ);
      
      mpfr_set(result_nm1_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      
      
      //~ gsl_sum_levin_u_step(mpfr_get_d(t_gsl_sum_levin_u_minmax, MPFR_RNDZ), n, nmax, w, &mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ));
      result_n_gsl_sum_levin_u_minmax_temp = mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
       gsl_sum_levin_u_step(mpfr_get_d(t_gsl_sum_levin_u_minmax, MPFR_RNDZ), n, nmax, w, &result_n_gsl_sum_levin_u_minmax_temp);
       mpfr_set_d(result_n_gsl_sum_levin_u_minmax,result_n_gsl_sum_levin_u_minmax_temp, MPFR_RNDZ);
       
       //~ printf("levinu_min_max_result1 %lf \n",mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ));
      
    }

        }

    mpfr_set(least_trunc_result_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
    mpfr_set_d(variance_gsl_sum_levin_u_minmax, 0, MPFR_RNDZ);
    for (i = 0; i < n; i++){
    {
      mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[i], MPFR_RNDZ);
      
            mpfr_mul_d(dn_gsl_sum_levin_u_minmax, array_temp_gsl_sum_levin_u_minmax, (w->dsum[i] * GSL_MACH_EPS), MPFR_RNDZ);
      
            mpfr_mul(temp_var_4, dn_gsl_sum_levin_u_minmax, dn_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      mpfr_add(variance_gsl_sum_levin_u_minmax, variance_gsl_sum_levin_u_minmax, temp_var_4, MPFR_RNDZ);
    }

        }

    mpfr_set_d(noise_n_gsl_sum_levin_u_minmax, sqrt(mpfr_get_d(variance_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);
   // double result_n_gsl_sum_levin_u_minmax_temp ;
    for (; n <= nmax; n++){
    {
      mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[n], MPFR_RNDZ);
      
      mpfr_set(t1_gsl_sum_levin_u_minmax,array_temp_gsl_sum_levin_u_minmax,MPFR_RNDZ);;
      
      mpfr_set(result_nm1_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      
      result_n_gsl_sum_levin_u_minmax_temp = mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      gsl_sum_levin_u_step(mpfr_get_d(t1_gsl_sum_levin_u_minmax, MPFR_RNDZ), n, nmax, w, &result_n_gsl_sum_levin_u_minmax_temp);
      
      mpfr_set_d(result_n_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax_temp , MPFR_RNDZ);
      
       //~ printf("levinu_min_max_result 2 %lf \n",mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ));
      
      mpfr_set(actual_trunc_nm1_gsl_sum_levin_u_minmax, actual_trunc_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      mpfr_set_d(actual_trunc_n_gsl_sum_levin_u_minmax, fabs(mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ) - mpfr_get_d(result_nm1_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);
      mpfr_set(trunc_nm1_gsl_sum_levin_u_minmax, trunc_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      

            mpfr_add(temp_var_6, actual_trunc_n_gsl_sum_levin_u_minmax, actual_trunc_nm1_gsl_sum_levin_u_minmax, MPFR_RNDZ);
            mpfr_mul_d(trunc_n_gsl_sum_levin_u_minmax, (temp_var_6), 0.5, MPFR_RNDZ);
      mpfr_set(noise_nm1_gsl_sum_levin_u_minmax, noise_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      mpfr_set_d(variance_gsl_sum_levin_u_minmax, 0, MPFR_RNDZ);
      for (i = 0; i <= n; i++){
      {
        mpfr_set_d(array_temp_gsl_sum_levin_u_minmax, array[i], MPFR_RNDZ);
        
                mpfr_mul_d(dn2_gsl_sum_levin_u_minmax, array_temp_gsl_sum_levin_u_minmax, (w->dsum[i] * GSL_MACH_EPS), MPFR_RNDZ);
        
                mpfr_mul(temp_var_8, dn2_gsl_sum_levin_u_minmax, dn2_gsl_sum_levin_u_minmax, MPFR_RNDZ);
        mpfr_add(variance_gsl_sum_levin_u_minmax, variance_gsl_sum_levin_u_minmax, temp_var_8, MPFR_RNDZ);
      }

            }

      mpfr_set_d(noise_n_gsl_sum_levin_u_minmax, sqrt(mpfr_get_d(variance_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);
      
      
      
            mpfr_mul_d(temp_var_9, SMALL_gsl_sum_levin_u_minmax, fabs(mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);



          //~ better = (trunc_n < trunc_nm1 || trunc_n < SMALL * fabs (result_n));


		better = (mpfr_less_p(trunc_n_gsl_sum_levin_u_minmax, trunc_nm1_gsl_sum_levin_u_minmax) ||mpfr_less_p(trunc_n_gsl_sum_levin_u_minmax, temp_var_9) );

      converging = converging || (better && before);
      before = better;
      if (converging)
      {
        if (mpfr_less_p(trunc_n_gsl_sum_levin_u_minmax , least_trunc_gsl_sum_levin_u_minmax))
        {
          mpfr_set(least_trunc_result_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
          mpfr_set(least_trunc_gsl_sum_levin_u_minmax, trunc_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
          mpfr_set(least_trunc_noise_gsl_sum_levin_u_minmax, noise_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
        }

		mpfr_div_d(temp_var_9,trunc_n_gsl_sum_levin_u_minmax,3.0,MPFR_RNDZ);
        //~ if (mpfr_greater_p(noise_n_gsl_sum_levin_u_minmax,temp_var_9))	
        if(n==11) //10 for e<10e-10. 11 for e= e-10
          break;
		//~ if (trunc_n < 10.0 * GSL_MACH_EPS * fabs (result_n))
        if (mpfr_cmp_d(trunc_n_gsl_sum_levin_u_minmax, ((10.0 * GSL_MACH_EPS) * fabs(mpfr_get_d(result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ)))) < 0)
			  break;

      }

    }

        }

    if (converging)
    {
      mpfr_set(sum_accel_temp_gsl_sum_levin_u_minmax, least_trunc_result_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      
*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      mpfr_set_d(abserr_temp_gsl_sum_levin_u_minmax, gsl_max_dbl_new(mpfr_get_d(least_trunc_gsl_sum_levin_u_minmax, MPFR_RNDZ), mpfr_get_d(least_trunc_noise_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);
      
*abserr = mpfr_get_d(abserr_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      w->terms_used = n;
      return GSL_SUCCESS;
    }
    else
    {
      mpfr_set(sum_accel_temp_gsl_sum_levin_u_minmax, result_n_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      
*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      mpfr_set_d(abserr_temp_gsl_sum_levin_u_minmax, gsl_max_dbl_new(mpfr_get_d(trunc_n_gsl_sum_levin_u_minmax, MPFR_RNDZ), mpfr_get_d(noise_n_gsl_sum_levin_u_minmax, MPFR_RNDZ)), MPFR_RNDZ);
      
*abserr = mpfr_get_d(abserr_temp_gsl_sum_levin_u_minmax, MPFR_RNDZ);
      w->terms_used = n;
      return GSL_SUCCESS;
    }

  }


}



int gsl_sum_levin_u_step(const double term, const size_t n, const size_t nmax, gsl_sum_levin_u_workspace *w, double *sum_accel)
{
  mpfr_set_d(term_temp_gsl_sum_levin_u_step, term, MPFR_RNDZ);
  if (n == 0)
  {
    mpfr_set(sum_accel_temp_gsl_sum_levin_u_step, term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);
    
*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_step, MPFR_RNDZ);
    
w->sum_plain = mpfr_get_d(term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);
    
        mpfr_d_div(temp_var_13, 1.0, term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);

w->q_den[0] = mpfr_get_d(temp_var_13, MPFR_RNDZ);
    w->q_num[0] = 1.0;
    
        mpfr_mul(temp_var_14, term_temp_gsl_sum_levin_u_step, term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);

        mpfr_d_div(temp_var_15, (-1.0), (temp_var_14), MPFR_RNDZ);

w->dq_den[(0 * (nmax + 1)) + 0] = mpfr_get_d(temp_var_15, MPFR_RNDZ);
    w->dq_num[(0 * (nmax + 1)) + 0] = 0.0;
    w->dsum[0] = 1.0;
    return GSL_SUCCESS;
  }
  else
  {
    mpfr_set_d(factor_gsl_sum_levin_u_step, 1.0, MPFR_RNDZ);
    //~ double ratio = (double) n / (n + 1.0);
    mpfr_set_d(ratio_gsl_sum_levin_u_step, (double) n / (n + 1.0), MPFR_RNDZ);
    unsigned int i;
    int j;
    
w->sum_plain += mpfr_get_d(term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);
    

        mpfr_mul_d(temp_var_18, term_temp_gsl_sum_levin_u_step, (n + 1.0), MPFR_RNDZ);


        mpfr_mul_d(temp_var_20, (temp_var_18), (n + 1.0), MPFR_RNDZ);

        mpfr_d_div(temp_var_21, 1.0, (temp_var_20), MPFR_RNDZ);

w->q_den[n] = mpfr_get_d(temp_var_21, MPFR_RNDZ);
    w->q_num[n] = w->sum_plain * w->q_den[n];
    for (i = 0; i < n; i++){
    {
      w->dq_den[(i * (nmax + 1)) + n] = 0;
      w->dq_num[(i * (nmax + 1)) + n] = w->q_den[n];
    }

        }

    

        mpfr_d_div(temp_var_23, (-w->q_den[n]), term_temp_gsl_sum_levin_u_step, MPFR_RNDZ);

w->dq_den[(n * (nmax + 1)) + n] = mpfr_get_d(temp_var_23, MPFR_RNDZ);
    w->dq_num[(n * (nmax + 1)) + n] = w->q_den[n] + (w->sum_plain * w->dq_den[(n * (nmax + 1)) + n]);
    for (j = n - 1; j >= 0; j--){
    {
      


            mpfr_mul_d(temp_var_26, factor_gsl_sum_levin_u_step, (j + 1), MPFR_RNDZ);

            mpfr_div_d(c_gsl_sum_levin_u_step, (temp_var_26), (n + 1), MPFR_RNDZ);
            
            //~ printf("c, factor, ratio %lf %lf %lf \n",mpfr_get_d(c_gsl_sum_levin_u_step,MPFR_RNDZ),mpfr_get_d(factor_gsl_sum_levin_u_step,MPFR_RNDZ),mpfr_get_d(ratio_gsl_sum_levin_u_step,MPFR_RNDZ));
            mpfr_mul(factor_gsl_sum_levin_u_step, factor_gsl_sum_levin_u_step, ratio_gsl_sum_levin_u_step, MPFR_RNDZ);
      
            mpfr_mul_d(temp_var_28, c_gsl_sum_levin_u_step, w->q_den[j], MPFR_RNDZ);

            mpfr_d_sub(temp_var_29, w->q_den[j + 1], (temp_var_28), MPFR_RNDZ);

w->q_den[j] = mpfr_get_d(temp_var_29, MPFR_RNDZ);
      
            mpfr_mul_d(temp_var_30, c_gsl_sum_levin_u_step, w->q_num[j], MPFR_RNDZ);

            mpfr_d_sub(temp_var_31, w->q_num[j + 1], (temp_var_30), MPFR_RNDZ);

w->q_num[j] = mpfr_get_d(temp_var_31, MPFR_RNDZ);
      for (i = 0; i < n; i++){
      {
        
                mpfr_mul_d(temp_var_32, c_gsl_sum_levin_u_step, w->dq_den[(i * (nmax + 1)) + j], MPFR_RNDZ);

                mpfr_d_sub(temp_var_33, w->dq_den[(i * (nmax + 1)) + (j + 1)], (temp_var_32), MPFR_RNDZ);

w->dq_den[(i * (nmax + 1)) + j] = mpfr_get_d(temp_var_33, MPFR_RNDZ);
        
                mpfr_mul_d(temp_var_34, c_gsl_sum_levin_u_step, w->dq_num[(i * (nmax + 1)) + j], MPFR_RNDZ);

                mpfr_d_sub(temp_var_35, w->dq_num[(i * (nmax + 1)) + (j + 1)], (temp_var_34), MPFR_RNDZ);

w->dq_num[(i * (nmax + 1)) + j] = mpfr_get_d(temp_var_35, MPFR_RNDZ);
      }

            }

      w->dq_den[(n * (nmax + 1)) + j] = w->dq_den[(n * (nmax + 1)) + (j + 1)];
      w->dq_num[(n * (nmax + 1)) + j] = w->dq_num[(n * (nmax + 1)) + (j + 1)];
    }

        }

    mpfr_set_d(result_gsl_sum_levin_u_step, w->q_num[0] / w->q_den[0], MPFR_RNDZ);
    mpfr_set(sum_accel_temp_gsl_sum_levin_u_step, result_gsl_sum_levin_u_step, MPFR_RNDZ);
    //~ printf("result %lf \n",mpfr_get_d(result_gsl_sum_levin_u_step,MPFR_RNDZ));

*sum_accel = mpfr_get_d(sum_accel_temp_gsl_sum_levin_u_step, MPFR_RNDZ);
    for (i = 0; i <= n; i++){
    {
      
            mpfr_mul_d(temp_var_37, result_gsl_sum_levin_u_step, w->dq_den[(i * (nmax + 1)) + 0], MPFR_RNDZ);

            mpfr_d_sub(temp_var_38, w->dq_num[(i * (nmax + 1)) + 0], (temp_var_37), MPFR_RNDZ);

            mpfr_div_d(temp_var_39, (temp_var_38), w->q_den[0], MPFR_RNDZ);

w->dsum[i] = mpfr_get_d(temp_var_39, MPFR_RNDZ);
		//~ printf("wdsum i %d   %lf \n",i, mpfr_get_d(temp_var_39, MPFR_RNDZ));
    }

        }

    return GSL_SUCCESS;
  }


}




/*
int
gsl_sum_levin_u_step (const double term, const size_t n, const size_t nmax,
                      gsl_sum_levin_u_workspace * w, double *sum_accel)
{

#define I(i,j) ((i)*(nmax+1) + (j))


	double sum_accel_temp;
	
		double term_temp = term;

  if (n == 0)
    {
      //~ *sum_accel = term;
      sum_accel_temp = term_temp; 
      *sum_accel = sum_accel_temp;
      
      w->sum_plain = term_temp;

      w->q_den[0] = 1.0 / term_temp;
      w->q_num[0] = 1.0;

      w->dq_den[I (0, 0)] = -1.0 / (term_temp * term_temp);
      w->dq_num[I (0, 0)] = 0.0;

      w->dsum[0] = 1.0;

      return GSL_SUCCESS;
    }
  else
    {
      double result;
      double factor = 1.0;
      double ratio = (double) n / (n + 1.0);
      unsigned int i;
      int j;

      w->sum_plain += term_temp;

      w->q_den[n] = 1.0 / (term_temp * (n + 1.0) * (n + 1.0));
      w->q_num[n] = w->sum_plain * w->q_den[n];

      for (i = 0; i < n; i++)
        {
          w->dq_den[I (i, n)] = 0;
          w->dq_num[I (i, n)] = w->q_den[n];
        }

      w->dq_den[I (n, n)] = -w->q_den[n] / term_temp;
      w->dq_num[I (n, n)] =
        w->q_den[n] + w->sum_plain * (w->dq_den[I (n, n)]);

      for (j = n - 1; j >= 0; j--)
        {
          double c = factor * (j + 1) / (n + 1);
          factor *= ratio;
          w->q_den[j] = w->q_den[j + 1] - c * w->q_den[j];
          w->q_num[j] = w->q_num[j + 1] - c * w->q_num[j];

          for (i = 0; i < n; i++)
            {
              w->dq_den[I (i, j)] =
                w->dq_den[I (i, j + 1)] - c * w->dq_den[I (i, j)];
              w->dq_num[I (i, j)] =
                w->dq_num[I (i, j + 1)] - c * w->dq_num[I (i, j)];
            }

          w->dq_den[I (n, j)] = w->dq_den[I (n, j + 1)];
          w->dq_num[I (n, j)] = w->dq_num[I (n, j + 1)];
        }

      result = w->q_num[0] / w->q_den[0];

		sum_accel_temp = result;
      //~ *sum_accel = result;
      *sum_accel = sum_accel_temp;

      for (i = 0; i <= n; i++)
        {
          w->dsum[i] =
            (w->dq_num[I (i, 0)] -
             result * w->dq_den[I (i, 0)]) / w->q_den[0];
        }

      return GSL_SUCCESS;
    }
}
*/


int main(void)
{
 init_readconfig();
  int i;
  int j;
  double t[100];
  double err_main_main_temp;
  double sum_accel_main_temp;
  int n;
  gsl_sum_levin_u_workspace *w = gsl_sum_levin_u_alloc(100);
  for (n = 0; n < 100; n++){
  {
    float np1 = n + 1.0;
    mpfr_set_d(t_temp_main, 1.0 / (np1 * np1), MPFR_RNDZ);
    


t[n] = mpfr_get_d(t_temp_main, MPFR_RNDZ);
  }

    }

  for (j = 0; j < ITERS; j++){
  {
    mpfr_set_d(sum_accel_main, 0, MPFR_RNDZ);
    mpfr_set_d(err_main, 0, MPFR_RNDZ);
     sum_accel_main_temp = mpfr_get_d(sum_accel_main, MPFR_RNDZ);
     err_main_main_temp = mpfr_get_d(err_main, MPFR_RNDZ);
    gsl_sum_levin_u_accel(t, 100, w, &sum_accel_main_temp, &err_main_main_temp);

    
  }

    }
    
    mpfr_set_d(sum_accel_main, sum_accel_main_temp, MPFR_RNDZ);

      mpfr_add_si(sum_accel_main, sum_accel_main, 0, MPFR_RNDZ);
  gsl_sum_levin_u_free(w);
  printf("%.12lf \n", mpfr_get_d(sum_accel_main, MPFR_RNDZ));
  return 0;
}

//end of conversion, hopefully it will work :)
