#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define INPUTS 10

long double fun( long double x )
{
  int k, n = 5;
  long double t1, d1 = 1.0L;

  t1 = x;

  for( k = 1; k <= n; k++ )
  {
    d1 = 2.0 * d1;
    t1 = t1 + sin (d1 * x) / d1;
    //~ printf("t1 %.12Lf \n",t1);
  }

  return t1;
}

int main()
{
  int i, n=2000000; 
  long double h, t1, t2, dppi;
  long double s1; 


    t1 = -1.0;
    dppi = acos(t1);
    //~ printf("ddpi %.12Lf \n",dppi);
    s1 = 0.0;
    t1 = 0.0;
    h = dppi / n;

    for( i = 1; i <= n; i++ )
    {
      t2 = fun (i * h);
      //~ printf("t2 %.12Lf \n",t2);
      s1 = s1 + sqrt (h*h + (t2 - t1)*(t2 - t1));
      t1 = t2;
    }
	printf("%.12Lf,",s1);

  return 0;

}


