#!/usr/bin/python
import sys
import random
#import subprocess
from subprocess import *
target_result = 1.300963e+06


epsilon = 0.000001

def random_search(conf_file, sample_c):
	original_array = read_conf(conf_file)
	for loop in range(len(original_array)):
		precision_array = list(original_array)
		permutation_array = get_permutation(len(original_array))
		for i in permutation_array:	
			previous_i = precision_array[i]
			write_conf(conf_file,precision_array)
			while run_program(sample_c):
				previous_i = precision_array[i]
				#print precision_array
				precision_array[i] = precision_array[i] - 1;
				#print precision_array[i] 
				#decrease the precision at point i
				write_conf(conf_file,precision_array)
			precision_array[i] = previous_i
			#print precision_array
			#print i
			#print permutation_array
		write_log(precision_array, loop)
		#print 'write log '
	write_conf(conf_file, original_array)		
	
def run_program(sample_c):
#	subprocess.check_output = check_output
#	subprocess.check_output(['./MyFile'])
	output = Popen(["./mpfr_sample", ""], stdout=PIPE).communicate()[0]
	#s1 = subprocess.check_output(['./mpfr_sample'])
	result = float(output)
    #result = float(open("OUTPUT.txt").read())
	
      	error = abs(target_result - result)/target_result
	print 'result ' + str(result)
	print 'target ' + str(target_result)
	print 'error ' + str(error)
    	if error < epsilon:
    	    #print(target_result, result, error, epsilon)
	    print 'result ' + str(result)
	    print 'error '+ str(error)
            return True
    	return False

def write_log(precision_array, loop):
	with open('log.txt', 'a') as log_file:
		log_file.write('Loop ' + str(loop + 1) +' :  ')
		log_file.write(str(precision_array) +'\n' )
		
def get_permutation(array_length):
	result = range(array_length)
	random.seed()
	random.shuffle(result)
	return result
	
def read_conf(conf_file_name):
	#format a1,a2,a3...
	list_argument = []
	with open(conf_file_name) as conf_file:
		for line in conf_file:
			line.replace(" ", "")
			#remove unexpected space
			array = line.split(',')
			


			for argument in array[:-1]:
				try:
			#		if(len(argument)>0):
						list_argument.append(int(argument))
				except:
					print "Failed to parse conf file"
	return 	list_argument	
	
def write_conf(conf_file,precision_array):
	conf_string = ''
	for i in precision_array:
		conf_string += str(i) + ","
	with open(conf_file, 'w') as write_file:
		write_file.write(conf_string)
		
def main(argv):
		if len(argv) != 2 :
			print "Usage: ./search.py config_file program"
		else :
			random_search(argv[0],argv[1])
			 
if __name__ == "__main__":
   main(sys.argv[1:])
