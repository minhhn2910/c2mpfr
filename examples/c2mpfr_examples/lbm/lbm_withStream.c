/* $Id: lbm.c,v 1.6 2004/05/03 08:23:51 pohlt Exp $ */

/*############################################################################*/

#include "lbm.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <mpir.h>
#include <mpfr.h>
#include <fenv.h>

#if !defined(SPEC_CPU)
#ifdef _OPENMP
#include <omp.h>
#endif
#endif
#define LEN 14 
int config_vals[LEN];
int init() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
 printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        return 0;             
}
/*############################################################################*/

#define DFL1 (1.0/ 3.0)
#define DFL2 (1.0/18.0)
#define DFL3 (1.0/36.0)

void LBM_allocateGrid( double** ptr ) {
	const size_t margin = 2*SIZE_X*SIZE_Y*N_CELL_ENTRIES,
	             size   = sizeof( LBM_Grid ) + 2*margin*sizeof( double );
	             
	*ptr = malloc( size );
	
/*	asm volatile ("movq %0, %%r11; nopq (%%r11);" 
				:
				:"r"(*ptr)
				:"%r11");
	asm volatile ("movq %0, %%r12; nopq (%%r12);"
				:
				:"r"(*ptr + size / sizeof(double))
				:"%r12");
*/
	if( ! *ptr ) {
		printf( "LBM_allocateGrid: could not allocate %f Byte\n",size / (1024.0*1024.0) );
		exit( 1 );
	}
#if !defined(SPEC_CPU)
	printf( "LBM_allocateGrid: allocated %zu Byte\n", size );
	//printf( "LBM_allocateGrid: ptr = %p\n", *ptr );
	//printf( "LBM_allocateGrid: ptr_end = %p\n", *ptr + size / 8);
#endif

	*ptr += margin;
}

/*############################################################################*/

void LBM_freeGrid( double** ptr ) {
	const size_t margin = 2*SIZE_X*SIZE_Y*N_CELL_ENTRIES;

	free( *ptr-margin );
	*ptr = NULL;
}

/*############################################################################*/

void LBM_initializeGrid( LBM_Grid grid ) {
//void LBM_initializeGrid( mpfr_t grid )
	SWEEP_VAR

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for
#endif
#endif
	SWEEP_START( 0, 0, -2, 0, 0, SIZE_Z+2 )
		LOCAL( grid, C  ) = DFL1;
		LOCAL( grid, N  ) = DFL2;
		LOCAL( grid, S  ) = DFL2;
		LOCAL( grid, E  ) = DFL2;
		LOCAL( grid, W  ) = DFL2;
		LOCAL( grid, T  ) = DFL2;
		LOCAL( grid, B  ) = DFL2;
		LOCAL( grid, NE ) = DFL3;
		LOCAL( grid, NW ) = DFL3;
		LOCAL( grid, SE ) = DFL3;
		LOCAL( grid, SW ) = DFL3;
		LOCAL( grid, NT ) = DFL3;
		LOCAL( grid, NB ) = DFL3;
		LOCAL( grid, ST ) = DFL3;
		LOCAL( grid, SB ) = DFL3;
		LOCAL( grid, ET ) = DFL3;
		LOCAL( grid, EB ) = DFL3;
		LOCAL( grid, WT ) = DFL3;
		LOCAL( grid, WB ) = DFL3;

		CLEAR_ALL_FLAGS_SWEEP( grid );
	SWEEP_END
}

/*############################################################################*/

void LBM_swapGrids( LBM_GridPtr* grid1, LBM_GridPtr* grid2 ) {
	LBM_GridPtr aux = *grid1;
	*grid1 = *grid2;
	*grid2 = aux;
}

/*############################################################################*/

void LBM_loadObstacleFile( LBM_Grid grid, const char* filename ) {
	int x,  y,  z;

	FILE* file = fopen( filename, "rb" );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( fgetc( file ) != '.' ) SET_FLAG( grid, x, y, z, OBSTACLE );
			}
			fgetc( file );
		}
		fgetc( file );
	}

	fclose( file );
}

/*############################################################################*/

void LBM_initializeSpecialCellsForLDC( LBM_Grid grid ) {
	int x,  y,  z;

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( x, y )
#endif
#endif
	for( z = -2; z < SIZE_Z+2; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( x == 0 || x == SIZE_X-1 ||
				    y == 0 || y == SIZE_Y-1 ||
				    z == 0 || z == SIZE_Z-1 ) {
					SET_FLAG( grid, x, y, z, OBSTACLE );
				}
				else {
					if( (z == 1 || z == SIZE_Z-2) &&
					     x > 1 && x < SIZE_X-2 &&
					     y > 1 && y < SIZE_Y-2 ) {
						SET_FLAG( grid, x, y, z, ACCEL );
					}
				}
			}
		}
	}
}

/*############################################################################*/

void LBM_initializeSpecialCellsForChannel( LBM_Grid grid ) {
	int x,  y,  z;

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( x, y )
#endif
#endif
	for( z = -2; z < SIZE_Z+2; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( x == 0 || x == SIZE_X-1 ||
				    y == 0 || y == SIZE_Y-1 ) {
					SET_FLAG( grid, x, y, z, OBSTACLE );

					if( (z == 0 || z == SIZE_Z-1) &&
					    ! TEST_FLAG( grid, x, y, z, OBSTACLE ))
						SET_FLAG( grid, x, y, z, IN_OUT_FLOW );
				}
			}
		}
	}
}

/*############################################################################*/

void LBM_performStreamCollide( LBM_Grid srcGrid, LBM_Grid dstGrid ) {
	SWEEP_VAR

//	double ux, uy, uz, u2, rho;
        mpfr_t t, ux, uy, uz, u2, rho,p,q,r,onept5,one,t1,t2,t3,O,s1,ss2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,D,c,n,s,e,w,tt,b,ne,nw,se,sw,nt,nb,st,sb,et,eb,wt,wb,three,fourpt5,fourpt5uy,fourpt5ux,fourpt5uz,src_N,src_C,src_S,src_E,src_W,src_T,src_B,src_NE,src_NW,src_SE,src_SW,src_NT,src_ST,src_SB,src_ET,src_EB,src_WT,src_WB,o,o2,o3,NEadd,NWadd,SEadd,SWadd,NTadd,NBadd,STadd,SBadd,ETadd,EBadd,WTadd,WBadd,ux_neg,uy_neg,uz_neg,strange;
	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( ux, uy, uz, u2, rho )
#endif
#endif

	 mpfr_init2(ux, config_vals[1]);
	mpfr_init2(uy, config_vals[2]);
	mpfr_init2(uz, config_vals[3]);
	mpfr_init2(u2, config_vals[4]);
	mpfr_init2(rho, config_vals[5]);
	mpfr_init2(p, config_vals[0]);
	mpfr_init2(q, config_vals[0]);
	mpfr_init2(r, config_vals[0]);
	mpfr_init2(onept5, config_vals[0]);
	mpfr_init2(one, config_vals[0]);
	mpfr_init2(t1, config_vals[0]);
	mpfr_init2(t2, config_vals[0]);
	mpfr_init2(t3, config_vals[0]);
	mpfr_init2(ss2, config_vals[0]);
	mpfr_init2(O, config_vals[0]);
	mpfr_init2(s1, config_vals[0]);
	mpfr_init2(s3, config_vals[0]);
	mpfr_init2(s4, config_vals[0]);
	mpfr_init2(s5, config_vals[0]);
	mpfr_init2(s6, config_vals[0]);
	mpfr_init2(s7, config_vals[0]);
	mpfr_init2(s8, config_vals[0]);
	mpfr_init2(s9, config_vals[0]);
	mpfr_init2(s10, config_vals[0]);
	mpfr_init2(s11, config_vals[0]);
	mpfr_init2(s12, config_vals[0]);
	mpfr_init2(s13, config_vals[0]);
	mpfr_init2(s14, config_vals[0]);
	mpfr_init2(s15, config_vals[0]);
	mpfr_init2(s16, config_vals[0]);
	mpfr_init2(s17, config_vals[0]);
	mpfr_init2(s18, config_vals[0]);
	mpfr_init2(s19, config_vals[0]);
	mpfr_init2(D, config_vals[0]);
	mpfr_init2(t, config_vals[0]);
	mpfr_init2(o3, config_vals[0]);
	mpfr_init2(o, config_vals[0]);
	mpfr_init2(o2, config_vals[0]);
	mpfr_init2(three, config_vals[0]);
	mpfr_init2(fourpt5, config_vals[0]);
	mpfr_init2(fourpt5uy, config_vals[0]);
	mpfr_init2(fourpt5uz, config_vals[0]);
	mpfr_init2(fourpt5ux, config_vals[0]);
	mpfr_init2(NTadd, config_vals[0]);
	mpfr_init2(NBadd, config_vals[0]);
	mpfr_init2(STadd, config_vals[0]);
	mpfr_init2(SBadd, config_vals[0]);
	mpfr_init2(ETadd, config_vals[0]);
	mpfr_init2(NEadd, config_vals[0]);
	mpfr_init2(SWadd, config_vals[0]);
	mpfr_init2(EBadd, config_vals[0]);
	mpfr_init2(WTadd, config_vals[0]);
	mpfr_init2(WBadd, config_vals[0]);
	mpfr_init2(NWadd, config_vals[0]);
	mpfr_init2(SEadd, config_vals[0]);
	mpfr_init2(ux_neg, config_vals[0]);
	mpfr_init2(uy_neg, config_vals[0]);
	mpfr_set_d(fourpt5ux,0.0,MPFR_RNDD);
	mpfr_set_d(fourpt5uy,0.0,MPFR_RNDD);
	mpfr_set_d(fourpt5uz,0.0,MPFR_RNDD);
	mpfr_set_d(three,3.0,MPFR_RNDD);
	mpfr_set_d(one,1.0,MPFR_RNDD);
	mpfr_set_d(fourpt5,4.5,MPFR_RNDD);
	mpfr_set_d(NTadd,0.0,MPFR_RNDD);
	mpfr_set_d(NBadd,0.0,MPFR_RNDD);
	mpfr_set_d(STadd,0.0,MPFR_RNDD);
	mpfr_set_d(SBadd,0.0,MPFR_RNDD);
	mpfr_set_d(ETadd,0.0,MPFR_RNDD);
	mpfr_set_d(NEadd,0.0,MPFR_RNDD);

	mpfr_set_d(EBadd,0.0,MPFR_RNDD);
	mpfr_set_d(WTadd,0.0,MPFR_RNDD);
	mpfr_set_d(WBadd,0.0,MPFR_RNDD);
	mpfr_set_d(NWadd,0.0,MPFR_RNDD);
	mpfr_set_d(SEadd,0.0,MPFR_RNDD);
	mpfr_set_d(SWadd,0.0,MPFR_RNDD);
	mpfr_set_d(o,DFL2,MPFR_RNDD);
	mpfr_set_d(o2,DFL1,MPFR_RNDD);
	mpfr_set_d(o3,DFL3,MPFR_RNDD);
	mpfr_set_d(ux_neg,0.0,MPFR_RNDD);
	mpfr_set_d(uy_neg,0.0,MPFR_RNDD);
	mpfr_init2(src_N, config_vals[0]);
	mpfr_set_d(src_N,0.0,MPFR_RNDD);
	mpfr_init2(src_C, config_vals[0]);
	mpfr_set_d(src_C,0.0,MPFR_RNDD);
	mpfr_init2(src_S, config_vals[0]);
	mpfr_set_d(src_S,0.0,MPFR_RNDD);
	mpfr_init2(src_E, config_vals[0]);
	mpfr_set_d(src_E,0.0,MPFR_RNDD);
	mpfr_init2(src_W, config_vals[0]);
	mpfr_set_d(src_W,0.0,MPFR_RNDD);
	mpfr_init2(src_T, config_vals[0]);
	mpfr_set_d(src_T,0.0,MPFR_RNDD);
	mpfr_init2(src_B, config_vals[0]);
	mpfr_set_d(src_B,0.0,MPFR_RNDD);
	mpfr_init2(src_NE, config_vals[0]);
	mpfr_set_d(src_NE,0.0,MPFR_RNDD);
	mpfr_init2(src_NW, config_vals[0]);
	mpfr_set_d(src_NW,0.0,MPFR_RNDD);
	mpfr_init2(src_SE, config_vals[0]);
	mpfr_set_d(src_SE,0.0,MPFR_RNDD);
	mpfr_init2(src_SW, config_vals[0]);
	mpfr_set_d(src_SW,0.0,MPFR_RNDD);
	mpfr_init2(src_NT, config_vals[0]);
	mpfr_set_d(src_NT,0.0,MPFR_RNDD);
	mpfr_init2(strange, config_vals[0]);
	mpfr_set_d(strange,0.0,MPFR_RNDD);
	mpfr_init2(src_ST, config_vals[0]);
	mpfr_set_d(src_ST,0.0,MPFR_RNDD);
	mpfr_init2(src_SB, config_vals[0]);
	mpfr_set_d(src_SB,0.0,MPFR_RNDD);
	mpfr_init2(src_ET, config_vals[0]);
	mpfr_set_d(src_ET,0.0,MPFR_RNDD);
	mpfr_init2(src_EB, config_vals[0]);
	mpfr_set_d(src_EB,0.0,MPFR_RNDD);
	mpfr_init2(src_WT, config_vals[0]);
	mpfr_set_d(src_WT,0.0,MPFR_RNDD);
	mpfr_init2(src_WB, config_vals[0]);
	mpfr_set_d(src_WB,0.0,MPFR_RNDD);

	SWEEP_START( 0, 0, 0, 0, 0, SIZE_Z )
		
		if( TEST_FLAG_SWEEP( srcGrid, OBSTACLE )) {
			
		#if 0

		mpfr_set_d(c,SRC_C ( srcGrid ),MPFR_RNDD);
		DST_C ( dstGrid )= mpfr_get_d(c,MPFR_RNDD);

		mpfr_set_d(n,SRC_N ( srcGrid ),MPFR_RNDD);
                DST_S ( dstGrid )= mpfr_get_d(n,MPFR_RNDD);

		mpfr_set_d(s,SRC_S ( srcGrid ),MPFR_RNDD);
                DST_N ( dstGrid )= mpfr_get_d(s,MPFR_RNDD);

		mpfr_set_d(e,SRC_E ( srcGrid ),MPFR_RNDD);
                DST_W ( dstGrid )= mpfr_get_d(e,MPFR_RNDD);

		mpfr_set_d(w,SRC_W ( srcGrid ),MPFR_RNDD);
                DST_E ( dstGrid )= mpfr_get_d(w,MPFR_RNDD);

		mpfr_set_d(tt,SRC_T ( srcGrid ),MPFR_RNDD);
                DST_B ( dstGrid )= mpfr_get_d(tt,MPFR_RNDD);

		mpfr_set_d(b,SRC_B ( srcGrid ),MPFR_RNDD);
                DST_T ( dstGrid )= mpfr_get_d(b,MPFR_RNDD);

		mpfr_set_d(ne,SRC_NE ( srcGrid ),MPFR_RNDD);
                DST_SW ( dstGrid )= mpfr_get_d(ne,MPFR_RNDD);

		mpfr_set_d(nw,SRC_NW ( srcGrid ),MPFR_RNDD);
                DST_SE ( dstGrid )= mpfr_get_d(nw,MPFR_RNDD);

		mpfr_set_d(se,SRC_SE ( srcGrid ),MPFR_RNDD);
                DST_NW ( dstGrid )= mpfr_get_d(se,MPFR_RNDD);

		mpfr_set_d(sw,SRC_SW ( srcGrid ),MPFR_RNDD);
                DST_NE ( dstGrid )= mpfr_get_d(sw,MPFR_RNDD);

		mpfr_set_d(nt,SRC_NT ( srcGrid ),MPFR_RNDD);
                DST_SB ( dstGrid )= mpfr_get_d(nt,MPFR_RNDD);

		mpfr_set_d(nb,SRC_NB ( srcGrid ),MPFR_RNDD);
                DST_ST ( dstGrid )= mpfr_get_d(nb,MPFR_RNDD);

		mpfr_set_d(st,SRC_ST ( srcGrid ),MPFR_RNDD);
                DST_NB ( dstGrid )= mpfr_get_d(st,MPFR_RNDD);

		mpfr_set_d(sb,SRC_SB ( srcGrid ),MPFR_RNDD);
                DST_NT ( dstGrid )= mpfr_get_d(sb,MPFR_RNDD);

		mpfr_set_d(et,SRC_ET ( srcGrid ),MPFR_RNDD);
                DST_WB ( dstGrid )= mpfr_get_d(et,MPFR_RNDD);

		mpfr_set_d(eb,SRC_EB ( srcGrid ),MPFR_RNDD);
                DST_WT ( dstGrid )= mpfr_get_d(eb,MPFR_RNDD);

		mpfr_set_d(wt,SRC_WT ( srcGrid ),MPFR_RNDD);
                DST_EB ( dstGrid )= mpfr_get_d(wt,MPFR_RNDD);

		mpfr_set_d(wb,SRC_WB ( srcGrid ),MPFR_RNDD);
                DST_ET ( dstGrid )= mpfr_get_d(wb,MPFR_RNDD);
                
		#endif 
		DST_C ( dstGrid ) = SRC_C ( srcGrid );
			DST_S ( dstGrid ) = SRC_N ( srcGrid );
			DST_N ( dstGrid ) = SRC_S ( srcGrid );
			DST_W ( dstGrid ) = SRC_E ( srcGrid );
			DST_E ( dstGrid ) = SRC_W ( srcGrid );
			DST_B ( dstGrid ) = SRC_T ( srcGrid );
			DST_T ( dstGrid ) = SRC_B ( srcGrid );
			DST_SW( dstGrid ) = SRC_NE( srcGrid );
			DST_SE( dstGrid ) = SRC_NW( srcGrid );
			DST_NW( dstGrid ) = SRC_SE( srcGrid );
			DST_NE( dstGrid ) = SRC_SW( srcGrid );
			DST_SB( dstGrid ) = SRC_NT( srcGrid );
			DST_ST( dstGrid ) = SRC_NB( srcGrid );
			DST_NB( dstGrid ) = SRC_ST( srcGrid );
			DST_NT( dstGrid ) = SRC_SB( srcGrid );
			DST_WB( dstGrid ) = SRC_ET( srcGrid );
			DST_WT( dstGrid ) = SRC_EB( srcGrid );
			DST_EB( dstGrid ) = SRC_WT( srcGrid );
			DST_ET( dstGrid ) = SRC_WB( srcGrid );
			continue;
		}
//printf("SRC_W=%lf",SRC_W ( srcGrid ));
//printf("\n");
//printf("SRC_E=%lf",SRC_E ( srcGrid ));
//mpfr_out_str(0,10,0,DST_C ( dstGrid ),MPFR_RNDD);
                mpfr_set_d(rho,0.0,MPFR_RNDD);
		//mpfr_set_d(t,0.0,MPFR_RNDD);
		mpfr_set_d(t,SRC_C ( srcGrid ),MPFR_RNDN);
       		mpfr_add(rho,rho,t,MPFR_RNDN);
//printf("\n");       		
//mpfr_out_str(0,10,0,rho,MPFR_RNDD);
                mpfr_set_d(t,SRC_N ( srcGrid ),MPFR_RNDN);
                mpfr_add(rho,rho,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_S ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_E ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_W ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_T ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_B ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_NE ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_NW ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_SE ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_SW ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_NT ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_NB ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_ST ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_SB ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_ET ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_EB ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_WT ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);
		mpfr_set_d(t,SRC_WB ( srcGrid ),MPFR_RNDD);
                mpfr_add(rho,rho,t,MPFR_RNDD);

//mpfr_out_str(0,10,0,rho,MPFR_RNDD);
//printf("\n");

		mpfr_set_d(one,1.0,MPFR_RNDD);
		 mpfr_set_d(onept5,1.5,MPFR_RNDD);

/*	rho = + SRC_C ( srcGrid ) + SRC_N ( srcGrid )
		      + SRC_S ( srcGrid ) + SRC_E ( srcGrid )
		      + SRC_W ( srcGrid ) + SRC_T ( srcGrid )
		      + SRC_B ( srcGrid ) + SRC_NE( srcGrid )
		      + SRC_NW( srcGrid ) + SRC_SE( srcGrid )
		      + SRC_SW( srcGrid ) + SRC_NT( srcGrid )
		      + SRC_NB( srcGrid ) + SRC_ST( srcGrid )
		      + SRC_SB( srcGrid ) + SRC_ET( srcGrid )
		      + SRC_EB( srcGrid ) + SRC_WT( srcGrid )
		      + SRC_WB( srcGrid );*/
//printf("SRC_E=%lf",SRC_E ( srcGrid ));
//printf("\n");
//printf("SRC_W=%lf",SRC_W ( srcGrid ));
                mpfr_set_d(ux,0.0,MPFR_RNDD);
               // mpfr_set_d(t,SRC_E ( srcGrid ),MPFR_RNDU);
		mpfr_add_d(ux,ux,SRC_E ( srcGrid ),MPFR_RNDD);
//mpfr_out_str(0,10,0,ux,MPFR_RNDD);
//printf("\n");
	//	mpfr_set_d(t,SRC_W ( srcGrid ),MPFR_RNDD);
                mpfr_sub_d(ux,ux,SRC_W ( srcGrid ),MPFR_RNDD);
//mpfr_out_str(0,10,0,ux,MPFR_RNDD);
//printf("\n");
	//	mpfr_set_d(t,SRC_NE ( srcGrid ),MPFR_RNDD);
                mpfr_add_d(ux,ux,SRC_NE ( srcGrid ),MPFR_RNDD);
		//mpfr_set_d(t,SRC_NW ( srcGrid ),MPFR_RNDU);
                mpfr_sub_d(ux,ux,SRC_NW ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_SE ( srcGrid ),MPFR_RNDU);
                mpfr_add_d(ux,ux,SRC_SE ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_SW ( srcGrid ),MPFR_RNDU);
                mpfr_sub_d(ux,ux,SRC_SW ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_ET ( srcGrid ),MPFR_RNDU);
                mpfr_add_d(ux,ux,SRC_ET ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_EB ( srcGrid ),MPFR_RNDU);
                mpfr_add_d(ux,ux,SRC_EB ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_WT ( srcGrid ),MPFR_RNDU);
                mpfr_sub_d(ux,ux,SRC_WT ( srcGrid ),MPFR_RNDD);
	//	mpfr_set_d(t,SRC_WB ( srcGrid ),MPFR_RNDU);
                mpfr_sub_d(ux,ux,SRC_WB ( srcGrid ),MPFR_RNDD);
		
             
//mpfr_out_str(0,10,0,ux,MPFR_RNDD);
//printf("\n");
	/*	ux = + SRC_E ( srcGrid ) - SRC_W ( srcGrid )
		     + SRC_NE( srcGrid ) - SRC_NW( srcGrid )
		     + SRC_SE( srcGrid ) - SRC_SW( srcGrid )
		     + SRC_ET( srcGrid ) + SRC_EB( srcGrid )
		     - SRC_WT( srcGrid ) - SRC_WB( srcGrid );*/
		
		 mpfr_set_d(uy,0.0,MPFR_RNDD);
               
                mpfr_set_d(t,SRC_N ( srcGrid ),MPFR_RNDD);
                mpfr_add(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_S ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NE ( srcGrid ),MPFR_RNDD);
                mpfr_add(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NW ( srcGrid ),MPFR_RNDD);
                mpfr_add(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_SE ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_SW ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NT ( srcGrid ),MPFR_RNDD);
                mpfr_add(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NB ( srcGrid ),MPFR_RNDD);
                mpfr_add(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_ST ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uy,uy,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_SB ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uy,uy,t,MPFR_RNDD);


	/*	uy = + SRC_N ( srcGrid ) - SRC_S ( srcGrid )
		     + SRC_NE( srcGrid ) + SRC_NW( srcGrid )
		     - SRC_SE( srcGrid ) - SRC_SW( srcGrid )
		     + SRC_NT( srcGrid ) + SRC_NB( srcGrid )
		     - SRC_ST( srcGrid ) - SRC_SB( srcGrid );*/


mpfr_set_d(uz,0.0,MPFR_RNDD);
               
               // mpfr_init2(t, config_vals[0]);
                mpfr_set_d(t,SRC_T ( srcGrid ),MPFR_RNDD);
                mpfr_add(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_B ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NT ( srcGrid ),MPFR_RNDD);
                mpfr_add(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_NB ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_ST ( srcGrid ),MPFR_RNDD);
                mpfr_add(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_SB ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_ET ( srcGrid ),MPFR_RNDD);
                mpfr_add(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_EB ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_WT ( srcGrid ),MPFR_RNDD);
                mpfr_add(uz,uz,t,MPFR_RNDD);
                mpfr_set_d(t,SRC_WB ( srcGrid ),MPFR_RNDD);
                mpfr_sub(uz,uz,t,MPFR_RNDD);

/*	uz = + SRC_T ( srcGrid ) - SRC_B ( srcGrid )
		     + SRC_NT( srcGrid ) - SRC_NB( srcGrid )
		     + SRC_ST( srcGrid ) - SRC_SB( srcGrid )
		     + SRC_ET( srcGrid ) - SRC_EB( srcGrid )
		     + SRC_WT( srcGrid ) - SRC_WB( srcGrid );

	ux /= rho;
		uy /= rho;
		uz /= rho;*/

                mpfr_div(ux,ux,rho,MPFR_RNDD);
		mpfr_div(uy,uy,rho,MPFR_RNDD);
		mpfr_div(uz,uz,rho,MPFR_RNDD);

		if( TEST_FLAG_SWEEP( srcGrid, ACCEL )) {
		/*	ux = 0.005;
			uy = 0.002;
			uz = 0.000;*/
                        mpfr_set_d(ux,0.005,MPFR_RNDD);
                        mpfr_set_d(uy,0.002,MPFR_RNDD);
			mpfr_set_d(uz,0.000,MPFR_RNDD);
		}

//	u2 = 1.5 * (ux*ux + uy*uy + uz*uz);
	  mpfr_mul(p, ux, ux, MPFR_RNDD);
	  mpfr_mul(q, uy, uy, MPFR_RNDD);
	  mpfr_mul(r, uz, uz, MPFR_RNDD);
	  mpfr_add(u2, p, q, MPFR_RNDD);
	  mpfr_add(u2, u2, r, MPFR_RNDD);
	  mpfr_set_d(onept5,1.5,MPFR_RNDD);
	  mpfr_set_d(O,OMEGA,MPFR_RNDD);
	  mpfr_set_d(s1,SRC_C ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(ss2,SRC_N ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s3,SRC_S ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s4,SRC_E ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s5,SRC_W ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s6,SRC_T ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s7,SRC_B ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s8,SRC_NE ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s9,SRC_NW ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s10,SRC_SE ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s11,SRC_SW ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s12,SRC_NT ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s13,SRC_NB ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s14,SRC_ST ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s15,SRC_SB ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s16,SRC_ET ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s17,SRC_EB ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s18,SRC_WT ( srcGrid ),MPFR_RNDD);
	mpfr_set_d(s19,SRC_WB ( srcGrid ),MPFR_RNDD);

	  mpfr_mul(u2, onept5, u2, MPFR_RNDD);


   		mpfr_sub(t,one,O,MPFR_RNDD);
		mpfr_mul(t,t,s1,MPFR_RNDD);
		mpfr_sub(t2,one,u2,MPFR_RNDD);
		mpfr_mul(t1,rho,t2,MPFR_RNDD);
		mpfr_mul(t1,t1,O,MPFR_RNDD);
  		 mpfr_mul(t1,t1,o2,MPFR_RNDD);
		mpfr_add(t,t,t1,MPFR_RNDD);
		DST_C ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);
		

//	DST_C ( dstGrid ) = (1.0-OMEGA)*SRC_C ( srcGrid ) + DFL1*OMEGA*rho*(1.0                                 - u2);

                mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,ss2,MPFR_RNDD);
                mpfr_mul(fourpt5uy,uy,fourpt5,MPFR_RNDD);
                mpfr_add(src_N,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uy,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_N ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);

	//	DST_N ( dstGrid ) = (1.0-OMEGA)*SRC_N ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       uy*(4.5*uy       + 3.0) - u2);
	//	DST_S ( dstGrid ) = (1.0-OMEGA)*SRC_S ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       uy*(4.5*uy       - 3.0) - u2);
		
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s3,MPFR_RNDD);
                mpfr_mul(fourpt5uy,uy,fourpt5,MPFR_RNDD);
                mpfr_sub(src_N,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uy,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_S ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);		


//		DST_E ( dstGrid ) = (1.0-OMEGA)*SRC_E ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       ux*(4.5*ux       + 3.0) - u2);
		
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s4,MPFR_RNDD);
                mpfr_mul(fourpt5ux,ux,fourpt5,MPFR_RNDD);
                mpfr_add(src_N,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,ux,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_E ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);
		
	//	DST_W ( dstGrid ) = (1.0-OMEGA)*SRC_W ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       ux*(4.5*ux       - 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s5,MPFR_RNDD);
                mpfr_mul(fourpt5ux,ux,fourpt5,MPFR_RNDD);
                mpfr_sub(src_N,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,ux,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_W ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);


//		DST_T ( dstGrid ) = (1.0-OMEGA)*SRC_T ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       uz*(4.5*uz       + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s6,MPFR_RNDD);
                mpfr_mul(fourpt5uz,uz,fourpt5,MPFR_RNDD);
                mpfr_add(src_N,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uz,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_T ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);


//		DST_B ( dstGrid ) = (1.0-OMEGA)*SRC_B ( srcGrid ) + DFL2*OMEGA*rho*(1.0 +       uz*(4.5*uz       - 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s7,MPFR_RNDD);
                mpfr_mul(fourpt5uz,uz,fourpt5,MPFR_RNDD);
                mpfr_sub(src_N,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uz,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t3,src_N,o,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

               DST_B ( dstGrid )= mpfr_get_d(t,MPFR_RNDD);

//		DST_NE( dstGrid ) = (1.0-OMEGA)*SRC_NE( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+ux+uy)*(4.5*(+ux+uy) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s8,MPFR_RNDD);
		mpfr_add(NEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_NE,fourpt5,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,three,MPFR_RNDD);
                mpfr_mul(src_NE,src_NE,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,one,MPFR_RNDD);
                mpfr_sub(src_NE,src_NE,u2,MPFR_RNDD);
                mpfr_mul(src_NE,src_NE,rho,MPFR_RNDD);
                mpfr_mul(t3,src_NE,o3,MPFR_RNDD);
		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

                 DST_NE( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);
		
	//	DST_NW( dstGrid ) = (1.0-OMEGA)*SRC_NW( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-ux+uy)*(4.5*(-ux+uy) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s9,MPFR_RNDD);
		mpfr_sub(NWadd,uy,ux,MPFR_RNDD);
                mpfr_mul(src_NW,fourpt5,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,three,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,one,MPFR_RNDD);
                mpfr_sub(src_NW,src_NW,u2,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,rho,MPFR_RNDD);
                mpfr_mul(t3,src_NW,o3,MPFR_RNDD);
		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);

                 DST_NW( dstGrid )= mpfr_get_d (t,MPFR_RNDD);


//		DST_SE( dstGrid ) = (1.0-OMEGA)*SRC_SE( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+ux-uy)*(4.5*(+ux-uy) + 3.0) - u2);


		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s10,MPFR_RNDD);
		 mpfr_sub(SEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_SE,fourpt5,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,three,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,one,MPFR_RNDD);
                mpfr_sub(src_SE,src_SE,u2,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,rho,MPFR_RNDD);
                mpfr_mul(t3,src_SE,o3,MPFR_RNDD);
 		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_SE( dstGrid )= mpfr_get_d (t,MPFR_RNDD);


//		DST_SW( dstGrid ) = (1.0-OMEGA)*SRC_SW( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-ux-uy)*(4.5*(-ux-uy) + 3.0) - u2);
		
		 mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s11,MPFR_RNDD);
		mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(SWadd,ux_neg,uy,MPFR_RNDD);
                mpfr_mul(src_SW,fourpt5,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,three,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,one,MPFR_RNDD);
                mpfr_sub(src_SW,src_SW,u2,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,rho,MPFR_RNDD);
                mpfr_mul(t3,src_SW,o3,MPFR_RNDD);
                mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
		DST_SW( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);

//		DST_NT( dstGrid ) = (1.0-OMEGA)*SRC_NT( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+uy+uz)*(4.5*(+uy+uz) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s12,MPFR_RNDD);
		mpfr_add(NTadd,uy,uz,MPFR_RNDD);
                mpfr_mul(src_NT,fourpt5,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,three,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,one,MPFR_RNDD);
                mpfr_sub(src_NT,src_NT,u2,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,rho,MPFR_RNDD);
                mpfr_mul(t3,src_NT,o3,MPFR_RNDD);
		 mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_NT( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);

//		DST_NB( dstGrid ) = (1.0-OMEGA)*SRC_NB( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+uy-uz)*(4.5*(+uy-uz) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s13,MPFR_RNDD);
		mpfr_sub(NBadd,uy,uz,MPFR_RNDD);
		mpfr_mul(strange,fourpt5,NBadd,MPFR_RNDD);
                mpfr_add(strange,strange,three,MPFR_RNDD);
                mpfr_mul(strange,strange,NBadd,MPFR_RNDD);
                mpfr_add(strange,strange,one,MPFR_RNDD);
                mpfr_sub(strange,strange,u2,MPFR_RNDD);
                mpfr_mul(strange,strange,rho,MPFR_RNDD);
                mpfr_mul(t3,strange,o3,MPFR_RNDD);
                 mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
		DST_NB( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);

//	DST_ST( dstGrid ) = (1.0-OMEGA)*SRC_ST( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-uy+uz)*(4.5*(-uy+uz) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s14,MPFR_RNDD);
                mpfr_sub(STadd,uz,uy,MPFR_RNDD);
                mpfr_mul(src_ST,fourpt5,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,three,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,one,MPFR_RNDD);
                mpfr_sub(src_ST,src_ST,u2,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,rho,MPFR_RNDD);
                mpfr_mul(t3,src_ST,o3,MPFR_RNDD);
		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                DST_ST( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);

//		DST_SB( dstGrid ) = (1.0-OMEGA)*SRC_SB( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-uy-uz)*(4.5*(-uy-uz) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s15,MPFR_RNDD);

		mpfr_neg(uy_neg,uy,MPFR_RNDD);
                mpfr_sub(SBadd,uy_neg,uz,MPFR_RNDD);
                mpfr_mul(src_SB,fourpt5,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,three,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,one,MPFR_RNDD);
                mpfr_sub(src_SB,src_SB,u2,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,rho,MPFR_RNDD);
                mpfr_mul(t3,src_SB,o3,MPFR_RNDD);

		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                DST_SB( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);

//		DST_ET( dstGrid ) = (1.0-OMEGA)*SRC_ET( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+ux+uz)*(4.5*(+ux+uz) + 3.0) - u2);
		
		 mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s16,MPFR_RNDD);
		mpfr_add(ETadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_ET,fourpt5,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,three,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,one,MPFR_RNDD);
                mpfr_sub(src_ET,src_ET,u2,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,rho,MPFR_RNDD);
                mpfr_mul(t3,src_ET,o3,MPFR_RNDD);

		 mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_ET( dstGrid )= mpfr_get_d (t,MPFR_RNDD);

//		DST_EB( dstGrid ) = (1.0-OMEGA)*SRC_EB( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (+ux-uz)*(4.5*(+ux-uz) + 3.0) - u2);
		
		 mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s17,MPFR_RNDD);
		mpfr_sub(EBadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_EB,fourpt5,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,three,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,one,MPFR_RNDD);
                mpfr_sub(src_EB,src_EB,u2,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,rho,MPFR_RNDD);
                mpfr_mul(t3,src_EB,o3,MPFR_RNDD);
		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_EB( dstGrid ) = mpfr_get_d (t,MPFR_RNDD);
//		DST_WT( dstGrid ) = (1.0-OMEGA)*SRC_WT( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-ux+uz)*(4.5*(-ux+uz) + 3.0) - u2);
		 mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s18,MPFR_RNDD);
		mpfr_sub(WTadd,uz,ux,MPFR_RNDD);
                mpfr_mul(src_WT,fourpt5,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,three,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,one,MPFR_RNDD);
                mpfr_sub(src_WT,src_WT,u2,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,rho,MPFR_RNDD);
                mpfr_mul(t3,src_WT,o3,MPFR_RNDD);
  		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_WT( dstGrid )= mpfr_get_d (t,MPFR_RNDD);

//		DST_WB( dstGrid ) = (1.0-OMEGA)*SRC_WB( srcGrid ) + DFL3*OMEGA*rho*(1.0 + (-ux-uz)*(4.5*(-ux-uz) + 3.0) - u2);
		mpfr_sub(t,one,O,MPFR_RNDD);
                mpfr_mul(t,t,s19,MPFR_RNDD);
                mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(WBadd,ux_neg,uz,MPFR_RNDD);
                mpfr_mul(src_WB,fourpt5,WBadd,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,three,MPFR_RNDD);
                mpfr_mul(src_WB,src_WB,WBadd,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,one,MPFR_RNDD);
                mpfr_sub(src_WB,src_WB,u2,MPFR_RNDD);
                mpfr_mul(src_WB,src_WB,rho,MPFR_RNDD);
                mpfr_mul(t3,src_WB,o3,MPFR_RNDD);
		mpfr_mul(t3,t3,O,MPFR_RNDD);
                mpfr_add(t,t,t3,MPFR_RNDD);
                 DST_WB( dstGrid )= mpfr_get_d (t,MPFR_RNDD);
	
	SWEEP_END
}

/*##########################################################################*/

void LBM_handleInOutFlow( LBM_Grid srcGrid ) {
/*	double ux , uy , uz , rho ,
	       ux1, uy1, uz1, rho1,
	       ux2, uy2, uz2, rho2,
	       u2, px, py;*/
  mpfr_t rho;
  mpfr_init2(rho, config_vals[5]);
  mpfr_t rho1;
  mpfr_init2(rho1, config_vals[6]);
  mpfr_t rho2;
  mpfr_init2(rho2, config_vals[7]);
  mpfr_t px;
  mpfr_init2(px, config_vals[8]);
  mpfr_t py;
  mpfr_init2(py, config_vals[9]);
  mpfr_t ux;
  mpfr_init2(ux, config_vals[1]);
  mpfr_t uy;
  mpfr_init2(uy, config_vals[2]);
  mpfr_t uz;
  mpfr_init2(uz, config_vals[3]);
  mpfr_t u2;
  mpfr_init2(u2, config_vals[4]);
  mpfr_t a;
  mpfr_init2(a, config_vals[0]);
  mpfr_t b;
  mpfr_init2(b, config_vals[0]);
  mpfr_t ux1,ux2,uy1,uy2,uz1,uz2;
  mpfr_init2(ux1, config_vals[0]);
  mpfr_init2(ux2, config_vals[0]);
  mpfr_init2(uy1, config_vals[0]);
  mpfr_init2(uy2, config_vals[0]);
  mpfr_init2(uz1, config_vals[0]);
  mpfr_init2(uz2, config_vals[0]);
  mpfr_t z,p,q,r;
  mpfr_init2(z,config_vals[0]);
  mpfr_t one,pt5,pt01,onept5;
  mpfr_init2(pt5, config_vals[0]);
  mpfr_init2(one, config_vals[0]);
  mpfr_init2(onept5, config_vals[0]);
  mpfr_init2(pt01, config_vals[0]);
  mpfr_init2(p,config_vals[0]);
  mpfr_init2(q,config_vals[0]);
  mpfr_init2(r,config_vals[0]);
  int index1;
 mpfr_t xx_1,o,o2,o3,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,three,fourpt5,fourpt5uy,fourpt5ux,fourpt5uz,src_N,src_C,src_S,src_E,src_W,src_T,src_B,src_NE,src_NW,src_SE,src_SW,src_NT,src_NB,src_ST,src_SB,src_ET,src_EB,src_WT,src_WB,NEadd,NWadd,SEadd,SWadd,NTadd,NBadd,STadd,SBadd,ETadd,EBadd,WTadd,WBadd,ux_neg,uy_neg,uz_neg;
 mpfr_init2(o3, config_vals[0]);
 mpfr_init2(o, config_vals[0]);
 mpfr_init2(o2, config_vals[0]);
 mpfr_init2(t1, config_vals[0]);
 mpfr_init2(t2, config_vals[0]);
 mpfr_init2(t3, config_vals[0]);
 mpfr_init2(t4, config_vals[0]);
 mpfr_init2(t5, config_vals[0]);
 mpfr_init2(t6, config_vals[0]);
 mpfr_init2(t7, config_vals[0]);
 mpfr_init2(t8, config_vals[0]);
 mpfr_init2(t9, config_vals[0]);
 mpfr_init2(t10, config_vals[0]);
 mpfr_init2(t11, config_vals[0]);
 mpfr_init2(t12, config_vals[0]);
 mpfr_init2(t13, config_vals[0]);
 mpfr_init2(t14, config_vals[0]);
 mpfr_init2(t15, config_vals[0]);
 mpfr_init2(t16, config_vals[0]);
 mpfr_init2(t17, config_vals[0]);
 mpfr_init2(t18, config_vals[0]);
 mpfr_init2(t19, config_vals[0]);
 mpfr_init2(three, config_vals[0]);
 mpfr_init2(fourpt5, config_vals[0]);
 mpfr_init2(fourpt5uy, config_vals[0]);
 mpfr_init2(fourpt5uz, config_vals[0]);
 mpfr_init2(fourpt5ux, config_vals[0]);
 mpfr_set_d(fourpt5ux,0.0,MPFR_RNDD);
 mpfr_set_d(fourpt5uy,0.0,MPFR_RNDD);
 mpfr_set_d(fourpt5uz,0.0,MPFR_RNDD);
 mpfr_set_d(three,3.0,MPFR_RNDD);
 mpfr_set_d(fourpt5,4.5,MPFR_RNDD);
 mpfr_set_d(o,DFL2,MPFR_RNDD);
 mpfr_set_d(o2,DFL1,MPFR_RNDD);
 mpfr_set_d(o3,DFL3,MPFR_RNDD);
	SWEEP_VAR

	/* inflow */
	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( ux, uy, uz, rho, ux1, uy1, uz1, rho1, \
                                  ux2, uy2, uz2, rho2, u2, px, py )
#endif
#endif
	SWEEP_START( 0, 0, 0, 0, 0, 1 )
        mpfr_set_d(rho1,0.0,MPFR_RNDD);
         for(index1=0;index1<19;index1++)
        {
         mpfr_set_d(z,GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, index1  ),MPFR_RNDD);
         mpfr_add(rho1,rho1,z,MPFR_RNDD);
        }

/*	rho1 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, C  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, N  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, S  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, E  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, W  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, T  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, B  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, NE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, NW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, SE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, SW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, NT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, NB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, ST )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, SB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, ET )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, EB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, WT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 1, WB );*/
        mpfr_set_d(rho2,0.0,MPFR_RNDD);
        for(index1=0;index1<19;index1++)
        {
         mpfr_set_d(z,GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, index1  ),MPFR_RNDD);
         mpfr_add(rho2,rho2,z,MPFR_RNDD);
        }
	/*	rho2 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, C  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, N  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, S  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, E  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, W  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, T  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, B  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, NE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, NW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, SE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, SW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, NT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, NB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, ST )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, SB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, ET )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, EB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, WT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, 2, WB );*/
  //      mpfr_set_d(rho,0.0,MPFR_RNDD);
	/*	rho = 2.0*rho1 - rho2;

		px = (SWEEP_X / (0.5*(SIZE_X-1))) - 1.0;
		py = (SWEEP_Y / (0.5*(SIZE_Y-1))) - 1.0;
		ux = 0.00;
		uy = 0.00;
		uz = 0.01 * (1.0-px*px) * (1.0-py*py);

		u2 = 1.5 * (ux*ux + uy*uy + uz*uz);*/
  mpfr_set_d(rho,2.0,MPFR_RNDD);
  mpfr_mul(rho, rho, rho1, MPFR_RNDD);
  mpfr_sub(rho, rho, rho2, MPFR_RNDD);
  mpfr_set_d(one,1.0,MPFR_RNDD);
  mpfr_set_d(z,SIZE_X,MPFR_RNDD);
  mpfr_sub(px,z, one, MPFR_RNDD);
  mpfr_set_d(pt5,0.5,MPFR_RNDD);
  mpfr_mul(px, pt5, px, MPFR_RNDD);
  mpfr_set_d(z,SWEEP_X,MPFR_RNDD);
  mpfr_div(px, z, px, MPFR_RNDD);
  mpfr_sub(px, px, one, MPFR_RNDD);
  mpfr_set_d(z,SIZE_Y,MPFR_RNDD);
  mpfr_sub(py,z, one, MPFR_RNDD);
  mpfr_mul(py, pt5, py, MPFR_RNDD);
  mpfr_set_d(z,SWEEP_Y,MPFR_RNDD);
  mpfr_div(py,z, py, MPFR_RNDD);
  mpfr_sub(py, py, one, MPFR_RNDD);
  mpfr_set_d(ux, 0.00, MPFR_RNDD);
  mpfr_set_d(uy, 0.00, MPFR_RNDD);
  mpfr_mul(a, px, px, MPFR_RNDD);
  mpfr_mul(b, py, py, MPFR_RNDD);
  mpfr_sub(a, one, a, MPFR_RNDD);
  mpfr_sub(b, one, b, MPFR_RNDD);
  mpfr_init2(src_N, config_vals[0]);
  mpfr_set_d(src_N,0.0,MPFR_RNDD);
  mpfr_init2(src_C, config_vals[0]);
  mpfr_set_d(src_C,0.0,MPFR_RNDD);
mpfr_init2(src_S, config_vals[0]);
mpfr_init2(NTadd, config_vals[0]);
mpfr_init2(NBadd, config_vals[0]);
mpfr_init2(STadd, config_vals[0]);
mpfr_init2(SBadd, config_vals[0]);
mpfr_init2(ETadd, config_vals[0]);
mpfr_init2(NEadd, config_vals[0]);
mpfr_init2(SWadd, config_vals[0]);
mpfr_init2(EBadd, config_vals[0]); 
mpfr_init2(WTadd, config_vals[0]);
mpfr_init2(WBadd, config_vals[0]);
//mpfr_init2(EBadd, config_vals[0]);

mpfr_init2(NTadd, config_vals[0]);
mpfr_init2(NBadd, config_vals[0]);
mpfr_init2(STadd, config_vals[0]);
//mpfr_init2(SBadd, config_vals[0]); 
//mpfr_init2(ETadd, config_vals[0]);

mpfr_set_d(NTadd,0.0,MPFR_RNDD);
mpfr_set_d(NBadd,0.0,MPFR_RNDD);
mpfr_set_d(STadd,0.0,MPFR_RNDD);
mpfr_set_d(SBadd,0.0,MPFR_RNDD);
mpfr_set_d(ETadd,0.0,MPFR_RNDD);
mpfr_set_d(NEadd,0.0,MPFR_RNDD);

mpfr_set_d(EBadd,0.0,MPFR_RNDD);
mpfr_set_d(WTadd,0.0,MPFR_RNDD);
mpfr_set_d(WBadd,0.0,MPFR_RNDD);
mpfr_set_d(NTadd,0.0,MPFR_RNDD);
mpfr_set_d(NBadd,0.0,MPFR_RNDD);
mpfr_set_d(STadd,0.0,MPFR_RNDD);
mpfr_set_d(SWadd,0.0,MPFR_RNDD);
mpfr_set_d(src_S,0.0,MPFR_RNDD);
mpfr_init2(src_E, config_vals[0]);
mpfr_set_d(src_E,0.0,MPFR_RNDD);
mpfr_init2(src_W, config_vals[0]);
mpfr_set_d(src_W,0.0,MPFR_RNDD);
mpfr_init2(src_T, config_vals[0]);
mpfr_set_d(src_T,0.0,MPFR_RNDD);
mpfr_init2(src_B, config_vals[0]);
mpfr_set_d(src_B,0.0,MPFR_RNDD);
mpfr_init2(src_NE, config_vals[0]);
mpfr_set_d(src_NE,0.0,MPFR_RNDD);
mpfr_init2(src_NW, config_vals[0]);
mpfr_set_d(src_NW,0.0,MPFR_RNDD);
mpfr_init2(src_SE, config_vals[0]);
mpfr_set_d(src_SE,0.0,MPFR_RNDD);
mpfr_init2(src_SW, config_vals[0]);
mpfr_set_d(src_SW,0.0,MPFR_RNDD);
mpfr_init2(src_NT, config_vals[0]);
mpfr_set_d(src_NT,0.0,MPFR_RNDD);
mpfr_init2(src_NB, config_vals[0]);
mpfr_set_d(src_NB,0.0,MPFR_RNDD);
mpfr_init2(src_ST, config_vals[0]);
mpfr_set_d(src_ST,0.0,MPFR_RNDD);
mpfr_init2(src_SB, config_vals[0]);
mpfr_set_d(src_SB,0.0,MPFR_RNDD);
mpfr_init2(src_ET, config_vals[0]);
mpfr_set_d(src_ET,0.0,MPFR_RNDD);
mpfr_init2(src_EB, config_vals[0]);
mpfr_set_d(src_EB,0.0,MPFR_RNDD);
mpfr_init2(src_WT, config_vals[0]);
mpfr_set_d(src_WT,0.0,MPFR_RNDD);
mpfr_init2(src_WB, config_vals[0]);
mpfr_set_d(src_WB,0.0,MPFR_RNDD);
mpfr_init2(ux_neg, config_vals[0]);
mpfr_set_d(ux_neg,0.0,MPFR_RNDD);
mpfr_init2(uy_neg, config_vals[0]);
mpfr_set_d(uy_neg,0.0,MPFR_RNDD);
mpfr_init2(uz_neg, config_vals[0]);
mpfr_set_d(uz_neg,0.0,MPFR_RNDD);

mpfr_init2(xx_1, config_vals[0]);
mpfr_mul(xx_1, a, b, MPFR_RNDD);
mpfr_set_d(pt01,0.01,MPFR_RNDD);
mpfr_mul(uz, pt01, xx_1, MPFR_RNDD);
mpfr_mul(p, ux, ux, MPFR_RNDD);
mpfr_mul(q, uy, uy, MPFR_RNDD);
mpfr_mul(r, uz, uz, MPFR_RNDD);
mpfr_add(u2, p, q, MPFR_RNDD);
mpfr_add(u2, u2, r, MPFR_RNDD);
mpfr_set_d(onept5,1.5,MPFR_RNDD);
mpfr_mul(u2, onept5, u2, MPFR_RNDD);
//local(N)
		mpfr_set_d(t2,LOCAL( srcGrid, N ),MPFR_RNDD);
                
                mpfr_mul(fourpt5uy,uy,fourpt5,MPFR_RNDD);
                mpfr_add(src_N,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uy,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t2,src_N,o,MPFR_RNDD);
		LOCAL( srcGrid, N )= mpfr_get_d (t2,MPFR_RNDD);
                        
         //local(C)
                mpfr_set_d(t1,LOCAL( srcGrid, C ),MPFR_RNDD);
                
                mpfr_sub(src_C,one,u2,MPFR_RNDD);
                mpfr_mul(src_C,src_C,rho,MPFR_RNDD);
                mpfr_mul(t1,src_C,o2,MPFR_RNDD);
		 LOCAL( srcGrid, C )= mpfr_get_d (t1,MPFR_RNDD);
         //local(S)                     

	//	LOCAL( srcGrid, N ) = DFL2*rho*(1.0 +       uy*(4.5*uy       + 3.0) - u2);
	//	LOCAL( srcGrid, S ) = DFL2*rho*(1.0 +       uy*(4.5*uy       - 3.0) - u2);
	        mpfr_set_d(t3,LOCAL( srcGrid, S ),MPFR_RNDD);
                               
                mpfr_sub(src_S,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_S,src_S,uy,MPFR_RNDD);
                mpfr_sub(src_S,src_S,u2,MPFR_RNDD);
                mpfr_add(src_S,src_S,one,MPFR_RNDD);
                mpfr_mul(src_S,src_S,rho,MPFR_RNDD);
                mpfr_mul(t3,src_S,o,MPFR_RNDD);
		LOCAL( srcGrid, S )= mpfr_get_d (t3,MPFR_RNDD);

	//	LOCAL( srcGrid, E ) = DFL2*rho*(1.0 +       ux*(4.5*ux       + 3.0) - u2);
	        mpfr_set_d(t4,LOCAL( srcGrid, E ),MPFR_RNDD);
                mpfr_mul(fourpt5ux,ux,fourpt5,MPFR_RNDD);
                mpfr_add(src_E,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_E,src_E,ux,MPFR_RNDD);
                mpfr_sub(src_E,src_E,u2,MPFR_RNDD);
                mpfr_add(src_E,src_E,one,MPFR_RNDD);
                mpfr_mul(src_E,src_E,rho,MPFR_RNDD);
                mpfr_mul(t4,src_E,o,MPFR_RNDD);
		LOCAL( srcGrid, E )= mpfr_get_d (t4,MPFR_RNDD);

		//LOCAL( srcGrid, W ) = DFL2*rho*(1.0 +       ux*(4.5*ux       - 3.0) - u2);
		mpfr_set_d(t5,LOCAL( srcGrid, W ),MPFR_RNDD);

                mpfr_sub(src_W,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_W,src_W,ux,MPFR_RNDD);
                mpfr_sub(src_W,src_W,u2,MPFR_RNDD);
                mpfr_add(src_W,src_W,one,MPFR_RNDD);
                mpfr_mul(src_W,src_W,rho,MPFR_RNDD);
                mpfr_mul(t5,src_W,o,MPFR_RNDD);
		 LOCAL( srcGrid, W )= mpfr_get_d (t5,MPFR_RNDD);

		//LOCAL( srcGrid, T ) = DFL2*rho*(1.0 +       uz*(4.5*uz       + 3.0) - u2);
		 mpfr_set_d(t6,LOCAL( srcGrid, T ),MPFR_RNDD);
                mpfr_mul(fourpt5uz,uz,fourpt5,MPFR_RNDD);
                mpfr_add(src_T,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_T,src_T,uz,MPFR_RNDD);
                mpfr_sub(src_T,src_T,u2,MPFR_RNDD);
                mpfr_add(src_T,src_T,one,MPFR_RNDD);
                mpfr_mul(src_T,src_T,rho,MPFR_RNDD);
                mpfr_mul(t6,src_T,o,MPFR_RNDD);
		 LOCAL( srcGrid, T )= mpfr_get_d (t6,MPFR_RNDD);
	//	LOCAL( srcGrid, B ) = DFL2*rho*(1.0 +       uz*(4.5*uz       - 3.0) - u2);
		mpfr_set_d(t7,LOCAL( srcGrid, B ),MPFR_RNDD);
                
                mpfr_sub(src_B,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_B,src_B,uz,MPFR_RNDD);
                mpfr_sub(src_B,src_B,u2,MPFR_RNDD);
                mpfr_add(src_B,src_B,one,MPFR_RNDD);
                mpfr_mul(src_B,src_B,rho,MPFR_RNDD);
                mpfr_mul(t7,src_B,o,MPFR_RNDD);
		 LOCAL( srcGrid, B )= mpfr_get_d (t7,MPFR_RNDD);

	//	LOCAL( srcGrid, NE) = DFL3*rho*(1.0 + (+ux+uy)*(4.5*(+ux+uy) + 3.0) - u2);
                mpfr_set_d(t8,LOCAL( srcGrid, NE ),MPFR_RNDD);
                
                mpfr_add(NEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_NE,fourpt5,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,three,MPFR_RNDD);
                mpfr_mul(src_NE,src_NE,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,one,MPFR_RNDD);
		mpfr_sub(src_NE,src_NE,u2,MPFR_RNDD);
		mpfr_mul(src_NE,src_NE,rho,MPFR_RNDD);
                mpfr_mul(t8,src_NE,o3,MPFR_RNDD);
                 LOCAL( srcGrid, NE )= mpfr_get_d (t8,MPFR_RNDD);
	//	LOCAL( srcGrid, NW) = DFL3*rho*(1.0 + (-ux+uy)*(4.5*(-ux+uy) + 3.0) - u2);
	        mpfr_set_d(t9,LOCAL( srcGrid, NW ),MPFR_RNDD);
                
                mpfr_sub(NWadd,uy,ux,MPFR_RNDD);
                mpfr_mul(src_NW,fourpt5,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,three,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,one,MPFR_RNDD);
                mpfr_sub(src_NW,src_NW,u2,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,rho,MPFR_RNDD);
                mpfr_mul(t9,src_NW,o3,MPFR_RNDD);
		 LOCAL( srcGrid, NW )= mpfr_get_d (t9,MPFR_RNDD);

	//	LOCAL( srcGrid, SE) = DFL3*rho*(1.0 + (+ux-uy)*(4.5*(+ux-uy) + 3.0) - u2);
		mpfr_set_d(t10,LOCAL( srcGrid, SE ),MPFR_RNDD);

                mpfr_sub(SEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_SE,fourpt5,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,three,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,one,MPFR_RNDD);
                mpfr_sub(src_SE,src_SE,u2,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,rho,MPFR_RNDD);
                mpfr_mul(t10,src_SE,o3,MPFR_RNDD);
		 LOCAL( srcGrid, SE )= mpfr_get_d (t10,MPFR_RNDD);

               // LOCAL( srcGrid, SW) = DFL3*rho*(1.0 + (-ux-uy)*(4.5*(-ux-uy) + 3.0) - u2);
                mpfr_set_d(t11,LOCAL( srcGrid, SW ),MPFR_RNDD);

                mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(SWadd,ux_neg,uy,MPFR_RNDD);
                mpfr_mul(src_SW,fourpt5,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,three,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,one,MPFR_RNDD);
                mpfr_sub(src_SW,src_SW,u2,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,rho,MPFR_RNDD);
                mpfr_mul(t11,src_SW,o3,MPFR_RNDD);
		 LOCAL( srcGrid, SW )= mpfr_get_d (t11,MPFR_RNDD);

		//LOCAL( srcGrid, NT) = DFL3*rho*(1.0 + (+uy+uz)*(4.5*(+uy+uz) + 3.0) - u2);
		mpfr_set_d(t12,LOCAL( srcGrid, NT ),MPFR_RNDD);

                mpfr_add(NTadd,uy,uz,MPFR_RNDD);
                mpfr_mul(src_NT,fourpt5,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,three,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,one,MPFR_RNDD);
                mpfr_sub(src_NT,src_NT,u2,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,rho,MPFR_RNDD);
                mpfr_mul(t12,src_NT,o3,MPFR_RNDD);
		 LOCAL( srcGrid, NT )= mpfr_get_d (t12,MPFR_RNDD);

	//	LOCAL( srcGrid, NB) = DFL3*rho*(1.0 + (+uy-uz)*(4.5*(+uy-uz) + 3.0) - u2);
	        mpfr_set_d(t13,LOCAL( srcGrid, NB ),MPFR_RNDD);

                mpfr_sub(NBadd,uy,uz,MPFR_RNDD);
                mpfr_mul(src_NB,fourpt5,NBadd,MPFR_RNDD);
                mpfr_add(src_NB,src_NB,three,MPFR_RNDD);
                mpfr_mul(src_NB,src_NB,NBadd,MPFR_RNDD);
                mpfr_add(src_NB,src_NB,one,MPFR_RNDD);
                mpfr_sub(src_NB,src_NB,u2,MPFR_RNDD);
                mpfr_mul(src_NB,src_NB,rho,MPFR_RNDD);
                mpfr_mul(t13,src_NB,o3,MPFR_RNDD);
		 LOCAL( srcGrid, NB )= mpfr_get_d (t13,MPFR_RNDD);
// 		LOCAL( srcGrid, ST) = DFL3*rho*(1.0 + (-uy+uz)*(4.5*(-uy+uz) + 3.0) - u2);
		mpfr_set_d(t14,LOCAL( srcGrid, ST ),MPFR_RNDD);

                mpfr_sub(STadd,uz,uy,MPFR_RNDD);
                mpfr_mul(src_ST,fourpt5,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,three,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,one,MPFR_RNDD);
                mpfr_sub(src_ST,src_ST,u2,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,rho,MPFR_RNDD);
                mpfr_mul(t14,src_ST,o3,MPFR_RNDD);
		 LOCAL( srcGrid, ST )= mpfr_get_d (t14,MPFR_RNDD);

//		LOCAL( srcGrid, SB) = DFL3*rho*(1.0 + (-uy-uz)*(4.5*(-uy-uz) + 3.0) - u2);
		mpfr_set_d(t15,LOCAL( srcGrid, SB ),MPFR_RNDD);

                mpfr_neg(uy_neg,uy,MPFR_RNDD);
                mpfr_sub(SBadd,uy_neg,uz,MPFR_RNDD);
                mpfr_mul(src_SB,fourpt5,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,three,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,one,MPFR_RNDD);
                mpfr_sub(src_SB,src_SB,u2,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,rho,MPFR_RNDD);
                mpfr_mul(t15,src_SB,o3,MPFR_RNDD);
		 LOCAL( srcGrid, SB )= mpfr_get_d (t15,MPFR_RNDD);
	//	LOCAL( srcGrid, ET) = DFL3*rho*(1.0 + (+ux+uz)*(4.5*(+ux+uz) + 3.0) - u2);
	        mpfr_set_d(t16,LOCAL( srcGrid, ET ),MPFR_RNDD);

                mpfr_add(ETadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_ET,fourpt5,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,three,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,one,MPFR_RNDD);
                mpfr_sub(src_ET,src_ET,u2,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,rho,MPFR_RNDD);
                mpfr_mul(t16,src_ET,o3,MPFR_RNDD);
		 LOCAL( srcGrid, ET )= mpfr_get_d (t16,MPFR_RNDD);

	//	LOCAL( srcGrid, EB) = DFL3*rho*(1.0 + (+ux-uz)*(4.5*(+ux-uz) + 3.0) - u2);
	        mpfr_set_d(t17,LOCAL( srcGrid, EB ),MPFR_RNDD);

                mpfr_sub(EBadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_EB,fourpt5,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,three,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,one,MPFR_RNDD);
                mpfr_sub(src_EB,src_EB,u2,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,rho,MPFR_RNDD);
                mpfr_mul(t17,src_EB,o3,MPFR_RNDD);
		 LOCAL( srcGrid, EB )= mpfr_get_d (t17,MPFR_RNDD);

	//	LOCAL( srcGrid, WT) = DFL3*rho*(1.0 + (-ux+uz)*(4.5*(-ux+uz) + 3.0) - u2);
		mpfr_set_d(t18,LOCAL( srcGrid, WT ),MPFR_RNDD);

                mpfr_sub(WTadd,uz,ux,MPFR_RNDD);
                mpfr_mul(src_WT,fourpt5,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,three,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,one,MPFR_RNDD);
                mpfr_sub(src_WT,src_WT,u2,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,rho,MPFR_RNDD);
                mpfr_mul(t18,src_WT,o3,MPFR_RNDD);
		 LOCAL( srcGrid, WT )= mpfr_get_d (t18,MPFR_RNDD);

	//	LOCAL( srcGrid, WB) = DFL3*rho*(1.0 + (-ux-uz)*(4.5*(-ux-uz) + 3.0) - u2);
		mpfr_set_d(t19,LOCAL( srcGrid, WB ),MPFR_RNDD);

                mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(WBadd,ux_neg,uz,MPFR_RNDD);
                mpfr_mul(src_WB,fourpt5,WBadd,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,three,MPFR_RNDD);
                mpfr_mul(src_WB,src_WB,WBadd,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,one,MPFR_RNDD);
                mpfr_sub(src_WB,src_WB,u2,MPFR_RNDD);
                mpfr_mul(src_WB,src_WB,rho,MPFR_RNDD);
                mpfr_mul(t19,src_WB,o3,MPFR_RNDD);
 		 LOCAL( srcGrid, WB )= mpfr_get_d (t19,MPFR_RNDD);
	SWEEP_END

	/* outflow */
	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( ux, uy, uz, rho, ux1, uy1, uz1, rho1, \
                                  ux2, uy2, uz2, rho2, u2, px, py )
#endif
#endif
int index;
	SWEEP_START( 0, 0, SIZE_Z-1, 0, 0, SIZE_Z )
		mpfr_set_d(rho1,0.0,MPFR_RNDD);		
		for(index=0;index<19;index++)
                   {
			mpfr_set_d(z,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, index  ),MPFR_RNDD);
			mpfr_add(rho1,rho1,z,MPFR_RNDD);
                   }
		/*rho1 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, C  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, N  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, S  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, E  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, W  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, T  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, B  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ST )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ET )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, EB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WB );*/
	
		/*ux1 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, E  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, W  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ET ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, EB )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WB );*/

                  mpfr_t e;
mpfr_init2(NTadd, config_vals[0]);
mpfr_init2(NBadd, config_vals[0]);
mpfr_init2(SBadd, config_vals[0]);
mpfr_init2(ETadd, config_vals[0]);
mpfr_init2(NEadd, config_vals[0]);
mpfr_init2(EBadd, config_vals[0]);
mpfr_init2(WTadd, config_vals[0]);
mpfr_init2(WBadd, config_vals[0]);
mpfr_init2(SWadd, config_vals[0]);
mpfr_init2(NWadd, config_vals[0]);
mpfr_init2(NTadd, config_vals[0]);
mpfr_init2(NBadd, config_vals[0]);
mpfr_init2(STadd, config_vals[0]);
mpfr_init2(SEadd, config_vals[0]);

mpfr_set_d(NTadd,0.0,MPFR_RNDD);
mpfr_set_d(NBadd,0.0,MPFR_RNDD);
mpfr_set_d(STadd,0.0,MPFR_RNDD);
mpfr_set_d(SBadd,0.0,MPFR_RNDD);
mpfr_set_d(ETadd,0.0,MPFR_RNDD);
mpfr_set_d(NEadd,0.0,MPFR_RNDD);
mpfr_set_d(SWadd,0.0,MPFR_RNDD);
mpfr_set_d(NWadd,0.0,MPFR_RNDD);

mpfr_set_d(EBadd,0.0,MPFR_RNDD);
mpfr_set_d(WTadd,0.0,MPFR_RNDD);
mpfr_set_d(WBadd,0.0,MPFR_RNDD);
mpfr_set_d(NTadd,0.0,MPFR_RNDD);
mpfr_set_d(NBadd,0.0,MPFR_RNDD);
mpfr_set_d(STadd,0.0,MPFR_RNDD);
mpfr_set_d(SEadd,0.0,MPFR_RNDD);
  mpfr_init2(e, config_vals[0]);
 mpfr_set_d(ux1,0.0,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, E  ),MPFR_RNDD);
mpfr_add(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, W  ),MPFR_RNDD);
mpfr_sub(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NE ),MPFR_RNDD);
mpfr_add(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NW ),MPFR_RNDD);
mpfr_sub(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SE ),MPFR_RNDD);
mpfr_add(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SW ),MPFR_RNDD);
mpfr_sub(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ET ),MPFR_RNDD);
mpfr_add(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, EB ),MPFR_RNDD);
mpfr_add(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WT ),MPFR_RNDD);
mpfr_sub(ux1,ux1,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WB ),MPFR_RNDD);
mpfr_sub(ux1,ux1,e,MPFR_RNDD);
	/*	uy1 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, N  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, S  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NE ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NW )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NT ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NB )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ST ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SB );*/
mpfr_t n;
  mpfr_init2(n, config_vals[0]);
 mpfr_set_d(uy1,0.0,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, N  ),MPFR_RNDD);
mpfr_add(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, S  ),MPFR_RNDD);
mpfr_sub(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NE  ),MPFR_RNDD);
mpfr_add(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NW  ),MPFR_RNDD);
mpfr_add(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SE  ),MPFR_RNDD);
mpfr_sub(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SW  ),MPFR_RNDD);
mpfr_sub(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NT  ),MPFR_RNDD);
mpfr_add(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NB  ),MPFR_RNDD);
mpfr_add(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ST  ),MPFR_RNDD);
mpfr_sub(uy1,uy1,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SB  ),MPFR_RNDD);
mpfr_sub(uy1,uy1,n,MPFR_RNDD);

		/*uz1 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, T  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, B  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ST ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ET ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, EB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WB );*/
mpfr_t q;
mpfr_init2(q, config_vals[0]);
mpfr_set_d(uz1,0.0,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, T  ),MPFR_RNDD);
mpfr_add(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, B  ),MPFR_RNDD);
mpfr_sub(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NT  ),MPFR_RNDD);
mpfr_add(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, NB  ),MPFR_RNDD);
mpfr_sub(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ST  ),MPFR_RNDD);
mpfr_add(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, SB  ),MPFR_RNDD);
mpfr_sub(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, ET  ),MPFR_RNDD);
mpfr_add(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, EB  ),MPFR_RNDD);
mpfr_sub(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WT  ),MPFR_RNDD);
mpfr_add(uz1,uz1,q,MPFR_RNDD);
 mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -1, WB  ),MPFR_RNDD);
mpfr_sub(uz1,uz1,q,MPFR_RNDD);

	//	ux1 /= rho1;
mpfr_div(ux1,ux1,rho1,MPFR_RNDD);
mpfr_div(uy1,uy1,rho1,MPFR_RNDD);
mpfr_div(uz1,uz1,rho1,MPFR_RNDD);	
//	uy1 /= rho1;
//		uz1 /= rho1;
 mpfr_set_d(rho2,0.0,MPFR_RNDD);
                for(index=0;index<19;index++)
                   {
                        mpfr_set_d(z,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, index  ),MPFR_RNDD);
                        mpfr_add(rho2,rho2,z,MPFR_RNDD);
                   }
	/*	rho2 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, C  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, N  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, S  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, E  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, W  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, T  )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, B  ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SE )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SW ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ST )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ET )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, EB ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WT )
		       + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WB );*/
/*		ux2 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, E  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, W  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ET ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, EB )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WB );*/
mpfr_set_d(ux2,0.0,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, E  ),MPFR_RNDD);
mpfr_add(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, W  ),MPFR_RNDD);
mpfr_sub(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NE ),MPFR_RNDD);
mpfr_add(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NW ),MPFR_RNDD);
mpfr_sub(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SE ),MPFR_RNDD);
mpfr_add(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SW ),MPFR_RNDD);
mpfr_sub(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ET ),MPFR_RNDD);
mpfr_add(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, EB ),MPFR_RNDD);
mpfr_add(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WT ),MPFR_RNDD);
mpfr_sub(ux2,ux2,e,MPFR_RNDD);
mpfr_set_d(e,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WB ),MPFR_RNDD);
mpfr_sub(ux2,ux2,e,MPFR_RNDD);
	/*	uy2 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, N  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, S  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NE ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NW )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SE ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SW )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NT ) + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NB )
		      - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ST ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SB );*/
mpfr_set_d(uy2,0.0,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, N  ),MPFR_RNDD);
mpfr_add(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, S  ),MPFR_RNDD);
mpfr_sub(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NE  ),MPFR_RNDD);
mpfr_add(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NW  ),MPFR_RNDD);
mpfr_add(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SE  ),MPFR_RNDD);
mpfr_sub(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SW  ),MPFR_RNDD);
mpfr_sub(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NT  ),MPFR_RNDD);
mpfr_add(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NB  ),MPFR_RNDD);
mpfr_add(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ST  ),MPFR_RNDD);
mpfr_sub(uy2,uy2,n,MPFR_RNDD);
mpfr_set_d(n,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SB  ),MPFR_RNDD);
mpfr_sub(uy2,uy2,n,MPFR_RNDD);

	/*	uz2 = + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, T  ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, B  )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ST ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ET ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, EB )
		      + GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WT ) - GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WB );*/
mpfr_set_d(uz2,0.0,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, T  ),MPFR_RNDD);
mpfr_add(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, B  ),MPFR_RNDD);
mpfr_sub(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NT  ),MPFR_RNDD);
mpfr_add(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, NB  ),MPFR_RNDD);
mpfr_sub(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ST  ),MPFR_RNDD);
mpfr_add(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, SB  ),MPFR_RNDD);
mpfr_sub(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, ET  ),MPFR_RNDD);
mpfr_add(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, EB  ),MPFR_RNDD);
mpfr_sub(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WT  ),MPFR_RNDD);
mpfr_add(uz2,uz2,q,MPFR_RNDD);
mpfr_set_d(q,GRID_ENTRY_SWEEP( srcGrid, 0, 0, -2, WB  ),MPFR_RNDD);
mpfr_sub(uz2,uz2,q,MPFR_RNDD);
	
      //	ux2 /= rho2;
	//	uy2 /= rho2;
	//	uz2 /= rho2;
mpfr_div(ux2,ux2,rho2,MPFR_RNDD);
mpfr_div(uy2,uy2,rho2,MPFR_RNDD);
mpfr_div(uz2,uz2,rho2,MPFR_RNDD);
	//	rho = 1.0;
mpfr_set_d(rho,1.0,MPFR_RNDD);
mpfr_set_d(ux,0.0,MPFR_RNDD);
mpfr_set_d(uy,0.0,MPFR_RNDD);
mpfr_set_d(uz,0.0,MPFR_RNDD);
mpfr_set_d(u2,0.0,MPFR_RNDD);
mpfr_t two;
mpfr_init2(two,config_vals[0]);
mpfr_set_d(two,2,MPFR_RNDD);
mpfr_mul(ux,two,ux1,MPFR_RNDD);
mpfr_sub(ux,ux,ux2,MPFR_RNDD);

mpfr_mul(uy,two,uy1,MPFR_RNDD);
mpfr_sub(uy,uy,uy2,MPFR_RNDD);

mpfr_mul(uz,two,uz1,MPFR_RNDD);
mpfr_sub(uz,uz,uz2,MPFR_RNDD);
	//	ux = 2*ux1 - ux2;
	//	uy = 2*uy1 - uy2;
	//	uz = 2*uz1 - uz2;

	//	u2 = 1.5 * (ux*ux + uy*uy + uz*uz);
	mpfr_mul(p, ux, ux, MPFR_RNDD);
  mpfr_mul(q, uy, uy, MPFR_RNDD);
  mpfr_mul(r, uz, uz, MPFR_RNDD);
  mpfr_add(u2, p, q, MPFR_RNDD);
  mpfr_add(u2, u2, r, MPFR_RNDD);
  mpfr_set_d(onept5,1.5,MPFR_RNDD);
  mpfr_mul(u2, onept5, u2, MPFR_RNDD);

	/*	LOCAL( srcGrid, C ) = DFL1*rho*(1.0                                 - u2);

		LOCAL( srcGrid, N ) = DFL2*rho*(1.0 +       uy*(4.5*uy       + 3.0) - u2);
		LOCAL( srcGrid, S ) = DFL2*rho*(1.0 +       uy*(4.5*uy       - 3.0) - u2);
		LOCAL( srcGrid, E ) = DFL2*rho*(1.0 +       ux*(4.5*ux       + 3.0) - u2);
		LOCAL( srcGrid, W ) = DFL2*rho*(1.0 +       ux*(4.5*ux       - 3.0) - u2);
		LOCAL( srcGrid, T ) = DFL2*rho*(1.0 +       uz*(4.5*uz       + 3.0) - u2);
		LOCAL( srcGrid, B ) = DFL2*rho*(1.0 +       uz*(4.5*uz       - 3.0) - u2);

		LOCAL( srcGrid, NE) = DFL3*rho*(1.0 + (+ux+uy)*(4.5*(+ux+uy) + 3.0) - u2);
		LOCAL( srcGrid, NW) = DFL3*rho*(1.0 + (-ux+uy)*(4.5*(-ux+uy) + 3.0) - u2);
		LOCAL( srcGrid, SE) = DFL3*rho*(1.0 + (+ux-uy)*(4.5*(+ux-uy) + 3.0) - u2);
		LOCAL( srcGrid, SW) = DFL3*rho*(1.0 + (-ux-uy)*(4.5*(-ux-uy) + 3.0) - u2);
		LOCAL( srcGrid, NT) = DFL3*rho*(1.0 + (+uy+uz)*(4.5*(+uy+uz) + 3.0) - u2);
		LOCAL( srcGrid, NB) = DFL3*rho*(1.0 + (+uy-uz)*(4.5*(+uy-uz) + 3.0) - u2);
		LOCAL( srcGrid, ST) = DFL3*rho*(1.0 + (-uy+uz)*(4.5*(-uy+uz) + 3.0) - u2);
		LOCAL( srcGrid, SB) = DFL3*rho*(1.0 + (-uy-uz)*(4.5*(-uy-uz) + 3.0) - u2);
		LOCAL( srcGrid, ET) = DFL3*rho*(1.0 + (+ux+uz)*(4.5*(+ux+uz) + 3.0) - u2);
		LOCAL( srcGrid, EB) = DFL3*rho*(1.0 + (+ux-uz)*(4.5*(+ux-uz) + 3.0) - u2);
		LOCAL( srcGrid, WT) = DFL3*rho*(1.0 + (-ux+uz)*(4.5*(-ux+uz) + 3.0) - u2);
		LOCAL( srcGrid, WB) = DFL3*rho*(1.0 + (-ux-uz)*(4.5*(-ux-uz) + 3.0) - u2);*/
                mpfr_set_d(t2,LOCAL( srcGrid, N ),MPFR_RNDD);
		mpfr_mul(fourpt5uy,uy,fourpt5,MPFR_RNDD);
                mpfr_add(src_N,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_N,src_N,uy,MPFR_RNDD);
                mpfr_sub(src_N,src_N,u2,MPFR_RNDD);
                mpfr_add(src_N,src_N,one,MPFR_RNDD);
                mpfr_mul(src_N,src_N,rho,MPFR_RNDD);
                mpfr_mul(t2,src_N,o,MPFR_RNDD);
                LOCAL( srcGrid, N )= mpfr_get_d (t2,MPFR_RNDD);

		mpfr_set_d(t1,LOCAL( srcGrid, C ),MPFR_RNDD);
                mpfr_sub(src_C,one,u2,MPFR_RNDD);
                mpfr_mul(src_C,src_C,rho,MPFR_RNDD);
                mpfr_mul(t1,src_C,o2,MPFR_RNDD);
                 LOCAL( srcGrid, C )= mpfr_get_d (t1,MPFR_RNDD);
 
		mpfr_set_d(t3,LOCAL( srcGrid, S ),MPFR_RNDD);
                mpfr_sub(src_S,fourpt5uy,three,MPFR_RNDD);
                mpfr_mul(src_S,src_S,uy,MPFR_RNDD);
                mpfr_sub(src_S,src_S,u2,MPFR_RNDD);
                mpfr_add(src_S,src_S,one,MPFR_RNDD);
                mpfr_mul(src_S,src_S,rho,MPFR_RNDD);
                mpfr_mul(t3,src_S,o,MPFR_RNDD);
                 LOCAL( srcGrid, S )= mpfr_get_d (t3,MPFR_RNDD);

		mpfr_set_d(t4,LOCAL( srcGrid, E ),MPFR_RNDD);
                mpfr_mul(fourpt5ux,ux,fourpt5,MPFR_RNDD);
                mpfr_add(src_E,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_E,src_E,ux,MPFR_RNDD);
                mpfr_sub(src_E,src_E,u2,MPFR_RNDD);
                mpfr_add(src_E,src_E,one,MPFR_RNDD);
                mpfr_mul(src_E,src_E,rho,MPFR_RNDD);
                mpfr_mul(t4,src_E,o,MPFR_RNDD);
                 LOCAL( srcGrid, E )= mpfr_get_d (t4,MPFR_RNDD);

	mpfr_set_d(t5,LOCAL( srcGrid, W ),MPFR_RNDD);

                mpfr_sub(src_W,fourpt5ux,three,MPFR_RNDD);
                mpfr_mul(src_W,src_W,ux,MPFR_RNDD);
                mpfr_sub(src_W,src_W,u2,MPFR_RNDD);
                mpfr_add(src_W,src_W,one,MPFR_RNDD);
                mpfr_mul(src_W,src_W,rho,MPFR_RNDD);
                mpfr_mul(t5,src_W,o,MPFR_RNDD);
                 LOCAL( srcGrid, W )= mpfr_get_d (t5,MPFR_RNDD);

mpfr_set_d(t6,LOCAL( srcGrid, T ),MPFR_RNDD);
                mpfr_mul(fourpt5uz,uz,fourpt5,MPFR_RNDD);
                mpfr_add(src_T,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_T,src_T,uz,MPFR_RNDD);
                mpfr_sub(src_T,src_T,u2,MPFR_RNDD);
                mpfr_add(src_T,src_T,one,MPFR_RNDD);
                mpfr_mul(src_T,src_T,rho,MPFR_RNDD);
                mpfr_mul(t6,src_T,o,MPFR_RNDD);
                LOCAL( srcGrid, T )= mpfr_get_d (t6,MPFR_RNDD);

mpfr_set_d(t7,LOCAL( srcGrid, B ),MPFR_RNDD);

                mpfr_sub(src_B,fourpt5uz,three,MPFR_RNDD);
                mpfr_mul(src_B,src_B,uz,MPFR_RNDD);
                mpfr_sub(src_B,src_B,u2,MPFR_RNDD);
                mpfr_add(src_B,src_B,one,MPFR_RNDD);
                mpfr_mul(src_B,src_B,rho,MPFR_RNDD);
                mpfr_mul(t7,src_B,o,MPFR_RNDD);
                 LOCAL( srcGrid, B )= mpfr_get_d (t7,MPFR_RNDD);

mpfr_set_d(t8,LOCAL( srcGrid, NE ),MPFR_RNDD);

                mpfr_add(NEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_NE,fourpt5,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,three,MPFR_RNDD);
                mpfr_mul(src_NE,src_NE,NEadd,MPFR_RNDD);
                mpfr_add(src_NE,src_NE,one,MPFR_RNDD);
                mpfr_sub(src_NE,src_NE,u2,MPFR_RNDD);
                mpfr_mul(src_NE,src_NE,rho,MPFR_RNDD);
                mpfr_mul(t8,src_NE,o3,MPFR_RNDD);
                 LOCAL( srcGrid, NE )= mpfr_get_d (t8,MPFR_RNDD);

mpfr_set_d(t9,LOCAL( srcGrid, NW ),MPFR_RNDD);

                mpfr_sub(NWadd,uy,ux,MPFR_RNDD);
                mpfr_mul(src_NW,fourpt5,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,three,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,NWadd,MPFR_RNDD);
                mpfr_add(src_NW,src_NW,one,MPFR_RNDD);
                mpfr_sub(src_NW,src_NW,u2,MPFR_RNDD);
                mpfr_mul(src_NW,src_NW,rho,MPFR_RNDD);
                mpfr_mul(t9,src_NW,o3,MPFR_RNDD);
                LOCAL( srcGrid, NW )= mpfr_get_d (t9,MPFR_RNDD);

mpfr_set_d(t10,LOCAL( srcGrid, SE ),MPFR_RNDD);

                mpfr_sub(SEadd,ux,uy,MPFR_RNDD);
                mpfr_mul(src_SE,fourpt5,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,three,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,SEadd,MPFR_RNDD);
                mpfr_add(src_SE,src_SE,one,MPFR_RNDD);
                mpfr_sub(src_SE,src_SE,u2,MPFR_RNDD);
                mpfr_mul(src_SE,src_SE,rho,MPFR_RNDD);
                mpfr_mul(t10,src_SE,o3,MPFR_RNDD);
                LOCAL( srcGrid, SE )= mpfr_get_d (t10,MPFR_RNDD);

 mpfr_set_d(t11,LOCAL( srcGrid, SW ),MPFR_RNDD);

                mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(SWadd,ux_neg,uy,MPFR_RNDD);
                mpfr_mul(src_SW,fourpt5,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,three,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,SWadd,MPFR_RNDD);
                mpfr_add(src_SW,src_SW,one,MPFR_RNDD);
                mpfr_sub(src_SW,src_SW,u2,MPFR_RNDD);
                mpfr_mul(src_SW,src_SW,rho,MPFR_RNDD);
                mpfr_mul(t11,src_SW,o3,MPFR_RNDD);
                LOCAL( srcGrid, SW )= mpfr_get_d (t11,MPFR_RNDD);

mpfr_set_d(t12,LOCAL( srcGrid, NT ),MPFR_RNDD);

                mpfr_add(NTadd,uy,uz,MPFR_RNDD);
                mpfr_mul(src_NT,fourpt5,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,three,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,NTadd,MPFR_RNDD);
                mpfr_add(src_NT,src_NT,one,MPFR_RNDD);
                mpfr_sub(src_NT,src_NT,u2,MPFR_RNDD);
                mpfr_mul(src_NT,src_NT,rho,MPFR_RNDD);
                mpfr_mul(t12,src_NT,o3,MPFR_RNDD);
                LOCAL( srcGrid, NT )= mpfr_get_d (t12,MPFR_RNDD);

mpfr_set_d(t13,LOCAL( srcGrid, NB ),MPFR_RNDD);

                mpfr_sub(NBadd,uy,uz,MPFR_RNDD);
                mpfr_mul(src_NB,fourpt5,NBadd,MPFR_RNDD);
                mpfr_add(src_NB,src_NB,three,MPFR_RNDD);
                mpfr_mul(src_NB,src_NB,NBadd,MPFR_RNDD);
                mpfr_add(src_NB,src_NB,one,MPFR_RNDD);
                mpfr_sub(src_NB,src_NB,u2,MPFR_RNDD);
                mpfr_mul(src_NB,src_NB,rho,MPFR_RNDD);
                mpfr_mul(t13,src_NB,o3,MPFR_RNDD);
                 LOCAL( srcGrid, NB )= mpfr_get_d (t13,MPFR_RNDD);

 mpfr_set_d(t14,LOCAL( srcGrid, ST ),MPFR_RNDD);

                mpfr_sub(STadd,uz,uy,MPFR_RNDD);
                mpfr_mul(src_ST,fourpt5,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,three,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,STadd,MPFR_RNDD);
                mpfr_add(src_ST,src_ST,one,MPFR_RNDD);
                mpfr_sub(src_ST,src_ST,u2,MPFR_RNDD);
                mpfr_mul(src_ST,src_ST,rho,MPFR_RNDD);
                mpfr_mul(t14,src_ST,o3,MPFR_RNDD);
		 LOCAL( srcGrid, ST )= mpfr_get_d (t14,MPFR_RNDD);

mpfr_set_d(t15,LOCAL( srcGrid, SB ),MPFR_RNDD);

                mpfr_neg(uy_neg,uy,MPFR_RNDD);
                mpfr_sub(SBadd,uy_neg,uz,MPFR_RNDD);
                mpfr_mul(src_SB,fourpt5,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,three,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,SBadd,MPFR_RNDD);
                mpfr_add(src_SB,src_SB,one,MPFR_RNDD);
                mpfr_sub(src_SB,src_SB,u2,MPFR_RNDD);
                mpfr_mul(src_SB,src_SB,rho,MPFR_RNDD);
                mpfr_mul(t15,src_SB,o3,MPFR_RNDD);
                 LOCAL( srcGrid, SB )= mpfr_get_d (t15,MPFR_RNDD);

mpfr_set_d(t16,LOCAL( srcGrid, ET ),MPFR_RNDD);

                mpfr_add(ETadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_ET,fourpt5,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,three,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,ETadd,MPFR_RNDD);
                mpfr_add(src_ET,src_ET,one,MPFR_RNDD);
                mpfr_sub(src_ET,src_ET,u2,MPFR_RNDD);
                mpfr_mul(src_ET,src_ET,rho,MPFR_RNDD);
                mpfr_mul(t16,src_ET,o3,MPFR_RNDD);
                LOCAL( srcGrid, ET )= mpfr_get_d (t16,MPFR_RNDD);

mpfr_set_d(t17,LOCAL( srcGrid, EB ),MPFR_RNDD);

                mpfr_sub(EBadd,ux,uz,MPFR_RNDD);
                mpfr_mul(src_EB,fourpt5,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,three,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,EBadd,MPFR_RNDD);
                mpfr_add(src_EB,src_EB,one,MPFR_RNDD);
                mpfr_sub(src_EB,src_EB,u2,MPFR_RNDD);
                mpfr_mul(src_EB,src_EB,rho,MPFR_RNDD);
                mpfr_mul(t17,src_EB,o3,MPFR_RNDD);
                 LOCAL( srcGrid, EB )= mpfr_get_d (t17,MPFR_RNDD);

mpfr_set_d(t18,LOCAL( srcGrid, WT ),MPFR_RNDD);

                mpfr_sub(WTadd,uz,ux,MPFR_RNDD);
                mpfr_mul(src_WT,fourpt5,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,three,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,WTadd,MPFR_RNDD);
                mpfr_add(src_WT,src_WT,one,MPFR_RNDD);
                mpfr_sub(src_WT,src_WT,u2,MPFR_RNDD);
                mpfr_mul(src_WT,src_WT,rho,MPFR_RNDD);
                mpfr_mul(t18,src_WT,o3,MPFR_RNDD);
                 LOCAL( srcGrid, WT )= mpfr_get_d (t18,MPFR_RNDD);

mpfr_set_d(t19,LOCAL( srcGrid, WB ),MPFR_RNDD);

                mpfr_neg(ux_neg,ux,MPFR_RNDD);
                mpfr_sub(WBadd,ux_neg,uz,MPFR_RNDD);
                mpfr_mul(src_WB,fourpt5,WBadd,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,three,MPFR_RNDD);
                mpfr_mul(src_WB,src_WB,WBadd,MPFR_RNDD);
                mpfr_sub(src_WB,src_WB,u2,MPFR_RNDD);
                mpfr_add(src_WB,src_WB,one,MPFR_RNDD);
                
                mpfr_mul(src_WB,src_WB,rho,MPFR_RNDD);
                mpfr_mul(t19,src_WB,o3,MPFR_RNDD);
                 LOCAL( srcGrid, WB )= mpfr_get_d (t19,MPFR_RNDD);
	SWEEP_END
}

/*############################################################################*/
//Converted MPFR code
void LBM_showGridStatistics( LBM_Grid grid) {
	
      
       int nObstacleCells = 0,
	    nAccelCells    = 0,
	    nFluidCells    = 0;
       //	double ux, uy, uz;
        mpfr_t ux;
  	mpfr_init2(ux, config_vals[1]);
        mpfr_t uy;
  	mpfr_init2(uy, config_vals[2]);
  	mpfr_t uz;
  	mpfr_init2(uz, config_vals[3]);
  	mpfr_t minU2;
//	double minU2  = 1e+30, maxU2  = -1e+30, u2;
//	double minRho = 1e+30, maxRho = -1e+30, rho;
//	double mass = 0;
        mpfr_set_d(ux,0.0,MPFR_RNDD);
        mpfr_set_d(uy,0.0,MPFR_RNDD);
        mpfr_set_d(uz,0.0,MPFR_RNDD);
	mpfr_init2(minU2, config_vals[10]);
        mpfr_set_d(minU2,(1e+30),MPFR_RNDD);
  	mpfr_t maxU2;
  	mpfr_init2(maxU2, config_vals[11]);
  	mpfr_set_d(maxU2,-(1e+30), MPFR_RNDD);
  	mpfr_t u2;
  	mpfr_init2(u2, config_vals[4]);
  	mpfr_t minRho;
  	mpfr_init2(minRho, config_vals[12]);
  	mpfr_set_d(minRho,(1e+30), MPFR_RNDD);
  	mpfr_t maxRho;
  	mpfr_init2(maxRho, config_vals[13]);
  	mpfr_set_d(maxRho,-(1e+30), MPFR_RNDD);
  	mpfr_t rho;
  	mpfr_init2(rho, config_vals[5]);
  	mpfr_set_d(rho,0.0,MPFR_RNDD);
       mpfr_t z;
       mpfr_init2(z,config_vals[0]);
        mpfr_t mass;
  	mpfr_init2(mass, config_vals[0]);
  	mpfr_set_d(mass, 0.0, MPFR_RNDD);
        
int index;
      
        SWEEP_VAR

	SWEEP_START( 0, 0, 0, 0, 0, SIZE_Z )
  	mpfr_set_d(rho,0.0,MPFR_RNDD);
	   for(index=0;index<19;index++)
{
mpfr_set_d(z,LOCAL(grid,index),MPFR_RNDD);  
           mpfr_add(rho,rho,z,MPFR_RNDD);
}
           
 	
/*rho= + LOCAL( grid, C  ) + LOCAL( grid, N  )
		      + LOCAL( grid, S  ) + LOCAL( grid, E  )
		      + LOCAL( grid, W  ) + LOCAL( grid, T  )
		      + LOCAL( grid, B  ) + LOCAL( grid, NE )
		      + LOCAL( grid, NW ) + LOCAL( grid, SE )
		      + LOCAL( grid, SW ) + LOCAL( grid, NT )
		      + LOCAL( grid, NB ) + LOCAL( grid, ST )
		      + LOCAL( grid, SB ) + LOCAL( grid, ET )
		      + LOCAL( grid, EB ) + LOCAL( grid, WT )
		      + LOCAL( grid, WB ),MPFR_RNDD);*/
	//	if( rho < minRho ) minRho = rho;
	//	if( rho > maxRho ) maxRho = rho;
	//	mass += rho;
	if (mpfr_less_p(rho, minRho))
    		mpfr_set(minRho, rho, MPFR_RNDD);

  	if (mpfr_greater_p(rho, maxRho))
    		mpfr_set(maxRho, rho, MPFR_RNDD);

  	mpfr_add(mass, mass, rho, MPFR_RNDD);
          if( TEST_FLAG_SWEEP( grid, OBSTACLE )) {
			nObstacleCells++;
		}
		else {
			if( TEST_FLAG_SWEEP( grid, ACCEL ))
				nAccelCells++;
			else
				nFluidCells++;

	         /*	ux =  LOCAL( grid, E  ) - LOCAL( grid, W  )
			     + LOCAL( grid, NE ) - LOCAL( grid, NW )
			     + LOCAL( grid, SE ) - LOCAL( grid, SW )
			     + LOCAL( grid, ET ) + LOCAL( grid, EB )
			     - LOCAL( grid, WT ) - LOCAL( grid, WB );*/
 


mpfr_set_d(ux, 0.0, MPFR_RNDD);
mpfr_set_d(uy, 0.0, MPFR_RNDD);
mpfr_set_d(uz, 0.0, MPFR_RNDD);
mpfr_set_d(u2, 0.0, MPFR_RNDD);

  mpfr_t e;
  mpfr_init2(e, config_vals[0]);
mpfr_set_d(e,LOCAL(grid,E),MPFR_RNDD);
mpfr_add(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,W),MPFR_RNDD);
mpfr_sub(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,NE),MPFR_RNDD);
mpfr_add(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,NW),MPFR_RNDD);
mpfr_sub(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,SE),MPFR_RNDD);
mpfr_add(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,SW),MPFR_RNDD);
mpfr_sub(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,ET),MPFR_RNDD);
mpfr_add(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,EB),MPFR_RNDD);
mpfr_add(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,WT),MPFR_RNDD);
mpfr_sub(ux,ux,e,MPFR_RNDD);
mpfr_set_d(e,LOCAL(grid,WB),MPFR_RNDD);
mpfr_sub(ux,ux,e,MPFR_RNDD);

		/*	uy =  LOCAL( grid, N  ) - LOCAL( grid, S  )
			     + LOCAL( grid, NE ) + LOCAL( grid, NW )
			     - LOCAL( grid, SE ) - LOCAL( grid, SW )
			     + LOCAL( grid, NT ) + LOCAL( grid, NB )
			     - LOCAL( grid, ST ) - LOCAL( grid, SB );*/
mpfr_t n;
mpfr_init2(n, config_vals[0]);
mpfr_set_d(n,LOCAL(grid,N),MPFR_RNDD);
mpfr_add(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,S),MPFR_RNDD);
mpfr_sub(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,NE),MPFR_RNDD);
mpfr_add(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,NW),MPFR_RNDD);
mpfr_add(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,SE),MPFR_RNDD);
mpfr_sub(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,SW),MPFR_RNDD);
mpfr_sub(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,NT),MPFR_RNDD);
mpfr_add(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,NB),MPFR_RNDD);
mpfr_add(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,ST),MPFR_RNDD);
mpfr_sub(uy,uy,n,MPFR_RNDD);
mpfr_set_d(n,LOCAL(grid,SB),MPFR_RNDD);
mpfr_sub(uy,uy,n,MPFR_RNDD);
		/*	uz =  LOCAL( grid, T  ) - LOCAL( grid, B  )
			     + LOCAL( grid, NT ) - LOCAL( grid, NB )
			     + LOCAL( grid, ST ) - LOCAL( grid, SB )
			     + LOCAL( grid, ET ) - LOCAL( grid, EB )
			     + LOCAL( grid, WT ) - LOCAL( grid, WB );*/
			
mpfr_t q;
mpfr_init2(q, config_vals[0]);
mpfr_set_d(q,LOCAL(grid,T),MPFR_RNDD);
mpfr_add(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,B),MPFR_RNDD);
mpfr_sub(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,NT),MPFR_RNDD);
mpfr_add(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,NB),MPFR_RNDD);
mpfr_sub(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,ST),MPFR_RNDD);
mpfr_add(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,SB),MPFR_RNDD);
mpfr_sub(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,ET),MPFR_RNDD);
mpfr_add(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,EB),MPFR_RNDD);
mpfr_sub(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,WT),MPFR_RNDD);
mpfr_add(uz,uz,q,MPFR_RNDD);
 mpfr_set_d(q,LOCAL(grid,WB),MPFR_RNDD);
mpfr_sub(uz,uz,q,MPFR_RNDD);
                   //     u2 = (ux*ux + uy*uy + uz*uz) / (rho*rho);
  mpfr_t a;
  mpfr_init2(a, config_vals[0]);
  mpfr_t b;
  mpfr_init2(b, config_vals[0]);
  mpfr_t c;
  mpfr_init2(c, config_vals[0]);
  mpfr_t d;
  mpfr_init2(d, config_vals[0]);
  mpfr_t u2;
  mpfr_init2(u2, config_vals[4]);
  mpfr_set_d(a, 0.0, MPFR_RNDD);
  mpfr_set_d(b, 0.0, MPFR_RNDD);
  mpfr_set_d(c, 0.0, MPFR_RNDD);
  mpfr_set_d(d, 0.0, MPFR_RNDD);
  mpfr_set_d(u2,0.0,MPFR_RNDD);
  mpfr_mul(a, ux, ux, MPFR_RNDD);
  mpfr_mul(b, uy, uy, MPFR_RNDD);
  mpfr_mul(c, uz, uz, MPFR_RNDD);
  mpfr_add(a, a, b, MPFR_RNDD);
  mpfr_add(a, a, c, MPFR_RNDD);
  mpfr_mul(d, rho, rho, MPFR_RNDD);
  mpfr_div(u2, a, d, MPFR_RNDD);
  		//	if( u2 < minU2 ) minU2 = u2;
		//	if( u2 > maxU2 ) maxU2 = u2;
		        if (mpfr_less_p(u2, minU2)) {
			   // mpfr_out_str(0,10,0,u2,MPFR_RNDD);
 			   // printf(" less than\n");        
                            mpfr_set(minU2, u2, MPFR_RNDD); }
			if (mpfr_greater_p(u2, maxU2)) {
			   // mpfr_out_str(0,10,0,u2,MPFR_RNDD);        
 			   // printf(" greater than\n");        
                            mpfr_set(maxU2, u2, MPFR_RNDD); }
		}
	SWEEP_END
        mpfr_sqrt(minU2,minU2,MPFR_RNDD);
	//mpfr_out_str(0,10,0,maxU2,MPFR_RNDD);        
	mpfr_sqrt(maxU2,maxU2,MPFR_RNDD);
	
       // printf( "LBM_showGridStatistics:\n"
       // "\tnObstacleCells: %7d nAccelCells: %7d nFluidCells: %7d\n");
       // "\tminRho: %8.4f maxRho: %8.4f mass: %e\n"
        //"\tminU: %e maxU: %e\n\n",
       // nObstacleCells, nAccelCells, nFluidCells,
       // minRho, maxRho, mass,
       // minU2, maxU2 );
        //mpfr_out_str( 0, 10,0, ux, MPFR_RNDN);
	//mpfr_out_str( 0, 10,0, uy, MPFR_RNDN);
	//printf("\n");
	//mpfr_out_str( 0, 10,0, uz, MPFR_RNDN);
	//printf("\n");
	//mpfr_out_str( 0, 10,0, rho, MPFR_RNDN);
	//printf("\n");
	//mpfr_out_str( 0, 10,0, u2, MPFR_RNDN);
       // printf("LBM_showGridStatistics:\n");
//printf("\tnObstacleCells:\n");       
// mpfr_out_str( stdout, 10,0, nObstacleCells, MPFR_RNDD);
        //mpfr_out_str(0,10,0,nObstacleCells,MPFR_RNDD);
        
//printf("\n nAccelCells:");
        //mpfr_out_str( stdout, 10,0, nAccelCells, MPFR_RNDN);
       // printf("\n nFluidCells:\n");
        //mpfr_out_str( stdout, 10,0, nFluidCells, MPFR_RNDN);
      // printf("\tminRho: ");
       mpfr_out_str( 0, 10,0, minRho, MPFR_RNDN);
       //printf("\t maxRho: ");
printf(",");       
mpfr_out_str( 0, 10,0, maxRho, MPFR_RNDN);
       //printf("\t mass: ");
printf(",");        
mpfr_out_str( 0, 10,0, mass, MPFR_RNDN);
       //printf("\n\t minU: ");
printf(",");  
       mpfr_out_str( 0, 10,0, minU2, MPFR_RNDN);
       //printf("\t maxU: ");
printf(",");  
       mpfr_out_str( 0, 10,0, maxU2, MPFR_RNDN);
 printf(",");   
//       printf("\n");
//       printf("\n");
       mpfr_clear(minRho);
       mpfr_clear(maxRho);
       mpfr_clear(mass);
       mpfr_clear(minU2);
       mpfr_clear(maxU2);
       
}

/*############################################################################*/

static void storeValue( FILE* file, OUTPUT_PRECISION* v ) {
	const int litteBigEndianTest = 1;
	if( (*((unsigned char*) &litteBigEndianTest)) == 0 ) {         /* big endian */
		const char* vPtr = (char*) v;
		char buffer[sizeof( OUTPUT_PRECISION )];
		int i;

		for (i = 0; i < sizeof( OUTPUT_PRECISION ); i++)
			buffer[i] = vPtr[sizeof( OUTPUT_PRECISION ) - i - 1];

		fwrite( buffer, sizeof( OUTPUT_PRECISION ), 1, file );
	}
	else {                                                     /* little endian */
		fwrite( v, sizeof( OUTPUT_PRECISION ), 1, file );
	}
}

/*############################################################################*/

static void loadValue( FILE* file, OUTPUT_PRECISION* v ) {
	const int litteBigEndianTest = 1;
	if( (*((unsigned char*) &litteBigEndianTest)) == 0 ) {         /* big endian */
		char* vPtr = (char*) v;
		char buffer[sizeof( OUTPUT_PRECISION )];
		int i;

		fread( buffer, sizeof( OUTPUT_PRECISION ), 1, file );

		for (i = 0; i < sizeof( OUTPUT_PRECISION ); i++)
			vPtr[i] = buffer[sizeof( OUTPUT_PRECISION ) - i - 1];
	}
	else {                                                     /* little endian */
		fread( v, sizeof( OUTPUT_PRECISION ), 1, file );
	}
}

/*############################################################################*/

void LBM_storeVelocityField( LBM_Grid grid, const char* filename,
                             const int binary ) {
	int x, y, z;
	OUTPUT_PRECISION rho, ux, uy, uz;

	FILE* file = fopen( filename, (binary ? "wb" : "w") );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				rho = + GRID_ENTRY( grid, x, y, z, C  ) + GRID_ENTRY( grid, x, y, z, N  )
				      + GRID_ENTRY( grid, x, y, z, S  ) + GRID_ENTRY( grid, x, y, z, E  )
				      + GRID_ENTRY( grid, x, y, z, W  ) + GRID_ENTRY( grid, x, y, z, T  )
				      + GRID_ENTRY( grid, x, y, z, B  ) + GRID_ENTRY( grid, x, y, z, NE )
				      + GRID_ENTRY( grid, x, y, z, NW ) + GRID_ENTRY( grid, x, y, z, SE )
				      + GRID_ENTRY( grid, x, y, z, SW ) + GRID_ENTRY( grid, x, y, z, NT )
				      + GRID_ENTRY( grid, x, y, z, NB ) + GRID_ENTRY( grid, x, y, z, ST )
				      + GRID_ENTRY( grid, x, y, z, SB ) + GRID_ENTRY( grid, x, y, z, ET )
				      + GRID_ENTRY( grid, x, y, z, EB ) + GRID_ENTRY( grid, x, y, z, WT )
				      + GRID_ENTRY( grid, x, y, z, WB );
				ux = + GRID_ENTRY( grid, x, y, z, E  ) - GRID_ENTRY( grid, x, y, z, W  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) - GRID_ENTRY( grid, x, y, z, NW ) 
				     + GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) + GRID_ENTRY( grid, x, y, z, EB ) 
				     - GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				uy = + GRID_ENTRY( grid, x, y, z, N  ) - GRID_ENTRY( grid, x, y, z, S  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) + GRID_ENTRY( grid, x, y, z, NW ) 
				     - GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) + GRID_ENTRY( grid, x, y, z, NB ) 
				     - GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB );
				uz = + GRID_ENTRY( grid, x, y, z, T  ) - GRID_ENTRY( grid, x, y, z, B  ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) - GRID_ENTRY( grid, x, y, z, NB ) 
				     + GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) - GRID_ENTRY( grid, x, y, z, EB ) 
				     + GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				ux /= rho;
				uy /= rho;
				uz /= rho;

				if( binary ) {
					/*
					fwrite( &ux, sizeof( ux ), 1, file );
					fwrite( &uy, sizeof( uy ), 1, file );
					fwrite( &uz, sizeof( uz ), 1, file );
					*/
					storeValue( file, &ux );
					storeValue( file, &uy );
					storeValue( file, &uz );
				} else
					fprintf( file, "%e %e %e\n", ux, uy, uz );

			}
		}
	}

	fclose( file );
}

/*############################################################################*/

void LBM_compareVelocityField( LBM_Grid grid, const char* filename,
                             const int binary ) {
	int x, y, z;
	double rho, ux, uy, uz;
	OUTPUT_PRECISION fileUx, fileUy, fileUz,
	                 dUx, dUy, dUz,
	                 diff2, maxDiff2 = -1e+30;

	FILE* file = fopen( filename, (binary ? "rb" : "r") );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				rho = + GRID_ENTRY( grid, x, y, z, C  ) + GRID_ENTRY( grid, x, y, z, N  )
				      + GRID_ENTRY( grid, x, y, z, S  ) + GRID_ENTRY( grid, x, y, z, E  )
				      + GRID_ENTRY( grid, x, y, z, W  ) + GRID_ENTRY( grid, x, y, z, T  )
				      + GRID_ENTRY( grid, x, y, z, B  ) + GRID_ENTRY( grid, x, y, z, NE )
				      + GRID_ENTRY( grid, x, y, z, NW ) + GRID_ENTRY( grid, x, y, z, SE )
				      + GRID_ENTRY( grid, x, y, z, SW ) + GRID_ENTRY( grid, x, y, z, NT )
				      + GRID_ENTRY( grid, x, y, z, NB ) + GRID_ENTRY( grid, x, y, z, ST )
				      + GRID_ENTRY( grid, x, y, z, SB ) + GRID_ENTRY( grid, x, y, z, ET )
				      + GRID_ENTRY( grid, x, y, z, EB ) + GRID_ENTRY( grid, x, y, z, WT )
				      + GRID_ENTRY( grid, x, y, z, WB );
				ux = + GRID_ENTRY( grid, x, y, z, E  ) - GRID_ENTRY( grid, x, y, z, W  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) - GRID_ENTRY( grid, x, y, z, NW ) 
				     + GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) + GRID_ENTRY( grid, x, y, z, EB ) 
				     - GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				uy = + GRID_ENTRY( grid, x, y, z, N  ) - GRID_ENTRY( grid, x, y, z, S  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) + GRID_ENTRY( grid, x, y, z, NW ) 
				     - GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) + GRID_ENTRY( grid, x, y, z, NB ) 
				     - GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB );
				uz = + GRID_ENTRY( grid, x, y, z, T  ) - GRID_ENTRY( grid, x, y, z, B  ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) - GRID_ENTRY( grid, x, y, z, NB ) 
				     + GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) - GRID_ENTRY( grid, x, y, z, EB ) 
				     + GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				ux /= rho;
				uy /= rho;
				uz /= rho;

				if( binary ) {
					loadValue( file, &fileUx );
					loadValue( file, &fileUy );
					loadValue( file, &fileUz );
				}
				else {
					if( sizeof( OUTPUT_PRECISION ) == sizeof( double )) {
						fscanf( file, "%f %f %f\n", &fileUx, &fileUy, &fileUz );
					}
					else {
						fscanf( file, "%f %f %f\n", &fileUx, &fileUy, &fileUz );
					}
				}

				dUx = ux - fileUx;
				dUy = uy - fileUy;
				dUz = uz - fileUz;
				diff2 = dUx*dUx + dUy*dUy + dUz*dUz;
				if( diff2 > maxDiff2 ) maxDiff2 = diff2;
			}
		}
	}

#if defined(SPEC_CPU)
	printf( "LBM_compareVelocityField: maxDiff = %e  \n\n", sqrt( maxDiff2 ) );
#else
	printf( "LBM_compareVelocityField: maxDiff = %e  ==>  %s\n\n",
	        sqrt( maxDiff2 ),
	        sqrt( maxDiff2 ) > 1e-5 ? "##### ERROR #####" : "OK" );
#endif
	fclose( file );
}

