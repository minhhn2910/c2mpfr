	/* $Id: lbm.c,v 1.6 2004/05/03 08:23:51 pohlt Exp $ */

/*############################################################################*/

#include "lbm.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <mpir.h>
#include <mpfr.h>
#include <fenv.h>

#if !defined(SPEC_CPU)
#ifdef _OPENMP
#include <omp.h>
#endif
#endif
#define LEN 30 
    mpfr_t ux_LBM_performStreamCollide;
  mpfr_t uy_LBM_performStreamCollide;
  mpfr_t uz_LBM_performStreamCollide;
  mpfr_t u2_LBM_performStreamCollide;
  mpfr_t rho_LBM_performStreamCollide;
        mpfr_t temp_var_1;
        mpfr_t temp_var_2;
        mpfr_t temp_var_3;
        mpfr_t temp_var_4;
        mpfr_t temp_var_5;
        mpfr_t temp_var_6;
        mpfr_t temp_var_7;
        mpfr_t temp_var_8;
        mpfr_t temp_var_9;
        mpfr_t temp_var_10;
        mpfr_t temp_var_11;
        mpfr_t temp_var_12;
        mpfr_t temp_var_13;
        mpfr_t temp_var_14;
        mpfr_t temp_var_15;
        mpfr_t temp_var_16;
        mpfr_t temp_var_17;
        mpfr_t temp_var_18;
        mpfr_t temp_var_19;
        mpfr_t temp_var_20;
        mpfr_t temp_var_21;
        mpfr_t temp_var_22;
        mpfr_t temp_var_23;
        mpfr_t temp_var_24;
        mpfr_t temp_var_25;
        mpfr_t temp_var_26;
        mpfr_t temp_var_27;
        mpfr_t temp_var_28;
        mpfr_t temp_var_29;
        mpfr_t temp_var_30;
        mpfr_t temp_var_31;
        mpfr_t temp_var_32;
        mpfr_t temp_var_33;
        mpfr_t temp_var_34;
        mpfr_t temp_var_35;
        mpfr_t temp_var_36;
        mpfr_t temp_var_37;
        mpfr_t temp_var_38;
        mpfr_t temp_var_39;
        mpfr_t temp_var_40;
        mpfr_t temp_var_41;
        mpfr_t temp_var_42;
        mpfr_t temp_var_43;
        mpfr_t temp_var_44;
        mpfr_t temp_var_45;
        mpfr_t temp_var_46;
        mpfr_t temp_var_47;
        mpfr_t temp_var_48;
        mpfr_t temp_var_49;
        mpfr_t temp_var_50;
        mpfr_t temp_var_51;
        mpfr_t temp_var_52;
        mpfr_t temp_var_53;
        mpfr_t temp_var_54;
        mpfr_t temp_var_55;
        mpfr_t temp_var_56;
        mpfr_t temp_var_57;
        mpfr_t temp_var_58;
        mpfr_t temp_var_59;
        mpfr_t temp_var_60;
        mpfr_t temp_var_61;
        mpfr_t temp_var_62;
        mpfr_t temp_var_63;
        mpfr_t temp_var_64;
        mpfr_t temp_var_65;
        mpfr_t temp_var_66;
        mpfr_t temp_var_67;
        mpfr_t temp_var_68;
        mpfr_t temp_var_69;
        mpfr_t temp_var_70;
        mpfr_t temp_var_71;
        mpfr_t temp_var_72;
        mpfr_t temp_var_73;
        mpfr_t temp_var_74;
        mpfr_t temp_var_75;
        mpfr_t temp_var_76;
        mpfr_t temp_var_77;
        mpfr_t temp_var_78;
        mpfr_t temp_var_79;
        mpfr_t temp_var_80;
        mpfr_t temp_var_81;
        mpfr_t temp_var_82;
        mpfr_t temp_var_83;
        mpfr_t temp_var_84;
        mpfr_t temp_var_85;
        mpfr_t temp_var_86;
        mpfr_t temp_var_87;
        mpfr_t temp_var_88;
        mpfr_t temp_var_89;
        mpfr_t temp_var_90;
        mpfr_t temp_var_91;
        mpfr_t temp_var_92;
        mpfr_t temp_var_93;
        mpfr_t temp_var_94;
        mpfr_t temp_var_95;
        mpfr_t temp_var_96;
        mpfr_t temp_var_97;
        mpfr_t temp_var_98;
        mpfr_t temp_var_99;
        mpfr_t temp_var_100;
        mpfr_t temp_var_101;
        mpfr_t temp_var_102;
        mpfr_t temp_var_103;
        mpfr_t temp_var_104;
        mpfr_t temp_var_105;
        mpfr_t temp_var_106;
        mpfr_t temp_var_107;
        mpfr_t temp_var_108;
        mpfr_t temp_var_109;
        mpfr_t temp_var_110;
        mpfr_t temp_var_111;
        mpfr_t temp_var_112;
        mpfr_t temp_var_113;
        mpfr_t temp_var_114;
        mpfr_t temp_var_115;
        mpfr_t temp_var_116;
        mpfr_t temp_var_117;
        mpfr_t temp_var_118;
        mpfr_t temp_var_119;
        mpfr_t temp_var_120;
        mpfr_t temp_var_121;
        mpfr_t temp_var_122;
        mpfr_t temp_var_123;
        mpfr_t temp_var_124;
        mpfr_t temp_var_125;
        mpfr_t temp_var_126;
        mpfr_t temp_var_127;
        mpfr_t temp_var_128;
        mpfr_t temp_var_129;
        mpfr_t temp_var_130;
        mpfr_t temp_var_131;
        mpfr_t temp_var_132;
        mpfr_t temp_var_133;
        mpfr_t temp_var_134;
        mpfr_t temp_var_135;
        mpfr_t temp_var_136;
        mpfr_t temp_var_137;
        mpfr_t temp_var_138;
        mpfr_t temp_var_139;
        mpfr_t temp_var_140;
        mpfr_t temp_var_141;
        mpfr_t temp_var_142;
        mpfr_t temp_var_143;
        mpfr_t temp_var_144;
        mpfr_t temp_var_145;
        mpfr_t temp_var_146;
        mpfr_t temp_var_147;
        mpfr_t temp_var_148;
        mpfr_t temp_var_149;
        mpfr_t temp_var_150;
        mpfr_t temp_var_151;
        mpfr_t temp_var_152;
        mpfr_t temp_var_153;
        mpfr_t temp_var_154;
        mpfr_t temp_var_155;
        mpfr_t temp_var_156;
        mpfr_t temp_var_157;
        mpfr_t temp_var_158;
        mpfr_t temp_var_159;
        mpfr_t temp_var_160;
        mpfr_t temp_var_161;
        mpfr_t temp_var_162;
        mpfr_t temp_var_163;
        mpfr_t temp_var_164;
        mpfr_t temp_var_165;
        mpfr_t temp_var_166;
        mpfr_t temp_var_167;
        mpfr_t temp_var_168;
        mpfr_t temp_var_169;
        mpfr_t temp_var_170;
        mpfr_t temp_var_171;
        mpfr_t temp_var_172;
        mpfr_t temp_var_173;
        mpfr_t temp_var_174;
        mpfr_t temp_var_175;
        mpfr_t temp_var_176;
        mpfr_t temp_var_177;
        mpfr_t temp_var_178;
        mpfr_t temp_var_179;
        mpfr_t temp_var_180;
        mpfr_t temp_var_181;
        mpfr_t temp_var_182;
        mpfr_t temp_var_183;
        mpfr_t temp_var_184;
        mpfr_t temp_var_185;
        mpfr_t temp_var_186;
        mpfr_t temp_var_187;
        mpfr_t temp_var_188;
        mpfr_t temp_var_189;
        mpfr_t temp_var_190;
        mpfr_t temp_var_191;
        mpfr_t temp_var_192;
        mpfr_t temp_var_193;
        mpfr_t temp_var_194;
        mpfr_t temp_var_195;
        mpfr_t temp_var_196;
        mpfr_t temp_var_197;
        mpfr_t temp_var_198;
        mpfr_t temp_var_199;
        mpfr_t temp_var_200;
        mpfr_t temp_var_201;
        mpfr_t temp_var_202;
        mpfr_t temp_var_203;
        mpfr_t temp_var_204;
        mpfr_t temp_var_205;
        mpfr_t temp_var_206;
        mpfr_t temp_var_207;
        mpfr_t temp_var_208;
        mpfr_t temp_var_209;
        mpfr_t temp_var_210;
        mpfr_t temp_var_211;
        mpfr_t temp_var_212;
        mpfr_t temp_var_213;
        mpfr_t temp_var_214;
        mpfr_t temp_var_215;
        mpfr_t temp_var_216;
        mpfr_t temp_var_217;
        mpfr_t temp_var_218;
        mpfr_t temp_var_219;
        mpfr_t temp_var_220;
        mpfr_t temp_var_221;
        mpfr_t temp_var_222;
        mpfr_t temp_var_223;
        mpfr_t temp_var_224;
        mpfr_t temp_var_225;
        mpfr_t temp_var_226;
        mpfr_t temp_var_227;
        mpfr_t temp_var_228;
        mpfr_t temp_var_229;
        mpfr_t temp_var_230;
        mpfr_t temp_var_231;
        mpfr_t temp_var_232;
        mpfr_t temp_var_233;
        mpfr_t temp_var_234;
        mpfr_t temp_var_235;
        mpfr_t temp_var_236;
        mpfr_t temp_var_237;
        mpfr_t temp_var_238;
        mpfr_t temp_var_239;
        mpfr_t temp_var_240;
        mpfr_t temp_var_241;
        mpfr_t temp_var_242;
        mpfr_t temp_var_243;
        mpfr_t temp_var_244;
        mpfr_t temp_var_245;
        mpfr_t temp_var_246;
        mpfr_t temp_var_247;
        mpfr_t temp_var_248;
        mpfr_t temp_var_249;
        mpfr_t temp_var_250;
        mpfr_t temp_var_251;
        mpfr_t temp_var_252;
        mpfr_t temp_var_253;
        mpfr_t temp_var_254;
        mpfr_t temp_var_255;
        mpfr_t temp_var_256;
        mpfr_t temp_var_257;
        mpfr_t temp_var_258;
        mpfr_t temp_var_259;
        mpfr_t temp_var_260;
        mpfr_t temp_var_261;
        mpfr_t temp_var_262;
        mpfr_t temp_var_263;
        mpfr_t temp_var_264;
        mpfr_t temp_var_265;
        mpfr_t temp_var_266;
        mpfr_t temp_var_267;
        mpfr_t temp_var_268;
        mpfr_t temp_var_269;
        mpfr_t temp_var_270;
        mpfr_t temp_var_271;
        mpfr_t temp_var_272;
        mpfr_t temp_var_273;
        mpfr_t temp_var_274;
        mpfr_t temp_var_275;
        mpfr_t temp_var_276;
        mpfr_t temp_var_277;
        mpfr_t temp_var_278;
        mpfr_t temp_var_279;
        mpfr_t temp_var_280;
        mpfr_t temp_var_281;
        mpfr_t temp_var_282;
        mpfr_t temp_var_283;
        mpfr_t temp_var_284;
        mpfr_t temp_var_285;
        mpfr_t temp_var_286;
        mpfr_t temp_var_287;
        mpfr_t temp_var_288;
        mpfr_t temp_var_289;
        mpfr_t temp_var_290;
        mpfr_t temp_var_291;
        mpfr_t temp_var_292;
        mpfr_t temp_var_293;
        mpfr_t temp_var_294;
        mpfr_t temp_var_295;
        mpfr_t temp_var_296;
        mpfr_t temp_var_297;
        mpfr_t temp_var_298;
        mpfr_t temp_var_299;
        mpfr_t temp_var_300;
        mpfr_t temp_var_301;
        mpfr_t temp_var_302;
        mpfr_t temp_var_303;
        mpfr_t temp_var_304;
        mpfr_t temp_var_305;
        mpfr_t temp_var_306;
        mpfr_t temp_var_307;
        mpfr_t temp_var_308;
        mpfr_t temp_var_309;
        mpfr_t temp_var_310;
        mpfr_t temp_var_311;
        mpfr_t temp_var_312;
        mpfr_t temp_var_313;
        mpfr_t temp_var_314;
        mpfr_t temp_var_315;
        mpfr_t temp_var_316;
  mpfr_t ux_LBM_handleInOutFlow;
  mpfr_t uy_LBM_handleInOutFlow;
  mpfr_t uz_LBM_handleInOutFlow;
  mpfr_t rho_LBM_handleInOutFlow;
  mpfr_t ux1_LBM_handleInOutFlow;
  mpfr_t uy1_LBM_handleInOutFlow;
  mpfr_t uz1_LBM_handleInOutFlow;
  mpfr_t rho1_LBM_handleInOutFlow;
  mpfr_t ux2_LBM_handleInOutFlow;
  mpfr_t uy2_LBM_handleInOutFlow;
  mpfr_t uz2_LBM_handleInOutFlow;
  mpfr_t rho2_LBM_handleInOutFlow;
  mpfr_t u2_LBM_handleInOutFlow;
  mpfr_t px_LBM_handleInOutFlow;
  mpfr_t py_LBM_handleInOutFlow;
        mpfr_t temp_var_317;
        mpfr_t temp_var_318;
        mpfr_t temp_var_319;
        mpfr_t temp_var_320;
        mpfr_t temp_var_321;
        mpfr_t temp_var_322;
        mpfr_t temp_var_323;
        mpfr_t temp_var_324;
        mpfr_t temp_var_325;
        mpfr_t temp_var_326;
        mpfr_t temp_var_327;
        mpfr_t temp_var_328;
        mpfr_t temp_var_329;
        mpfr_t temp_var_330;
        mpfr_t temp_var_331;
        mpfr_t temp_var_332;
        mpfr_t temp_var_333;
        mpfr_t temp_var_334;
        mpfr_t temp_var_335;
        mpfr_t temp_var_336;
        mpfr_t temp_var_337;
        mpfr_t temp_var_338;
        mpfr_t temp_var_339;
        mpfr_t temp_var_340;
        mpfr_t temp_var_341;
        mpfr_t temp_var_342;
        mpfr_t temp_var_343;
        mpfr_t temp_var_344;
        mpfr_t temp_var_345;
        mpfr_t temp_var_346;
        mpfr_t temp_var_347;
        mpfr_t temp_var_348;
        mpfr_t temp_var_349;
        mpfr_t temp_var_350;
        mpfr_t temp_var_351;
        mpfr_t temp_var_352;
        mpfr_t temp_var_353;
        mpfr_t temp_var_354;
        mpfr_t temp_var_355;
        mpfr_t temp_var_356;
        mpfr_t temp_var_357;
        mpfr_t temp_var_358;
        mpfr_t temp_var_359;
        mpfr_t temp_var_360;
        mpfr_t temp_var_361;
        mpfr_t temp_var_362;
        mpfr_t temp_var_363;
        mpfr_t temp_var_364;
        mpfr_t temp_var_365;
        mpfr_t temp_var_366;
        mpfr_t temp_var_367;
        mpfr_t temp_var_368;
        mpfr_t temp_var_369;
        mpfr_t temp_var_370;
        mpfr_t temp_var_371;
        mpfr_t temp_var_372;
        mpfr_t temp_var_373;
        mpfr_t temp_var_374;
        mpfr_t temp_var_375;
        mpfr_t temp_var_376;
        mpfr_t temp_var_377;
        mpfr_t temp_var_378;
        mpfr_t temp_var_379;
        mpfr_t temp_var_380;
        mpfr_t temp_var_381;
        mpfr_t temp_var_382;
        mpfr_t temp_var_383;
        mpfr_t temp_var_384;
        mpfr_t temp_var_385;
        mpfr_t temp_var_386;
        mpfr_t temp_var_387;
        mpfr_t temp_var_388;
        mpfr_t temp_var_389;
        mpfr_t temp_var_390;
        mpfr_t temp_var_391;
        mpfr_t temp_var_392;
        mpfr_t temp_var_393;
        mpfr_t temp_var_394;
        mpfr_t temp_var_395;
        mpfr_t temp_var_396;
        mpfr_t temp_var_397;
        mpfr_t temp_var_398;
        mpfr_t temp_var_399;
        mpfr_t temp_var_400;
        mpfr_t temp_var_401;
        mpfr_t temp_var_402;
        mpfr_t temp_var_403;
        mpfr_t temp_var_404;
        mpfr_t temp_var_405;
        mpfr_t temp_var_406;
        mpfr_t temp_var_407;
        mpfr_t temp_var_408;
        mpfr_t temp_var_409;
        mpfr_t temp_var_410;
        mpfr_t temp_var_411;
        mpfr_t temp_var_412;
        mpfr_t temp_var_413;
        mpfr_t temp_var_414;
        mpfr_t temp_var_415;
        mpfr_t temp_var_416;
        mpfr_t temp_var_417;
        mpfr_t temp_var_418;
        mpfr_t temp_var_419;
        mpfr_t temp_var_420;
        mpfr_t temp_var_421;
        mpfr_t temp_var_422;
        mpfr_t temp_var_423;
        mpfr_t temp_var_424;
        mpfr_t temp_var_425;
        mpfr_t temp_var_426;
        mpfr_t temp_var_427;
        mpfr_t temp_var_428;
        mpfr_t temp_var_429;
        mpfr_t temp_var_430;
        mpfr_t temp_var_431;
        mpfr_t temp_var_432;
        mpfr_t temp_var_433;
        mpfr_t temp_var_434;
        mpfr_t temp_var_435;
        mpfr_t temp_var_436;
        mpfr_t temp_var_437;
        mpfr_t temp_var_438;
        mpfr_t temp_var_439;
        mpfr_t temp_var_440;
        mpfr_t temp_var_441;
        mpfr_t temp_var_442;
        mpfr_t temp_var_443;
        mpfr_t temp_var_444;
        mpfr_t temp_var_445;
        mpfr_t temp_var_446;
        mpfr_t temp_var_447;
        mpfr_t temp_var_448;
        mpfr_t temp_var_449;
        mpfr_t temp_var_450;
        mpfr_t temp_var_451;
        mpfr_t temp_var_452;
        mpfr_t temp_var_453;
        mpfr_t temp_var_454;
        mpfr_t temp_var_455;
        mpfr_t temp_var_456;
        mpfr_t temp_var_457;
        mpfr_t temp_var_458;
        mpfr_t temp_var_459;
        mpfr_t temp_var_460;
        mpfr_t temp_var_461;
        mpfr_t temp_var_462;
        mpfr_t temp_var_463;
        mpfr_t temp_var_464;
        mpfr_t temp_var_465;
        mpfr_t temp_var_466;
        mpfr_t temp_var_467;
        mpfr_t temp_var_468;
        mpfr_t temp_var_469;
        mpfr_t temp_var_470;
        mpfr_t temp_var_471;
        mpfr_t temp_var_472;
        mpfr_t temp_var_473;
        mpfr_t temp_var_474;
        mpfr_t temp_var_475;
        mpfr_t temp_var_476;
        mpfr_t temp_var_477;
        mpfr_t temp_var_478;
        mpfr_t temp_var_479;
        mpfr_t temp_var_480;
        mpfr_t temp_var_481;
        mpfr_t temp_var_482;
        mpfr_t temp_var_483;
        mpfr_t temp_var_484;
        mpfr_t temp_var_485;
        mpfr_t temp_var_486;
        mpfr_t temp_var_487;
        mpfr_t temp_var_488;
        mpfr_t temp_var_489;
        mpfr_t temp_var_490;
        mpfr_t temp_var_491;
        mpfr_t temp_var_492;
        mpfr_t temp_var_493;
        mpfr_t temp_var_494;
        mpfr_t temp_var_495;
        mpfr_t temp_var_496;
        mpfr_t temp_var_497;
        mpfr_t temp_var_498;
        mpfr_t temp_var_499;
        mpfr_t temp_var_500;
        mpfr_t temp_var_501;
        mpfr_t temp_var_502;
        mpfr_t temp_var_503;
        mpfr_t temp_var_504;
        mpfr_t temp_var_505;
        mpfr_t temp_var_506;
        mpfr_t temp_var_507;
        mpfr_t temp_var_508;
        mpfr_t temp_var_509;
        mpfr_t temp_var_510;
        mpfr_t temp_var_511;
        mpfr_t temp_var_512;
        mpfr_t temp_var_513;
        mpfr_t temp_var_514;
        mpfr_t temp_var_515;
        mpfr_t temp_var_516;
        mpfr_t temp_var_517;
        mpfr_t temp_var_518;
        mpfr_t temp_var_519;
        mpfr_t temp_var_520;
        mpfr_t temp_var_521;
        mpfr_t temp_var_522;
        mpfr_t temp_var_523;
        mpfr_t temp_var_524;
        mpfr_t temp_var_525;
        mpfr_t temp_var_526;
        mpfr_t temp_var_527;
        mpfr_t temp_var_528;
        mpfr_t temp_var_529;
        mpfr_t temp_var_530;
        mpfr_t temp_var_531;
        mpfr_t temp_var_532;
        mpfr_t temp_var_533;
        mpfr_t temp_var_534;
        mpfr_t temp_var_535;
        mpfr_t temp_var_536;
        mpfr_t temp_var_537;
        mpfr_t temp_var_538;
        mpfr_t temp_var_539;
        mpfr_t temp_var_540;
        mpfr_t temp_var_541;
        mpfr_t temp_var_542;
        mpfr_t temp_var_543;
        mpfr_t temp_var_544;
        mpfr_t temp_var_545;
        mpfr_t temp_var_546;
        mpfr_t temp_var_547;
        mpfr_t temp_var_548;
        mpfr_t temp_var_549;
        mpfr_t temp_var_550;
        mpfr_t temp_var_551;
        mpfr_t temp_var_552;
        mpfr_t temp_var_553;
        mpfr_t temp_var_554;
        mpfr_t temp_var_555;
        mpfr_t temp_var_556;
        mpfr_t temp_var_557;
        mpfr_t temp_var_558;
        mpfr_t temp_var_559;
        mpfr_t temp_var_560;
        mpfr_t temp_var_561;
        mpfr_t temp_var_562;
        mpfr_t temp_var_563;
        mpfr_t temp_var_564;
        mpfr_t temp_var_565;
        mpfr_t temp_var_566;
        mpfr_t temp_var_567;
        mpfr_t temp_var_568;
        mpfr_t temp_var_569;
        mpfr_t temp_var_570;
        mpfr_t temp_var_571;
        mpfr_t temp_var_572;
        mpfr_t temp_var_573;
        mpfr_t temp_var_574;
        mpfr_t temp_var_575;
        mpfr_t temp_var_576;
        mpfr_t temp_var_577;
        mpfr_t temp_var_578;
        mpfr_t temp_var_579;
        mpfr_t temp_var_580;
        mpfr_t temp_var_581;
        mpfr_t temp_var_582;
        mpfr_t temp_var_583;
        mpfr_t temp_var_584;
        mpfr_t temp_var_585;
        mpfr_t temp_var_586;
        mpfr_t temp_var_587;
        mpfr_t temp_var_588;
        mpfr_t temp_var_589;
        mpfr_t temp_var_590;
        mpfr_t temp_var_591;
        mpfr_t temp_var_592;
        mpfr_t temp_var_593;
        mpfr_t temp_var_594;
        mpfr_t temp_var_595;
        mpfr_t temp_var_596;
        mpfr_t temp_var_597;
        mpfr_t temp_var_598;
        mpfr_t temp_var_599;
        mpfr_t temp_var_600;
        mpfr_t temp_var_601;
        mpfr_t temp_var_602;
        mpfr_t temp_var_603;
        mpfr_t temp_var_604;
        mpfr_t temp_var_605;
        mpfr_t temp_var_606;
        mpfr_t temp_var_607;
        mpfr_t temp_var_608;
        mpfr_t temp_var_609;
        mpfr_t temp_var_610;
        mpfr_t temp_var_611;
        mpfr_t temp_var_612;
        mpfr_t temp_var_613;
        mpfr_t temp_var_614;
        mpfr_t temp_var_615;
        mpfr_t temp_var_616;
        mpfr_t temp_var_617;
        mpfr_t temp_var_618;
        mpfr_t temp_var_619;
        mpfr_t temp_var_620;
        mpfr_t temp_var_621;
        mpfr_t temp_var_622;
        mpfr_t temp_var_623;
        mpfr_t temp_var_624;
        mpfr_t temp_var_625;
        mpfr_t temp_var_626;
        mpfr_t temp_var_627;
        mpfr_t temp_var_628;
        mpfr_t temp_var_629;
        mpfr_t temp_var_630;
        mpfr_t temp_var_631;
        mpfr_t temp_var_632;
        mpfr_t temp_var_633;
        mpfr_t temp_var_634;
        mpfr_t temp_var_635;
        mpfr_t temp_var_636;
        mpfr_t temp_var_637;
        mpfr_t temp_var_638;
        mpfr_t temp_var_639;
        mpfr_t temp_var_640;
        mpfr_t temp_var_641;
        mpfr_t temp_var_642;
        mpfr_t temp_var_643;
        mpfr_t temp_var_644;
        mpfr_t temp_var_645;
        mpfr_t temp_var_646;
        mpfr_t temp_var_647;
        mpfr_t temp_var_648;
        mpfr_t temp_var_649;
        mpfr_t temp_var_650;
        mpfr_t temp_var_651;
        mpfr_t temp_var_652;
        mpfr_t temp_var_653;
        mpfr_t temp_var_654;
        mpfr_t temp_var_655;
        mpfr_t temp_var_656;
        mpfr_t temp_var_657;
        mpfr_t temp_var_658;
        mpfr_t temp_var_659;
        mpfr_t temp_var_660;
        mpfr_t temp_var_661;
        mpfr_t temp_var_662;
        mpfr_t temp_var_663;
        mpfr_t temp_var_664;
        mpfr_t temp_var_665;
        mpfr_t temp_var_666;
        mpfr_t temp_var_667;
        mpfr_t temp_var_668;
        mpfr_t temp_var_669;
        mpfr_t temp_var_670;
        mpfr_t temp_var_671;
        mpfr_t temp_var_672;
        mpfr_t temp_var_673;
        mpfr_t temp_var_674;
        mpfr_t temp_var_675;
        mpfr_t temp_var_676;
        mpfr_t temp_var_677;
        mpfr_t temp_var_678;
        mpfr_t temp_var_679;
        mpfr_t temp_var_680;
        mpfr_t temp_var_681;
        mpfr_t temp_var_682;
        mpfr_t temp_var_683;
        mpfr_t temp_var_684;
        mpfr_t temp_var_685;
        mpfr_t temp_var_686;
        mpfr_t temp_var_687;
        mpfr_t temp_var_688;
        mpfr_t temp_var_689;
        mpfr_t temp_var_690;
        mpfr_t temp_var_691;
        mpfr_t temp_var_692;
        mpfr_t temp_var_693;
        mpfr_t temp_var_694;
        mpfr_t temp_var_695;
        mpfr_t temp_var_696;
        mpfr_t temp_var_697;
        mpfr_t temp_var_698;
        mpfr_t temp_var_699;
        mpfr_t temp_var_700;
        mpfr_t temp_var_701;
        mpfr_t temp_var_702;
        mpfr_t temp_var_703;
        mpfr_t temp_var_704;
        mpfr_t temp_var_705;
        mpfr_t temp_var_706;
        mpfr_t temp_var_707;
        mpfr_t temp_var_708;
        mpfr_t temp_var_709;
        mpfr_t temp_var_710;
        mpfr_t temp_var_711;
        mpfr_t temp_var_712;
        mpfr_t temp_var_713;
        mpfr_t temp_var_714;
        mpfr_t temp_var_715;
        mpfr_t temp_var_716;
        mpfr_t temp_var_717;
        mpfr_t temp_var_718;
        mpfr_t temp_var_719;
        mpfr_t temp_var_720;
        mpfr_t temp_var_721;
        mpfr_t temp_var_722;
        mpfr_t temp_var_723;
        mpfr_t temp_var_724;
        mpfr_t temp_var_725;
        mpfr_t temp_var_726;
        mpfr_t temp_var_727;
        mpfr_t temp_var_728;
        mpfr_t temp_var_729;
        mpfr_t temp_var_730;
        mpfr_t temp_var_731;
        mpfr_t temp_var_732;
        mpfr_t temp_var_733;
        mpfr_t temp_var_734;
        mpfr_t temp_var_735;
        mpfr_t temp_var_736;
        mpfr_t temp_var_737;
        mpfr_t temp_var_738;
        mpfr_t temp_var_739;
        mpfr_t temp_var_740;
        mpfr_t temp_var_741;
        mpfr_t temp_var_742;
        mpfr_t temp_var_743;
        mpfr_t temp_var_744;
        mpfr_t temp_var_745;
        mpfr_t temp_var_746;
        mpfr_t temp_var_747;
        mpfr_t temp_var_748;
        mpfr_t temp_var_749;
        mpfr_t temp_var_750;
        mpfr_t temp_var_751;
        mpfr_t temp_var_752;
        mpfr_t temp_var_753;
        mpfr_t temp_var_754;
        mpfr_t temp_var_755;
        mpfr_t temp_var_756;
        mpfr_t temp_var_757;
        mpfr_t temp_var_758;
        mpfr_t temp_var_759;
        mpfr_t temp_var_760;
        mpfr_t temp_var_761;
        mpfr_t temp_var_762;
        mpfr_t temp_var_763;
        mpfr_t temp_var_764;
        mpfr_t temp_var_765;
        mpfr_t temp_var_766;
        mpfr_t temp_var_767;
        mpfr_t temp_var_768;
        mpfr_t temp_var_769;
        mpfr_t temp_var_770;
        mpfr_t temp_var_771;
        mpfr_t temp_var_772;
        mpfr_t temp_var_773;
        mpfr_t temp_var_774;
        mpfr_t temp_var_775;
        mpfr_t temp_var_776;
        mpfr_t temp_var_777;
        mpfr_t temp_var_778;
        mpfr_t temp_var_779;
        mpfr_t temp_var_780;
        mpfr_t temp_var_781;
        mpfr_t temp_var_782;
        mpfr_t temp_var_783;
        mpfr_t temp_var_784;
        mpfr_t temp_var_785;
        mpfr_t temp_var_786;
        mpfr_t temp_var_787;
        mpfr_t temp_var_788;
        mpfr_t temp_var_789;
        mpfr_t temp_var_790;
        mpfr_t temp_var_791;
        mpfr_t temp_var_792;
        mpfr_t temp_var_793;
        mpfr_t temp_var_794;
        mpfr_t temp_var_795;
        mpfr_t temp_var_796;
        mpfr_t temp_var_797;
        mpfr_t temp_var_798;
        mpfr_t temp_var_799;
        mpfr_t temp_var_800;
        mpfr_t temp_var_801;
        mpfr_t temp_var_802;
        mpfr_t temp_var_803;
        mpfr_t temp_var_804;
        mpfr_t temp_var_805;
        mpfr_t temp_var_806;
        mpfr_t temp_var_807;
        mpfr_t temp_var_808;
        mpfr_t temp_var_809;
        mpfr_t temp_var_810;
        mpfr_t temp_var_811;
        mpfr_t temp_var_812;
        mpfr_t temp_var_813;
        mpfr_t temp_var_814;
        mpfr_t temp_var_815;
        mpfr_t temp_var_816;
        mpfr_t temp_var_817;
        mpfr_t temp_var_818;
        mpfr_t temp_var_819;
        mpfr_t temp_var_820;
        mpfr_t temp_var_821;
        mpfr_t temp_var_822;
        mpfr_t temp_var_823;
        mpfr_t temp_var_824;
        mpfr_t temp_var_825;
        mpfr_t temp_var_826;
        mpfr_t temp_var_827;
        mpfr_t temp_var_828;
        mpfr_t temp_var_829;
        mpfr_t temp_var_830;
        mpfr_t temp_var_831;
        mpfr_t temp_var_832;
        mpfr_t temp_var_833;
        mpfr_t temp_var_834;
        mpfr_t temp_var_835;
        mpfr_t temp_var_836;
        mpfr_t temp_var_837;
        mpfr_t temp_var_838;
        mpfr_t temp_var_839;
        mpfr_t temp_var_840;
        mpfr_t temp_var_841;
        mpfr_t temp_var_842;
        mpfr_t temp_var_843;
        mpfr_t temp_var_844;
        mpfr_t temp_var_845;
        mpfr_t temp_var_846;
        mpfr_t temp_var_847;
        mpfr_t temp_var_848;
        mpfr_t temp_var_849;
        mpfr_t temp_var_850;
        mpfr_t temp_var_851;
        mpfr_t temp_var_852;
        mpfr_t temp_var_853;
        mpfr_t temp_var_854;
        mpfr_t temp_var_855;
        mpfr_t temp_var_856;
        mpfr_t temp_var_857;
        mpfr_t temp_var_858;
        mpfr_t temp_var_859;
        mpfr_t temp_var_860;
        mpfr_t temp_var_861;
        mpfr_t temp_var_862;
        mpfr_t temp_var_863;
        mpfr_t temp_var_864;
        mpfr_t temp_var_865;
        mpfr_t temp_var_866;
        mpfr_t temp_var_867;
        mpfr_t temp_var_868;
        mpfr_t temp_var_869;
        mpfr_t temp_var_870;
        mpfr_t temp_var_871;
        mpfr_t temp_var_872;
        mpfr_t temp_var_873;
        mpfr_t temp_var_874;
        mpfr_t temp_var_875;
        mpfr_t temp_var_876;
        mpfr_t temp_var_877;
        mpfr_t temp_var_878;
        mpfr_t temp_var_879;
        mpfr_t temp_var_880;
        mpfr_t temp_var_881;
        mpfr_t temp_var_882;
        mpfr_t temp_var_883;
        mpfr_t temp_var_884;
        mpfr_t temp_var_885;
        mpfr_t temp_var_886;
        mpfr_t temp_var_887;
        mpfr_t temp_var_888;
        mpfr_t temp_var_889;
        mpfr_t temp_var_890;
        mpfr_t temp_var_891;
        mpfr_t temp_var_892;
        mpfr_t temp_var_893;
        mpfr_t temp_var_894;
        mpfr_t temp_var_895;
        mpfr_t temp_var_896;
        mpfr_t temp_var_897;
        mpfr_t temp_var_898;
        mpfr_t temp_var_899;
        mpfr_t temp_var_900;
        mpfr_t temp_var_901;
        mpfr_t temp_var_902;
        mpfr_t temp_var_903;
        mpfr_t temp_var_904;
        mpfr_t temp_var_905;
        mpfr_t temp_var_906;
        mpfr_t temp_var_907;
        mpfr_t temp_var_908;
        mpfr_t temp_var_909;
        mpfr_t temp_var_910;
        mpfr_t temp_var_911;
        mpfr_t temp_var_912;
        mpfr_t temp_var_913;
        mpfr_t temp_var_914;
        mpfr_t temp_var_915;
        mpfr_t temp_var_916;
        mpfr_t temp_var_917;
        mpfr_t temp_var_918;
        mpfr_t temp_var_919;
  mpfr_t ux_LBM_showGridStatistics;
  mpfr_t uy_LBM_showGridStatistics;
  mpfr_t uz_LBM_showGridStatistics;
  mpfr_t minU2_LBM_showGridStatistics;
  mpfr_t maxU2_LBM_showGridStatistics;
  mpfr_t u2_LBM_showGridStatistics;
  mpfr_t minRho_LBM_showGridStatistics;
  mpfr_t maxRho_LBM_showGridStatistics;
  mpfr_t rho_LBM_showGridStatistics;
  mpfr_t mass_LBM_showGridStatistics;
        mpfr_t temp_var_920;
        mpfr_t temp_var_921;
        mpfr_t temp_var_922;
        mpfr_t temp_var_923;
        mpfr_t temp_var_924;
        mpfr_t temp_var_925;
        mpfr_t temp_var_926;
        mpfr_t temp_var_927;
        mpfr_t temp_var_928;
        mpfr_t temp_var_929;
        mpfr_t temp_var_930;
        mpfr_t temp_var_931;
        mpfr_t temp_var_932;
        mpfr_t temp_var_933;
        mpfr_t temp_var_934;
        mpfr_t temp_var_935;
        mpfr_t temp_var_936;
        mpfr_t temp_var_937;
        mpfr_t temp_var_938;
        mpfr_t temp_var_939;
        mpfr_t temp_var_940;
        mpfr_t temp_var_941;
        mpfr_t temp_var_942;
        mpfr_t temp_var_943;
        mpfr_t temp_var_944;
        mpfr_t temp_var_945;
        mpfr_t temp_var_946;
        mpfr_t temp_var_947;
        mpfr_t temp_var_948;
        mpfr_t temp_var_949;
        mpfr_t temp_var_950;
        mpfr_t temp_var_951;
        mpfr_t temp_var_952;
        mpfr_t temp_var_953;
        mpfr_t temp_var_954;
        mpfr_t temp_var_955;
            mpfr_t temp_var_956;
            mpfr_t temp_var_957;
            mpfr_t temp_var_958;
            mpfr_t temp_var_959;
            mpfr_t temp_var_960;
            mpfr_t temp_var_961;
            mpfr_t temp_var_962;
            mpfr_t temp_var_963;
            mpfr_t temp_var_964;
            mpfr_t temp_var_965;
            mpfr_t temp_var_966;
            mpfr_t temp_var_967;
            mpfr_t temp_var_968;
            mpfr_t temp_var_969;
            mpfr_t temp_var_970;
            mpfr_t temp_var_971;
            mpfr_t temp_var_972;
            mpfr_t temp_var_973;
            mpfr_t temp_var_974;
            mpfr_t temp_var_975;
            mpfr_t temp_var_976;
            mpfr_t temp_var_977;
            mpfr_t temp_var_978;
            mpfr_t temp_var_979;
            mpfr_t temp_var_980;
            mpfr_t temp_var_981;
            mpfr_t temp_var_982;
            mpfr_t temp_var_983;
            mpfr_t temp_var_984;
            mpfr_t temp_var_985;
            mpfr_t temp_var_986;
            mpfr_t temp_var_987;
            mpfr_t temp_var_988;
            mpfr_t temp_var_989;
            mpfr_t temp_var_990;
            mpfr_t temp_var_991;
            mpfr_t temp_var_992;
            mpfr_t temp_var_993;
            mpfr_t temp_var_994;
            mpfr_t temp_var_995;
            mpfr_t temp_var_996;
            mpfr_t temp_var_997;
            mpfr_t temp_var_998;
            mpfr_t temp_var_999;
            mpfr_t temp_var_1000;
            mpfr_t temp_var_1001;
            mpfr_t temp_var_1002;
            mpfr_t temp_var_1003;
            mpfr_t temp_var_1004;
            mpfr_t temp_var_1005;
            mpfr_t temp_var_1006;
            mpfr_t temp_var_1007;
            mpfr_t temp_var_1008;
            mpfr_t temp_var_1009;
            mpfr_t temp_var_1010;
            mpfr_t temp_var_1011;
            mpfr_t temp_var_1012;
            mpfr_t temp_var_1013;
            mpfr_t temp_var_1014;
            mpfr_t temp_var_1015;
int config_vals[LEN];

int init_mpfr() { 
  mpfr_init2(ux_LBM_performStreamCollide, config_vals[0]);
  mpfr_init2(uy_LBM_performStreamCollide, config_vals[1]);
  mpfr_init2(uz_LBM_performStreamCollide, config_vals[2]);
  mpfr_init2(u2_LBM_performStreamCollide, config_vals[3]);
  mpfr_init2(rho_LBM_performStreamCollide, config_vals[4]);
        mpfr_init2 (temp_var_1, 64);
        mpfr_init2 (temp_var_2, 64);
        mpfr_init2 (temp_var_3, 64);
        mpfr_init2 (temp_var_4, 64);
        mpfr_init2 (temp_var_5, 64);
        mpfr_init2 (temp_var_6, 64);
        mpfr_init2 (temp_var_7, 64);
        mpfr_init2 (temp_var_8, 64);
        mpfr_init2 (temp_var_9, 64);
        mpfr_init2 (temp_var_10, 64);
        mpfr_init2 (temp_var_11, 64);
        mpfr_init2 (temp_var_12, 64);
        mpfr_init2 (temp_var_13, 64);
        mpfr_init2 (temp_var_14, 64);
        mpfr_init2 (temp_var_15, 64);
        mpfr_init2 (temp_var_16, 64);
        mpfr_init2 (temp_var_17, 64);
        mpfr_init2 (temp_var_18, 64);
        mpfr_init2 (temp_var_19, 64);
        mpfr_init2 (temp_var_20, 64);
        mpfr_init2 (temp_var_21, 64);
        mpfr_init2 (temp_var_22, 64);
        mpfr_init2 (temp_var_23, 64);
        mpfr_init2 (temp_var_24, 64);
        mpfr_init2 (temp_var_25, 64);
        mpfr_init2 (temp_var_26, 64);
        mpfr_init2 (temp_var_27, 64);
        mpfr_init2 (temp_var_28, 64);
        mpfr_init2 (temp_var_29, 64);
        mpfr_init2 (temp_var_30, 64);
        mpfr_init2 (temp_var_31, 64);
        mpfr_init2 (temp_var_32, 64);
        mpfr_init2 (temp_var_33, 64);
        mpfr_init2 (temp_var_34, 64);
        mpfr_init2 (temp_var_35, 64);
        mpfr_init2 (temp_var_36, 64);
        mpfr_init2 (temp_var_37, 64);
        mpfr_init2 (temp_var_38, 64);
        mpfr_init2 (temp_var_39, 64);
        mpfr_init2 (temp_var_40, 64);
        mpfr_init2 (temp_var_41, 64);
        mpfr_init2 (temp_var_42, 64);
        mpfr_init2 (temp_var_43, 64);
        mpfr_init2 (temp_var_44, 64);
        mpfr_init2 (temp_var_45, 64);
        mpfr_init2 (temp_var_46, 64);
        mpfr_init2 (temp_var_47, 64);
        mpfr_init2 (temp_var_48, 64);
        mpfr_init2 (temp_var_49, 64);
        mpfr_init2 (temp_var_50, 64);
        mpfr_init2 (temp_var_51, 64);
        mpfr_init2 (temp_var_52, 64);
        mpfr_init2 (temp_var_53, 64);
        mpfr_init2 (temp_var_54, 64);
        mpfr_init2 (temp_var_55, 64);
        mpfr_init2 (temp_var_56, 64);
        mpfr_init2 (temp_var_57, 64);
        mpfr_init2 (temp_var_58, 64);
        mpfr_init2 (temp_var_59, 64);
        mpfr_init2 (temp_var_60, 64);
        mpfr_init2 (temp_var_61, 64);
        mpfr_init2 (temp_var_62, 64);
        mpfr_init2 (temp_var_63, 64);
        mpfr_init2 (temp_var_64, 64);
        mpfr_init2 (temp_var_65, 64);
        mpfr_init2 (temp_var_66, 64);
        mpfr_init2 (temp_var_67, 64);
        mpfr_init2 (temp_var_68, 64);
        mpfr_init2 (temp_var_69, 64);
        mpfr_init2 (temp_var_70, 64);
        mpfr_init2 (temp_var_71, 64);
        mpfr_init2 (temp_var_72, 64);
        mpfr_init2 (temp_var_73, 64);
        mpfr_init2 (temp_var_74, 64);
        mpfr_init2 (temp_var_75, 64);
        mpfr_init2 (temp_var_76, 64);
        mpfr_init2 (temp_var_77, 64);
        mpfr_init2 (temp_var_78, 64);
        mpfr_init2 (temp_var_79, 64);
        mpfr_init2 (temp_var_80, 64);
        mpfr_init2 (temp_var_81, 64);
        mpfr_init2 (temp_var_82, 64);
        mpfr_init2 (temp_var_83, 64);
        mpfr_init2 (temp_var_84, 64);
        mpfr_init2 (temp_var_85, 64);
        mpfr_init2 (temp_var_86, 64);
        mpfr_init2 (temp_var_87, 64);
        mpfr_init2 (temp_var_88, 64);
        mpfr_init2 (temp_var_89, 64);
        mpfr_init2 (temp_var_90, 64);
        mpfr_init2 (temp_var_91, 64);
        mpfr_init2 (temp_var_92, 64);
        mpfr_init2 (temp_var_93, 64);
        mpfr_init2 (temp_var_94, 64);
        mpfr_init2 (temp_var_95, 64);
        mpfr_init2 (temp_var_96, 64);
        mpfr_init2 (temp_var_97, 64);
        mpfr_init2 (temp_var_98, 64);
        mpfr_init2 (temp_var_99, 64);
        mpfr_init2 (temp_var_100, 64);
        mpfr_init2 (temp_var_101, 64);
        mpfr_init2 (temp_var_102, 64);
        mpfr_init2 (temp_var_103, 64);
        mpfr_init2 (temp_var_104, 64);
        mpfr_init2 (temp_var_105, 64);
        mpfr_init2 (temp_var_106, 64);
        mpfr_init2 (temp_var_107, 64);
        mpfr_init2 (temp_var_108, 64);
        mpfr_init2 (temp_var_109, 64);
        mpfr_init2 (temp_var_110, 64);
        mpfr_init2 (temp_var_111, 64);
        mpfr_init2 (temp_var_112, 64);
        mpfr_init2 (temp_var_113, 64);
        mpfr_init2 (temp_var_114, 64);
        mpfr_init2 (temp_var_115, 64);
        mpfr_init2 (temp_var_116, 64);
        mpfr_init2 (temp_var_117, 64);
        mpfr_init2 (temp_var_118, 64);
        mpfr_init2 (temp_var_119, 64);
        mpfr_init2 (temp_var_120, 64);
        mpfr_init2 (temp_var_121, 64);
        mpfr_init2 (temp_var_122, 64);
        mpfr_init2 (temp_var_123, 64);
        mpfr_init2 (temp_var_124, 64);
        mpfr_init2 (temp_var_125, 64);
        mpfr_init2 (temp_var_126, 64);
        mpfr_init2 (temp_var_127, 64);
        mpfr_init2 (temp_var_128, 64);
        mpfr_init2 (temp_var_129, 64);
        mpfr_init2 (temp_var_130, 64);
        mpfr_init2 (temp_var_131, 64);
        mpfr_init2 (temp_var_132, 64);
        mpfr_init2 (temp_var_133, 64);
        mpfr_init2 (temp_var_134, 64);
        mpfr_init2 (temp_var_135, 64);
        mpfr_init2 (temp_var_136, 64);
        mpfr_init2 (temp_var_137, 64);
        mpfr_init2 (temp_var_138, 64);
        mpfr_init2 (temp_var_139, 64);
        mpfr_init2 (temp_var_140, 64);
        mpfr_init2 (temp_var_141, 64);
        mpfr_init2 (temp_var_142, 64);
        mpfr_init2 (temp_var_143, 64);
        mpfr_init2 (temp_var_144, 64);
        mpfr_init2 (temp_var_145, 64);
        mpfr_init2 (temp_var_146, 64);
        mpfr_init2 (temp_var_147, 64);
        mpfr_init2 (temp_var_148, 64);
        mpfr_init2 (temp_var_149, 64);
        mpfr_init2 (temp_var_150, 64);
        mpfr_init2 (temp_var_151, 64);
        mpfr_init2 (temp_var_152, 64);
        mpfr_init2 (temp_var_153, 64);
        mpfr_init2 (temp_var_154, 64);
        mpfr_init2 (temp_var_155, 64);
        mpfr_init2 (temp_var_156, 64);
        mpfr_init2 (temp_var_157, 64);
        mpfr_init2 (temp_var_158, 64);
        mpfr_init2 (temp_var_159, 64);
        mpfr_init2 (temp_var_160, 64);
        mpfr_init2 (temp_var_161, 64);
        mpfr_init2 (temp_var_162, 64);
        mpfr_init2 (temp_var_163, 64);
        mpfr_init2 (temp_var_164, 64);
        mpfr_init2 (temp_var_165, 64);
        mpfr_init2 (temp_var_166, 64);
        mpfr_init2 (temp_var_167, 64);
        mpfr_init2 (temp_var_168, 64);
        mpfr_init2 (temp_var_169, 64);
        mpfr_init2 (temp_var_170, 64);
        mpfr_init2 (temp_var_171, 64);
        mpfr_init2 (temp_var_172, 64);
        mpfr_init2 (temp_var_173, 64);
        mpfr_init2 (temp_var_174, 64);
        mpfr_init2 (temp_var_175, 64);
        mpfr_init2 (temp_var_176, 64);
        mpfr_init2 (temp_var_177, 64);
        mpfr_init2 (temp_var_178, 64);
        mpfr_init2 (temp_var_179, 64);
        mpfr_init2 (temp_var_180, 64);
        mpfr_init2 (temp_var_181, 64);
        mpfr_init2 (temp_var_182, 64);
        mpfr_init2 (temp_var_183, 64);
        mpfr_init2 (temp_var_184, 64);
        mpfr_init2 (temp_var_185, 64);
        mpfr_init2 (temp_var_186, 64);
        mpfr_init2 (temp_var_187, 64);
        mpfr_init2 (temp_var_188, 64);
        mpfr_init2 (temp_var_189, 64);
        mpfr_init2 (temp_var_190, 64);
        mpfr_init2 (temp_var_191, 64);
        mpfr_init2 (temp_var_192, 64);
        mpfr_init2 (temp_var_193, 64);
        mpfr_init2 (temp_var_194, 64);
        mpfr_init2 (temp_var_195, 64);
        mpfr_init2 (temp_var_196, 64);
        mpfr_init2 (temp_var_197, 64);
        mpfr_init2 (temp_var_198, 64);
        mpfr_init2 (temp_var_199, 64);
        mpfr_init2 (temp_var_200, 64);
        mpfr_init2 (temp_var_201, 64);
        mpfr_init2 (temp_var_202, 64);
        mpfr_init2 (temp_var_203, 64);
        mpfr_init2 (temp_var_204, 64);
        mpfr_init2 (temp_var_205, 64);
        mpfr_init2 (temp_var_206, 64);
        mpfr_init2 (temp_var_207, 64);
        mpfr_init2 (temp_var_208, 64);
        mpfr_init2 (temp_var_209, 64);
        mpfr_init2 (temp_var_210, 64);
        mpfr_init2 (temp_var_211, 64);
        mpfr_init2 (temp_var_212, 64);
        mpfr_init2 (temp_var_213, 64);
        mpfr_init2 (temp_var_214, 64);
        mpfr_init2 (temp_var_215, 64);
        mpfr_init2 (temp_var_216, 64);
        mpfr_init2 (temp_var_217, 64);
        mpfr_init2 (temp_var_218, 64);
        mpfr_init2 (temp_var_219, 64);
        mpfr_init2 (temp_var_220, 64);
        mpfr_init2 (temp_var_221, 64);
        mpfr_init2 (temp_var_222, 64);
        mpfr_init2 (temp_var_223, 64);
        mpfr_init2 (temp_var_224, 64);
        mpfr_init2 (temp_var_225, 64);
        mpfr_init2 (temp_var_226, 64);
        mpfr_init2 (temp_var_227, 64);
        mpfr_init2 (temp_var_228, 64);
        mpfr_init2 (temp_var_229, 64);
        mpfr_init2 (temp_var_230, 64);
        mpfr_init2 (temp_var_231, 64);
        mpfr_init2 (temp_var_232, 64);
        mpfr_init2 (temp_var_233, 64);
        mpfr_init2 (temp_var_234, 64);
        mpfr_init2 (temp_var_235, 64);
        mpfr_init2 (temp_var_236, 64);
        mpfr_init2 (temp_var_237, 64);
        mpfr_init2 (temp_var_238, 64);
        mpfr_init2 (temp_var_239, 64);
        mpfr_init2 (temp_var_240, 64);
        mpfr_init2 (temp_var_241, 64);
        mpfr_init2 (temp_var_242, 64);
        mpfr_init2 (temp_var_243, 64);
        mpfr_init2 (temp_var_244, 64);
        mpfr_init2 (temp_var_245, 64);
        mpfr_init2 (temp_var_246, 64);
        mpfr_init2 (temp_var_247, 64);
        mpfr_init2 (temp_var_248, 64);
        mpfr_init2 (temp_var_249, 64);
        mpfr_init2 (temp_var_250, 64);
        mpfr_init2 (temp_var_251, 64);
        mpfr_init2 (temp_var_252, 64);
        mpfr_init2 (temp_var_253, 64);
        mpfr_init2 (temp_var_254, 64);
        mpfr_init2 (temp_var_255, 64);
        mpfr_init2 (temp_var_256, 64);
        mpfr_init2 (temp_var_257, 64);
        mpfr_init2 (temp_var_258, 64);
        mpfr_init2 (temp_var_259, 64);
        mpfr_init2 (temp_var_260, 64);
        mpfr_init2 (temp_var_261, 64);
        mpfr_init2 (temp_var_262, 64);
        mpfr_init2 (temp_var_263, 64);
        mpfr_init2 (temp_var_264, 64);
        mpfr_init2 (temp_var_265, 64);
        mpfr_init2 (temp_var_266, 64);
        mpfr_init2 (temp_var_267, 64);
        mpfr_init2 (temp_var_268, 64);
        mpfr_init2 (temp_var_269, 64);
        mpfr_init2 (temp_var_270, 64);
        mpfr_init2 (temp_var_271, 64);
        mpfr_init2 (temp_var_272, 64);
        mpfr_init2 (temp_var_273, 64);
        mpfr_init2 (temp_var_274, 64);
        mpfr_init2 (temp_var_275, 64);
        mpfr_init2 (temp_var_276, 64);
        mpfr_init2 (temp_var_277, 64);
        mpfr_init2 (temp_var_278, 64);
        mpfr_init2 (temp_var_279, 64);
        mpfr_init2 (temp_var_280, 64);
        mpfr_init2 (temp_var_281, 64);
        mpfr_init2 (temp_var_282, 64);
        mpfr_init2 (temp_var_283, 64);
        mpfr_init2 (temp_var_284, 64);
        mpfr_init2 (temp_var_285, 64);
        mpfr_init2 (temp_var_286, 64);
        mpfr_init2 (temp_var_287, 64);
        mpfr_init2 (temp_var_288, 64);
        mpfr_init2 (temp_var_289, 64);
        mpfr_init2 (temp_var_290, 64);
        mpfr_init2 (temp_var_291, 64);
        mpfr_init2 (temp_var_292, 64);
        mpfr_init2 (temp_var_293, 64);
        mpfr_init2 (temp_var_294, 64);
        mpfr_init2 (temp_var_295, 64);
        mpfr_init2 (temp_var_296, 64);
        mpfr_init2 (temp_var_297, 64);
        mpfr_init2 (temp_var_298, 64);
        mpfr_init2 (temp_var_299, 64);
        mpfr_init2 (temp_var_300, 64);
        mpfr_init2 (temp_var_301, 64);
        mpfr_init2 (temp_var_302, 64);
        mpfr_init2 (temp_var_303, 64);
        mpfr_init2 (temp_var_304, 64);
        mpfr_init2 (temp_var_305, 64);
        mpfr_init2 (temp_var_306, 64);
        mpfr_init2 (temp_var_307, 64);
        mpfr_init2 (temp_var_308, 64);
        mpfr_init2 (temp_var_309, 64);
        mpfr_init2 (temp_var_310, 64);
        mpfr_init2 (temp_var_311, 64);
        mpfr_init2 (temp_var_312, 64);
        mpfr_init2 (temp_var_313, 64);
        mpfr_init2 (temp_var_314, 64);
        mpfr_init2 (temp_var_315, 64);
        mpfr_init2 (temp_var_316, 64);
  mpfr_init2(ux_LBM_handleInOutFlow, config_vals[5]);
  mpfr_init2(uy_LBM_handleInOutFlow, config_vals[6]);
  mpfr_init2(uz_LBM_handleInOutFlow, config_vals[7]);
  mpfr_init2(rho_LBM_handleInOutFlow, config_vals[8]);
  mpfr_init2(ux1_LBM_handleInOutFlow, config_vals[9]);
  mpfr_init2(uy1_LBM_handleInOutFlow, config_vals[10]);
  mpfr_init2(uz1_LBM_handleInOutFlow, config_vals[11]);
  mpfr_init2(rho1_LBM_handleInOutFlow, config_vals[12]);
  mpfr_init2(ux2_LBM_handleInOutFlow, config_vals[13]);
  mpfr_init2(uy2_LBM_handleInOutFlow, config_vals[14]);
  mpfr_init2(uz2_LBM_handleInOutFlow, config_vals[15]);
  mpfr_init2(rho2_LBM_handleInOutFlow, config_vals[16]);
  mpfr_init2(u2_LBM_handleInOutFlow, config_vals[17]);
  mpfr_init2(px_LBM_handleInOutFlow, config_vals[18]);
  mpfr_init2(py_LBM_handleInOutFlow, config_vals[19]);
        mpfr_init2 (temp_var_317, 64);
        mpfr_init2 (temp_var_318, 64);
        mpfr_init2 (temp_var_319, 64);
        mpfr_init2 (temp_var_320, 64);
        mpfr_init2 (temp_var_321, 64);
        mpfr_init2 (temp_var_322, 64);
        mpfr_init2 (temp_var_323, 64);
        mpfr_init2 (temp_var_324, 64);
        mpfr_init2 (temp_var_325, 64);
        mpfr_init2 (temp_var_326, 64);
        mpfr_init2 (temp_var_327, 64);
        mpfr_init2 (temp_var_328, 64);
        mpfr_init2 (temp_var_329, 64);
        mpfr_init2 (temp_var_330, 64);
        mpfr_init2 (temp_var_331, 64);
        mpfr_init2 (temp_var_332, 64);
        mpfr_init2 (temp_var_333, 64);
        mpfr_init2 (temp_var_334, 64);
        mpfr_init2 (temp_var_335, 64);
        mpfr_init2 (temp_var_336, 64);
        mpfr_init2 (temp_var_337, 64);
        mpfr_init2 (temp_var_338, 64);
        mpfr_init2 (temp_var_339, 64);
        mpfr_init2 (temp_var_340, 64);
        mpfr_init2 (temp_var_341, 64);
        mpfr_init2 (temp_var_342, 64);
        mpfr_init2 (temp_var_343, 64);
        mpfr_init2 (temp_var_344, 64);
        mpfr_init2 (temp_var_345, 64);
        mpfr_init2 (temp_var_346, 64);
        mpfr_init2 (temp_var_347, 64);
        mpfr_init2 (temp_var_348, 64);
        mpfr_init2 (temp_var_349, 64);
        mpfr_init2 (temp_var_350, 64);
        mpfr_init2 (temp_var_351, 64);
        mpfr_init2 (temp_var_352, 64);
        mpfr_init2 (temp_var_353, 64);
        mpfr_init2 (temp_var_354, 64);
        mpfr_init2 (temp_var_355, 64);
        mpfr_init2 (temp_var_356, 64);
        mpfr_init2 (temp_var_357, 64);
        mpfr_init2 (temp_var_358, 64);
        mpfr_init2 (temp_var_359, 64);
        mpfr_init2 (temp_var_360, 64);
        mpfr_init2 (temp_var_361, 64);
        mpfr_init2 (temp_var_362, 64);
        mpfr_init2 (temp_var_363, 64);
        mpfr_init2 (temp_var_364, 64);
        mpfr_init2 (temp_var_365, 64);
        mpfr_init2 (temp_var_366, 64);
        mpfr_init2 (temp_var_367, 64);
        mpfr_init2 (temp_var_368, 64);
        mpfr_init2 (temp_var_369, 64);
        mpfr_init2 (temp_var_370, 64);
        mpfr_init2 (temp_var_371, 64);
        mpfr_init2 (temp_var_372, 64);
        mpfr_init2 (temp_var_373, 64);
        mpfr_init2 (temp_var_374, 64);
        mpfr_init2 (temp_var_375, 64);
        mpfr_init2 (temp_var_376, 64);
        mpfr_init2 (temp_var_377, 64);
        mpfr_init2 (temp_var_378, 64);
        mpfr_init2 (temp_var_379, 64);
        mpfr_init2 (temp_var_380, 64);
        mpfr_init2 (temp_var_381, 64);
        mpfr_init2 (temp_var_382, 64);
        mpfr_init2 (temp_var_383, 64);
        mpfr_init2 (temp_var_384, 64);
        mpfr_init2 (temp_var_385, 64);
        mpfr_init2 (temp_var_386, 64);
        mpfr_init2 (temp_var_387, 64);
        mpfr_init2 (temp_var_388, 64);
        mpfr_init2 (temp_var_389, 64);
        mpfr_init2 (temp_var_390, 64);
        mpfr_init2 (temp_var_391, 64);
        mpfr_init2 (temp_var_392, 64);
        mpfr_init2 (temp_var_393, 64);
        mpfr_init2 (temp_var_394, 64);
        mpfr_init2 (temp_var_395, 64);
        mpfr_init2 (temp_var_396, 64);
        mpfr_init2 (temp_var_397, 64);
        mpfr_init2 (temp_var_398, 64);
        mpfr_init2 (temp_var_399, 64);
        mpfr_init2 (temp_var_400, 64);
        mpfr_init2 (temp_var_401, 64);
        mpfr_init2 (temp_var_402, 64);
        mpfr_init2 (temp_var_403, 64);
        mpfr_init2 (temp_var_404, 64);
        mpfr_init2 (temp_var_405, 64);
        mpfr_init2 (temp_var_406, 64);
        mpfr_init2 (temp_var_407, 64);
        mpfr_init2 (temp_var_408, 64);
        mpfr_init2 (temp_var_409, 64);
        mpfr_init2 (temp_var_410, 64);
        mpfr_init2 (temp_var_411, 64);
        mpfr_init2 (temp_var_412, 64);
        mpfr_init2 (temp_var_413, 64);
        mpfr_init2 (temp_var_414, 64);
        mpfr_init2 (temp_var_415, 64);
        mpfr_init2 (temp_var_416, 64);
        mpfr_init2 (temp_var_417, 64);
        mpfr_init2 (temp_var_418, 64);
        mpfr_init2 (temp_var_419, 64);
        mpfr_init2 (temp_var_420, 64);
        mpfr_init2 (temp_var_421, 64);
        mpfr_init2 (temp_var_422, 64);
        mpfr_init2 (temp_var_423, 64);
        mpfr_init2 (temp_var_424, 64);
        mpfr_init2 (temp_var_425, 64);
        mpfr_init2 (temp_var_426, 64);
        mpfr_init2 (temp_var_427, 64);
        mpfr_init2 (temp_var_428, 64);
        mpfr_init2 (temp_var_429, 64);
        mpfr_init2 (temp_var_430, 64);
        mpfr_init2 (temp_var_431, 64);
        mpfr_init2 (temp_var_432, 64);
        mpfr_init2 (temp_var_433, 64);
        mpfr_init2 (temp_var_434, 64);
        mpfr_init2 (temp_var_435, 64);
        mpfr_init2 (temp_var_436, 64);
        mpfr_init2 (temp_var_437, 64);
        mpfr_init2 (temp_var_438, 64);
        mpfr_init2 (temp_var_439, 64);
        mpfr_init2 (temp_var_440, 64);
        mpfr_init2 (temp_var_441, 64);
        mpfr_init2 (temp_var_442, 64);
        mpfr_init2 (temp_var_443, 64);
        mpfr_init2 (temp_var_444, 64);
        mpfr_init2 (temp_var_445, 64);
        mpfr_init2 (temp_var_446, 64);
        mpfr_init2 (temp_var_447, 64);
        mpfr_init2 (temp_var_448, 64);
        mpfr_init2 (temp_var_449, 64);
        mpfr_init2 (temp_var_450, 64);
        mpfr_init2 (temp_var_451, 64);
        mpfr_init2 (temp_var_452, 64);
        mpfr_init2 (temp_var_453, 64);
        mpfr_init2 (temp_var_454, 64);
        mpfr_init2 (temp_var_455, 64);
        mpfr_init2 (temp_var_456, 64);
        mpfr_init2 (temp_var_457, 64);
        mpfr_init2 (temp_var_458, 64);
        mpfr_init2 (temp_var_459, 64);
        mpfr_init2 (temp_var_460, 64);
        mpfr_init2 (temp_var_461, 64);
        mpfr_init2 (temp_var_462, 64);
        mpfr_init2 (temp_var_463, 64);
        mpfr_init2 (temp_var_464, 64);
        mpfr_init2 (temp_var_465, 64);
        mpfr_init2 (temp_var_466, 64);
        mpfr_init2 (temp_var_467, 64);
        mpfr_init2 (temp_var_468, 64);
        mpfr_init2 (temp_var_469, 64);
        mpfr_init2 (temp_var_470, 64);
        mpfr_init2 (temp_var_471, 64);
        mpfr_init2 (temp_var_472, 64);
        mpfr_init2 (temp_var_473, 64);
        mpfr_init2 (temp_var_474, 64);
        mpfr_init2 (temp_var_475, 64);
        mpfr_init2 (temp_var_476, 64);
        mpfr_init2 (temp_var_477, 64);
        mpfr_init2 (temp_var_478, 64);
        mpfr_init2 (temp_var_479, 64);
        mpfr_init2 (temp_var_480, 64);
        mpfr_init2 (temp_var_481, 64);
        mpfr_init2 (temp_var_482, 64);
        mpfr_init2 (temp_var_483, 64);
        mpfr_init2 (temp_var_484, 64);
        mpfr_init2 (temp_var_485, 64);
        mpfr_init2 (temp_var_486, 64);
        mpfr_init2 (temp_var_487, 64);
        mpfr_init2 (temp_var_488, 64);
        mpfr_init2 (temp_var_489, 64);
        mpfr_init2 (temp_var_490, 64);
        mpfr_init2 (temp_var_491, 64);
        mpfr_init2 (temp_var_492, 64);
        mpfr_init2 (temp_var_493, 64);
        mpfr_init2 (temp_var_494, 64);
        mpfr_init2 (temp_var_495, 64);
        mpfr_init2 (temp_var_496, 64);
        mpfr_init2 (temp_var_497, 64);
        mpfr_init2 (temp_var_498, 64);
        mpfr_init2 (temp_var_499, 64);
        mpfr_init2 (temp_var_500, 64);
        mpfr_init2 (temp_var_501, 64);
        mpfr_init2 (temp_var_502, 64);
        mpfr_init2 (temp_var_503, 64);
        mpfr_init2 (temp_var_504, 64);
        mpfr_init2 (temp_var_505, 64);
        mpfr_init2 (temp_var_506, 64);
        mpfr_init2 (temp_var_507, 64);
        mpfr_init2 (temp_var_508, 64);
        mpfr_init2 (temp_var_509, 64);
        mpfr_init2 (temp_var_510, 64);
        mpfr_init2 (temp_var_511, 64);
        mpfr_init2 (temp_var_512, 64);
        mpfr_init2 (temp_var_513, 64);
        mpfr_init2 (temp_var_514, 64);
        mpfr_init2 (temp_var_515, 64);
        mpfr_init2 (temp_var_516, 64);
        mpfr_init2 (temp_var_517, 64);
        mpfr_init2 (temp_var_518, 64);
        mpfr_init2 (temp_var_519, 64);
        mpfr_init2 (temp_var_520, 64);
        mpfr_init2 (temp_var_521, 64);
        mpfr_init2 (temp_var_522, 64);
        mpfr_init2 (temp_var_523, 64);
        mpfr_init2 (temp_var_524, 64);
        mpfr_init2 (temp_var_525, 64);
        mpfr_init2 (temp_var_526, 64);
        mpfr_init2 (temp_var_527, 64);
        mpfr_init2 (temp_var_528, 64);
        mpfr_init2 (temp_var_529, 64);
        mpfr_init2 (temp_var_530, 64);
        mpfr_init2 (temp_var_531, 64);
        mpfr_init2 (temp_var_532, 64);
        mpfr_init2 (temp_var_533, 64);
        mpfr_init2 (temp_var_534, 64);
        mpfr_init2 (temp_var_535, 64);
        mpfr_init2 (temp_var_536, 64);
        mpfr_init2 (temp_var_537, 64);
        mpfr_init2 (temp_var_538, 64);
        mpfr_init2 (temp_var_539, 64);
        mpfr_init2 (temp_var_540, 64);
        mpfr_init2 (temp_var_541, 64);
        mpfr_init2 (temp_var_542, 64);
        mpfr_init2 (temp_var_543, 64);
        mpfr_init2 (temp_var_544, 64);
        mpfr_init2 (temp_var_545, 64);
        mpfr_init2 (temp_var_546, 64);
        mpfr_init2 (temp_var_547, 64);
        mpfr_init2 (temp_var_548, 64);
        mpfr_init2 (temp_var_549, 64);
        mpfr_init2 (temp_var_550, 64);
        mpfr_init2 (temp_var_551, 64);
        mpfr_init2 (temp_var_552, 64);
        mpfr_init2 (temp_var_553, 64);
        mpfr_init2 (temp_var_554, 64);
        mpfr_init2 (temp_var_555, 64);
        mpfr_init2 (temp_var_556, 64);
        mpfr_init2 (temp_var_557, 64);
        mpfr_init2 (temp_var_558, 64);
        mpfr_init2 (temp_var_559, 64);
        mpfr_init2 (temp_var_560, 64);
        mpfr_init2 (temp_var_561, 64);
        mpfr_init2 (temp_var_562, 64);
        mpfr_init2 (temp_var_563, 64);
        mpfr_init2 (temp_var_564, 64);
        mpfr_init2 (temp_var_565, 64);
        mpfr_init2 (temp_var_566, 64);
        mpfr_init2 (temp_var_567, 64);
        mpfr_init2 (temp_var_568, 64);
        mpfr_init2 (temp_var_569, 64);
        mpfr_init2 (temp_var_570, 64);
        mpfr_init2 (temp_var_571, 64);
        mpfr_init2 (temp_var_572, 64);
        mpfr_init2 (temp_var_573, 64);
        mpfr_init2 (temp_var_574, 64);
        mpfr_init2 (temp_var_575, 64);
        mpfr_init2 (temp_var_576, 64);
        mpfr_init2 (temp_var_577, 64);
        mpfr_init2 (temp_var_578, 64);
        mpfr_init2 (temp_var_579, 64);
        mpfr_init2 (temp_var_580, 64);
        mpfr_init2 (temp_var_581, 64);
        mpfr_init2 (temp_var_582, 64);
        mpfr_init2 (temp_var_583, 64);
        mpfr_init2 (temp_var_584, 64);
        mpfr_init2 (temp_var_585, 64);
        mpfr_init2 (temp_var_586, 64);
        mpfr_init2 (temp_var_587, 64);
        mpfr_init2 (temp_var_588, 64);
        mpfr_init2 (temp_var_589, 64);
        mpfr_init2 (temp_var_590, 64);
        mpfr_init2 (temp_var_591, 64);
        mpfr_init2 (temp_var_592, 64);
        mpfr_init2 (temp_var_593, 64);
        mpfr_init2 (temp_var_594, 64);
        mpfr_init2 (temp_var_595, 64);
        mpfr_init2 (temp_var_596, 64);
        mpfr_init2 (temp_var_597, 64);
        mpfr_init2 (temp_var_598, 64);
        mpfr_init2 (temp_var_599, 64);
        mpfr_init2 (temp_var_600, 64);
        mpfr_init2 (temp_var_601, 64);
        mpfr_init2 (temp_var_602, 64);
        mpfr_init2 (temp_var_603, 64);
        mpfr_init2 (temp_var_604, 64);
        mpfr_init2 (temp_var_605, 64);
        mpfr_init2 (temp_var_606, 64);
        mpfr_init2 (temp_var_607, 64);
        mpfr_init2 (temp_var_608, 64);
        mpfr_init2 (temp_var_609, 64);
        mpfr_init2 (temp_var_610, 64);
        mpfr_init2 (temp_var_611, 64);
        mpfr_init2 (temp_var_612, 64);
        mpfr_init2 (temp_var_613, 64);
        mpfr_init2 (temp_var_614, 64);
        mpfr_init2 (temp_var_615, 64);
        mpfr_init2 (temp_var_616, 64);
        mpfr_init2 (temp_var_617, 64);
        mpfr_init2 (temp_var_618, 64);
        mpfr_init2 (temp_var_619, 64);
        mpfr_init2 (temp_var_620, 64);
        mpfr_init2 (temp_var_621, 64);
        mpfr_init2 (temp_var_622, 64);
        mpfr_init2 (temp_var_623, 64);
        mpfr_init2 (temp_var_624, 64);
        mpfr_init2 (temp_var_625, 64);
        mpfr_init2 (temp_var_626, 64);
        mpfr_init2 (temp_var_627, 64);
        mpfr_init2 (temp_var_628, 64);
        mpfr_init2 (temp_var_629, 64);
        mpfr_init2 (temp_var_630, 64);
        mpfr_init2 (temp_var_631, 64);
        mpfr_init2 (temp_var_632, 64);
        mpfr_init2 (temp_var_633, 64);
        mpfr_init2 (temp_var_634, 64);
        mpfr_init2 (temp_var_635, 64);
        mpfr_init2 (temp_var_636, 64);
        mpfr_init2 (temp_var_637, 64);
        mpfr_init2 (temp_var_638, 64);
        mpfr_init2 (temp_var_639, 64);
        mpfr_init2 (temp_var_640, 64);
        mpfr_init2 (temp_var_641, 64);
        mpfr_init2 (temp_var_642, 64);
        mpfr_init2 (temp_var_643, 64);
        mpfr_init2 (temp_var_644, 64);
        mpfr_init2 (temp_var_645, 64);
        mpfr_init2 (temp_var_646, 64);
        mpfr_init2 (temp_var_647, 64);
        mpfr_init2 (temp_var_648, 64);
        mpfr_init2 (temp_var_649, 64);
        mpfr_init2 (temp_var_650, 64);
        mpfr_init2 (temp_var_651, 64);
        mpfr_init2 (temp_var_652, 64);
        mpfr_init2 (temp_var_653, 64);
        mpfr_init2 (temp_var_654, 64);
        mpfr_init2 (temp_var_655, 64);
        mpfr_init2 (temp_var_656, 64);
        mpfr_init2 (temp_var_657, 64);
        mpfr_init2 (temp_var_658, 64);
        mpfr_init2 (temp_var_659, 64);
        mpfr_init2 (temp_var_660, 64);
        mpfr_init2 (temp_var_661, 64);
        mpfr_init2 (temp_var_662, 64);
        mpfr_init2 (temp_var_663, 64);
        mpfr_init2 (temp_var_664, 64);
        mpfr_init2 (temp_var_665, 64);
        mpfr_init2 (temp_var_666, 64);
        mpfr_init2 (temp_var_667, 64);
        mpfr_init2 (temp_var_668, 64);
        mpfr_init2 (temp_var_669, 64);
        mpfr_init2 (temp_var_670, 64);
        mpfr_init2 (temp_var_671, 64);
        mpfr_init2 (temp_var_672, 64);
        mpfr_init2 (temp_var_673, 64);
        mpfr_init2 (temp_var_674, 64);
        mpfr_init2 (temp_var_675, 64);
        mpfr_init2 (temp_var_676, 64);
        mpfr_init2 (temp_var_677, 64);
        mpfr_init2 (temp_var_678, 64);
        mpfr_init2 (temp_var_679, 64);
        mpfr_init2 (temp_var_680, 64);
        mpfr_init2 (temp_var_681, 64);
        mpfr_init2 (temp_var_682, 64);
        mpfr_init2 (temp_var_683, 64);
        mpfr_init2 (temp_var_684, 64);
        mpfr_init2 (temp_var_685, 64);
        mpfr_init2 (temp_var_686, 64);
        mpfr_init2 (temp_var_687, 64);
        mpfr_init2 (temp_var_688, 64);
        mpfr_init2 (temp_var_689, 64);
        mpfr_init2 (temp_var_690, 64);
        mpfr_init2 (temp_var_691, 64);
        mpfr_init2 (temp_var_692, 64);
        mpfr_init2 (temp_var_693, 64);
        mpfr_init2 (temp_var_694, 64);
        mpfr_init2 (temp_var_695, 64);
        mpfr_init2 (temp_var_696, 64);
        mpfr_init2 (temp_var_697, 64);
        mpfr_init2 (temp_var_698, 64);
        mpfr_init2 (temp_var_699, 64);
        mpfr_init2 (temp_var_700, 64);
        mpfr_init2 (temp_var_701, 64);
        mpfr_init2 (temp_var_702, 64);
        mpfr_init2 (temp_var_703, 64);
        mpfr_init2 (temp_var_704, 64);
        mpfr_init2 (temp_var_705, 64);
        mpfr_init2 (temp_var_706, 64);
        mpfr_init2 (temp_var_707, 64);
        mpfr_init2 (temp_var_708, 64);
        mpfr_init2 (temp_var_709, 64);
        mpfr_init2 (temp_var_710, 64);
        mpfr_init2 (temp_var_711, 64);
        mpfr_init2 (temp_var_712, 64);
        mpfr_init2 (temp_var_713, 64);
        mpfr_init2 (temp_var_714, 64);
        mpfr_init2 (temp_var_715, 64);
        mpfr_init2 (temp_var_716, 64);
        mpfr_init2 (temp_var_717, 64);
        mpfr_init2 (temp_var_718, 64);
        mpfr_init2 (temp_var_719, 64);
        mpfr_init2 (temp_var_720, 64);
        mpfr_init2 (temp_var_721, 64);
        mpfr_init2 (temp_var_722, 64);
        mpfr_init2 (temp_var_723, 64);
        mpfr_init2 (temp_var_724, 64);
        mpfr_init2 (temp_var_725, 64);
        mpfr_init2 (temp_var_726, 64);
        mpfr_init2 (temp_var_727, 64);
        mpfr_init2 (temp_var_728, 64);
        mpfr_init2 (temp_var_729, 64);
        mpfr_init2 (temp_var_730, 64);
        mpfr_init2 (temp_var_731, 64);
        mpfr_init2 (temp_var_732, 64);
        mpfr_init2 (temp_var_733, 64);
        mpfr_init2 (temp_var_734, 64);
        mpfr_init2 (temp_var_735, 64);
        mpfr_init2 (temp_var_736, 64);
        mpfr_init2 (temp_var_737, 64);
        mpfr_init2 (temp_var_738, 64);
        mpfr_init2 (temp_var_739, 64);
        mpfr_init2 (temp_var_740, 64);
        mpfr_init2 (temp_var_741, 64);
        mpfr_init2 (temp_var_742, 64);
        mpfr_init2 (temp_var_743, 64);
        mpfr_init2 (temp_var_744, 64);
        mpfr_init2 (temp_var_745, 64);
        mpfr_init2 (temp_var_746, 64);
        mpfr_init2 (temp_var_747, 64);
        mpfr_init2 (temp_var_748, 64);
        mpfr_init2 (temp_var_749, 64);
        mpfr_init2 (temp_var_750, 64);
        mpfr_init2 (temp_var_751, 64);
        mpfr_init2 (temp_var_752, 64);
        mpfr_init2 (temp_var_753, 64);
        mpfr_init2 (temp_var_754, 64);
        mpfr_init2 (temp_var_755, 64);
        mpfr_init2 (temp_var_756, 64);
        mpfr_init2 (temp_var_757, 64);
        mpfr_init2 (temp_var_758, 64);
        mpfr_init2 (temp_var_759, 64);
        mpfr_init2 (temp_var_760, 64);
        mpfr_init2 (temp_var_761, 64);
        mpfr_init2 (temp_var_762, 64);
        mpfr_init2 (temp_var_763, 64);
        mpfr_init2 (temp_var_764, 64);
        mpfr_init2 (temp_var_765, 64);
        mpfr_init2 (temp_var_766, 64);
        mpfr_init2 (temp_var_767, 64);
        mpfr_init2 (temp_var_768, 64);
        mpfr_init2 (temp_var_769, 64);
        mpfr_init2 (temp_var_770, 64);
        mpfr_init2 (temp_var_771, 64);
        mpfr_init2 (temp_var_772, 64);
        mpfr_init2 (temp_var_773, 64);
        mpfr_init2 (temp_var_774, 64);
        mpfr_init2 (temp_var_775, 64);
        mpfr_init2 (temp_var_776, 64);
        mpfr_init2 (temp_var_777, 64);
        mpfr_init2 (temp_var_778, 64);
        mpfr_init2 (temp_var_779, 64);
        mpfr_init2 (temp_var_780, 64);
        mpfr_init2 (temp_var_781, 64);
        mpfr_init2 (temp_var_782, 64);
        mpfr_init2 (temp_var_783, 64);
        mpfr_init2 (temp_var_784, 64);
        mpfr_init2 (temp_var_785, 64);
        mpfr_init2 (temp_var_786, 64);
        mpfr_init2 (temp_var_787, 64);
        mpfr_init2 (temp_var_788, 64);
        mpfr_init2 (temp_var_789, 64);
        mpfr_init2 (temp_var_790, 64);
        mpfr_init2 (temp_var_791, 64);
        mpfr_init2 (temp_var_792, 64);
        mpfr_init2 (temp_var_793, 64);
        mpfr_init2 (temp_var_794, 64);
        mpfr_init2 (temp_var_795, 64);
        mpfr_init2 (temp_var_796, 64);
        mpfr_init2 (temp_var_797, 64);
        mpfr_init2 (temp_var_798, 64);
        mpfr_init2 (temp_var_799, 64);
        mpfr_init2 (temp_var_800, 64);
        mpfr_init2 (temp_var_801, 64);
        mpfr_init2 (temp_var_802, 64);
        mpfr_init2 (temp_var_803, 64);
        mpfr_init2 (temp_var_804, 64);
        mpfr_init2 (temp_var_805, 64);
        mpfr_init2 (temp_var_806, 64);
        mpfr_init2 (temp_var_807, 64);
        mpfr_init2 (temp_var_808, 64);
        mpfr_init2 (temp_var_809, 64);
        mpfr_init2 (temp_var_810, 64);
        mpfr_init2 (temp_var_811, 64);
        mpfr_init2 (temp_var_812, 64);
        mpfr_init2 (temp_var_813, 64);
        mpfr_init2 (temp_var_814, 64);
        mpfr_init2 (temp_var_815, 64);
        mpfr_init2 (temp_var_816, 64);
        mpfr_init2 (temp_var_817, 64);
        mpfr_init2 (temp_var_818, 64);
        mpfr_init2 (temp_var_819, 64);
        mpfr_init2 (temp_var_820, 64);
        mpfr_init2 (temp_var_821, 64);
        mpfr_init2 (temp_var_822, 64);
        mpfr_init2 (temp_var_823, 64);
        mpfr_init2 (temp_var_824, 64);
        mpfr_init2 (temp_var_825, 64);
        mpfr_init2 (temp_var_826, 64);
        mpfr_init2 (temp_var_827, 64);
        mpfr_init2 (temp_var_828, 64);
        mpfr_init2 (temp_var_829, 64);
        mpfr_init2 (temp_var_830, 64);
        mpfr_init2 (temp_var_831, 64);
        mpfr_init2 (temp_var_832, 64);
        mpfr_init2 (temp_var_833, 64);
        mpfr_init2 (temp_var_834, 64);
        mpfr_init2 (temp_var_835, 64);
        mpfr_init2 (temp_var_836, 64);
        mpfr_init2 (temp_var_837, 64);
        mpfr_init2 (temp_var_838, 64);
        mpfr_init2 (temp_var_839, 64);
        mpfr_init2 (temp_var_840, 64);
        mpfr_init2 (temp_var_841, 64);
        mpfr_init2 (temp_var_842, 64);
        mpfr_init2 (temp_var_843, 64);
        mpfr_init2 (temp_var_844, 64);
        mpfr_init2 (temp_var_845, 64);
        mpfr_init2 (temp_var_846, 64);
        mpfr_init2 (temp_var_847, 64);
        mpfr_init2 (temp_var_848, 64);
        mpfr_init2 (temp_var_849, 64);
        mpfr_init2 (temp_var_850, 64);
        mpfr_init2 (temp_var_851, 64);
        mpfr_init2 (temp_var_852, 64);
        mpfr_init2 (temp_var_853, 64);
        mpfr_init2 (temp_var_854, 64);
        mpfr_init2 (temp_var_855, 64);
        mpfr_init2 (temp_var_856, 64);
        mpfr_init2 (temp_var_857, 64);
        mpfr_init2 (temp_var_858, 64);
        mpfr_init2 (temp_var_859, 64);
        mpfr_init2 (temp_var_860, 64);
        mpfr_init2 (temp_var_861, 64);
        mpfr_init2 (temp_var_862, 64);
        mpfr_init2 (temp_var_863, 64);
        mpfr_init2 (temp_var_864, 64);
        mpfr_init2 (temp_var_865, 64);
        mpfr_init2 (temp_var_866, 64);
        mpfr_init2 (temp_var_867, 64);
        mpfr_init2 (temp_var_868, 64);
        mpfr_init2 (temp_var_869, 64);
        mpfr_init2 (temp_var_870, 64);
        mpfr_init2 (temp_var_871, 64);
        mpfr_init2 (temp_var_872, 64);
        mpfr_init2 (temp_var_873, 64);
        mpfr_init2 (temp_var_874, 64);
        mpfr_init2 (temp_var_875, 64);
        mpfr_init2 (temp_var_876, 64);
        mpfr_init2 (temp_var_877, 64);
        mpfr_init2 (temp_var_878, 64);
        mpfr_init2 (temp_var_879, 64);
        mpfr_init2 (temp_var_880, 64);
        mpfr_init2 (temp_var_881, 64);
        mpfr_init2 (temp_var_882, 64);
        mpfr_init2 (temp_var_883, 64);
        mpfr_init2 (temp_var_884, 64);
        mpfr_init2 (temp_var_885, 64);
        mpfr_init2 (temp_var_886, 64);
        mpfr_init2 (temp_var_887, 64);
        mpfr_init2 (temp_var_888, 64);
        mpfr_init2 (temp_var_889, 64);
        mpfr_init2 (temp_var_890, 64);
        mpfr_init2 (temp_var_891, 64);
        mpfr_init2 (temp_var_892, 64);
        mpfr_init2 (temp_var_893, 64);
        mpfr_init2 (temp_var_894, 64);
        mpfr_init2 (temp_var_895, 64);
        mpfr_init2 (temp_var_896, 64);
        mpfr_init2 (temp_var_897, 64);
        mpfr_init2 (temp_var_898, 64);
        mpfr_init2 (temp_var_899, 64);
        mpfr_init2 (temp_var_900, 64);
        mpfr_init2 (temp_var_901, 64);
        mpfr_init2 (temp_var_902, 64);
        mpfr_init2 (temp_var_903, 64);
        mpfr_init2 (temp_var_904, 64);
        mpfr_init2 (temp_var_905, 64);
        mpfr_init2 (temp_var_906, 64);
        mpfr_init2 (temp_var_907, 64);
        mpfr_init2 (temp_var_908, 64);
        mpfr_init2 (temp_var_909, 64);
        mpfr_init2 (temp_var_910, 64);
        mpfr_init2 (temp_var_911, 64);
        mpfr_init2 (temp_var_912, 64);
        mpfr_init2 (temp_var_913, 64);
        mpfr_init2 (temp_var_914, 64);
        mpfr_init2 (temp_var_915, 64);
        mpfr_init2 (temp_var_916, 64);
        mpfr_init2 (temp_var_917, 64);
        mpfr_init2 (temp_var_918, 64);
        mpfr_init2 (temp_var_919, 64);
  mpfr_init2(ux_LBM_showGridStatistics, config_vals[20]);
  mpfr_init2(uy_LBM_showGridStatistics, config_vals[21]);
  mpfr_init2(uz_LBM_showGridStatistics, config_vals[22]);
  mpfr_init2(minU2_LBM_showGridStatistics, config_vals[23]);
  mpfr_init2(maxU2_LBM_showGridStatistics, config_vals[24]);
  mpfr_init2(u2_LBM_showGridStatistics, config_vals[25]);
  mpfr_init2(minRho_LBM_showGridStatistics, config_vals[26]);
  mpfr_init2(maxRho_LBM_showGridStatistics, config_vals[27]);
  mpfr_init2(rho_LBM_showGridStatistics, config_vals[28]);
  mpfr_init2(mass_LBM_showGridStatistics, config_vals[29]);
        mpfr_init2 (temp_var_920, 64);
        mpfr_init2 (temp_var_921, 64);
        mpfr_init2 (temp_var_922, 64);
        mpfr_init2 (temp_var_923, 64);
        mpfr_init2 (temp_var_924, 64);
        mpfr_init2 (temp_var_925, 64);
        mpfr_init2 (temp_var_926, 64);
        mpfr_init2 (temp_var_927, 64);
        mpfr_init2 (temp_var_928, 64);
        mpfr_init2 (temp_var_929, 64);
        mpfr_init2 (temp_var_930, 64);
        mpfr_init2 (temp_var_931, 64);
        mpfr_init2 (temp_var_932, 64);
        mpfr_init2 (temp_var_933, 64);
        mpfr_init2 (temp_var_934, 64);
        mpfr_init2 (temp_var_935, 64);
        mpfr_init2 (temp_var_936, 64);
        mpfr_init2 (temp_var_937, 64);
        mpfr_init2 (temp_var_938, 64);
        mpfr_init2 (temp_var_939, 64);
        mpfr_init2 (temp_var_940, 64);
        mpfr_init2 (temp_var_941, 64);
        mpfr_init2 (temp_var_942, 64);
        mpfr_init2 (temp_var_943, 64);
        mpfr_init2 (temp_var_944, 64);
        mpfr_init2 (temp_var_945, 64);
        mpfr_init2 (temp_var_946, 64);
        mpfr_init2 (temp_var_947, 64);
        mpfr_init2 (temp_var_948, 64);
        mpfr_init2 (temp_var_949, 64);
        mpfr_init2 (temp_var_950, 64);
        mpfr_init2 (temp_var_951, 64);
        mpfr_init2 (temp_var_952, 64);
        mpfr_init2 (temp_var_953, 64);
        mpfr_init2 (temp_var_954, 64);
        mpfr_init2 (temp_var_955, 64);
            mpfr_init2 (temp_var_956, 64);
            mpfr_init2 (temp_var_957, 64);
            mpfr_init2 (temp_var_958, 64);
            mpfr_init2 (temp_var_959, 64);
            mpfr_init2 (temp_var_960, 64);
            mpfr_init2 (temp_var_961, 64);
            mpfr_init2 (temp_var_962, 64);
            mpfr_init2 (temp_var_963, 64);
            mpfr_init2 (temp_var_964, 64);
            mpfr_init2 (temp_var_965, 64);
            mpfr_init2 (temp_var_966, 64);
            mpfr_init2 (temp_var_967, 64);
            mpfr_init2 (temp_var_968, 64);
            mpfr_init2 (temp_var_969, 64);
            mpfr_init2 (temp_var_970, 64);
            mpfr_init2 (temp_var_971, 64);
            mpfr_init2 (temp_var_972, 64);
            mpfr_init2 (temp_var_973, 64);
            mpfr_init2 (temp_var_974, 64);
            mpfr_init2 (temp_var_975, 64);
            mpfr_init2 (temp_var_976, 64);
            mpfr_init2 (temp_var_977, 64);
            mpfr_init2 (temp_var_978, 64);
            mpfr_init2 (temp_var_979, 64);
            mpfr_init2 (temp_var_980, 64);
            mpfr_init2 (temp_var_981, 64);
            mpfr_init2 (temp_var_982, 64);
            mpfr_init2 (temp_var_983, 64);
            mpfr_init2 (temp_var_984, 64);
            mpfr_init2 (temp_var_985, 64);
            mpfr_init2 (temp_var_986, 64);
            mpfr_init2 (temp_var_987, 64);
            mpfr_init2 (temp_var_988, 64);
            mpfr_init2 (temp_var_989, 64);
            mpfr_init2 (temp_var_990, 64);
            mpfr_init2 (temp_var_991, 64);
            mpfr_init2 (temp_var_992, 64);
            mpfr_init2 (temp_var_993, 64);
            mpfr_init2 (temp_var_994, 64);
            mpfr_init2 (temp_var_995, 64);
            mpfr_init2 (temp_var_996, 64);
            mpfr_init2 (temp_var_997, 64);
            mpfr_init2 (temp_var_998, 64);
            mpfr_init2 (temp_var_999, 64);
            mpfr_init2 (temp_var_1000, 64);
            mpfr_init2 (temp_var_1001, 64);
            mpfr_init2 (temp_var_1002, 64);
            mpfr_init2 (temp_var_1003, 64);
            mpfr_init2 (temp_var_1004, 64);
            mpfr_init2 (temp_var_1005, 64);
            mpfr_init2 (temp_var_1006, 64);
            mpfr_init2 (temp_var_1007, 64);
            mpfr_init2 (temp_var_1008, 64);
            mpfr_init2 (temp_var_1009, 64);
            mpfr_init2 (temp_var_1010, 64);
            mpfr_init2 (temp_var_1011, 64);
            mpfr_init2 (temp_var_1012, 64);
            mpfr_init2 (temp_var_1013, 64);
            mpfr_init2 (temp_var_1014, 64);
            mpfr_init2 (temp_var_1015, 64);
}

int init() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
	
        if (myFile == NULL) {
 printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN; s++) {
            fscanf(myFile, "%d,", &config_vals[s]);
                          }

        fclose(myFile);
        return 0;             
}
/*############################################################################*/

#define DFL1 (1.0/ 3.0)
#define DFL2 (1.0/18.0)
#define DFL3 (1.0/36.0)

void LBM_allocateGrid( double** ptr ) {
	const size_t margin = 2*SIZE_X*SIZE_Y*N_CELL_ENTRIES,
	             size   = sizeof( LBM_Grid ) + 2*margin*sizeof( double );
	             
	*ptr = malloc( size );
	
/*	asm volatile ("movq %0, %%r11; nopq (%%r11);" 
				:
				:"r"(*ptr)
				:"%r11");
	asm volatile ("movq %0, %%r12; nopq (%%r12);"
				:
				:"r"(*ptr + size / sizeof(double))
				:"%r12");
*/
	if( ! *ptr ) {
		printf( "LBM_allocateGrid: could not allocate %f Byte\n",size / (1024.0*1024.0) );
		exit( 1 );
	}
#if !defined(SPEC_CPU)
	printf( "LBM_allocateGrid: allocated %zu Byte\n", size );
	//printf( "LBM_allocateGrid: ptr = %p\n", *ptr );
	//printf( "LBM_allocateGrid: ptr_end = %p\n", *ptr + size / 8);
#endif

	*ptr += margin;
}

/*############################################################################*/

void LBM_freeGrid( double** ptr ) {
	const size_t margin = 2*SIZE_X*SIZE_Y*N_CELL_ENTRIES;

	free( *ptr-margin );
	*ptr = NULL;
}

/*############################################################################*/

void LBM_initializeGrid( LBM_Grid grid ) {
//void LBM_initializeGrid( mpfr_t grid )
	SWEEP_VAR

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for
#endif
#endif
	SWEEP_START( 0, 0, -2, 0, 0, SIZE_Z+2 )
		LOCAL( grid, C  ) = DFL1;
		LOCAL( grid, N  ) = DFL2;
		LOCAL( grid, S  ) = DFL2;
		LOCAL( grid, E  ) = DFL2;
		LOCAL( grid, W  ) = DFL2;
		LOCAL( grid, T  ) = DFL2;
		LOCAL( grid, B  ) = DFL2;
		LOCAL( grid, NE ) = DFL3;
		LOCAL( grid, NW ) = DFL3;
		LOCAL( grid, SE ) = DFL3;
		LOCAL( grid, SW ) = DFL3;
		LOCAL( grid, NT ) = DFL3;
		LOCAL( grid, NB ) = DFL3;
		LOCAL( grid, ST ) = DFL3;
		LOCAL( grid, SB ) = DFL3;
		LOCAL( grid, ET ) = DFL3;
		LOCAL( grid, EB ) = DFL3;
		LOCAL( grid, WT ) = DFL3;
		LOCAL( grid, WB ) = DFL3;

		CLEAR_ALL_FLAGS_SWEEP( grid );
	SWEEP_END
}

/*############################################################################*/

void LBM_swapGrids( LBM_GridPtr* grid1, LBM_GridPtr* grid2 ) {
	LBM_GridPtr aux = *grid1;
	*grid1 = *grid2;
	*grid2 = aux;
}

/*############################################################################*/

void LBM_loadObstacleFile( LBM_Grid grid, const char* filename ) {
	int x,  y,  z;

	FILE* file = fopen( filename, "rb" );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( fgetc( file ) != '.' ) SET_FLAG( grid, x, y, z, OBSTACLE );
			}
			fgetc( file );
		}
		fgetc( file );
	}

	fclose( file );
}

/*############################################################################*/

void LBM_initializeSpecialCellsForLDC( LBM_Grid grid ) {
	int x,  y,  z;

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( x, y )
#endif
#endif
	for( z = -2; z < SIZE_Z+2; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( x == 0 || x == SIZE_X-1 ||
				    y == 0 || y == SIZE_Y-1 ||
				    z == 0 || z == SIZE_Z-1 ) {
					SET_FLAG( grid, x, y, z, OBSTACLE );
				}
				else {
					if( (z == 1 || z == SIZE_Z-2) &&
					     x > 1 && x < SIZE_X-2 &&
					     y > 1 && y < SIZE_Y-2 ) {
						SET_FLAG( grid, x, y, z, ACCEL );
					}
				}
			}
		}
	}
}

/*############################################################################*/

void LBM_initializeSpecialCellsForChannel( LBM_Grid grid ) {
	int x,  y,  z;

	/*voption indep*/
#if !defined(SPEC_CPU)
#ifdef _OPENMP
#pragma omp parallel for private( x, y )
#endif
#endif
	for( z = -2; z < SIZE_Z+2; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				if( x == 0 || x == SIZE_X-1 ||
				    y == 0 || y == SIZE_Y-1 ) {
					SET_FLAG( grid, x, y, z, OBSTACLE );

					if( (z == 0 || z == SIZE_Z-1) &&
					    ! TEST_FLAG( grid, x, y, z, OBSTACLE ))
						SET_FLAG( grid, x, y, z, IN_OUT_FLOW );
				}
			}
		}
	}
}


void LBM_performStreamCollide(LBM_Grid srcGrid, LBM_Grid dstGrid)
{
  int i;
  for (i = 0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100)))); i < (0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((130 * (1 * 100)) * (1 * 100))))); i += N_CELL_ENTRIES){
  {
    if ((*((unsigned int *) ((void *) (&srcGrid[(FLAGS + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i])))) & OBSTACLE)
    {
      dstGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(S + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(N + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(W + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(E + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(SW + (N_CELL_ENTRIES * (((-1) + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(SE + (N_CELL_ENTRIES * (((+1) + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(NW + (N_CELL_ENTRIES * (((-1) + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(NE + (N_CELL_ENTRIES * (((+1) + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(SB + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(ST + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(NB + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(NT + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(WB + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(WT + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(EB + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      dstGrid[(ET + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i];
      continue;
    }

    mpfr_set_d(rho_LBM_performStreamCollide, +srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
    
        mpfr_set_d(temp_var_1, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_set_d(temp_var_2, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_3, temp_var_1, temp_var_2, MPFR_RNDD);

        mpfr_set_d(temp_var_4, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_5, (temp_var_3), temp_var_4, MPFR_RNDD);

        mpfr_set_d(temp_var_6, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_7, (temp_var_5), temp_var_6, MPFR_RNDD);

        mpfr_set_d(temp_var_8, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_9, (temp_var_7), temp_var_8, MPFR_RNDD);

        mpfr_set_d(temp_var_10, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_11, (temp_var_9), temp_var_10, MPFR_RNDD);

        mpfr_set_d(temp_var_12, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_13, (temp_var_11), temp_var_12, MPFR_RNDD);

        mpfr_set_d(temp_var_14, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_15, (temp_var_13), temp_var_14, MPFR_RNDD);

        mpfr_set_d(temp_var_16, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_17, (temp_var_15), temp_var_16, MPFR_RNDD);

        mpfr_set_d(temp_var_18, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_19, (temp_var_17), temp_var_18, MPFR_RNDD);

        mpfr_set_d(temp_var_20, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_21, (temp_var_19), temp_var_20, MPFR_RNDD);

        mpfr_set_d(temp_var_22, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_23, (temp_var_21), temp_var_22, MPFR_RNDD);

        mpfr_set_d(temp_var_24, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_25, (temp_var_23), temp_var_24, MPFR_RNDD);

        mpfr_set_d(temp_var_26, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_27, (temp_var_25), temp_var_26, MPFR_RNDD);

        mpfr_set_d(temp_var_28, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_29, (temp_var_27), temp_var_28, MPFR_RNDD);

        mpfr_set_d(temp_var_30, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_31, (temp_var_29), temp_var_30, MPFR_RNDD);

        mpfr_set_d(temp_var_32, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_33, (temp_var_31), temp_var_32, MPFR_RNDD);

        mpfr_set_d(temp_var_34, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_35, (temp_var_33), temp_var_34, MPFR_RNDD);
    mpfr_add(rho_LBM_performStreamCollide, rho_LBM_performStreamCollide, temp_var_35, MPFR_RNDD);
    
        mpfr_set_d(temp_var_36, (+srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_37, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(ux_LBM_performStreamCollide, temp_var_36, temp_var_37, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_38, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_set_d(temp_var_39, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_40, temp_var_38, temp_var_39, MPFR_RNDD);

        mpfr_set_d(temp_var_41, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_42, (temp_var_40), temp_var_41, MPFR_RNDD);

        mpfr_set_d(temp_var_43, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_44, (temp_var_42), temp_var_43, MPFR_RNDD);

        mpfr_set_d(temp_var_45, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_46, (temp_var_44), temp_var_45, MPFR_RNDD);

        mpfr_set_d(temp_var_47, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_48, (temp_var_46), temp_var_47, MPFR_RNDD);

        mpfr_set_d(temp_var_49, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_50, (temp_var_48), temp_var_49, MPFR_RNDD);

        mpfr_set_d(temp_var_51, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_52, (temp_var_50), temp_var_51, MPFR_RNDD);

        mpfr_add_d(temp_var_53, (temp_var_52), 0.001, MPFR_RNDD);
    mpfr_add(ux_LBM_performStreamCollide, ux_LBM_performStreamCollide, temp_var_53, MPFR_RNDD);
    
        mpfr_set_d(temp_var_54, (+srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_55, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_56, temp_var_54, temp_var_55, MPFR_RNDD);

        mpfr_set_d(temp_var_57, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_58, (temp_var_56), temp_var_57, MPFR_RNDD);

        mpfr_set_d(temp_var_59, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_60, (temp_var_58), temp_var_59, MPFR_RNDD);

        mpfr_set_d(temp_var_61, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_62, (temp_var_60), temp_var_61, MPFR_RNDD);

        mpfr_set_d(temp_var_63, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_64, (temp_var_62), temp_var_63, MPFR_RNDD);

        mpfr_set_d(temp_var_65, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_66, (temp_var_64), temp_var_65, MPFR_RNDD);

        mpfr_set_d(temp_var_67, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_68, (temp_var_66), temp_var_67, MPFR_RNDD);

        mpfr_set_d(temp_var_69, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_70, (temp_var_68), temp_var_69, MPFR_RNDD);

        mpfr_set_d(temp_var_71, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uy_LBM_performStreamCollide, (temp_var_70), temp_var_71, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_72, (+srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_73, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_74, temp_var_72, temp_var_73, MPFR_RNDD);

        mpfr_set_d(temp_var_75, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_76, (temp_var_74), temp_var_75, MPFR_RNDD);

        mpfr_set_d(temp_var_77, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_78, (temp_var_76), temp_var_77, MPFR_RNDD);

        mpfr_set_d(temp_var_79, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_80, (temp_var_78), temp_var_79, MPFR_RNDD);

        mpfr_set_d(temp_var_81, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_82, (temp_var_80), temp_var_81, MPFR_RNDD);

        mpfr_set_d(temp_var_83, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_84, (temp_var_82), temp_var_83, MPFR_RNDD);

        mpfr_set_d(temp_var_85, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_86, (temp_var_84), temp_var_85, MPFR_RNDD);

        mpfr_set_d(temp_var_87, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_88, (temp_var_86), temp_var_87, MPFR_RNDD);

        mpfr_set_d(temp_var_89, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uz_LBM_performStreamCollide, (temp_var_88), temp_var_89, MPFR_RNDD);
;
        mpfr_div(ux_LBM_performStreamCollide, ux_LBM_performStreamCollide, rho_LBM_performStreamCollide, MPFR_RNDD);
        mpfr_div(uy_LBM_performStreamCollide, uy_LBM_performStreamCollide, rho_LBM_performStreamCollide, MPFR_RNDD);
        mpfr_div(uz_LBM_performStreamCollide, uz_LBM_performStreamCollide, rho_LBM_performStreamCollide, MPFR_RNDD);
    if ((*((unsigned int *) ((void *) (&srcGrid[(FLAGS + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i])))) & ACCEL)
    {
      mpfr_set_d(ux_LBM_performStreamCollide, 0.005, MPFR_RNDD);
      mpfr_set_d(uy_LBM_performStreamCollide, 0.002, MPFR_RNDD);
      mpfr_set_d(uz_LBM_performStreamCollide, 0.000, MPFR_RNDD);
    }

    
        mpfr_mul(temp_var_90, ux_LBM_performStreamCollide, ux_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_91, uy_LBM_performStreamCollide, uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_add(temp_var_92, (temp_var_90), (temp_var_91), MPFR_RNDD);

        mpfr_mul(temp_var_93, uz_LBM_performStreamCollide, uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_add(temp_var_94, (temp_var_92), (temp_var_93), MPFR_RNDD);
        mpfr_mul_d(u2_LBM_performStreamCollide, (temp_var_94), 1.5, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_95, srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_96, temp_var_95, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_97, rho_LBM_performStreamCollide, ((1.0 / 3.0) * 1.95), MPFR_RNDD);

        mpfr_d_sub(temp_var_98, 1.0, u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_99, (temp_var_97), (temp_var_98), MPFR_RNDD);

        mpfr_add(temp_var_100, (temp_var_96), (temp_var_99), MPFR_RNDD);

dstGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_100, MPFR_RNDD);
    
        mpfr_set_d(temp_var_101, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_102, temp_var_101, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_103, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_104, uy_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_105, (temp_var_104), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_106, uy_LBM_performStreamCollide, (temp_var_105), MPFR_RNDD);

        mpfr_add_d(temp_var_107, (temp_var_106), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_108, (temp_var_107), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_109, (temp_var_103), (temp_var_108), MPFR_RNDD);

        mpfr_add(temp_var_110, (temp_var_102), (temp_var_109), MPFR_RNDD);

dstGrid[(N + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_110, MPFR_RNDD);
    
        mpfr_set_d(temp_var_111, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_112, temp_var_111, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_113, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_114, uy_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_115, (temp_var_114), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_116, uy_LBM_performStreamCollide, (temp_var_115), MPFR_RNDD);

        mpfr_add_d(temp_var_117, (temp_var_116), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_118, (temp_var_117), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_119, (temp_var_113), (temp_var_118), MPFR_RNDD);

        mpfr_add(temp_var_120, (temp_var_112), (temp_var_119), MPFR_RNDD);

dstGrid[(S + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_120, MPFR_RNDD);
    
        mpfr_set_d(temp_var_121, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_122, temp_var_121, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_123, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_124, ux_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_125, (temp_var_124), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_126, ux_LBM_performStreamCollide, (temp_var_125), MPFR_RNDD);

        mpfr_add_d(temp_var_127, (temp_var_126), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_128, (temp_var_127), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_129, (temp_var_123), (temp_var_128), MPFR_RNDD);

        mpfr_add(temp_var_130, (temp_var_122), (temp_var_129), MPFR_RNDD);

dstGrid[(E + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_130, MPFR_RNDD);
    
        mpfr_set_d(temp_var_131, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_132, temp_var_131, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_133, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_134, ux_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_135, (temp_var_134), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_136, ux_LBM_performStreamCollide, (temp_var_135), MPFR_RNDD);

        mpfr_add_d(temp_var_137, (temp_var_136), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_138, (temp_var_137), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_139, (temp_var_133), (temp_var_138), MPFR_RNDD);

        mpfr_add(temp_var_140, (temp_var_132), (temp_var_139), MPFR_RNDD);

dstGrid[(W + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_140, MPFR_RNDD);
    
        mpfr_set_d(temp_var_141, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_142, temp_var_141, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_143, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_144, uz_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_145, (temp_var_144), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_146, uz_LBM_performStreamCollide, (temp_var_145), MPFR_RNDD);

        mpfr_add_d(temp_var_147, (temp_var_146), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_148, (temp_var_147), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_149, (temp_var_143), (temp_var_148), MPFR_RNDD);

        mpfr_add(temp_var_150, (temp_var_142), (temp_var_149), MPFR_RNDD);

dstGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_150, MPFR_RNDD);
    
        mpfr_set_d(temp_var_151, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_152, temp_var_151, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_153, rho_LBM_performStreamCollide, ((1.0 / 18.0) * 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_154, uz_LBM_performStreamCollide, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_155, (temp_var_154), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_156, uz_LBM_performStreamCollide, (temp_var_155), MPFR_RNDD);

        mpfr_add_d(temp_var_157, (temp_var_156), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_158, (temp_var_157), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_159, (temp_var_153), (temp_var_158), MPFR_RNDD);

        mpfr_add(temp_var_160, (temp_var_152), (temp_var_159), MPFR_RNDD);

dstGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_160, MPFR_RNDD);
    
        mpfr_set_d(temp_var_161, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_162, temp_var_161, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_163, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_add(temp_var_164, (ux_LBM_performStreamCollide), uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_add(temp_var_165, (ux_LBM_performStreamCollide), uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_166, (temp_var_165), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_167, (temp_var_166), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_168, (temp_var_164), (temp_var_167), MPFR_RNDD);

        mpfr_add_d(temp_var_169, (temp_var_168), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_170, (temp_var_169), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_171, (temp_var_163), (temp_var_170), MPFR_RNDD);

        mpfr_add(temp_var_172, (temp_var_162), (temp_var_171), MPFR_RNDD);

dstGrid[(NE + (N_CELL_ENTRIES * (((+1) + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_172, MPFR_RNDD);
    
        mpfr_set_d(temp_var_173, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_174, temp_var_173, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_175, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_177, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_176, temp_var_177, uy_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_179, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_178, temp_var_179, uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_180, (temp_var_178), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_181, (temp_var_180), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_182, (temp_var_176), (temp_var_181), MPFR_RNDD);

        mpfr_add_d(temp_var_183, (temp_var_182), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_184, (temp_var_183), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_185, (temp_var_175), (temp_var_184), MPFR_RNDD);

        mpfr_add(temp_var_186, (temp_var_174), (temp_var_185), MPFR_RNDD);

dstGrid[(NW + (N_CELL_ENTRIES * (((-1) + ((+1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_186, MPFR_RNDD);
    
        mpfr_set_d(temp_var_187, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_188, temp_var_187, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_189, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_sub(temp_var_190, (ux_LBM_performStreamCollide), uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_sub(temp_var_191, (ux_LBM_performStreamCollide), uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_192, (temp_var_191), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_193, (temp_var_192), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_194, (temp_var_190), (temp_var_193), MPFR_RNDD);

        mpfr_add_d(temp_var_195, (temp_var_194), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_196, (temp_var_195), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_197, (temp_var_189), (temp_var_196), MPFR_RNDD);

        mpfr_add(temp_var_198, (temp_var_188), (temp_var_197), MPFR_RNDD);

dstGrid[(SE + (N_CELL_ENTRIES * (((+1) + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_198, MPFR_RNDD);
    
        mpfr_set_d(temp_var_199, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_200, temp_var_199, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_201, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_203, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_202, temp_var_203, uy_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_205, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_204, temp_var_205, uy_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_206, (temp_var_204), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_207, (temp_var_206), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_208, (temp_var_202), (temp_var_207), MPFR_RNDD);

        mpfr_add_d(temp_var_209, (temp_var_208), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_210, (temp_var_209), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_211, (temp_var_201), (temp_var_210), MPFR_RNDD);

        mpfr_add(temp_var_212, (temp_var_200), (temp_var_211), MPFR_RNDD);

dstGrid[(SW + (N_CELL_ENTRIES * (((-1) + ((-1) * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_212, MPFR_RNDD);
    
        mpfr_set_d(temp_var_213, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_214, temp_var_213, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_215, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_add(temp_var_216, (uy_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_add(temp_var_217, (uy_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_218, (temp_var_217), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_219, (temp_var_218), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_220, (temp_var_216), (temp_var_219), MPFR_RNDD);

        mpfr_add_d(temp_var_221, (temp_var_220), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_222, (temp_var_221), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_223, (temp_var_215), (temp_var_222), MPFR_RNDD);

        mpfr_add(temp_var_224, (temp_var_214), (temp_var_223), MPFR_RNDD);

dstGrid[(NT + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_224, MPFR_RNDD);
    
        mpfr_set_d(temp_var_225, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_226, temp_var_225, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_227, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_sub(temp_var_228, (uy_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_sub(temp_var_229, (uy_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_230, (temp_var_229), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_231, (temp_var_230), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_232, (temp_var_228), (temp_var_231), MPFR_RNDD);

        mpfr_add_d(temp_var_233, (temp_var_232), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_234, (temp_var_233), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_235, (temp_var_227), (temp_var_234), MPFR_RNDD);

        mpfr_add(temp_var_236, (temp_var_226), (temp_var_235), MPFR_RNDD);

dstGrid[(NB + (N_CELL_ENTRIES * ((0 + ((+1) * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_236, MPFR_RNDD);
    
        mpfr_set_d(temp_var_237, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_238, temp_var_237, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_239, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_241, (uy_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_240, temp_var_241, uz_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_243, (uy_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_242, temp_var_243, uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_244, (temp_var_242), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_245, (temp_var_244), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_246, (temp_var_240), (temp_var_245), MPFR_RNDD);

        mpfr_add_d(temp_var_247, (temp_var_246), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_248, (temp_var_247), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_249, (temp_var_239), (temp_var_248), MPFR_RNDD);

        mpfr_add(temp_var_250, (temp_var_238), (temp_var_249), MPFR_RNDD);

dstGrid[(ST + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_250, MPFR_RNDD);
    
        mpfr_set_d(temp_var_251, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_252, temp_var_251, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_253, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_255, (uy_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_254, temp_var_255, uz_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_257, (uy_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_256, temp_var_257, uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_258, (temp_var_256), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_259, (temp_var_258), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_260, (temp_var_254), (temp_var_259), MPFR_RNDD);

        mpfr_add_d(temp_var_261, (temp_var_260), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_262, (temp_var_261), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_263, (temp_var_253), (temp_var_262), MPFR_RNDD);

        mpfr_add(temp_var_264, (temp_var_252), (temp_var_263), MPFR_RNDD);

dstGrid[(SB + (N_CELL_ENTRIES * ((0 + ((-1) * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_264, MPFR_RNDD);
    
        mpfr_set_d(temp_var_265, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_266, temp_var_265, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_267, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_add(temp_var_268, (ux_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_add(temp_var_269, (ux_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_270, (temp_var_269), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_271, (temp_var_270), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_272, (temp_var_268), (temp_var_271), MPFR_RNDD);

        mpfr_add_d(temp_var_273, (temp_var_272), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_274, (temp_var_273), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_275, (temp_var_267), (temp_var_274), MPFR_RNDD);

        mpfr_add(temp_var_276, (temp_var_266), (temp_var_275), MPFR_RNDD);

dstGrid[(ET + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_276, MPFR_RNDD);
    
        mpfr_set_d(temp_var_277, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_278, temp_var_277, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_279, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        mpfr_sub(temp_var_280, (ux_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_sub(temp_var_281, (ux_LBM_performStreamCollide), uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_282, (temp_var_281), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_283, (temp_var_282), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_284, (temp_var_280), (temp_var_283), MPFR_RNDD);

        mpfr_add_d(temp_var_285, (temp_var_284), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_286, (temp_var_285), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_287, (temp_var_279), (temp_var_286), MPFR_RNDD);

        mpfr_add(temp_var_288, (temp_var_278), (temp_var_287), MPFR_RNDD);

dstGrid[(EB + (N_CELL_ENTRIES * (((+1) + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_288, MPFR_RNDD);
    
        mpfr_set_d(temp_var_289, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_290, temp_var_289, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_291, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_293, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_292, temp_var_293, uz_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_295, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_add(temp_var_294, temp_var_295, uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_296, (temp_var_294), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_297, (temp_var_296), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_298, (temp_var_292), (temp_var_297), MPFR_RNDD);

        mpfr_add_d(temp_var_299, (temp_var_298), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_300, (temp_var_299), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_301, (temp_var_291), (temp_var_300), MPFR_RNDD);

        mpfr_add(temp_var_302, (temp_var_290), (temp_var_301), MPFR_RNDD);

dstGrid[(WT + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + (((+1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_302, MPFR_RNDD);
    
        mpfr_set_d(temp_var_303, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_mul_d(temp_var_304, temp_var_303, (1.0 - 1.95), MPFR_RNDD);

        mpfr_mul_d(temp_var_305, rho_LBM_performStreamCollide, ((1.0 / 36.0) * 1.95), MPFR_RNDD);

        
mpfr_neg (temp_var_307, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_306, temp_var_307, uz_LBM_performStreamCollide, MPFR_RNDD);

        
mpfr_neg (temp_var_309, (ux_LBM_performStreamCollide), MPFR_RNDD);
mpfr_sub(temp_var_308, temp_var_309, uz_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul_d(temp_var_310, (temp_var_308), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_311, (temp_var_310), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_312, (temp_var_306), (temp_var_311), MPFR_RNDD);

        mpfr_add_d(temp_var_313, (temp_var_312), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_314, (temp_var_313), u2_LBM_performStreamCollide, MPFR_RNDD);

        mpfr_mul(temp_var_315, (temp_var_305), (temp_var_314), MPFR_RNDD);

        mpfr_add(temp_var_316, (temp_var_304), (temp_var_315), MPFR_RNDD);

dstGrid[(WB + (N_CELL_ENTRIES * (((-1) + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_316, MPFR_RNDD);
  }

    }

}

void LBM_handleInOutFlow(LBM_Grid srcGrid)
{
  int i;
  for (i = 0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100)))); i < (0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))); i += N_CELL_ENTRIES){
  {
    
        mpfr_set_d(temp_var_317, (+srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_318, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_319, temp_var_317, temp_var_318, MPFR_RNDD);

        mpfr_set_d(temp_var_320, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_321, (temp_var_319), temp_var_320, MPFR_RNDD);

        mpfr_set_d(temp_var_322, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_323, (temp_var_321), temp_var_322, MPFR_RNDD);

        mpfr_set_d(temp_var_324, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_325, (temp_var_323), temp_var_324, MPFR_RNDD);

        mpfr_set_d(temp_var_326, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_327, (temp_var_325), temp_var_326, MPFR_RNDD);

        mpfr_set_d(temp_var_328, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_329, (temp_var_327), temp_var_328, MPFR_RNDD);

        mpfr_set_d(temp_var_330, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_331, (temp_var_329), temp_var_330, MPFR_RNDD);

        mpfr_set_d(temp_var_332, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_333, (temp_var_331), temp_var_332, MPFR_RNDD);

        mpfr_set_d(temp_var_334, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_335, (temp_var_333), temp_var_334, MPFR_RNDD);

        mpfr_set_d(temp_var_336, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_337, (temp_var_335), temp_var_336, MPFR_RNDD);

        mpfr_set_d(temp_var_338, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_339, (temp_var_337), temp_var_338, MPFR_RNDD);

        mpfr_set_d(temp_var_340, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_341, (temp_var_339), temp_var_340, MPFR_RNDD);

        mpfr_set_d(temp_var_342, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_343, (temp_var_341), temp_var_342, MPFR_RNDD);

        mpfr_set_d(temp_var_344, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_345, (temp_var_343), temp_var_344, MPFR_RNDD);

        mpfr_set_d(temp_var_346, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_347, (temp_var_345), temp_var_346, MPFR_RNDD);

        mpfr_set_d(temp_var_348, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_349, (temp_var_347), temp_var_348, MPFR_RNDD);

        mpfr_set_d(temp_var_350, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_351, (temp_var_349), temp_var_350, MPFR_RNDD);

        mpfr_set_d(temp_var_352, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((1 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_add(rho1_LBM_handleInOutFlow, (temp_var_351), temp_var_352, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_353, (+srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_354, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_355, temp_var_353, temp_var_354, MPFR_RNDD);

        mpfr_set_d(temp_var_356, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_357, (temp_var_355), temp_var_356, MPFR_RNDD);

        mpfr_set_d(temp_var_358, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_359, (temp_var_357), temp_var_358, MPFR_RNDD);

        mpfr_set_d(temp_var_360, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_361, (temp_var_359), temp_var_360, MPFR_RNDD);

        mpfr_set_d(temp_var_362, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_363, (temp_var_361), temp_var_362, MPFR_RNDD);

        mpfr_set_d(temp_var_364, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_365, (temp_var_363), temp_var_364, MPFR_RNDD);

        mpfr_set_d(temp_var_366, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_367, (temp_var_365), temp_var_366, MPFR_RNDD);

        mpfr_set_d(temp_var_368, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_369, (temp_var_367), temp_var_368, MPFR_RNDD);

        mpfr_set_d(temp_var_370, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_371, (temp_var_369), temp_var_370, MPFR_RNDD);

        mpfr_set_d(temp_var_372, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_373, (temp_var_371), temp_var_372, MPFR_RNDD);

        mpfr_set_d(temp_var_374, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_375, (temp_var_373), temp_var_374, MPFR_RNDD);

        mpfr_set_d(temp_var_376, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_377, (temp_var_375), temp_var_376, MPFR_RNDD);

        mpfr_set_d(temp_var_378, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_379, (temp_var_377), temp_var_378, MPFR_RNDD);

        mpfr_set_d(temp_var_380, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_381, (temp_var_379), temp_var_380, MPFR_RNDD);

        mpfr_set_d(temp_var_382, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_383, (temp_var_381), temp_var_382, MPFR_RNDD);

        mpfr_set_d(temp_var_384, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_385, (temp_var_383), temp_var_384, MPFR_RNDD);

        mpfr_set_d(temp_var_386, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_387, (temp_var_385), temp_var_386, MPFR_RNDD);

        mpfr_set_d(temp_var_388, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((2 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_add(rho2_LBM_handleInOutFlow, (temp_var_387), temp_var_388, MPFR_RNDD);
;
    
        mpfr_mul_d(temp_var_389, rho1_LBM_handleInOutFlow, 2.0, MPFR_RNDD);
        mpfr_sub(rho_LBM_handleInOutFlow, (temp_var_389), rho2_LBM_handleInOutFlow, MPFR_RNDD);
;
    mpfr_set_d(px_LBM_handleInOutFlow, (((i / N_CELL_ENTRIES) % (1 * 100)) / (0.5 * ((1 * 100) - 1))) - 1.0, MPFR_RNDD);
    mpfr_set_d(py_LBM_handleInOutFlow, ((((i / N_CELL_ENTRIES) / (1 * 100)) % (1 * 100)) / (0.5 * ((1 * 100) - 1))) - 1.0, MPFR_RNDD);
    mpfr_set_d(ux_LBM_handleInOutFlow, 0.00, MPFR_RNDD);
    mpfr_set_d(uy_LBM_handleInOutFlow, 0.00, MPFR_RNDD);
    


        mpfr_mul(temp_var_392, px_LBM_handleInOutFlow, px_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_d_sub(temp_var_393, 1.0, (temp_var_392), MPFR_RNDD);

        mpfr_mul_d(temp_var_394, (temp_var_393), 0.01, MPFR_RNDD);

        mpfr_mul(temp_var_395, py_LBM_handleInOutFlow, py_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_d_sub(temp_var_396, 1.0, (temp_var_395), MPFR_RNDD);
        mpfr_mul(uz_LBM_handleInOutFlow, (temp_var_394), (temp_var_396), MPFR_RNDD);
;
    
        mpfr_mul(temp_var_397, ux_LBM_handleInOutFlow, ux_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_398, uy_LBM_handleInOutFlow, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_399, (temp_var_397), (temp_var_398), MPFR_RNDD);

        mpfr_mul(temp_var_400, uz_LBM_handleInOutFlow, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_401, (temp_var_399), (temp_var_400), MPFR_RNDD);
        mpfr_mul_d(u2_LBM_handleInOutFlow, (temp_var_401), 1.5, MPFR_RNDD);
;
    
        mpfr_mul_d(temp_var_402, rho_LBM_handleInOutFlow, (1.0 / 3.0), MPFR_RNDD);

        mpfr_d_sub(temp_var_403, 1.0, u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_404, (temp_var_402), (temp_var_403), MPFR_RNDD);

srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_404, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_405, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_406, uy_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_407, (temp_var_406), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_408, uy_LBM_handleInOutFlow, (temp_var_407), MPFR_RNDD);

        mpfr_add_d(temp_var_409, (temp_var_408), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_410, (temp_var_409), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_411, (temp_var_405), (temp_var_410), MPFR_RNDD);

srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_411, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_412, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_413, uy_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_414, (temp_var_413), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_415, uy_LBM_handleInOutFlow, (temp_var_414), MPFR_RNDD);

        mpfr_add_d(temp_var_416, (temp_var_415), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_417, (temp_var_416), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_418, (temp_var_412), (temp_var_417), MPFR_RNDD);

srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_418, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_419, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_420, ux_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_421, (temp_var_420), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_422, ux_LBM_handleInOutFlow, (temp_var_421), MPFR_RNDD);

        mpfr_add_d(temp_var_423, (temp_var_422), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_424, (temp_var_423), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_425, (temp_var_419), (temp_var_424), MPFR_RNDD);

srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_425, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_426, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_427, ux_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_428, (temp_var_427), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_429, ux_LBM_handleInOutFlow, (temp_var_428), MPFR_RNDD);

        mpfr_add_d(temp_var_430, (temp_var_429), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_431, (temp_var_430), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_432, (temp_var_426), (temp_var_431), MPFR_RNDD);

srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_432, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_433, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_434, uz_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_435, (temp_var_434), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_436, uz_LBM_handleInOutFlow, (temp_var_435), MPFR_RNDD);

        mpfr_add_d(temp_var_437, (temp_var_436), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_438, (temp_var_437), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_439, (temp_var_433), (temp_var_438), MPFR_RNDD);

srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_439, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_440, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_441, uz_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_442, (temp_var_441), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_443, uz_LBM_handleInOutFlow, (temp_var_442), MPFR_RNDD);

        mpfr_add_d(temp_var_444, (temp_var_443), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_445, (temp_var_444), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_446, (temp_var_440), (temp_var_445), MPFR_RNDD);

srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_446, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_447, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_448, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_449, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_450, (temp_var_449), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_451, (temp_var_450), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_452, (temp_var_448), (temp_var_451), MPFR_RNDD);

        mpfr_add_d(temp_var_453, (temp_var_452), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_454, (temp_var_453), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_455, (temp_var_447), (temp_var_454), MPFR_RNDD);

srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_455, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_456, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_458, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_457, temp_var_458, uy_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_460, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_459, temp_var_460, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_461, (temp_var_459), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_462, (temp_var_461), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_463, (temp_var_457), (temp_var_462), MPFR_RNDD);

        mpfr_add_d(temp_var_464, (temp_var_463), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_465, (temp_var_464), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_466, (temp_var_456), (temp_var_465), MPFR_RNDD);

srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_466, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_467, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_468, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_469, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_470, (temp_var_469), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_471, (temp_var_470), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_472, (temp_var_468), (temp_var_471), MPFR_RNDD);

        mpfr_add_d(temp_var_473, (temp_var_472), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_474, (temp_var_473), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_475, (temp_var_467), (temp_var_474), MPFR_RNDD);

srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_475, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_476, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_478, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_477, temp_var_478, uy_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_480, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_479, temp_var_480, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_481, (temp_var_479), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_482, (temp_var_481), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_483, (temp_var_477), (temp_var_482), MPFR_RNDD);

        mpfr_add_d(temp_var_484, (temp_var_483), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_485, (temp_var_484), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_486, (temp_var_476), (temp_var_485), MPFR_RNDD);

srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_486, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_487, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_488, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_489, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_490, (temp_var_489), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_491, (temp_var_490), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_492, (temp_var_488), (temp_var_491), MPFR_RNDD);

        mpfr_add_d(temp_var_493, (temp_var_492), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_494, (temp_var_493), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_495, (temp_var_487), (temp_var_494), MPFR_RNDD);

srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_495, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_496, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_497, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_498, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_499, (temp_var_498), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_500, (temp_var_499), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_501, (temp_var_497), (temp_var_500), MPFR_RNDD);

        mpfr_add_d(temp_var_502, (temp_var_501), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_503, (temp_var_502), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_504, (temp_var_496), (temp_var_503), MPFR_RNDD);

srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_504, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_505, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_507, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_506, temp_var_507, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_509, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_508, temp_var_509, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_510, (temp_var_508), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_511, (temp_var_510), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_512, (temp_var_506), (temp_var_511), MPFR_RNDD);

        mpfr_add_d(temp_var_513, (temp_var_512), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_514, (temp_var_513), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_515, (temp_var_505), (temp_var_514), MPFR_RNDD);

srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_515, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_516, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_518, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_517, temp_var_518, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_520, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_519, temp_var_520, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_521, (temp_var_519), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_522, (temp_var_521), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_523, (temp_var_517), (temp_var_522), MPFR_RNDD);

        mpfr_add_d(temp_var_524, (temp_var_523), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_525, (temp_var_524), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_526, (temp_var_516), (temp_var_525), MPFR_RNDD);

srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_526, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_527, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_528, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_529, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_530, (temp_var_529), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_531, (temp_var_530), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_532, (temp_var_528), (temp_var_531), MPFR_RNDD);

        mpfr_add_d(temp_var_533, (temp_var_532), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_534, (temp_var_533), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_535, (temp_var_527), (temp_var_534), MPFR_RNDD);

srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_535, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_536, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_537, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_538, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_539, (temp_var_538), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_540, (temp_var_539), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_541, (temp_var_537), (temp_var_540), MPFR_RNDD);

        mpfr_add_d(temp_var_542, (temp_var_541), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_543, (temp_var_542), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_544, (temp_var_536), (temp_var_543), MPFR_RNDD);

srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_544, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_545, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_547, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_546, temp_var_547, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_549, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_548, temp_var_549, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_550, (temp_var_548), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_551, (temp_var_550), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_552, (temp_var_546), (temp_var_551), MPFR_RNDD);

        mpfr_add_d(temp_var_553, (temp_var_552), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_554, (temp_var_553), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_555, (temp_var_545), (temp_var_554), MPFR_RNDD);

srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_555, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_556, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_558, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_557, temp_var_558, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_560, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_559, temp_var_560, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_561, (temp_var_559), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_562, (temp_var_561), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_563, (temp_var_557), (temp_var_562), MPFR_RNDD);

        mpfr_add_d(temp_var_564, (temp_var_563), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_565, (temp_var_564), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_566, (temp_var_556), (temp_var_565), MPFR_RNDD);

srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_566, MPFR_RNDD);
  }

    }

  for (i = 0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((130 - 1) * (1 * 100)) * (1 * 100)))); i < (0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((130 * (1 * 100)) * (1 * 100))))); i += N_CELL_ENTRIES){
  {
    
        mpfr_set_d(temp_var_567, (+srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_568, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_569, temp_var_567, temp_var_568, MPFR_RNDD);

        mpfr_set_d(temp_var_570, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_571, (temp_var_569), temp_var_570, MPFR_RNDD);

        mpfr_set_d(temp_var_572, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_573, (temp_var_571), temp_var_572, MPFR_RNDD);

        mpfr_set_d(temp_var_574, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_575, (temp_var_573), temp_var_574, MPFR_RNDD);

        mpfr_set_d(temp_var_576, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_577, (temp_var_575), temp_var_576, MPFR_RNDD);

        mpfr_set_d(temp_var_578, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_579, (temp_var_577), temp_var_578, MPFR_RNDD);

        mpfr_set_d(temp_var_580, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_581, (temp_var_579), temp_var_580, MPFR_RNDD);

        mpfr_set_d(temp_var_582, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_583, (temp_var_581), temp_var_582, MPFR_RNDD);

        mpfr_set_d(temp_var_584, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_585, (temp_var_583), temp_var_584, MPFR_RNDD);

        mpfr_set_d(temp_var_586, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_587, (temp_var_585), temp_var_586, MPFR_RNDD);

        mpfr_set_d(temp_var_588, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_589, (temp_var_587), temp_var_588, MPFR_RNDD);

        mpfr_set_d(temp_var_590, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_591, (temp_var_589), temp_var_590, MPFR_RNDD);

        mpfr_set_d(temp_var_592, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_593, (temp_var_591), temp_var_592, MPFR_RNDD);

        mpfr_set_d(temp_var_594, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_595, (temp_var_593), temp_var_594, MPFR_RNDD);

        mpfr_set_d(temp_var_596, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_597, (temp_var_595), temp_var_596, MPFR_RNDD);

        mpfr_set_d(temp_var_598, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_599, (temp_var_597), temp_var_598, MPFR_RNDD);

        mpfr_set_d(temp_var_600, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_601, (temp_var_599), temp_var_600, MPFR_RNDD);

        mpfr_set_d(temp_var_602, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_add(rho1_LBM_handleInOutFlow, (temp_var_601), temp_var_602, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_603, (+srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_604, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_605, temp_var_603, temp_var_604, MPFR_RNDD);

        mpfr_set_d(temp_var_606, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_607, (temp_var_605), temp_var_606, MPFR_RNDD);

        mpfr_set_d(temp_var_608, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_609, (temp_var_607), temp_var_608, MPFR_RNDD);

        mpfr_set_d(temp_var_610, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_611, (temp_var_609), temp_var_610, MPFR_RNDD);

        mpfr_set_d(temp_var_612, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_613, (temp_var_611), temp_var_612, MPFR_RNDD);

        mpfr_set_d(temp_var_614, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_615, (temp_var_613), temp_var_614, MPFR_RNDD);

        mpfr_set_d(temp_var_616, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_617, (temp_var_615), temp_var_616, MPFR_RNDD);

        mpfr_set_d(temp_var_618, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_619, (temp_var_617), temp_var_618, MPFR_RNDD);

        mpfr_set_d(temp_var_620, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(ux1_LBM_handleInOutFlow, (temp_var_619), temp_var_620, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_621, (+srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_622, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_623, temp_var_621, temp_var_622, MPFR_RNDD);

        mpfr_set_d(temp_var_624, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_625, (temp_var_623), temp_var_624, MPFR_RNDD);

        mpfr_set_d(temp_var_626, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_627, (temp_var_625), temp_var_626, MPFR_RNDD);

        mpfr_set_d(temp_var_628, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_629, (temp_var_627), temp_var_628, MPFR_RNDD);

        mpfr_set_d(temp_var_630, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_631, (temp_var_629), temp_var_630, MPFR_RNDD);

        mpfr_set_d(temp_var_632, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_633, (temp_var_631), temp_var_632, MPFR_RNDD);

        mpfr_set_d(temp_var_634, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_635, (temp_var_633), temp_var_634, MPFR_RNDD);

        mpfr_set_d(temp_var_636, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_637, (temp_var_635), temp_var_636, MPFR_RNDD);

        mpfr_set_d(temp_var_638, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uy1_LBM_handleInOutFlow, (temp_var_637), temp_var_638, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_639, (+srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_640, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_641, temp_var_639, temp_var_640, MPFR_RNDD);

        mpfr_set_d(temp_var_642, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_643, (temp_var_641), temp_var_642, MPFR_RNDD);

        mpfr_set_d(temp_var_644, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_645, (temp_var_643), temp_var_644, MPFR_RNDD);

        mpfr_set_d(temp_var_646, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_647, (temp_var_645), temp_var_646, MPFR_RNDD);

        mpfr_set_d(temp_var_648, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_649, (temp_var_647), temp_var_648, MPFR_RNDD);

        mpfr_set_d(temp_var_650, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_651, (temp_var_649), temp_var_650, MPFR_RNDD);

        mpfr_set_d(temp_var_652, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_653, (temp_var_651), temp_var_652, MPFR_RNDD);

        mpfr_set_d(temp_var_654, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_655, (temp_var_653), temp_var_654, MPFR_RNDD);

        mpfr_set_d(temp_var_656, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-1) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uz1_LBM_handleInOutFlow, (temp_var_655), temp_var_656, MPFR_RNDD);
;
        mpfr_div(ux1_LBM_handleInOutFlow, ux1_LBM_handleInOutFlow, rho1_LBM_handleInOutFlow, MPFR_RNDD);
        mpfr_div(uy1_LBM_handleInOutFlow, uy1_LBM_handleInOutFlow, rho1_LBM_handleInOutFlow, MPFR_RNDD);
        mpfr_div(uz1_LBM_handleInOutFlow, uz1_LBM_handleInOutFlow, rho1_LBM_handleInOutFlow, MPFR_RNDD);
    
        mpfr_set_d(temp_var_657, (+srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_658, srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_659, temp_var_657, temp_var_658, MPFR_RNDD);

        mpfr_set_d(temp_var_660, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_661, (temp_var_659), temp_var_660, MPFR_RNDD);

        mpfr_set_d(temp_var_662, srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_663, (temp_var_661), temp_var_662, MPFR_RNDD);

        mpfr_set_d(temp_var_664, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_665, (temp_var_663), temp_var_664, MPFR_RNDD);

        mpfr_set_d(temp_var_666, srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_667, (temp_var_665), temp_var_666, MPFR_RNDD);

        mpfr_set_d(temp_var_668, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_669, (temp_var_667), temp_var_668, MPFR_RNDD);

        mpfr_set_d(temp_var_670, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_671, (temp_var_669), temp_var_670, MPFR_RNDD);

        mpfr_set_d(temp_var_672, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_673, (temp_var_671), temp_var_672, MPFR_RNDD);

        mpfr_set_d(temp_var_674, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_675, (temp_var_673), temp_var_674, MPFR_RNDD);

        mpfr_set_d(temp_var_676, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_677, (temp_var_675), temp_var_676, MPFR_RNDD);

        mpfr_set_d(temp_var_678, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_679, (temp_var_677), temp_var_678, MPFR_RNDD);

        mpfr_set_d(temp_var_680, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_681, (temp_var_679), temp_var_680, MPFR_RNDD);

        mpfr_set_d(temp_var_682, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_683, (temp_var_681), temp_var_682, MPFR_RNDD);

        mpfr_set_d(temp_var_684, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_685, (temp_var_683), temp_var_684, MPFR_RNDD);

        mpfr_set_d(temp_var_686, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_687, (temp_var_685), temp_var_686, MPFR_RNDD);

        mpfr_set_d(temp_var_688, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_689, (temp_var_687), temp_var_688, MPFR_RNDD);

        mpfr_set_d(temp_var_690, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_691, (temp_var_689), temp_var_690, MPFR_RNDD);

        mpfr_set_d(temp_var_692, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_add(rho2_LBM_handleInOutFlow, (temp_var_691), temp_var_692, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_693, (+srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_694, srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_695, temp_var_693, temp_var_694, MPFR_RNDD);

        mpfr_set_d(temp_var_696, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_697, (temp_var_695), temp_var_696, MPFR_RNDD);

        mpfr_set_d(temp_var_698, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_699, (temp_var_697), temp_var_698, MPFR_RNDD);

        mpfr_set_d(temp_var_700, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_701, (temp_var_699), temp_var_700, MPFR_RNDD);

        mpfr_set_d(temp_var_702, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_703, (temp_var_701), temp_var_702, MPFR_RNDD);

        mpfr_set_d(temp_var_704, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_705, (temp_var_703), temp_var_704, MPFR_RNDD);

        mpfr_set_d(temp_var_706, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_707, (temp_var_705), temp_var_706, MPFR_RNDD);

        mpfr_set_d(temp_var_708, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_709, (temp_var_707), temp_var_708, MPFR_RNDD);

        mpfr_set_d(temp_var_710, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(ux2_LBM_handleInOutFlow, (temp_var_709), temp_var_710, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_711, (+srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_712, srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_713, temp_var_711, temp_var_712, MPFR_RNDD);

        mpfr_set_d(temp_var_714, srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_715, (temp_var_713), temp_var_714, MPFR_RNDD);

        mpfr_set_d(temp_var_716, srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_717, (temp_var_715), temp_var_716, MPFR_RNDD);

        mpfr_set_d(temp_var_718, srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_719, (temp_var_717), temp_var_718, MPFR_RNDD);

        mpfr_set_d(temp_var_720, srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_721, (temp_var_719), temp_var_720, MPFR_RNDD);

        mpfr_set_d(temp_var_722, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_723, (temp_var_721), temp_var_722, MPFR_RNDD);

        mpfr_set_d(temp_var_724, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_725, (temp_var_723), temp_var_724, MPFR_RNDD);

        mpfr_set_d(temp_var_726, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_727, (temp_var_725), temp_var_726, MPFR_RNDD);

        mpfr_set_d(temp_var_728, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uy2_LBM_handleInOutFlow, (temp_var_727), temp_var_728, MPFR_RNDD);
;
    
        mpfr_set_d(temp_var_729, (+srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_730, srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_731, temp_var_729, temp_var_730, MPFR_RNDD);

        mpfr_set_d(temp_var_732, srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_733, (temp_var_731), temp_var_732, MPFR_RNDD);

        mpfr_set_d(temp_var_734, srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_735, (temp_var_733), temp_var_734, MPFR_RNDD);

        mpfr_set_d(temp_var_736, srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_737, (temp_var_735), temp_var_736, MPFR_RNDD);

        mpfr_set_d(temp_var_738, srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_739, (temp_var_737), temp_var_738, MPFR_RNDD);

        mpfr_set_d(temp_var_740, srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_741, (temp_var_739), temp_var_740, MPFR_RNDD);

        mpfr_set_d(temp_var_742, srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_sub(temp_var_743, (temp_var_741), temp_var_742, MPFR_RNDD);

        mpfr_set_d(temp_var_744, srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_745, (temp_var_743), temp_var_744, MPFR_RNDD);

        mpfr_set_d(temp_var_746, srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + (((-2) * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_sub(uz2_LBM_handleInOutFlow, (temp_var_745), temp_var_746, MPFR_RNDD);
;
        mpfr_div(ux2_LBM_handleInOutFlow, ux2_LBM_handleInOutFlow, rho2_LBM_handleInOutFlow, MPFR_RNDD);
        mpfr_div(uy2_LBM_handleInOutFlow, uy2_LBM_handleInOutFlow, rho2_LBM_handleInOutFlow, MPFR_RNDD);
        mpfr_div(uz2_LBM_handleInOutFlow, uz2_LBM_handleInOutFlow, rho2_LBM_handleInOutFlow, MPFR_RNDD);
    mpfr_set_d(rho_LBM_handleInOutFlow, 1.0, MPFR_RNDD);
    
        mpfr_mul_si(temp_var_747, ux1_LBM_handleInOutFlow, 2, MPFR_RNDD);
        mpfr_sub(ux_LBM_handleInOutFlow, (temp_var_747), ux2_LBM_handleInOutFlow, MPFR_RNDD);
;
    
        mpfr_mul_si(temp_var_748, uy1_LBM_handleInOutFlow, 2, MPFR_RNDD);
        mpfr_sub(uy_LBM_handleInOutFlow, (temp_var_748), uy2_LBM_handleInOutFlow, MPFR_RNDD);
;
    
        mpfr_mul_si(temp_var_749, uz1_LBM_handleInOutFlow, 2, MPFR_RNDD);
        mpfr_sub(uz_LBM_handleInOutFlow, (temp_var_749), uz2_LBM_handleInOutFlow, MPFR_RNDD);
;
    
        mpfr_mul(temp_var_750, ux_LBM_handleInOutFlow, ux_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_751, uy_LBM_handleInOutFlow, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_752, (temp_var_750), (temp_var_751), MPFR_RNDD);

        mpfr_mul(temp_var_753, uz_LBM_handleInOutFlow, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_754, (temp_var_752), (temp_var_753), MPFR_RNDD);
        mpfr_mul_d(u2_LBM_handleInOutFlow, (temp_var_754), 1.5, MPFR_RNDD);
;
    
        mpfr_mul_d(temp_var_755, rho_LBM_handleInOutFlow, (1.0 / 3.0), MPFR_RNDD);

        mpfr_d_sub(temp_var_756, 1.0, u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_757, (temp_var_755), (temp_var_756), MPFR_RNDD);

srcGrid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_757, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_758, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_759, uy_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_760, (temp_var_759), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_761, uy_LBM_handleInOutFlow, (temp_var_760), MPFR_RNDD);

        mpfr_add_d(temp_var_762, (temp_var_761), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_763, (temp_var_762), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_764, (temp_var_758), (temp_var_763), MPFR_RNDD);

srcGrid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_764, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_765, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_766, uy_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_767, (temp_var_766), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_768, uy_LBM_handleInOutFlow, (temp_var_767), MPFR_RNDD);

        mpfr_add_d(temp_var_769, (temp_var_768), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_770, (temp_var_769), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_771, (temp_var_765), (temp_var_770), MPFR_RNDD);

srcGrid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_771, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_772, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_773, ux_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_774, (temp_var_773), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_775, ux_LBM_handleInOutFlow, (temp_var_774), MPFR_RNDD);

        mpfr_add_d(temp_var_776, (temp_var_775), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_777, (temp_var_776), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_778, (temp_var_772), (temp_var_777), MPFR_RNDD);

srcGrid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_778, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_779, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_780, ux_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_781, (temp_var_780), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_782, ux_LBM_handleInOutFlow, (temp_var_781), MPFR_RNDD);

        mpfr_add_d(temp_var_783, (temp_var_782), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_784, (temp_var_783), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_785, (temp_var_779), (temp_var_784), MPFR_RNDD);

srcGrid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_785, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_786, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_787, uz_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_788, (temp_var_787), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_789, uz_LBM_handleInOutFlow, (temp_var_788), MPFR_RNDD);

        mpfr_add_d(temp_var_790, (temp_var_789), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_791, (temp_var_790), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_792, (temp_var_786), (temp_var_791), MPFR_RNDD);

srcGrid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_792, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_793, rho_LBM_handleInOutFlow, (1.0 / 18.0), MPFR_RNDD);

        mpfr_mul_d(temp_var_794, uz_LBM_handleInOutFlow, 4.5, MPFR_RNDD);

        mpfr_sub_d(temp_var_795, (temp_var_794), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_796, uz_LBM_handleInOutFlow, (temp_var_795), MPFR_RNDD);

        mpfr_add_d(temp_var_797, (temp_var_796), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_798, (temp_var_797), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_799, (temp_var_793), (temp_var_798), MPFR_RNDD);

srcGrid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_799, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_800, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_801, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_802, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_803, (temp_var_802), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_804, (temp_var_803), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_805, (temp_var_801), (temp_var_804), MPFR_RNDD);

        mpfr_add_d(temp_var_806, (temp_var_805), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_807, (temp_var_806), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_808, (temp_var_800), (temp_var_807), MPFR_RNDD);

srcGrid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_808, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_809, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_811, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_810, temp_var_811, uy_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_813, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_812, temp_var_813, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_814, (temp_var_812), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_815, (temp_var_814), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_816, (temp_var_810), (temp_var_815), MPFR_RNDD);

        mpfr_add_d(temp_var_817, (temp_var_816), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_818, (temp_var_817), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_819, (temp_var_809), (temp_var_818), MPFR_RNDD);

srcGrid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_819, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_820, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_821, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_822, (ux_LBM_handleInOutFlow), uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_823, (temp_var_822), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_824, (temp_var_823), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_825, (temp_var_821), (temp_var_824), MPFR_RNDD);

        mpfr_add_d(temp_var_826, (temp_var_825), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_827, (temp_var_826), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_828, (temp_var_820), (temp_var_827), MPFR_RNDD);

srcGrid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_828, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_829, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_831, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_830, temp_var_831, uy_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_833, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_832, temp_var_833, uy_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_834, (temp_var_832), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_835, (temp_var_834), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_836, (temp_var_830), (temp_var_835), MPFR_RNDD);

        mpfr_add_d(temp_var_837, (temp_var_836), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_838, (temp_var_837), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_839, (temp_var_829), (temp_var_838), MPFR_RNDD);

srcGrid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_839, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_840, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_841, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_842, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_843, (temp_var_842), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_844, (temp_var_843), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_845, (temp_var_841), (temp_var_844), MPFR_RNDD);

        mpfr_add_d(temp_var_846, (temp_var_845), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_847, (temp_var_846), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_848, (temp_var_840), (temp_var_847), MPFR_RNDD);

srcGrid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_848, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_849, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_850, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_851, (uy_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_852, (temp_var_851), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_853, (temp_var_852), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_854, (temp_var_850), (temp_var_853), MPFR_RNDD);

        mpfr_add_d(temp_var_855, (temp_var_854), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_856, (temp_var_855), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_857, (temp_var_849), (temp_var_856), MPFR_RNDD);

srcGrid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_857, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_858, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_860, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_859, temp_var_860, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_862, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_861, temp_var_862, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_863, (temp_var_861), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_864, (temp_var_863), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_865, (temp_var_859), (temp_var_864), MPFR_RNDD);

        mpfr_add_d(temp_var_866, (temp_var_865), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_867, (temp_var_866), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_868, (temp_var_858), (temp_var_867), MPFR_RNDD);

srcGrid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_868, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_869, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_871, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_870, temp_var_871, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_873, (uy_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_872, temp_var_873, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_874, (temp_var_872), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_875, (temp_var_874), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_876, (temp_var_870), (temp_var_875), MPFR_RNDD);

        mpfr_add_d(temp_var_877, (temp_var_876), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_878, (temp_var_877), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_879, (temp_var_869), (temp_var_878), MPFR_RNDD);

srcGrid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_879, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_880, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_add(temp_var_881, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_add(temp_var_882, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_883, (temp_var_882), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_884, (temp_var_883), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_885, (temp_var_881), (temp_var_884), MPFR_RNDD);

        mpfr_add_d(temp_var_886, (temp_var_885), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_887, (temp_var_886), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_888, (temp_var_880), (temp_var_887), MPFR_RNDD);

srcGrid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_888, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_889, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        mpfr_sub(temp_var_890, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_sub(temp_var_891, (ux_LBM_handleInOutFlow), uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_892, (temp_var_891), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_893, (temp_var_892), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_894, (temp_var_890), (temp_var_893), MPFR_RNDD);

        mpfr_add_d(temp_var_895, (temp_var_894), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_896, (temp_var_895), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_897, (temp_var_889), (temp_var_896), MPFR_RNDD);

srcGrid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_897, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_898, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_900, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_899, temp_var_900, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_902, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_add(temp_var_901, temp_var_902, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_903, (temp_var_901), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_904, (temp_var_903), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_905, (temp_var_899), (temp_var_904), MPFR_RNDD);

        mpfr_add_d(temp_var_906, (temp_var_905), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_907, (temp_var_906), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_908, (temp_var_898), (temp_var_907), MPFR_RNDD);

srcGrid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_908, MPFR_RNDD);
    
        mpfr_mul_d(temp_var_909, rho_LBM_handleInOutFlow, (1.0 / 36.0), MPFR_RNDD);

        
mpfr_neg (temp_var_911, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_910, temp_var_911, uz_LBM_handleInOutFlow, MPFR_RNDD);

        
mpfr_neg (temp_var_913, (ux_LBM_handleInOutFlow), MPFR_RNDD);
mpfr_sub(temp_var_912, temp_var_913, uz_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul_d(temp_var_914, (temp_var_912), 4.5, MPFR_RNDD);

        mpfr_add_d(temp_var_915, (temp_var_914), 3.0, MPFR_RNDD);

        mpfr_mul(temp_var_916, (temp_var_910), (temp_var_915), MPFR_RNDD);

        mpfr_add_d(temp_var_917, (temp_var_916), 1.0, MPFR_RNDD);

        mpfr_sub(temp_var_918, (temp_var_917), u2_LBM_handleInOutFlow, MPFR_RNDD);

        mpfr_mul(temp_var_919, (temp_var_909), (temp_var_918), MPFR_RNDD);

srcGrid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i] = mpfr_get_d(temp_var_919, MPFR_RNDD);
  }

    }

}

void LBM_showGridStatistics(LBM_Grid grid)
{
  int nObstacleCells = 0;
  int nAccelCells = 0;
  int nFluidCells = 0;
  mpfr_set_d(minU2_LBM_showGridStatistics, 1e+30, MPFR_RNDD);
  mpfr_set_d(maxU2_LBM_showGridStatistics, -1e+30, MPFR_RNDD);
  mpfr_set_d(minRho_LBM_showGridStatistics, 1e+30, MPFR_RNDD);
  mpfr_set_d(maxRho_LBM_showGridStatistics, -1e+30, MPFR_RNDD);
  mpfr_set_d(mass_LBM_showGridStatistics, 0, MPFR_RNDD);
  int i;
  for (i = 0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100)))); i < (0 + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((130 * (1 * 100)) * (1 * 100))))); i += N_CELL_ENTRIES){
  {
    
        mpfr_set_d(temp_var_920, (+grid[(C + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

        mpfr_set_d(temp_var_921, grid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_922, temp_var_920, temp_var_921, MPFR_RNDD);

        mpfr_set_d(temp_var_923, grid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_924, (temp_var_922), temp_var_923, MPFR_RNDD);

        mpfr_set_d(temp_var_925, grid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_926, (temp_var_924), temp_var_925, MPFR_RNDD);

        mpfr_set_d(temp_var_927, grid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_928, (temp_var_926), temp_var_927, MPFR_RNDD);

        mpfr_set_d(temp_var_929, grid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_930, (temp_var_928), temp_var_929, MPFR_RNDD);

        mpfr_set_d(temp_var_931, grid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_932, (temp_var_930), temp_var_931, MPFR_RNDD);

        mpfr_set_d(temp_var_933, grid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_934, (temp_var_932), temp_var_933, MPFR_RNDD);

        mpfr_set_d(temp_var_935, grid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_936, (temp_var_934), temp_var_935, MPFR_RNDD);

        mpfr_set_d(temp_var_937, grid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_938, (temp_var_936), temp_var_937, MPFR_RNDD);

        mpfr_set_d(temp_var_939, grid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_940, (temp_var_938), temp_var_939, MPFR_RNDD);

        mpfr_set_d(temp_var_941, grid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_942, (temp_var_940), temp_var_941, MPFR_RNDD);

        mpfr_set_d(temp_var_943, grid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_944, (temp_var_942), temp_var_943, MPFR_RNDD);

        mpfr_set_d(temp_var_945, grid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_946, (temp_var_944), temp_var_945, MPFR_RNDD);

        mpfr_set_d(temp_var_947, grid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_948, (temp_var_946), temp_var_947, MPFR_RNDD);

        mpfr_set_d(temp_var_949, grid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_950, (temp_var_948), temp_var_949, MPFR_RNDD);

        mpfr_set_d(temp_var_951, grid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_952, (temp_var_950), temp_var_951, MPFR_RNDD);

        mpfr_set_d(temp_var_953, grid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

        mpfr_add(temp_var_954, (temp_var_952), temp_var_953, MPFR_RNDD);

        mpfr_set_d(temp_var_955, grid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
        mpfr_add(rho_LBM_showGridStatistics, (temp_var_954), temp_var_955, MPFR_RNDD);
;
    if (mpfr_less_p(rho_LBM_showGridStatistics, minRho_LBM_showGridStatistics))
      mpfr_set(minRho_LBM_showGridStatistics, rho_LBM_showGridStatistics, MPFR_RNDD);

    if (mpfr_greater_p(rho_LBM_showGridStatistics, maxRho_LBM_showGridStatistics))
      mpfr_set(maxRho_LBM_showGridStatistics, rho_LBM_showGridStatistics, MPFR_RNDD);

        mpfr_add(mass_LBM_showGridStatistics, mass_LBM_showGridStatistics, rho_LBM_showGridStatistics, MPFR_RNDD);
    if ((*((unsigned int *) ((void *) (&grid[(FLAGS + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i])))) & OBSTACLE)
    {
      nObstacleCells++;
    }
    else
    {
      if ((*((unsigned int *) ((void *) (&grid[(FLAGS + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i])))) & ACCEL)
        nAccelCells++;
      else
        nFluidCells++;

      
            mpfr_set_d(temp_var_956, (+grid[(E + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

            mpfr_set_d(temp_var_957, grid[(W + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_958, temp_var_956, temp_var_957, MPFR_RNDD);

            mpfr_set_d(temp_var_959, grid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_960, (temp_var_958), temp_var_959, MPFR_RNDD);

            mpfr_set_d(temp_var_961, grid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_962, (temp_var_960), temp_var_961, MPFR_RNDD);

            mpfr_set_d(temp_var_963, grid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_964, (temp_var_962), temp_var_963, MPFR_RNDD);

            mpfr_set_d(temp_var_965, grid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_966, (temp_var_964), temp_var_965, MPFR_RNDD);

            mpfr_set_d(temp_var_967, grid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_968, (temp_var_966), temp_var_967, MPFR_RNDD);

            mpfr_set_d(temp_var_969, grid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_970, (temp_var_968), temp_var_969, MPFR_RNDD);

            mpfr_set_d(temp_var_971, grid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_972, (temp_var_970), temp_var_971, MPFR_RNDD);

            mpfr_set_d(temp_var_973, grid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
            mpfr_sub(ux_LBM_showGridStatistics, (temp_var_972), temp_var_973, MPFR_RNDD);
;
      
            mpfr_set_d(temp_var_974, (+grid[(N + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

            mpfr_set_d(temp_var_975, grid[(S + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_976, temp_var_974, temp_var_975, MPFR_RNDD);

            mpfr_set_d(temp_var_977, grid[(NE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_978, (temp_var_976), temp_var_977, MPFR_RNDD);

            mpfr_set_d(temp_var_979, grid[(NW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_980, (temp_var_978), temp_var_979, MPFR_RNDD);

            mpfr_set_d(temp_var_981, grid[(SE + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_982, (temp_var_980), temp_var_981, MPFR_RNDD);

            mpfr_set_d(temp_var_983, grid[(SW + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_984, (temp_var_982), temp_var_983, MPFR_RNDD);

            mpfr_set_d(temp_var_985, grid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_986, (temp_var_984), temp_var_985, MPFR_RNDD);

            mpfr_set_d(temp_var_987, grid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_988, (temp_var_986), temp_var_987, MPFR_RNDD);

            mpfr_set_d(temp_var_989, grid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_990, (temp_var_988), temp_var_989, MPFR_RNDD);

            mpfr_set_d(temp_var_991, grid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
            mpfr_sub(uy_LBM_showGridStatistics, (temp_var_990), temp_var_991, MPFR_RNDD);
;
      
            mpfr_set_d(temp_var_992, (+grid[(T + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i]), MPFR_RNDD);

            mpfr_set_d(temp_var_993, grid[(B + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_994, temp_var_992, temp_var_993, MPFR_RNDD);

            mpfr_set_d(temp_var_995, grid[(NT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_996, (temp_var_994), temp_var_995, MPFR_RNDD);

            mpfr_set_d(temp_var_997, grid[(NB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_998, (temp_var_996), temp_var_997, MPFR_RNDD);

            mpfr_set_d(temp_var_999, grid[(ST + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_1000, (temp_var_998), temp_var_999, MPFR_RNDD);

            mpfr_set_d(temp_var_1001, grid[(SB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_1002, (temp_var_1000), temp_var_1001, MPFR_RNDD);

            mpfr_set_d(temp_var_1003, grid[(ET + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_1004, (temp_var_1002), temp_var_1003, MPFR_RNDD);

            mpfr_set_d(temp_var_1005, grid[(EB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_sub(temp_var_1006, (temp_var_1004), temp_var_1005, MPFR_RNDD);

            mpfr_set_d(temp_var_1007, grid[(WT + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);

            mpfr_add(temp_var_1008, (temp_var_1006), temp_var_1007, MPFR_RNDD);

            mpfr_set_d(temp_var_1009, grid[(WB + (N_CELL_ENTRIES * ((0 + (0 * (1 * 100))) + ((0 * (1 * 100)) * (1 * 100))))) + i], MPFR_RNDD);
            mpfr_sub(uz_LBM_showGridStatistics, (temp_var_1008), temp_var_1009, MPFR_RNDD);
;
      
            mpfr_mul(temp_var_1010, ux_LBM_showGridStatistics, ux_LBM_showGridStatistics, MPFR_RNDD);

            mpfr_mul(temp_var_1011, uy_LBM_showGridStatistics, uy_LBM_showGridStatistics, MPFR_RNDD);

            mpfr_add(temp_var_1012, (temp_var_1010), (temp_var_1011), MPFR_RNDD);

            mpfr_mul(temp_var_1013, uz_LBM_showGridStatistics, uz_LBM_showGridStatistics, MPFR_RNDD);

            mpfr_add(temp_var_1014, (temp_var_1012), (temp_var_1013), MPFR_RNDD);

            mpfr_mul(temp_var_1015, rho_LBM_showGridStatistics, rho_LBM_showGridStatistics, MPFR_RNDD);
            mpfr_div(u2_LBM_showGridStatistics, (temp_var_1014), (temp_var_1015), MPFR_RNDD);
;
      if (mpfr_less_p(u2_LBM_showGridStatistics, minU2_LBM_showGridStatistics))
      {
        mpfr_set(minU2_LBM_showGridStatistics, u2_LBM_showGridStatistics, MPFR_RNDD);
      }

      if (mpfr_greater_p(u2_LBM_showGridStatistics, maxU2_LBM_showGridStatistics))
      {
        mpfr_set(maxU2_LBM_showGridStatistics, u2_LBM_showGridStatistics, MPFR_RNDD);
      }

    }

  }

    }

  printf("LBM_showGridStatistics:\n\tnObstacleCells: %7i nAccelCells: %7i nFluidCells: %7i\n\tminRho: %8.4f maxRho: %8.4f mass: %e\n\tminU: %e maxU: %e\n\n", nObstacleCells, nAccelCells, nFluidCells, mpfr_get_d(minRho_LBM_showGridStatistics, MPFR_RNDD), mpfr_get_d(maxRho_LBM_showGridStatistics, MPFR_RNDD), mpfr_get_d(mass_LBM_showGridStatistics, MPFR_RNDD), sqrt(mpfr_get_d(minU2_LBM_showGridStatistics, MPFR_RNDD)), sqrt(mpfr_get_d(maxU2_LBM_showGridStatistics, MPFR_RNDD)));
}

/*############################################################################*/

static void storeValue( FILE* file, OUTPUT_PRECISION* v ) {
	const int litteBigEndianTest = 1;
	if( (*((unsigned char*) &litteBigEndianTest)) == 0 ) {         /* big endian */
		const char* vPtr = (char*) v;
		char buffer[sizeof( OUTPUT_PRECISION )];
		int i;

		for (i = 0; i < sizeof( OUTPUT_PRECISION ); i++)
			buffer[i] = vPtr[sizeof( OUTPUT_PRECISION ) - i - 1];

		fwrite( buffer, sizeof( OUTPUT_PRECISION ), 1, file );
	}
	else {                                                     /* little endian */
		fwrite( v, sizeof( OUTPUT_PRECISION ), 1, file );
	}
}

/*############################################################################*/

static void loadValue( FILE* file, OUTPUT_PRECISION* v ) {
	const int litteBigEndianTest = 1;
	if( (*((unsigned char*) &litteBigEndianTest)) == 0 ) {         /* big endian */
		char* vPtr = (char*) v;
		char buffer[sizeof( OUTPUT_PRECISION )];
		int i;

		fread( buffer, sizeof( OUTPUT_PRECISION ), 1, file );

		for (i = 0; i < sizeof( OUTPUT_PRECISION ); i++)
			vPtr[i] = buffer[sizeof( OUTPUT_PRECISION ) - i - 1];
	}
	else {                                                     /* little endian */
		fread( v, sizeof( OUTPUT_PRECISION ), 1, file );
	}
}

/*############################################################################*/

void LBM_storeVelocityField( LBM_Grid grid, const char* filename,
                             const int binary ) {
	int x, y, z;
	OUTPUT_PRECISION rho, ux, uy, uz;

	FILE* file = fopen( filename, (binary ? "wb" : "w") );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				rho = + GRID_ENTRY( grid, x, y, z, C  ) + GRID_ENTRY( grid, x, y, z, N  )
				      + GRID_ENTRY( grid, x, y, z, S  ) + GRID_ENTRY( grid, x, y, z, E  )
				      + GRID_ENTRY( grid, x, y, z, W  ) + GRID_ENTRY( grid, x, y, z, T  )
				      + GRID_ENTRY( grid, x, y, z, B  ) + GRID_ENTRY( grid, x, y, z, NE )
				      + GRID_ENTRY( grid, x, y, z, NW ) + GRID_ENTRY( grid, x, y, z, SE )
				      + GRID_ENTRY( grid, x, y, z, SW ) + GRID_ENTRY( grid, x, y, z, NT )
				      + GRID_ENTRY( grid, x, y, z, NB ) + GRID_ENTRY( grid, x, y, z, ST )
				      + GRID_ENTRY( grid, x, y, z, SB ) + GRID_ENTRY( grid, x, y, z, ET )
				      + GRID_ENTRY( grid, x, y, z, EB ) + GRID_ENTRY( grid, x, y, z, WT )
				      + GRID_ENTRY( grid, x, y, z, WB );
				ux = + GRID_ENTRY( grid, x, y, z, E  ) - GRID_ENTRY( grid, x, y, z, W  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) - GRID_ENTRY( grid, x, y, z, NW ) 
				     + GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) + GRID_ENTRY( grid, x, y, z, EB ) 
				     - GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				uy = + GRID_ENTRY( grid, x, y, z, N  ) - GRID_ENTRY( grid, x, y, z, S  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) + GRID_ENTRY( grid, x, y, z, NW ) 
				     - GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) + GRID_ENTRY( grid, x, y, z, NB ) 
				     - GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB );
				uz = + GRID_ENTRY( grid, x, y, z, T  ) - GRID_ENTRY( grid, x, y, z, B  ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) - GRID_ENTRY( grid, x, y, z, NB ) 
				     + GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) - GRID_ENTRY( grid, x, y, z, EB ) 
				     + GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				ux /= rho;
				uy /= rho;
				uz /= rho;

				if( binary ) {
					/*
					fwrite( &ux, sizeof( ux ), 1, file );
					fwrite( &uy, sizeof( uy ), 1, file );
					fwrite( &uz, sizeof( uz ), 1, file );
					*/
					storeValue( file, &ux );
					storeValue( file, &uy );
					storeValue( file, &uz );
				} else
					fprintf( file, "%e %e %e\n", ux, uy, uz );

			}
		}
	}

	fclose( file );
}

/*############################################################################*/

void LBM_compareVelocityField( LBM_Grid grid, const char* filename,
                             const int binary ) {
	int x, y, z;
	double rho, ux, uy, uz;
	OUTPUT_PRECISION fileUx, fileUy, fileUz,
	                 dUx, dUy, dUz,
	                 diff2, maxDiff2 = -1e+30;

	FILE* file = fopen( filename, (binary ? "rb" : "r") );

	for( z = 0; z < SIZE_Z; z++ ) {
		for( y = 0; y < SIZE_Y; y++ ) {
			for( x = 0; x < SIZE_X; x++ ) {
				rho = + GRID_ENTRY( grid, x, y, z, C  ) + GRID_ENTRY( grid, x, y, z, N  )
				      + GRID_ENTRY( grid, x, y, z, S  ) + GRID_ENTRY( grid, x, y, z, E  )
				      + GRID_ENTRY( grid, x, y, z, W  ) + GRID_ENTRY( grid, x, y, z, T  )
				      + GRID_ENTRY( grid, x, y, z, B  ) + GRID_ENTRY( grid, x, y, z, NE )
				      + GRID_ENTRY( grid, x, y, z, NW ) + GRID_ENTRY( grid, x, y, z, SE )
				      + GRID_ENTRY( grid, x, y, z, SW ) + GRID_ENTRY( grid, x, y, z, NT )
				      + GRID_ENTRY( grid, x, y, z, NB ) + GRID_ENTRY( grid, x, y, z, ST )
				      + GRID_ENTRY( grid, x, y, z, SB ) + GRID_ENTRY( grid, x, y, z, ET )
				      + GRID_ENTRY( grid, x, y, z, EB ) + GRID_ENTRY( grid, x, y, z, WT )
				      + GRID_ENTRY( grid, x, y, z, WB );
				ux = + GRID_ENTRY( grid, x, y, z, E  ) - GRID_ENTRY( grid, x, y, z, W  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) - GRID_ENTRY( grid, x, y, z, NW ) 
				     + GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) + GRID_ENTRY( grid, x, y, z, EB ) 
				     - GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				uy = + GRID_ENTRY( grid, x, y, z, N  ) - GRID_ENTRY( grid, x, y, z, S  ) 
				     + GRID_ENTRY( grid, x, y, z, NE ) + GRID_ENTRY( grid, x, y, z, NW ) 
				     - GRID_ENTRY( grid, x, y, z, SE ) - GRID_ENTRY( grid, x, y, z, SW ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) + GRID_ENTRY( grid, x, y, z, NB ) 
				     - GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB );
				uz = + GRID_ENTRY( grid, x, y, z, T  ) - GRID_ENTRY( grid, x, y, z, B  ) 
				     + GRID_ENTRY( grid, x, y, z, NT ) - GRID_ENTRY( grid, x, y, z, NB ) 
				     + GRID_ENTRY( grid, x, y, z, ST ) - GRID_ENTRY( grid, x, y, z, SB ) 
				     + GRID_ENTRY( grid, x, y, z, ET ) - GRID_ENTRY( grid, x, y, z, EB ) 
				     + GRID_ENTRY( grid, x, y, z, WT ) - GRID_ENTRY( grid, x, y, z, WB );
				ux /= rho;
				uy /= rho;
				uz /= rho;

				if( binary ) {
					loadValue( file, &fileUx );
					loadValue( file, &fileUy );
					loadValue( file, &fileUz );
				}
				else {
					if( sizeof( OUTPUT_PRECISION ) == sizeof( double )) {
						fscanf( file, "%f %f %f\n", &fileUx, &fileUy, &fileUz );
					}
					else {
						fscanf( file, "%f %f %f\n", &fileUx, &fileUy, &fileUz );
					}
				}

				dUx = ux - fileUx;
				dUy = uy - fileUy;
				dUz = uz - fileUz;
				diff2 = dUx*dUx + dUy*dUy + dUz*dUz;
				if( diff2 > maxDiff2 ) maxDiff2 = diff2;
			}
		}
	}

#if defined(SPEC_CPU)
	printf( "LBM_compareVelocityField: maxDiff = %e  \n\n", sqrt( maxDiff2 ) );
#else
	printf( "LBM_compareVelocityField: maxDiff = %e  ==>  %s\n\n",
	        sqrt( maxDiff2 ),
	        sqrt( maxDiff2 ) > 1e-5 ? "##### ERROR #####" : "OK" );
#endif
	fclose( file );
}

