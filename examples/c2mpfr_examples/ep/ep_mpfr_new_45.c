#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//~ #include "randdp.h"
#include <mpfr.h>
typedef enum { false, true } logical;
typedef struct { 
  double real;
  double imag;
} dcomplex;
#define LEN 46 
 int config_vals[LEN];
mpfr_t x_temp;
mpfr_t q_temp;
  mpfr_t star_x_temp_randlc;
  mpfr_t a_temp_randlc;
  mpfr_t r23_randlc;
  mpfr_t r46_randlc;
  mpfr_t t23_randlc;
  mpfr_t t46_randlc;
  mpfr_t t1_randlc;
  mpfr_t t2_randlc;
  mpfr_t t3_randlc;
  mpfr_t t4_randlc;
  mpfr_t a1_randlc;
  mpfr_t a2_randlc;
  mpfr_t x1_randlc;
  mpfr_t x2_randlc;
  mpfr_t z_randlc;
  mpfr_t r_randlc;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
  mpfr_t star_x_temp_vranlc;
  mpfr_t a_temp_vranlc;
  mpfr_t y_temp_vranlc;
  mpfr_t r23_vranlc;
  mpfr_t r46_vranlc;
  mpfr_t t23_vranlc;
  mpfr_t t46_vranlc;
  mpfr_t t1_vranlc;
  mpfr_t t2_vranlc;
  mpfr_t t3_vranlc;
  mpfr_t t4_vranlc;
  mpfr_t a1_vranlc;
  mpfr_t a2_vranlc;
  mpfr_t x1_vranlc;
  mpfr_t x2_vranlc;
  mpfr_t z_vranlc;
    mpfr_t temp_var_11;
        mpfr_t temp_var_12;
        mpfr_t temp_var_13;
        mpfr_t temp_var_14;
        mpfr_t temp_var_15;
        mpfr_t temp_var_16;
        mpfr_t temp_var_17;
        mpfr_t temp_var_18;
        mpfr_t temp_var_19;
        mpfr_t temp_var_20;
  mpfr_t Mops_main;
  mpfr_t t1_main;
  mpfr_t t2_main;
  mpfr_t t3_main;
  mpfr_t t4_main;
  mpfr_t x1_main;
  mpfr_t x2_main;
  mpfr_t sx_main;
  mpfr_t sy_main;
  mpfr_t an_main;
  mpfr_t gc_main;
        mpfr_t temp_var_21;
            mpfr_t temp_var_22;
            mpfr_t temp_var_23;
            mpfr_t temp_var_24;
            mpfr_t temp_var_25;
            mpfr_t temp_var_26;
            mpfr_t temp_var_27;
            mpfr_t temp_var_28;
                mpfr_t temp_var_29;
                mpfr_t temp_var_30;
                mpfr_t temp_var_31;
int init_mpfr() { 
mpfr_init2(x_temp, config_vals[1]);
mpfr_init2(q_temp, config_vals[2]);
  mpfr_init2(star_x_temp_randlc, config_vals[3]);
  mpfr_init2(a_temp_randlc, config_vals[4]);
  mpfr_init2(r23_randlc, config_vals[5]);
  mpfr_init2(r46_randlc, config_vals[6]);
  mpfr_init2(t23_randlc, config_vals[7]);
  mpfr_init2(t46_randlc, config_vals[8]);
  mpfr_init2(t1_randlc, config_vals[9]);
  mpfr_init2(t2_randlc, config_vals[10]);
  mpfr_init2(t3_randlc, config_vals[11]);
  mpfr_init2(t4_randlc, config_vals[12]);
  mpfr_init2(a1_randlc, config_vals[13]);
  mpfr_init2(a2_randlc, config_vals[14]);
  mpfr_init2(x1_randlc, config_vals[15]);
  mpfr_init2(x2_randlc, config_vals[16]);
  mpfr_init2(z_randlc, config_vals[17]);
  mpfr_init2(r_randlc, config_vals[18]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
    mpfr_init2 (temp_var_4, config_vals[0]);
    mpfr_init2 (temp_var_5, config_vals[0]);
    mpfr_init2 (temp_var_6, config_vals[0]);
    mpfr_init2 (temp_var_7, config_vals[0]);
    mpfr_init2 (temp_var_8, config_vals[0]);
    mpfr_init2 (temp_var_9, config_vals[0]);
    mpfr_init2 (temp_var_10, config_vals[0]);
  mpfr_init2(star_x_temp_vranlc, config_vals[19]);
  mpfr_init2(a_temp_vranlc, config_vals[20]);
  mpfr_init2(y_temp_vranlc, config_vals[21]);
  mpfr_init2(r23_vranlc, config_vals[22]);
  mpfr_init2(r46_vranlc, config_vals[23]);
  mpfr_init2(t23_vranlc, config_vals[24]);
  mpfr_init2(t46_vranlc, config_vals[25]);
  mpfr_init2(t1_vranlc, config_vals[26]);
  mpfr_init2(t2_vranlc, config_vals[27]);
  mpfr_init2(t3_vranlc, config_vals[28]);
  mpfr_init2(t4_vranlc, config_vals[29]);
  mpfr_init2(a1_vranlc, config_vals[30]);
  mpfr_init2(a2_vranlc, config_vals[31]);
  mpfr_init2(x1_vranlc, config_vals[32]);
  mpfr_init2(x2_vranlc, config_vals[33]);
  mpfr_init2(z_vranlc, config_vals[34]);
    mpfr_init2 (temp_var_11, config_vals[0]);
        mpfr_init2 (temp_var_12, config_vals[0]);
        mpfr_init2 (temp_var_13, config_vals[0]);
        mpfr_init2 (temp_var_14, config_vals[0]);
        mpfr_init2 (temp_var_15, config_vals[0]);
        mpfr_init2 (temp_var_16, config_vals[0]);
        mpfr_init2 (temp_var_17, config_vals[0]);
        mpfr_init2 (temp_var_18, config_vals[0]);
        mpfr_init2 (temp_var_19, config_vals[0]);
        mpfr_init2 (temp_var_20, config_vals[0]);
  mpfr_init2(Mops_main, config_vals[35]);
  mpfr_init2(t1_main, config_vals[36]);
  mpfr_init2(t2_main, config_vals[37]);
  mpfr_init2(t3_main, config_vals[38]);
  mpfr_init2(t4_main, config_vals[39]);
  mpfr_init2(x1_main, config_vals[40]);
  mpfr_init2(x2_main, config_vals[41]);
  mpfr_init2(sx_main, config_vals[42]);
  mpfr_init2(sy_main, config_vals[43]);
  mpfr_init2(an_main, config_vals[44]);
  mpfr_init2(gc_main, config_vals[45]);
        mpfr_init2 (temp_var_21, config_vals[0]);
            mpfr_init2 (temp_var_22, config_vals[0]);
            mpfr_init2 (temp_var_23, config_vals[0]);
            mpfr_init2 (temp_var_24, config_vals[0]);
            mpfr_init2 (temp_var_25, config_vals[0]);
            mpfr_init2 (temp_var_26, config_vals[0]);
            mpfr_init2 (temp_var_27, config_vals[0]);
            mpfr_init2 (temp_var_28, config_vals[0]);
                mpfr_init2 (temp_var_29, config_vals[0]);
                mpfr_init2 (temp_var_30, config_vals[0]);
                mpfr_init2 (temp_var_31, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

double randlc(double *x, double a)
{
  mpfr_set_d(r23_randlc, 1.1920928955078125e-07, MPFR_RNDN);
      mpfr_mul(r46_randlc, r23_randlc, r23_randlc, MPFR_RNDN);
  mpfr_set_d(t23_randlc, 8.388608e+06, MPFR_RNDN);
      mpfr_mul(t46_randlc, t23_randlc, t23_randlc, MPFR_RNDN);
  mpfr_set_d(a_temp_randlc, a, MPFR_RNDN);
      mpfr_mul(t1_randlc, r23_randlc, a_temp_randlc, MPFR_RNDN);
 
	//a1 = (int) t1;
  mpfr_set_si(a1_randlc, (int) mpfr_get_d(t1_randlc,MPFR_RNDN), MPFR_RNDN);
  
  
  
    mpfr_mul(temp_var_1, t23_randlc, a1_randlc, MPFR_RNDN);
    mpfr_sub(a2_randlc, a_temp_randlc, (temp_var_1), MPFR_RNDN);
  mpfr_set_d(star_x_temp_randlc, *x, MPFR_RNDN);
      mpfr_mul(t1_randlc, r23_randlc, star_x_temp_randlc, MPFR_RNDN);
      
      
    //~ x1 = (int) t1;  
  mpfr_set_si(x1_randlc, (int) mpfr_get_d(t1_randlc,MPFR_RNDN), MPFR_RNDN);
  
    mpfr_mul(temp_var_2, t23_randlc, x1_randlc, MPFR_RNDN);
    mpfr_sub(x2_randlc, star_x_temp_randlc, (temp_var_2), MPFR_RNDN);
  
    mpfr_mul(temp_var_3, a1_randlc, x2_randlc, MPFR_RNDN);

    mpfr_mul(temp_var_4, a2_randlc, x1_randlc, MPFR_RNDN);
    mpfr_add(t1_randlc, (temp_var_3), (temp_var_4), MPFR_RNDN);
    
    //~ temp_var_5 = r23*t1 ;
    
      //~ t2 = (int) (r23 * t1);
   
    mpfr_mul(temp_var_5, r23_randlc, t1_randlc, MPFR_RNDN);
    
  mpfr_set_si(t2_randlc, (int) mpfr_get_d(temp_var_5,MPFR_RNDN), MPFR_RNDN);
  
  

    mpfr_mul(temp_var_6, t23_randlc, t2_randlc, MPFR_RNDN);
    mpfr_sub(z_randlc, t1_randlc, (temp_var_6), MPFR_RNDN);
  
    mpfr_mul(temp_var_7, t23_randlc, z_randlc, MPFR_RNDN);

    mpfr_mul(temp_var_8, a2_randlc, x2_randlc, MPFR_RNDN);
    mpfr_add(t3_randlc, (temp_var_7), (temp_var_8), MPFR_RNDN);
  
    //~ temp_var_9 = (r46 * t3) ;
    //~ t4 = (int) (r46 * t3) ;
    
    mpfr_mul(temp_var_9, r46_randlc, t3_randlc, MPFR_RNDN);
      
  mpfr_set_si(t4_randlc, (int) mpfr_get_d(temp_var_9,MPFR_RNDN), MPFR_RNDN);
  
  
    

    mpfr_mul(temp_var_10, t46_randlc, t4_randlc, MPFR_RNDN);
    mpfr_sub(star_x_temp_randlc, t3_randlc, (temp_var_10), MPFR_RNDN);
  
*x = mpfr_get_d(star_x_temp_randlc, MPFR_RNDN);
      mpfr_mul(r_randlc, r46_randlc, star_x_temp_randlc, MPFR_RNDN);
  return mpfr_get_d(r_randlc, MPFR_RNDN);
}

void vranlc(int n, double *x, double a, double y[])
{
  mpfr_set_d(r23_vranlc, 1.1920928955078125e-07, MPFR_RNDN);
      mpfr_mul(r46_vranlc, r23_vranlc, r23_vranlc, MPFR_RNDN);
  mpfr_set_d(t23_vranlc, 8.388608e+06, MPFR_RNDN);
      mpfr_mul(t46_vranlc, t23_vranlc, t23_vranlc, MPFR_RNDN);
  int i;
  mpfr_set_d(a_temp_vranlc, a, MPFR_RNDN);
      mpfr_mul(t1_vranlc, r23_vranlc, a_temp_vranlc, MPFR_RNDN);
      
  //a1 = (int) t1;
  mpfr_set_si(a1_vranlc, (int) mpfr_get_d(t1_vranlc,MPFR_RNDN), MPFR_RNDN);
  //~ mpfr_set_d(a1_vranlc, (int) t1_vranlc, MPFR_RNDN);
  
  
    mpfr_mul(temp_var_11, t23_vranlc, a1_vranlc, MPFR_RNDN);
    mpfr_sub(a2_vranlc, a_temp_vranlc, (temp_var_11), MPFR_RNDN);
  for (i = 0; i < n; i++){
  {
    mpfr_set_d(star_x_temp_vranlc, *x, MPFR_RNDN);
            mpfr_mul(t1_vranlc, r23_vranlc, star_x_temp_vranlc, MPFR_RNDN);
            
              //~ x1 = (int) t1;  
  mpfr_set_si(x1_vranlc, (int) mpfr_get_d(t1_vranlc,MPFR_RNDN), MPFR_RNDN);
    //~ mpfr_set_d(x1_vranlc, (int) t1_vranlc, MPFR_RNDN);
    
        mpfr_mul(temp_var_12, t23_vranlc, x1_vranlc, MPFR_RNDN);
        mpfr_sub(x2_vranlc, star_x_temp_vranlc, (temp_var_12), MPFR_RNDN);
    
        mpfr_mul(temp_var_13, a1_vranlc, x2_vranlc, MPFR_RNDN);

        mpfr_mul(temp_var_14, a2_vranlc, x1_vranlc, MPFR_RNDN);
        mpfr_add(t1_vranlc, (temp_var_13), (temp_var_14), MPFR_RNDN);


   //~ temp_var_15 = r23*t1 ;
    
      //~ t2 = (int) (r23 * t1);
     mpfr_mul(temp_var_15, r23_vranlc, t1_vranlc, MPFR_RNDN);
    
  mpfr_set_si(t2_vranlc, (int) mpfr_get_d(temp_var_15,MPFR_RNDN), MPFR_RNDN);
  

    //~ mpfr_set_d(t2_vranlc, (int) (temp_var_15), MPFR_RNDN);
    
      

        mpfr_mul(temp_var_16, t23_vranlc, t2_vranlc, MPFR_RNDN);
        mpfr_sub(z_vranlc, t1_vranlc, (temp_var_16), MPFR_RNDN);
    
        mpfr_mul(temp_var_17, t23_vranlc, z_vranlc, MPFR_RNDN);

        mpfr_mul(temp_var_18, a2_vranlc, x2_vranlc, MPFR_RNDN);
        mpfr_add(t3_vranlc, (temp_var_17), (temp_var_18), MPFR_RNDN);
  
  
    //~ temp_var_9 = (r46 * t3) ;
    //~ t4 = (int) (r46 * t3) ;
    
         mpfr_mul(temp_var_19, r46_vranlc, t3_vranlc, MPFR_RNDN);
  mpfr_set_si(t4_vranlc, (int) mpfr_get_d(temp_var_19,MPFR_RNDN), MPFR_RNDN);
  
    //~ mpfr_set_d(t4_vranlc, (int) (temp_var_19), MPFR_RNDN);
    
     

        mpfr_mul(temp_var_20, t46_vranlc, t4_vranlc, MPFR_RNDN);
        mpfr_sub(star_x_temp_vranlc, t3_vranlc, (temp_var_20), MPFR_RNDN);
    
*x = mpfr_get_d(star_x_temp_vranlc, MPFR_RNDN);
            mpfr_mul(y_temp_vranlc, r46_vranlc, star_x_temp_vranlc, MPFR_RNDN);
    
y[i] = mpfr_get_d(y_temp_vranlc, MPFR_RNDN);
  }

    }

  return ;
}


static double x[2 * (1 << 16)];
static double q[10];

int main()
{
 init_readconfig();
  double sx_verify_value;
  double sy_verify_value;
  double sx_err;
  double sy_err;
  double tt_main, tm_main;
  int np;
  int i;
  int ik;
  int kk;
  int l;
  int k;
  int nit;
  int k_offset;
  int j;
  logical verified;
  logical timers_enabled;
  double dum[3] = {1.0, 1.0, 1.0};
  char size[16];
 // printf("M = %d\n", 28);
  sprintf(size, "%15.0lf", pow(2.0, 28 + 1));
  j = 14;
  if (size[j] == '.')
    j--;

  size[j + 1] = '\0';
 // printf("\n\n NAS Parallel Benchmarks (NPB3.3-SER-C) - EP Benchmark\n");
 // printf("\n Number of random numbers generated: %15s\n", size);
  verified = false;
  np = 1 << (28 - 16);
  vranlc(0, &dum[0], dum[1], &dum[2]);
  dum[0] = randlc(&dum[1], dum[2]);
  for (i = 0; i < (2 * (1 << 16)); i++){
  {
	  mpfr_set_d(x_temp,-1.0e99,MPFR_RNDN);
    //~ x[i] = -1.0e99;
    x[i]=  mpfr_get_d(x_temp,MPFR_RNDN);
  }

    }

  mpfr_set_d(Mops_main, log(sqrt(fabs(1.0 > 1.0 ? 1.0 : 1.0))), MPFR_RNDN);
  double t1_main_fix,t2_main_fix;
  
  mpfr_set_d(t1_main, 1220703125.0, MPFR_RNDN);
  t1_main_fix = 1220703125.0;
  
  vranlc(0, &t1_main_fix, 1220703125.0, x);
  mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
  
  
  mpfr_set_d(t1_main, 1220703125.0, MPFR_RNDN);
  t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
  
  for (i = 0; i < (16 + 1); i++){
  {
    mpfr_set_d(t2_main, randlc(&t1_main_fix, t1_main_fix), MPFR_RNDN);
  }
  

    }

mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);


  mpfr_set(an_main, t1_main, MPFR_RNDN);
 // mpfr_set_d(tt_main, 271828183.0, MPFR_RNDN);
  tt_main = 271828183.0;
  mpfr_set_d(gc_main, 0.0, MPFR_RNDN);
  mpfr_set_d(sx_main, 0.0, MPFR_RNDN);
  mpfr_set_d(sy_main, 0.0, MPFR_RNDN);
  for (i = 0; i < 10; i++){
  {
	  
  mpfr_set_d(q_temp, 0.0, MPFR_RNDN);
  
    //~ q[i] = 0.0;
    q[i] = mpfr_get_d(q_temp,MPFR_RNDN);
  }

    }

  k_offset = -1;
  for (k = 1; k <= np; k++){
  {
    kk = k_offset + k;
    mpfr_set_d(t1_main, 271828183.0, MPFR_RNDN);
    t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
    
    mpfr_set(t2_main, an_main, MPFR_RNDN);
    t2_main_fix = mpfr_get_d(t2_main, MPFR_RNDN);
    
    for (i = 1; i <= 100; i++){
    {
      ik = kk / 2;
      if ((2 * ik) != kk)
      {
		t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
        mpfr_set_d(t3_main, randlc(&t1_main_fix, mpfr_get_d(t2_main, MPFR_RNDN)), MPFR_RNDN);
		mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
		}
      if (ik == 0)
        break;
	
		t2_main_fix = mpfr_get_d(t2_main, MPFR_RNDN);
      mpfr_set_d(t3_main, randlc(&t2_main_fix, mpfr_get_d(t2_main, MPFR_RNDN)), MPFR_RNDN);
      mpfr_set_d(t2_main, t2_main_fix, MPFR_RNDN);
      
      kk = ik;
    }

        }

	t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
    vranlc(131072, &t1_main_fix, 1220703125.0, x);
    mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
    
    //65536 iterations
    for (i = 0; i < (1 << 16); i++){
    {
      
		
            //~ mpfr_set_d(temp_var_3, x[2 * i], MPFR_RNDN);
            mpfr_set_d(x_temp, x[2 * i], MPFR_RNDN);
            mpfr_mul_d(temp_var_4, x_temp, 2.0, MPFR_RNDN);

            //~ mpfr_mul_d(temp_var_4, temp_var_3, 2.0, MPFR_RNDN);
            mpfr_sub_d(x1_main, (temp_var_4), 1.0, MPFR_RNDN);
       
		//~ mpfr_set_d(x1_main, 2.0 * x[2*i] - 1.0, MPFR_RNDN);
       
     //       mpfr_set_d(temp_var_5, x[(2 * i) + 1], MPFR_RNDN);
			mpfr_set_d(x_temp, x[(2 * i) + 1], MPFR_RNDN);
			
     //       mpfr_mul_d(temp_var_6, temp_var_5, 2.0, MPFR_RNDN);
			mpfr_mul_d(temp_var_6, x_temp, 2.0, MPFR_RNDN);
      //      mpfr_sub_d(x2_main, (temp_var_6), 1.0, MPFR_RNDN);
			mpfr_sub_d(x2_main, (temp_var_6), 1.0, MPFR_RNDN);
			
      //~ mpfr_set_d(x2_main, 2.0 * x[2*i+1] - 1.0, MPFR_RNDN);
      
            mpfr_mul(temp_var_7, x1_main, x1_main, MPFR_RNDN);

            mpfr_mul(temp_var_8, x2_main, x2_main, MPFR_RNDN);
            mpfr_add(t1_main, (temp_var_7), (temp_var_8), MPFR_RNDN);
            
      if (mpfr_get_d(t1_main, MPFR_RNDN)<=1.0)
      {
		  
       
        

                mpfr_d_div(temp_var_10, ((-2.0) * log(mpfr_get_d(t1_main, MPFR_RNDN))), t1_main, MPFR_RNDN);
                 mpfr_set_d(t2_main, sqrt(mpfr_get_d(temp_var_10, MPFR_RNDN)), MPFR_RNDN);
                 
                mpfr_mul(t3_main, x1_main, t2_main, MPFR_RNDN);
                        mpfr_mul(t4_main, x2_main, t2_main, MPFR_RNDN);
        l = fabs(mpfr_get_d(t3_main, MPFR_RNDN)) > fabs(mpfr_get_d(t4_main, MPFR_RNDN)) ? fabs(mpfr_get_d(t3_main, MPFR_RNDN)) : fabs(mpfr_get_d(t4_main, MPFR_RNDN));

		mpfr_set_d(q_temp,q[l],MPFR_RNDN);
		mpfr_add_d(q_temp,q_temp,1.0,MPFR_RNDN);
        //~ q[l] = q[l] + 1.0;
        q[l] = mpfr_get_d(q_temp, MPFR_RNDN);
        
                mpfr_add(sx_main, sx_main, t3_main, MPFR_RNDN);
                        mpfr_add(sy_main, sy_main, t4_main, MPFR_RNDN);
      }

    }

        }

  }

    }

  for (i = 0; i < 10; i++){
  {
			mpfr_set_d(q_temp,q[i], MPFR_RNDN);
            mpfr_add(gc_main, gc_main, q_temp, MPFR_RNDN);
  }

    }

  nit = 0;
  verified = true;
  if (28 == 24)
  {
    sx_verify_value = -3.247834652034740e+3;
    sy_verify_value = -6.958407078382297e+3;
  }
  else
    if (28 == 25)
  {
    sx_verify_value = -2.863319731645753e+3;
    sy_verify_value = -6.320053679109499e+3;
  }
  else
    if (28 == 28)
  {
    sx_verify_value = -4.295875165629892e+3;
    sy_verify_value = -1.580732573678431e+4;
  }
  else
    if (28 == 30)
  {
    sx_verify_value = 4.033815542441498e+4;
    sy_verify_value = -2.660669192809235e+4;
  }
  else
    if (28 == 32)
  {
    sx_verify_value = 4.764367927995374e+4;
    sy_verify_value = -8.084072988043731e+4;
  }
  else
    if (28 == 36)
  {
    sx_verify_value = 1.982481200946593e+5;
    sy_verify_value = -1.020596636361769e+5;
  }
  else
    if (28 == 40)
  {
    sx_verify_value = -5.319717441530e+05;
    sy_verify_value = -3.688834557731e+05;
  }
  else
  {
    verified = false;
  }







  if (verified)
  {
    sx_err = fabs((mpfr_get_d(sx_main, MPFR_RNDN) - sx_verify_value) / sx_verify_value);
    sy_err = fabs((mpfr_get_d(sy_main, MPFR_RNDN) - sy_verify_value) / sy_verify_value);
    verified = (sx_err <= 1.0e-8) && (sy_err <= 1.0e-8);
  }

  printf("%.10lf,%.10lf,", sx_err, sy_err);
  


    mpfr_set_d(temp_var_14, pow(2.0, 28 + 1)/tm_main, MPFR_RNDN);
    mpfr_div_d(Mops_main, (temp_var_14), 1000000.0, MPFR_RNDN);
  return 0;
}

//end of conversion, hopefully it will work :)
