	.file	"ep.c"
	.local	x
	.comm	x,524288,32
	.local	q
	.comm	q,40,32
	.section	.rodata
.LC2:
	.string	"M = %d\n"
.LC4:
	.string	"%15.0lf"
	.align 8
.LC5:
	.string	"\n\n NAS Parallel Benchmarks (NPB3.3-SER-C) - EP Benchmark"
	.align 8
.LC6:
	.string	"\n Number of random numbers generated: %15s\n"
	.align 8
.LC18:
	.string	"verify epsilon %.10lf, sx_err %.10lf, sy_err %.10lf\n"
.LC20:
	.string	"\nEP Benchmark Results:\n"
.LC21:
	.string	"CPU Time =%10.4lf\n"
.LC22:
	.string	"N = 2^%5d\n"
.LC23:
	.string	"No. Gaussian Pairs = %15.0lf\n"
.LC24:
	.string	"Sums = %25.15lE %25.15lE\n"
.LC25:
	.string	"Counts: "
.LC26:
	.string	"%3d%15.0lf\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movabsq	$4607182418800017408, %rax
	movq	%rax, -80(%rbp)
	movabsq	$4607182418800017408, %rax
	movq	%rax, -72(%rbp)
	movabsq	$4607182418800017408, %rax
	movq	%rax, -64(%rbp)
	movl	.LC1(%rip), %eax
	movl	%eax, -96(%rbp)
	movl	.LC1(%rip), %eax
	movl	%eax, -92(%rbp)
	movl	.LC1(%rip), %eax
	movl	%eax, -88(%rbp)
	movl	$28, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	movabsq	$4737786807993761792, %rax
	leaq	-48(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movl	$.LC4, %esi
	movq	%rdx, %rdi
	movl	$1, %eax
	call	sprintf
	movl	$14, -224(%rbp)
	movl	-224(%rbp), %eax
	cltq
	movzbl	-48(%rbp,%rax), %eax
	cmpb	$46, %al
	jne	.L2
	subl	$1, -224(%rbp)
.L2:
	movl	-224(%rbp), %eax
	addl	$1, %eax
	cltq
	movb	$0, -48(%rbp,%rax)
	movl	$.LC5, %edi
	call	puts
	leaq	-48(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	movl	$0, -220(%rbp)
	movl	$4096, -216(%rbp)
	movl	$0, -236(%rbp)
	jmp	.L3
.L4:
	movl	-236(%rbp), %eax
	movslq	%eax, %rdx
	movl	.LC7(%rip), %eax
	movl	%eax, x(,%rdx,4)
	addl	$1, -236(%rbp)
.L3:
	cmpl	$131071, -236(%rbp)
	jle	.L4
	movl	.LC8(%rip), %eax
	movl	%eax, -212(%rbp)
	movabsq	$4742906807993761792, %rax
	movq	%rax, -176(%rbp)
	movabsq	$4742906807993761792, %rax
	leaq	-176(%rbp), %rcx
	movl	$x, %edx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movq	%rcx, %rsi
	movl	$0, %edi
	call	vranlc
	movabsq	$4742906807993761792, %rax
	movq	%rax, -176(%rbp)
	movl	$0, -236(%rbp)
	jmp	.L5
.L6:
	movq	-176(%rbp), %rax
	leaq	-176(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movq	%rdx, %rdi
	call	randlc
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	%xmm0, -168(%rbp)
	addl	$1, -236(%rbp)
.L5:
	cmpl	$16, -236(%rbp)
	jle	.L6
	movq	-176(%rbp), %rax
	movq	%rax, -136(%rbp)
	movabsq	$4733340128880099328, %rax
	movq	%rax, -128(%rbp)
	movl	.LC8(%rip), %eax
	movl	%eax, -240(%rbp)
	movl	$0, %eax
	movq	%rax, -160(%rbp)
	movl	.LC8(%rip), %eax
	movl	%eax, -244(%rbp)
	movl	$0, -236(%rbp)
	jmp	.L7
.L8:
	movl	-236(%rbp), %eax
	movslq	%eax, %rdx
	movl	.LC8(%rip), %eax
	movl	%eax, q(,%rdx,4)
	addl	$1, -236(%rbp)
.L7:
	cmpl	$9, -236(%rbp)
	jle	.L8
	movl	$-1, -208(%rbp)
	movl	$1, -228(%rbp)
	jmp	.L9
.L22:
	movl	-228(%rbp), %eax
	movl	-208(%rbp), %edx
	addl	%edx, %eax
	movl	%eax, -232(%rbp)
	movabsq	$4733340128880099328, %rax
	movq	%rax, -176(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -168(%rbp)
	movl	$1, -236(%rbp)
	jmp	.L10
.L14:
	movl	-232(%rbp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	movl	%eax, -204(%rbp)
	movl	-204(%rbp), %eax
	addl	%eax, %eax
	cmpl	-232(%rbp), %eax
	je	.L11
	movq	-168(%rbp), %rax
	leaq	-176(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movq	%rdx, %rdi
	call	randlc
	movss	%xmm0, -264(%rbp)
	movl	-264(%rbp), %eax
	movl	%eax, -200(%rbp)
.L11:
	cmpl	$0, -204(%rbp)
	jne	.L12
	jmp	.L13
.L12:
	movq	-168(%rbp), %rax
	leaq	-168(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movq	%rdx, %rdi
	call	randlc
	movss	%xmm0, -264(%rbp)
	movl	-264(%rbp), %eax
	movl	%eax, -200(%rbp)
	movl	-204(%rbp), %eax
	movl	%eax, -232(%rbp)
	addl	$1, -236(%rbp)
.L10:
	cmpl	$100, -236(%rbp)
	jle	.L14
.L13:
	movabsq	$4742906807993761792, %rax
	leaq	-176(%rbp), %rcx
	movl	$x, %edx
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movq	%rcx, %rsi
	movl	$131072, %edi
	call	vranlc
	movl	$0, -236(%rbp)
	jmp	.L15
.L21:
	movl	-236(%rbp), %eax
	addl	%eax, %eax
	cltq
	movss	x(,%rax,4), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	addsd	%xmm0, %xmm0
	movsd	.LC0(%rip), %xmm1
	subsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm3
	movss	%xmm3, -196(%rbp)
	movl	-236(%rbp), %eax
	addl	%eax, %eax
	addl	$1, %eax
	cltq
	movss	x(,%rax,4), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	addsd	%xmm0, %xmm0
	movsd	.LC0(%rip), %xmm1
	subsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm4
	movss	%xmm4, -192(%rbp)
	movss	-196(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-196(%rbp), %xmm1
	movss	-192(%rbp), %xmm0
	mulss	-192(%rbp), %xmm0
	addss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	%xmm0, -176(%rbp)
	movsd	-176(%rbp), %xmm1
	movsd	.LC0(%rip), %xmm0
	ucomisd	%xmm1, %xmm0
	jb	.L16
	movq	-176(%rbp), %rax
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	call	log
	movsd	.LC12(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	-176(%rbp), %xmm1
	divsd	%xmm1, %xmm0
	call	sqrt
	movsd	%xmm0, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	%rax, -168(%rbp)
	movss	-196(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-168(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm5
	movss	%xmm5, -200(%rbp)
	movss	-192(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-168(%rbp), %xmm1
	mulsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm6
	movss	%xmm6, -188(%rbp)
	movss	-200(%rbp), %xmm1
	movss	.LC13(%rip), %xmm0
	andps	%xmm1, %xmm0
	movss	-188(%rbp), %xmm2
	movss	.LC13(%rip), %xmm1
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.L38
	movss	-200(%rbp), %xmm1
	movss	.LC13(%rip), %xmm0
	andps	%xmm1, %xmm0
	cvttss2si	%xmm0, %eax
	jmp	.L20
.L38:
	movss	-188(%rbp), %xmm1
	movss	.LC13(%rip), %xmm0
	andps	%xmm1, %xmm0
	cvttss2si	%xmm0, %eax
.L20:
	movl	%eax, -184(%rbp)
	movl	-184(%rbp), %eax
	cltq
	movss	q(,%rax,4), %xmm1
	movss	.LC1(%rip), %xmm0
	addss	%xmm1, %xmm0
	movl	-184(%rbp), %eax
	cltq
	movss	%xmm0, q(,%rax,4)
	movss	-200(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-160(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -160(%rbp)
	movss	-244(%rbp), %xmm0
	addss	-188(%rbp), %xmm0
	movss	%xmm0, -244(%rbp)
.L16:
	addl	$1, -236(%rbp)
.L15:
	cmpl	$65535, -236(%rbp)
	jle	.L21
	addl	$1, -228(%rbp)
.L9:
	movl	-228(%rbp), %eax
	cmpl	-216(%rbp), %eax
	jle	.L22
	movl	$0, -236(%rbp)
	jmp	.L23
.L24:
	movl	-236(%rbp), %eax
	cltq
	movss	q(,%rax,4), %xmm0
	movss	-240(%rbp), %xmm1
	addss	%xmm1, %xmm0
	movss	%xmm0, -240(%rbp)
	addl	$1, -236(%rbp)
.L23:
	cmpl	$9, -236(%rbp)
	jle	.L24
	movl	$0, -180(%rbp)
	movl	$1, -220(%rbp)
	movabsq	$-4561926657457598728, %rax
	movq	%rax, -120(%rbp)
	movabsq	$-4553456253300493860, %rax
	movq	%rax, -112(%rbp)
	cmpl	$0, -220(%rbp)
	je	.L25
	movsd	-160(%rbp), %xmm0
	subsd	-120(%rbp), %xmm0
	divsd	-120(%rbp), %xmm0
	movsd	.LC16(%rip), %xmm1
	andpd	%xmm1, %xmm0
	movsd	%xmm0, -152(%rbp)
	movss	-244(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	subsd	-112(%rbp), %xmm0
	divsd	-112(%rbp), %xmm0
	movsd	.LC16(%rip), %xmm1
	andpd	%xmm1, %xmm0
	movsd	%xmm0, -144(%rbp)
	movsd	.LC17(%rip), %xmm0
	ucomisd	-152(%rbp), %xmm0
	jb	.L26
	movsd	.LC17(%rip), %xmm0
	ucomisd	-144(%rbp), %xmm0
	jb	.L26
	movl	$1, %eax
	jmp	.L29
.L26:
	movl	$0, %eax
.L29:
	movl	%eax, -220(%rbp)
.L25:
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movabsq	$4487126258331716666, %rax
	movq	%rcx, -264(%rbp)
	movsd	-264(%rbp), %xmm2
	movq	%rdx, -264(%rbp)
	movsd	-264(%rbp), %xmm1
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movl	$.LC18, %edi
	movl	$3, %eax
	call	printf
	movsd	.LC3(%rip), %xmm0
	divsd	-104(%rbp), %xmm0
	movsd	.LC19(%rip), %xmm1
	divsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm7
	movss	%xmm7, -212(%rbp)
	movl	$.LC20, %edi
	call	puts
	movq	-104(%rbp), %rax
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movl	$.LC21, %edi
	movl	$1, %eax
	call	printf
	movl	$28, %esi
	movl	$.LC22, %edi
	movl	$0, %eax
	call	printf
	movss	-240(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movl	$.LC23, %edi
	movl	$1, %eax
	call	printf
	movss	-244(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movq	-160(%rbp), %rax
	movapd	%xmm0, %xmm1
	movq	%rax, -264(%rbp)
	movsd	-264(%rbp), %xmm0
	movl	$.LC24, %edi
	movl	$2, %eax
	call	printf
	movl	$.LC25, %edi
	call	puts
	movl	$0, -236(%rbp)
	jmp	.L30
.L31:
	movl	-236(%rbp), %eax
	cltq
	movss	q(,%rax,4), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movl	-236(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC26, %edi
	movl	$1, %eax
	call	printf
	addl	$1, -236(%rbp)
.L30:
	cmpl	$9, -236(%rbp)
	jle	.L31
	movl	$0, %eax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	je	.L33
	call	__stack_chk_fail
.L33:
	addq	$264, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 4
.LC1:
	.long	1065353216
	.align 8
.LC3:
	.long	0
	.long	1103101952
	.align 4
.LC7:
	.long	4286578688
	.align 4
.LC8:
	.long	0
	.align 8
.LC12:
	.long	0
	.long	-1073741824
	.align 16
.LC13:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.align 16
.LC16:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 8
.LC17:
	.long	3794832442
	.long	1044740494
	.align 8
.LC19:
	.long	0
	.long	1093567616
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
