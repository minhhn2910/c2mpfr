
randdp.o:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <randlc>:
#include <stdio.h>
#include <math.h>

float randlc( double *x, double a )
{
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 89 7d b8          	mov    %rdi,-0x48(%rbp)
   8:	f2 0f 11 45 b0       	movsd  %xmm0,-0x50(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
   d:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 13 <randlc+0x13>
  13:	89 45 c0             	mov    %eax,-0x40(%rbp)
  const float r46 = r23 * r23;
  16:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  1b:	f3 0f 59 45 c0       	mulss  -0x40(%rbp),%xmm0
  20:	f3 0f 11 45 c4       	movss  %xmm0,-0x3c(%rbp)
  const float t23 = 8.388608e+06;
  25:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 2b <randlc+0x2b>
  2b:	89 45 c8             	mov    %eax,-0x38(%rbp)
  const float t46 = t23 * t23;
  2e:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  33:	f3 0f 59 45 c8       	mulss  -0x38(%rbp),%xmm0
  38:	f3 0f 11 45 cc       	movss  %xmm0,-0x34(%rbp)
  float r;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
  3d:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  42:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  45:	f2 0f 59 45 b0       	mulsd  -0x50(%rbp),%xmm0
  4a:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  a1 = (int) t1;
  4f:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
  54:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  58:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
  5c:	f3 0f 11 45 d0       	movss  %xmm0,-0x30(%rbp)
  a2 = a - t23 * a1;
  61:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  66:	f3 0f 59 45 d0       	mulss  -0x30(%rbp),%xmm0
  6b:	0f 14 c0             	unpcklps %xmm0,%xmm0
  6e:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  71:	f2 0f 10 4d b0       	movsd  -0x50(%rbp),%xmm1
  76:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  7a:	66 0f 28 c1          	movapd %xmm1,%xmm0
  7e:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
  82:	66 0f 5a d0          	cvtpd2ps %xmm0,%xmm2
  86:	f3 0f 11 55 d4       	movss  %xmm2,-0x2c(%rbp)
  //--------------------------------------------------------------------
  //  Break X into two parts such that X = 2^23 * X1 + X2, compute
  //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
  //  X = 2^23 * Z + A2 * X2  (mod 2^46).
  //--------------------------------------------------------------------
  t1 = r23 * (*x);
  8b:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  90:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  93:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
  97:	f2 0f 10 08          	movsd  (%rax),%xmm1
  9b:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
  9f:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  x1 = (int) t1;
  a4:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
  a9:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  ad:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
  b1:	f3 0f 11 45 d8       	movss  %xmm0,-0x28(%rbp)
  
  x2 = *x - t23 * x1;
  b6:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
  ba:	f2 0f 10 08          	movsd  (%rax),%xmm1
  be:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  c3:	f3 0f 59 45 d8       	mulss  -0x28(%rbp),%xmm0
  c8:	0f 14 c0             	unpcklps %xmm0,%xmm0
  cb:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  ce:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  d2:	66 0f 28 c1          	movapd %xmm1,%xmm0
  d6:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
  da:	66 0f 5a d8          	cvtpd2ps %xmm0,%xmm3
  de:	f3 0f 11 5d dc       	movss  %xmm3,-0x24(%rbp)
  
  t1 = a1 * x2 + a2 * x1;
  e3:	f3 0f 10 45 d0       	movss  -0x30(%rbp),%xmm0
  e8:	0f 28 c8             	movaps %xmm0,%xmm1
  eb:	f3 0f 59 4d dc       	mulss  -0x24(%rbp),%xmm1
  f0:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
  f5:	f3 0f 59 45 d8       	mulss  -0x28(%rbp),%xmm0
  fa:	f3 0f 58 c1          	addss  %xmm1,%xmm0
  fe:	0f 14 c0             	unpcklps %xmm0,%xmm0
 101:	0f 5a e0             	cvtps2pd %xmm0,%xmm4
 104:	f2 0f 11 65 f0       	movsd  %xmm4,-0x10(%rbp)
  
//~ if(t1!=t1_temp)
	//~ printf("diff %lf, %lf \n\n",t1,t1_temp);
//~ printf("%E, %E \n\n",t1,t1_temp);
  
  t2 = (int) (r23 * t1);
 109:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
 10e:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 111:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 116:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 11a:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 11e:	f3 0f 11 45 e0       	movss  %xmm0,-0x20(%rbp)
  z = t1 - t23 * t2;
 123:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 128:	f3 0f 59 45 e0       	mulss  -0x20(%rbp),%xmm0
 12d:	0f 14 c0             	unpcklps %xmm0,%xmm0
 130:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 133:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 138:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 13c:	66 0f 28 c1          	movapd %xmm1,%xmm0
 140:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 144:	66 0f 5a e8          	cvtpd2ps %xmm0,%xmm5
 148:	f3 0f 11 6d e4       	movss  %xmm5,-0x1c(%rbp)
  
  //~ t3 = t23 * z + a2 * (double) x2;
  t3 = t23 * z + a2 * x2;
 14d:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 152:	0f 28 c8             	movaps %xmm0,%xmm1
 155:	f3 0f 59 4d e4       	mulss  -0x1c(%rbp),%xmm1
 15a:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
 15f:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 164:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 168:	0f 14 c0             	unpcklps %xmm0,%xmm0
 16b:	0f 5a f0             	cvtps2pd %xmm0,%xmm6
 16e:	f2 0f 11 75 f8       	movsd  %xmm6,-0x8(%rbp)
  
  t4 = (int) (r46 * t3);
 173:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 178:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 17b:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 180:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 184:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 188:	f3 0f 11 45 e8       	movss  %xmm0,-0x18(%rbp)
  *x = t3 - t46 * t4;
 18d:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 192:	f3 0f 59 45 e8       	mulss  -0x18(%rbp),%xmm0
 197:	0f 14 c0             	unpcklps %xmm0,%xmm0
 19a:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 19d:	f2 0f 10 4d f8       	movsd  -0x8(%rbp),%xmm1
 1a2:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 1a6:	66 0f 28 c1          	movapd %xmm1,%xmm0
 1aa:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
 1ae:	f2 0f 11 00          	movsd  %xmm0,(%rax)
  r = r46 * (*x);
 1b2:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 1b7:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 1ba:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
 1be:	f2 0f 10 08          	movsd  (%rax),%xmm1
 1c2:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 1c6:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 1ca:	66 0f 5a f8          	cvtpd2ps %xmm0,%xmm7
 1ce:	f3 0f 11 7d ec       	movss  %xmm7,-0x14(%rbp)

  return r;
 1d3:	8b 45 ec             	mov    -0x14(%rbp),%eax
}
 1d6:	89 45 ac             	mov    %eax,-0x54(%rbp)
 1d9:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
 1de:	5d                   	pop    %rbp
 1df:	c3                   	retq   

00000000000001e0 <vranlc>:


void vranlc( int n, double *x, double a, float y[] )
{
 1e0:	55                   	push   %rbp
 1e1:	48 89 e5             	mov    %rsp,%rbp
 1e4:	89 7d bc             	mov    %edi,-0x44(%rbp)
 1e7:	48 89 75 b0          	mov    %rsi,-0x50(%rbp)
 1eb:	f2 0f 11 45 a8       	movsd  %xmm0,-0x58(%rbp)
 1f0:	48 89 55 a0          	mov    %rdx,-0x60(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
 1f4:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 1fa <vranlc+0x1a>
 1fa:	89 45 c4             	mov    %eax,-0x3c(%rbp)
  const float r46 = r23 * r23;
 1fd:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 202:	f3 0f 59 45 c4       	mulss  -0x3c(%rbp),%xmm0
 207:	f3 0f 11 45 c8       	movss  %xmm0,-0x38(%rbp)
  const float t23 = 8.388608e+06;
 20c:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 212 <vranlc+0x32>
 212:	89 45 cc             	mov    %eax,-0x34(%rbp)
  const float t46 = t23 * t23;
 215:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 21a:	f3 0f 59 45 cc       	mulss  -0x34(%rbp),%xmm0
 21f:	f3 0f 11 45 d0       	movss  %xmm0,-0x30(%rbp)
  int i;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
 224:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 229:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 22c:	f2 0f 59 45 a8       	mulsd  -0x58(%rbp),%xmm0
 231:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  a1 = (int) t1;
 236:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 23b:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 23f:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 243:	f3 0f 11 45 d4       	movss  %xmm0,-0x2c(%rbp)
  a2 = a - t23 * a1;
 248:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 24d:	f3 0f 59 45 d4       	mulss  -0x2c(%rbp),%xmm0
 252:	0f 14 c0             	unpcklps %xmm0,%xmm0
 255:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 258:	f2 0f 10 4d a8       	movsd  -0x58(%rbp),%xmm1
 25d:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 261:	66 0f 28 c1          	movapd %xmm1,%xmm0
 265:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 269:	66 0f 5a d0          	cvtpd2ps %xmm0,%xmm2
 26d:	f3 0f 11 55 d8       	movss  %xmm2,-0x28(%rbp)

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 272:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
 279:	e9 5f 01 00 00       	jmpq   3dd <vranlc+0x1fd>
    //--------------------------------------------------------------------
    //  Break X into two parts such that X = 2^23 * X1 + X2, compute
    //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
    //  X = 2^23 * Z + A2 * X2  (mod 2^46).
    //--------------------------------------------------------------------
    t1 = r23 * (*x);
 27e:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 283:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 286:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 28a:	f2 0f 10 08          	movsd  (%rax),%xmm1
 28e:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 292:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
    x1 = (int) t1;
 297:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 29c:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 2a0:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 2a4:	f3 0f 11 45 dc       	movss  %xmm0,-0x24(%rbp)
    
    x2 = *x - t23 * x1;
 2a9:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2ad:	f2 0f 10 08          	movsd  (%rax),%xmm1
 2b1:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 2b6:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 2bb:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2be:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 2c1:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 2c5:	66 0f 28 c1          	movapd %xmm1,%xmm0
 2c9:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 2cd:	66 0f 5a d8          	cvtpd2ps %xmm0,%xmm3
 2d1:	f3 0f 11 5d e0       	movss  %xmm3,-0x20(%rbp)
    
    //~ t1 = a1 * (double)x2 + a2 * (double)x1;
    t1 = a1 * x2 + a2 * x1;
 2d6:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
 2db:	0f 28 c8             	movaps %xmm0,%xmm1
 2de:	f3 0f 59 4d e0       	mulss  -0x20(%rbp),%xmm1
 2e3:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 2e8:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 2ed:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 2f1:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2f4:	0f 5a e0             	cvtps2pd %xmm0,%xmm4
 2f7:	f2 0f 11 65 f0       	movsd  %xmm4,-0x10(%rbp)
    
    t2 = (int) (r23 * t1);
 2fc:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 301:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 304:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 309:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 30d:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 311:	f3 0f 11 45 e4       	movss  %xmm0,-0x1c(%rbp)
    z = t1 - t23 * t2;
 316:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 31b:	f3 0f 59 45 e4       	mulss  -0x1c(%rbp),%xmm0
 320:	0f 14 c0             	unpcklps %xmm0,%xmm0
 323:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 326:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 32b:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 32f:	66 0f 28 c1          	movapd %xmm1,%xmm0
 333:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 337:	66 0f 5a e8          	cvtpd2ps %xmm0,%xmm5
 33b:	f3 0f 11 6d e8       	movss  %xmm5,-0x18(%rbp)
    
    //~ t3 = t23 * z + a2 * (double)x2;
    t3 = t23 * z + a2 * x2;
 340:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 345:	0f 28 c8             	movaps %xmm0,%xmm1
 348:	f3 0f 59 4d e8       	mulss  -0x18(%rbp),%xmm1
 34d:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 352:	f3 0f 59 45 e0       	mulss  -0x20(%rbp),%xmm0
 357:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 35b:	0f 14 c0             	unpcklps %xmm0,%xmm0
 35e:	0f 5a f0             	cvtps2pd %xmm0,%xmm6
 361:	f2 0f 11 75 f8       	movsd  %xmm6,-0x8(%rbp)
    
    t4 = (int) (r46 * t3) ;
 366:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 36b:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 36e:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 373:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 377:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 37b:	f3 0f 11 45 ec       	movss  %xmm0,-0x14(%rbp)
    *x = t3 - t46 * t4;
 380:	f3 0f 10 45 d0       	movss  -0x30(%rbp),%xmm0
 385:	f3 0f 59 45 ec       	mulss  -0x14(%rbp),%xmm0
 38a:	0f 14 c0             	unpcklps %xmm0,%xmm0
 38d:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 390:	f2 0f 10 4d f8       	movsd  -0x8(%rbp),%xmm1
 395:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 399:	66 0f 28 c1          	movapd %xmm1,%xmm0
 39d:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3a1:	f2 0f 11 00          	movsd  %xmm0,(%rax)
    y[i] = r46 * (*x);
 3a5:	8b 45 c0             	mov    -0x40(%rbp),%eax
 3a8:	48 98                	cltq   
 3aa:	48 8d 14 85 00 00 00 	lea    0x0(,%rax,4),%rdx
 3b1:	00 
 3b2:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
 3b6:	48 01 c2             	add    %rax,%rdx
 3b9:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 3be:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 3c1:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3c5:	f2 0f 10 08          	movsd  (%rax),%xmm1
 3c9:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 3cd:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 3d1:	66 0f 5a c0          	cvtpd2ps %xmm0,%xmm0
 3d5:	f3 0f 11 02          	movss  %xmm0,(%rdx)
  a2 = a - t23 * a1;

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 3d9:	83 45 c0 01          	addl   $0x1,-0x40(%rbp)
 3dd:	8b 45 c0             	mov    -0x40(%rbp),%eax
 3e0:	3b 45 bc             	cmp    -0x44(%rbp),%eax
 3e3:	0f 8c 95 fe ff ff    	jl     27e <vranlc+0x9e>
    t4 = (int) (r46 * t3) ;
    *x = t3 - t46 * t4;
    y[i] = r46 * (*x);
  }

  return;
 3e9:	90                   	nop
}
 3ea:	5d                   	pop    %rbp
 3eb:	c3                   	retq   
