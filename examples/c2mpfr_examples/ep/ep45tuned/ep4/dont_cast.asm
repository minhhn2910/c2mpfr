
randdp_cast.o:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <randlc>:
#include <stdio.h>
#include <math.h>

float randlc( double *x, double a )
{
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 89 7d 98          	mov    %rdi,-0x68(%rbp)
   8:	f2 0f 11 45 90       	movsd  %xmm0,-0x70(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
   d:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 13 <randlc+0x13>
  13:	89 45 a4             	mov    %eax,-0x5c(%rbp)
  const float r46 = r23 * r23;
  16:	f3 0f 10 45 a4       	movss  -0x5c(%rbp),%xmm0
  1b:	f3 0f 59 45 a4       	mulss  -0x5c(%rbp),%xmm0
  20:	f3 0f 11 45 a8       	movss  %xmm0,-0x58(%rbp)
  const float t23 = 8.388608e+06;
  25:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 2b <randlc+0x2b>
  2b:	89 45 ac             	mov    %eax,-0x54(%rbp)
  const float t46 = t23 * t23;
  2e:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
  33:	f3 0f 59 45 ac       	mulss  -0x54(%rbp),%xmm0
  38:	f3 0f 11 45 b0       	movss  %xmm0,-0x50(%rbp)
  float r;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
  3d:	f3 0f 10 45 a4       	movss  -0x5c(%rbp),%xmm0
  42:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  45:	f2 0f 59 45 90       	mulsd  -0x70(%rbp),%xmm0
  4a:	f2 0f 11 45 b8       	movsd  %xmm0,-0x48(%rbp)
  a1 = (int) t1;
  4f:	f2 0f 10 45 b8       	movsd  -0x48(%rbp),%xmm0
  54:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  58:	f2 0f 2a c0          	cvtsi2sd %eax,%xmm0
  5c:	f2 0f 11 45 c0       	movsd  %xmm0,-0x40(%rbp)
  a2 = a - t23 * a1;
  61:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
  66:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  69:	f2 0f 59 45 c0       	mulsd  -0x40(%rbp),%xmm0
  6e:	f2 0f 10 4d 90       	movsd  -0x70(%rbp),%xmm1
  73:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  77:	66 0f 28 c1          	movapd %xmm1,%xmm0
  7b:	f2 0f 11 45 c8       	movsd  %xmm0,-0x38(%rbp)
  //--------------------------------------------------------------------
  //  Break X into two parts such that X = 2^23 * X1 + X2, compute
  //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
  //  X = 2^23 * Z + A2 * X2  (mod 2^46).
  //--------------------------------------------------------------------
  t1 = r23 * (*x);
  80:	f3 0f 10 45 a4       	movss  -0x5c(%rbp),%xmm0
  85:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  88:	48 8b 45 98          	mov    -0x68(%rbp),%rax
  8c:	f2 0f 10 08          	movsd  (%rax),%xmm1
  90:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
  94:	f2 0f 11 45 b8       	movsd  %xmm0,-0x48(%rbp)
  x1 = (int) t1;
  99:	f2 0f 10 45 b8       	movsd  -0x48(%rbp),%xmm0
  9e:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  a2:	f2 0f 2a c0          	cvtsi2sd %eax,%xmm0
  a6:	f2 0f 11 45 d0       	movsd  %xmm0,-0x30(%rbp)
  
  x2 = *x - t23 * x1;
  ab:	48 8b 45 98          	mov    -0x68(%rbp),%rax
  af:	f2 0f 10 08          	movsd  (%rax),%xmm1
  b3:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
  b8:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  bb:	f2 0f 59 45 d0       	mulsd  -0x30(%rbp),%xmm0
  c0:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  c4:	66 0f 28 c1          	movapd %xmm1,%xmm0
  c8:	f2 0f 11 45 d8       	movsd  %xmm0,-0x28(%rbp)
  
  t1 = a1 * x2 + a2 * x1;
  cd:	f2 0f 10 45 c0       	movsd  -0x40(%rbp),%xmm0
  d2:	66 0f 28 c8          	movapd %xmm0,%xmm1
  d6:	f2 0f 59 4d d8       	mulsd  -0x28(%rbp),%xmm1
  db:	f2 0f 10 45 c8       	movsd  -0x38(%rbp),%xmm0
  e0:	f2 0f 59 45 d0       	mulsd  -0x30(%rbp),%xmm0
  e5:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
  e9:	f2 0f 11 45 b8       	movsd  %xmm0,-0x48(%rbp)
  
//~ if(t1!=t1_temp)
	//~ printf("diff %lf, %lf \n\n",t1,t1_temp);
//~ printf("%E, %E \n\n",t1,t1_temp);
  
  t2 = (int) (r23 * t1);
  ee:	f3 0f 10 45 a4       	movss  -0x5c(%rbp),%xmm0
  f3:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  f6:	f2 0f 59 45 b8       	mulsd  -0x48(%rbp),%xmm0
  fb:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  ff:	f2 0f 2a c0          	cvtsi2sd %eax,%xmm0
 103:	f2 0f 11 45 e0       	movsd  %xmm0,-0x20(%rbp)
  z = t1 - t23 * t2;
 108:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
 10d:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 110:	f2 0f 59 45 e0       	mulsd  -0x20(%rbp),%xmm0
 115:	f2 0f 10 4d b8       	movsd  -0x48(%rbp),%xmm1
 11a:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 11e:	66 0f 28 c1          	movapd %xmm1,%xmm0
 122:	f2 0f 11 45 e8       	movsd  %xmm0,-0x18(%rbp)
  
  //~ t3 = t23 * z + a2 * (double) x2;
  t3 = t23 * z + a2 * x2;
 127:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
 12c:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 12f:	66 0f 28 c8          	movapd %xmm0,%xmm1
 133:	f2 0f 59 4d e8       	mulsd  -0x18(%rbp),%xmm1
 138:	f2 0f 10 45 c8       	movsd  -0x38(%rbp),%xmm0
 13d:	f2 0f 59 45 d8       	mulsd  -0x28(%rbp),%xmm0
 142:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
 146:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  
  t4 = (int) (r46 * t3);
 14b:	f3 0f 10 45 a8       	movss  -0x58(%rbp),%xmm0
 150:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 153:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 158:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 15c:	f2 0f 2a c0          	cvtsi2sd %eax,%xmm0
 160:	f2 0f 11 45 f8       	movsd  %xmm0,-0x8(%rbp)
  *x = t3 - t46 * t4;
 165:	f3 0f 10 45 b0       	movss  -0x50(%rbp),%xmm0
 16a:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 16d:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 172:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 177:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 17b:	66 0f 28 c1          	movapd %xmm1,%xmm0
 17f:	48 8b 45 98          	mov    -0x68(%rbp),%rax
 183:	f2 0f 11 00          	movsd  %xmm0,(%rax)
  r = r46 * (*x);
 187:	f3 0f 10 45 a8       	movss  -0x58(%rbp),%xmm0
 18c:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 18f:	48 8b 45 98          	mov    -0x68(%rbp),%rax
 193:	f2 0f 10 08          	movsd  (%rax),%xmm1
 197:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 19b:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 19f:	66 0f 5a d0          	cvtpd2ps %xmm0,%xmm2
 1a3:	f3 0f 11 55 b4       	movss  %xmm2,-0x4c(%rbp)

  return r;
 1a8:	8b 45 b4             	mov    -0x4c(%rbp),%eax
}
 1ab:	89 45 8c             	mov    %eax,-0x74(%rbp)
 1ae:	f3 0f 10 45 8c       	movss  -0x74(%rbp),%xmm0
 1b3:	5d                   	pop    %rbp
 1b4:	c3                   	retq   

00000000000001b5 <vranlc>:


void vranlc( int n, double *x, double a, float y[] )
{
 1b5:	55                   	push   %rbp
 1b6:	48 89 e5             	mov    %rsp,%rbp
 1b9:	89 7d bc             	mov    %edi,-0x44(%rbp)
 1bc:	48 89 75 b0          	mov    %rsi,-0x50(%rbp)
 1c0:	f2 0f 11 45 a8       	movsd  %xmm0,-0x58(%rbp)
 1c5:	48 89 55 a0          	mov    %rdx,-0x60(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
 1c9:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 1cf <vranlc+0x1a>
 1cf:	89 45 c4             	mov    %eax,-0x3c(%rbp)
  const float r46 = r23 * r23;
 1d2:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 1d7:	f3 0f 59 45 c4       	mulss  -0x3c(%rbp),%xmm0
 1dc:	f3 0f 11 45 c8       	movss  %xmm0,-0x38(%rbp)
  const float t23 = 8.388608e+06;
 1e1:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 1e7 <vranlc+0x32>
 1e7:	89 45 cc             	mov    %eax,-0x34(%rbp)
  const float t46 = t23 * t23;
 1ea:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 1ef:	f3 0f 59 45 cc       	mulss  -0x34(%rbp),%xmm0
 1f4:	f3 0f 11 45 d0       	movss  %xmm0,-0x30(%rbp)
  int i;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
 1f9:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 1fe:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 201:	f2 0f 59 45 a8       	mulsd  -0x58(%rbp),%xmm0
 206:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  a1 = (int) t1;
 20b:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 210:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 214:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 218:	f3 0f 11 45 d4       	movss  %xmm0,-0x2c(%rbp)
  a2 = a - t23 * a1;
 21d:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 222:	f3 0f 59 45 d4       	mulss  -0x2c(%rbp),%xmm0
 227:	0f 14 c0             	unpcklps %xmm0,%xmm0
 22a:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 22d:	f2 0f 10 4d a8       	movsd  -0x58(%rbp),%xmm1
 232:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 236:	66 0f 28 c1          	movapd %xmm1,%xmm0
 23a:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 23e:	66 0f 5a d0          	cvtpd2ps %xmm0,%xmm2
 242:	f3 0f 11 55 d8       	movss  %xmm2,-0x28(%rbp)

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 247:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
 24e:	e9 5f 01 00 00       	jmpq   3b2 <vranlc+0x1fd>
    //--------------------------------------------------------------------
    //  Break X into two parts such that X = 2^23 * X1 + X2, compute
    //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
    //  X = 2^23 * Z + A2 * X2  (mod 2^46).
    //--------------------------------------------------------------------
    t1 = r23 * (*x);
 253:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 258:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 25b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 25f:	f2 0f 10 08          	movsd  (%rax),%xmm1
 263:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 267:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
    x1 = (int) t1;
 26c:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 271:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 275:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 279:	f3 0f 11 45 dc       	movss  %xmm0,-0x24(%rbp)
    
    x2 = *x - t23 * x1;
 27e:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 282:	f2 0f 10 08          	movsd  (%rax),%xmm1
 286:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 28b:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 290:	0f 14 c0             	unpcklps %xmm0,%xmm0
 293:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 296:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 29a:	66 0f 28 c1          	movapd %xmm1,%xmm0
 29e:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 2a2:	66 0f 5a d8          	cvtpd2ps %xmm0,%xmm3
 2a6:	f3 0f 11 5d e0       	movss  %xmm3,-0x20(%rbp)
    
    //~ t1 = a1 * (double)x2 + a2 * (double)x1;
    t1 = a1 * x2 + a2 * x1;
 2ab:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
 2b0:	0f 28 c8             	movaps %xmm0,%xmm1
 2b3:	f3 0f 59 4d e0       	mulss  -0x20(%rbp),%xmm1
 2b8:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 2bd:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 2c2:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 2c6:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2c9:	0f 5a e0             	cvtps2pd %xmm0,%xmm4
 2cc:	f2 0f 11 65 f0       	movsd  %xmm4,-0x10(%rbp)
    
    t2 = (int) (r23 * t1);
 2d1:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 2d6:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 2d9:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 2de:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 2e2:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 2e6:	f3 0f 11 45 e4       	movss  %xmm0,-0x1c(%rbp)
    z = t1 - t23 * t2;
 2eb:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 2f0:	f3 0f 59 45 e4       	mulss  -0x1c(%rbp),%xmm0
 2f5:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2f8:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 2fb:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 300:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 304:	66 0f 28 c1          	movapd %xmm1,%xmm0
 308:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 30c:	66 0f 5a e8          	cvtpd2ps %xmm0,%xmm5
 310:	f3 0f 11 6d e8       	movss  %xmm5,-0x18(%rbp)
    
    //~ t3 = t23 * z + a2 * (double)x2;
    t3 = t23 * z + a2 * x2;
 315:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 31a:	0f 28 c8             	movaps %xmm0,%xmm1
 31d:	f3 0f 59 4d e8       	mulss  -0x18(%rbp),%xmm1
 322:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 327:	f3 0f 59 45 e0       	mulss  -0x20(%rbp),%xmm0
 32c:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 330:	0f 14 c0             	unpcklps %xmm0,%xmm0
 333:	0f 5a f0             	cvtps2pd %xmm0,%xmm6
 336:	f2 0f 11 75 f8       	movsd  %xmm6,-0x8(%rbp)
    
    t4 = (int) (r46 * t3) ;
 33b:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 340:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 343:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 348:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 34c:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 350:	f3 0f 11 45 ec       	movss  %xmm0,-0x14(%rbp)
    *x = t3 - t46 * t4;
 355:	f3 0f 10 45 d0       	movss  -0x30(%rbp),%xmm0
 35a:	f3 0f 59 45 ec       	mulss  -0x14(%rbp),%xmm0
 35f:	0f 14 c0             	unpcklps %xmm0,%xmm0
 362:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 365:	f2 0f 10 4d f8       	movsd  -0x8(%rbp),%xmm1
 36a:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 36e:	66 0f 28 c1          	movapd %xmm1,%xmm0
 372:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 376:	f2 0f 11 00          	movsd  %xmm0,(%rax)
    y[i] = r46 * (*x);
 37a:	8b 45 c0             	mov    -0x40(%rbp),%eax
 37d:	48 98                	cltq   
 37f:	48 8d 14 85 00 00 00 	lea    0x0(,%rax,4),%rdx
 386:	00 
 387:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
 38b:	48 01 c2             	add    %rax,%rdx
 38e:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 393:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 396:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 39a:	f2 0f 10 08          	movsd  (%rax),%xmm1
 39e:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 3a2:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 3a6:	66 0f 5a c0          	cvtpd2ps %xmm0,%xmm0
 3aa:	f3 0f 11 02          	movss  %xmm0,(%rdx)
  a2 = a - t23 * a1;

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 3ae:	83 45 c0 01          	addl   $0x1,-0x40(%rbp)
 3b2:	8b 45 c0             	mov    -0x40(%rbp),%eax
 3b5:	3b 45 bc             	cmp    -0x44(%rbp),%eax
 3b8:	0f 8c 95 fe ff ff    	jl     253 <vranlc+0x9e>
    t4 = (int) (r46 * t3) ;
    *x = t3 - t46 * t4;
    y[i] = r46 * (*x);
  }

  return;
 3be:	90                   	nop
}
 3bf:	5d                   	pop    %rbp
 3c0:	c3                   	retq   
