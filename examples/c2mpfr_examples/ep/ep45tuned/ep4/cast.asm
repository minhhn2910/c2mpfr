
randdp_cast.o:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <randlc>:
#include <stdio.h>
#include <math.h>

float randlc( double *x, double a )
{
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 89 7d b8          	mov    %rdi,-0x48(%rbp)
   8:	f2 0f 11 45 b0       	movsd  %xmm0,-0x50(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
   d:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 13 <randlc+0x13>
  13:	89 45 c0             	mov    %eax,-0x40(%rbp)
  const float r46 = r23 * r23;
  16:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  1b:	f3 0f 59 45 c0       	mulss  -0x40(%rbp),%xmm0
  20:	f3 0f 11 45 c4       	movss  %xmm0,-0x3c(%rbp)
  const float t23 = 8.388608e+06;
  25:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 2b <randlc+0x2b>
  2b:	89 45 c8             	mov    %eax,-0x38(%rbp)
  const float t46 = t23 * t23;
  2e:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  33:	f3 0f 59 45 c8       	mulss  -0x38(%rbp),%xmm0
  38:	f3 0f 11 45 cc       	movss  %xmm0,-0x34(%rbp)
  float r;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
  3d:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  42:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  45:	f2 0f 59 45 b0       	mulsd  -0x50(%rbp),%xmm0
  4a:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  a1 = (int) t1;
  4f:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
  54:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  58:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
  5c:	f3 0f 11 45 d0       	movss  %xmm0,-0x30(%rbp)
  a2 = a - t23 * a1;
  61:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  66:	f3 0f 59 45 d0       	mulss  -0x30(%rbp),%xmm0
  6b:	0f 14 c0             	unpcklps %xmm0,%xmm0
  6e:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  71:	f2 0f 10 4d b0       	movsd  -0x50(%rbp),%xmm1
  76:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  7a:	66 0f 28 c1          	movapd %xmm1,%xmm0
  7e:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
  82:	66 0f 5a d8          	cvtpd2ps %xmm0,%xmm3
  86:	f3 0f 11 5d d4       	movss  %xmm3,-0x2c(%rbp)
  //--------------------------------------------------------------------
  //  Break X into two parts such that X = 2^23 * X1 + X2, compute
  //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
  //  X = 2^23 * Z + A2 * X2  (mod 2^46).
  //--------------------------------------------------------------------
  t1 = r23 * (*x);
  8b:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
  90:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  93:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
  97:	f2 0f 10 08          	movsd  (%rax),%xmm1
  9b:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
  9f:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  x1 = (int) t1;
  a4:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
  a9:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
  ad:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
  b1:	f3 0f 11 45 d8       	movss  %xmm0,-0x28(%rbp)
  
  x2 = *x - t23 * x1;
  b6:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
  ba:	f2 0f 10 08          	movsd  (%rax),%xmm1
  be:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
  c3:	f3 0f 59 45 d8       	mulss  -0x28(%rbp),%xmm0
  c8:	0f 14 c0             	unpcklps %xmm0,%xmm0
  cb:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  ce:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
  d2:	66 0f 28 c1          	movapd %xmm1,%xmm0
  d6:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
  da:	66 0f 5a e0          	cvtpd2ps %xmm0,%xmm4
  de:	f3 0f 11 65 dc       	movss  %xmm4,-0x24(%rbp)
  
  //~ t1 = a1 * x2 + a2 * x1;
  t1 = a1 * (double) x2 + a2 * (double) x1;
  e3:	f3 0f 10 4d d0       	movss  -0x30(%rbp),%xmm1
  e8:	0f 5a c9             	cvtps2pd %xmm1,%xmm1
  eb:	f3 0f 10 45 dc       	movss  -0x24(%rbp),%xmm0
  f0:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
  f3:	f2 0f 59 c8          	mulsd  %xmm0,%xmm1
  f7:	f3 0f 10 55 d4       	movss  -0x2c(%rbp),%xmm2
  fc:	0f 5a d2             	cvtps2pd %xmm2,%xmm2
  ff:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 104:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 107:	f2 0f 59 c2          	mulsd  %xmm2,%xmm0
 10b:	f2 0f 58 c1          	addsd  %xmm1,%xmm0
 10f:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  
//~ if(t1!=t1_temp)
	//~ printf("diff %lf, %lf \n\n",t1,t1_temp);
//~ printf("%E, %E \n\n",t1,t1_temp);
  
  t2 = (int) (r23 * t1);
 114:	f3 0f 10 45 c0       	movss  -0x40(%rbp),%xmm0
 119:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 11c:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 121:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 125:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 129:	f3 0f 11 45 e0       	movss  %xmm0,-0x20(%rbp)
  z = t1 - t23 * t2;
 12e:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 133:	f3 0f 59 45 e0       	mulss  -0x20(%rbp),%xmm0
 138:	0f 14 c0             	unpcklps %xmm0,%xmm0
 13b:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 13e:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 143:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 147:	66 0f 28 c1          	movapd %xmm1,%xmm0
 14b:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 14f:	66 0f 5a e8          	cvtpd2ps %xmm0,%xmm5
 153:	f3 0f 11 6d e4       	movss  %xmm5,-0x1c(%rbp)
  
  //~ t3 = t23 * z + a2 * (double) x2;
  t3 = t23 * z + a2 * x2;
 158:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 15d:	0f 28 c8             	movaps %xmm0,%xmm1
 160:	f3 0f 59 4d e4       	mulss  -0x1c(%rbp),%xmm1
 165:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
 16a:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 16f:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 173:	0f 14 c0             	unpcklps %xmm0,%xmm0
 176:	0f 5a f0             	cvtps2pd %xmm0,%xmm6
 179:	f2 0f 11 75 f8       	movsd  %xmm6,-0x8(%rbp)
  
  t4 = (int) (r46 * t3);
 17e:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 183:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 186:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 18b:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 18f:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 193:	f3 0f 11 45 e8       	movss  %xmm0,-0x18(%rbp)
  *x = t3 - t46 * t4;
 198:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 19d:	f3 0f 59 45 e8       	mulss  -0x18(%rbp),%xmm0
 1a2:	0f 14 c0             	unpcklps %xmm0,%xmm0
 1a5:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 1a8:	f2 0f 10 4d f8       	movsd  -0x8(%rbp),%xmm1
 1ad:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 1b1:	66 0f 28 c1          	movapd %xmm1,%xmm0
 1b5:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
 1b9:	f2 0f 11 00          	movsd  %xmm0,(%rax)
  r = r46 * (*x);
 1bd:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 1c2:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 1c5:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
 1c9:	f2 0f 10 08          	movsd  (%rax),%xmm1
 1cd:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 1d1:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 1d5:	66 0f 5a f8          	cvtpd2ps %xmm0,%xmm7
 1d9:	f3 0f 11 7d ec       	movss  %xmm7,-0x14(%rbp)

  return r;
 1de:	8b 45 ec             	mov    -0x14(%rbp),%eax
}
 1e1:	89 45 ac             	mov    %eax,-0x54(%rbp)
 1e4:	f3 0f 10 45 ac       	movss  -0x54(%rbp),%xmm0
 1e9:	5d                   	pop    %rbp
 1ea:	c3                   	retq   

00000000000001eb <vranlc>:


void vranlc( int n, double *x, double a, float y[] )
{
 1eb:	55                   	push   %rbp
 1ec:	48 89 e5             	mov    %rsp,%rbp
 1ef:	89 7d bc             	mov    %edi,-0x44(%rbp)
 1f2:	48 89 75 b0          	mov    %rsi,-0x50(%rbp)
 1f6:	f2 0f 11 45 a8       	movsd  %xmm0,-0x58(%rbp)
 1fb:	48 89 55 a0          	mov    %rdx,-0x60(%rbp)
  // r46 = r23 * r23;
  // t23 = pow(2.0, 23.0);
  ////  pow(2.0, 23.0) = 8.388608e+06
  // t46 = t23 * t23;

  const float r23 = 1.1920928955078125e-07;
 1ff:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 205 <vranlc+0x1a>
 205:	89 45 c4             	mov    %eax,-0x3c(%rbp)
  const float r46 = r23 * r23;
 208:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 20d:	f3 0f 59 45 c4       	mulss  -0x3c(%rbp),%xmm0
 212:	f3 0f 11 45 c8       	movss  %xmm0,-0x38(%rbp)
  const float t23 = 8.388608e+06;
 217:	8b 05 00 00 00 00    	mov    0x0(%rip),%eax        # 21d <vranlc+0x32>
 21d:	89 45 cc             	mov    %eax,-0x34(%rbp)
  const float t46 = t23 * t23;
 220:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 225:	f3 0f 59 45 cc       	mulss  -0x34(%rbp),%xmm0
 22a:	f3 0f 11 45 d0       	movss  %xmm0,-0x30(%rbp)
  int i;

  //--------------------------------------------------------------------
  //  Break A into two parts such that A = 2^23 * A1 + A2.
  //--------------------------------------------------------------------
  t1 = r23 * a;
 22f:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 234:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 237:	f2 0f 59 45 a8       	mulsd  -0x58(%rbp),%xmm0
 23c:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
  a1 = (int) t1;
 241:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 246:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 24a:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 24e:	f3 0f 11 45 d4       	movss  %xmm0,-0x2c(%rbp)
  a2 = a - t23 * a1;
 253:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 258:	f3 0f 59 45 d4       	mulss  -0x2c(%rbp),%xmm0
 25d:	0f 14 c0             	unpcklps %xmm0,%xmm0
 260:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 263:	f2 0f 10 4d a8       	movsd  -0x58(%rbp),%xmm1
 268:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 26c:	66 0f 28 c1          	movapd %xmm1,%xmm0
 270:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 274:	66 0f 5a d0          	cvtpd2ps %xmm0,%xmm2
 278:	f3 0f 11 55 d8       	movss  %xmm2,-0x28(%rbp)

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 27d:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
 284:	e9 5f 01 00 00       	jmpq   3e8 <vranlc+0x1fd>
    //--------------------------------------------------------------------
    //  Break X into two parts such that X = 2^23 * X1 + X2, compute
    //  Z = A1 * X2 + A2 * X1  (mod 2^23), and then
    //  X = 2^23 * Z + A2 * X2  (mod 2^46).
    //--------------------------------------------------------------------
    t1 = r23 * (*x);
 289:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 28e:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 291:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 295:	f2 0f 10 08          	movsd  (%rax),%xmm1
 299:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 29d:	f2 0f 11 45 f0       	movsd  %xmm0,-0x10(%rbp)
    x1 = (int) t1;
 2a2:	f2 0f 10 45 f0       	movsd  -0x10(%rbp),%xmm0
 2a7:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 2ab:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 2af:	f3 0f 11 45 dc       	movss  %xmm0,-0x24(%rbp)
    
    x2 = *x - t23 * x1;
 2b4:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2b8:	f2 0f 10 08          	movsd  (%rax),%xmm1
 2bc:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 2c1:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 2c6:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2c9:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 2cc:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 2d0:	66 0f 28 c1          	movapd %xmm1,%xmm0
 2d4:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 2d8:	66 0f 5a d8          	cvtpd2ps %xmm0,%xmm3
 2dc:	f3 0f 11 5d e0       	movss  %xmm3,-0x20(%rbp)
    
    //~ t1 = a1 * (double)x2 + a2 * (double)x1;
    t1 = a1 * x2 + a2 * x1;
 2e1:	f3 0f 10 45 d4       	movss  -0x2c(%rbp),%xmm0
 2e6:	0f 28 c8             	movaps %xmm0,%xmm1
 2e9:	f3 0f 59 4d e0       	mulss  -0x20(%rbp),%xmm1
 2ee:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 2f3:	f3 0f 59 45 dc       	mulss  -0x24(%rbp),%xmm0
 2f8:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 2fc:	0f 14 c0             	unpcklps %xmm0,%xmm0
 2ff:	0f 5a e0             	cvtps2pd %xmm0,%xmm4
 302:	f2 0f 11 65 f0       	movsd  %xmm4,-0x10(%rbp)
    
    t2 = (int) (r23 * t1);
 307:	f3 0f 10 45 c4       	movss  -0x3c(%rbp),%xmm0
 30c:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 30f:	f2 0f 59 45 f0       	mulsd  -0x10(%rbp),%xmm0
 314:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 318:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 31c:	f3 0f 11 45 e4       	movss  %xmm0,-0x1c(%rbp)
    z = t1 - t23 * t2;
 321:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 326:	f3 0f 59 45 e4       	mulss  -0x1c(%rbp),%xmm0
 32b:	0f 14 c0             	unpcklps %xmm0,%xmm0
 32e:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 331:	f2 0f 10 4d f0       	movsd  -0x10(%rbp),%xmm1
 336:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 33a:	66 0f 28 c1          	movapd %xmm1,%xmm0
 33e:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 342:	66 0f 5a e8          	cvtpd2ps %xmm0,%xmm5
 346:	f3 0f 11 6d e8       	movss  %xmm5,-0x18(%rbp)
    
    //~ t3 = t23 * z + a2 * (double)x2;
    t3 = t23 * z + a2 * x2;
 34b:	f3 0f 10 45 cc       	movss  -0x34(%rbp),%xmm0
 350:	0f 28 c8             	movaps %xmm0,%xmm1
 353:	f3 0f 59 4d e8       	mulss  -0x18(%rbp),%xmm1
 358:	f3 0f 10 45 d8       	movss  -0x28(%rbp),%xmm0
 35d:	f3 0f 59 45 e0       	mulss  -0x20(%rbp),%xmm0
 362:	f3 0f 58 c1          	addss  %xmm1,%xmm0
 366:	0f 14 c0             	unpcklps %xmm0,%xmm0
 369:	0f 5a f0             	cvtps2pd %xmm0,%xmm6
 36c:	f2 0f 11 75 f8       	movsd  %xmm6,-0x8(%rbp)
    
    t4 = (int) (r46 * t3) ;
 371:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 376:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 379:	f2 0f 59 45 f8       	mulsd  -0x8(%rbp),%xmm0
 37e:	f2 0f 2c c0          	cvttsd2si %xmm0,%eax
 382:	f3 0f 2a c0          	cvtsi2ss %eax,%xmm0
 386:	f3 0f 11 45 ec       	movss  %xmm0,-0x14(%rbp)
    *x = t3 - t46 * t4;
 38b:	f3 0f 10 45 d0       	movss  -0x30(%rbp),%xmm0
 390:	f3 0f 59 45 ec       	mulss  -0x14(%rbp),%xmm0
 395:	0f 14 c0             	unpcklps %xmm0,%xmm0
 398:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 39b:	f2 0f 10 4d f8       	movsd  -0x8(%rbp),%xmm1
 3a0:	f2 0f 5c c8          	subsd  %xmm0,%xmm1
 3a4:	66 0f 28 c1          	movapd %xmm1,%xmm0
 3a8:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3ac:	f2 0f 11 00          	movsd  %xmm0,(%rax)
    y[i] = r46 * (*x);
 3b0:	8b 45 c0             	mov    -0x40(%rbp),%eax
 3b3:	48 98                	cltq   
 3b5:	48 8d 14 85 00 00 00 	lea    0x0(,%rax,4),%rdx
 3bc:	00 
 3bd:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
 3c1:	48 01 c2             	add    %rax,%rdx
 3c4:	f3 0f 10 45 c8       	movss  -0x38(%rbp),%xmm0
 3c9:	0f 5a c0             	cvtps2pd %xmm0,%xmm0
 3cc:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3d0:	f2 0f 10 08          	movsd  (%rax),%xmm1
 3d4:	f2 0f 59 c1          	mulsd  %xmm1,%xmm0
 3d8:	66 0f 14 c0          	unpcklpd %xmm0,%xmm0
 3dc:	66 0f 5a c0          	cvtpd2ps %xmm0,%xmm0
 3e0:	f3 0f 11 02          	movss  %xmm0,(%rdx)
  a2 = a - t23 * a1;

  //--------------------------------------------------------------------
  //  Generate N results.   This loop is not vectorizable.
  //--------------------------------------------------------------------
  for ( i = 0; i < n; i++ ) {
 3e4:	83 45 c0 01          	addl   $0x1,-0x40(%rbp)
 3e8:	8b 45 c0             	mov    -0x40(%rbp),%eax
 3eb:	3b 45 bc             	cmp    -0x44(%rbp),%eax
 3ee:	0f 8c 95 fe ff ff    	jl     289 <vranlc+0x9e>
    t4 = (int) (r46 * t3) ;
    *x = t3 - t46 * t4;
    y[i] = r46 * (*x);
  }

  return;
 3f4:	90                   	nop
}
 3f5:	5d                   	pop    %rbp
 3f6:	c3                   	retq   
