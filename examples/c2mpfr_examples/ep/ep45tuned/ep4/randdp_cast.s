	.file	"randdp_cast.c"
	.text
	.globl	randlc
	.type	randlc, @function
randlc:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -72(%rbp)
	movsd	%xmm0, -80(%rbp)
	movl	.LC0(%rip), %eax
	movl	%eax, -64(%rbp)
	movss	-64(%rbp), %xmm0
	mulss	-64(%rbp), %xmm0
	movss	%xmm0, -60(%rbp)
	movl	.LC1(%rip), %eax
	movl	%eax, -56(%rbp)
	movss	-56(%rbp), %xmm0
	mulss	-56(%rbp), %xmm0
	movss	%xmm0, -52(%rbp)
	movss	-64(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-80(%rbp), %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -48(%rbp)
	movss	-56(%rbp), %xmm0
	mulss	-48(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-80(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm3
	movss	%xmm3, -44(%rbp)
	movss	-64(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movq	-72(%rbp), %rax
	movsd	(%rax), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -40(%rbp)
	movq	-72(%rbp), %rax
	movsd	(%rax), %xmm1
	movss	-56(%rbp), %xmm0
	mulss	-40(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm4
	movss	%xmm4, -36(%rbp)
	movss	-48(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-36(%rbp), %xmm1
	movss	-44(%rbp), %xmm0
	mulss	-40(%rbp), %xmm0
	addss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm5
	movsd	%xmm5, -16(%rbp)
	movss	-48(%rbp), %xmm1
	cvtps2pd	%xmm1, %xmm1
	movss	-36(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm1
	movss	-44(%rbp), %xmm2
	cvtps2pd	%xmm2, %xmm2
	movss	-40(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbp)
	movss	-64(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -32(%rbp)
	movss	-56(%rbp), %xmm0
	mulss	-32(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-16(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm6
	movss	%xmm6, -28(%rbp)
	movss	-56(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-28(%rbp), %xmm1
	movss	-44(%rbp), %xmm0
	mulss	-36(%rbp), %xmm0
	addss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm7
	movsd	%xmm7, -8(%rbp)
	movss	-60(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-8(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -24(%rbp)
	movss	-52(%rbp), %xmm0
	mulss	-24(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-8(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movq	-72(%rbp), %rax
	movsd	%xmm0, (%rax)
	movss	-60(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movq	-72(%rbp), %rax
	movsd	(%rax), %xmm1
	mulsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm2
	movss	%xmm2, -20(%rbp)
	movl	-20(%rbp), %eax
	movl	%eax, -84(%rbp)
	movss	-84(%rbp), %xmm0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	randlc, .-randlc
	.globl	vranlc
	.type	vranlc, @function
vranlc:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -68(%rbp)
	movq	%rsi, -80(%rbp)
	movsd	%xmm0, -88(%rbp)
	movq	%rdx, -96(%rbp)
	movl	.LC0(%rip), %eax
	movl	%eax, -60(%rbp)
	movss	-60(%rbp), %xmm0
	mulss	-60(%rbp), %xmm0
	movss	%xmm0, -56(%rbp)
	movl	.LC1(%rip), %eax
	movl	%eax, -52(%rbp)
	movss	-52(%rbp), %xmm0
	mulss	-52(%rbp), %xmm0
	movss	%xmm0, -48(%rbp)
	movss	-60(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-88(%rbp), %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -44(%rbp)
	movss	-52(%rbp), %xmm0
	mulss	-44(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-88(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm2
	movss	%xmm2, -40(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L4
.L5:
	movss	-60(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movq	-80(%rbp), %rax
	movsd	(%rax), %xmm1
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -36(%rbp)
	movq	-80(%rbp), %rax
	movsd	(%rax), %xmm1
	movss	-52(%rbp), %xmm0
	mulss	-36(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm3
	movss	%xmm3, -32(%rbp)
	movss	-44(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-32(%rbp), %xmm1
	movss	-40(%rbp), %xmm0
	mulss	-36(%rbp), %xmm0
	addss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm4
	movsd	%xmm4, -16(%rbp)
	movss	-60(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-16(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -28(%rbp)
	movss	-52(%rbp), %xmm0
	mulss	-28(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-16(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm5
	movss	%xmm5, -24(%rbp)
	movss	-52(%rbp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	-24(%rbp), %xmm1
	movss	-40(%rbp), %xmm0
	mulss	-32(%rbp), %xmm0
	addss	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm6
	movsd	%xmm6, -8(%rbp)
	movss	-56(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	mulsd	-8(%rbp), %xmm0
	cvttsd2si	%xmm0, %eax
	cvtsi2ss	%eax, %xmm0
	movss	%xmm0, -20(%rbp)
	movss	-48(%rbp), %xmm0
	mulss	-20(%rbp), %xmm0
	unpcklps	%xmm0, %xmm0
	cvtps2pd	%xmm0, %xmm0
	movsd	-8(%rbp), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movq	-80(%rbp), %rax
	movsd	%xmm0, (%rax)
	movl	-64(%rbp), %eax
	cltq
	leaq	0(,%rax,4), %rdx
	movq	-96(%rbp), %rax
	addq	%rax, %rdx
	movss	-56(%rbp), %xmm0
	cvtps2pd	%xmm0, %xmm0
	movq	-80(%rbp), %rax
	movsd	(%rax), %xmm1
	mulsd	%xmm1, %xmm0
	unpcklpd	%xmm0, %xmm0
	cvtpd2ps	%xmm0, %xmm0
	movss	%xmm0, (%rdx)
	addl	$1, -64(%rbp)
.L4:
	movl	-64(%rbp), %eax
	cmpl	-68(%rbp), %eax
	jl	.L5
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	vranlc, .-vranlc
	.section	.rodata
	.align 4
.LC0:
	.long	872415232
	.align 4
.LC1:
	.long	1258291200
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
