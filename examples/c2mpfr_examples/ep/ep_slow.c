#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "randdp.h"
#include <mpfr.h>
typedef enum { false, true } logical;
typedef struct { 
  double real;
  double imag;
} dcomplex;

#define LEN 14 
 int config_vals[LEN];
  mpfr_t Mops_main;
  mpfr_t t1_main;
  mpfr_t t2_main;
  mpfr_t t3_main;
  mpfr_t t4_main;
  mpfr_t x1_main;
  mpfr_t x2_main;
  mpfr_t sx_main;
  mpfr_t sy_main;
  mpfr_t tm_main;
  mpfr_t an_main;
  mpfr_t tt_main;
  mpfr_t gc_main;
        mpfr_t temp_var_1;
            mpfr_t temp_var_2;
            mpfr_t temp_var_3;
            mpfr_t temp_var_4;
            mpfr_t temp_var_5;
            mpfr_t temp_var_6;
            mpfr_t temp_var_7;
            mpfr_t temp_var_8;
                mpfr_t temp_var_9;
                mpfr_t temp_var_10;
                mpfr_t temp_var_11;
        mpfr_t temp_var_12;
        mpfr_t temp_var_13;
    mpfr_t temp_var_14;
int init_mpfr() { 
  mpfr_init2(Mops_main, config_vals[1]);
  mpfr_init2(t1_main, config_vals[2]);
  mpfr_init2(t2_main, config_vals[3]);
  mpfr_init2(t3_main, config_vals[4]);
  mpfr_init2(t4_main, config_vals[5]);
  mpfr_init2(x1_main, config_vals[6]);
  mpfr_init2(x2_main, config_vals[7]);
  mpfr_init2(sx_main, config_vals[8]);
  mpfr_init2(sy_main, config_vals[9]);
  mpfr_init2(tm_main, config_vals[10]);
  mpfr_init2(an_main, config_vals[11]);
  mpfr_init2(tt_main, config_vals[12]);
  mpfr_init2(gc_main, config_vals[13]);
        mpfr_init2 (temp_var_1, config_vals[0]);
            mpfr_init2 (temp_var_2, config_vals[0]);
            mpfr_init2 (temp_var_3, config_vals[0]);
            mpfr_init2 (temp_var_4, config_vals[0]);
            mpfr_init2 (temp_var_5, config_vals[0]);
            mpfr_init2 (temp_var_6, config_vals[0]);
            mpfr_init2 (temp_var_7, config_vals[0]);
            mpfr_init2 (temp_var_8, config_vals[0]);
                mpfr_init2 (temp_var_9, config_vals[0]);
                mpfr_init2 (temp_var_10, config_vals[0]);
                mpfr_init2 (temp_var_11, config_vals[0]);
        mpfr_init2 (temp_var_12, config_vals[0]);
        mpfr_init2 (temp_var_13, config_vals[0]);
    mpfr_init2 (temp_var_14, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 60; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        return 0;             
}

double randlc(double *x, double a);
void vranlc(int n, double *x, double a, double y[]);
static double x[2 * (1 << 16)];
static double q[10];
int main()
{
 init_readconfig();
  double sx_verify_value;
  double sy_verify_value;
  double sx_err;
  double sy_err;
  int np;
  int i;
  int ik;
  int kk;
  int l;
  int k;
  int nit;
  int k_offset;
  int j;
  logical verified;
  logical timers_enabled;
  double dum[3] = {1.0, 1.0, 1.0};
  char size[16];
  printf("M = %d\n", 28);
  sprintf(size, "%15.0lf", pow(2.0, 28 + 1));
  j = 14;
  if (size[j] == '.')
    j--;

  size[j + 1] = '\0';
  printf("\n\n NAS Parallel Benchmarks (NPB3.3-SER-C) - EP Benchmark\n");
  printf("\n Number of random numbers generated: %15s\n", size);
  verified = false;
  np = 1 << (28 - 16);
  vranlc(0, &dum[0], dum[1], &dum[2]);
  dum[0] = randlc(&dum[1], dum[2]);
  for (i = 0; i < (2 * (1 << 16)); i++){
  {
    x[i] = -1.0e99;
  }

    }

  mpfr_set_d(Mops_main, log(sqrt(fabs(1.0 > 1.0 ? 1.0 : 1.0))), MPFR_RNDN);
  double t1_main_fix,t2_main_fix;
  
  mpfr_set_d(t1_main, 1220703125.0, MPFR_RNDN);
  t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
  
  vranlc(0, &t1_main_fix, 1220703125.0, x);
  mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
  
  
  mpfr_set_d(t1_main, 1220703125.0, MPFR_RNDN);
  t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
  
  for (i = 0; i < (16 + 1); i++){
  {
    mpfr_set_d(t2_main, randlc(&t1_main_fix, t1_main_fix), MPFR_RNDN);
  }
  

    }

mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);


  mpfr_set(an_main, t1_main, MPFR_RNDN);
  mpfr_set_d(tt_main, 271828183.0, MPFR_RNDN);
  mpfr_set_d(gc_main, 0.0, MPFR_RNDN);
  mpfr_set_d(sx_main, 0.0, MPFR_RNDN);
  mpfr_set_d(sy_main, 0.0, MPFR_RNDN);
  for (i = 0; i < 10; i++){
  {
    q[i] = 0.0;
  }

    }

  k_offset = -1;
  for (k = 1; k <= np; k++){
  {
    kk = k_offset + k;
    mpfr_set_d(t1_main, 271828183.0, MPFR_RNDN);
    t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
    
    mpfr_set(t2_main, an_main, MPFR_RNDN);
    t2_main_fix = mpfr_get_d(t2_main, MPFR_RNDN);
    
    for (i = 1; i <= 100; i++){
    {
      ik = kk / 2;
      if ((2 * ik) != kk)
      {
		t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
        mpfr_set_d(t3_main, randlc(&t1_main_fix, mpfr_get_d(t2_main, MPFR_RNDN)), MPFR_RNDN);
		mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
		}
      if (ik == 0)
        break;
		t2_main_fix = mpfr_get_d(t2_main, MPFR_RNDN);
      mpfr_set_d(t3_main, randlc(&t2_main_fix, mpfr_get_d(t2_main, MPFR_RNDN)), MPFR_RNDN);
      mpfr_set_d(t2_main, t2_main_fix, MPFR_RNDN);
      
      kk = ik;
    }

        }

	t1_main_fix = mpfr_get_d(t1_main, MPFR_RNDN);
    vranlc(2 * (1 << 16), &t1_main_fix, 1220703125.0, x);
    mpfr_set_d(t1_main, t1_main_fix, MPFR_RNDN);
    
    
    for (i = 0; i < (1 << 16); i++){
    {
      
            mpfr_set_d(temp_var_3, x[2 * i], MPFR_RNDN);

            mpfr_mul_d(temp_var_4, temp_var_3, 2.0, MPFR_RNDN);
            mpfr_sub_d(x1_main, (temp_var_4), 1.0, MPFR_RNDN);
      
            mpfr_set_d(temp_var_5, x[(2 * i) + 1], MPFR_RNDN);

            mpfr_mul_d(temp_var_6, temp_var_5, 2.0, MPFR_RNDN);
            mpfr_sub_d(x2_main, (temp_var_6), 1.0, MPFR_RNDN);
      
            mpfr_mul(temp_var_7, x1_main, x1_main, MPFR_RNDN);

            mpfr_mul(temp_var_8, x2_main, x2_main, MPFR_RNDN);
            mpfr_add(t1_main, (temp_var_7), (temp_var_8), MPFR_RNDN);
      if (mpfr_get_d(t1_main, MPFR_RNDN)<=1.0)
      {
		  
       
        

                mpfr_d_div(temp_var_10, ((-2.0) * log(mpfr_get_d(t1_main, MPFR_RNDN))), t1_main, MPFR_RNDN);
                 mpfr_set_d(t2_main, sqrt(mpfr_get_d(temp_var_10, MPFR_RNDN)), MPFR_RNDN);
                 
                mpfr_mul(t3_main, x1_main, t2_main, MPFR_RNDN);
                        mpfr_mul(t4_main, x2_main, t2_main, MPFR_RNDN);
        l = fabs(mpfr_get_d(t3_main, MPFR_RNDN)) > fabs(mpfr_get_d(t4_main, MPFR_RNDN)) ? fabs(mpfr_get_d(t3_main, MPFR_RNDN)) : fabs(mpfr_get_d(t4_main, MPFR_RNDN));
        q[l] = q[l] + 1.0;
        
                mpfr_add(sx_main, sx_main, t3_main, MPFR_RNDN);
                        mpfr_add(sy_main, sy_main, t4_main, MPFR_RNDN);
      }

    }

        }

  }

    }

  for (i = 0; i < 10; i++){
  {
            mpfr_add_d(gc_main, gc_main, q[i], MPFR_RNDN);
  }

    }

  nit = 0;
  verified = true;
  if (28 == 24)
  {
    sx_verify_value = -3.247834652034740e+3;
    sy_verify_value = -6.958407078382297e+3;
  }
  else
    if (28 == 25)
  {
    sx_verify_value = -2.863319731645753e+3;
    sy_verify_value = -6.320053679109499e+3;
  }
  else
    if (28 == 28)
  {
    sx_verify_value = -4.295875165629892e+3;
    sy_verify_value = -1.580732573678431e+4;
  }
  else
    if (28 == 30)
  {
    sx_verify_value = 4.033815542441498e+4;
    sy_verify_value = -2.660669192809235e+4;
  }
  else
    if (28 == 32)
  {
    sx_verify_value = 4.764367927995374e+4;
    sy_verify_value = -8.084072988043731e+4;
  }
  else
    if (28 == 36)
  {
    sx_verify_value = 1.982481200946593e+5;
    sy_verify_value = -1.020596636361769e+5;
  }
  else
    if (28 == 40)
  {
    sx_verify_value = -5.319717441530e+05;
    sy_verify_value = -3.688834557731e+05;
  }
  else
  {
    verified = false;
  }







  if (verified)
  {
    sx_err = fabs((mpfr_get_d(sx_main, MPFR_RNDN) - sx_verify_value) / sx_verify_value);
    sy_err = fabs((mpfr_get_d(sy_main, MPFR_RNDN) - sy_verify_value) / sy_verify_value);
    verified = (sx_err <= 1.0e-8) && (sy_err <= 1.0e-8);
  }

  printf("verify epsilon %.10lf, sx_err %.10lf, sy_err %.10lf\n", 1.0e-8, sx_err, sy_err);
  


    mpfr_d_div(temp_var_14, pow(2.0, 28 + 1), tm_main, MPFR_RNDN);
    mpfr_div_d(Mops_main, (temp_var_14), 1000000.0, MPFR_RNDN);
  return 0;
}

//end of conversion, hopefully it will work :)
