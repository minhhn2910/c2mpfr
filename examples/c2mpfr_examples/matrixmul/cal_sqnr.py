#!/usr/bin/env python
import os
import sys
import time
import subprocess
from math import log10
from mpi4py import MPI
#basic info
mpi_comm = MPI.COMM_WORLD
mpi_rank = MPI.COMM_WORLD.Get_rank()
mpi_size = MPI.COMM_WORLD.Get_size()
mpi_name = MPI.Get_processor_name()

def parse_output(line):
        list_target = []
        line.replace(" ", "")
        line.replace('\n','')
        #remove unexpected space
        array = line.split(',')
#       print array
        for target in array:
                try:
                        if(len(target)>0 and target!='\n'):
                                list_target.append(float(target))
                except:
                        #print "Failed to parse output string"
                        continue
        return  list_target
        
def run_program(double_program,float_program, seed):
	#get approximate ouput by float type
	output = subprocess.Popen([float_program, '%s'%(seed)], stdout=subprocess.PIPE).communicate()[0]
	floating_result = parse_output(output)
	#get target output
	output = subprocess.Popen([double_program, '%s'%(seed)], stdout=subprocess.PIPE).communicate()[0]
	target_result = parse_output(output)
	return check_output(floating_result,target_result)

def check_output(floating_result,target_result):
#TODO: modify this func to return checksum error. instead of true and false. feed the checsum error to greedy decision func
	if len(floating_result)== 0:
			print 'error: len(result) = 0'
	if len(floating_result) != len(target_result):
		print 'Error : float result has length: %s while double_result has length: %s' %(len(floating_result),len(target_result))
		print floating_result
		return 0.0
	signal_sqr = 0.0
	error_sqr = 0.0	
	for i in range(len(floating_result)):
		signal_sqr += target_result[i]**2
		error_sqr  += (floating_result[i]-target_result[i])**2
		#~ print error_sqr
	sqnr = 0.0
	if error_sqr !=0.0:
		sqnr = signal_sqr/error_sqr
	if sqnr != 0:
		return 1.0/sqnr
	else:
		return 0.0
def main (double_ver, float_ver, search_range):
	mpi_comm.Barrier() 
	if mpi_rank == 0: #master
		cal_sqnr_master(double_ver, float_ver, int(search_range))
	else:
		cal_sqnr_worker(double_ver, float_ver)
def cal_sqnr_master (double_ver, float_ver, search_range):
	SEED_RANGE  = search_range;
	sum_sqnr = 0.0
	max_sqnr = 0.0
	max_seed = 0
	min_sqnr = 100.0
	min_seed = 0
	num_elements_sent = 0
	recv_element = 0.0
	sqnr_vector = []
	for i in range(mpi_size-1):
		#~ print "send from %d  to %d data %d"%(0,i+1,i)
		if num_elements_sent < SEED_RANGE:
			mpi_comm.send(i, dest=i+1, tag=1)
			num_elements_sent += 1
		else:
			mpi_comm.send(0, dest=i+1, tag=0) #signal finish proc.
	for i in range(SEED_RANGE):	#mpi_here
		
		status = MPI.Status()
		recv_element = mpi_comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status) 		
		index = status.Get_tag()
		dest_proc = status.Get_source()
		
		current_sqnr = recv_element
		sqnr_vector.append(current_sqnr)
		if i % 20000 == 0:
			print "Tick %d %f"%(i,sum_sqnr/(i+1))
		
		if current_sqnr > max_sqnr : 
			max_sqnr = current_sqnr
			max_seed = index
		if current_sqnr < min_sqnr : 
			min_sqnr = current_sqnr
			min_seed = index
		sum_sqnr += current_sqnr		
		if num_elements_sent < SEED_RANGE: #send more to available proc
			mpi_comm.send(num_elements_sent, dest=dest_proc, tag=1)
			#~ print "send from %d  to %d data %d"%(0,dest_proc,num_elements_sent)
			num_elements_sent += 1
		else:
			mpi_comm.send(0, dest=dest_proc, tag=0) #signal finish proc.	

	print " \n max: " + str(max_sqnr) + "  max_seed " + str(max_seed) 
	print " \n min: " + str(min_sqnr) + "  min_seed " + str(min_seed)
	print "\n average: "+ str(sum_sqnr/SEED_RANGE) 		
	write_log(sqnr_vector)
	#~ for seed in range(SEED_RANGE):
		#~ if seed % 20000 == 0:
			#~ print "Tick %d %f"%(seed,sum_sqnr/(seed+1))
		#~ current_sqnr = 10*abs(log10(run_program(double_ver,float_ver,seed)))
		#~ if current_sqnr > max_sqnr : 
			#~ max_sqnr = current_sqnr
			#~ max_seed = seed
		#~ if current_sqnr < min_sqnr : 
			#~ min_sqnr = current_sqnr
			#~ min_seed = seed
		#~ sum_sqnr += current_sqnr

	
def cal_sqnr_worker (double_ver, float_ver):
	working_index = 0
	status = MPI.Status()
	working_index = mpi_comm.recv(source=0, tag=MPI.ANY_TAG, status=status) 
	while(status.Get_tag() > 0):	
		current_sqnr = 10*abs(log10(run_program(double_ver,float_ver,working_index)))
		mpi_comm.send(current_sqnr, dest=0, tag=working_index)		
		working_index = mpi_comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
	
	
def write_log(sqnr_vector):
	with open('log_sqnr.txt', 'w') as log_file:
		for element in sqnr_vector:
			log_file.write(str(element) + ",")
			
if __name__ == '__main__':
	arguments = sys.argv[1:]
	if len(arguments)!=3:
		print "usage ./cal_sqnr.py float_ver mpfr_ver range"
		exit(0)
	if not ('/' in arguments[0]):
		arguments[0] = './' + arguments[0]
	if not ('/' in arguments[1]):
		arguments[1] = './' + arguments[1]
	main(arguments[0],arguments[1], arguments[2])

