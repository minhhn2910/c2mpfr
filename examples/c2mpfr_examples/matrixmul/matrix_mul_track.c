
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpfr.h>
#define LEN 11 
 int config_vals[LEN];
 double max_abs_vars[LEN], min_abs_vars[LEN];
 
  mpfr_t a_track_main;
  mpfr_t b_track_main;
  mpfr_t c_track_main;
  mpfr_t p0_main;
  mpfr_t p1_main;
  mpfr_t p2_main;
  mpfr_t p3_main;
  mpfr_t p4_main;
  mpfr_t p5_main;
  mpfr_t p6_main;
    mpfr_t temp_var_1;
    mpfr_t temp_var_2;
    mpfr_t temp_var_3;
    mpfr_t temp_var_4;
    mpfr_t temp_var_5;
    mpfr_t temp_var_6;
    mpfr_t temp_var_7;
    mpfr_t temp_var_8;
    mpfr_t temp_var_9;
    mpfr_t temp_var_10;
    mpfr_t temp_var_11;
    mpfr_t temp_var_12;
    mpfr_t temp_var_13;
    mpfr_t temp_var_14;
int init_mpfr() { 
  mpfr_init2(a_track_main, config_vals[1]);
  mpfr_init2(b_track_main, config_vals[2]);
  mpfr_init2(c_track_main, config_vals[3]);
  mpfr_init2(p0_main, config_vals[4]);
  mpfr_init2(p1_main, config_vals[5]);
  mpfr_init2(p2_main, config_vals[6]);
  mpfr_init2(p3_main, config_vals[7]);
  mpfr_init2(p4_main, config_vals[8]);
  mpfr_init2(p5_main, config_vals[9]);
  mpfr_init2(p6_main, config_vals[10]);
    mpfr_init2 (temp_var_1, config_vals[0]);
    mpfr_init2 (temp_var_2, config_vals[0]);
    mpfr_init2 (temp_var_3, config_vals[0]);
    mpfr_init2 (temp_var_4, config_vals[0]);
    mpfr_init2 (temp_var_5, config_vals[0]);
    mpfr_init2 (temp_var_6, config_vals[0]);
    mpfr_init2 (temp_var_7, config_vals[0]);
    mpfr_init2 (temp_var_8, config_vals[0]);
    mpfr_init2 (temp_var_9, config_vals[0]);
    mpfr_init2 (temp_var_10, config_vals[0]);
    mpfr_init2 (temp_var_11, config_vals[0]);
    mpfr_init2 (temp_var_12, config_vals[0]);
    mpfr_init2 (temp_var_13, config_vals[0]);
    mpfr_init2 (temp_var_14, config_vals[0]);
}

int init_readconfig() {

  // For reading precision contents of config_file into array
   FILE *myFile;
     myFile = fopen("config_file.txt", "r");
 
        if (myFile == NULL) {
					printf("Error Reading File\n");
                exit (0);
                }
 
        int s;
        for (s = 0; s < LEN-1; s++) {
            fscanf(myFile, "%d,", &config_vals[s+1]);
                          }
		config_vals[0] = 53; //all temp_vars are 53 bits in mantissa
        fclose(myFile);
        init_mpfr();
        
        int i;
        for(i=0; i<LEN; i++)
        {
            max_abs_vars[i]=0;
            min_abs_vars[i]=10e10;
		}
        
        return 0;             
}


	double update_abs_max(int index, double current_val)
        {       int i = index;
                if (fabs(current_val) > max_abs_vars[i])
					max_abs_vars[i] = fabs(current_val);
                return max_abs_vars[i];
        }
 	double update_abs_min(int index, double current_val)
        {       int i = index;
                if ((fabs(current_val) < min_abs_vars[i]) && (fabs(current_val) != 0.0))
					min_abs_vars[i] = fabs(current_val);
                return min_abs_vars[i];
        }  


		void print_range_result()
		{
			printf("\n\n******************Final result*****************\n");
			int i;
			//calculate rage,iwl,exp
			int exp_vars[LEN], exp_avg[LEN];
			for (i = 0; i<LEN; i++)
			{
				exp_vars[i] = (int)(floor(log2(fabs(min_abs_vars[i]))));
				exp_avg[i] = (int)(floor(log2((min_abs_vars[i]+max_abs_vars[i])/2.0)));
			}
			
			
			for (i = 0; i<LEN; i++)
			{
				printf("VarName: %d \n",i);
	
				printf("Exponent(min_range): %d \n",exp_vars[i]);
				printf("Min_range: %.5f \n",min_abs_vars[i]);
				printf("Max_range: %.5f \n",max_abs_vars[i]);
				printf("Average_exponent: %d\n",exp_avg[i]);			
				printf("----------\n\n");
			}
		}




int main()
{
 init_readconfig();
  float a[2][2];
  float b[2][2];
  float c[2][2];
  int i;
  int j;
  int k;
  int sum = 0;
  srand(1234);
  for (i = 0; i < 2; i++){
    for (j = 0; j < 2; j++){
  {
    mpfr_set_d(a_track_main,  (float)rand()/RAND_MAX, MPFR_RNDZ);
    
a[i][j] = mpfr_get_d(a_track_main, MPFR_RNDZ);
	update_abs_max(0,a[i][j]);
	update_abs_min(0,a[i][j]);

printf("%f, ",a[i][j]);
printf("\n");
  }

    }


    }

  for (i = 0; i < 2; i++){
    for (j = 0; j < 2; j++){
  {
    mpfr_set_d(b_track_main,  (float)rand()/RAND_MAX, MPFR_RNDZ);
    
b[i][j] = mpfr_get_d(b_track_main, MPFR_RNDZ);
update_abs_max(1, b[i][j]);
update_abs_min(1, b[i][j]);
printf("%f, ",b[i][j]);
printf("\n");
  }

    }


    }

  mpfr_set_d(p0_main, (a[0][0] + a[1][1]) * (b[0][0] + b[1][1]), MPFR_RNDZ);
  update_abs_max(3, mpfr_get_d(p0_main, MPFR_RNDZ));
	update_abs_min(3, mpfr_get_d(p0_main, MPFR_RNDZ));
  mpfr_set_d(p1_main, (a[1][0] + a[1][1]) * b[0][0], MPFR_RNDZ);
  
    update_abs_max(4, mpfr_get_d(p1_main, MPFR_RNDZ));
	update_abs_min(4, mpfr_get_d(p1_main, MPFR_RNDZ));
  
  mpfr_set_d(p2_main, a[0][0] * (b[0][1] - b[1][1]), MPFR_RNDZ);
  
    
    update_abs_max(5, mpfr_get_d(p2_main, MPFR_RNDZ));
	update_abs_min(5, mpfr_get_d(p2_main, MPFR_RNDZ));
  
  mpfr_set_d(p3_main, a[1][1] * (b[1][0] - b[0][0]), MPFR_RNDZ);
  
      update_abs_max(6, mpfr_get_d(p3_main, MPFR_RNDZ));
	update_abs_min(6, mpfr_get_d(p3_main, MPFR_RNDZ));
  
  mpfr_set_d(p4_main, (a[0][0] + a[0][1]) * b[1][1], MPFR_RNDZ);
  
      update_abs_max(7, mpfr_get_d(p4_main, MPFR_RNDZ));
	update_abs_min(7, mpfr_get_d(p4_main, MPFR_RNDZ));
  
  mpfr_set_d(p5_main, (a[1][0] - a[0][0]) * (b[0][0] + b[0][1]), MPFR_RNDZ);
  
        update_abs_max(8, mpfr_get_d(p5_main, MPFR_RNDZ));
	update_abs_min(8, mpfr_get_d(p5_main, MPFR_RNDZ));
	
  mpfr_set_d(p6_main, (a[0][1] - a[1][1]) * (b[1][0] + b[1][1]), MPFR_RNDZ);
  
        update_abs_max(9, mpfr_get_d(p6_main, MPFR_RNDZ));
	update_abs_min(9, mpfr_get_d(p6_main, MPFR_RNDZ));









    mpfr_add(temp_var_11, p0_main, p3_main, MPFR_RNDZ);

    mpfr_sub(temp_var_12, (temp_var_11), p4_main, MPFR_RNDZ);
    mpfr_add(c_track_main, (temp_var_12), p6_main, MPFR_RNDZ);
  
c[0][0] = mpfr_get_d(c_track_main, MPFR_RNDZ);

      mpfr_add(c_track_main, p2_main, p4_main, MPFR_RNDZ);
  
c[0][1] = mpfr_get_d(c_track_main, MPFR_RNDZ);
      mpfr_add(c_track_main, p1_main, p3_main, MPFR_RNDZ);
  
c[1][0] = mpfr_get_d(c_track_main, MPFR_RNDZ);
  
    mpfr_add(temp_var_13, p0_main, p2_main, MPFR_RNDZ);

    mpfr_sub(temp_var_14, (temp_var_13), p1_main, MPFR_RNDZ);
    mpfr_add(c_track_main, (temp_var_14), p5_main, MPFR_RNDZ);
  
c[1][1] = mpfr_get_d(c_track_main, MPFR_RNDZ);


  for (i = 0; i < 2; i++){
    for (j = 0; j < 2; j++){
   printf("%f,", c[i][j]);
update_abs_max(2, c[i][j]);
update_abs_min(2, c[i][j]);
    }


    }
print_range_result();
  return 0;
}

//end of conversion, hopefully it will work :)
