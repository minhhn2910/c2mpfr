#include<stdio.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define M 2
#define N 2

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
			printf("./program seed\n");
			exit(0);
	}

double a[M][N], b[M][N],c[M][N];
double a_track, b_track, c_track;
double p0,p1,p2,p3,p4,p5,p6;
int i,j,k,sum=0;

srand(atoi(argv[1]));

for(i=0;i<M;i++)
   for(j=0;j<N;j++)
		{
			a_track = (double)rand()/RAND_MAX;
			a[i][j]= a_track;
		}

//for(i=0;i<M;i++)
//   for(j=0;j<N;j++)
      //  printf("\n Matrix a[%d][%d] = %f \n ",i,j,a[i][j]);

for(i=0;i<M;i++)
   for(j=0;j<N;j++)
        {
			b_track = (double)rand()/RAND_MAX;
			b[i][j]= b_track;
		}


//for(i=0;i<M;i++)
//   for(j=0;j<N;j++)
     //   printf("\n Matrix b[%d][%d] = %f \n ",i,j,b[i][j]);

        p0 = (a[0][0] + a[1][1]) * (b[0][0]+ b[1][1]);
        p1 = (a[1][0] + a[1][1]) *	 b[0][0];
        p2 = a[0][0] * (b[0][1] - b[1][1]);
        p3 = a[1][1] * (b[1][0] - b[0][0]);
        p4 = (a[0][0] + a[0][1]) * b[1][1];
        p5 = (a[1][0] - a[0][0]) * (b[0][0] + b[0][1]);
        p6 = (a[0][1] - a[1][1]) * (b[1][0] + b[1][1]);

       c_track = p0 + p3 - p4 + p6;
       c[0][0] = c_track;
       
       c_track = p2 + p4;
       c[0][1] = c_track;
       
       c_track = p1 + p3;
       c[1][0] = c_track;
       
       c_track = p0 + p2 - p1 + p5;
       c[1][1] = c_track;

for(i=0;i<M;i++)
   for(j=0;j<N;j++)
        printf("%.8lf,",c[i][j]);

  return 0;
}
